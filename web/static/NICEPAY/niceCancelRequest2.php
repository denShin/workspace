<?php
define('ULLENG_AREA_CODE', 'O_24');
define('CARMORE_DEV_REAL', 'app_v3');

header("Content-Type: text/html; charset=UTF-8");

try {
    require_once '../security/preventGet.php';
    require_once '../../../lib/php/pdo.php';
    require_once '../lib/log/cancel/cancelLogController.class.php';
    require_once '../lib/carmore/peakSeason/carmorePeakSeasonViewer.class.php';

    $peakseason_viewer = new carmore\peakSeason\CarmorePeakSeasonViewer();
    $cancel_date = date('Y-m-d H:i:s');

    if (__CheckGet__()) {
        $information = new ServerInformation();
        $msg = 'GET으로 접근하였습니다.||'.$information->__getIp__().'||'.$information->__getQueryString__();
        throw new Exception($msg, 99);
    }

    if (!(isset($_POST['phoneId'])) || !(isset($_POST['loginKey'])) || !(isset($_POST['userSerial'])) || !(isset($_POST['reservationIdx'])))
        throw new Exception('POST 값이 들어오지 않았습니다.',80);

    $phone_id = preg_replace("/[;\']/", "", $_POST['phoneId']);
    $login_key = preg_replace("/[;\']/", "", $_POST['loginKey']);
    $user_serial = preg_replace("/[;\']/", "", $_POST['userSerial']);
    $reservation_idx = preg_replace("/[;\']/", "", $_POST['reservationIdx']);

    $moid = trim($reservation_idx);

    $cancel_able_flag = 1;

    $query = "SELECT f_TID, f_Amt, f_resultcode, f_logdate FROM tbl_pay_log WHERE f_reservationidx=?";
    $sql = $pdo->prepare($query);
    $sql->execute([$reservation_idx]);
    $stmt = $sql->fetch();

    $tid = $stmt['f_TID'];
    $cancel_amt = $stmt['f_Amt'];
    $result_code = $stmt['f_resultcode'];
    $paid_date = $stmt['f_logdate'];

    $date2 = strtotime($cancel_date);
    $date1 = strtotime($paid_date);
    $paid_diff = (int)($date2 - $date1);

    $query = ' SELECT reserv_area_code, f_reservationidx, f_bookingid, f_usrserial, f_rentstartdate,
  f_rentenddate, f_usepoint, f_rentprice, f_insuprice, f_delivprice, f_couponserial FROM tbl_reservation_list WHERE f_reservationidx=? ';
    $sql = $pdo->prepare($query);
    $sql->execute([$reservation_idx]);
    $stmt = $sql->fetch(PDO::FETCH_ASSOC);

    $reserv_area_code = $stmt['reserv_area_code'];
    $bookId = $stmt['f_bookingid'];
    $post_user_serial = $stmt['f_usrserial'];
    $rentstart = $stmt['f_rentstartdate'];
    $rentend = $stmt['f_rentenddate'];
    $use_point = $stmt['f_usepoint'];
    $rental_cost = $stmt['f_rentprice'];
    $cdw_cost = $stmt['f_insuprice'];
    $deliv_cost = $stmt['f_delivprice'];
    $idx = $stmt['f_reservationidx'];

    $coupon_serial = $stmt['f_couponserial'];

    $date2 = strtotime($rentstart);
    $date1 = strtotime($cancel_date);
    $diff = (int)($date2 - $date1);

    $cancel_log_controller = new \cancel\log\CancelLogController();
    $cancel_log_controller->requestCancel($post_user_serial, $reservation_idx);

    $user_return_pointer = 0;
    $pay_all_point_flag = 0;
    $account_info_cancel_val = 0;

    $cancel_per = 1;

    if ($reserv_area_code == ULLENG_AREA_CODE) {    //울릉도
        if ($diff >= 604800 || $paid_diff <= 3600) {  //일주일 이전
            $cancel_able_flag = 1;
            $partial_cancel_code = 0;

            $cancel_amt = (int)trim($cancel_amt);
            $user_return_pointer = $use_point;

            if ($result_code == '3000') {    //전부 포인트로 결제한것 > 나이스페이 안감
                $pay_all_point_flag = 1;
                $account_info_cancel_val = 3;
            }
        } elseif ($diff < 604800 && $diff >= 259200) {  //일주일~3일 사이
            $cancel_able_flag = 1;
            $partial_cancel_code = 1;
            $cancel_amt = (int)trim($cancel_amt);

            $cancel_per = 0.7;

            $tmp_total_cost = (int)$rental_cost + (int)$cdw_cost + (int)$deliv_cost;    //총 렌트비
            $tmp_refund_pay = (int)($tmp_total_cost * 0.7);    //환불비
            $tmp_penalty_pay = $tmp_total_cost - $tmp_refund_pay;

            if ($use_point >= $tmp_penalty_pay) {
                //사용한 포인트가 위약금보다 많은 경우
                //(사용한포인트-위약금)=>a 포인트복구 // 결제한금액 모두 취소
                $user_return_pointer = (int)$use_point-(int)$tmp_penalty_pay;
                $partial_cancel_code = 0;
            } else {
                //환불금이 내가 사용한 포인트보다 많은경우
                //포인트 0원이 되고  토탈에서 90%가 취소금액 (어떠한경우에도)
                $user_return_pointer = 0;
                $cancel_amt = (int)$cancel_amt-((int)$tmp_penalty_pay-(int)$use_point);

            }

            if ($result_code == '3000') {    //전부 포인트로 결제한것 > 나이스페이 안감
                $pay_all_point_flag = 1;
                $account_info_cancel_val = 2;
            }
        } elseif ($diff < 259200 && $diff >= 86400) {    //3일이내~ 당일사이(24시간)
            $cancel_able_flag = 1;
            $partial_cancel_code = 1;
            $cancel_amt = (int)trim($cancel_amt);

            $cancel_per = 0.5;

            $tmp_total_cost = (int)$rental_cost + (int)$cdw_cost + (int)$deliv_cost;    //총 렌트비
            $tmp_refund_pay = (int)($tmp_total_cost * 0.5);    //환불비
            $tmp_penalty_pay = $tmp_total_cost - $tmp_refund_pay;

            if ($use_point >= $tmp_penalty_pay) {
                //사용한 포인트가 위약금보다 많은 경우
                //(사용한포인트-위약금)=>a 포인트복구 // 결제한금액 모두 취소
                $user_return_pointer = (int)$use_point-(int)$tmp_penalty_pay;
                $partial_cancel_code = 0;
            } else {
                //환불금이 내가 사용한 포인트보다 많은경우
                //포인트 0원이 되고  토탈에서 90%가 취소금액 (어떠한경우에도)
                $user_return_pointer = 0;
                $cancel_amt = (int)$cancel_amt-((int)$tmp_penalty_pay-(int)$use_point);

            }

            if ($result_code == '3000') {    //전부 포인트로 결제한것 > 나이스페이 안감
                $pay_all_point_flag = 1;
                $account_info_cancel_val = 2;
            }
        } elseif ($diff < 86400)    //당일취소 & 시간지나서 취소 (취소불가)
            $cancel_able_flag = "0";
    } else {
        $rentstart_date = explode(' ', $rentstart)[0];
        $rentend_date = explode(' ', $rentend)[0];
        $is_peakseason = $peakseason_viewer->chkIsInPeakseason($rentstart_date, $rentend_date);
        if ($is_peakseason) {   //성수기
            if ($diff < 345600 && $diff >= 259200 || $paid_diff <= 3600) {  //4일~3일
                $cancel_able_flag = 1;
                $partial_cancel_code = 0;

                $cancel_amt = (int)trim($cancel_amt);
                $user_return_pointer = $use_point;

                if ($result_code == '3000') {    //전부 포인트로 결제한것 > 나이스페이 안감
                    $pay_all_point_flag = 1;
                    $account_info_cancel_val = 3;
                }
            } elseif ($diff < 259200 && $diff >= 172800) {  //3일~2일
                $cancel_able_flag = 1;
                $partial_cancel_code = 1;
                $cancel_amt = (int)trim($cancel_amt);

                $cancel_per = 0.9;

                $tmp_total_cost = (int)$rental_cost + (int)$cdw_cost + (int)$deliv_cost;    //총 렌트비
                $tmp_refund_pay = (int)($tmp_total_cost * 0.9);    //환불비
                $tmp_penalty_pay = $tmp_total_cost - $tmp_refund_pay;

                if ($use_point >= $tmp_penalty_pay) {
                    //사용한 포인트가 위약금보다 많은 경우
                    //(사용한포인트-위약금)=>a 포인트복구 // 결제한금액 모두 취소
                    $user_return_pointer = (int)$use_point-(int)$tmp_penalty_pay;
                    $partial_cancel_code = 0;
                } else {
                    //환불금이 내가 사용한 포인트보다 많은경우
                    //포인트 0원이 되고  토탈에서 90%가 취소금액 (어떠한경우에도)
                    $user_return_pointer = 0;
                    $cancel_amt = (int)$cancel_amt-((int)$tmp_penalty_pay-(int)$use_point);

                }

                if ($result_code == '3000') {    //전부 포인트로 결제한것 > 나이스페이 안감
                    $pay_all_point_flag = 1;
                    $account_info_cancel_val = 2;
                }
            } elseif ($diff < 172800 && $diff >= 86400) {   //2일~1일
                $cancel_able_flag = 1;
                $partial_cancel_code = 1;
                $cancel_amt = (int)trim($cancel_amt);

                $cancel_per = 0.8;

                $tmp_total_cost = (int)$rental_cost + (int)$cdw_cost + (int)$deliv_cost;    //총 렌트비
                $tmp_refund_pay = (int)($tmp_total_cost * 0.8);    //환불비
                $tmp_penalty_pay = $tmp_total_cost - $tmp_refund_pay;

                if ($use_point >= $tmp_penalty_pay) {
                    //사용한 포인트가 위약금보다 많은 경우
                    //(사용한포인트-위약금)=>a 포인트복구 // 결제한금액 모두 취소
                    $user_return_pointer = (int)$use_point-(int)$tmp_penalty_pay;
                    $partial_cancel_code = 0;
                } else {
                    //환불금이 내가 사용한 포인트보다 많은경우
                    //포인트 0원이 되고  토탈에서 90%가 취소금액 (어떠한경우에도)
                    $user_return_pointer = 0;
                    $cancel_amt = (int)$cancel_amt-((int)$tmp_penalty_pay-(int)$use_point);

                }

                if ($result_code == '3000') {    //전부 포인트로 결제한것 > 나이스페이 안감
                    $pay_all_point_flag = 1;
                    $account_info_cancel_val = 2;
                }
            } elseif ($diff < 86400 && $diff >= 0) {  //24시간 이내 취소(당일취소)
                $cancel_able_flag = 1;
                $partial_cancel_code = 1;
                $cancel_amt = (int)trim($cancel_amt);

                $cancel_per = 0.7;

                $tmp_total_cost = (int)$rental_cost + (int)$cdw_cost + (int)$deliv_cost;    //총 렌트비
                $tmp_refund_pay = (int)($tmp_total_cost * 0.7);    //환불비
                $tmp_penalty_pay = $tmp_total_cost - $tmp_refund_pay;

                if ($use_point >= $tmp_penalty_pay) {
                    //사용한 포인트가 위약금보다 많은 경우
                    //(사용한포인트-위약금)=>a 포인트복구 // 결제한금액 모두 취소
                    $user_return_pointer = (int)$use_point-(int)$tmp_penalty_pay;
                    $partial_cancel_code = 0;
                } else {
                    //환불금이 내가 사용한 포인트보다 많은경우
                    //포인트 0원이 되고  토탈에서 90%가 취소금액 (어떠한경우에도)
                    $user_return_pointer = 0;
                    $cancel_amt = (int)$cancel_amt-((int)$tmp_penalty_pay-(int)$use_point);

                }

                if ($result_code == '3000') {    //전부 포인트로 결제한것 > 나이스페이 안감
                    $pay_all_point_flag = 1;
                    $account_info_cancel_val = 2;
                }
            } elseif ($diff < 0)
                $cancel_able_flag = "0";
        } else {    //비수기
            if ($diff >= 86400 || $paid_diff <= 3600) {
                $cancel_able_flag = 1;
                $partial_cancel_code = 0;

                $cancel_amt = (int)trim($cancel_amt);
                $user_return_pointer = $use_point;

                if ($result_code == '3000') {    //전부 포인트로 결제한것 > 나이스페이 안감
                    $pay_all_point_flag = 1;
                    $account_info_cancel_val = 3;
                }
            } elseif ($diff < 86400 && $diff >= 0) {
                $cancel_able_flag = 1;
                $partial_cancel_code = 1;
                $cancel_amt = (int)trim($cancel_amt);

                $cancel_per = 0.9;

                $tmp_total_cost = (int)$rental_cost + (int)$cdw_cost + (int)$deliv_cost;    //총 렌트비
                $tmp_refund_pay = (int)($tmp_total_cost * 0.9);    //환불비
                $tmp_penalty_pay = $tmp_total_cost - $tmp_refund_pay;

                if ($use_point >= $tmp_penalty_pay) {
                    //사용한 포인트가 위약금보다 많은 경우
                    //(사용한포인트-위약금)=>a 포인트복구 // 결제한금액 모두 취소
                    $user_return_pointer = (int)$use_point-(int)$tmp_penalty_pay;
                    $partial_cancel_code = 0;
                } else {
                    //환불금이 내가 사용한 포인트보다 많은경우
                    //포인트 0원이 되고  토탈에서 90%가 취소금액 (어떠한경우에도)
                    $user_return_pointer = 0;
                    $cancel_amt = (int)$cancel_amt-((int)$tmp_penalty_pay-(int)$use_point);

                }

                if ($result_code == '3000') {    //전부 포인트로 결제한것 > 나이스페이 안감
                    $pay_all_point_flag = 1;
                    $account_info_cancel_val = 2;
                }
            } elseif ($diff < 0)    //취소 안됨
                $cancel_able_flag = "0";
        }
    }

    if ($cancel_able_flag == '1') {

        $query = "UPDATE tbl_reservation_list SET cancel_per=? WHERE f_reservationidx=?";
        $sql = $pdo->prepare($query);
        $sql->execute([$cancel_per, $reservation_idx]);

        if ($pay_all_point_flag == '1') {    //전체포인트결제의 취소처리
            $query = "UPDATE tbl_reservation_list SET f_paystatus = '2' WHERE f_reservationidx=?";
            $sql = $pdo->prepare($query);
            $sql->execute([$reservation_idx]);

            $query = "UPDATE carmore_reservation_term_information SET crti_state=2 WHERE crti_reservation_idx=?";
            $sql = $pdo->prepare($query);
            $sql->execute([$reservation_idx]);

            $cancel_log_controller->respondCancel($post_user_serial, $reservation_idx);

            $query = "
INSERT INTO tbl_cancel_log (f_reservationidx, f_bookingid, f_usrserial, f_regdate, f_resultcode, f_resultmsg, f_cancelamt, f_MID, f_TID, f_PartialCancelCode)
VALUES (:idx, :book, :us, :log, '2000', 'ALL POINT', '0', '', '', '1')";
            $sql = $pdo->prepare($query);
            $sql->bindParam(":idx", $reservation_idx);
            $sql->bindParam(":book", $bookId);
            $sql->bindParam(":us", $user_serial);
            $sql->bindParam(":log", $cancel_date);
            $sql->execute();

            $query = "UPDATE tbl_account_info SET status=? WHERE f_reservationidx=?";
            $sql = $pdo->prepare($query);
            $sql->execute([$account_info_cancel_val, $reservation_idx]);

            $query = "UPDATE new_rentSchedule SET state='11' WHERE serial=?";
            $sql = $pdo->prepare($query);
            $sql->execute([$bookId]);

            $chkQuery = "SELECT pointsrl FROM tbl_point_log WHERE pointmsg='취소로 인한 포인트 복구' AND f_reservationidx=?";
            $chkSql = $pdo->prepare($chkQuery);
            $chkSql->execute([$reservation_idx]);
            if ($chkStmt = $chkSql->fetch()) {

            } else {
                $query = "INSERT INTO tbl_point_log(usrserial, valpoint, regdate, pointtype, destroydate, pointmsg, logmsg, f_reservationidx) VALUES (:us, :val, :log, '4', '0000-00-00 00:00:00', '취소로 인한 포인트 복구', '취소로 인한 포인트 복구', :idx)";
                $sql = $pdo->prepare($query);
                $sql->bindParam(":us", $user_serial);
                $sql->bindParam(":val", $user_return_pointer);
                $sql->bindParam(":log", $cancel_date);
                $sql->bindParam(":idx", $reservation_idx);
                $sql->execute();

                $query = "UPDATE tbl_usr_list SET usrpoint=usrpoint+:p WHERE usrserial=:us";
                $sql = $pdo->prepare($query);
                $sql->bindParam(":p", $user_return_pointer);
                $sql->bindParam(":us", $post_user_serial);
                $sql->execute();
            }

            $query = 'UPDATE new_coupon_list SET status="0" WHERE serial=:cs';
            $sql = $pdo->prepare($query);
            $sql->bindParam(':cs', $coupon_serial);
            $sql->execute();
        }
    }
} catch (Exception $e) {
    $error_controller = new \error\ErrorController();
    $error_controller->errorLog($e, 'pay/payBefore');
}
?>

    <html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script language="javascript">
            function goCancel() {
                var payFlag = '<?=$pay_all_point_flag?>';
                var idx = '<?=$idx?>';
                var amt = '<?=$cancel_amt?>';
                var formNm = document.tranMgr;
                var cancelAble = '<?=$cancel_able_flag?>';

                if (cancelAble == 1) {
                    if (payFlag == 0)
                        formNm.submit();
                    else
                        location.href = 'https://carmore.kr/<?=CARMORE_DEV_REAL?>/php/cancelResultView.php?ver=180807&&result=1&&idx=' + idx + '&&amt=' + amt;
                } else
                    location.href = "https://carmore.kr/<?=CARMORE_DEV_REAL?>/php/cancelResultView.php?ver=180807&result=2&&idx="+idx+"&&amt=0";
            }
        </script>

    </head>

    <body onload="goCancel()">
        <form name="tranMgr" method="post" action="../niceCancelResult2.php?ver=180622">
            <input name="MID" type="hidden" value="teamo2000m" />
            <input name="TID" type="hidden" value="<?=$tid?>" />
            <input name="CancelAmt" type="hidden" value="<?=$cancel_amt?>" />
            <input name="CancelMsg" type="hidden" value="고객 요청" />
            <input name="CancelPwd" type="hidden" value="teamo2000" />
            <input name="Moid" type="hidden" value="<?=$moid?>" />
            <input name="PartialCancelCode" type="hidden" value="<?=$partial_cancel_code?>" />
            <input name="payAllPointFlag" type="hidden" value="<?=$pay_all_point_flag?>" />
            <input name="returnPoint" type="hidden" value="<?=$user_return_pointer?>" />
            <!--복구 포인트보내기-->
        </form>



    </body>

    </html>
