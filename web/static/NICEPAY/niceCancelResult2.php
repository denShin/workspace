<?php
header('Content-Type: text/html; charset=utf-8');
include_once('../../lib/php/pdo.php');
include_once('../../lib/security/decrypt.php');
include_once('../../lib/security/encrypt.php');
require_once 'lib/log/cancel/cancelLogController.class.php';
// Import Classes
require_once dirname(__FILE__).'/lib/nicepay/web/NicePayWEB.php';
require_once dirname(__FILE__).'/lib/nicepay/core/Constants.php';
require_once dirname(__FILE__).'/lib/nicepay/web/NicePayHttpServletRequestWrapper.php';

/** 1. Request Wrapper 클래스를 등록한다.  */ 
$httpRequestWrapper = new NicePayHttpServletRequestWrapper($_REQUEST);
$_REQUEST = $httpRequestWrapper->getHttpRequestMap();

/** 2. 소켓 어댑터와 연동하는 Web 인터페이스 객체를 생성한다.*/
$nicepayWEB = new NicePayWEB();

/** 2-1. 로그 디렉토리 설정 */
$nicepayWEB->setParam("NICEPAY_LOG_HOME","paylog");

/** 2-2. 로그 모드 설정(0: DISABLE, 1: ENABLE) */
$nicepayWEB->setParam("APP_LOG","1");

/** 2-3. 암호화플래그 설정(N: 평문, S:암호화) */
$nicepayWEB->setParam("EncFlag","S");

/** 2-4. 서비스모드 설정(결제 서비스 : PY0 , 취소 서비스 : CL0) */
$nicepayWEB->setParam("SERVICE_MODE", "CL0");

/** 2-5 UTF-8 (den)*/
$nicepayWEB->setParam("CHARSET", "UTF8"); // utf-8 사용시 옵션 설정

/** 3. 결제취소 요청 */
$responseDTO = $nicepayWEB->doService($_REQUEST);

/** 4. 취소결과 */
$resultCode = $responseDTO->getParameter("ResultCode"); // 결과코드 (정상 :2001(취소성공), 2211(환불성공), 그 외 에러)
$resultMsg = $responseDTO->getParameter("ResultMsg");   // 결과메시지
$cancelAmt = $responseDTO->getParameter("CancelAmt");   // 취소금액
$v_CancelAmt = (int)$cancelAmt;

$cancelDate = $responseDTO->getParameter("CancelDate");     // 취소일
$cancelTime = $responseDTO->getParameter("CancelTime");   // 취소시간
$cancelNum = $responseDTO->getParameter("CancelNum");   // 취소번호
$payMethod = $responseDTO->getParameter("PayMethod");   // 취소 결제수단
$mid = 	$responseDTO->getParameter("MID");              // 상점 ID
$tid = $responseDTO->getParameter("TID");               // TID

$getMoid = $responseDTO->getParameter("Moid");  // 주문번호


$resultCode = trim($resultCode);
$getMoid = trim($getMoid);
$getMoid = (int)$getMoid;

if ($resultCode == '2001')
{
    $payResult="1";
    
    $p_PartialCancelCode = preg_replace("/[;\']/", "", $_REQUEST["PartialCancelCode"]);
    $payAllPointFlag = preg_replace("/[;\']/", "", $_REQUEST["payAllPointFlag"]);
    $returnPoint = preg_replace("/[;\']/", "", $_REQUEST["returnPoint"]);
    
    $query = ' SELECT f_reservationidx, f_bookingid, f_usrserial, f_couponserial FROM tbl_reservation_list WHERE f_reservationidx=:m ';
    $sql = $pdo -> prepare($query);
    $sql -> bindParam(":m",$getMoid);
    $sql -> execute();
    $stmt = $sql -> fetch();
    
    $reservationIdx = $stmt['f_reservationidx'];
    $bookingId = $stmt['f_bookingid'];
    $userSerial = $stmt['f_usrserial'];
    $coupon_serial = $stmt['f_couponserial'];
    
    $logDate = date('Y-m-d H:i:s');  
    
    
    if ($payAllPointFlag == 1) {

        //전부 포인트로 한경우 이전에 이미 DB처리 완료
    } else {
        $query = ' UPDATE tbl_reservation_list SET f_paystatus="2" WHERE f_reservationidx=:m ';
        $sql = $pdo -> prepare($query);
        $sql -> bindParam(":m",$getMoid);
        $sql -> execute();

        $query = "UPDATE carmore_reservation_term_information SET crti_state=2 WHERE crti_reservation_idx=?";
        $sql = $pdo->prepare($query);
        $sql->execute([$getMoid]);

        $cancel_log_controller = new \cancel\log\CancelLogController();
        $cancel_log_controller->respondCancel($userSerial, $getMoid);

        $query = ' INSERT INTO tbl_cancel_log( f_reservationidx, f_bookingid, f_usrserial, f_regdate, f_resultcode, f_resultmsg, f_cancelamt, f_MID, f_TID, f_PartialCancelCode ) VALUES (:reservationIdx, :book, :us, :log, :resultcode, :msg, :amt, :mid, :tid, :partial) ';
        $sql = $pdo -> prepare($query);
        $sql -> bindParam(":reservationIdx",$reservationIdx);
        $sql -> bindParam(":book",$bookingId);
        $sql -> bindParam(":us",$userSerial);
        $sql -> bindParam(":log",$logDate);
        $sql -> bindParam(":resultcode",$resultCode);
        $sql -> bindParam(":msg",$resultMsg);
        $sql -> bindParam(":amt",$v_CancelAmt);
        $sql -> bindParam(":mid",$mid);
        $sql -> bindParam(":tid",$tid);
        $sql -> bindParam(":partial",$p_PartialCancelCode);
        $sql -> execute();
        
        //status는 1:반납완료 2:부분취소 3:완전취소 
        $cancelStatus = 3;
        if ($p_PartialCancelCode == '1')
            $cancelStatus = 2;
        
        $query = ' UPDATE tbl_account_info SET status=:c WHERE f_reservationidx=:idx ';
        $sql = $pdo -> prepare($query);
        $sql -> bindParam(":idx",$reservationIdx);
        $sql -> bindParam(":c",$cancelStatus);
        $sql -> execute();
        
        if ($returnPoint > 0) {
            $query = ' UPDATE tbl_usr_list SET usrpoint = usrpoint+:p WHERE usrserial=:us ';
            $sql = $pdo -> prepare($query);
            $sql -> bindParam(":p",$returnPoint);
            $sql -> bindParam(":us",$userSerial);
            $sql -> execute();
            
            $registerDate = date('Y-m-d H:i:s');
            $destroyDate = date('Y-m-d H:i:s',strtotime($registerDate.'+'.'1'.' years')); //1년 
            
            $query = ' INSERT INTO tbl_point_log(usrserial, valpoint, regdate, pointtype, destroydate, pointmsg, logmsg, f_reservationidx) VALUES (:us, :val, :log, "4", "0000-00-00 00:00:00", "포인트 복구", "포인트 복구", :idx) ';
            $sql = $pdo -> prepare($query);
            $sql -> bindParam(":us",$userSerial);
            $sql -> bindParam(":val",$returnPoint);
            $sql -> bindParam(":log",$registerDate);
            $sql -> bindParam(":idx",$reservationIdx);
            $sql -> execute();
        }
        $query = ' UPDATE new_rentSchedule SET state="11" WHERE serial=:s ';
        $sql = $pdo -> prepare($query);
        $sql -> bindParam(":s",$bookingId);
        $sql -> execute();
        
        $query = 'UPDATE new_coupon_list SET status="0" WHERE serial=:cs';
        $sql = $pdo->prepare($query);
        $sql->bindParam(':cs', $coupon_serial);
        $sql->execute();
    }
    
} else {
    //결제 실패
    $payResult= "0";
    
    $contents = trim(iconv("EUC-KR","UTF-8", $resultMsg));   // 결과메시지
    
    $query = ' SELECT * FROM tbl_reservation_list WHERE f_reservationidx=:m ';
    $sql = $pdo -> prepare($query);
    $sql -> bindParam(":m",$getMoid);
    $sql -> execute();
    $stmt = $sql -> fetch();

    
    $userSerial_e = $stmt['f_usrserial'];
    $now = date('Y-m-d H:i:s');
    
    $query = ' INSERT INTO new_pay_error_log (user_serial, location, result_code, msg, mid, tid, moid, amt, card_quota, card_code, card_name, register_date)
                VALUES (:user_serial, "1", :result_code, :msg, :mid, :tid, :moid, :amt, "", "", "", :resgiter) ';
    $sql = $pdo -> prepare($query);
    $sql -> bindParam(":user_serial",$userSerial_e);
    $sql -> bindParam(":result_code",$resultCode);
    $sql -> bindParam(":msg",$contents);
    $sql -> bindParam(":mid",$mid);
    $sql -> bindParam(":tid",$tid);
    $sql -> bindParam(":moid",$getMoid);
    $sql -> bindParam(":amt",$v_CancelAmt);
    $sql -> bindParam(":resgiter",$now);
    $sql -> execute();
}

?>

<!DOCTYPE html>
<head>
</head>
<body>
</body>
<meta http-equiv='refresh' content='0;url=cancelResultView.php?ver=180807&&idx=<?=$getMoid?>&&result=<?=$payResult?>&&amt=<?=$v_CancelAmt?>'>
