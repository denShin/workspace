<?php
include ($_SERVER["DOCUMENT_ROOT"]."/static/lib/php-kt-ucloudstorage/lib/cloudfiles-kt.php");

$auth = new CF_Authentication("mrclick@ppss.kr", "MTUwNjIxODI5NjE1MDYyMTYzNDA0MDUw");
if($auth->authenticate() != "True"){
    echo "False";
}
$path = '/v1/account/ppssfree';
$redirect = 'http://ppss.fishcall.co.kr/static/lib/plupload/examples/jquery/ktucloudupload.php'; # set to '' if redirect not in form
$max_file_size = 104857600104;

$max_file_count = 10;
$expires = strtotime("+1 hour");

$key = $auth->auth_token;
$hmac_body="$path\n$redirect\n$max_file_size\n$max_file_count\n$expires";
$signature = hash_hmac('sha1', $hmac_body, $key);


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

    <title>Plupload to KT Ucloud Example</title>


    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="../../js/jquery.ui.plupload/css/jquery.ui.plupload.css" type="text/css" />

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>


    <!-- production -->
    <script type="text/javascript" src="../../js/plupload.full.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.ui.plupload/jquery.ui.plupload.js"></script>

    <!-- debug
    <script type="text/javascript" src="../../js/plupload.dev.js"></script>
    <script type="text/javascript" src="../../js/jquery.ui.plupload/jquery.ui.plupload.js"></script>
    -->

</head>
<body style="font: 13px Verdana; background: #eee; color: #333">
http://ppss.fishcall.co.kr/static/lib/plupload/examples/jquery/ktucloudupload.php
<h1>Plupload to  KT Ucloud Example</h1>

<div id="uploader">
    <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
</div>

<script type="text/javascript">
    // Convert divs to queue widgets when the DOM is ready
    $(function() {
        $("#uploader").plupload({
            headers: {
                'X-Account-Meta-Temp-URL-Key': '<?=$key ?>'
            },
            runtimes : 'html5,flash,silverlight',
            url : 'http://ssproxy.ucloudbiz.olleh.com/v1/AUTH_a4740f81-3b76-4331-af0c-70439dbd8b61/ppssfree',
            multipart: true,
            multipart_params: {
                'expires': '<?=$expires?>',
                'max_file_count' : '<?=$max_file_count?>',
                'signature' : '<?=$signature?>'

            },

            // !!!Important!!!
            // this is not recommended with S3, since it will force Flash runtime into the mode, with no progress indication
            //resize : {width : 800, height : 600, quality : 60},  // Resize images on clientside, if possible

            // optional, but better be specified directly
            file_data_name: 'file',

            filters : {
                // Maximum file size
                max_file_size : '300mb',
                // Specify what files to browse for
                mime_types: [
                    {title : "Image files", extensions : "jpg,jpeg,png"},
                    {title : "Movie files", extensions : "mp4"}
                ]
            },

            // Flash settings
            flash_swf_url : '../../js/Moxie.swf',

            // Silverlight settings
            silverlight_xap_url : '../../js/Moxie.xap'
        });
    });
</script>

</body>
</html>