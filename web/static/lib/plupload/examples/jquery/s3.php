<?php

$access_key         = "AKIAIEA5WIYFXDNBAAXQ"; //Access Key
$secret_key         = "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"; //Secret Key
$my_bucket          = "ppssmovie"; //bucket name
$region             = "ap-northeast-2"; //bucket region
$allowd_file_size   = "314572800"; //300 MB allowed Size

//dates
$short_date         = gmdate('Ymd'); //short date
$iso_date           = gmdate("Ymd\THis\Z"); //iso format date
$expiration_date    = gmdate('Y-m-d\TG:i:s\Z', strtotime('+1 hours')); //policy expiration 1 hour from now

//POST Policy required in order to control what is allowed in the request
//For more info http://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-HTTPPOSTConstructPolicy.html
$policy = utf8_encode(json_encode(array(
    'expiration' => $expiration_date,
    'conditions' => array(
        array('acl' => 'public-read'),
        array('bucket' => $my_bucket),
        array('starts-with', '$key', ''),
        array('starts-with', '$name', ''),
        array('content-length-range', '1', $allowd_file_size),
        array('x-amz-credential' => $access_key.'/'.$short_date.'/'.$region.'/s3/aws4_request'),
        array('x-amz-algorithm' => 'AWS4-HMAC-SHA256'),
        array('X-amz-date' => $iso_date)
    ))));

//Signature calculation (AWS Signature Version 4)
//For more info http://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-authenticating-requests.html
$kDate = hash_hmac('sha256', $short_date, 'AWS4' . $secret_key, true);
$kRegion = hash_hmac('sha256', $region, $kDate, true);
$kService = hash_hmac('sha256', "s3", $kRegion, true);
$kSigning = hash_hmac('sha256', "aws4_request", $kService, true);
$signature = hash_hmac('sha256', base64_encode($policy), $kSigning);

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

    <title>Plupload to Amazon S3 Example</title>


    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="../../js/jquery.ui.plupload/css/jquery.ui.plupload.css" type="text/css" />

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>


    <!-- production -->
    <script type="text/javascript" src="../../js/plupload.full.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.ui.plupload/jquery.ui.plupload.js"></script>

    <!-- debug
    <script type="text/javascript" src="../../js/plupload.dev.js"></script>
    <script type="text/javascript" src="../../js/jquery.ui.plupload/jquery.ui.plupload.js"></script>
    -->

</head>
<body style="font: 13px Verdana; background: #eee; color: #333">
http://ppss.fishcall.co.kr/static/lib/plupload/examples/jquery/s3.php
<h1>Plupload to Amazon S3 Example</h1>

<div id="uploader">
    <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
</div>

<script type="text/javascript">
    // Convert divs to queue widgets when the DOM is ready
    $(function() {
        $("#uploader").plupload({
            runtimes : 'html5,flash,silverlight',

            url : 'https://<?= $my_bucket ?>.s3-<?= $region ?>.amazonaws.com/',

            multipart: true,
            multipart_params: {
                'key': '${filename}', // use filename as a key
                //'name2': '${filename}', // adding this to keep consistency across the runtimes
                'acl': 'public-read',
                'X-Amz-Credential' : '<?= $access_key; ?>/<?= $short_date; ?>/<?= $region; ?>/s3/aws4_request',
                'X-Amz-Algorithm' : 'AWS4-HMAC-SHA256',
                'X-Amz-Date' : '<?=$iso_date ; ?>',
                'policy' : '<?=base64_encode($policy); ?>',
                'X-Amz-Signature' : '<?=$signature ?>'
            },

            // !!!Important!!!
            // this is not recommended with S3, since it will force Flash runtime into the mode, with no progress indication
            //resize : {width : 800, height : 600, quality : 60},  // Resize images on clientside, if possible

            // optional, but better be specified directly
            file_data_name: 'file',

            filters : {
                // Maximum file size
                max_file_size : '300mb',
                // Specify what files to browse for
                mime_types: [
                    {title : "Image files", extensions : "jpg,jpeg,png"},
                    {title : "Movie files", extensions : "mp4"}
                ]
            },

            // Flash settings
            flash_swf_url : '../../js/Moxie.swf',

            // Silverlight settings
            silverlight_xap_url : '../../js/Moxie.xap'
        });
    });
</script>

</body>
</html>