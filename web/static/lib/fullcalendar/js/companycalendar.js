$(function(){

    var currentDate; // Holds the day clicked when adding a new event
    var currentEvent; // Holds the event object when editing an event

    $('#color').colorpicker(); // Colopicker


    // Fullcalendar
    $('#calendar').fullCalendar({
        header: {
            left: 'prev, next, today',
            center: 'title',
            right:'today prev,next'
        },
        // Get all events stored in database
        lang : "ko",
        selectable: false,
        selectHelper: true,

        allDayDefault: true,
        monthNames: ["1월","2월","3월","4월","5월","6월","7월","8월","9월","10월","11월","12월"],
        monthNamesShort: ["1월","2월","3월","4월","5월","6월","7월","8월","9월","10월","11월","12월"],
        dayNames: ["일요일","월요일","화요일","수요일","목요일","금요일","토요일"],
        dayNamesShort: ["일","월","화","수","목","금","토"],
        buttonText: {
            today: "오늘",
            month: "월별",
            week: "주별",
            day: "일별"
        },
        editable: false,
        navLinks: true, // can click day/week names to navigate views
        eventLimit: true, // allow "more" link when too many events
        events: {
            url: '/data/Jsoncompanycalendar',
            type: 'GET',
            data: function() { // a function that returns an object
                return {
                    dynamic_value: Math.random()
                };
            }
        },
        loading: function(bool) {
            $('#loading').toggle(bool);
        }
    });


});