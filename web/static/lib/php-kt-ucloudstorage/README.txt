< ucloud storage php sdk >
ucloud storage php sdk는 ucloud storage 서비스에 대한 php api를 제공한다.
본 sdk를 이용하여, ucloud storage의 다양한 응용프로그램을 개발할 수 있다.
ucloud storage php sdk는 openstack에서 제공하는 Swift php binding 을
ucloud storage에 적합하도록 수정/보완하였다.

< ucloudstorage php sdk 구성 >
ㅇ lib - ucloudstorage php library
ㅇ doc - ucloudstorage php document
ㅇ samples - php 샘플 파일
ㅇ share - ssl key 파일

< 개발 환경 >
ㅇ Windows7 64bit
ㅇ PHP 5.3

< 요구사항 >
ㅇ PHP version 5.x (developed against 5.2.0)
ㅇ PHP's cURL module
ㅇ PHP enabled with mbstring (multi-byte string) support
ㅇ PEAR FileInfo module (for Content-Type detection) -> 선택적