<?php
abstract class KeyUtils {

	
	private function KeyUtils(){
		
	}
	
	public static function genTID($mid,$svcCd,$svcPrdtCd){
		$buffer = array();
		
		$nanotime = microtime(true);
		
		
		$nanotimeLen = strlen($nanotime);
		$nanoString = str_replace(".","",$nanotime, $nanotimeLen);
		
		$nanoStrLength = strlen($nanoString);
		
		$yyyyMMddHHmmss = date("YmdHis");
		
		
		$appendNanoStr = substr($nanoString,$nanoStrLength-4,4);
		
		$buffer = array_merge($buffer,str_split($mid));
		$buffer = array_merge($buffer,str_split($svcCd));
		$buffer = array_merge($buffer,str_split($svcPrdtCd));
		$buffer = array_merge($buffer,str_split(substr($yyyyMMddHHmmss,2,strlen($yyyyMMddHHmmss))));
		$buffer = array_merge($buffer,str_split($appendNanoStr));
		
		
		return implode($buffer);
	}
}
?>
