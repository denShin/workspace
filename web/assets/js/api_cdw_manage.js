const M_ACM_CDW_NORMAL = "1";
const M_ACM_CDW_ADVANCED = "2";
const M_ACM_CDW_SUPER = "3";
const M_ACM_GRIM = "1";
const M_ACM_INS = "2";
const M_ACM_REBORN = "3";
const M_ACM_CDW_INVENTORY = "cdwInven";
const M_ACM_AJAX_SUCCESS = 1;
const M_ACM_MATCHING_COMPLETE_WORD = " (매칭완)";

let mAcmCdwInventory = [];
let mAcmCdwMapping = [];
let mAcmSelectBranchIdx = "";
let mAcmSelectApi = "";
let mAcmSelectApiIdx = "";
let mAcmSelectMappingIdx = "";
let mAcmSelectCdwButton = null;

$(document).on('click', '.acm-affiliate-button', function() {
    const thisButton = this;
    const api = thisButton.dataset.apiType;
    const waabIdx = thisButton.dataset.waabIdx;

    mAcmSelectBranchIdx = waabIdx;

    $.ajax({
        method: "GET",
        url: "/apiAffiliate/ApiCdwManage?crud=r&data=".concat(M_ACM_CDW_INVENTORY, "&api=", api, "&waabIdx=", btoa(waabIdx)),
        dataType: "json",
        /**
         * @param _res.result
         * @param _res.inventory
         * @param _res.inventory.L렌트회사코드
         * @param _res.inventory.R가입제한
         * @param _res.inventory.R계산법
         * @param _res.inventory.R렌트회사
         * @param _res.inventory.R면책금
         * @param _res.inventory.R보상한도
         * @param _res.inventory.R보험내용
         * @param _res.inventory.R보험명
         * @param _res.inventory.R보험번호
         * @param _res.inventory.R보험요금DATA
         * @param _res.inventory.R보험타입
         * @param _res.inventory.R적용모델
         * @param _res.inventory.R휴차보상
         * @param _res.mapping
         */
        success: function(_res) {
            if (_res.result === M_ACM_AJAX_SUCCESS) {
                mAcmCdwInventory = _res.inventory;
                mAcmCdwMapping = _res.mapping;

                console.log(mAcmCdwMapping);
                console.log(mAcmCdwInventory);

                console.log(_res.diff);

                acmShowEmptyCdwInformation(true);
                acmMarkClickedAffiliateButton(thisButton);
                acmSettingCdwInventory(api);

            }
        }
    });
});

$(document).on('click', '.acm-cdw-inventory-button', function() {
    const thisButton = this;
    const api = this.dataset.api;
    const idx = this.dataset.cdwIdx;
    const mappingIdx = this.dataset.mappingIdx;
    mAcmSelectCdwButton = thisButton;

    let obj = null;
    mAcmCdwInventory.some(function(item) {
        let compareIdx = acmGetCdwInventoryButtonIdx(api, item);
        if (compareIdx === idx) {
            obj = item;

            mAcmSelectApi = api;
            mAcmSelectApiIdx = idx;
            mAcmSelectMappingIdx = mappingIdx;

            return true;
        }
    });

    $("#acm_unmatching_button").hide();
    (obj !== null)? acmSettingCdwInformation(thisButton, obj) : acmShowEmptyCdwInformation(true);

});

function acmDoMatching() {
    const crudType = (mAcmSelectMappingIdx === "") ? "c" : "u";
    const method = (mAcmSelectMappingIdx === "") ? "POST" : "PATCH";
    const restObj = acmGetCdwSaveRestfulObj(crudType);

    $.ajax({
        method: method,
        data: restObj,
        dataType: 'json',
        url: "/apiAffiliate/ApiCdwManage?crud=".concat(crudType),
        /**
         *
         * @param _res
         * @param _res.result
         * @param _res.wcmIdx
         */
        success: function (_res) {
            if (_res.result === M_ACM_AJAX_SUCCESS) {
                const mappingIdx = _res.wcmIdx;

                const origMappingIdx = mAcmSelectCdwButton.dataset.mappingIdx;
                if (origMappingIdx === "") {
                    mAcmSelectCdwButton.dataset.mappingIdx = mappingIdx;
                    mAcmSelectCdwButton.classList.remove('btn-default');
                    mAcmSelectCdwButton.classList.add('btn-info');
                    mAcmSelectCdwButton.textContent = mAcmSelectCdwButton.textContent.concat(M_ACM_MATCHING_COMPLETE_WORD);

                }

                Swal.fire('', '매칭완료', 'success');
            }

        }
    });
}

$(document).on('click', '#acm_save_button', function() {
    acmDoMatching();
});

$(document).on('click', '#acm_unmatching_button', function() {
    if (mAcmSelectMappingIdx !== "") {
        $.ajax({
            method: "DELETE",
            data: {
                'waabIdx': mAcmSelectBranchIdx,
                'wcmIdx': mAcmSelectMappingIdx
            },
            dataType: 'json',
            url: "/apiAffiliate/ApiCdwManage?crud=d",
            success: function (_res) {
                if (_res.result === M_ACM_AJAX_SUCCESS) {
                    mAcmSelectCdwButton.dataset.mappingIdx = '';
                    mAcmSelectCdwButton.classList.add('btn-default');
                    mAcmSelectCdwButton.classList.remove('btn-info');
                    mAcmSelectCdwButton.textContent = mAcmSelectCdwButton.textContent.split(M_ACM_MATCHING_COMPLETE_WORD)[0];

                    Swal.fire('', '해제완료', 'success');
                }
            }
        });
    }
});

function acmGetCdwSaveRestfulObj(_crud) {
    return (_crud !== "c" && _crud !== "u")?
        {} :
        {
            "api": mAcmSelectApi,
            "apiIdx": mAcmSelectApiIdx,
            "cdwObj": {
                "branch": mAcmSelectBranchIdx,
                "category": document.getElementById('acm_insurance_category_select').value,
                "compensation": document.getElementById('acm_compensation_input').value,
                "deductible1": document.getElementById('acm_deductible_input1').value,
                "deductible2": document.getElementById('acm_deductible_input2').value,
                "age": document.getElementById('acm_age_select').value,
                "career": document.getElementById('acm_career_input').value,
                "description": document.getElementById('acm_description_textarea').value,
            }
        };
}

function acmMarkClickedAffiliateButton(_button) {
    $('.acm-affiliate-button').removeClass('btn-primary');
    _button.classList.add('btn-primary');
}

function acmMarkClickedCdwButton(_button) {
    $('.acm-cdw-inventory-button').removeClass('btn-primary');
    _button.classList.add('btn-primary');

}

function acmSettingCdwInventory(_api) {
    if (mAcmCdwInventory.length !== 0) {
        acmShowEmptyCdwInventory(false);
        document.getElementById('acm_cdw_inventory_div').innerHTML = "";

        let fragment = document.createDocumentFragment();
        mAcmCdwInventory.forEach(function (item) {
            const button = acmCreateCdwInventoryButton(_api, item);

            fragment.appendChild(button);
        });

        document.getElementById('acm_cdw_inventory_div').appendChild(fragment);

    } else {

        acmShowEmptyCdwInventory(true);
    }
}

function acmSettingCdwInformation(_button, _obj) {
    acmInitializationCdwInformation();
    acmShowEmptyCdwInformation(false);
    acmMarkClickedCdwButton(_button);

    const objKeys = Object.keys(_obj);
    let fragment = document.createDocumentFragment();

    objKeys.forEach(function (_key) {
        const value = _obj[_key];

        const div = acmCreateCdwApiInformationFormGroup(_key, value);

        fragment.appendChild(div);

    });

    document.getElementById('acm_cdw_api_information_div').appendChild(fragment);

    if (_button.dataset.mappingIdx !== "")
        acmSettingMappingInformation(_button.dataset.cdwIdx);
}

function acmSettingMappingInformation(_cdwIdx) {
    const mappingObj = mAcmCdwMapping[_cdwIdx];

    document.getElementById('acm_insurance_category_select').value = mappingObj.category;
    document.getElementById('acm_compensation_input').value = mappingObj.compensation;
    document.getElementById('acm_deductible_input1').value = mappingObj.deductible1;
    document.getElementById('acm_deductible_input2').value = mappingObj.deductible2
    document.getElementById('acm_age_select').value = mappingObj.age;
    document.getElementById('acm_career_input').value = mappingObj.career;
    document.getElementById('acm_description_textarea').value = mappingObj.description;

    $("#acm_unmatching_button").show();
}

function acmInitializationCdwInformation() {
    document.getElementById('acm_cdw_api_information_div').innerHTML = "";

    document.getElementById('acm_insurance_category_select').value = "";
    document.getElementById('acm_compensation_input').value = "";
    document.getElementById('acm_deductible_input1').value = "";
    document.getElementById('acm_deductible_input2').value = "";
    document.getElementById('acm_age_select').value = "";
    document.getElementById('acm_career_input').value = "";
    document.getElementById('acm_description_textarea').value = "";
}

function acmCreateCdwApiInformationFormGroup(_key, _value) {

    let div = document.createElement('div');
    let label = document.createElement('label');
    let p = document.createElement('p');

    div.classList.add("form-group", "col-md-6");
    label.textContent = _key;
    p.textContent = _value;

    p.style.marginLeft = "1em";

    div.appendChild(label);
    div.appendChild(p);

    return div;
}

function acmCreateCdwInventoryButton(_api, _obj) {
    let button = document.createElement("button");
    const cdwIdx = acmGetCdwInventoryButtonIdx(_api, _obj);
    const mappingIdx = (cdwIdx in mAcmCdwMapping)? mAcmCdwMapping[cdwIdx]['wcmIdx'] : "";
    const btnClass = (mappingIdx === "")? "btn-default" : "btn-info";

    button.type = "button";
    button.classList.add("col-md-6", "btn", "btn-sm", btnClass, "acm-cdw-inventory-button");
    button.dataset.api = _api;
    button.dataset.cdwIdx = cdwIdx;
    button.dataset.mappingIdx = mappingIdx;
    button.textContent = acmGetCdwInventoryButtonText(_api, _obj, mappingIdx !== "");

    return button;
}

function acmGetCdwInventoryButtonText(_api, _obj, _isMapping) {
    switch (_api) {
        case M_ACM_GRIM:
            return (_isMapping)? _obj.name.concat(M_ACM_MATCHING_COMPLETE_WORD) : _obj.name;
        case M_ACM_INS:
            return (_isMapping)? _obj.R보험번호.concat(M_ACM_MATCHING_COMPLETE_WORD) : _obj.R보험번호;
        case M_ACM_REBORN:
            return "";
    }

}

function acmGetCdwInventoryButtonIdx(_api, _obj) {
    switch (_api) {
        case M_ACM_GRIM:
            return _obj.code;
        case M_ACM_INS:
            return _obj.R보험번호;
        case M_ACM_REBORN:
            return "";
    }
}

function acmShowEmptyCdwInventory(_bool) {
    if (_bool) {
        $("#acm_cdw_inventory_div").hide();
        $("#acm_cdw_empty_inventory_div").show();

    } else {
        $("#acm_cdw_inventory_div").show();
        $("#acm_cdw_empty_inventory_div").hide();

    }

}

function acmShowEmptyCdwInformation(_bool) {
    if (_bool) {
        $("#acm_cdw_information_div").hide();
        $("#acm_cdw_empty_information_div").show();
        $("#acm_carmore_cdw_div").hide();

    } else {
        $("#acm_cdw_information_div").show();
        $("#acm_cdw_empty_information_div").hide();
        $("#acm_carmore_cdw_div").show();

    }
}