Dropzone.autoDiscover = false;

var mAainfoObj = {};

var mAainfoIsChange = false;                 // 지점 정보 변경했는지
var mAainfoIsAffiNameChange = false;         // 회사명 변경했는지
var mAainfoIsBranchNameChange = false;       // 지점명 변경했는지
var mAainfoIsApiChange = false;              // API 업체 변경했는지
var mAainfoIsMainAddressChange = false;      // 메인주소 변경했는지
var mAainfoIsSubAddressChange = false;       // 상세주소내역 변경했는지
var mAainfoIsTelChange = false;              // 전화번호 변경했는지
var mAainfoIsMaxReservChange = false;        // 단기 최대판매일수 변경
var mAainfoIsOpenTimeChange = false;         // 영업시작시간 변경
var mAainfoIsCloseTimeChange = false;        // 영업종료시간 변경
var mAainfoIsSaleStartChange = false;        // 단기판매시작시간 변경
var mAainfoIsShuttleChange = false;          // 셔틀정보변경
var mAainfoIsIntroChange = false;            // 업체한마디변경
var mAainfoIsUniquenessChange = false;       // 업체 특이사항 변경
var mAainfoIsEnterDateChange = false;                // 입점일 변경
var mAainfoIsBiztalkMemberChange = false;    // 알림톡 멤버 변경
var mAainfoIsServiceDeliveryChange = false;  // 서비스딜리버리지역 변경
var mAainfoIsSpecialLocationChange = false;    // 주요지역 변경했는지
var mAainfoCompressBaseSize = 200000;    // 200k 미만의 파일들은 압축안함
var mAainfoCallback = null;

var mAainfoIsMainImgChange = false;
var mAainfoIsMainDelete = false;
var mAainfoMainUploadReady = true;
var mAainfoMainDropzone = new Dropzone("#aainfo_main_dropzone_div", {
    url: '/data/ImgUpload?type=u',
    addRemoveLinks : true,
    maxFilesize: 100,
    maxFiles:1,
    acceptedFiles: 'image/*',
    autoProcessQueue : false,
    paramName: "uploadImg",
    parallelUploads : 100,
    dictMaxFilesExceeded : "더이상 파일을 업로드 할 수 없습니다",
    init: function() {
        this.on("addedfile", function(_file) {
            var onThis = this;

            if (this.files.length > 1)
                this.removeFile(this.files[0]);

            if (mAainfoMainUploadReady) {
                mAainfoIsMainImgChange = true;
                mAainfoIsChange = true;

                if (_file.size > mAainfoCompressBaseSize) {
                    setTimeout(function() {
                        commonCompressImage(_file, 640, 200, function(_callbackFile) {
                            onThis.removeFile(onThis.files[0]);
                            mAainfoMainUploadReady = false;
                            onThis.addFile(_callbackFile);
                        })
                    }, 100);
                }
            } else
                mAainfoMainUploadReady = true;

        });

        this.on("thumbnail", function(file, dataUrl) {
            $('.dz-image').last().find('img').attr({width: '100%', height: 'auto'});

        });

        this.on("removedfile", function(_file) {
            if (mAainfoMainOrigSaveName !== "") {
                mAainfoIsMainDelete = true;
                mAainfoIsChange = true;

            }
        });

    },
    sending: function(file, xhr, formData) {
        formData.append('boardCode', 'partners');
        formData.append('fileUpType', 'api_logo');
        formData.append('contentCode', 'aplg_'.concat(mAainfoBranchIdx));

    },
    success: function(_file, _response) {
        var obj = JSON.parse(_response);

        if (obj.result === 1) {
            var info = obj.info;
            updateAainfoLogo(info.saveName);

        }

    },
});

var mAainfoIsSubImgChange = false;
var mAainfoIsSubDelete = false;
var mAainfoSubUploadReady = true;
var mAainfoSubDeleteArr = [];
var mAainfoSubDropzone = new Dropzone("#aainfo_sub_dropzone_div", {
    url: '/data/ImgUpload?type=u',
    addRemoveLinks : true,
    maxFilesize: 100,
    maxFiles:5,
    acceptedFiles: 'image/*',
    autoProcessQueue : false,
    paramName: "uploadImg",
    parallelUploads : 100,
    dictMaxFilesExceeded : "더이상 파일을 업로드 할 수 없습니다",
    init: function() {
        this.on("addedfile", function(_file) {
            var onThis = this;

            if (mAainfoSubUploadReady) {
                mAainfoIsSubImgChange = true;
                mAainfoIsChange = true;

                if (_file.size > mAainfoCompressBaseSize) {
                    setTimeout(function() {
                        commonCompressImage(_file, 640, 200, function(_callbackFile) {
                            onThis.removeFile(onThis.files[onThis.files.length-1]);
                            mAainfoSubUploadReady = false;
                            onThis.addFile(_callbackFile);
                        })
                    }, 100);
                }
            } else {
                mAainfoSubUploadReady = true;

            }

        });

        this.on("thumbnail", function(file, dataUrl) {
            $('.dz-image').last().find('img').attr({width: '100%', height: 'auto'});

        });

        this.on("removedfile", function(_file) {
            mAainfoIsSubDelete = true;
            mAainfoIsChange = true;
            mAainfoSubDeleteArr.push(_file.name);

        });

    },
    sending: function(file, xhr, formData) {
        formData.append('boardCode', 'partners');
        formData.append('fileUpType', 'api_'.concat(mAainfoBranchIdx));
        formData.append('contentCode', 'apsb_'.concat(mAainfoBranchIdx));

    },
    success: function(_file, _response) {
        var obj = JSON.parse(_response);

        if (obj.result === 1) {
            mAainfoCallback();

        }
    },
});

$(function() {
    $('#aainfo_search_address_modal_container').load(assetUrl.concat('modal/search_address_modal.html'), function() {
        $("#aainfo_modal050_container").load(assetUrl.concat('modal/matching_050_modal.html'), function() {
            $("#aainfo_sale_location_modal_container").load(assetUrl.concat('modal/sale_location_modal.html'), function() {
                $("#aainfo_special_location_modal_container").load(assetUrl.concat('modal/special_location_modal.html'), function() {
                    toastr.options = {
                        'closeButton': true,
                        'positionClass': 'toast-bottom-right',
                        'preventDuplicates': true
                    };

                    $('[data-toggle="tooltip"]').tooltip();

                    if (mAainfoBranchIdx !== -1) {
                        document.getElementById('aainfo_open_worktime_hour_select').value = mAainfoServerOpenHour;
                        document.getElementById('aainfo_open_worktime_minute_select').value = mAainfoServerOpenMinute;
                        document.getElementById('aainfo_close_worktime_hour_select').value = mAainfoServerCloseHour;
                        document.getElementById('aainfo_close_worktime_minute_select').value = mAainfoServerCloseMinute;

                        settingAainfoApiInfo();

                        if (mAainfoMainOrigSaveName !== "") {
                            mAainfoMainUploadReady = false;

                            var mockFile = {name: "orig_name.jpg", size: mAainfoMainOrigSize};

                            mAainfoMainDropzone.emit("addedfile", mockFile);
                            mAainfoMainDropzone.emit("thumbnail", mockFile, mAainfoMainOrigSaveName);
                            mAainfoMainDropzone.emit("complete", mockFile);
                            mAainfoMainDropzone.files.push(mockFile);

                        }

                        if (mAainfoSubOrigImgs.length !== 0) {

                            mAainfoSubOrigImgs.forEach(function (item) {
                                mAainfoSubUploadReady = false;

                                var mockFile = {name: item.name, size: item.size};

                                mAainfoSubDropzone.emit("addedfile", mockFile);
                                mAainfoSubDropzone.emit("thumbnail", mockFile, item.url);
                                mAainfoSubDropzone.emit("complete", mockFile);
                                mAainfoSubDropzone.files.push(mockFile);
                            })

                        }

                        for (var i = 0, bizLen = mAainfoBiztalkInventory.length; i < bizLen; i++) {
                            var bizObj = mAainfoBiztalkInventory[i];

                            appendAainfoBiztalkMemberRow(1, bizObj);

                        }

                        $('#aainfo_delete_btn').show();
                    } else
                        $('#aainfo_delete_btn').hide();
                });

            });

        });

    });
});

/**
 * 지점명 변경
 */
$(document).on('keyup', '.aainfo-branch-name-input', function() {
    if (!mAainfoIsBranchNameChange) {
        mAainfoIsChange = true;
        mAainfoIsBranchNameChange = true;

    }

    mAainfoObj.branchName = this.value;
});

/**
 * 지점 선택
 */
$(document).on('click', '.aainfo-branch-select-btn', function() {
    if (!this.classList.contains('btn-default')) {
        var branchIdx = this.closest('.aainfo-branch-name-input-container').dataset.branchIdx;
        if (mAainfoIsChange) {
            Swal.fire({
                title: '변경 사항이 있습니다',
                text: '저장하지 않을 시 기재된 데이터를 잃습니다.\r\n그래도 진행하시겠습니까?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: "네, 진행합니다",
                cancelButtonText: "아니오"
            }).then((result) => {
                if (result.value)
                    location.href = '/apiAffiliate/ApiAffiliateInformation?type=1&waai='.concat(btoa(mAainfoIdx), '&waabi=', btoa(branchIdx));

            });
        } else
            location.href = '/apiAffiliate/ApiAffiliateInformation?type=1&waai='.concat(btoa(mAainfoIdx), '&waabi=', btoa(branchIdx));
    }


});

/**
 * API 라디오버튼 클릭 시
 */
$(document).on('change', '.aainfo-api-radio', function() {
    if (!mAainfoIsApiChange) {
        mAainfoIsChange = true;
        mAainfoIsApiChange = true;

    }

    var apiValue = Number(this.value);

    mAainfoApiInfo = {
        'api': this.value,
        'info1': "",
        'info2': ""
    };

    if (apiValue === 1)
        mAainfoApiInfo.info3 = "";

    mAainfoObj.apiInfo = mAainfoApiInfo;

    settingAainfoApiInfo();

});

/**
 * API 정보 변경시
 */
$(document).on('change', '.aainfo-api-info-input1, .aainfo-api-info-input2, .aainfo-api-info-input3, .aainfo-api-info-input4', function() {
    if (!mAainfoIsApiChange) {
        mAainfoIsChange = true;
        mAainfoIsApiChange = true;

    }

    var infoNum = this.dataset.apiInfoNum;

    if (mAainfoObj.hasOwnProperty('apiInfo')) {
        mAainfoApiInfo['info'.concat(infoNum)] = this.value;

    } else {
        mAainfoApiInfo = {};
        mAainfoApiInfo['info'.concat(infoNum)] = this.value;

        mAainfoObj.apiInfo = mAainfoApiInfo;

    }

});

/**
 * 알림톡 명단 수정
 */
$(document).on('keyup', '.aainfo-biztalk-member-name-input, .aainfo-biztalk-member-tel-input', function() {
    var row = this.closest('tr');

    var memberIdx = row.dataset.memberIdx;
    var tel = remainOnlyNumber(row.getElementsByClassName('aainfo-biztalk-member-tel-input')[0].value);

    row.getElementsByClassName('aainfo-biztalk-member-tel-input')[0].value = telWithHyphen(tel);

    if (memberIdx !== "-1") {
        if (!mAainfoIsBiztalkMemberChange) {
            mAainfoIsChange = true;
            mAainfoIsBiztalkMemberChange = true;
        }

        var name = row.getElementsByClassName('aainfo-biztalk-member-name-input')[0].value;

        if (mAainfoObj.hasOwnProperty('biztalk')) {
            for (var i = 0, len = mAainfoObj.biztalk.length; i < len; i++) {
                if (mAainfoObj.biztalk[i].memberIdx === memberIdx) {
                    mAainfoObj.biztalk[i].name = name;
                    mAainfoObj.biztalk[i].tel = tel;
                } else {
                    mAainfoObj.biztalk.push({
                        'memberIdx': memberIdx,
                        'name': name,
                        'tel': tel
                    });
                }
            }

        } else {
            mAainfoObj.biztalk = [];
            mAainfoObj.biztalk.push({
                'memberIdx': memberIdx,
                'name': name,
                'tel': tel
            });
        }

    }

});

/**
 * 업체명 변경
 */
$(document).on('keyup', '#aainfo_name_input', function() {
    if (!mAainfoIsAffiNameChange) {
        mAainfoIsChange = true;
        mAainfoIsAffiNameChange = true;

    }

    mAainfoObj.affiName = this.value;
});

/**
 * 상세주소내역 쳤는지 [true : 상세주소내역 변경했으니 저장할때 address 에 붙여서 작업]
 */
$(document).on('keyup', '#aainfo_sub_address_input', function() {
    if (!mAainfoIsSubAddressChange) {
        mAainfoIsChange = true;
        mAainfoIsSubAddressChange = true;

    }

    if (!mAainfoObj.hasOwnProperty('addressInfo'))
        mAainfoObj.addressInfo = {};

    mAainfoObj.addressInfo.subAddress = this.value.trim();
});

/**
 * 전화변호 변경했을때
 */
$(document).on('keyup', '#aainfo_tel_input', function() {
    if (!mAainfoIsSubAddressChange) {
        mAainfoIsChange = true;
        mAainfoIsTelChange = true;

    }

    mAainfoObj.tel = remainOnlyNumber(this.value);
    if (this.value !== "")
        this.value = telWithHyphen(this.value);

});

/**
 * 최대 예약일 수 변경
 */
$(document).on('keyup', '#aainfo_max_reserv_input', function() {
    if (!mAainfoIsMaxReservChange) {
        mAainfoIsChange = true;
        mAainfoIsMaxReservChange = true;
    }

    mAainfoObj.maxReservDate = this.value;
});

/**
 * 영업시작시간 변경
 */
$(document).on('change', '#aainfo_open_worktime_hour_select, #aainfo_open_worktime_minute_select', function() {
    if (!mAainfoIsOpenTimeChange) {
        mAainfoIsChange = true;
        mAainfoIsOpenTimeChange = true;

    }

    var openHour = document.getElementById('aainfo_open_worktime_hour_select').value;
    var openMinute = document.getElementById('aainfo_open_worktime_minute_select').value;

    if (!mAainfoObj.hasOwnProperty('workTime'))
        mAainfoObj.workTime = {};

    if (openHour === '' || openMinute === '')
        mAainfoObj.workTime.openTime = "";
    else
        mAainfoObj.workTime.openTime = openHour.concat(':', openMinute);

});

/**
 * 영업종료시간 변경
 */
$(document).on('change', '#aainfo_close_worktime_hour_select, #aainfo_close_worktime_minute_select', function() {
    if (!mAainfoIsCloseTimeChange) {
        mAainfoIsChange = true;
        mAainfoIsCloseTimeChange = true;

    }

    var closeHour = document.getElementById('aainfo_close_worktime_hour_select').value;
    var closeMinute = document.getElementById('aainfo_close_worktime_minute_select').value;

    if (!mAainfoObj.hasOwnProperty('workTime'))
        mAainfoObj.workTime = {};

    if (closeHour === '' || closeMinute === '')
        mAainfoObj.workTime.closeTime = "";
    else
        mAainfoObj.workTime.closeTime = closeHour.concat(':', closeMinute);

});

/**
 * 단기 판매 시작시간 변경
 */
$(document).on('change', '#aainfo_sale_start_input', function() {
    if (!mAainfoIsSaleStartChange) {
        mAainfoIsChange = true;
        mAainfoIsSaleStartChange = true;

    }

    mAainfoObj.saleStart = this.value
});

/**
 * 셔틀정보 변경
 */
$(document).on('change',
    '#aainfo_shuttle_name_input, #aainfo_shuttle_area_input, #aainfo_shuttle_area_num_input, #aainfo_shuttle_race_input, #aainfo_shuttle_spend_input',
    function() {
        if (!mAainfoIsShuttleChange) {
            mAainfoIsChange = true;
            mAainfoIsShuttleChange = true;

        }

        if (!mAainfoObj.hasOwnProperty('shuttleInfo'))
            mAainfoObj.shuttleInfo = {};

        switch (this.id) {
            case "aainfo_shuttle_name_input":
                mAainfoObj.shuttleInfo.stName = this.value;
                break;
            case "aainfo_shuttle_area_input":
                mAainfoObj.shuttleInfo.stArea = this.value;
                break;
            case "aainfo_shuttle_area_num_input":
                mAainfoObj.shuttleInfo.stAreaNumber = this.value;
                break;
            case "aainfo_shuttle_race_input":
                mAainfoObj.shuttleInfo.stRace = this.value;
                break;
            case "aainfo_shuttle_spend_input":
                mAainfoObj.shuttleInfo.stSpendTime = this.value;
                break;
        }
    });

/**
 * 업체한마디 변경
 */
$(document).on('change', '#aainfo_affi_intro_textarea', function() {
    if (!mAainfoIsIntroChange) {
        mAainfoIsChange = true;
        mAainfoIsIntroChange = true;

    }

    mAainfoObj.intro = this.value;
});

/**
 * 업체특이사항 변경
 */
$(document).on('change', '#aainfo_affi_uniqueness_textarea', function() {
    if (!mAainfoIsUniquenessChange) {
        mAainfoIsChange = true;
        mAainfoIsUniquenessChange = true;

    }

    mAainfoObj.uniqueness = this.value;
});

/**
 * 업체입점일변경
 */
$(document).on('change', '#aainfo_enter_date_input', function() {
    if (!mAainfoIsEnterDateChange) {
        mAainfoIsChange = true;
        mAainfoIsEnterDateChange = true;

    }

    mAainfoObj.enterDate = this.value;
});

/**
 * 서비스지역/딜리버리 설정 버튼 클릭
 */
$(document).on('click', '#aainfo_sale_location_btn, #aainfo_sale_delivery_btn', function() {
    var type = this.id === "aainfo_sale_location_btn"? 1 : 2;    // 1: LOC, 2: DELIV

    var param = {
        'type': type,
        'areaString': mAainfoServiceString,
        'deliveryString': mAainfoDeliveryString
    };

    callSaleLocationModal(param, function(_zipServiceString, _zipDeliveryString) {
        mAainfoIsServiceDeliveryChange = true;
        mAainfoIsChange = true;

        mAainfoServiceString = _zipServiceString;
        mAainfoDeliveryString = _zipDeliveryString;

        mAainfoObj.serviceString = mAainfoServiceString;
        mAainfoObj.deliveryString = mAainfoDeliveryString;
    });
});

$(document).on('click', '#aainfo_special_location_btn', function() {
    callSpecialLocationModal(mAainfoSpecialLocationString, function(_returnString) {
        mAainfoIsSpecialLocationChange = true;
        mAainfoIsChange = true;

        mAainfoSpecialLocationString = _returnString;

        mAainfoObj.specialLocation = mAainfoSpecialLocationString;

    });
});

/**
 * 초기화
 */
function initAainfo() {
    mAainfoBranchIdx = -1;
    mAainfoObj = {};

    mAainfoIsChange = false;
    mAainfoIsAffiNameChange = false;
    mAainfoIsBranchNameChange = false;
    mAainfoIsApiChange = false;
    mAainfoIsMainAddressChange = false;
    mAainfoIsSubAddressChange = false;
    mAainfoIsTelChange = false;
    mAainfoIsMaxReservChange = false;
    mAainfoIsOpenTimeChange = false;
    mAainfoIsCloseTimeChange = false;
    mAainfoIsSaleStartChange = false;
    mAainfoIsShuttleChange = false;
    mAainfoIsIntroChange = false;
    mAainfoIsUniquenessChange = false;
    mAainfoIsBiztalkMemberChange = false;
    mAainfoIsEnterDateChange = false;

    var containers = document.getElementsByClassName('aainfo-branch-name-input-container');

    for (var i = 0, len = containers.length; i < len; i++) {
        if (containers[i].dataset.branchIdx === "-1") {
            $(containers[i]).remove();
            break;
        }
    }

    $('.aainfo-branch-select-btn').removeClass("btn-default");
    $('.aainfo-branch-select-btn').addClass("btn-primary");
    $('.aainfo-branch-select-btn').text("선택");

    $('input:radio[name=aainfo_api_radio]:checked').attr('checked', false);
    $(".aainfo-grim-div").hide();
    $(".aainfo-reborn-div").hide();
    $(".aainfo-ins-div").hide();
    document.getElementById('aainfo_main_address_input').value = '';
    document.getElementById('aainfo_sub_address_input').value = '';
    document.getElementById('aainfo_sub_address_input').readOnly = true;
    document.getElementById('aainfo_tel_input').value = '';
    document.getElementById('aainfo_max_reserv_input').value = '';
    document.getElementById('aainfo_open_worktime_hour_select').value = '';
    document.getElementById('aainfo_open_worktime_minute_select').value = '';
    document.getElementById('aainfo_close_worktime_hour_select').value = '';
    document.getElementById('aainfo_close_worktime_minute_select').value = '';
    document.getElementById('aainfo_sale_start_input').value = '';
    document.getElementById('aainfo_shuttle_name_input').value = '';
    document.getElementById('aainfo_shuttle_area_input').value = '';
    document.getElementById('aainfo_shuttle_area_num_input').value = '';
    document.getElementById('aainfo_shuttle_race_input').value = '';
    document.getElementById('aainfo_shuttle_spend_input').value = '';

    document.getElementById('aainfo_biztalk_member_tbody').innerHTML = '';

    $('#aainfo_biztalk_member_add_btn_container').hide();
    $('#aainfo_biztalk_member_none_container').show();

    $('#aainfo_delete_btn').hide();

}

/**
 * API 정보 세팅
 */
function settingAainfoApiInfo() {
    var api = Number(mAainfoApiInfo.api);

    $(".aainfo-api-info-div").hide();

    if (api === 1) {
        // 그림
        var section = $(".aainfo-grim-div");

        section.show();

        var partner = mAainfoApiInfo.info1;
        var pmId = mAainfoApiInfo.info2;
        var pmPw = mAainfoApiInfo.info3;
        var domain = mAainfoApiInfo.info4;

        section.find(".aainfo-api-info-input1").val(partner);
        section.find(".aainfo-api-info-input2").val(pmId);
        section.find(".aainfo-api-info-input3").val(pmPw);
        section.find(".aainfo-api-info-input4").val(domain);

    } else if (api === 2) {
        //인스
        var section = $(".aainfo-ins-div");
        section.show();

        var rcname = mAainfoApiInfo.info1;
        var rccode = mAainfoApiInfo.info2;

        section.find(".aainfo-api-info-input1").val(rcname);
        section.find(".aainfo-api-info-input2").val(rccode);

    } else if (api === 3) {
        // 리본
        var section = $(".aainfo-reborn-div");
        section.show();

        var apiKey = mAainfoApiInfo.info1;
        var certiKey = mAainfoApiInfo.info2;

        section.find(".aainfo-api-info-input1").val(apiKey);
        section.find(".aainfo-api-info-input2").val(certiKey);

    }

}

/**
 * 지점 추가 버튼 클릭 시
 *
 * @author Den
 */
function aainfoBranchAddBtn() {
    if (mAainfoIsChange) {
        Swal.fire({
            type: 'question',
            title: '변경사항이 있습니다',
            text: '저장하지 않을 시 변경사항이 사라지게 됩니다\r\n' +
                '그래도 진행하시겠습니까?',
            showCancelButton: true,
            confirmButtonText: '네, 진행하겠습니다',
            cancelButtonText: '아니오'
        }).then((_result) => {
            if (_result.value)
                procAainfoBranchAdd();
        });

    } else
        procAainfoBranchAdd();

}

/**
 * 지점 추가 프로세스
 */
function procAainfoBranchAdd() {
    initAainfo();

    var div = document.createElement('div');

    div.classList.add('input-group');
    div.classList.add('aainfo-branch-name-input-container');

    div.dataset.branchIdx = "-1";

    div.innerHTML =
        "<input type='text' class='form-control aainfo-branch-name-input' />" +
        "<span class='input-group-btn'>" +
        "<button class='btn btn-default aainfo-branch-select-btn' type='button'>선택 됨</button>" +
        "</span>";

    document.getElementById('aainfo_branch_wrapper').appendChild(div);

}

/**
 * 주소 검색버튼 클릭 시
 */
function clickSearchAddressBtn() {
    var mainAddress = document.getElementById('aainfo_main_address_input').value;

    callSearchAddressModal(mainAddress, function(_retObj) {
        if (!mAainfoIsMainAddressChange) {
            mAainfoIsChange = true;
            mAainfoIsMainAddressChange = true;
        }

        var address = _retObj.address;

        mAainfoObj.addressInfo = {
            'mainAddress': address,
            'subAddress': "",
            'latitude': _retObj.latitude,
            'longitude': _retObj.longitude
        };

        document.getElementById('aainfo_main_address_input').value = address;
        document.getElementById('aainfo_sub_address_input').readOnly = false;
    });
}

/**
 * 비즈톡 알림멤버 추가 버튼 클릭시
 *
 * @author Den
 * @param _type INT         -1:신규, 0:추가버튼, 1:기존데이터
 * @param _obj  Object|null _type:1일 경우 데이터 그 외에는 null
 */
function appendAainfoBiztalkMemberRow(_type, _obj) {
    if (_type !== 1)
        mAainfoIsChange = true;

    if (_type === -1) {
        $("#aainfo_biztalk_member_none_container").hide();
        $("#aainfo_biztalk_member_add_btn_container").show();

    }

    var name = (_type === 1)? _obj.name : "";
    var tel  = (_type === 1)? telWithHyphen(_obj.tel) : "";

    var row = document.createElement('tr');
    var html = '<td><input type="text" class="form-control aainfo-biztalk-member-name-input" value="'.concat(name, '" placeholder="김아무개" /></td>');
    html = html.concat('<td><input type="tel" class="form-control aainfo-biztalk-member-tel-input" value="', tel, '" placeholder="010-1234-5678" /></td>');
    html = html.concat('<td class="text-center"><button type="button" class="btn btn-danger" onclick="deleteAainfoBiztalkMemberRow(this)">삭제</button></td>');

    row.classList.add('aainfo-biztalk-member-row');
    if (_type !== 1)
        row.classList.add('aainfo-biztalk-member-new-row');
    row.dataset.memberIdx = (_type === 1)? String(_obj.idx) : "-1";

    row.innerHTML = html;

    document.getElementById('aainfo_biztalk_member_tbody').appendChild(row);

}

/**
 * 비즈톡 알림멤버 삭제 버튼 클릭 시
 *
 * @param _btnElement element 클릭된 버튼
 */
function deleteAainfoBiztalkMemberRow(_btnElement) {
    mAainfoIsChange = true;

    var row = _btnElement.closest('tr');

    var memberIdx = row.dataset.memberIdx;

    if (memberIdx === "-1")
        row.remove();
    else {
        if (!mAainfoObj.hasOwnProperty('biztalk'))
            mAainfoObj.biztalk = [];

        mAainfoObj.biztalk.push({
            'memberIdx': memberIdx,
            'isDelete': true
        });

        row.remove();
    }

    var childrenLength = document.getElementById('aainfo_biztalk_member_tbody').children.length;

    if (childrenLength === 0) {
        $("#aainfo_biztalk_member_none_container").show();
        $("#aainfo_biztalk_member_add_btn_container").hide();

    }
}

/**
 * "목록"버튼 클릭
 */
function procAainfoIntoInventory() {
    location.href='/apiAffiliate/ApiAffiliateInventory';

}

/**
 * 알림톡 멤버 가져오기
 *
 * @param _type int 목록 가져올 타입 (0: 신규, 1: 전체)
 */
function getAainfoBiztalkMember(_type) {
    var inventory  = [];

    var rows = (_type === 0)? document.getElementsByClassName('aainfo-biztalk-member-new-row') : document.getElementsByClassName('aainfo-biztalk-member-row');

    for (var i = 0, len = rows.length; i < len; i++) {
        var row = rows[i];

        var tel = remainOnlyNumber(row.getElementsByClassName('aainfo-biztalk-member-tel-input')[0].value);

        if (tel === "" || tel === "0")
            continue;

        inventory.push({
            'isNew': true,
            'name': row.getElementsByClassName('aainfo-biztalk-member-name-input')[0].value,
            'tel': tel
        });
    }

    return inventory;
}

/**
 * "저장"버튼 클릭
 */
function procAainfoSave() {
    var saveData = getAainfoSaveData();

    if (saveData.result === 1) {
        var saveDataObj = saveData.saveData;

        var form = document.createElement('form');

        form.appendChild(getFormHiddenInput("affiIdx", mAainfoIdx));
        form.appendChild(getFormHiddenInput("affiBranchIdx", mAainfoBranchIdx));
        if (mAainfoIsMainDelete)
            form.appendChild(getFormHiddenInput("isLogoDelete", 1));

        if (mAainfoIsSubDelete) {
            mAainfoSubDeleteArr.forEach(function (_item, _index) {
                form.appendChild(getFormHiddenInput("subDelete[".concat(String(_index), "]"), _item));

            })
        }

        var saveDataObjKeys = Object.keys(saveDataObj);
        for (var i = 0, len = saveDataObjKeys.length; i < len; i++) {
            var key = saveDataObjKeys[i];

            var param = saveDataObj[key];
            if (typeof param === "object" || $.isArray(param)) {
                if ($.isArray(param)) {
                    for (var j = 0, arrLen = param.length; j < arrLen; j++) {
                        var arrayObj = param[j];
                        var arrayObjKeys = Object.keys(arrayObj);

                        for (var k = 0, arrayObjLen = arrayObjKeys.length; k < arrayObjLen; k++) {
                            var arrayObjKey = arrayObjKeys[k];

                            var name = key.concat('[', j, '][', arrayObjKey, ']');
                            var value = arrayObj[arrayObjKey];

                            form.appendChild(getFormHiddenInput(name, value));

                        }

                    }

                } else {
                    var paramKeys = Object.keys(param);
                    for (var j = 0, paramLen = paramKeys.length; j < paramLen; j++) {
                        var paramKey = paramKeys[j];

                        var name = key.concat('[', paramKey, ']');
                        var value = param[paramKey];

                        form.appendChild(getFormHiddenInput(name, value));
                    }

                }
            } else
                form.appendChild(getFormHiddenInput(key, param));

        }

        document.body.appendChild(form);

        $.ajax({
            type: 'POST',
            url: '/apiAffiliate/ApiAffiliateRegister',
            data: $(form).serialize(),
            dataType: 'json',
            error: function(xhr, status, error) {
                console.log(error);

            },
            success: function(_json) {
                if (_json.result === 1) {
                    var swalTitle = mAainfoBranchIdx === -1 ? "등록 완료" : "수정 완료";

                    swal.fire({
                        title: swalTitle,
                        text: "저장이 완료되었습니다.",
                        type: "success"
                    }).then((result) => {
                        if (result.value) {
                            var regInfo = _json.regInfo;

                            aainfoCallLocationCashingApi();

                            if (mAainfoBranchIdx === -1) {
                                if (mAainfoIsMainImgChange) {
                                    uploadAainfoLogoImg(regInfo.affiIdx, regInfo.affiBranchIdx, function() {
                                        if (mAainfoIsSubImgChange)
                                            uploadAainfoSubImg(regInfo.affiIdx, regInfo.affiBranchIdx, function() {
                                                location.href = '/apiAffiliate/ApiAffiliateInformation?type=1&waai='.concat(regInfo.waai, '&waabi=', regInfo.waabi);

                                            });
                                        else
                                            location.href = '/apiAffiliate/ApiAffiliateInformation?type=1&waai='.concat(regInfo.waai, '&waabi=', regInfo.waabi);

                                    });

                                } else {
                                    if (mAainfoIsSubImgChange)
                                        uploadAainfoSubImg(regInfo.affiIdx, regInfo.affiBranchIdx, function() {
                                            location.href = '/apiAffiliate/ApiAffiliateInformation?type=1&waai='.concat(regInfo.waai, '&waabi=', regInfo.waabi);

                                        });
                                    else
                                        location.href = '/apiAffiliate/ApiAffiliateInformation?type=1&waai='.concat(regInfo.waai, '&waabi=', regInfo.waabi);
                                }
                            } else {
                                if (mAainfoIsMainImgChange) {
                                    uploadAainfoLogoImg(regInfo.affiIdx, regInfo.affiBranchIdx, function() {
                                        if (mAainfoIsSubImgChange)
                                            uploadAainfoSubImg(regInfo.affiIdx, regInfo.affiBranchIdx, function() {
                                                location.reload();

                                            });
                                        else
                                            location.reload();

                                    });

                                } else {
                                    if (mAainfoIsSubImgChange)
                                        uploadAainfoSubImg(regInfo.affiIdx, regInfo.affiBranchIdx, function() {
                                            location.reload();

                                        });
                                    else
                                        location.reload();
                                }

                            }
                        }
                    });
                }
            }
        });

    }
}

function aainfoCallLocationCashingApi() {
    $.ajax({
        method: 'POST',
        url: "https://carmore.kr/home/php/api/v1/servicesLocation.php?key=C3AR24M1O34RE1",
        success: function(_res) { }
    });

}

/**
 * 저장할 데이터들 정제해서 가져오는 함수
 *
 * @return object
 */
function getAainfoSaveData() {
    var newBiztalkMember = getAainfoBiztalkMember(0);

    if (!mAainfoIsChange) {
        toastr.error('변경사항이 없습니다');

        return {'result' : 0};

    } else if (mAainfoBranchIdx === -1 && newBiztalkMember.length === 0) {
        toastr.error('알림톡 명단이 비어있습니다');

        return {'result': 0};
    } else if (mAainfoObj.hasOwnProperty('apiInfo')) {
        if (mAainfoObj.apiInfo.hasOwnProperty('info1')) {
            if (mAainfoObj.apiInfo.info1 === "") {
                toastr.error('모든 정보는 필수 입니다');

                return {'result': 0};
            }

        }

        if (mAainfoObj.apiInfo.hasOwnProperty('info2')) {
            if (mAainfoObj.apiInfo.info2 === "") {
                toastr.error('모든 정보는 필수 입니다');

                return {'result': 0};
            }

        }

        if (mAainfoObj.apiInfo.hasOwnProperty('info3')) {
            if (mAainfoObj.apiInfo.info3 === "") {
                toastr.error('모든 정보는 필수 입니다');

                return {'result': 0};
            }

        }
    }

    if (newBiztalkMember.length !== 0) {
        if (!mAainfoObj.hasOwnProperty('biztalk'))
            mAainfoObj.biztalk = [];

        for (var i = 0, len = newBiztalkMember.length; i < len; i++)
            mAainfoObj.biztalk.push(newBiztalkMember[i]);
    }

    // 업체명 체크
    if (document.getElementById('aainfo_name_input').value === "") {
        toastr.error("업체명을 입력해주세요");
        $("#aainfo_name_input").focus();

        return {'result': 0};

    }

    // API 체크
    if ($('input[name="aainfo_api_radio"]:checked').val() === undefined) {
        toastr.error("업체 API 를 선택해주세요");

        return {'result': 0};

    }

    // 주소 체크
    if (document.getElementById('aainfo_main_address_input').value === "") {
        toastr.error("주소를 검색해주세요");

        return {'result': 0};

    }

    // 연락처 체크
    if (document.getElementById('aainfo_tel_input').value === "") {
        toastr.error("업체 연락처를 입력해주세요");
        $("#aainfo_tel_input").focus();

        return {'result': 0};

    }

    // 최대 예약일 수 체크
    if (document.getElementById('aainfo_max_reserv_input').value === "") {
        toastr.error("최대 예약일을 입력해주세요");
        $("#aainfo_max_reserv_input").focus();

        return {'result': 0};

    }

    // 카모아 영업시간 체크
    if (document.getElementById('aainfo_open_worktime_hour_select').value === "" || document.getElementById('aainfo_open_worktime_minute_select').value === "") {
        toastr.error("카모아 영업시작시간을 입력해주세요");

        return {'result': 0};

    } else if (document.getElementById('aainfo_close_worktime_hour_select').value === "" || document.getElementById('aainfo_close_worktime_minute_select').value === "") {
        toastr.error("카모아 영업종료시간을 입력해주세요");

        return {'result': 0};

    }

    // 단기판매시작시간 체크
    if (document.getElementById('aainfo_sale_start_input').value === "") {
        toastr.error("단기판매시작시간을 입력해주세요");
        $("#aainfo_sale_start_input").focus();

        return {'result': 0};

    }

    // 셔틀명 체크
    if (document.getElementById('aainfo_shuttle_name_input').value === "") {
        toastr.error("셔틀명을 입력해주세요");
        $("#aainfo_shuttle_name_input").focus();

        return {'result': 0};

    }

    // 셔틀구역번호 체크
    if (document.getElementById('aainfo_shuttle_area_input').value === "") {
        toastr.error("셔틀구역을 입력해주세요");
        $("#aainfo_shuttle_area_input").focus();

        return {'result': 0};

    }

    // 셔틀구역번호 체크
    if (document.getElementById('aainfo_shuttle_area_num_input').value === "") {
        toastr.error("셔틀구역번호를 입력해주세요");
        $("#aainfo_shuttle_area_num_input").focus();

        return {'result': 0};

    }

    // 셔틀운행간격 체크
    if (document.getElementById('aainfo_shuttle_race_input').value === "") {
        toastr.error("셔틀운행간격을 입력해주세요");
        $("#aainfo_shuttle_race_input").focus();

        return {'result': 0};

    }

    // 셔틀소요시간 체크
    if (document.getElementById('aainfo_shuttle_spend_input').value === "") {
        toastr.error("셔틀소요시간을 입력해주세요");
        $("#aainfo_shuttle_spend_input").focus();

        return {'result': 0};

    }

    // 알림톡명단 체크
    if (getAainfoBiztalkMember(1).length === 0) {
        toastr.error('알림톡 명단이 없습니다');

        return {'result': 0};
    }

    return {
        'result': 1,
        'saveData': mAainfoObj
    };
}

/**
 * "삭제"버튼 클릭
 */
function procAainfoDelete() {
    Swal.fire({
        type: 'warning',
        title: '지점 삭제',
        text: '정말 지점을 삭제하시겠습니까?',
        showCancelButton: true,
        confirmButtonText: '네, 삭제하겠습니다',
        cancelButtonText: '아니오'
    }).then((_result) => {
        if (_result.value) {
            var containers = $('.aainfo-branch-name-input-container');

            $.ajax({
                type: 'GET',
                url: '/apiAffiliate/ApiAffiliateInformation?type=0&waai='.concat(btoa(mAainfoIdx), '&waabi=', btoa(mAainfoBranchIdx)),
                dataType: 'json',
                error: function(xhr, status, error) {
                    console.log(error);

                },
                success: function(_json) {
                    if (_json.result === 1) {

                        swal.fire({
                            title: "삭제완료",
                            text: "삭제가 완료되었습니다",
                            type: "success"
                        }).then((result) => {
                            if (result.value) {
                                if (containers.length === 1)
                                    location.href='/apiAffiliate/ApiAffiliateInventory';
                                else {
                                    var divs = $('.aainfo-branch-select-btn').filter('.btn-primary').parents('.aainfo-branch-name-input-container');
                                    var branchIdx = divs.eq(0).data('branchIdx');

                                    location.href = '/apiAffiliate/ApiAffiliateInformation?type=1&waai='.concat(btoa(mAainfoIdx), '&waabi=', btoa(branchIdx));
                                }
                            }
                        });
                    }
                }
            });

        }


    });
}

/**
 * 050 번호 매칭
 */
function click050MatchingBtn(_this) {
    if (mAainfoBranchIdx !== -1) {

        var tel = remainOnlyNumber(document.getElementById('aainfo_tel_input').value);

        if (tel === "" || tel === "0" || tel === 0)
            swal.fire('', '전화번호를 입력해주세요', 'error');
        else {
            var thisBtn = _this;
            var isMatching = Number(thisBtn.dataset.isMatching);    // 0:매칭 안됨, 1: 매칭 됨, 2: 방금 매칭 함/ 매칭 해제 함

            if (isMatching === 0) {
                var obj = {
                    'type': 1,
                    'title': '(7777 ~ 9999)',
                    'affiType': 2,
                    'affiIdx': mAainfoBranchIdx,
                    'branchTel': tel,
                    'part': 'api',
                    'recommendNum': mAainfoRecommendNum

                };

                callMatch050Modal(obj, function() {
                    closeMatch050Modal();
                    thisBtn.classList.remove('btn-default');
                    thisBtn.classList.add('btn-success');
                    thisBtn.dataset.isMatching = "2";
                    thisBtn.textContent = "050번호매칭 완료";

                });
            } else if (isMatching === 1) {
                var obj = {
                    'type': 2,
                    'affiType': 2,
                    'affiIdx': mAainfoBranchIdx,
                    'delTel': thisBtn.dataset.matchingTel,
                    'branchTel': tel,
                    'part': 'api'
                };;

                callMatch050Modal(obj, function() {
                    closeMatch050Modal();
                    thisBtn.classList.remove('btn-info');
                    thisBtn.classList.add('btn-danger');
                    thisBtn.dataset.isMatching = "2";
                    thisBtn.textContent = "050번호매칭 해제완료";

                });
            } else if (isMatching === 2) {
                // 방금 등록했다(매칭했다)

                Swal.fire('', '이미 매칭작업을 완료했습니다', 'error');

            }

        }

    } else {
        swal.fire('', '신규업체는 050 번호를 매칭할 수 없습니다', 'error');

    }

}

/**
 * Dropzone 로고 업로드 진행
 *
 * @param _aaInfoIdx       int
 * @param _aaInfoBranchIdx int
 * @param _callback        function|null
 */
function uploadAainfoLogoImg(_aaInfoIdx, _aaInfoBranchIdx, _callback) {
    mAainfoIdx = _aaInfoIdx;
    mAainfoBranchIdx = _aaInfoBranchIdx;
    mAainfoCallback = _callback;

    mAainfoMainDropzone.processQueue();

}

/**
 * Dropzone 서브 업로드 진행
 *
 * @param _aaInfoIdx       int
 * @param _aaInfoBranchIdx int
 * @param _callback        function|null
 */
function uploadAainfoSubImg(_aaInfoIdx, _aaInfoBranchIdx, _callback) {
    mAainfoIdx = _aaInfoIdx;
    mAainfoBranchIdx = _aaInfoBranchIdx;
    mAainfoCallback = _callback;

    mAainfoSubDropzone.processQueue();
}

/**
 * 업체 로고 업로드
 *
 * @param _saveName
 */
function updateAainfoLogo(_saveName) {

    $.ajax({
        type: 'POST',
        data: {
            'affiIdx': mAainfoIdx,
            'affiBranchIdx': mAainfoBranchIdx,
            'logo': _saveName
        },
        url: '/apiAffiliate/ApiAffiliateRegister',
        dataType: 'JSON',
        success: function(_res) {
            if (_res.result === 1) {
                mAainfoCallback();

            }

        }
    });

}