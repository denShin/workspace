$(function() {
    toastr.options = {
        'closeButton': true,
        'positionClass': 'toast-bottom-right',
        'preventDuplicates': true
    };

});

$(document).on('click', '#rit_biztalk_send_history_btn', function() {

    window.open("/carmore/alarm/biztalk/BiztalkInventory?moid=".concat(this.dataset.moid, "&recNum=", this.dataset.recNum))


});

$(document).on('click', '#rit_inventory_btn', function() {
    history.back();

});

$(document).on('click', '#rit_cancel_btn', function() {
    Swal.fire({
        title: '취소확인',
        text: '렌트카 업체의 ERP 설정에 따라\r\n취소가 되지 않을 수 있습니다.\r\n아래 확인 버튼을 누르시면 취소가 진행됩니다.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        confirmButtonText: "예약 취소 진행",
        cancelButtonText: "아니오"
    }).then((_result) => {
        if (_result) {

            $.ajax({
                data: {},
                url: "/apiAffiliate/reserv/ReservItem?crud=".concat(btoa("d"), "&api=", btoa(mRitApiCode), "&waabIdx=", btoa(mRitWaabIdx), "&apiReservCode=", btoa(mRitApiReservCode)),
                type: "GET",
                dataType: "json",
                success: function(_res) {
                    var result = _res.result;

                    if (result === 1) {
                        Swal.fire({
                            title: '취소완료',
                            text: '취소가 완료되었습니다.',
                            type: 'success',
                            confirmButtonText: "확인",
                        }).then((_result) => {
                            if (_result.value)
                                location.reload();

                        });
                    } else if (result === 2) {
                        var errorInfo = _res.errInfo;

                        Swal.fire({
                            title: '취소실패',
                            html: '취소에 실패하였습니다. ('.concat(errorInfo.api, ")<br>", "에러코드 : ", errorInfo.code, "<br>", errorInfo.msg),
                            type: 'error',
                            confirmButtonText: "확인",
                        }).then((_result) => {
                            if (_result.value)
                                location.reload();

                        });
                    } else
                        Swal.fire("취소 실패", "취소에 실패하였습니다", "error");

                }
            });



        }

    });

});