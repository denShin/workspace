Dropzone.autoDiscover = false;

var mStinfoObj                  = {};
var mStinfoMatchingInven        = [];
var mStinfoLogoChange           = false;
var mStinfoLogoDelete           = false;
var mStinfoUploadReady          = true;
var mStinfoCallback             = null;
var mStinfoLogoCompressBaseSize = 200000;    // 200k 미만의 파일들은 압축안함
var mStinfoDropzone             = new Dropzone("#stinfo_dropzone_div", {
    url: '/data/ImgUpload?type=u',
    addRemoveLinks : true,
    maxFilesize: 100,
    maxFiles:1,
    acceptedFiles: 'image/*',
    autoProcessQueue : false,
    paramName: "uploadImg",
    parallelUploads : 100,
    dictMaxFilesExceeded : "더이상 파일을 업로드 할 수 없습니다",
    init: function() {
        this.on("addedfile", function(_file) {
            var onThis = this;

            if (this.files.length > 1)
                this.removeFile(this.files[0]);

            if (mStinfoUploadReady) {
                mStinfoLogoChange = true;

                if (_file.size > mStinfoLogoCompressBaseSize) {
                    setTimeout(function() {
                        commonCompressImage(_file, 640, 200, function(_callbackFile) {
                            onThis.removeFile(onThis.files[0]);
                            mStinfoUploadReady = false;
                            onThis.addFile(_callbackFile);
                        })
                    }, 100);
                }
            } else
                mStinfoUploadReady = true;

        });

        this.on("removedfile", function(_file) {
            if (mStinfoOrigSaveName !== "") {
                mStinfoLogoDelete = true;
            }
        });

    },
    sending: function(file, xhr, formData) {
        formData.append('boardCode', 'stroller');
        formData.append('fileUpType', 'stroller_logo');
        formData.append('contentCode', 'stlg_'.concat(mStinfoIdx));

    },
    success: function(_file, _response) {
        var obj = JSON.parse(_response);

        if (obj.result === 1) {
            var info = obj.info;
            updateStinfoLogo(info.saveName);

        }

    },
});

$(function() {
    $("#stinfo_search_affiliate_modal_container").load(mStinfoAssetUrl.concat('modal/search_affiliate_modal.html?ver=190529'), function() {
        toastr.options = {
            'closeButton': true,
            'positionClass': 'toast-bottom-right',
            'preventDuplicates': true
        };
        $('[data-toggle="tooltip"]').tooltip();

        if (mStinfoOrigSaveName !== "") {
            mStinfoUploadReady = false;

            var mockFile = {name: "orig_name.jpg", size: mStinfoOrigSaveSize};

            mStinfoDropzone.emit("addedfile", mockFile);
            mStinfoDropzone.emit("thumbnail", mockFile, mStinfoOrigSaveName);
            mStinfoDropzone.emit("complete", mockFile);
            mStinfoDropzone.files.push(mockFile);

        }

        if (Object.keys(mStinfoMatchingServerInven).length !== 0) {
            mStinfoMatchingServerInven.forEach(function(_obj) {
                var param = {
                    'type': _obj.type,
                    'branchIdx': _obj.idx,
                    'companyName': _obj.name,
                    'branchName': "",
                };

                appendStinfoMatchingRow(param, false);
            });
        }

    });
});

/**
 * 변화감지
 */
$(document).on('change', '.st-change-detect-element', function() {
    var key = this.dataset.objKey;

    if (key === "tel") {
        this.value = telWithHyphen(this.value);

        mStinfoObj.tel = remainOnlyNumber(this.value);

    } else if (key === "memo")
        mStinfoObj[key] = this.value.replace(/(?:\r\n|\r|\n)/g, '<br/>');
    else
        mStinfoObj[key] = this.value;

});

/**
 * 매칭해제버튼
 */
$(document).on('click', '.stinfo-affiliate-matching-cancel-btn', function() {
    var row = this.closest('.stinfo-affiliate-matching-row');

    var idx = Number(row.dataset.branchIdx);
    var type = Number(row.dataset.affiType);

    if (!mStinfoObj.hasOwnProperty("matchingAffiliate")) {
        mStinfoObj.matchingAffiliate = {};
        mStinfoObj.matchingAffiliate.newInven = [];
        mStinfoObj.matchingAffiliate.delInven = [{
            'type': type,
            'idx': idx
        }];

    } else {
        var newInvenIndex = -1;
        mStinfoObj.matchingAffiliate.newInven.some(function(_obj, _index) {
            if (_obj.type === type && Number(_obj.idx) === idx) {
                newInvenIndex = _index;
                return true;
            }

        });

        if (newInvenIndex !== -1) {
            var totalInvenIndex = -1;
            mStinfoMatchingInven.some(function(_obj, _index) {
                if (_obj.type === type && Number(_obj.idx) === idx) {
                    totalInvenIndex = _index;
                    return true;
                }

            });

            mStinfoObj.matchingAffiliate.newInven.splice(newInvenIndex, 1);
            mStinfoMatchingInven.splice(totalInvenIndex, 1);
        } else {
            mStinfoObj.matchingAffiliate.delInven.push({
                'type': type,
                'idx': idx
            });

        }

    }

    $(row).remove();
});

/**
 * 업체검색 모달 띄우기
 */
function callStinfoRegAffiliate() {
    callSearchAffiliateModal(function(_resAffiObj) {
        if (chkStinfoHasMatching(_resAffiObj))
            toastr.error("이미 매칭된 업체입니다");
        else {
            if (!mStinfoObj.hasOwnProperty("matchingAffiliate")) {
                mStinfoObj.matchingAffiliate = {};
                mStinfoObj.matchingAffiliate.newInven = [];
                mStinfoObj.matchingAffiliate.delInven = [];
            }

            appendStinfoMatchingRow(_resAffiObj, true);
        }
    });
}

/**
 * 매칭된 업체가 있는지 체크하는 함수
 */
function chkStinfoHasMatching(_affiObj) {
    var branchIdx = Number(_affiObj.branchIdx);
    var type = Number(_affiObj.type);
    var has = false;

    mStinfoMatchingInven.some(function(_obj) {
        if (_obj.type === type && Number(_obj.idx) === branchIdx) {
            has = true;
            return true;

        }

    });

    return has;
}

/**
 * 추가하려는 업체 행 추가
 */
function appendStinfoMatchingRow(_affiObj, _isItNew) {
    var branchIdx = _affiObj.branchIdx;
    var affiType = _affiObj.type;
    var affiliateName = _affiObj.companyName.concat(' ', _affiObj.branchName);

    $("#stinfo_affiliate_matching_div").show();
    $("#stinfo_affiliate_matching_empty_div").hide();

    var row = document.createElement('tr');

    row.classList.add('stinfo-affiliate-matching-row');
    row.dataset.branchIdx = branchIdx;
    row.dataset.affiType = affiType;

    var html = '<td>'.concat(affiliateName, '</td>');
    html = html.concat('<td><button type="button" class="btn btn-danger btn-xs stinfo-affiliate-matching-cancel-btn">매칭취소</button></td>');

    row.innerHTML = html;

    document.getElementById('stinfo_affiliate_matching_tbody').append(row);

    var param = {
        'type': affiType,
        'idx': branchIdx
    };

    if (_isItNew)
        mStinfoObj.matchingAffiliate.newInven.push(param);

    mStinfoMatchingInven.push(param);

}

/**
 * 저장하기 루틴
 */
function saveStinfo() {
    var chkResultInfo = checkStinfoSaveAvailable();

    if (chkResultInfo.result) {
        var crudType = chkResultInfo.type;

        $.ajax({
            type: 'POST',
            data: {
                'strollerInfo': mStinfoObj,
                'isDelete': (mStinfoLogoDelete)? 1 : 0
            },
            url: '/carmore/StrollerInformation?crud='.concat(crudType, '&csIdx=', btoa(mStinfoIdx)),
            dataType: 'JSON',
            success: function(_res) {
                if (_res.result === 1) {
                    if (crudType === "c") {
                        mStinfoIdx = _res.insertedIdx;
                        if (mStinfoLogoChange) {
                            uploadStinfoLogoImg(mStinfoIdx, function() {
                                createStinfoComplete();

                            });

                        } else
                            createStinfoComplete();

                    } else if (crudType === "u") {
                        if (mStinfoLogoChange) {
                            uploadStinfoLogoImg(mStinfoIdx, function() {
                                updateStinfoComplete();

                            });

                        } else
                            updateStinfoComplete();

                    }
                }

            }
        });

    } else {
        Swal.fire(chkResultInfo.swalTitle, chkResultInfo.swalText, 'error');
    }

}

/**
 * 로고 업데이트하기
 *
 * @param _logo
 */
function updateStinfoLogo(_logo) {
    var obj = {
        'logo': _logo
    };
    $.ajax({
        type: 'POST',
        data: {
            'strollerInfo': obj
        },
        url: '/carmore/StrollerInformation?crud=u&csIdx='.concat(btoa(mStinfoIdx)),
        dataType: 'JSON',
        success: function(_res) {
            if (_res.result === 1) {
                mStinfoCallback();

            }

        }
    });
}

/**
 * 저장을 할 수 있는지 없는지 판별
 *
 * @returns Object
 */
function checkStinfoSaveAvailable() {
    if (mStinfoIsItNew) {
        if (!mStinfoObj.hasOwnProperty("name") || !mStinfoObj.hasOwnProperty("type")) {
            return {
                'result': false,
                'swalTitle': "필수정보를 입력해주세요",
                'swalText': "업체명과 타입은 필수입니다."
            };

        } else
            return {
                'result': true,
                'type': 'c'
            };

    } else {
        if (Object.keys(mStinfoObj).length === 0 && !mStinfoLogoChange && !mStinfoLogoDelete) {
            return {
                'result': false,
                'swalTitle': "변경사항이 없습니다.",
                'swalText': "변경사항이 없습니다"
            };
        } else if (document.getElementById('st_name').value === "" || document.getElementById('st_type').value === "") {
            return {
                'result': false,
                'swalTitle': "필수정보를 입력해주세요",
                'swalText': "업체명과 타입은 필수입니다."
            };
        } else
            return {
                'result': true,
                'type': 'u'
            };

    }
}

/**
 * Dropzone 이미지 업로드 진행
 *
 * @param _csIdx    int
 * @param _callback function|null
 */
function uploadStinfoLogoImg(_csIdx, _callback) {
    mStinfoIdx = _csIdx;
    mStinfoCallback = _callback;

    mStinfoDropzone.processQueue();

}

/**
 * 수정완료 문구 / 액션
 */
function updateStinfoComplete() {
    Swal.fire({
        title: '저장완료',
        text: '유모차 업체정보 수정이 완료되었습니다',
        type: 'success',
        confirmButtonText: "확인",
    }).then((_result) => {
        if (_result)
            location.reload();

    });
}

/**
 * 신규등록 문구 / 액션
 */
function createStinfoComplete() {
    Swal.fire({
        title: '저장완료',
        text: '신규 유모차 등록이 완료되었습니다',
        type: 'success',
        confirmButtonText: "확인",
    }).then((_result) => {
        if (_result)
            location.href = '/carmore/StrollerInformation?crud=r&csIdx='.concat(btoa(mStinfoIdx));

    });
}

/**
 * 유모차 업체 삭제
 */
function deleteStinfo() {
    Swal.fire({
        title: '유모차 업체 삭제',
        text: '해당 유모차 업체를 삭제하시겠습니까?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: "네, 진행합니다",
        cancelButtonText: "아니오"
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: 'POST',
                data: {},
                url: '/carmore/StrollerInformation?crud=d&csIdx='.concat(btoa(mStinfoIdx)),
                dataType: 'JSON',
                success: function(_res) {
                    if (_res.result === 1) {
                        Swal.fire({
                            title: '삭제완료',
                            text: '유모차 삭제가 완료되었습니다.',
                            type: 'success',
                            confirmButtonText: "확인",
                        }).then((_result) => {
                            if (_result)
                                location.href = '/carmore/StrollerInventory';

                        });
                    }

                }
            });

        }

    });

}