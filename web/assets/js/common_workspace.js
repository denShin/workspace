/**
 * 숫자만 남기기
 *
 * @param _val string
 */
function remainOnlyNumber(_val) {
    if (_val === undefined || _val === null)
        return 0;

    if (_val.length === 0)
        return 0;

    var strVal = (typeof _val === "string")? _val : _val.toString();

    return strVal.replace(/[^0-9]/g, "");
}

/**
 * 하이픈 넣기
 *
 * @param _str string
 * @returns string
 */
function telWithHyphen(_str) {
    _str = remainOnlyNumber(_str);

    if (_str === "")
        return "";

    var formatNum = '';

    if (_str.length === 11) {
        formatNum = _str.replace(/(\d{3})(\d{4})(\d{4})/, '$1-$2-$3');
    } else if (_str.length === 10) {
        if (_str.indexOf('02') === 0) {
            formatNum = _str.replace(/(\d{2})(\d{4})(\d{4})/, '$1-$2-$3');
        } else {
            formatNum = _str.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
        }
    } else if (_str.length === 8) {
        formatNum = _str.replace(/(\d{4})(\d{4})/, '$1-$2');
    } else {
        if (_str.indexOf('02') === 0) {
            formatNum = _str.replace(/(\d{2})(\d{4})(\d{4})/, '$1-$2-$3');
        } else {
            formatNum = _str.replace(/(\d{3})(\d{4})(\d{4})/, '$1-$2-$3');
        }
    }
    return formatNum;
}

/**
 * 히든 객체 가져오기
 *
 * @param _name
 * @param _val
 *
 * @returns {HTMLElement}
 */
function getFormHiddenInput(_name, _val) {
    var input = document.createElement('input');
    input.type = "hidden";
    input.name = _name;
    input.value = _val;

    return input;
}

/**
 * br 태그 \r\n 으로
 *
 * @param _str
 *
 * @returns string
 */
function replaceBrToRn(_str) {
    return _str.replace(/<br>/gi, "\r\n");
}

/**
 * br 태그 \r\n 으로
 *
 * @param _str
 *
 * @returns string
 */
function replaceRnToBr(_str) {
    return _str.replace(/(?:\r\n|\r|\n)/g, '<br>');
}