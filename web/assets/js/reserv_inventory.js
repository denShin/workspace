$(document).on('click', '.ri-data-row-item-btn', function(e) {
    var row = this.closest('.ri-data-row');

    var apiReservCode = row.dataset.apiReservCode;

    if (apiReservCode !== "") {
        var api = row.dataset.api;
        var waabIdx = row.dataset.waabIdx;
        var wariIdx = row.dataset.wariIdx;

        Swal.fire({
            title: '예약정보열람',
            text: '해당예약정보를 최신화 및 열람하시겠습니까?',
            type: 'question',
            showCancelButton: true,
            confirmButtonText: "네, 진행합니다",
            cancelButtonText: "아니오"
        }).then((result) => {
            if (result.value)
                location.href = "/apiAffiliate/reserv/ReservItem?crud=".concat(btoa("r"), "&api=", btoa(api), "&apiReservCode=", btoa(apiReservCode), '&waabIdx=', btoa(waabIdx), '&wariIdx=', btoa(wariIdx));

        });

    }
});

$(document).on('keyup', '#ri_search_input', function(e) {
    if (e.keyCode === 13)
        searchRi();

});

function searchRi() {
    var searchParam = Number(document.getElementById('ri_search_param_select').value);
    var searchValue = (searchParam === 0)? "" : document.getElementById('ri_search_input').value;
    var stateu1 = $("#ri_stateu1").is(":checked")? "1" : "0";
    var state0 = $("#ri_state0").is(":checked")? "1" : "0";
    var state1 = $("#ri_state1").is(":checked")? "1" : "0";
    var state10 = $("#ri_state10").is(":checked")? "1" : "0";
    var state11 = $("#ri_state11").is(":checked")? "1" : "0";

    location.href = '/apiAffiliate/reserv/reservInventory?per_page=1&sp='.concat(String(searchParam), '&sv=', searchValue, "&stateu1=", stateu1, "&state0=", state0, "&state1=", state1, "&state10=", state10, "&state11=", state11);

}