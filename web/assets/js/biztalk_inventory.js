$(function() {
    toastr.options = {
        'closeButton': true,
        'positionClass': 'toast-bottom-right',
        'preventDuplicates': true
    };

    $("#bi_biztalk_modal_container").load(mBiAssetUrl.concat('modal/biztalk_retransmit_modal.html?ver=190531'), function() {

    });
});

$(document).on('click', '.bi-modify-content-btn', function() {
    var rows = $(this).parents('tr');
    var contents = rows.find('.bi-biztalk-msg').text();
    var type = this.dataset.modifyType;
    var code = this.dataset.templateCode;
    var mtPr = this.closest('tr').dataset.mtPr;
    var reqTime = (type === "1")? this.dataset.reqTime : "";
    var recNum = this.dataset.recNum;

    $.ajax({
        type: 'GET',
        url: "/carmore/alarm/biztalk/BiztalkContent?type=gtc&code=".concat(code),
        data: {},
        dataType: 'json',
        success: function(_res) {

            if (_res.result === 1) {
                var template = _res.template;
                if (template === "")
                    toastr.error("해당 코드는 템플릿 입력이 안 되어 있습니다. 개발팀에 문의주시기 바랍니다");
                else {

                    var modalData = {
                        "type": type,
                        "template": template,
                        "contents": contents,
                        "code": code,
                        "mtPr": mtPr,
                        "reqTime": reqTime,
                        "recNum": recNum
                    };

                    initBizretransModal(modalData, function() {
                        Swal.fire({
                            title: '적용완료',
                            text: "확인버튼을 누르면 새로고침 됩니다",
                            type: 'success',
                            confirmButtonText: "확인",
                        }).then((result) => {
                            if (result.value)
                                location.reload();

                        });
                    });

                }

            } else {
                toastr.error("코드를 가져오는데 오류가 발생했습니다");
            }

        }
    });

});

$(document).on('click', '#bi_search_btn', function() {
    searchBi();
});

$(document).on('keyup', '#bi_reserv_input, #bi_rec_num_input', function(e) {
    if (e.keyCode === 13)
        $("#bi_search_btn").click();
});

function searchBi() {
    var moid = document.getElementById('bi_reserv_input').value;
    var recNum = remainOnlyNumber(document.getElementById('bi_rec_num_input').value);

    location.href = '/carmore/alarm/biztalk/BiztalkInventory?per_page=1&moid='.concat(moid, '&recNum=', recNum);
}

function reqBiCancel(_mtPr) {
    Swal.fire({
        title: '알림톡 취소',
        text: '해당 알림톡 발송을 취소하시겠습니까?',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: "네, 취소합니다",
        cancelButtonText: "아니오"
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "POST",
                data: {
                    "mtPr": _mtPr
                },
                url: "/carmore/alarm/biztalk/BiztalkContent?type=c",
                dataType: 'json',
                success: function(_res) {
                    if (_res.result === 1) {
                        Swal.fire({
                            title: '발송취소완료',
                            text: "취소요청을 완료했습니다\r\n확인버튼을 누르면 새로고침 됩니다",
                            type: 'success',
                            confirmButtonText: "확인",
                        }).then((result) => {
                            if (result.value)
                                location.reload();

                        });

                    }
                }
            });
        }

    });
}

function reqBiRetransmission(_mtPr) {
    Swal.fire({
        title: '알림톡 재전송',
        text: '해당 알림톡을 재전송하시겠습니까?',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: "네, 재전송합니다",
        cancelButtonText: "아니오"
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "POST",
                data: {
                    "mtPr": _mtPr
                },
                url: "/carmore/alarm/biztalk/BiztalkContent?type=rt",
                dataType: 'json',
                success: function(_res) {
                    if (_res.result === 1) {
                        Swal.fire('재전송요청완료', "수 초 내에 새로고침을 하시면\r\n전송결과를 확인 할 수 있습니다.\r\n자동으로 새로고침 되지 않습니다", 'success');


                    }
                }
            });
        }

    });
}