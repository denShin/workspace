var mAcmObj = {};

$(function() {
    toastr.options = {
        'closeButton': true,
        'positionClass': 'toast-bottom-right',
        'preventDuplicates': true
    };

    if (mAcmInventory.length !== 0) {
        $('#acw_compensation_btn_container').show();
        $('#acw_compensation_empty_container').hide();
        $('#acw_compensation_info_container').show();
        $('#acw_compensation_info_empty_container').hide();

        for (var i = 0, len = mAcmInventory.length; i < len; i++) {
            addAcmCompensationBtn(1, mAcmInventory[i]);
        }

    } else {
        $('#acw_compensation_btn_container').hide();
        $('#acw_compensation_empty_container').show();
        $('#acw_compensation_info_container').hide();
        $('#acw_compensation_info_empty_container').show();
    }

    switch (mAcmCrudType) {
        case "c":
            addAcmCompensationBtn(0, null, function() {
                $('#acw_compensation_info_container').show();
                $('#acw_compensation_info_empty_container').hide();

                $("#acm_delete_btn").hide();

            });
            break;
        case "r":
            break;
    }

});

/**
 * 종합보험 리스트 클릭
 */
$(document).on('click', '.acm-compensation-btn', function() {
    var wacIdx = Number(this.dataset.wacIdx);

    if (wacIdx === 0 || wacIdx === mAcmSelectedIdx)
        return;
    else
        showAcmCompensationInfo(1, wacIdx)

});

/**
 * 카모아 노출 종류 변경 시
 */
$(document).on('change', '.acm-compensation-type-radio', function() {
    var value = Number(this.value);

    mAcmObj.compensationType = value;

    if (value === 1)
        activeAcmCompensationFee(true);
    else if (value === 2)
        activeAcmCompensationFee(false);

});

/**
 * 보상범위 변경
 */
$(document).on('change', '.acm-compensation-range-select', function() {
    var value = this.value;
    var key = this.dataset.key;
    if (value !== "")
        mAcmObj[key] = this.value;
    else {
        if (mAcmObj.hasOwnProperty(key))
            delete mAcmObj[key];

    }
});

/**
 * 면책금
 */
$(document).on('change', '.acm-compensation-fee-input', function() {
    var value = this.value;
    var key = this.dataset.key;
    if (value !== "")
        mAcmObj[key] = this.value;
    else {
        if (mAcmObj.hasOwnProperty(key))
            delete mAcmObj[key];

    }
});

/**
 * 종합보험 "추가하기" 버튼 클릭 시
 */
$(document).on('click', '#acw_add_btn', function() {
    if (Object.keys(mAcmObj).length === 0)
        showAcmCompensationInfo(0, null);
    else {
        Swal.fire({
            title: "변경사항",
            text: "변경사항을 저장하지 않을 시 정보를 잃습니다\r\n그래도 진행하시겠습니까?",
            type: "question",
            showCancelButton: true,
            confirmButtonText: '네, 진행하겠습니다',
            cancelButtonText: '아니오'
        }).then((result) => {
            if (result.value)
                showAcmCompensationInfo(0, null);

        });
    }

});

/**
 * 종합보험명 변경시
 */
$(document).on('change', '#acm_compensation_name', function() {
    var name = this.value;
    if (name !== "")
        mAcmObj.compensationName = name;
    else {
        if (mAcmObj.hasOwnProperty("compensationName"))
            delete mAcmObj.compensationName;

    }

});

/**
 * "삭제" 버튼 클릭 시
 */
$(document).on('click', '#acm_delete_btn', function() {
    Swal.fire({
        title: "삭제",
        text: "해당종합보험을 삭제하시겠습니까?",
        type: "question",
        showCancelButton: true,
        confirmButtonText: '네, 진행하겠습니다',
        cancelButtonText: '아니오'
    }).then((result) => {
        if (result.value)
            procAcmDelete(mAcmSelectedIdx);

    });
});

/**
 * "저장" 버튼 클릭 시
 */
$(document).on('click', '#acm_save_btn', function() {
    if (mAcmCrudType === "c") {
        if (!mAcmObj.hasOwnProperty("compensationType")) {
            toastr.error("모든 값은 필수 입니다");
            return;

        }

        var type = Number(mAcmObj.compensationType);

        if (type === 1 && !mAcmObj.hasOwnProperty("compensationName")) {
            toastr.error("모든 값은 필수 입니다");
            return;

        } else if (type === 2 &&
            (!mAcmObj.hasOwnProperty("compensationName") ||
                !mAcmObj.hasOwnProperty("compensation1") || !mAcmObj.hasOwnProperty("compensation2") || !mAcmObj.hasOwnProperty("compensation3") ||
                !mAcmObj.hasOwnProperty("compensation4") || !mAcmObj.hasOwnProperty("compensation5") || !mAcmObj.hasOwnProperty("compensation6") ||
                !mAcmObj.hasOwnProperty("fee1") || !mAcmObj.hasOwnProperty("fee2") || !mAcmObj.hasOwnProperty("fee3") ||
                !mAcmObj.hasOwnProperty("fee4") || !mAcmObj.hasOwnProperty("fee5") || !mAcmObj.hasOwnProperty("fee6")
            )) {
            toastr.error("모든 값은 필수 입니다");
            return;

        }

        Swal.fire({
            title: "신규 등록",
            text: "신규 종합보험을 등록하시겠습니까?",
            type: "question",
            showCancelButton: true,
            confirmButtonText: '네, 진행하겠습니다',
            cancelButtonText: '아니오'
        }).then((result) => {
            if (result.value)
                procAcmSave();

        });
    } else {
        var type = Number($(".acm-compensation-type-radio:checked").val());

        if (Object.keys(mAcmObj).length !== 0) {

            if (type === 1 && document.getElementById('acm_compensation_name').value === "") {
                toastr.error("모든 값은 필수 입니다");

            } else if (type === 2 && (
                document.getElementById('acm_compensation1_select').value === "" ||
                document.getElementById('acm_compensation2_select').value === "" ||
                document.getElementById('acm_compensation3_select').value === "" ||
                document.getElementById('acm_compensation4_select').value === "" ||
                document.getElementById('acm_compensation5_select').value === "" ||
                document.getElementById('acm_compensation6_select').value === "" ||
                document.getElementById('acm_fee1_input').value === "" ||
                document.getElementById('acm_fee2_input').value === "" ||
                document.getElementById('acm_fee3_input').value === "" ||
                document.getElementById('acm_fee4_input').value === "" ||
                document.getElementById('acm_fee5_input').value === "" ||
                document.getElementById('acm_fee6_input').value === "")) {
                toastr.error("모든 값은 필수 입니다");

            } else {
                Swal.fire({
                    title: "보험 수정",
                    text: "종합보험을 수정하시겠습니까?",
                    type: "question",
                    showCancelButton: true,
                    confirmButtonText: '네, 진행하겠습니다',
                    cancelButtonText: '아니오'
                }).then((result) => {
                    if (result.value)
                        procAcmSave();

                });
            }

        } else
            toastr.error("변경사항이 없습니다");
    }
});

/**
 * 보상범위, 면책금 활성화 여부
 *
 * @param _boolean Boolean
 */
function activeAcmCompensationFee(_boolean) {
    $(".acm-compensation-range-select").attr("disabled", _boolean);
    $(".acm-compensation-fee-input").attr("readonly", _boolean);

    if (_boolean)
        $(".acm-compensation-range-select").css('background', 'rgb(235, 235, 235)');
    else
        $(".acm-compensation-range-select").css('background', 'white');
}

/**
 * 종합보험 리스트 추가 함수
 *
 * @param _type     int    추가 타입 [0:신규추가, 1:데이터 로드]
 * @param _obj      object
 * @param _callback function
 */
function addAcmCompensationBtn(_type, _obj, _callback) {
    $("#acw_compensation_btn_container").show();
    $("#acw_compensation_empty_container").hide();

    var wacIdx = (_type === 0)? 0 : Number(_obj.idx);

    var btn = document.createElement('button');

    btn.type = "button";
    btn.classList.add('btn');
    if (mAcmSelectedIdx !== wacIdx)
        btn.classList.add('btn-default');
    else
        btn.classList.add('btn-success');

    btn.classList.add('col-md-12');
    btn.classList.add('acm-compensation-btn');
    btn.textContent = (_type === 0)? "종합보험" : _obj.name;

    btn.dataset.wacIdx = String(wacIdx);

    document.getElementById('acw_compensation_btn_container').appendChild(btn);

    if (typeof _callback === "function")
        _callback();
}

/**
 * 종합보험 정보 보기
 *
 * @param _type int 뷰타입 [0:신규, 1: 데이터]
 * @param _idx  int 종합보험 아이디
 */
function showAcmCompensationInfo(_type, _idx) {
    if (_type === 0)
        location.href = "/apiAffiliate/ApiCompensationManage?type=c";
    else if (_type === 1)
        location.href = "/apiAffiliate/ApiCompensationManage?type=r&wac=".concat(btoa(_idx));

}

/**
 * 저장 프로세스
 */
function procAcmSave() {
    var form = document.createElement('form');

    var saveDataObjKeys = Object.keys(mAcmObj);
    for (var i = 0, len = saveDataObjKeys.length; i < len; i++) {
        var key = saveDataObjKeys[i];

        var param = mAcmObj[key];
        form.appendChild(getFormHiddenInput(key, param));

    }
    var getType = mAcmCrudType === "r" ? "u" : mAcmCrudType;
    document.body.appendChild(form);

    var addedUrl = (mAcmCrudType === "r")? "&wac=".concat(btoa(mAcmSelectedIdx)) : "";

    $.ajax({
        type: 'POST',
        url: '/apiAffiliate/ApiCompensationManageRegister?type='.concat(getType, addedUrl),
        data: $(form).serialize(),
        dataType: 'json',
        error: function(xhr, status, error) {
            console.log(error);

        },
        success: function(_json) {
            if (_json.result === 1) {
                if (mAcmCrudType === "c") {
                    var regInfo = _json.regInfo;
                    var insertedIdx = regInfo.wacIdx;

                    Swal.fire({
                        title: "신규 등록",
                        text: "신규 저장이 완료되었습니다",
                        type: "success"
                    }).then((result) => {
                        if (result.value) {
                            location.href = '/apiAffiliate/ApiCompensationManage?type=r&wac='.concat(btoa(insertedIdx));

                        }
                    });

                } else if (mAcmCrudType === "r") {
                    Swal.fire({
                        title: "수정 완료",
                        text: "종합보험 수정이 완료되었습니다",
                        type: "success"
                    }).then((result) => {
                        if (result.value) {
                            location.reload();

                        }
                    });
                }

            }
        }
    });
}

/**
 * 삭제 프로세스
 *
 * @param _wac int 종합보험아이디
 */
function procAcmDelete(_wac) {
    $.ajax({
        type: "POST",
        url: "/apiAffiliate/ApiCompensationManageRegister?type=d&wac=".concat(btoa(_wac)),
        dataType: "json",
        error: function(xhr, status, error) {
            console.log(error);

        },
        success: function(_json) {
            if (_json.result === 1) {
                Swal.fire({
                    title: "삭제 완료",
                    text: "종합보험 삭제가 완료되었습니다",
                    type: "success"
                }).then((result) => {
                    if (result.value) {
                        location.href = '/apiAffiliate/ApiCompensationManage?type=r';

                    }
                });

            }
        }
    });

}