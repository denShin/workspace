var mAacChangeInventory = {};    // 변경내역

$(function() {
    toastr.options = {
        'closeButton': true,
        'positionClass': 'toast-bottom-right',
        'preventDuplicates': true
    };

    console.log(mAacSelectedIdx);

    if (mAacSelectedIdx !== 0)
        $("#aac_alert_div").hide();

    settingAacLoadInformation();
});

/**
 * 업체 변경시
 */
$(document).on('click', '.aac-affiliate-btn', function() {
    var waabIdx = Number(this.dataset.waabIdx);

    if (waabIdx !== mAacSelectedIdx) {
        if (Object.keys(mAacChangeInventory).length !== 0) {
            Swal.fire({
                title: '변경 사항이 있습니다',
                text: '저장하지 않을 시 기재된 데이터를 잃습니다.\r\n그래도 진행하시겠습니까?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: "네, 진행합니다",
                cancelButtonText: "아니오"
            }).then((result) => {
                if (result.value)
                    viewAacWaab(waabIdx);

            });
        } else
            viewAacWaab(waabIdx);


    }
});

/**
 * 매칭 차량등급 변경시
 */
$(document).on('change', '.aac-master-car-type-select', function() {
    var panelBody = $(this).parents('.aac-match-panel-body');
    var modelSelect = panelBody.find('.aac-master-car-model-select').eq(0);
    var carType = this.value;

    modelSelect.html("");

    if (carType !== "") {
        var typeInventory = mAacMasterCarInventory['type'+carType];

        appendAacMasterCarModel(modelSelect, 0, typeInventory, null);
    }
});

/**
 * 매칭 차종 변경시
 */
$(document).on('change', '.aac-master-car-model-select', function() {
    var inherenceIdx = this.closest('.aac-match-panel-group').dataset.inherenceIdx;

    if (!mAacChangeInventory.hasOwnProperty("inherence"+inherenceIdx))
        mAacChangeInventory["inherence"+inherenceIdx] = {};

    mAacChangeInventory["inherence"+inherenceIdx].model = this.value;
});

/**
 * 매칭 종합보험 변경시
 */
$(document).on('change', '.aac-compensation-select', function() {
    var inherenceIdx = this.closest('.aac-match-panel-group').dataset.inherenceIdx;

    if (!mAacChangeInventory.hasOwnProperty("inherence"+inherenceIdx))
        mAacChangeInventory["inherence"+inherenceIdx] = {};

    mAacChangeInventory["inherence"+inherenceIdx].compensation = this.value;
});

/**
 * 회사명/지점명 검색시
 */
$(document).on('keyup', '#aac_search_input', function() {
    var value = this.value.toLowerCase();
    var btns = document.getElementsByClassName('aac-affiliate-btn');
    var searchedIdxArr = [];

    if (value === "")
        $(btns).show();
    else {
        for (var i = 0, len = mAacAffiliateInventory.length; i < len; i++) {
            var obj = mAacAffiliateInventory[i];
            if ((obj.waaName.toLowerCase()).indexOf(value) !== -1 || (obj.waabName.toLowerCase()).indexOf(value) !== -1)
                searchedIdxArr.push(obj.waabIdx);

        }

        for (var i = 0, len = btns.length; i < len; i++) {
            if (searchedIdxArr.indexOf(btns[i].dataset.waabIdx) !== -1)
                $(btns).eq(i).show();
            else
                $(btns).eq(i).hide();

        }
    }

});

/**
 * "저장"버튼 클릭 시
 */
$(document).on('click', '#aac_save_btn', function() {
    if (Object.keys(mAacChangeInventory).length === 0) {
        toastr.error("변경사항이 없습니다");
        return;

    } else if (mAacSelectedIdx === "") {
        toastr.error("업체가 선택되지 않았습니다");
        return;

    } else {
        Swal.fire({
            title: '저장',
            text: '해당 내용으로 저장하시겠습니까?',
            type: 'question',
            showCancelButton: true,
            confirmButtonText: "네, 진행합니다",
            cancelButtonText: "아니오"
        }).then((result) => {
            if (result.value)
                saveAac();

        });
    }
});

/**
 * 다른 업체 보기
 */
function viewAacWaab(_idx) {
    location.href = "/apiAffiliate/ApiAffiliateCar?type=r&waab=".concat(btoa(_idx));
}

/**
 * 불러와진 매칭정보 세팅
 * */
function settingAacLoadInformation() {
    var selects = document.getElementsByClassName('aac-master-car-type-select');

    for (var i = 0, len = selects.length; i < len; i++) {
        var typeSelect = selects[i];
        var type = typeSelect.value;
        if (type !== "") {
            var panelGroup = typeSelect.closest('.aac-match-panel-group');
            var selectedCimasterIdx = panelGroup.dataset.origCimasterIdx;

            appendAacMasterCarModel($(panelGroup).find('.aac-master-car-model-select'), 1, mAacMasterCarInventory['type'+type], selectedCimasterIdx);
        }

    }
}

/**
 * 매칭모델 차종들 넣는 함수
 *
 * @param _sel         JQUERYElement
 * @param _type        int           넣는타입 [0:차종이 선택되어진 상태가 아닌 새로 등급선택시, 1:차종이 선택되어진경우]
 * @param _inventory   array
 * @param _selectedIdx int           선택되어진 차종 [_type 이 1인 경우]
 */
function appendAacMasterCarModel(_sel, _type, _inventory, _selectedIdx) {
    var selectedIdx = _type === 1? _selectedIdx : 0;

    var fragment = document.createDocumentFragment();

    fragment.appendChild(getAacOptionElement("", "모델선택", false));

    for (var i = 0, len = _inventory.length; i < len; i++) {
        var obj = _inventory[i];

        var selected = Number(obj.carInfoIdx) === Number(selectedIdx);

        var option = getAacOptionElement(obj.carInfoIdx, "[".concat(obj.brand, "] ", obj.model), selected);

        fragment.appendChild(option);
    }

    $(_sel).append($(fragment));
}

/**
 * 옵션 엘리먼트 얻기
 *
 * @param _value            옵션 값
 * @param _text             옵션 텍스트
 * @param _selected boolean
 *
 * @returns HTMLElement
 */
function getAacOptionElement(_value, _text, _selected) {
    var option = document.createElement('option');
    option.value = _value;
    option.textContent = _text;
    option.selected = _selected;

    return option;
}

/**
 * 저장 프로세스
 */
function saveAac() {
    var saveData = {
        'selectedIdx': mAacSelectedIdx,
        'selectedApi': mAacSelectedApi,
        'changeObj': mAacChangeInventory
    };

    $.ajax({
        type: "POST",
        data: saveData,
        url: "/apiAffiliate/ApiAffiliateCar?type=u",
        dataType: 'json',
        success: function(_res) {
            if (_res.result === 1) {
                Swal.fire({
                    title: '저장완료',
                    text: '저장이 완료되었습니다',
                    type: 'success',
                    confirmButtonText: "확인",
                }).then((result) => {
                    if (result.value)
                        location.reload();

                });
            }
        }
    });
}