var PMBL_CRUD_U = "u";

var PMBL_CRUD_ENTER_DATE = "ed";

toastr.options = {
    'closeButton': true,
    'positionClass': 'toast-bottom-right',
    'preventDuplicates': true
};

$(function() {
    $("#pmbl_affiliate_intro_modal_container").load(assetUrl.concat("modal/affiliate_intro_modal.html"), function() {
        $("#pmbl_affiliate_uniqueness_modal_container").load(assetUrl.concat("modal/affiliate_uniqueness_modal.html"), function() {
            $("#pmbl_tel050_matching_container").load(assetUrl.concat("modal/matching_050_modal.html?ver=190607"), function() {});
        });
    });
});

$(document).on('click', '.pmbl-intro-msg-btn', function() {
    var isWrote = this.dataset.isWrote;
    var introMsg = (isWrote === "0")? "" : $(this).parents('td').find('.pmbl-intro-msg-span').text();

    var obj = {
        'affiIdx': this.dataset.affiIdx,
        'affiPart': this.dataset.affiPart,
        'isWrote': isWrote,
        'introMsg': introMsg,
    };

    callAffiIntroModal(obj, function() {
        location.reload();
    });
});

/**
 * 업체특이사항
 */
$(document).on('click', '.pmbl-uniqueness-btn', function() {
    var branchIdx = this.dataset.branchIdx;
    var span      = $(this).parents('.pmbl-uniqueness-td').find('.pmbl-uniqueness-span');

    var obj = {
        'branchIdx': branchIdx,
        'contents': span.html()
    };

    initAffiUniquenessModal(obj);

});

/*
part : in, out, api
 */
$(document).on('click', '.pmbl-tel050-a', function() {
    var type = Number(this.dataset.aType);

    if (type === 1 || type === 2) {
        var obj = (type === 1)?
            {
                'type': 1,
                'title': '(0001 ~ 4999)',
                'affiType': 1,
                'affiIdx': this.dataset.affiliateIdx,
                'branchTel': this.dataset.tel,
                'part': 'in',
                'recommendNum': mPmblRecommendNum

            } :
            {
                'type': 2,
                'affiType': 1,
                'affiIdx': this.dataset.affiliateIdx,
                'delTel': this.dataset.origin050Tel,
                'branchTel': this.dataset.tel,
            };

        callMatch050Modal(obj);
    } else {

    }

});

$(document).on('click', '#pmbl_enter_date_btn', function() {
    var affiIdx = this.dataset.affiliateIdx;
    var enterDate = $(this).parents('tr').find('.pmbl-enter-date').val();

    if (enterDate !== "") {
        Swal.fire({
            title: '입점일 변경',
            text: '해당업체의 일점일을 변경하시겠습니까?',
            type: 'question',
            showCancelButton: true,
            confirmButtonText: "네, 진행합니다.",
            cancelButtonText: "아니오"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    method: 'PATCH',
                    dataType: 'json',
                    url: '/partners/PartnersAffiliate?crud='.concat(PMBL_CRUD_U, '&type=', PMBL_CRUD_ENTER_DATE),
                    data: {
                        'affiIdx': affiIdx,
                        'enterDate': enterDate
                    },
                    statusCode : {
                        404: function() {
                            toastr.error('잘못된 경로 입니다');

                        }
                    },
                    success: function(_res) {
                        if (_res.result === 1) {
                            Swal.fire({
                                title: '변경완료',
                                text: '입점일 변경이 완료되었습니다.',
                                type: 'success',
                                confirmButtonText: "확인",
                            }).then((result) => {
                                if (result.value)
                                    location.reload();

                            });

                        }
                    }
                });


            }

        });

    } else
        toastr.error("날짜가 유효하지 않습니다");

});