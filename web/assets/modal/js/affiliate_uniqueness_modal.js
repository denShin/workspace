var mAffiUniquenessCallback = null;
var mAffiuniquenessBranchIdx = 0;

$(document).on('click', '#affiliate_uniqueness_modal_apply_btn', function() {
    Swal.fire({
        title: '업체 특이사항',
        text: '업체 특이사항을 적용하시겠습니까?',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: "네, 적용합니다",
        cancelButtonText: "아니오"
    }).then((_result) => {
        if (_result) {
            var uniqueness = replaceRnToBr(document.getElementById('affiliate_uniqueness_modal_textarea').value);

            var postData = {
                'uniqueness': uniqueness,
                'branchIdx': mAffiuniquenessBranchIdx
            };

            $.ajax({
                type: "POST",
                url:"/partners/PartnerMemberProc?emode=uniqueness",
                data: postData,
                dataType: "json",
                success: function(_res) {
                    if (_res.result === 1) {
                        Swal.fire({
                            title: '저장완료',
                            text: '저장이 완료되었습니다',
                            type: 'success',
                            confirmButtonText: "확인",
                        }).then((result) => {
                            if (result.value) {
                                if (typeof mAffiUniquenessCallback === 'function')
                                    mAffiUniquenessCallback();
                                else
                                    location.reload();

                            }

                        });
                    }

                },
                error: function(x, o, e) {

                }
            });
        }

    });
});

$(document).on('click', '#affiliate_uniqueness_modal_close_btn', function() {
    closeModal('#affiliate_uniqueness_modal');
});

function initAffiUniquenessModal(_obj, _callback) {
    mAffiUniquenessCallback = _callback;
    mAffiuniquenessBranchIdx = _obj.branchIdx;

    showModal('#affiliate_uniqueness_modal', function() {
        document.getElementById('affiliate_uniqueness_modal_textarea').value = replaceBrToRn(_obj.contents);

    });
}