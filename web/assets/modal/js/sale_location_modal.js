var mSalocCallback = null;
var mSalocSidoJson = [];
var mSalocGugunJson = [];
var mSalocDongJson = [];
var mSalocSelectedGugunJson = [];
var mSalocSeletedDongJson = [];

var M_SALOC_CODE_JEJU = 'Q';

$(document).on('click', '.saloc-service-sido-a', function() {
    var selectedSido = this.dataset.sido;

    if (!(selectedSido in mSalocGugunJson)) {
        loadSalocGugunJson(selectedSido, function() {
            settingSalocGugun(selectedSido);
        })
    } else {
        settingSalocGugun(selectedSido);
    }

});

$(document).on('click', '.saloc-delivery-sido-a', function() {
    settingSalocDong(this.dataset.sido);
});

$(document).on('change', '.saloc-service-gugun-checkbox', function() {
    var thisCheckbox = this;
    var sidoGugun = thisCheckbox.dataset.gugun;

    if (!(sidoGugun in mSalocDongJson)) {
        loadSalocDongJson(sidoGugun, function() {
            clickAfterSalocGugunCheckbox(thisCheckbox, sidoGugun);
        });

    } else
        clickAfterSalocGugunCheckbox(thisCheckbox, sidoGugun);
});

$(document).on('click', '.saloc-gugun-button', function() {
    var sidoGugun = this.dataset.sidoGugun;
    var dongJson = mSalocDongJson[sidoGugun];

    var fragment = document.createDocumentFragment();

    document.getElementById('saloc_delivery_gugun_tab_ul').innerHTML = "";
    dongJson.forEach(function(_dong) {
        var li = document.createElement('li');
        var label = document.createElement('label');
        var checkbox = document.createElement('input');

        var sidoGugunDongCode = sidoGugun.concat('~', _dong.c);
        var checkboxId = 'saloc_delivery_dong_'.concat(sidoGugunDongCode);

        var checkedValue = mSalocSeletedDongJson.indexOf(sidoGugunDongCode) !== -1;

        li.classList.add('col-xs-4');
        li.classList.add('saloc-service-gugun-dong-li');
        li.classList.add('workspace-li-none-bullet');

        label.innerHTML = '&nbsp;&nbsp;'.concat(_dong.s);
        label.htmlFor = checkboxId;

        checkbox.classList.add('saloc-delivery-gugun-dong-checkbox');
        checkbox.type = 'checkbox';
        checkbox.id = checkboxId;
        checkbox.dataset.type = 'gugun-dong';
        checkbox.dataset.sidoGugunDong = sidoGugunDongCode;
        checkbox.checked = checkedValue;

        li.appendChild(checkbox);
        li.appendChild(label);
        fragment.appendChild(li);
    });

    document.getElementById('saloc_delivery_gugun_tab_ul').appendChild(fragment);
    document.getElementById('saloc_delivery_gugun_header').textContent = this.textContent;
});

$(document).on('change', '.saloc-delivery-gugun-dong-checkbox', function() {
    var sidoGugunDong = this.dataset.sidoGugunDong;

    if (this.checked) {
        if (mSalocSeletedDongJson.indexOf(sidoGugunDong) === -1)
            mSalocSeletedDongJson.push(sidoGugunDong);

    } else {
        var index = mSalocSeletedDongJson.indexOf(sidoGugunDong);
        if (index !== -1)
            mSalocSeletedDongJson.splice(index, 1);

    }

});

$(document).on('click', '#saloc_service_view_arrow_span', function() {
    var isView = this.dataset.view;

    if (isView === "1") {
        $("#saloc_service_div").hide();
        this.classList.remove('glyphicon-chevron-up');
        this.classList.add('glyphicon-chevron-down');
        this.dataset.view = "0";

    } else {
        $("#saloc_service_div").show();
        this.classList.remove('glyphicon-chevron-down');
        this.classList.add('glyphicon-chevron-up');
        this.dataset.view = "1";

    }

});

$(document).on('click', '#saloc_delivery_view_arrow_span', function() {
    var isView = this.dataset.view;

    if (isView === "1") {
        $("#saloc_delivery_div").hide();
        this.classList.remove('glyphicon-chevron-up');
        this.classList.add('glyphicon-chevron-down');
        this.dataset.view = "0";

    } else {
        $("#saloc_delivery_div").show();
        this.classList.remove('glyphicon-chevron-down');
        this.classList.add('glyphicon-chevron-up');
        this.dataset.view = "1";

    }

});

$(document).on('click', '#saloc_service_gugun_select_all_button', function() {
    var liArray = document.getElementsByClassName('saloc-service-gugun-checkbox');

    [].forEach.call(liArray, function (_checkbox) {
        _checkbox.checked = false;
        $(_checkbox).click();

    });

});

$(document).on('click', '#saloc_service_gugun_deselect_all_button', function() {
    var liArray = document.getElementsByClassName('saloc-service-gugun-checkbox');

    [].forEach.call(liArray, function (_checkbox) {
        _checkbox.checked = true;
        $(_checkbox).click();

    });

});

$(document).on('click', '#saloc_delivery_gugun_select_all_button', function() {
    var liArray = document.getElementsByClassName('saloc-delivery-gugun-dong-checkbox');

    [].forEach.call(liArray, function (_checkbox) {
        _checkbox.checked = false;
        $(_checkbox).click();

    });

});

$(document).on('click', '#saloc_delivery_gugun_deselect_all_button', function() {
    var liArray = document.getElementsByClassName('saloc-delivery-gugun-dong-checkbox');

    [].forEach.call(liArray, function (_checkbox) {
        _checkbox.checked = true;
        $(_checkbox).click();

    });

});

$(document).on('click', '#saloc_ok_button', function() {
    closeModal("#sale_loc_modal", function() {
        mSalocCallback(changeSalocArrToString(mSalocSelectedGugunJson), changeSalocArrToString(mSalocSeletedDongJson));

    });

});

function callSaleLocationModal(_obj, _callback) {
    if (typeof _callback === 'function')
        mSalocCallback = _callback;

    initSalocLocationInfo();

    showModal('#sale_loc_modal', function() {
        mSalocSelectedGugunJson = changeSalocStringToArr(_obj['areaString']);
        mSalocSeletedDongJson = changeSalocStringToArr(_obj['deliveryString']);

    });
}

function initSalocLocationInfo() {
    if (mSalocSidoJson.length === 0) {
        loadSalocSidoJson(function() {
            settingSalocSido();
        });
    } else {
        settingSalocSido();
    }

}

function loadSalocSidoJson(_callback) {
    $.ajax({
        dataType: "json",
        url: assetUrl.concat('json/sido.json'),
        success: function(_res) {
            mSalocSidoJson = _res;

            _callback();
        },
        statusCode: {
            404 : function() {
                toastr.error("파일에 접근할 수 없습니다");
            },
            405 : function() {
                toastr.error("파일 접근권한이 없습니다");
            }
        }
    });
}

function loadSalocGugunJson(_selectedSido, _callback) {
    $.ajax({
        dataType: "json",
        url: assetUrl.concat('json/', _selectedSido, '.json'),
        success: function(_res) {
            mSalocGugunJson[_selectedSido] = _res;

            _callback();
        },
        statusCode: {
            404 : function() {
                toastr.error("파일에 접근할 수 없습니다");
            },
            405 : function() {
                toastr.error("파일 접근권한이 없습니다");
            }
        }
    });
}

function loadSalocDongJson(_selectedGugun, _callback) {
    $.ajax({
        dataType: "json",
        url: assetUrl.concat('json/', _selectedGugun, '.json'),
        success: function(_res) {
            mSalocDongJson[_selectedGugun] = _res;

            _callback();
        },
        statusCode: {
            404 : function() {
                toastr.error("파일에 접근할 수 없습니다");
            },
            405 : function() {
                toastr.error("파일 접근권한이 없습니다");
            }
        }
    });
}

function settingSalocSido() {
    initializationSalocSido();
    initializationSalocDeliverySido();

    var fragment = document.createDocumentFragment();
    var fragmentDelivery = document.createDocumentFragment();

    mSalocSidoJson.forEach(function (_data, _index) {
        fragment.appendChild(createSalocSidoTab(_data, _index === 0, "service"));
        fragmentDelivery.appendChild(createSalocSidoTab(_data, _index === 0, "delivery"));

    });

    $('#saloc_service_sido_tab_ul').append(fragment);
    $('#saloc_delivery_sido_tab_ul').append(fragmentDelivery);
    $('.saloc-service-li:first-child .saloc-service-sido-a').click();

}

function createSalocSidoTab(_data, _isFirst, _serviceDeliveryString) {
    var li = document.createElement('li');

    li.classList.add('saloc-'.concat(_serviceDeliveryString, '-li'));
    if (_isFirst)
        li.classList.add('active');

    li.innerHTML = "<a data-toggle='tab' class='saloc-".concat(_serviceDeliveryString, "-sido-a' id='saloc_", _serviceDeliveryString, "_", _data.c, "' data-sido='", _data.c, "'>", _data.s, "</a>");

    return li;
}

function settingSalocGugun(_sido) {
    initializationSalocGugun();

    var gugun = mSalocGugunJson[_sido];

    var fragment = document.createDocumentFragment();

    gugun.forEach(function (_data) {
        var li = document.createElement('li');
        var label = document.createElement('label');
        var checkbox = document.createElement('input');

        var sidoGugunCode = _sido.concat('_', _data.c);
        var checkboxId = 'saloc_service_'.concat(sidoGugunCode);

        var checkedValue = mSalocSelectedGugunJson.indexOf(sidoGugunCode) !== -1;

        li.classList.add('col-xs-3');
        li.classList.add('saloc-service-gugun-li');
        li.classList.add('workspace-li-none-bullet');

        label.innerHTML = '&nbsp;&nbsp;'.concat(_data.s);
        label.htmlFor = checkboxId;

        checkbox.classList.add('saloc-service-gugun-checkbox');
        checkbox.type = 'checkbox';
        checkbox.id = checkboxId;
        checkbox.dataset.type = 'service';
        checkbox.dataset.gugun = sidoGugunCode;
        checkbox.checked = checkedValue;

        li.appendChild(checkbox);
        li.appendChild(label);
        fragment.appendChild(li);
    });

    document.getElementById('saloc_service_gugun_tab_ul').appendChild(fragment);

    $("#saloc_delivery_".concat(_sido)).click();

}

function settingSalocDong(_sido) {
    var fragment = document.createDocumentFragment();

    document.getElementById('saloc_delivery_selected_gugun_inventory_div').innerHTML = "";
    document.getElementById('saloc_delivery_gugun_header').innerHTML = "";
    document.getElementById('saloc_delivery_gugun_tab_ul').innerHTML = "";

    mSalocSelectedGugunJson.some(function (_gugun) {
        var splited = _gugun.split('_');
        var sido = splited[0];
        var gugun = Number(splited[1]);

        if (_sido !== sido)
            return false;

        var locationString = (sido === "C" && gugun > 13)? mSalocGugunJson[sido][gugun-2]['s'] : mSalocGugunJson[sido][gugun-1]['s'];

        document.getElementById('saloc_delivery_selected_gugun_inventory_div').innerHTML = "";

        var button = document.createElement('button');

        button.classList.add('col-md-12');
        button.classList.add('btn');
        button.classList.add('btn-sm');
        button.classList.add('btn-default');
        button.classList.add('saloc-gugun-button');

        button.textContent = locationString;

        button.dataset.sidoGugun = _gugun;

        button.style.marginBottom = ".5em";

        fragment.appendChild(button);
    });

    document.getElementById('saloc_delivery_selected_gugun_inventory_div').appendChild(fragment);
}

function initializationSalocSido() {
    document.getElementById('saloc_service_sido_tab_ul').innerHTML = '';

}

function initializationSalocDeliverySido() {
    document.getElementById('saloc_delivery_sido_tab_ul').innerHTML = '';

}

function initializationSalocGugun() {
    document.getElementById('saloc_service_gugun_tab_ul').innerHTML = '';
}

function clickAfterSalocGugunCheckbox(_selectCheckbox, _sidoGugun) {
    var sido = _sidoGugun.split('_')[0];

    if (_selectCheckbox.checked) {
        if (mSalocSelectedGugunJson.indexOf(_sidoGugun) === -1)
            mSalocSelectedGugunJson.push(_sidoGugun);

    } else {
        var index = mSalocSelectedGugunJson.indexOf(_sidoGugun);
        if (index !== -1)
            mSalocSelectedGugunJson.splice(index, 1);
    }

    $("#saloc_delivery_".concat(sido)).click();
}

function changeSalocStringToArr(areaStr) {
    var tempArr = areaStr !== null ? areaStr.split(',') : [];
    var areaArr = [];

    for (var i = 0; i < tempArr.length; i++) {
        var temp = tempArr[i].split(/[_~]/);

        if (temp.length === 1) { // 시도 코드만
            areaArr.push(tempArr[i]);
        } else { // 구군 코드도
            var rangeGugun = temp[1].split('-');
            var firstGugun, lastGugun;

            if (rangeGugun.length === 1) {
                firstGugun = parseInt(rangeGugun[0]);
                lastGugun = parseInt(rangeGugun[0]);
            } else {
                firstGugun = parseInt(rangeGugun[0]);
                lastGugun = parseInt(rangeGugun[1]);
            }

            if (temp.length === 2) { // 구군 코드까지만
                for (var j = firstGugun; j <= lastGugun; j++)
                    areaArr.push(temp[0] + '_' + j);
            } else { // 동 코드도
                var rangeDong = temp[2].split('-');
                var firstDong, lastDong;

                if (rangeDong.length === 1) {
                    firstDong = parseInt(rangeDong[0]);
                    lastDong = parseInt(rangeDong[0]);
                } else {
                    firstDong = parseInt(rangeDong[0]);
                    lastDong = parseInt(rangeDong[1]);
                }

                for (var j = firstGugun; j <= lastGugun; j++)
                    for (var k = firstDong; k <= lastDong; k++)
                        areaArr.push(temp[0] + '_' + j + '~' + k);
            }
        }
    }

    // 정렬
    return areaArr.sort(function (a, b) {
        var tempA = a.split(/[_~]/);
        var tempB = b.split(/[_~]/);

        if (tempA[0] === tempB[0]) {
            if (tempA[1] === tempB[1] && tempA.length === 3)
                return tempA[2] - tempB[2];
            else
                return tempA[1] - tempB[1];
        }

        return tempA[0] > tempB[0] ? 1 : -1;
    });
}

function changeSalocArrToString(areaArr) {
    areaArr.sort(function (a, b) {
        var tempA = a.split(/[_~]/);
        var tempB = b.split(/[_~]/);

        if (tempA[0] === tempB[0]) {
            if (tempA[1] === tempB[1] && tempA.length === 3)
                return tempA[2] - tempB[2];
            else
                return tempA[1] - tempB[1];
        }

        return tempA[0] > tempB[0] ? 1 : -1;
    });

    // 범위 문자열 생성
    var resultArr = [];
    var prevTmp = null;
    var rangeStart, rangeEnd;
    var resultStr;

    for (var i = 0; i < areaArr.length; i++) {
        var tmp = areaArr[i].split(/[_~]/);

        if (i === 0) { // 첫 번째
            if (tmp.length === 2) {
                rangeStart = rangeEnd = parseInt(tmp[1]);
            } else if (tmp.length !== 1) {
                rangeStart = rangeEnd = parseInt(tmp[2]);
            }
        } else {
            if (tmp.length !== prevTmp.length) { // 아예 설정 범위가 달라지는 경우
                resultStr = createSalocRangeString(prevTmp, rangeStart, rangeEnd);

                if (resultStr !== "")
                    resultArr.push(resultStr);

                switch (tmp.length) {
                    case 2:
                        rangeStart = rangeEnd = parseInt(tmp[1]);
                        break;
                    case 3:
                        rangeStart = rangeEnd = parseInt(tmp[2]);
                        break;
                }
            } else {
                if (prevTmp.length === 1) { // 시도 전체인 경우 무조건 추가
                    if (prevTmp[0] !== "")
                        resultArr.push(prevTmp[0]);
                } else {
                    var curNum = (tmp.length === 2) ? parseInt(tmp[1]) : parseInt(tmp[2]);

                    if (curNum === rangeEnd + 1) { // 연속적인 범위
                        rangeEnd = curNum;
                    } else { // 연속 X
                        resultStr = createSalocRangeString(prevTmp, rangeStart, rangeEnd);
                        if (resultStr !== "")
                            resultArr.push(resultStr);

                        rangeStart = rangeEnd = curNum;
                    }
                }
            }
        }

        prevTmp = tmp;
    }


    if (prevTmp !== null) {
        // 마지막 하나
        resultStr = createSalocRangeString(prevTmp, rangeStart, rangeEnd);
        resultArr.push(resultStr);

    }

    return resultArr.join();
}

function createSalocRangeString(splittedStr, rangeStart, rangeEnd) {
    if (splittedStr.length === 1)
        return splittedStr[0];

    switch (splittedStr.length) {
        case 2:
            if (rangeStart === rangeEnd)
                return splittedStr[0] + '_' + rangeStart;
            else
                return splittedStr[0] + '_' + rangeStart + '-' + rangeEnd;
        case 3:
            if (rangeStart === rangeEnd)
                return splittedStr[0] + '_' + splittedStr[1] + '~' + rangeStart;
            else
                return splittedStr[0] + '_' + splittedStr[1] + '~' + rangeStart + '-' + rangeEnd;
    }
}