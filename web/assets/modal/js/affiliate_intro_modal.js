var AFFI_INFO_CARD_OBJ = [
    "",
    "안녕하세요, 고객님. 저희는 고객님의 기분 좋은 렌트카 이용을 위해 깨끗한 차량 제공을 최우선으로 생각합니다.",
    "고객님의 차량 인수/반납을 꼼꼼히 도와드리겠습니다.  저희 업체가 가장 잘 하고, 자신있는 부분입니다.",
    "저희 업체의 차량은 자차보험 가입으로 안심하고 이용하실 수 있습니다.  항상 노력하는 저희 업체가 되겠습니다.",
    "꼼꼼한 차량 정비/세차는 저희 업체의 자랑입니다.  깨끗하고 안전한 렌트카로 고객님을 맞이하겠습니다.",
    "안녕하세요, 고객님. 저희는 친절함을 최우선의 가치로 생각합니다.  고객님의 기분 좋은 이용을 위해 항상 노력하겠습니다."
];
var AFFI_PARTNERS_CODE = "in";
var AFFI_TEL_CODE = "out";
var AFFI_API_CODE = "API";

var mAffiIntroAffiPart = ""; // in : 파트너스, out : 전화걸기 (없음), api : 제주 API
var mAffiIntroAffiIdx = "";
var mAffiIntroCallback = null;

/**
 * 카드 변경시마다 template 제공
 *
 * @param _value string
 */
function changeAffiIntroCard(_value) {
    document.getElementById('affi_intro_modal_intro_msg').value = AFFI_INFO_CARD_OBJ[_value];

}

/**
 * 모달 호출
 *
 * @param _obj      object
 * @param _callback null|function
 */
function callAffiIntroModal(_obj, _callback) {
    mAffiIntroAffiIdx = _obj.affiIdx;
    mAffiIntroAffiPart = _obj.affiPart;
    mAffiIntroCallback = (typeof _callback === 'undefined')? null : _callback;

    var introMsg = _obj.isWrote === "1"? _obj.introMsg : "";

    document.getElementById('affi_intro_modal_intro_code').value = '0';
    document.getElementById('affi_intro_modal_intro_msg').value = introMsg;

    showModal('#affi_intro_modal', function() {});

}

function saveAffiIntro() {

    if (mAffiIntroAffiPart === AFFI_PARTNERS_CODE) {

        $.ajax({
            method: 'PATCH',
            dataType: 'json',
            url: "/partners/PartnersAffiliate?crud=u&type=im",
            data: {
                'affiIdx': mAffiIntroAffiIdx,
                'introCode': document.getElementById('affi_intro_modal_intro_code').value,
                'introMsg': document.getElementById('affi_intro_modal_intro_msg').value
            },
            statusCode : {
                404: function() {
                    toastr.error('잘못된 경로 입니다');

                }
            },
            success: function(_res) {
                if (_res.result === 1) {
                    Swal.fire({
                        title: '변경완료',
                        text: '업체한마디 변경이 완료되었습니다.',
                        type: 'success',
                        confirmButtonText: "확인",
                    }).then((result) => {
                        if (result.value) {
                            if (typeof mAffiIntroCallback === "function")
                                mAffiIntroCallback();
                        }

                    });

                }
            }
        });
    }

}