var mBizretransCallback = null;
var mBizretransType = 0;
var mBizretransMtpr = 0;

$(document).on('click', '#bizretrans_close_btn', function() {
    closeModal('#biztalk_retransmit_modal');
});

$(document).on('click', '#bizretrans_apply_btn', function() {
    var reqDateTime = "";
    if (mBizretransType === 1)
        reqDateTime = $("#bizretrans_date_input").val().concat(" ", $("#bizretrans_time_input").val(), ":00");

    var recNum = remainOnlyNumber($("#bizretrans_rec_num_input").val());

    Swal.fire({
        title: (mBizretransType === 1)? "내용수정" : "내용 수정 후 즉시발송",
        text: (mBizretransType === 1)? "해당 알림톡 내용을 변경하시겠습니까?" : "해당 알림톡 내용 수정 후 즉시발송하시겠습니까?",
        type: 'question',
        showCancelButton: true,
        confirmButtonText: "네, 진행합니다",
        cancelButtonText: "아니오"
    }).then((result) => {
        if (result.value) {
            var content = $("#bizretrans_modify_content_textarea").val();
            var urlType = (mBizretransType === 1)? "mc" : "mcrt";

            $.ajax({
                type: "POST",
                data: {
                    "mtPr": mBizretransMtpr,
                    "content": content,
                    "reqTime": reqDateTime,
                    "recNum": recNum
                },
                url: "/carmore/alarm/biztalk/BiztalkContent?type=".concat(urlType),
                dataType: 'json',
                success: function(_res) {
                    if (_res.result === 1) {
                        if (typeof mBizretransCallback === "function")
                            mBizretransCallback();

                    }
                }
            });
        }

    });



});

function initBizretransModal(_obj, _callback) {
    mBizretransCallback = _callback;
    mBizretransType = Number(_obj.type);
    mBizretransMtpr = Number(_obj.mtPr);

    if (mBizretransType === 1) {
        var dateTime = _obj.reqTime;
        $("#bizretrans_date_input").val(dateTime.substr(0, 10));
        $("#bizretrans_time_input").val(dateTime.substr(11, 5));
    }
    else
        $("#bizretrans_modal_datetime_div").hide();

    showModal('#biztalk_retransmit_modal', function() {
        $("#bizretrans_title_h4").text((mBizretransType === 1)? "비즈톡 내용수정" : "비즈톡 내용수정 후 즉시발송");
        $("#bizretrans_modal_code_div").text(_obj.code.concat(" 코드"));
        $("#bizretrans_modal_template_div").html(_obj.template.replace(/(\n|\r\n)/g, '<br>'));
        $("#bizretrans_rec_num_input").val(_obj.recNum);
        $("#bizretrans_modify_content_textarea").text(_obj.contents);
    });
}