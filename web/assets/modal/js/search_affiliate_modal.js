var mSfamCallback  = null;
var mSfamAffiInven = {};

$(document).on('click', '.search-affiliate-modal-select-btn', function() {
    var row = this.closest('.search-affiliate-modal-result-row');

    var branchIdx = row.dataset.branchIdx;
    closeModal('#search_affiliate_modal', function() {
        mSfamCallback(mSfamAffiInven[branchIdx]);

    });
});

/**
 * 업체찾기 인풋 엔터
 */
$(document).on('keyup', '#search_affiliate_modal_search_input', function(e) {
    if (e.keyCode === 13)
        $('#search_affiliate_modal_search_btn').click();
});

/**
 * 검색버튼
 */
$(document).on('click', '#search_affiliate_modal_search_btn', function() {
    procSfamSearch(document.getElementById('search_affiliate_modal_search_type_select').value, document.getElementById('search_affiliate_modal_search_input').value);
});

/**
 * 모달 첫 호출시 부르는 함수
 *
 * @param _callback function
 */
function callSearchAffiliateModal(_callback) {
    showModal('#search_affiliate_modal', function() {
        initSfam();
        mSfamCallback = _callback;

    });

}

/**
 * 변수 초기화
 */
function initSfam() {
    mSfamCallback = null;
    mSfamAffiInven = {};
    document.getElementById('search_affiliate_modal_tbody').textContent = '';
    document.getElementById('search_affiliate_modal_search_input').value = '';
    $("#search_affiliate_modal_container").hide();
    $("#search_affiliate_modal_empty_container").show();
}

/**
 * 업체검색
 *
 * @param _sp int 검색업체 타입 [1:파트너스 업체, 2:API 업체]
 * @param _sv
 */
function procSfamSearch(_sp, _sv) {
    if (_sv === "")
        toastr.error("검색어를 입력해주세요");
    else {
        $.ajax({
            type: 'POST',
            url: '/modal/affiliate/AffiliateInventory?searchParam='.concat(encodeURI(_sp), '&searchValue=', encodeURI(_sv)),
            data: {},
            dataType: 'json',
            success: function(_res) {
                if (_res.result === 1) {
                    var inventory = _res.inventory;

                    var inventoryLength = inventory.length;

                    if (inventoryLength === 0) {
                        $("#search_affiliate_modal_container").hide();
                        $("#search_affiliate_modal_empty_container").show();
                    } else {
                        $("#search_affiliate_modal_container").show();
                        $("#search_affiliate_modal_empty_container").hide();

                        var fragment = document.createDocumentFragment();
                        for (var i = 0; i < inventoryLength; i++)
                            appendSfamSearchResultRow(fragment, inventory[i]);

                        document.getElementById('search_affiliate_modal_tbody').appendChild(fragment);
                    }
                }
            },
            beforeSend: function() {
                mSfamAffiInven = {};
                document.getElementById('search_affiliate_modal_tbody').textContent = '';
            }
        });
    }
}

/**
 * 검색결과 행 넣기
 *
 * @param _fragment Fragment
 * @param _obj      Object
 */
function appendSfamSearchResultRow(_fragment, _obj) {
    var branchIdx = _obj.branchIdx;
    var affiliateName = _obj.companyName.concat(' ', _obj.branchName);

    mSfamAffiInven[branchIdx] = _obj;

    var row = document.createElement('tr');

    row.classList.add('search-affiliate-modal-result-row');

    var html = '<td>'.concat(affiliateName.trim(), '</td>');
    html = html.concat('<td>', _obj.address, '</td>');
    html = html.concat('<td>', _obj.tel, '</td>');
    html = html.concat('<td><button type="button" class="btn btn-success btn-xs search-affiliate-modal-select-btn">업체 선택</button></td>');

    row.innerHTML = html;

    row.dataset.branchIdx = branchIdx;
    row.dataset.affiType = _obj.type;

    _fragment.appendChild(row);

}

/**
 * 모달창 닫기
 */
function closeSearchAffiliateModal() {
    closeModal('#search_affiliate_modal');
}