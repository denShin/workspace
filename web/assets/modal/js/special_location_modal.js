let mSlmCallback = null;
let mSlmLocation = [];
let mSlmSelectedLocation = [];

const SLM_AJAX_RESULT_SUCCESS = 1;

$(document).on('click', '.slm-location-tab-a', function() {
    if (!(this.classList.contains('active')))
        settingSlmLocation(this.dataset.locationType);
});

$(document).on('click', '.slm-special-location-checkbox', function() {
    var locationIdx = this.value;

    if (this.checked) {
        if (mSlmSelectedLocation.indexOf(locationIdx) === -1)
            mSlmSelectedLocation.push(locationIdx);

    } else {
        var index = mSlmSelectedLocation.indexOf(locationIdx);
        if (index !== -1)
            mSlmSelectedLocation.splice(index, 1);
    }
});

$(document).on('click', '#slm_ok_button', function() {
    closeModal('#special_location_modal', function() {
        mSlmCallback(mSlmSelectedLocation.join());
    });
});

function callSpecialLocationModal(_locationString, _callback) {
    showModal('#special_location_modal', function() {
        mSlmSelectedLocation = (_locationString === "")? [] : _locationString.split(',');
        mSlmCallback = _callback;

        slmLoadSpecialLocation(function() {
            slmInitializationSetting();

        });
    });
}

/**
 * @param _callback
 */
function slmLoadSpecialLocation(_callback) {
    $.ajax({
        dataType: 'json',
        url: '/carmore/CarmoreSpecialLocation',
        /**
         * @param _res.result                   int
         * @param _res.specialLocationInventory object
         */
        success: function(_res) {
            if (_res.result === SLM_AJAX_RESULT_SUCCESS) {
                mSlmLocation = _res.specialLocationInventory;

                _callback();
            }
        }
    });
}

function slmInitializationSetting() {
    $("#slm_airport_tab").click();

}

function slmInitializationLocationUl() {
    document.getElementById('slm_location_ul').innerHTML = "";
}

function settingSlmLocation(_key) {
    const specialLocationArray = mSlmLocation[_key];

    slmInitializationLocationUl();

    var fragment = document.createDocumentFragment();

    specialLocationArray.forEach(function(_obj) {
        let checkBox = slmCreateCheckbox(_obj);

        fragment.appendChild(checkBox);

    });

    document.getElementById('slm_location_ul').appendChild(fragment);
}

function slmCreateCheckbox(_obj) {
    let li = document.createElement('li');
    let label = document.createElement('label');
    let checkbox = document.createElement('input');

    const idx = _obj.idx;
    const checkboxId = 'slm_special_location'.concat(idx);

    li.classList.add('col-xs-4');
    li.classList.add('slm-special-location-li');
    li.classList.add('workspace-li-none-bullet');

    label.innerHTML = '&nbsp;&nbsp;'.concat(_obj.name);
    label.htmlFor = checkboxId;

    checkbox.classList.add('slm-special-location-checkbox');
    checkbox.type = 'checkbox';
    checkbox.id = checkboxId;
    checkbox.value = idx;
    checkbox.checked = (mSlmSelectedLocation.indexOf(idx) !== -1);

    li.appendChild(checkbox);
    li.appendChild(label);

    return li;
}