var mMatch050Callback = null;
var mMatch050AffiType = 0;
var mMatch050AffiIdx = 0;
var mMatch050AffiPart = "";
var mMatchBranchTel = "";

var ONSE_050_PREFIX = "05037961";

$(document).on('click', '#match050_modal_register_btn', function() {

    Swal.fire({
        title: '050번호 매칭',
        text: '050번호를 매칭하시겠습니까?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: "네, 진행합니다",
        cancelButtonText: "아니오"
    }).then((result) => {
        if (result.value) {
            var addNum = document.getElementById('match050_modal_number_input').value;
            var intNum = Number(addNum);
            if (addNum === "") {
                Swal.fire('', '번호를 입력하세요', 'error');

            } else {

                var url = "/partners/PartnerMemberProc?emode=sendnewtel&datamode=new&tel050_part=".concat(mMatch050AffiPart, "&fk_serial=", mMatch050AffiIdx, "&affiType=", mMatch050AffiType, "&sendnewtelnum=", addNum, "&branch_tel=", mMatchBranchTel);

                $.ajax({
                    type: "get",
                    url: url,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        var json = JSON.parse(response);

                        var result = json["result"];
                        if (result ==="y") {

                            Swal.fire({
                                title: '매칭완료',
                                text: '매칭이 완료되었습니다',
                                type: 'success',
                                confirmButtonText: "확인",
                            }).then((result) => {
                                if (result.value) {
                                    if (typeof mMatch050Callback === 'function')
                                        mMatch050Callback();
                                }

                            });


                        } else if (result ==="tel_exists") {
                            alert('이미 등록된 번호 입니다. 다른번호를 기입해주세요');

                        }

                    }
                });


            }

        }

    });

});

function callMatch050Modal(_obj, _callback) {
    console.log(_obj);
    mMatch050Callback = (typeof _callback === 'function')? _callback : null;
    mMatch050AffiType = _obj.affiType;
    mMatch050AffiIdx = _obj.affiIdx;
    mMatchBranchTel = _obj.branchTel;

    if (_obj.type === 1) {
        mMatch050AffiPart = _obj.part;

        showModal('#match050_modal', function() {
            $("#match050_modal_range_span").text(_obj.title);
            $("#match050_modal_number_input").val(_obj.recommendNum);

        });
    } else {
        mMatch050AffiPart = (typeof _obj.part !== 'undefined')? _obj.part : '';

        deleteMatch050Modal(_obj.delTel);

    }

}

function closeMatch050Modal() {
    closeModal('#match050_modal');
}

function deleteMatch050Modal(_tel050) {
    Swal.fire({
        title: '050번호 해제',
        text: '매칭한 050번호를 해제하시겠습니까?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: "네, 진행합니다",
        cancelButtonText: "아니오"
    }).then((result) => {
        if (result.value) {
            var url = "/partners/PartnerMemberProc?emode=sendelnewtel&datamode=del&bserial=".concat(mMatch050AffiIdx, "&affiType=", mMatch050AffiType, "&deltel=", _tel050, "&branch_tel=", mMatchBranchTel, '&tel050_part=', mMatch050AffiPart);

            // ajax 통신 필
            $.ajax({
                type: "GET",
                url: url,
                contentType: false,
                processData: false,
                success: function () {

                    Swal.fire({
                        title: '해제완료',
                        text: '해제가 완료되었습니다',
                        type: 'success',
                        confirmButtonText: "확인",
                    }).then((result) => {
                        if (result.value)
                            location.reload();

                    });
                }
            });

        }

    });

}