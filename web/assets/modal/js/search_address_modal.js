var mSearchAddressGeocoder = null;
var mSearchAddressPlaceSearch = null;
var mSearchAddressPrevLocationTxt = "";
var mSearchAddressLastKeyword = "";
var mSearchAddressResult = null;
var mSearchAddressPagination = null;
var mSearchAddressCallback = null;

/**
 * 주소찾기 인풋 엔터
 */
$(document).on('keyup', '#search_address_modal_search_input', function(e) {
    if (e.keyCode === 13)
        $('#search_address_modal_search_btn').click();
});

/**
 * 검색버튼
 */
$(document).on('click', '#search_address_modal_search_btn', function() {
    procSamSearch(document.getElementById('search_address_modal_search_input').value);
});

/**
 * 행 클릭
 */
$(document).on("click", ".search-address-row", function (e) {
    if (document.getElementsByClassName('search-address-row-success')[0]) {
        document.getElementsByClassName('search-address-row-success')[0].classList.remove('success');
        document.getElementsByClassName('search-address-row-success')[0].classList.remove('search-address-row-success');
    }
    this.classList.add('success');
    this.classList.add('search-address-row-success');

});

/**
 * 자세히보기 클릭
 */
$(document).on('click', '.address-detail-info-link', function () {
    var row = this.closest('tr');

    var idx = row.dataset.rowIdx;

    if (mSearchAddressResult[idx]) {
        if (mSearchAddressResult[idx].place_url)
            window.open(mSearchAddressResult[idx].place_url);
    }

});

/**
 * 모달 첫 호출시 부르는 함수
 *
 * @param _mainAddress string 메인주소
 */
function callSearchAddressModal(_mainAddress, _callback) {
    initSam(false);
    showModal('#search_address_modal', function() {
        document.getElementById('search_address_modal_search_input').value = _mainAddress;

        if (mSearchAddressGeocoder === null)
            mSearchAddressGeocoder = new daum.maps.services.Geocoder();

        if (mSearchAddressPlaceSearch === null)
            mSearchAddressPlaceSearch = new daum.maps.services.Places();

        mSearchAddressCallback = _callback;
    });

}

/**
 * 초기화
 *
 * @param _data boolean data 만 초기화 하는지
 */
function initSam(_data) {
    $("#search_address_modal_result_tbody").empty();
    $("#search_address_modal_paging_wrapper").empty();

    if (!_data) {
        $("#search_address_modal_result_container").hide();
        $("#search_address_modal_empty_container").hide();

        mSearchAddressResult = null;
        mSearchAddressPagination = null;
    }
}

/**
 * 주소찾기 모달 닫기
 */
function closeSearchAddressModal() {
    closeModal('#search_address_modal');
}

/**
 * 검색, API 호출
 *
 * @param _mainAddress string
 */
function procSamSearch(_mainAddress) {
    if (_mainAddress === null || _mainAddress === undefined || _mainAddress === "" || mSearchAddressPrevLocationTxt === _mainAddress)
        return false;

    if (!_mainAddress.replace(/^\s+|\s+$/g, '')) {
         //빈 검색결과

        return false;
    }

    mSearchAddressPrevLocationTxt = _mainAddress;

    // 장소검색 객체를 통해 키워드로 장소검색을 요청합니다
    mSearchAddressPlaceSearch.keywordSearch(_mainAddress, callbackSamSearch);
}

/**
 * API 검색결과 리턴
 *
 * @param result
 * @param status
 * @param pagination
 */
function callbackSamSearch(result, status, pagination) {
    initSam(false);

    if (status === daum.maps.services.Status.OK) {
        mSearchAddressPagination = pagination;

        setSamPaging(mSearchAddressPagination.current, mSearchAddressPagination.first, mSearchAddressPagination.last, mSearchAddressPagination.last, mSearchAddressPagination.last);

        // 정상적으로 검색이 완료됐으면
        // 검색 목록과 마커를 표출합니다

        if (result.length === 0) {
            showSamNoSearchResult(true);
        } else {
            showSamNoSearchResult(false);
            displaySamSearchResult(result);
        }

    } else if (status === daum.maps.services.Status.ZERO_RESULT) {
        if (mSearchAddressGeocoder == null)
            mSearchAddressGeocoder = new daum.maps.services.Geocoder();

        // 키워드검색에 실패했으니 주소검색
        var keyword = document.getElementById('search_address_modal_search_input').value;

        if (mSearchAddressLastKeyword !== keyword) {
            initSam(false);

            mSearchAddressGeocoder.addressSearch(keyword, function (result, status) {
                mSearchAddressLastKeyword = keyword;
                // 정상적으로 검색이 완료됐으면
                if (status === daum.maps.services.Status.OK) {
                    showSamNoSearchResult(false);
                    displaySamSearchResult(result);
                } else if (status === daum.maps.services.Status.ZERO_RESULT)
                    showSamNoSearchResult(true);
                else if (status === daum.maps.services.Status.ERROR)
                    showSamNoSearchResult(true);

            });
        }

    } else if (status === daum.maps.services.Status.ERROR)
        showNoSearchResult(true);

}

/**
 * pagination 세팅
 *
 * @param _page   현재 페이지
 * @param _firstP
 * @param _lastP
 * @param _total
 * @param _last
 */
function setSamPaging(_page, _firstP, _lastP, _total, _last) {
    var ulPaging = "<ul class='pagination'>";
    if (_page >= 2)
        ulPaging = ulPaging.concat("<li class='li-chevron'><a onclick='goSamPage(", Number(_page) - 1, ", true)'><i class='fa fa-chevron-left' aria-hidden='true'></i></a></li>");

    for (var j = _firstP; j <= _lastP; j++) {
        if (Number(_page) === j)
            ulPaging = ulPaging.concat("<li class='active'><a>", j, "</a></li>");
        else
            ulPaging = ulPaging.concat("<li><a onclick='goSamPage(", j, ", true)'>", j, "</a></li>");
    }

    if (_last < _total)
        ulPaging = ulPaging.concat("<li class='li-chevron'><a onclick='goSamPage(", Number(_page) + 1, ", true)'><i class='fa fa-chevron-right' aria-hidden='true'></i></a></li>");

    ulPaging += "</ul></div>";

    document.getElementById('search_address_modal_paging_wrapper').innerHTML = ulPaging;
}

/**
 * 검색결과 없는 문구 보여줄지말지
 *
 * @param _isShow boolean
 */
function showSamNoSearchResult(_isShow) {
    if (_isShow) {
        mSearchAddressResult = null;
        mSearchAddressPagination = null;

        $('#search_address_modal_empty_container').show();
        $('#search_address_modal_paging_wrapper').hide();
    } else {
        $("#search_address_modal_empty_container").hide();
        $('#search_address_modal_paging_wrapper').show();
    }
}

/**
 * 검색결과 항목들 보여주기
 *
 * @param _places
 */
function displaySamSearchResult(_places) {
    if (_places.length === 0)
        return;

    mSearchAddressResult = _places;

    var len = _places.length;

    for (var i = 0; i < len; i++)
        appendSamSearchResultRow(_places[i], i);

    $("#search_address_modal_result_container").show();
}

/**
 * 검색결과 행 추가
 *
 * @param _data
 */
function appendSamSearchResultRow(_data, i) {
    var row = document.createElement('tr');

    row.classList.add('search-address-row');

    row.dataset.rowIdx = String(i);

    var newAddress = (_data.road_address_name !== undefined && _data.road_address_name !== null)? _data.road_address_name : "";
    var oldAddress = (_data.address_name !== undefined && _data.address_name !== null)? _data.address_name : "";

    var html = "<td class='text-center'>".concat(_data.place_name, "</td>");
    html = html.concat("<td class='text-center'>", newAddress, "</td>");
    html = html.concat("<td class='text-center'>", oldAddress, "</td>");
    html = html.concat("<td class='text-center'><button class='btn btn-info btn-sm address-detail-info-link'>보기</button></td>");

    row.innerHTML = html;

    $("#search_address_modal_result_tbody").append(row);
}

function goSamPage(_page) {
    initSam(true);

    mSearchAddressPagination.gotoPage(_page);
}

/**
 * 주소 행 선택 후 확인
 */
function selectSamSearchResultRow() {
    var row = document.getElementsByClassName('search-address-row-success')[0];
    if (row) {
        var rowIdx = row.dataset.rowIdx;

        var obj = mSearchAddressResult[rowIdx];

        var param = {
            'address': (obj.road_address_name === "")? obj.address_name : obj.road_address_name,
            'latitude': obj.y,
            'longitude': obj.x
        };

        closeModal("#search_address_modal", function() {
            mSearchAddressCallback(param);
        });
    } else
        toastr.error("주소가 선택되지 않았습니다");

}
