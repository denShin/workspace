/**
 * 모달 오픈하는 함수
 *
 * @param _modal string 모달 아이디
 */
function showModal(_modal, _callback) {
    $(_modal).modal({
        backdrop: 'static',
        keyboard: true
    });

    if (typeof _callback === "function")
        _callback();
}

/**
 * 모달 닫는 함수
 *
 * @param _modal string 모달 아이디
 */
function closeModal(_modal, _callback) {
    $(_modal).modal('hide');

    if (typeof _callback === "function")
        _callback();
}