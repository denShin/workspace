<?php
/**
 * Main.php - 사이트 첫번째 로딩 -파일 , 원하는 곳으로 redirect 함
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    public function index()
    {
        Header("Location:/carmore/Dashboard");
    }


}
