<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * BranchTaxinfo.php - 업체별 세금계산서/ 정산정보, 사업자등록증 정보 등록,수정
 *
 * @property CI_Session $session
 * @property WS_Input   $input
 * @property CI_Config   $config
 * @property CI_Pagination   $pagination
 *
 * @property Workspace_common_func $workspace_common_func
 * @property Customfunc $customfunc
 *
 * @property Taxinfo_model $Taxinfo_model
 */
class BranchTaxinfo extends CI_Controller {

    private $admin_permission,$login_id;

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->library('workspace_common_func');
        $this->load->model('calculate/Taxinfo_model');

        $this->login_id = $this->session->userdata('admin_id');
        $this->admin_permission = $this->customfunc->get_permissionArray($this->login_id);

        if ($this->login_id === '') {
            echo "<script>location.href='/adminmanage/Login'</script>";
            exit();

        }

    }

    public function index()
    {
        $ptype = $this->input->post('ptype', TRUE);

        switch ($ptype)
        {
            case "savetaxinfo":
                $this->savetaxinfo();
                break;
            default:
                $this->loadPartnerMemberBranchList();
                break;
        }
    }

    // 업체별 세금계산서정보 사업자등록증 정보 저장
    public function savetaxinfo(){
        $data["branch_serial"] = $this->input->post('branch_serial', TRUE);
        $data["registration_number"] = $this->input->post('registration_number', TRUE);
        $data["company_name"] = $this->input->post('company_name', TRUE);
        $data["ceo_name"] = $this->input->post('ceo_name', TRUE);
        $data["addr"] = $this->input->post('addr', TRUE);
        $data["bizclass"] = $this->input->post('bizclass', TRUE);
        $data["biztype"] = $this->input->post('biztype', TRUE);



        $data["tax_email"] = $this->input->post('tax_email', TRUE);
        $data["cal_email"] = $this->input->post('cal_email', TRUE);

        $data["tax_email2"] = $this->input->post('tax_email2', TRUE);
        $data["cal_email2"] = $this->input->post('cal_email2', TRUE);
        $data["contactname"] = $this->input->post('contactname', TRUE);

        $data["bankinfo"] = $this->input->post('bankinfo', TRUE);
        $data["bankaccount"] = $this->input->post('bankaccount', TRUE);

        $data["alias"] = $this->input->post('alias', TRUE);

        $result = $this->Taxinfo_model->savetaxinfo($data);

    }

    // 업체별 사업자등록증 리스트 로드
    public function loadPartnerMemberBranchList()
    {

        $this->load->helper('url'); // load the helper first

        $per_page = $this->input->get('per_page', TRUE);
        $sp = $this->workspace_common_func->check_false_value($this->input->get('sp', TRUE), TRUE);
        $sv = $this->workspace_common_func->check_false_value($this->input->get('sv', TRUE), TRUE);
        $mem_stats = $this->input->get('mem_stats', TRUE);

        if ($per_page < 20)
            $per_page = 0;

        $page = ($per_page/20)+1;

        if (!$mem_stats)
            $mem_stats = 'y';

        $total = $this->Taxinfo_model->getBranchTaxinfoCount($sp, $sv);
        $rowresult = $this->Taxinfo_model->getBranchTaxinfoPageList($page, 20, $sp, $sv);

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows'] = $total; // 게시물총수
        $config['per_page']   = "20";  // 게시물출력수
        $config['base_url']   = $config['base_url']."/calculate/BranchTaxinfo?mem_stats=".$mem_stats."&sp=".$sp."&sv=".$sv;
        $config['suffix']     = "";
        $config['use_global_url_suffix'] = false;
        $config['reuse_query_string'] = false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];

        foreach($rowresult as $entry)
        {
            $arr_datalist["serial"]=$entry->serial;
            $arr_datalist["rentCompany_serial"]=$entry->rentCompany_serial;
            $arr_datalist["branchName"]=$entry->branchName;
            $arr_datalist["company_name"]=$entry->company_name;
            $arr_datalist["mem_name"]=$entry->mem_name;
            $arr_datalist["taxinfocnt"]=$entry->taxinfocnt;

            $taxdtl = $this->Taxinfo_model->getBranchTaxinfoDetail($entry->serial);

            $arr_datalist["registration_number"]=$taxdtl->registration_number;
            $arr_datalist["tcompany_name"]=$taxdtl->company_name;
            $arr_datalist["addr"]=$taxdtl->addr;
            $arr_datalist["bizclass"]=$taxdtl->bizclass;
            $arr_datalist["biztype"]=$taxdtl->biztype;
            $arr_datalist["cal_email"]=$taxdtl->cal_email;
            $arr_datalist["tax_email"]=$taxdtl->tax_email;
            $arr_datalist["cal_email2"]=$taxdtl->cal_email2;
            $arr_datalist["tax_email2"]=$taxdtl->tax_email2;
            $arr_datalist["contactname"]=$taxdtl->contactname;

            $arr_datalist["bankinfo"]=$taxdtl->bankinfo;
            $arr_datalist["bankaccount"]=$taxdtl->bankaccount;
            $arr_datalist["ceo_name"]=$taxdtl->ceo_name;
            $arr_datalist["alias"]=$taxdtl->alias;

            $arr_alldatalist[]=$arr_datalist;
        }

        $data["list"]=$arr_alldatalist;
        $data["mem_stats"]=$mem_stats;
        $data["per_page"]=$per_page;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->login_id;
        $data["new050"] =$this->customfunc->get_suggest050number("in");

        $this->load->view('head', array('data'=>$this->admin_permission));
        $this->load->view('calculate/branchtaxinfolist', array('data'=>$data));
        $this->load->view('footer');
    }


}
