<?php
/**
 * Calculatelog.php - 정산내역 리스트, 정산취소,신규정산,세금계산서등록,정산내역이동,추가,삭제 등 컨트롤러
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class CalculateAllog extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION;

    function __construct()
    {
        parent::__construct();


        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->library('Aws_ses');

        $this->load->model('calculate/Calculate_model');
        $this->load->model('calculate/Taxinfo_model');

    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);
        if($ptype=="") $ptype = $this->input->post('ptype', TRUE);

        switch($ptype) {
            default : $this->loadAllog(); break;
        }
    }


    // 세무사 전달용 전체 링크만들기
    public function loadAllog(){
        $calmainmst_code= $this->input->get('calmainmst_code', TRUE);
        $arr_data = $this->Calculate_model->alltaxbanklist($calmainmst_code);

        $arr_allboardlist=[];
        foreach($arr_data as $entry)
        {

            $arr_boardlist["companyname"] =$entry["companyname"];
            $arr_boardlist["branchname"] =$entry["branchname"];
            $arr_boardlist["branchserial"] =$entry["branchserial"];
            $arr_boardlist["companyserial"] =$entry["companyserial"];
            $arr_boardlist["calbranch_idx"] =$entry["calbranch_idx"];
            $arr_boardlist["calmainmst_code"] =$entry["calmainmst_code"];
            $arr_boardlist["calmaindtl_branch"] =$entry["calmaindtl_branch"];
            $arr_boardlist["dtlcnt"] =$entry["dtlcnt"];
            $arr_boardlist["calculate_yn"] =$entry["calculate_yn"];
            $arr_boardlist["bankins_yn"] =$entry["bankins_yn"];
            $arr_boardlist["taxmail_yn"] =$entry["taxmail_yn"];
            $arr_boardlist["tax_msg"] =$entry["tax_msg"];

            if($entry["bankins_yn"]=="y"){
                $arr_boardlist["bankins_ynstr"]="<span style='color:blue'>입금완료</span>";
            }else{
                $arr_boardlist["bankins_ynstr"]="<span style='color:red'>입금대기</span>";
            }
            if($entry["taxmail_yn"]=="y"){
                $arr_boardlist["taxmail_ynstr"]="<span style='color:blue'>발행완료</span>";
            }else if($entry["taxmail_yn"]=="x"){
                $arr_boardlist["taxmail_ynstr"]="<span style='color:red'>발행오류</span>";
            }else if($entry["taxmail_yn"]=="n"){
                $arr_boardlist["taxmail_ynstr"]="<span style='color:gray'>발행대기</span>";
            }


            $arr_boardlist["sumtotalprice"] =$entry["sumtotalprice"];
            $arr_boardlist["sumcalamount"] = $entry["sumcalamount"];
            $arr_boardlist["sumrentfee"] = $entry["sumrentfee"];
            $arr_boardlist["sumdiscount"] =$entry["sumdiscount"];


            $arr_boardlist["sumtotalpricestr"] =number_format( $entry["sumtotalprice"],0);
            $arr_boardlist["sumcalamountstr"] =number_format( $entry["sumcalamount"],0);
            $arr_boardlist["sumrentfeestr"] = number_format( $entry["sumrentfee"],0);
            $arr_boardlist["sumdiscountstr"] = number_format( $entry["sumdiscount"],0);


            $taxdtl = $this->Taxinfo_model->getBranchTaxinfoDetail($entry["calmaindtl_branch"]);

            $arr_boardlist["registration_number"]=$taxdtl->registration_number;
            $arr_boardlist["tcompany_name"]=$taxdtl->company_name;
            $arr_boardlist["addr"]=$taxdtl->addr;
            $arr_boardlist["bizpart"]=$taxdtl->bizpart;
            $arr_boardlist["cal_email"]=$taxdtl->cal_email;
            $arr_boardlist["tax_email"]=$taxdtl->tax_email;
            $arr_boardlist["bankaccount"]=$taxdtl->bankaccount;

            $arr_boardlist["bankinfo"]=$taxdtl->bankinfo;
            $arr_boardlist["ceo_name"]=$taxdtl->ceo_name;


            ///// null check - 세금계산서 정보없으면 정산불가
            if($arr_boardlist["registration_number"]=="" || $arr_boardlist["tax_email"]=="" ||
                $arr_boardlist["cal_email"]=="" ||   $arr_boardlist["bankinfo"]==""
                ||   $arr_boardlist["bankaccount"]=="" ){

                $arr_boardlist["calbranch_idx"] ="x";

            }


            $arr_boardlist["regdate"] =$entry["regdate"];

            $arr_allboardlist[]=$arr_boardlist;
        }
        $data["list"]=$arr_allboardlist;
        $data["calmainmst_code"]=$calmainmst_code;

        $this->load->view('calculate/allog', array('data'=>$data));
        $this->load->view('footer');
    }

}
