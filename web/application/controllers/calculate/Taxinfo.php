<?php
/**
 * Taxinfo.php - 팀오투 세금계산서/ 정산정보, 사업자등록증 정보 등록,수정
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class Taxinfo extends CI_Controller
{

    private $BOARD_CODE, $ARR_PERMISSION, $LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('calculate/Taxinfo_model');
        $this->LOGINID = $this->session->userdata('admin_id');

        if ($this->session->userdata('admin_id') == "") {
            echo "<script>location.href='/adminmanage/Login'</script>";
            exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION = $this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);
        if($ptype=="") $ptype = $this->input->post('ptype', TRUE);

        switch ($ptype) {
            case "mailform":
                $this->loadCalculateconfig();
                break;
            case "mycompany":
                $this->loadCalculateconfig();
                break;
            case "mailproc":
                $this->procmail();
                break;
            case "taxproc":
                $this->proctaxinfo();
                break;
            case "taxpaperproc":
                $this->proctaxpaper();
                break;

        }
    }

    // 팀오투 세금계산서 발행 정보 등록
    public function proctaxpaper(){

        $data["taxkey"] = "tax".time();
        $data["mailtitle"] = $this->input->post('mailtitle', TRUE);
        $data["mycorpnum"] = $this->input->post('mycorpnum', TRUE);
        $data["mycorpname"] = $this->input->post('mycorpname', TRUE);
        $data["myceoname"] = $this->input->post('myceoname', TRUE);
        $data["myaddr"] = $this->input->post('myaddr', TRUE);
        $data["myemail"] = $this->input->post('myemail', TRUE);
        $data["amounttotal"] = $this->input->post('amounttotal', TRUE);
        $data["vattotal"] = $this->input->post('vattotal', TRUE);
        $data["totalamount"] = $this->input->post('totalamount', TRUE);
        $data["clientcorpnum"] = $this->input->post('clientcorpnum', TRUE);
        $data["clientcorpname"] = $this->input->post('clientcorpname', TRUE);
        $data["clientceoname"] = $this->input->post('clientceoname', TRUE);
        $data["clientcontactname"] = $this->input->post('clientcontactname', TRUE);
        $data["clientaddr"] = $this->input->post('clientaddr', TRUE);
        $data["clientbiztype"] = $this->input->post('clientbiztype', TRUE);
        $data["clientbizclass"] = $this->input->post('clientbizclass', TRUE);
        $data["stats"] = $this->input->post('stats', TRUE);
        $data["regdate"] = $this->input->post('regdate', TRUE);


        $return_v = $this->Taxinfo_model->setaxpaperlog( $data);

        echo "<script>location.href='/calculate/Taxpaperlog'</script>";
        exit();
    }


    // 정산 메일 내용 등록,수정처리
    public function procmail(){
        $data["emailtitle"] = $this->input->post('emailtitle', TRUE);
        $data["emailform"] = $this->input->post('emailform', TRUE);

        $return_v = $this->Taxinfo_model->setemailform( $data);
        echo "<script>location.href='/calculate/Taxinfo?ptype=mailform'</script>";
        exit();

    }


    // 팀오투 사업자등록증 정보 등록
    public function proctaxinfo(){

        $data["biztype"] = $this->input->post('biztype', TRUE);
        $data["bizclass"] = $this->input->post('bizclass', TRUE);
        $data["company_name"] = $this->input->post('company_name', TRUE);
        $data["ceo_name"] = $this->input->post('ceo_name', TRUE);
        $data["addr"] = $this->input->post('addr', TRUE);
        $data["tax_email"] = $this->input->post('tax_email', TRUE);
        $data["company_number"] = $this->input->post('company_number', TRUE);

        $return_v = $this->Taxinfo_model->setaxform( $data);

        echo "<script>location.href='/calculate/Taxinfo?ptype=mycompany'</script>";
        exit();

    }

    // 팀오투 세금계산서 정보 로드,이메일 내용 정보로드
    public function loadCalculateconfig(){

        $ptype = $this->input->get('ptype', TRUE);
        $arr_data = $this->Taxinfo_model->getCalculateconfig();
        $data["company_name"] =$arr_data["company_name"];
        $data["ceo_name"] =$arr_data["ceo_name"];
        $data["addr"] =$arr_data["addr"];

        $data["biztype"]  =$arr_data["biztype"];
        $data["bizclass"]  =$arr_data["bizclass"];


        $data["tax_email"] =$arr_data["tax_email"];
        $data["emailtitle"] =$arr_data["emailtitle"];
        $data["emailform"] =$arr_data["emailform"];
        $data["company_number"] =$arr_data["company_number"];

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        if($ptype=="mailform"){
            $this->load->view("calculate/calculatemailform", array('data'=>$data));
        }else if($ptype=="mycompany"){
            $this->load->view("calculate/mycompanyinfo", array('data'=>$data));
        }
        $this->load->view('footer');



    }

}