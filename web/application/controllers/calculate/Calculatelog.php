<?php
/**
 * Calculatelog.php - 정산내역 리스트, 정산취소,신규정산,세금계산서등록,정산내역이동,추가,삭제 등 컨트롤러
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class Calculatelog extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION;

    function __construct()
    {
        parent::__construct();

        $this->BOARD_CODE="notice";

        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->library('Aws_ses');

        $this->load->model('calculate/Calculate_model');
        $this->load->model('calculate/Taxinfo_model');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);
        if($ptype=="") $ptype = $this->input->post('ptype', TRUE);

        switch($ptype) {
            case "addcalbranch":$this->addcalbranch();break;
            case "savecalmemo": $this->savecalmemo();break;
            case "setnewcaldata" : $this->setnewcaldata(); break;
            case "settaxpapercomplete" : $this->settaxpapercomplete(); break;
            case "delcalculate" : $this->delcalculate(); break;
            case "emailbody" : $this->setemailbody(); break;
            case "transcopy" : $this->loadTranscopy(); break;
            case "finalcheck" : $this->loadFinalcheck(); break;
            case "allcalculatecomplete" : $this->allcalculatecomplete(); break;
            case "setpaycomplete" : $this->setpaycomplete(); break;
            case "calculatecomplete" : $this->calculatecomplete(); break;
            case "cancelcalculate" : $this->cancelcalculate(); break;
            case "changedtlrow" : $this->changedtlrow(); break;
            case "changebranch" : $this->changebranch(); break;
            case "v" : $this->loadCalculatelogView(); break;
            case "w" : $this->loadCalculatelogWrite(); break;
            default : $this->loadCalculatelogList(); break;
        }
    }


    // 각 정산에서 신규 정산 지점등록
    public function addcalbranch(){


        $calmainmst_code = $this->input->post('calmainmst_code', TRUE);
        $calmaindtl_company = $this->input->post('calmaindtl_company', TRUE);
        $calmaindtl_branch = $this->input->post('calmaindtl_branch', TRUE);


        $returnarr = $this->Calculate_model->addcalbranch($calmainmst_code,$calmaindtl_company,$calmaindtl_branch);

        echo  json_encode($returnarr);
    }




    // 각 지점별 정산 메모 등록수정
    public function savecalmemo(){


        $calmainmst_code = $this->input->post('calmainmst_code', TRUE);
        $calmaindtl_branch = $this->input->post('calmaindtl_branch', TRUE);
        $calmemo= $this->input->post('calmemo', TRUE);

         
        $this->Calculate_model->savecalmemo($calmainmst_code,$calmaindtl_branch,$calmemo);
    }

    // 세금계산서 발행 처리파일
    // 루트의 Popbill 폴더안의 RegistIssue 파일이 세금계산서 발행 파일임
    public function settaxpapercomplete(){

        // 1. calculate_taxpaperlog 테이블에 stats = w 으로 등록
        $data["calmainmst_code"] = $this->input->post('calmainmst_code', TRUE);
        $data["strcalbranch_idx"] = $this->input->post('strcalbranch_idx', TRUE);

        $this->Calculate_model->settaxoffer($data);

        // http://workspace.teamo2.kr/Popbill/RegistIssue.php 호출하기
         $this->customfunc->Popbillexec();
    }


    // 각 회사,지점별 신규 정산 데이터 갱신
    public function setnewcaldata(){


        $data["calmainmst_code"] = $this->input->post('calmainmst_code', TRUE);
        $data["calmaindtl_company"]  = $this->input->post('calmaindtl_company', TRUE);
        $data["calmaindtl_branch"]  = $this->input->post('calmaindtl_branch', TRUE);

        $data["calmaindtl_gubn"]  = $this->input->post('calmaindtl_gubn', TRUE);
        $data["calmaindtl_period"]  = $this->input->post('calmaindtl_period', TRUE);
        $data["driver_name"]  = $this->input->post('driver_name', TRUE);
        $data["calmaindtl_rentstart"]  = $this->input->post('calmaindtl_rentstart', TRUE);
        $data["calmaindtl_rentend"]  = $this->input->post('calmaindtl_rentend', TRUE);
        $data["car_model"]  = $this->input->post('car_model', TRUE);
        $data["rentcar_number"]  = $this->input->post('rentcar_number', TRUE);
        $data["calmaindtl_totalprice"]  = $this->input->post('calmaindtl_totalprice', TRUE);
        $data["calmaindtl_calamount"]  = $this->input->post('calmaindtl_calamount', TRUE);
        $data["calmaindtl_rentfee"]  = $this->input->post('calmaindtl_rentfee', TRUE);
        $data["calmaindtl_discount"]  = $this->input->post('calmaindtl_discount', TRUE);

        $this->Calculate_model->setnewcaldata($data);

    }


    // 각 회사,지점별 정산데이터 선택삭제
    public function delcalculate(){

        $calmainmst_code = $this->input->post('calmainmst_code', TRUE);
        $calmaindtl_branch = $this->input->post('calmaindtl_branch', TRUE);


        $strcalmaindtl_idx= $this->input->post('strcalmaindtl_idx', TRUE);
        $arrcalmaindtl_idx =explode("-",$strcalmaindtl_idx );


        foreach ($arrcalmaindtl_idx as $entry) {

            $calmaindtl_idx= $entry;
            $this->Calculate_model->delcalmaindtlinfo($calmaindtl_idx);
        }

        // 지점별 데이터 종합 갱신
        $this->Calculate_model->updatecalbranchcnt($calmainmst_code,$calmaindtl_branch);

    }

    // 고객사 발송용 정산 이메일 내용 세팅
    public function setemailbody(){
        $requestdata["calmainmst_code"] = $this->input->post('calmainmst_code', TRUE);
        $requestdata["registration_number"] = $this->input->post('registration_number', TRUE);

        $registration_number = $this->input->post('registration_number', TRUE);
        $calmainmst_code = $this->input->post('calmainmst_code', TRUE);
        $calmaindtl_branch =$this->input->post('calmaindtl_branch', TRUE);
        $arr_calmain = $this->Calculate_model->getCalmaininfo($calmainmst_code);

        $calmainmst_startdate = str_replace("-",".", $arr_calmain["calmainmst_startdate"]) ;
        $calmainmst_enddate = str_replace("-",".",$arr_calmain["calmainmst_enddate"]);
        $period= $calmainmst_startdate."~".$calmainmst_enddate;


        $arr_data = $this->Taxinfo_model->getCalculateconfig();

        $data["tax_email"] =$arr_data["tax_email"];
        $origin_emailtitle =$arr_data["emailtitle"];
        $origin_emailform =$arr_data["emailform"];

        $arr_companyinfo=$this->Calculate_model->getcalbranchcompanyinfoByBranchcode($calmaindtl_branch);
        $company_name =$arr_companyinfo["company_name"];
        $cal_email = $arr_companyinfo["cal_email"];
        $cal_email= "eric@teamo2.kr";

        $title = str_replace("{period}",$period,$origin_emailtitle);
        $title = str_replace("{company_name}",$company_name,$title);


        //Taxinfo?calmainmst_code={calmainmst_code}&amp;calmaindtl_branch={calmaindtl_branch}
        $dtlink = "http://paper.teamo2.kr/calculate/CalculateDetail?calmainmst_code=$calmainmst_code&calmaindtl_branch=$calmaindtl_branch";
        $content = str_replace("{period}",$period,$origin_emailform);
        $content = str_replace("{company_name}",$company_name,$content);
        $content = str_replace("{dtlink}",$dtlink,$content);

        $data["email"] =$cal_email;
        $data["title"] =$title;
        $data["content"] =$content;

        $this->load->view('calculate/emailbody', array('data'=>$data));
    }


    // 각 지점별 정산 완료 진행 - 메일발송
    public function setpaycomplete(){

        $calmainmst_code = $this->input->post('calmainmst_code', TRUE);
        $arr_calmain = $this->Calculate_model->getCalmaininfo($calmainmst_code);

        $calmainmst_startdate = str_replace("-",".", $arr_calmain["calmainmst_startdate"]) ;
        $calmainmst_enddate = str_replace("-",".",$arr_calmain["calmainmst_enddate"]);
        $period= $calmainmst_startdate."~".$calmainmst_enddate;


        $arr_data = $this->Taxinfo_model->getCalculateconfig();

        $data["tax_email"] =$arr_data["tax_email"];
        $origin_emailtitle =$arr_data["emailtitle"];
        $origin_emailform =$arr_data["emailform"];


        $strcalbranch_idx = $this->input->post('strcalbranch_idx', TRUE);
        $arrcalbranch_idx =explode("-",$strcalbranch_idx );


        foreach ($arrcalbranch_idx as $entry){

            $calbranch_idx = $entry;

            $arr_companyinfo=$this->Calculate_model->getcalbranchcompanyinfo($calbranch_idx);

            $company_name = $arr_companyinfo["company_name"];
            $registration_number= $arr_companyinfo["registration_number"];
            $calmaindtl_branch= $arr_companyinfo["branch_serial"];
            $cal_email = $arr_companyinfo["cal_email"];
            $cal_email2 = $arr_companyinfo["cal_email2"];


            $title = str_replace("{period}",$period,$origin_emailtitle);
            $title = str_replace("{company_name}",$company_name,$title);


            //Taxinfo?calmainmst_code={calmainmst_code}&amp;calmaindtl_branch={calmaindtl_branch}
            $dtlink = "http://paper.teamo2.kr/calculate/CalculateDetail?calmainmst_code=$calmainmst_code&calmaindtl_branch=$calmaindtl_branch";
            $content = str_replace("{period}",$period,$origin_emailform);
            $content = str_replace("{company_name}",$company_name,$content);
            $content = str_replace("{dtlink}",$dtlink,$content);


            $arr_dest= (empty($cal_email2))? array($cal_email) : [$cal_email, $cal_email2];
            /*
            print_r($arr_dest);
            echo $title ."<BR>";
            echo $content."<BR>" ;
            echo "<hr>";
            */
            // 이메일 발송
            $ses = $this->aws_ses->sendCalculateEmail($arr_dest,$title,$content);


            //입금확인 완료하기
            $this->Calculate_model->setcaculatecalbranchstats($calbranch_idx,'bankins_yn','y');


        }
    }


    // 은행이체용 엑셀파일 만드는 역할 진행
    public function loadTranscopy(){
        $requestdata["calmainmst_code"] = $this->input->post('calmainmst_code', TRUE);
        $requestdata["strcalbranch_idx"] = $this->input->post('strcalbranch_idx', TRUE);

        $result = $this->Calculate_model->getranscopylist($requestdata);

        $arr_allboardlist=[];
        foreach($result as $entry)
        {

            $arr_boardlist["bankinfo"] =$entry["bankinfo"];
            $arr_boardlist["bankaccount"] =$entry["bankaccount"];
            $arr_boardlist["sumcalamount"] =$entry["sumcalamount"];
            $arr_boardlist["calmainmst_title"] =$entry["calmainmst_title"];
            $arr_boardlist["mytxt"] =$entry["mytxt"];

            $arr_allboardlist[]=$arr_boardlist;
        }
        $data["list"]=$arr_allboardlist;

        $this->load->view('calculate/transcopy', array('data'=>$data));

    }


    // 전체 기업,지점 정산 진행
    public function allcalculatecomplete(){
        $data["calmainmst_code"] = $this->input->post('calmainmst_code', TRUE);
        $result = $this->Calculate_model->allcalculatecomplete($data);
    }


    // 선택 기업,지점 정산 진행
    public function calculatecomplete(){
        $data["calmaindtl_idx"] = $this->input->post('calmaindtl_idx', TRUE);
        $data["calmainmst_code"] = $this->input->post('calmainmst_code', TRUE);
        $data["calmaindtl_branch"] = $this->input->post('calmaindtl_branch', TRUE);

        $result = $this->Calculate_model->calculatecomplete($data);
    }


    // 선택한 정산 내역 정산취소
    public function cancelcalculate(){
        $data["calmaindtl_idx"] = $this->input->post('calmaindtl_idx', TRUE);
        $data["calmainmst_code"] = $this->input->post('calmainmst_code', TRUE);

        $result = $this->Calculate_model->calcelcalculate($data);
    }


    // 선택한 정산 정보의 상세 데이터 변경
    public function changedtlrow()
    {
        $data["calmaindtl_idx"] = $this->input->post('calmaindtl_idx', TRUE);
        $data["calmaindtl_totalprice"] = $this->input->post('calmaindtl_totalprice', TRUE);
        $data["calmaindtl_calamount"] = $this->input->post('calmaindtl_calamount', TRUE);
        $data["calmaindtl_rentfee"] = $this->input->post('calmaindtl_rentfee', TRUE);

        $data["car_model"] = $this->input->post('car_model', TRUE);
        $data["rentcar_number"] = $this->input->post('rentcar_number', TRUE);

        $result = $this->Calculate_model->changedtlrow($data);
    }


    // 선택한 정산 정보의 지점 이동
    public function changebranch(){
        $data["selectbranch"] = $this->input->post('selectbranch', TRUE);
        $data["nowbranch"] = $this->input->post('nowbranch', TRUE);
        $data["calmainmst_code"] = $this->input->post('calmainmst_code', TRUE);
        $data["strcalmaindtl_idx"] = $this->input->post('strcalmaindtl_idx', TRUE);

        $result = $this->Calculate_model->changebranch($data);
    }


    // 정산 완료후 세금계산서 발행내역 페이지 로딩
    public function loadFinalcheck(){
        $calmainmst_code= $this->input->get('calmainmst_code', TRUE);
        $arr_data = $this->Calculate_model->taxbanklist($calmainmst_code);

        $arr_allboardlist=[];
        foreach($arr_data as $entry)
        {

            $arr_boardlist["companyname"] =$entry["companyname"];
            $arr_boardlist["branchname"] =$entry["branchname"];
            $arr_boardlist["branchserial"] =$entry["branchserial"];
            $arr_boardlist["companyserial"] =$entry["companyserial"];
            $arr_boardlist["calbranch_idx"] =$entry["calbranch_idx"];
            $arr_boardlist["calmainmst_code"] =$entry["calmainmst_code"];
            $arr_boardlist["calmaindtl_branch"] =$entry["calmaindtl_branch"];
            $arr_boardlist["dtlcnt"] =$entry["dtlcnt"];
            $arr_boardlist["calculate_yn"] =$entry["calculate_yn"];
            $arr_boardlist["bankins_yn"] =$entry["bankins_yn"];
            $arr_boardlist["taxmail_yn"] =$entry["taxmail_yn"];
            $arr_boardlist["tax_msg"] =$entry["tax_msg"];

            if($entry["bankins_yn"]=="y"){
                $arr_boardlist["bankins_ynstr"]="<span style='color:blue'>입금완료</span>";
            }else{
                $arr_boardlist["bankins_ynstr"]="<span style='color:red'>입금대기</span>";
            }
            if($entry["taxmail_yn"]=="y"){
                $arr_boardlist["taxmail_ynstr"]="<span style='color:blue'>발행완료</span>";
            }else if($entry["taxmail_yn"]=="x"){
                $arr_boardlist["taxmail_ynstr"]="<span style='color:red'>발행오류</span>";
            }else if($entry["taxmail_yn"]=="n"){
            $arr_boardlist["taxmail_ynstr"]="<span style='color:gray'>발행대기</span>";
        }


            $arr_boardlist["sumtotalprice"] =$entry["sumtotalprice"];
            $arr_boardlist["sumcalamount"] = $entry["sumcalamount"];
            $arr_boardlist["sumrentfee"] = $entry["sumrentfee"];
            $arr_boardlist["sumdiscount"] =$entry["sumdiscount"];


            $arr_boardlist["sumtotalpricestr"] =number_format( $entry["sumtotalprice"],0);
            $arr_boardlist["sumcalamountstr"] =number_format( $entry["sumcalamount"],0);
            $arr_boardlist["sumrentfeestr"] = number_format( $entry["sumrentfee"],0);
            $arr_boardlist["sumdiscountstr"] = number_format( $entry["sumdiscount"],0);


            $taxdtl = $this->Taxinfo_model->getBranchTaxinfoDetail($entry["calmaindtl_branch"]);

            $arr_boardlist["registration_number"]=$taxdtl->registration_number;
            $arr_boardlist["tcompany_name"]=$taxdtl->company_name;
            $arr_boardlist["addr"]=$taxdtl->addr;
            $arr_boardlist["bizpart"]=$taxdtl->bizpart;
            $arr_boardlist["cal_email"]=$taxdtl->cal_email;
            $arr_boardlist["tax_email"]=$taxdtl->tax_email;
            $arr_boardlist["bankaccount"]=$taxdtl->bankaccount;

            $arr_boardlist["bankinfo"]=$taxdtl->bankinfo;
            $arr_boardlist["ceo_name"]=$taxdtl->ceo_name;


            ///// null check - 세금계산서 정보없으면 정산불가
            if($arr_boardlist["registration_number"]=="" || $arr_boardlist["tax_email"]=="" ||
                $arr_boardlist["cal_email"]=="" ||   $arr_boardlist["bankinfo"]=="" 
                ||   $arr_boardlist["bankaccount"]=="" ){

                $arr_boardlist["calbranch_idx"] ="x";

            }


            $arr_boardlist["regdate"] =$entry["regdate"];

            $arr_allboardlist[]=$arr_boardlist;
        }
        $data["list"]=$arr_allboardlist;
        $data["calmainmst_code"]=$calmainmst_code;

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('calculate/calstep3', array('data'=>$data));
        $this->load->view('footer');
    }


    // 정산내역 마스터 정보 리스트 페이지 로딩
    public function loadCalculatelogList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $arr_AdminBoarddata = $this->Calculate_model->getCalmstList($page,20,$sp,$sv);
        $total = $this->Calculate_model->getCalmstCount($this->BOARD_CODE, $sp,$sv);

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/calculate/Calculatelog";
        $config['suffix']     ="&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_allboardlist=[];
        foreach($arr_AdminBoarddata as $entry)
        {
            $arr_boardlist["calmainmst_code"] =$entry["calmainmst_code"];
            $arr_boardlist["calmainmst_startdate"] =$entry["calmainmst_startdate"];
            $arr_boardlist["calmainmst_enddate"] =$entry["calmainmst_enddate"];
            $calmainmst_step =$entry["calmainmst_step"];

            if($calmainmst_step=="step1") {
                $calmainmst_stepstr = "Step1 : 정산시작일/마감일 설정";
            }else if($calmainmst_step=="step2"){
                $calmainmst_stepstr="Step2 : 정산 데이터 확인 ";
            }else if($calmainmst_step=="step3"){
                $calmainmst_stepstr="Step3 : 계산서발행/입금 ";
            }else if($calmainmst_step=="step4"){
                $calmainmst_stepstr="Step4 : 정산/입금/세금완료 ";
            }

            $arr_boardlist["calmainmst_step"] =$calmainmst_step;
            $arr_boardlist["calmainmst_stepstr"] =$calmainmst_stepstr;
            $arr_boardlist["calmainmst_idx"] =$entry["calmainmst_idx"];
            $arr_boardlist["calmainmst_title"] =$entry["calmainmst_title"];

            $arr_boardlist["regdate"] =$entry["regdate"];

            $arr_allboardlist[]=$arr_boardlist;
        }
        $data["list"]=$arr_allboardlist;
        $data["per_page"]=$per_page;
        $data["pagination"]=$pagination;

        $this->load->view('calculate/calculateloglist', array('data'=>$data,'permission'=>$this->ARR_PERMISSION));
        $this->load->view('footer');
    }


    // 정산내역 마스터 정보 등록,수정 페이지 로딩
    public function loadCalculatelogWrite()
    {
        $board_idx = $this->input->get('board_idx', TRUE);

        if($board_idx !=""){
            $data["emode"]="edit";
            $arr_AdminBoarddata = $this->AdminBoard_model->getAdminBoardDetail($board_idx);

            $data["board_idx"] =$arr_AdminBoarddata["board_idx"];
            $data["content_code"] =$arr_AdminBoarddata["content_code"];
            $data["startdate"]= $arr_AdminBoarddata["startdate"];
            $data["enddate"] =$arr_AdminBoarddata["enddate"];
            $data["starttime"] =$arr_AdminBoarddata["starttime"];
            $data["endtime"] =$arr_AdminBoarddata["endtime"];
            $data["board_title"] =$arr_AdminBoarddata["board_title"];
            $data["board_code"] =$arr_AdminBoarddata["board_code"];
            $data["board_content"] =$arr_AdminBoarddata["board_content"];
            $data["board_part"] =$arr_AdminBoarddata["board_part"];
            $data["mem_id"] =$arr_AdminBoarddata["mem_id"];
            $data["readcnt"] =$arr_AdminBoarddata["readcnt"];
            $data["regdate"] =$arr_AdminBoarddata["regdate"];
            $data["content_type"] =$arr_AdminBoarddata["content_type"];
        }else{

            $data["emode"]="new";
            $data["board_idx"] ="";
            $data["content_code"]= $this->customfunc->get_contentcode($this->BOARD_CODE);
            $data["startdate"]= "";
            $data["enddate"] ="";
            $data["starttime"] ="";
            $data["endtime"] ="";
            $data["board_title"] ="";
            $data["board_code"] =$this->BOARD_CODE;
            $data["board_content"] ="";
            $data["board_part"] ="";
            $data["mem_id"] ="";
            $data["readcnt"] ="";
            $data["regdate"] ="";
            $data["content_type"] ="";

        }

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('calculate/calculatelogwrite', array('data'=>$data));
        $this->load->view('footer');
    }

    // 정산내역 마스터 정보 View 로딩
    public function loadCalculatelogView()
    {
        $calmainmst_code= $this->input->get('calmainmst_code', TRUE);
        $calmaindtl_branch= $this->input->get('calmaindtl_branch', TRUE);

        $data["emode"]="edit";
        $arr_data = $this->Calculate_model->getCalinfoDetail($calmainmst_code, $calmaindtl_branch );
        $data["mst"] = $arr_data["mst"][0];
        $data["list"] = $arr_data["list"];
        $data["dtl"] = $arr_data["dtl"];
        $data["calbranchdtl"] = $arr_data["calbranchdtl"][0];

        $data["sumdata"]=$arr_data["sumdata"];
        $data["calmainmst_code"] =$calmainmst_code;
        $data["calmaindtl_branch"] = $arr_data["calmaindtl_branch"];
        $data["rentCompany_serial"] = $arr_data["rentCompany_serial"];
        $data["settlement_rental_rate"] = $arr_data["settlement_rental_rate"];
        $data["select_company"]=$this->customfunc->select_company('');


        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('calculate/calstep2', array('data'=>$data));
        $this->load->view('footer');
    }
}
