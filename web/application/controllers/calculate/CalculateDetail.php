<?php
/**
 * CalculateDetail.php - 정산 기준서 상세내역
 */
defined('BASEPATH') OR exit('No direct script access allowed');

//http://workspace2.teamo2.kr/calculate/CalculateDetail?ptype=v&calmainmst_code=CMS1551248976&calmaindtl_branch=509
class CalculateDetail extends CI_Controller
{
    private $BOARD_CODE,$ARR_PERMISSION;

    function __construct()
    {
        parent::__construct();

        $this->BOARD_CODE="notice";

        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->library('Aws_ses');

        $this->load->model('calculate/Calculate_model');
        $this->load->model('calculate/Taxinfo_model');

    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);
        if($ptype=="") $ptype = $this->input->post('ptype', TRUE);

        switch($ptype) {
            default : $this->loadCalculatelogView(); break;
        }
    }

    // 정산기준서 상세내역 보기페이지
    public function loadCalculatelogView()
    {
        $calmainmst_code= $this->input->get('calmainmst_code', TRUE);
        $registration_number = $this->input->get('registration_number', TRUE);
        $calmaindtl_branch = $this->input->get('calmaindtl_branch', TRUE);

        $data["emode"]="edit";
        $arr_data = $this->Calculate_model->getCalinfoDetailByBranchcode($calmainmst_code, $calmaindtl_branch );
        $data["mst"] = $arr_data["mst"][0];
        $data["list"] = $arr_data["list"];
        $data["dtl"] = $arr_data["dtl"];
        $data["calbranchdtl"] = $arr_data["calbranchdtl"][0];

        $data["sumdata"]=$arr_data["sumdata"];
        $data["calmainmst_code"] =$calmainmst_code;
        $data["registration_number"] =$registration_number;
        $data["calmaindtl_branch"] = $arr_data["calmaindtl_branch"];
        $data["settlement_rental_rate"] = $arr_data["settlement_rental_rate"];

        $arr_companyinfo=$this->Calculate_model->getcalbranchdtlByBranchcode($calmainmst_code,$calmaindtl_branch);


        $data["company_name"] = $arr_companyinfo["company_name"];



         $this->load->view('calculate/caldtl', array('data'=>$data));
     }



}