<?php
/**
 * Taxpaperlog.php - 팀오투 전자세금계산서 발행내역 (사용안함)
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class Taxpaperlog extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION;

    function __construct()
    {
        parent::__construct();

        $this->BOARD_CODE="notice";

        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('calculate/Taxinfo_model');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);

        switch($ptype) {
            case "v" : $this->loadTaxpaperlogView(); break;
            case "w" : $this->loadTaxpaperlogWrite(); break;
            default : $this->loadTaxpaperlogList(); break;
        }

    }

    public function loadTaxpaperlogList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $arr_AdminBoarddata = $this->Taxinfo_model->getTaxpaperlogList($page,20,$sp,$sv);
        $total = $this->Taxinfo_model->getTaxpaperlogCount($this->BOARD_CODE, $sp,$sv);

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/adminmanage/AdminBoardManage";
        $config['suffix']     ="&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_allboardlist=[];
        foreach($arr_AdminBoarddata as $entry)
        {
            $arr_boardlist["board_idx"] =$entry->board_idx;
            $arr_boardlist["content_code"] =$entry->content_code;
            $arr_boardlist["startdate"]= $entry->startdate;
            $arr_boardlist["enddate"] =$entry->enddate;
            $arr_boardlist["starttime"] =$entry->starttime;
            $arr_boardlist["endtime"] =$entry->endtime;
            $arr_boardlist["board_title"] =$entry->board_title;
            $arr_boardlist["board_code"] =$this->BOARD_CODE ;
            $arr_boardlist["board_content"] =$entry->board_content;
            $arr_boardlist["mem_id"] =$entry->mem_id;
            $arr_boardlist["reply_cnt"] =$entry->reply_cnt;
            $arr_boardlist["mem_name"] =$entry->mem_name;
            $arr_boardlist["confirm_name"] =$entry->confirm_name;
            $arr_boardlist["agree_yn"] =$entry->agree_yn;


            $arr_boardlist["regdate"] =$this->customfunc->get_dateformat($entry->regdate);
            $arr_boardlist["contentype_str"] =$entry->contentype_str;

            $arr_allboardlist[]=$arr_boardlist;
        }
        $data["list"]=$arr_allboardlist;
        $data["per_page"]=$per_page;
        $data["pagination"]=$pagination;

        $this->load->view('calculate/taxpaperloglist', array('data'=>$data,'permission'=>$this->ARR_PERMISSION));
        $this->load->view('footer');
    }

    public function loadTaxpaperlogWrite()
    {
        $board_idx = $this->input->get('board_idx', TRUE);

        if($board_idx !=""){
            $data["emode"]="edit";
            $arr_AdminBoarddata = $this->AdminBoard_model->getAdminBoardDetail($board_idx);

            $data["board_idx"] =$arr_AdminBoarddata["board_idx"];
            $data["content_code"] =$arr_AdminBoarddata["content_code"];
            $data["startdate"]= $arr_AdminBoarddata["startdate"];
            $data["enddate"] =$arr_AdminBoarddata["enddate"];
            $data["starttime"] =$arr_AdminBoarddata["starttime"];
            $data["endtime"] =$arr_AdminBoarddata["endtime"];
            $data["board_title"] =$arr_AdminBoarddata["board_title"];
            $data["board_code"] =$arr_AdminBoarddata["board_code"];
            $data["board_content"] =$arr_AdminBoarddata["board_content"];
            $data["board_part"] =$arr_AdminBoarddata["board_part"];
            $data["mem_id"] =$arr_AdminBoarddata["mem_id"];
            $data["readcnt"] =$arr_AdminBoarddata["readcnt"];
            $data["regdate"] =$arr_AdminBoarddata["regdate"];
            $data["content_type"] =$arr_AdminBoarddata["content_type"];
        }else{

            $data["emode"]="new";
            $data["board_idx"] ="";
            $data["content_code"]= $this->customfunc->get_contentcode($this->BOARD_CODE);
            $data["startdate"]= "";
            $data["enddate"] ="";
            $data["starttime"] ="";
            $data["endtime"] ="";
            $data["board_title"] ="";
            $data["board_code"] =$this->BOARD_CODE;
            $data["board_content"] ="";
            $data["board_part"] ="";
            $data["mem_id"] ="";
            $data["readcnt"] ="";
            $data["regdate"] ="";
            $data["content_type"] ="";

        }

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('calculate/taxpaperlogwrite', array('data'=>$data));
        $this->load->view('footer');
    }

    public function loadTaxpaperlogView()
    {
        $board_idx = $this->input->get('board_idx', TRUE);

        $data["emode"]="edit";
        $arr_AdminBoarddata = $this->AdminBoard_model->getAdminBoardDetail($board_idx);

        $data["board_idx"] =$arr_AdminBoarddata["board_idx"];
        $data["content_code"] =$arr_AdminBoarddata["content_code"];
        $data["startdate"]= $arr_AdminBoarddata["startdate"];
        $data["enddate"] =$arr_AdminBoarddata["enddate"];
        $data["board_title"] =$arr_AdminBoarddata["board_title"];
        $data["board_code"] =$arr_AdminBoarddata["board_code"];
        $data["board_content"] =$arr_AdminBoarddata["board_content"];
        $data["board_part"] =$this->customfunc->get_notice_type($arr_AdminBoarddata["board_part"]);
        $data["mem_id"] =$arr_AdminBoarddata["mem_id"];
        $data["readcnt"] =$arr_AdminBoarddata["readcnt"];
        $data["contentype_str"] =$arr_AdminBoarddata["contentype_str"];
        $data["regdate"] =$arr_AdminBoarddata["regdate"];
        $data["wauth"]=$this->customfunc->get_writeauth($this->session->userdata('admin_id'),$data["mem_id"],$this->session->userdata('master_yn'));
        $data["per_page"] = $this->input->get('per_page', TRUE);

        $arr_AdminBoardCommentdata=$this->AdminBoard_model->getBoardReplyList($data["board_code"],$board_idx);
        $arr_allboardcommentlist=[];
        foreach($arr_AdminBoardCommentdata as $entry) {
            $arr_boardcomlist["board_idx"] =$entry->board_idx;
            $arr_boardcomlist["board_code"] =$entry->board_code;
            $arr_boardcomlist["mem_id"] =$entry->mem_id;
            $arr_boardcomlist["regdate"] =  $this->customfunc->get_dateformat($entry->regdate);
            $arr_boardcomlist["commenttext"] =$entry->commenttext;
            $arr_boardcomlist["comment_idx"] =$entry->comment_idx;
            $arr_boardcomlist["mem_name"] =$entry->mem_name;
            $arr_boardcomlist["wauth"] =$this->customfunc->get_writeauth($this->session->userdata('admin_id'),$arr_boardcomlist["mem_id"],$this->session->userdata('master_yn'));
            $arr_allboardcommentlist[]=$arr_boardcomlist;
        }

        $data["commentlist"] =$arr_allboardcommentlist;

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('adminmanage/adminboardview', array('data'=>$data));
        $this->load->view('footer');
    }
}
