<?php
/**
 * TeamManage.php - 관리자 팀 정보 관리 컨트롤러 리스트,등록,수정처리
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class TeamManage extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION;

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('adminmanage/Adminmember_model');

        if ($this->session->userdata('admin_id') == "") {
            echo "<script>location.href='/adminmanage/Login'</script>";
            exit();
        }

        $pageauth =$this->customfunc->get_pageauth("master");

        if ( $pageauth != "y") {
            echo "<script>location.href='/carmore/Dashboard'</script>";
            exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        switch($ptype) {
            case "w" : $this->loadAdminTeamWrite(); break;
            default : $this->loadAdminTeamList(); break;
        }
        $this->load->view('footer');
    }

    // 관리자 팀 리스트 페이지 호출
    public function loadAdminTeamList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);
        $arr_adminteamdata = $this->Adminmember_model->getAdminTeamInfo("list","");

        $data["list"]=$arr_adminteamdata;
        $this->load->view('adminmanage/teamlist', array('data'=>$data));

    }

    // 관리자 팀 정보 등록,수정 페이지 호출
    public function loadAdminTeamWrite()
    {

        $this->load->library('form_validation');
        $team_code = $this->input->get('team_code', TRUE);

        if($team_code !=""){
            $data["emode"]="edit";
            $arr_adminteamdata = $this->Adminmember_model->getAdminTeamInfo("d",$team_code);

            $data["team_code"] =$arr_adminteamdata["team_code"];
            $data["team_name"] =$arr_adminteamdata["team_name"];

        }else{
            $data["emode"]="new";

            $data["team_code"] ="";
            $data["team_name"] ="";
        }
        $this->load->view('adminmanage/teamwrite', array('data'=>$data));
    }

}
