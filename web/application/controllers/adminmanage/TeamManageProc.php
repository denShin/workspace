<?php
/**
 * TeamManageProc.php - 관리자 팀 정보 수정,삭제등록 처리
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class TeamManageProc extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('adminmanage/Adminmember_model');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        $demode =$this->input->get('emode');

        switch($demode) {
            case "del" : $this->delAdminTeamInfo(); break;
            default : $this->setAdminTeamInfo(); break;
        }
    }

    // 팀정보 수정,등록
    public function setAdminTeamInfo()
    {
        $this->load->library('form_validation');

        $data["emode"] =$this->input->post('emode');
        $data["team_code"] =$this->input->post('team_code');
        $data["team_name"] =$this->input->post('team_name');


        if($this->form_validation->run() === false){

            echo "<script>alert('데이터 기입 오류입니다..');history.back();</script>";
            exit();
        } else {
            $return_v = $this->Adminmember_model->procAdminTeam( $data["emode"] ,$data);
            if($return_v=="e"){
                echo "<script>alert('팀코드 중복.');history.back();</script>";
                exit();
            }else if($return_v=="y"){
                echo "<script>location.href='/adminmanage/TeamManage'</script>";
                exit();
            }

        }
    }

    // 팀정보 삭제 처리
    public function delAdminTeamInfo()
    {
        $data["emode"] =$this->input->get('emode');
        $data["team_code"] =$this->input->get('team_code');

        $return_v = $this->Adminmember_model->procAdminTeam( $data["emode"] ,$data);
        if($return_v=="e"){
            echo "<script>alert('팀 정보 삭제 불가능.');history.back();</script>";
            exit();
        }else if($return_v=="y"){
            echo "<script>location.href='/adminmanage/TeamManage'</script>";
            exit();
        }
    }

}
