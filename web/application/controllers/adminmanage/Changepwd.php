<?php
/**
 * Changepwd.php - 관리자 회원의 비밀번호 변경 처리 컨트롤러
 */
class Changepwd extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->model('adminmanage/Adminmember_model');
        $this->load->library('Customfunc');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {

        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);

        $this->loadChangePwd();
    }


    //비밀번호 변경 페이지 호출
    public function loadChangePwd()
    {

        $pemail = $this->input->get('admin_email', TRUE);

        if($pemail !=""){
            $data["emode"]="edit";
            $arr_adminmemberdata = $this->Adminmember_model->getAdminMemberDetail($pemail);

            $data["admin_name"] =$arr_adminmemberdata["admin_name"];
            $data["admin_email"] =$arr_adminmemberdata["admin_email"];
            $data["admin_pwd"]=$arr_adminmemberdata["admin_pwd"];
            $data["admin_tel"] =$arr_adminmemberdata["admin_tel"];
            $data["team_code"] =$arr_adminmemberdata["team_code"];
            $data["admin_stats"] =$arr_adminmemberdata["admin_stats"];
            $data["member_grade"] =$arr_adminmemberdata["member_grade"];
            $data["config_yn"] =$arr_adminmemberdata["config_yn"];
            $data["offwork_yn"] =$arr_adminmemberdata["offwork_yn"];
            $data["buyoffer_yn"] =$arr_adminmemberdata["buyoffer_yn"];
            $data["dmaster_yn"] =$arr_adminmemberdata["master_yn"];
            $data["buyofferdeposit_yn"] =$arr_adminmemberdata["buyofferdeposit_yn"];
            $data["upmoneyoffer_yn"] =$arr_adminmemberdata["upmoneyoffer_yn"];
            $data["ordermng_yn"] =$arr_adminmemberdata["ordermng_yn"];
            $data["calc_yn"] =$arr_adminmemberdata["calc_yn"];
            $data["teamoption"]= $this->customfunc->select_team($data["team_code"]);

        }else{
            $data["emode"]="new";

            $data["admin_name"] ="";
            $data["admin_email"] ="";
            $data["admin_pwd"]= "";
            $data["admin_tel"] ="";
            $data["team_code"] ="";
            $data["admin_stats"] ="";
            $data["member_grade"] ="";
            $data["config_yn"] ="";
            $data["offwork_yn"] ="";
            $data["buyoffer_yn"] ="";
            $data["dmaster_yn"] ="";
            $data["buyofferdeposit_yn"] ="";
            $data["upmoneyoffer_yn"] ="";
            $data["branch_code"] ="";
            $data["ordermng_yn"] ="";
            $data["calc_yn"] ="";

            $data["teamoption"]= $this->customfunc->select_team("");
        }

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('adminmanage/changepwd', array('data'=>$data));
        $this->load->view('footer');
    }
}
