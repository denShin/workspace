<?php
/**
 * Login.php - 관리자 로그인 등록 페이지 컨트롤러
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('adminmanage/Adminmember_model');

    }

    public function index()
    {
        $emode = $this->input->post('emode', TRUE);

        switch($emode) {
            case "login" : $this->loginProc(); break;
            default : $this->loginBox(); break;
        }
    }

    // 로그인 view 페이지 로드
    public function loginBox()
    {
        $this->load->view('/adminmanage/login');
    }

    // 로그인 처리 진행
    public function loginProc()
    {
        $admin_id = $this->input->post('admin_id', TRUE);
        $admin_pw = $this->input->post('admin_pwd', TRUE);

        $data = $this->Adminmember_model->getLoginResult( $admin_id );


        if($data["return_v"]=="e"){
            echo "<script>alert('존재하지 않는 아이디입니다.');history.back();</script>";
            exit();
        }else if($data["return_v"]=="y"){
            $admin_pw =trim($admin_pw);

            if (password_verify($admin_pw, $data["admin_pwd"])) {


                $this->session->set_userdata(array('admin_id'=> $data["admin_email"]));
                $this->session->set_userdata(array('admin_name'=> $data["admin_name"]));

                $this->session->set_userdata(array('loginmaster_yn'=> $data["master_yn"]));
                $this->session->set_userdata(array('loginpointauth_yn'=> $data["pointauth_yn"]));
                $this->session->set_userdata(array('logincalc_yn'=> $data["calc_yn"]));
                $this->session->set_userdata(array('loginordermng_yn'=> $data["ordermng_yn"]));

                $_SESSION["loginmaster_yn"] =$data["master_yn"];
                $_SESSION["loginpointauth_yn"] = $data["pointauth_yn"];
                $_SESSION["logincalc_yn"] = $data["calc_yn"];
                $_SESSION["loginordermng_yn"] = $data["ordermng_yn"];

                $this->session->set_userdata(array('team_code'=> $data["team_code"]));
                $this->session->set_userdata(array('member_grade'=> $data["member_grade"]));

                echo "<script>location.href='/carmore/Dashboard'</script>";
                exit();
            } else {
                echo "<script>alert('비밀번호 오류입니다.');history.back();</script>";
                exit();
            }
        }

    }
}
