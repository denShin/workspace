<?php
/**
 * AdminBoardManageProc.php - 관리자전용 게시판 등록수정삭제 처리
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminBoardManageProc extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('admin/AdminBoard_model');
        $this->load->library('Customfunc');
        $this->load->library('Aws_s3');
        $this->load->library('Aws_sqs');


        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        $demode =$this->input->get('emode');
        if($demode=="")  $demode =$this->input->post('emode');

        switch($demode) {
            case "edit" : $this->setAdminBoardInfo(); break;
            case "del" : $this->delAdminBoardInfo(); break;
            case "agree_yn" : $this->setAgreeYn(); break;
            default : $this->setAdminBoardInfo(); break;
        }
    }

    public function setAdminBoardInfo()
    {

        $data["emode"] =$this->input->post('emode');

        $data["board_idx"] =$this->input->post('board_idx');
        $data["content_code"] =$this->input->post('content_code');
        $data["startdate"]= $this->input->post('startdate');
        $data["enddate"] =$this->input->post('enddate');
        $data["starttime"] =$this->input->post('starttime');
        $data["endtime"] =$this->input->post('endtime');
        $data["board_title"] =$this->input->post('board_title');
        $data["board_code"] =$this->input->post('board_code');
        $data["board_content"] =$this->input->post('board_content');
        $data["board_part"] =$this->input->post('board_part');
        $data["mem_id"] =$this->session->userdata('admin_id') ;
        $data["readcnt"] =$this->input->post('readcnt');
        $data["regdate"] =$this->input->post('regdate');
        $data["price"] =$this->input->post('price');
        $data["content_type"] =$this->input->post('content_type');
        $data["agree_yn"] =$this->input->post('agree_yn');


        $return_v = $this->AdminBoard_model->procAdminBoard( $data["emode"] ,$data);

        //  메일발송
        if($data["emode"]=="new"){
            //공지사항
            if($data["board_code"] =="notice"){
                $desemail =$this->customfunc->getAllMemberEmail("all");
                $arr_email=$this->customfunc->getContentMailContent(APP_URL,$data["board_code"], $data["board_title"], $data["board_content"] );
            }

            //휴가신청
            if($data["board_code"] =="vacation"){
                // 결정권자 이메일 발송
                $mem_name =$this->customfunc->get_memname( $data["mem_id"] );
                $content_typestr = $this->customfunc->getContentypeStr($data["content_type"]);
                $title = "[".$mem_name." ".$content_typestr." ".date("Y-m-d")." 승인요청] ".$data["board_title"];
                $content ="[".$mem_name." ".$content_typestr." ".date("Y-m-d")."  ]" ;
                $content .="<br><BR>일시 : ".$data["startdate"] ."~".$data["enddate"]."<BR><BR>".  $data["board_content"];

                $desemail =$this->customfunc->getAllMemberEmail("vacation");
                $arr_email=$this->customfunc->getContentMailContent(APP_URL,$data["board_code"], $title , $content );
            }

            //구매신청
            if($data["board_code"] =="buyoffer"){
                // 결정권자 이메일 발송
                $mem_name =$this->customfunc->get_memname( $data["mem_id"] );
                $oprice = number_format($data["price"],0);

                $title = "[".$mem_name." ".date("Y-m-d")." 승인요청] ".$data["board_title"];
                $content ="[".$mem_name."   ".date("Y-m-d")."  ]" ;
                $content .="<br><BR>금액 : ".$oprice." 원<BR><BR>".  $data["board_content"];

                $desemail =$this->customfunc->getAllMemberEmail("buyoffer");
                $arr_email=$this->customfunc->getContentMailContent(APP_URL,$data["board_code"], $title , $content );

            }

            if($data["board_code"] =="buyoffer" || $data["board_code"] =="vacation" || $data["board_code"] =="notice") {
                $quedata["sendemail"] = SES_SENDER;
                $quedata["receiveemail"] = $desemail;
                $quedata["title"] = $arr_email["title"];
                $quedata["content"] = $arr_email["content"];

                $this->aws_sqs->set_Quemessage("emailsendque", $quedata);
            }
        }

        switch($data["board_code"]) {
            case "vacation" : $backpage="/etcmanage/Vacation"; break;
            case "buyoffer" : $backpage="/etcmanage/BuyOffer"; break;
            case "notice" : $backpage="/etcmanage/AdminNotice"; break;
            case "dayreport" : $backpage="/etcmanage/DayReport"; break;
            case "calendar" : $backpage="/etcmanage/CompanyCalendar"; break;
            case "comfile" : $backpage="/etcmanage/CompanyFile"; break;
        }

        echo "<script>location.href='$backpage'</script>";
        exit();

    }

    public function delAdminBoardInfo()
    {
        $data["emode"] =$this->input->get('emode');
        $data["board_idx"] =$this->input->get('board_idx');
        $data["board_code"] =$this->input->get('board_code');
        $data["content_code"] =$this->input->get('content_code');

        switch($data["board_code"]) {
            case "vacation" : $backpage="/etcmanage/Vacation"; break;
            case "buyoffer" : $backpage="/etcmanage/BuyOffer"; break;
            case "notice" : $backpage="/etcmanage/AdminNotice"; break;
            case "dayreport" : $backpage="/etcmanage/DayReport"; break;
            case "calendar" : $backpage="/etcmanage/CompanyCalendar"; break;
            case "comfile" : $backpage="/etcmanage/CompanyFile"; break;
        }
        $return_v = $this->AdminBoard_model->procAdminBoard( $data["emode"] ,$data);

        //s3 객체 삭제
        $this->aws_s3->del_S3Folder("ppssadmin",$data["board_code"]."/".$data["content_code"]);

        echo "<script>location.href='$backpage'</script>";
        exit();
    }

    public function setAgreeYn()
    {
        $data["emode"] =$this->input->post('emode');
        $data["board_idx"] =$this->input->post('board_idx');
        $board_code =$this->input->post('board_code');
        $data["isConfirm"] =$this->input->post('isConfirm');
        $data["confirm_list"] =$this->input->post('confirm_list');
        $data["confirm_id"] =$this->session->userdata('admin_id') ;

        switch($board_code) {
            case "vacation" : $backpage="/etcmanage/Vacation"; break;
            case "buyoffer" : $backpage="/etcmanage/BuyOffer"; break;
        }

        $return_v = $this->AdminBoard_model->procAdminBoard( "agree_yn" ,$data);

        // 결정권자 이메일 발송
        $confirm_str="";
        if($data["isConfirm"]=="1"){
            $confirm_str="-승인";
        }else if($data["isConfirm"]=="2"){
            $confirm_str="-반려";
        }

        $arr_confirmlist=$data["confirm_list"];

        for($i=0;$i < sizeof($arr_confirmlist); $i++)
        {
            $board_idx = $arr_confirmlist[$i];

            $arr_AdminBoarddata = $this->AdminBoard_model->getAdminBoardDetail($board_idx);

            $rdata["content_code"] =$arr_AdminBoarddata["content_code"];
            $rdata["startdate"]= $arr_AdminBoarddata["startdate"];
            $rdata["enddate"] =$arr_AdminBoarddata["enddate"];
            $rdata["board_title"] =$arr_AdminBoarddata["board_title"];
            $rdata["board_code"] =$arr_AdminBoarddata["board_code"];
            $rdata["board_content"] =$arr_AdminBoarddata["board_content"];
            $rdata["board_part"] =$arr_AdminBoarddata["board_part"];
            $rdata["mem_id"] =$arr_AdminBoarddata["mem_id"];
            $rdata["content_type"] =$arr_AdminBoarddata["content_type"];
            $rdata["contentype_str"] =$arr_AdminBoarddata["contentype_str"];
            $rdata["regdate"] =$arr_AdminBoarddata["regdate"];
            $rdata["price"] =number_format($arr_AdminBoarddata["price"],0);

            //휴가신청
            if($rdata["board_code"] =="vacation"){
                // 결정권자 이메일 발송
                $mem_name =$this->customfunc->get_memname( $rdata["mem_id"] );
                $content_typestr = $this->customfunc->getContentypeStr($rdata["content_type"]);
                $title = "[".$mem_name." ".$content_typestr." ".date("Y-m-d")." ".$confirm_str."] ".$rdata["board_title"];
                $content ="[".$mem_name." ".$content_typestr." ".date("Y-m-d")." ".$confirm_str." ]" ;
                $content .="<br><BR>일시 : ".$rdata["startdate"] ."~".$rdata["enddate"]."<BR><BR>".  $rdata["board_content"];

                $desemail =$this->customfunc->getAllMemberEmail("vacation");
                $arr_email=$this->customfunc->getContentMailContent(APP_URL,$rdata["board_code"], $title , $content );
            }

            //구매신청
            if($rdata["board_code"] =="buyoffer"){
                // 결정권자 이메일 발송
                $mem_name =$this->customfunc->get_memname( $rdata["mem_id"] );
                $oprice = number_format($rdata["price"],0);

                $title = "[".$mem_name." ".date("Y-m-d")." ".$confirm_str."] ".$rdata["board_title"];
                $content ="[".$mem_name."   ".date("Y-m-d")." ".$confirm_str."  ]" ;
                $content .="<br><BR>금액 : ".$oprice." 원<BR><BR>".  $rdata["board_content"];

                $desemail =$this->customfunc->getAllMemberEmail("buyoffer");
                $arr_email=$this->customfunc->getContentMailContent(APP_URL,$rdata["board_code"], $title , $content );

            }

            $quedata["sendemail"] = SES_SENDER;
            $quedata["receiveemail"] =$desemail;
            $quedata["title"] = $arr_email["title"];
            $quedata["content"] = $arr_email["content"];

            $this->aws_sqs->set_Quemessage("emailsendque",$quedata);

        }
        echo "<script>location.href='$backpage'</script>";
        exit();
    }

}
