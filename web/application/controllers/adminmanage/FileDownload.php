<?php
/**
 * FileDownload.php : aws s3 에서 파일 다운로드 처리 컨트롤러
 */

defined('BASEPATH') OR exit('No direct script access allowed');


class FileDownload extends CI_Controller {

    private $ofilename, $sfilename,$board_code,$board_contentcode;

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library('Aws_s3');
        $this->load->model('adminmanage/FileDownload_model');

    }

    public function index()
    {
        $this->sfilename = $_GET["sfilename"];
        $this->board_code = $_GET["board_code"];
        $this->board_contentcode = $_GET["board_contentcode"];
        $this->ofilename = $this->FileDownload_model->get_ofilename($this->sfilename);

        $file_url = 'https://s3.ap-northeast-2.amazonaws.com/phonesosimage/'.$this->board_code."/" . $this->sfilename;

        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=".$this->ofilename);
        readfile($file_url);

        exit;
    }


}
