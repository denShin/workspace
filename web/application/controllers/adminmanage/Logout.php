<?php
/**
 * Logout.php - 관리자 로그아웃 등록 페이지 컨트롤러
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        ob_start(); # add this
        $this->load->library('Session'); # add this
    }

    // 로그아웃 처리
    public function index()
    {
        $this->load->driver('cache'); # add
        $this->session->sess_destroy(); # Change
        $this->cache->clean();  # add
        ob_clean(); # add

        echo "<script>location.href='/adminmanage/Login'</script>";
        exit();

    }

}
