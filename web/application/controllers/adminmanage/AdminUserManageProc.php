<?php
/**
 * AdminUserManageProc.php - 관리자멤버 등록,수정,삭제, 비밀번호 변경 처리
 */

class AdminUserManageProc extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->database();

        $this->load->library('Customfunc');
        $this->load->model('adminmanage/Adminmember_model');


        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        $demode =$this->input->get('emode');
        if($demode=="") $demode =$this->input->post('emode');

        switch($demode) {
            case "del" : $this->delAdminMemberInfo(); break;
            case "setnewpwd" : $this->set_changePassword(); break;
            case "password" : $this->set_NewPassword(); break;
            default : $this->setAdminMemberInfo(); break;
        }
    }

    // 비밀번호 변경 처리
    public function set_changePassword()
    {

        $data["old_pwd"]=$this->input->post('admin_pwd');
        $data["newpwd"]=$this->input->post('newpwd');
        $data["newpwd1"]=$this->input->post('newpwd1');
        $loginmaster_yn=$this->session->userdata('loginmaster_yn');

        $admin_id=$this->session->userdata('admin_id');
        $data["admin_email"]=$admin_id;

        $admin_pw =trim($data["old_pwd"]);

        if($data["newpwd"] != $data["newpwd1"]) {
            echo "<script>alert('신규 비밀번호 오류입니다.');history.back();</script>";
            exit();
        }


        $passdata = $this->Adminmember_model->getLoginResult( $admin_id );

        if (password_verify($admin_pw, $passdata["admin_pwd"]) || $loginmaster_yn=="y") {

            $data["admin_pwd"] = password_hash( $data["newpwd"] , PASSWORD_BCRYPT);

            $this->Adminmember_model->procChangePassword($data);
            echo "<script>alert('비밀번호 변경이 완료되었습니다.');location.href='/adminmanage/Changepwd';</script>";
            exit();


        } else {
            echo "<script>alert('기존 비밀번호 오류입니다.');history.back();</script>";
            exit();
        }
    }

    // 신규 비밀번호 세팅
    public function set_NewPassword()
    {
        $loginmaster_yn=$this->session->userdata('loginmaster_yn');

        if($loginmaster_yn=="y"){
            $admin_id=$this->input->post('admin_email');
            $data["admin_email"]=$this->input->post('admin_email');
        }else{

            $admin_id=$this->session->userdata('admin_id');
            $data["admin_email"]=$admin_id;
        }
        $data["password"]=$this->input->post('admin_pwd');
        $data["admin_pwd"] = password_hash( $data["password"] , PASSWORD_BCRYPT);


        $this->Adminmember_model->procChangePassword($data);
        echo "<script>alert('비밀번호 변경이 완료되었습니다.');location.href='/adminmanage/AdminUserManage';</script>";
        exit();
    }

    // 관리자 멤버 정보 등록,수정 호출
    public function setAdminMemberInfo()
    {
        $data["emode"] =$this->input->post('emode');

        $data["admin_name"]=$this->customfunc->SQL_Injection( $this->input->post('admin_name') );
        $data["team_code"]=$this->customfunc->SQL_Injection( $this->input->post('team_code') );
        $data["member_grade"]=$this->customfunc->SQL_Injection( $this->input->post('member_grade') );
        $data["admin_email"]=$this->customfunc->SQL_Injection( $this->input->post('admin_email') );
        $data["admin_pwd"]=$this->customfunc->SQL_Injection( $this->input->post('admin_pwd') );
        $data["admin_tel"]=$this->customfunc->SQL_Injection( $this->input->post('admin_tel') );
        $data["admin_stats"]=$this->customfunc->SQL_Injection( $this->input->post('admin_stats') );

        $data["config_yn"]=$this->customfunc->SQL_Injection( $this->input->post('config_yn') );
        $data["paycancel_yn"]=$this->customfunc->SQL_Injection( $this->input->post('paycancel_yn') );
        $data["pointauth_yn"]=$this->customfunc->SQL_Injection( $this->input->post('pointauth_yn') );
        $data["calc_yn"]=$this->customfunc->SQL_Injection( $this->input->post('calc_yn') );
        $data["ordermng_yn"]=$this->customfunc->SQL_Injection( $this->input->post('ordermng_yn') );
        $data["master_yn"]=$this->customfunc->SQL_Injection( $this->input->post('master_yn') );
        $data["kakaoid"]=$this->customfunc->SQL_Injection( $this->input->post('kakaoid') );


        $data["admin_pwd"] = password_hash( $data["admin_pwd"] , PASSWORD_BCRYPT);

        $return_v = $this->Adminmember_model->procAdminMember( $data["emode"] ,$data);
        if($return_v=="e"){
            echo "<script>alert('이메일정보 중복.');history.back();</script>";
            exit();
        }else if($return_v=="y"){
            echo "<script>location.href='/adminmanage/AdminUserManage'</script>";
            exit();
        }
    }


    // 관리자 멤버 정보 삭제
    public function delAdminMemberInfo()
    {
        $data["emode"] =$this->input->get('emode');
        $data["admin_email"] =$this->input->get('admin_email');

        $return_v = $this->Adminmember_model->procAdminMember( $data["emode"] ,$data);
        if($return_v=="e"){
            echo "<script>alert('관리자정보 삭제 불가능.');history.back();</script>";
            exit();
        }else if($return_v=="y"){
            echo "<script>location.href='/adminmanage/AdminUserManage'</script>";
            exit();
        }
    }

}
