<?php
/**
 * AdminUserManage.php - 관리자 리스트, 등록 처리 컨트롤러
 */

class AdminUserManage extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->model('adminmanage/Adminmember_model');
        $this->load->library('Customfunc');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }

        $pageauth =$this->customfunc->get_pageauth("master");

        if ( $pageauth != "y") {
            echo "<script>location.href='/carmore/Dashboard'</script>";
            exit();
        }

    }

    public function index()
    {

        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);

        switch($ptype) {
            case "w" : $this->loadAdminMemberWrite(); break;
            default : $this->loadAdminMemberList(); break;
        }
    }


    // 관리자 멤버 리스트
    public function loadAdminMemberList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);


        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $arr_adminmemberdata = $this->Adminmember_model->getAdminMemberPageList($page,20,$sp,$sv);
        $total = $this->Adminmember_model->getAdminMemberCount($sp,$sv);


        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/adminmanage/AdminUserManage";
        $config['suffix']     ="&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $data["list"]=$arr_adminmemberdata;
        $data["per_page"]=$per_page;
        $data["pagination"]=$pagination;

        $this->load->view('adminmanage/adminmemberlist', array('data'=>$data));
        $this->load->view('footer');
    }


    // 관리자 멤버 등록,수정 처리 
    public function loadAdminMemberWrite()
    {

        $pemail = $this->input->get('admin_email', TRUE);

        if($pemail !=""){
            $data["emode"]="edit";
            $arr_adminmemberdata = $this->Adminmember_model->getAdminMemberDetail($pemail);

            $data["admin_name"] =$arr_adminmemberdata["admin_name"];
            $data["admin_email"] =$arr_adminmemberdata["admin_email"];
            $data["admin_pwd"]=$arr_adminmemberdata["admin_pwd"];
            $data["admin_tel"] =$arr_adminmemberdata["admin_tel"];
            $data["team_code"] =$arr_adminmemberdata["team_code"];
            $data["admin_stats"] =$arr_adminmemberdata["admin_stats"];
            $data["member_grade"] = $arr_adminmemberdata["member_grade"];
            $data["config_yn"] =$arr_adminmemberdata["config_yn"];
            $data["offwork_yn"] =$arr_adminmemberdata["offwork_yn"];
            $data["dmaster_yn"] =$arr_adminmemberdata["master_yn"];
            $data["ordermng_yn"] =$arr_adminmemberdata["ordermng_yn"];
            $data["pointauth_yn"] =$arr_adminmemberdata["pointauth_yn"];
            $data["calc_yn"] =$arr_adminmemberdata["calc_yn"];
            $data["kakaoid"] =$arr_adminmemberdata["kakaoid"];

            $data["teamoption"]= $this->customfunc->select_team($data["team_code"]);

        }else{
            $data["emode"]="new";

            $data["admin_name"] ="";
            $data["admin_email"] ="";
            $data["admin_pwd"]= "";
            $data["admin_tel"] ="";
            $data["team_code"] ="";
            $data["admin_stats"] ="";
            $data["member_grade"] ="";
            $data["config_yn"] ="";
            $data["offwork_yn"] ="";
            $data["buyoffer_yn"] ="";
            $data["dmaster_yn"] ="";
            $data["buyofferdeposit_yn"] ="";
            $data["upmoneyoffer_yn"] ="";
            $data["branch_code"] ="";
            $data["ordermng_yn"] ="";
            $data["pointauth_yn"] ="";
            $data["calc_yn"] ="";
            $data["kakaoid"] ="";

            $data["teamoption"]= $this->customfunc->select_team("");
        }

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('adminmanage/adminmemberwrite', array('data'=>$data));
        $this->load->view('footer');
    }
}
