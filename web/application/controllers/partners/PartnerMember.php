<?php
/**
 * PartnerMember.php -파트너스 기업회원 리스트,등록수정 컨트롤러
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class PartnerMember extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('partners/PartnerMember_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);

        switch($ptype) {
            case "w" : $this->loadPartnerMemberWrite(); break;
            default : $this->loadPartnerMemberList(); break;
        }
    }

    // 파트너스 기업 회원 리스트 로드
    public function loadPartnerMemberList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);
        $mem_stats = $this->input->get('mem_stats', TRUE);
        if(!$mem_stats) $mem_stats="y";
        if($mem_stats =="y"){
            $listitle="기업 회원관리";
        }else if($mem_stats =="x"){
            $listitle="기업 탈퇴 회원관리";
        }else if($mem_stats =="n"){
            $listitle="기업 차단 회원관리";
        }

        if(!$sp) $sp="";
        if(!$sv) $sv="";

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));


        $rowresult = $this->PartnerMember_model->getPartnerMemberPageList($page, 20, $sp, $sv);
        $total = $this->PartnerMember_model->getPartnerMemberCount($sp, $sv);

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/partners/PartnerMember";
        $config['suffix']     ="&mem_stats=".$mem_stats."&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];
        foreach($rowresult as $entry)
        {
            $arr_datalist["serial"]=$entry->serial;
            $arr_datalist["NAME"]=$entry->name;
            $arr_datalist["OWNER"]=$entry->owner;
            $arr_datalist["ownertel"]=$entry->ownerTel;
            $arr_datalist["logo"]=$entry->logo;
            $arr_datalist["regdate"]=$entry->regdate;
            $arr_datalist["contact"]=$entry->contact;
            $arr_datalist["carmore"]=$entry->carmore;
            $arr_datalist["sub"]=$entry->sub;
            $arr_datalist["sub_email"]=$entry->sub_email;
            $arr_datalist["test_check"]=$entry->test_check;
            $arr_datalist["authority"]=$entry->authority;
            if($entry->authority =="1"){
                $arr_datalist["authoritystr"] ="승인";
            }else{
                $arr_datalist["authoritystr"] ="<span style='color:red'>승인대기</span>";
            }

            $arr_datalist["simple_ver"]=$entry->simple_ver;
            if($entry->simple_ver =="1"){
                $arr_datalist["simple_verstr"] ="심플";
            }else{
                $arr_datalist["simple_verstr"] ="풀버전";
            }


            $arr_datalist["mainimage"]=$entry->mainimage;
            if(strlen($arr_datalist["mainimage"]) < 3 ){
                $fullurl="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/partners/noimage.png";
            }else{
                $fullurl="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/partners/mainimg_".$entry->serial."/".$entry->mainimage;
            }

            $arr_datalist["fullurl"]=$fullurl;
            $arr_datalist["subcnt"]=$entry->subcnt;
            $regdate =$entry->regdate;
            $arr_datalist["regdatev"] =$regdate;
            $arr_datalist["regdate"] =$this->customfunc->get_dateformat($regdate);


            $arr_alldatalist[]=$arr_datalist;
        }

        $data["list"]=$arr_alldatalist;
        $data["mem_stats"]=$mem_stats;
        $data["per_page"]=$per_page;
        $data["listitle"]=$listitle;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->LOGINID;

        $this->load->view('partners/partnermemberlist', array('data'=>$data));
        $this->load->view('footer');
    }

    // 파트너스 기업 회원 등록,수정 로드
    public function loadPartnerMemberWrite()
    {

        $pemail = $this->input->get('admin_email', TRUE);

        if($pemail !=""){
            $data["emode"]="edit";
            $arr_PartnerMemberdata = $this->PartnerMember_model->getPartnerMemberDetail($pemail);

            $data["admin_name"] =$arr_PartnerMemberdata["admin_name"];
            $data["admin_email"] =$arr_PartnerMemberdata["admin_email"];
            $data["admin_pwd"]=$arr_PartnerMemberdata["admin_pwd"];
            $data["admin_tel"] =$arr_PartnerMemberdata["admin_tel"];
            $data["team_code"] =$arr_PartnerMemberdata["team_code"];
            $data["admin_stats"] =$arr_PartnerMemberdata["admin_stats"];
            $data["member_grade"] =$arr_PartnerMemberdata["member_grade"];
            $data["config_yn"] =$arr_PartnerMemberdata["config_yn"];
            $data["offwork_yn"] =$arr_PartnerMemberdata["offwork_yn"];
            $data["buyoffer_yn"] =$arr_PartnerMemberdata["buyoffer_yn"];
            $data["dmaster_yn"] =$arr_PartnerMemberdata["master_yn"];
            $data["buyofferdeposit_yn"] =$arr_PartnerMemberdata["buyofferdeposit_yn"];
            $data["upmoneyoffer_yn"] =$arr_PartnerMemberdata["upmoneyoffer_yn"];
            $data["ordermng_yn"] =$arr_PartnerMemberdata["ordermng_yn"];
            $data["calc_yn"] =$arr_PartnerMemberdata["calc_yn"];
            $data["teamoption"]= $this->customfunc->select_team($data["team_code"]);

        }else{
            $data["emode"]="new";

            $data["admin_name"] ="";
            $data["admin_email"] ="";
            $data["admin_pwd"]= "";
            $data["admin_tel"] ="";
            $data["team_code"] ="";
            $data["admin_stats"] ="";
            $data["member_grade"] ="";
            $data["config_yn"] ="";
            $data["offwork_yn"] ="";
            $data["buyoffer_yn"] ="";
            $data["dmaster_yn"] ="";
            $data["buyofferdeposit_yn"] ="";
            $data["upmoneyoffer_yn"] ="";
            $data["branch_code"] ="";
            $data["ordermng_yn"] ="";
            $data["calc_yn"] ="";

            $data["teamoption"]= $this->customfunc->select_team("");
        }

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('partners/partnermemberwrite', array('data'=>$data));
        $this->load->view('footer');
    }
}
