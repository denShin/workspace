<?php
/**
 * Update050number.php -050 번호 업데이트 컨트롤러 (사용안함 X)
 */

class Update050number extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('partners/PartnerMember_model');
        $this->load->model('adminmanage/Adminmember_model');

    }

    function index(){
        $demode =$this->input->get('emode');
        if($demode==""){
            $demode =$this->input->post('emode');
        }
        switch($demode) {

            default : $this->setupdatetelnum(); break;
        }
    }

    function setupdatetelnum()
    {
        $rowresult = $this->PartnerMember_model->geterror050list();

        foreach ($rowresult as $entry) {

            $data["tel050_idx"] = $entry["tel050_idx"];
            $data["tel050_number"] = $entry["tel050_number"];
            $tel050_matnumber = $entry["tel050_matnumber"];
            $tel050_matnumber = preg_replace("/[^0-9]*/s", "", $tel050_matnumber);
            $data["tel050_matnumber"] = $tel050_matnumber;


            $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
            if ($socket === false) {
                $errno = socket_last_error();
                $error = sprintf('%s (%d)', socket_strerror($errno), $errno);
                trigger_error($error, E_USER_ERROR);

                $result["result"] = "error";
                echo json_encode($result);
                exit();
            }

            /*
             * 1.     업체명 : 팀오투
    2.     CompanyID : 443
    3.     할당번호 : 0503-7961-YYYY (1만개)
    4.     상용서버 : 203.240.244.116
    5.     연동방식 및 포트 : 소켓방식 / Port : 61311
    6.     방화벽작업 : 고객ip 52.79.254.165, 52.78.47.207 등록 완료
    7.     기타 : 고객측 방화벽 운용시 세종 서버 정보
    (203.240.244.116, 203..240.244.113, 203.240.244.114) 등록 요망
    8. VNS 안내맨트 TTS로 생성하여 반영 완료
             */

            $address = '203.240.244.116';
            if (!socket_connect($socket, $address, 61311)) {
                $errno = socket_last_error($socket);
                $error = sprintf('%s (%d)', socket_strerror($errno), $errno);
                trigger_error($error, E_USER_ERROR);

                $result["result"] = "error";
                echo json_encode($result);
                exit();
            } else {
                $sendnewtelnum = $data["tel050_number"];
                $phone_num1 = $data["tel050_matnumber"];
                if( strlen($phone_num1) < 11 ){
                    $tlen = 11-strlen($phone_num1) ;
                    $blankstr="";
                    for($i=0;$i< $tlen;$i++){
                        $blankstr =$blankstr." ";
                    }
                    $phone_num1 =$phone_num1.$blankstr;
                }

                $packet_id = '2501';#2501: 번호사용등록요구 패킷/ 2502 : 번호사용해지요구/ 2600:상태체크요구  -  3501,3502,3600
                $company_id = '443';
                $system_id = '000';
                $sequence = '                    '; #고정 20칸
                $result_msg = '  '; #고정 2칸
                $method = '1';
                $onse_number = $sendnewtelnum . "    "; //할당이 0001~9999 까지
                $phone_num1 = $phone_num1 . "   "; //업체번호 1
                $phone_num2 = '               '; //업체번호2
                $onse_number2 = '               ';
                $phone2_num1 = '               ';
                $phone2_num2 = '               ';
                $use_flag = 1;


                $msg = '#' . $packet_id . $company_id . $system_id . $sequence . $result_msg . $method . $onse_number . $phone_num1 . $phone_num2 . $onse_number2 . $phone2_num1 . $phone2_num2 . $use_flag . '$';

                echo $msg;
                echo "<BR>len :".strlen($msg)."<BR>";

                $result =socket_write($socket, $msg, strlen($msg)) or print("Could not send data to server\n");
                echo $result."<BR>";

                $result = socket_read($socket, 1024) or print("Could not read server response\n");
                echo $result."<BR>";


                // :#2501443000                    01105037961111101030043970                                                            1$
                //Reply From Server :#3501450000 001050379610001 0262490202 1$
                socket_close($socket);

                $rowresult = $this->PartnerMember_model->updaterror050info($data);
                print_r($data);
                print_r($rowresult);
                sleep(1);

            }

        }
    }

}