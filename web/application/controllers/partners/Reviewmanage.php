<?php
/**
 * Reviewmanage.php -리뷰관리 리스트 ,등록,수정,보기 컨트롤
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Reviewmanage extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('partners/PartnerMember_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $companyserial =$this->customfunc->SQL_Injection($this->input->get('companyserial'));
        $branch_serial =$this->customfunc->SQL_Injection($this->input->get('branchserial'));
        $ptype = $this->input->get('ptype', TRUE);

        switch($ptype) {

            case "setreviewblind" : $this->setreviewblind(); break;
            case "setreviewanswer" : $this->setreviewanswer(); break;
            case "popreview" : $this->loadpopReviewdata(); break;
            default : $this->loadReviewList(); break;
        }
    }


    // 리뷰 블라인드 처리
    public function setreviewblind(){
        $data["reservation_idx"] = $this->input->get('reservation_idx', TRUE);
        $data["work"] =  $this->input->get('work', TRUE);
        $data["blind_code"] =  $this->input->get('blind_code', TRUE);
        $data["blind_codetext"] =  $this->input->get('blind_codetext', TRUE);

        $return_v = $this->PartnerMember_model->setreviewblind($data);

        return;
    }


    // 리뷰 답변 처리
    public function setreviewanswer(){
        $data["reservation_idx"] = $this->input->get('reservation_idx', TRUE);
        $data["crr_reply_content"] =  $this->input->get('crr_reply_content', TRUE);
        $return_v = $this->PartnerMember_model->setreviewanswer($data);

        return;
    }


    // 리뷰 팝업 리스트 -jsondata 로드
    public function loadpopReviewdata(){
        $data["reservation_idx"] = $this->input->get('reservation_idx', TRUE);

        $return_v = $this->PartnerMember_model->getreservereview($data);

        echo json_encode($return_v);

    }

    // 리뷰 리스트 로드
    public function loadReviewList()
    {

        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);
        $mem_stats = $this->input->get('mem_stats', TRUE);
        $pageorderby = $this->input->get('pageorderby', TRUE);


        if(!$pageorderby) $pageorderby="register_date-desc";
        if(!$mem_stats) $mem_stats="y";
        if($mem_stats =="y"){
            $listitle="기업 지점 관리";
        }else if($mem_stats =="x"){
            $listitle="기업 탈퇴 관리";
        }else if($mem_stats =="n"){
            $listitle="기업 차단 회원관리";
        }

        if(!$sp) $sp="";
        if(!$sv) $sv="";

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));


        $rowresult = $this->PartnerMember_model->getReviewPageList($page, 20, $sp, $sv,$pageorderby);
        $total = $this->PartnerMember_model->getReviewCount($sp, $sv);
        $cntdata = $this->PartnerMember_model->getReviewallcount();

        $data["totalcnt"] = $cntdata["totalcnt"];
        $data["replycnt"] = $cntdata["replycnt"];
        $data["gapcnt"] = $cntdata["totalcnt"]-$cntdata["replycnt"];
        $data["gapper"] = 100- (int)(($cntdata["replycnt"]/$cntdata["totalcnt"])*100) ;

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/partners/Reviewmanage?mem_stats=".$mem_stats."&sp=".$sp."&sv=".$sv."&pageorderby=".$pageorderby;
        $config['suffix']     ="";
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];



        foreach($rowresult as $entry)
        {

            $arr_datalist["serial"]=$entry->serial;
            $arr_datalist["branchname"]=$entry->branchname;
            $arr_datalist["companyname"]=$entry->companyname;
            $arr_datalist["driver_name"]=$entry->driver_name;
            $arr_datalist["opinion"]=$entry->opinion;
            $arr_datalist["admincheck_yn"]=$entry->admincheck_yn;

            if($entry->admincheck_yn !="y"){
                $arr_datalist["admincheck_yncolor"]="red";
            }else{
                $arr_datalist["admincheck_yncolor"]="black";
            }


            $arr_datalist["branch_evaluation"]=$entry->branch_evaluation;
            $arr_datalist["car_evaluation"]=$entry->car_evaluation;
            $arr_datalist["take_evaluation"]=$entry->take_evaluation;
            $arr_datalist["f_rentstartdate"]=$entry->f_rentstartdate;
            $arr_datalist["f_rentenddate"]=$entry->f_rentenddate;
            $arr_datalist["blindstatus"]=$entry->status;
            $arr_datalist["replycnt"]=$entry->replycnt;
            $arr_datalist["reservation_idx"]=$entry->reservation_idx;

            $arr_datalist["take_evaluation"]=$entry->take_evaluation;
            $arr_datalist["branch_evaluation"]=$entry->branch_evaluation;
            $arr_datalist["car_evaluation"]=$entry->car_evaluation;


            $arr_datalist["revcnt"]=$entry->revcnt;
            $arr_datalist["model"]=$entry->model;

            $driver_phone_encrypt =$this->customfunc->decrypt($entry->driver_phone_encrypt);
            $driver_phone_encrypt =$this->customfunc->add_phonehyphen($driver_phone_encrypt);
            $arr_datalist["driver_phone_encrypt"]=$driver_phone_encrypt;

            $register_date =$this->customfunc->get_dateformat($entry->register_date,"date");
            $arr_datalist["regdate"] =$register_date;

            $arr_alldatalist[]=$arr_datalist;
        }

        $data["sv"]=$sv;
        $data["sp"]=$sp;
        $data["list"]=$arr_alldatalist;
        $data["per_page"]=$per_page;
        $data["pageorderby"]=$pageorderby;
        $data["listitle"]=$listitle;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->LOGINID;

        $this->load->view('partners/reviewlist', array('data'=>$data));
        $this->load->view('footer');
    }




}
