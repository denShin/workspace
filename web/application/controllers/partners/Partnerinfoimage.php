<?php
/**
 * Partnerinfoimage.php -파트너스 기업지점관리 이미지 컨트롤러
 */


class Partnerinfoimage extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION;

    function __construct()
    {
        parent::__construct();


        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('partners/PartnerMember_model');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);


        switch($ptype) {
            case "sub" : $this->loadSubimage(); break;
            case "main" : $this->loadMainimage(); break;
        }

    }

    // 기업지점 메인 이미지 정보 로드 
    public function loadMainimage()
    {
        $this->load->helper('url'); // load the helper first
        $serial = $this->input->get('serial', TRUE);
        $content_code ="mainimg_".$serial;
        $arr_data = $this->PartnerMember_model->getPartnerinfoImage('main',$content_code);
        foreach($arr_data as $entry) {

            $data["content_code"] = $entry["content_code"];
            $data["fileSaveName"] = $entry["fileSaveName"];
            $data["publish_yn"] =$entry["publish_yn"];

            if($data["publish_yn"]=="y"){
                $data["publish_ynstr"]="배포완료";
            }else{
                $data["publish_ynstr"]="<span style='color:red'>배포대기</span>";
            }
        }

        if(strlen($data["fileSaveName"]) < 3 ){
            $fullurl="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/partners/noimage.png";
        }else{
            $fullurl="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/partners/".$data["content_code"]."/".$data["fileSaveName"];
        }
        $mstcode = "mainimg_".$serial;


        $data["mainimage"]=$fullurl;
        $data["content_code"]=$mstcode;
        $data["fileuptype"]="main";

        $data["pagetitle"]="메인이미지";

        $this->load->view('partners/partnermainimage', array('data'=>$data));

    }


    // 기업지점 서브 이미지 정보 로드
    public function loadSubimage()
    {
        $this->load->helper('url'); // load the helper first
        $serial = $this->input->get('serial', TRUE);

        $content_code ="subimg_".$serial;
        $arr_data = $this->PartnerMember_model->getPartnerinfoImage('sub',$content_code);


        $arr_alllist=[];
        foreach($arr_data as $entry)
        {
            $arr_imageinfo["content_code"] =$entry["content_code"];
            $arr_imageinfo["fileSaveName"] =$entry["fileSaveName"];
            $arr_imageinfo["publish_yn"] =$entry["publish_yn"];


            if($arr_imageinfo["publish_yn"]=="y"){
                $arr_imageinfo["publish_ynstr"]="배포완료";
            }else{
                $arr_imageinfo["publish_ynstr"]="배포대기";
            }
            if(strlen($entry->fileSaveName) < 3 ){
                $fullurl="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/partners/noimage.png";
            }else{
                $fullurl="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/partners/".$arr_imageinfo["content_code"]."/".$entry->fileSaveName;
            }


            $arr_imageinfo["fullurl"] =$fullurl;

            $arr_alllist[]=$arr_imageinfo;
        }
        $data["list"]=$arr_alllist;
        $data["content_code"]=$content_code;
        $data["fileuptype"]="sub";

        $data["pagetitle"]="서브 이미지";

        $this->load->view('partners/partnersubimage', array('data'=>$data));

    }


}