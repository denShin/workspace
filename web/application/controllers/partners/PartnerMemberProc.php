<?php
/**
 * PartnerMemberProc.php -파트너스 정보 관련 다양한 항목 수정처리 컨트롤러
 *
 * @author Eric
 * @author Den <den@tm2.kr> 19.05.29
 *
 * @property CI_Session $session
 * @property WS_Input   $input
 *
 * @property Customfunc            $customfunc
 * @property Workspace_common_func $workspace_common_func
 *
 * @property PartnerMember_model        $PartnerMember_model
 * @property Partners_affiliate_model   $partners_affiliate_model
 * @property Api_affiliate_branch_model $api_affiliate_branch_model
 */

class PartnerMemberProc  extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library('Customfunc');
        $this->load->model('partners/PartnerMember_model');
        $this->load->model('partners/partners_affiliate_model');
        $this->load->model('apiAffiliate/api_affiliate_branch_model');
        $this->load->model('adminmanage/Adminmember_model');

    }

    public function index()
    {
        $demode =$this->input->get('emode');
        if($demode==""){
            $demode =$this->input->post('emode');
        }
        switch ($demode)
        {
            case "setbranchintro":
                $this->setbranchintro();
                break;
            case "sendrestarttel":
                $this->sendrestarttel();
                break;
            case "sendnewgoldtel":
                $this->sendnewgoldtel();
                break;
            case "setpartnerout":
                $this->setpartnerout();
                break;
            case "sendelnewtel":
                $this->sendelnewtel();
                break;
            case "sendnewtel":
                $this->sendnewtel();
                break;
            case "changestats":
                $this->setchangestats();
                break;
            case "setallstats":
                $this->setallstats();
                break;
            case "uniqueness":
                $this->update_uniqueness();
                break;
        }
    }

    // 기업 지점 한마디 등록,수정 
    public function setbranchintro()
    {
        $data["serial"] = $this->input->get('serial');
        $data["introcode"] =$this->input->get('introcode');
        $data["intromsg"] =  $this->customfunc->SQL_Injection($this->input->get('intromsg'));

        $return_v = $this->PartnerMember_model->setbranchintro( $data);

        exit();
    }

    // 기업 상태 변경처리
    public function setchangestats()
    {

        $data["emode"] =$this->input->post('emode');
        $data["comserial"] =$this->input->post('comserial');
        $data["stats"] =$this->input->post('stats');

        $return_v = $this->PartnerMember_model->chagecomstats( $data);
        echo "<script>location.href='/partners/PartnerMember'</script>";
        exit();
    }

    // 선택기업 상태 변경처리(승인,승인대기, 풀버전,심플버전)
    public function setallstats()
    {

        $data["emode"] =$this->input->get('emode');
        $data["saveparam"] =$this->input->get('saveparam');
        $data["stats"] =$this->input->get('stats');
        $data["upart"] =$this->input->get('upart');

        $return_v = $this->PartnerMember_model->chageallcomstats( $data);
        exit();
    }

    // 기업회원 탈퇴처리
    public function setpartnerout()
    {
        $admin_id = $this->session->userdata('admin_id');
        $admin_pw = $this->input->get('admin_pwd', TRUE);

        $data = $this->Adminmember_model->getLoginResult( $admin_id );


        if($data["return_v"]=="e"){
            $return["result"]="n";
            $return["code"]="pwd_error";
            echo  json_encode($return);
            exit();
        }else if($data["return_v"]=="y"){

            $admin_pw =trim($admin_pw);

            if (password_verify($admin_pw, $data["admin_pwd"])) {

                $data["emode"] = $this->input->get('emode');
                $data["companyserial"] = $this->input->get('companyserial');


                $return_v = $this->PartnerMember_model->setpartnerout($data);


                $return["result"] = "y";
                $return["code"] = "";
                echo json_encode($return);
                exit();
            }else{
                $return["result"]="n";
                $return["code"]="pwd_error";
                echo  json_encode($return);
                exit();
            }

        }


    }

    // 050 골드번호 설정처리
    public function sendnewgoldtel(){

        $data["emode"] =$this->input->get('emode');
        $data["sendnewtelnum"] =$this->input->get('sendnewtelnum');
        $data["datamode"]=$this->input->get('datamode');
        $data["tel050_idx"]=$this->input->get('tel050_idx');


        $return_v = $this->PartnerMember_model->proctelgoldnum($data);

        if($return_v >0){
            $return["result"]="n";
        }else{
            $return["result"]="y";
        }

        echo  json_encode($return);
        exit();
    }

    // 기업 지점 050 번호 신규 세팅
    public function sendnewtel()
    {
        //  http://workspace.teamo2.kr/partners/PartnerMemberProc?emode=sendnewtel&bserial=559&sendnewtelnum=1111
        $data["emode"] =$this->input->get('emode');

        $sendnewtelnum =$this->input->get('sendnewtelnum');
        $data["sendnewtelnum"] ="05037961".$sendnewtelnum;
        $data["bserial"] =$this->input->get('bserial');

        $branch_tel =$this->input->get('branch_tel');
        $branch_tel = preg_replace("/[^0-9]*/s", "", $branch_tel);
        $data["branch_tel"] = $branch_tel;

        $data["datamode"] ="new";

        $data["fk_serial"]  =$this->input->get('fk_serial');
        $data["tel050_part"]  =$this->input->get('tel050_part');

        $tcnt = $this->PartnerMember_model->get050telCount($data["sendnewtelnum"] );

        if($tcnt >0){
            $result["result"]="tel_exists";
            echo  json_encode($result);
            exit();
        }

        $socket=socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if ($socket === false) {
            $errno = socket_last_error();
            $error = sprintf('%s (%d)', socket_strerror($errno), $errno);
            trigger_error($error, E_USER_ERROR);

            $result["result"]="error";
            echo  json_encode($result);
            exit();
        }

        /*
         * 1.     업체명 : 팀오투
2.     CompanyID : 443
3.     할당번호 : 0503-7961-YYYY (1만개)
4.     상용서버 : 203.240.244.116
5.     연동방식 및 포트 : 소켓방식 / Port : 61311
6.     방화벽작업 : 고객ip 52.79.254.165, 52.78.47.207 등록 완료
7.     기타 : 고객측 방화벽 운용시 세종 서버 정보
(203.240.244.116, 203..240.244.113, 203.240.244.114) 등록 요망
8. VNS 안내맨트 TTS로 생성하여 반영 완료
         */

        $address = '203.240.244.116';
        if ( !socket_connect($socket, $address, 61311) ) {
            $errno = socket_last_error($socket);
            $error = sprintf('%s (%d)', socket_strerror($errno), $errno);
            trigger_error($error, E_USER_ERROR);

            $result["result"]="error";
            echo  json_encode($result);
            exit();
        } else {
            $sendnewtelnum =$data["sendnewtelnum"];
            $phone_num1 = $data["branch_tel"];
            if( strlen($phone_num1) < 11 ){
                $tlen = 11-strlen($phone_num1) ;
                $blankstr="";
                for($i=0;$i< $tlen;$i++){
                    $blankstr =$blankstr." ";
                }
                $phone_num1 =$phone_num1.$blankstr;
            }

            $packet_id = '2501';#2501: 번호사용등록요구 패킷/ 2502 : 번호사용해지요구/ 2600:상태체크요구  -  3501,3502,3600
            $company_id = '443';
            $system_id = '000';
            $sequence = '                    '; #고정 20칸
            $result_msg = '  '; #고정 2칸
            $method = '1';
            $onse_number = $sendnewtelnum."    " ; //할당이 0001~9999 까지
            $phone_num1 = $phone_num1."   "; //업체번호 1
            $phone_num2 = '               '; //업체번호2
            $onse_number2 = '               ';
            $phone2_num1 = '               ';
            $phone2_num2 = '               ';
            $use_flag = 1;


            $msg = '#'.$packet_id.$company_id.$system_id.$sequence.$result_msg.$method.$onse_number.$phone_num1.$phone_num2.$onse_number2.$phone2_num1.$phone2_num2.$use_flag.'$';

            socket_write($socket, $msg, strlen($msg)) or print("Could not send data to server\n");

            $result = socket_read ($socket, 1024) or print("Could not read server response\n");

            // :#2501443000                    01105037961111101030043970                                                            1$
            //Reply From Server :#3501450000 001050379610001 0262490202 1$
            socket_close($socket);

            $return_v = $this->PartnerMember_model->proctel050($data);

            if ($data['tel050_part'] === 'api')    // API 업체의 050 연결
                $this->api_affiliate_branch_model->update_tel050($data['fk_serial'], $sendnewtelnum);

            $return["result"]="y";
            echo  json_encode($return);
            exit();
        }
    }

    // 기업 지점 050 번호 재시작 등록
    public function sendrestarttel()
    {
        //  http://workspace.teamo2.kr/partners/PartnerMemberProc?emode=sendnewtel&bserial=559&sendnewtelnum=1111
        $data["emode"] =$this->input->get('emode');

        $data["bserial"] =$this->input->get('bserial');
        $data["sendnewtelnum"] =$this->input->get('retel');
        $branch_tel =$this->input->get('branch_tel');
        $branch_tel = preg_replace("/[^0-9]*/s", "", $branch_tel);
        $data["branch_tel"] = $branch_tel;

        $data["datamode"] ="restart";

        $socket=socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if ($socket === false) {
            $errno = socket_last_error();
            $error = sprintf('%s (%d)', socket_strerror($errno), $errno);
            trigger_error($error, E_USER_ERROR);

            $result["result"]="error";
            echo  json_encode($result);
            exit();
        }

        /*
         * 1.     업체명 : 팀오투
2.     CompanyID : 443
3.     할당번호 : 0503-7961-YYYY (1만개)
4.     상용서버 : 203.240.244.116
5.     연동방식 및 포트 : 소켓방식 / Port : 61311
6.     방화벽작업 : 고객ip 52.79.254.165, 52.78.47.207 등록 완료
7.     기타 : 고객측 방화벽 운용시 세종 서버 정보
(203.240.244.116, 203..240.244.113, 203.240.244.114) 등록 요망
8. VNS 안내맨트 TTS로 생성하여 반영 완료
         */

        $address = '203.240.244.116';
        if ( !socket_connect($socket, $address, 61311) ) {
            $errno = socket_last_error($socket);
            $error = sprintf('%s (%d)', socket_strerror($errno), $errno);
            trigger_error($error, E_USER_ERROR);

            $result["result"]="error";
            echo  json_encode($result);
            exit();
        } else {
            $sendnewtelnum =$data["sendnewtelnum"];
            $phone_num1 = $data["branch_tel"];
            if( strlen($phone_num1) < 11 ){
                $tlen = 11-strlen($phone_num1) ;
                $blankstr="";
                for($i=0;$i< $tlen;$i++){
                    $blankstr =$blankstr." ";
                }
                $phone_num1 =$phone_num1.$blankstr;
            }

            $packet_id = '2501';#2501: 번호사용등록요구 패킷/ 2502 : 번호사용해지요구/ 2600:상태체크요구  -  3501,3502,3600
            $company_id = '443';
            $system_id = '000';
            $sequence = '                    '; #고정 20칸
            $result_msg = '  '; #고정 2칸
            $method = '1';
            $onse_number = $sendnewtelnum."    " ; //할당이 0001~9999 까지
            $phone_num1 = $phone_num1."   "; //업체번호 1
            $phone_num2 = '               '; //업체번호2
            $onse_number2 = '               ';
            $phone2_num1 = '               ';
            $phone2_num2 = '               ';
            $use_flag = 1;


            $msg = '#'.$packet_id.$company_id.$system_id.$sequence.$result_msg.$method.$onse_number.$phone_num1.$phone_num2.$onse_number2.$phone2_num1.$phone2_num2.$use_flag.'$';

            socket_write($socket, $msg, strlen($msg)) or print("Could not send data to server\n");

            $result = socket_read ($socket, 1024) or print("Could not read server response\n");

            // :#2501443000                    01105037961111101030043970                                                            1$
            //Reply From Server :#3501450000 001050379610001 0262490202 1$
            socket_close($socket);


            $return_v = $this->PartnerMember_model->proctel050($data);

            $return["result"]="y";
            echo  json_encode($return);
            exit();
        }
    }

    // 기업 지점 050 번호 삭제
    public function sendelnewtel(){
        $data["emode"] =$this->input->get('emode');
        $data["bserial"] =$this->input->get('bserial');
        $data["deltel"] =$this->input->get('deltel');

        $branch_tel =$this->input->get('branch_tel');
        $branch_tel = preg_replace("/[^0-9]*/s", "", $branch_tel);
        $data["branch_tel"] = $branch_tel;

        $data["datamode"] =$this->input->get('datamode');

        if($data["datamode"]=="comdel"){


        }else{
            $data["datamode"] ="del";

        }

        $socket=socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if ($socket === false) {
            $errno = socket_last_error();
            $error = sprintf('%s (%d)', socket_strerror($errno), $errno);
            trigger_error($error, E_USER_ERROR);

            $result["result"]="error";
            echo  json_encode($result);
            exit();
        }

        $address = '203.240.244.116';
        if ( !socket_connect($socket, $address, 61311) ) {
            $errno = socket_last_error($socket);
            $error = sprintf('%s (%d)', socket_strerror($errno), $errno);
            trigger_error($error, E_USER_ERROR);

            $result["result"]="error";
            echo  json_encode($result);
            exit();
        } else {
            $sendnewtelnum = $data["deltel"];
            $phone_num1 = $data["branch_tel"];
            if( strlen($phone_num1) < 11 ){
                $tlen = 11-strlen($phone_num1) ;
                $blankstr="";
                for($i=0;$i< $tlen;$i++){
                    $blankstr =$blankstr." ";
                }
                $phone_num1 =$phone_num1.$blankstr;
            }

            $packet_id = '2502';#2501: 번호사용등록요구 패킷/ 2502 : 번호사용해지요구/ 2600:상태체크요구  -  3501,3502,3600
            $company_id = '443';
            $system_id = '000';
            $sequence = '                    '; #고정 20칸
            $result_msg = '  '; #고정 2칸
            $method = '1';
            $onse_number = $sendnewtelnum."    " ; //할당이 0001~9999 까지
            $phone_num1 = $phone_num1."   "; //업체번호 1
            $phone_num2 = '               '; //업체번호2
            $onse_number2 = '               ';
            $phone2_num1 = '               ';
            $phone2_num2 = '               ';
            $use_flag = 1;


            $msg = '#'.$packet_id.$company_id.$system_id.$sequence.$result_msg.$method.$onse_number.$phone_num1.$phone_num2.$onse_number2.$phone2_num1.$phone2_num2.$use_flag.'$';


            socket_write($socket, $msg, strlen($msg)) or die("Could not send data to server\n");
            $result = socket_read ($socket, 1024) or die("Could not read server response\n");
            //echo "Reply From Server del :".$result;    //Reply From Server :#3501450000 001050379610001 0262490202 1$
            socket_close($socket);

            $return_v = $this->PartnerMember_model->proctel050($data);
            $return["result"]="y";

            if ($this->input->get('tel050_part') === 'api')    // API 업체의 050 연결
                $this->api_affiliate_branch_model->update_tel050($data['bserial'], '');

            echo  json_encode($return);
            exit();
        }
    }

    /**
     * 업체특이사항 저장
     */
    private function update_uniqueness()
    {
        $uniqueness = $this->input->post('uniqueness');
        $branch_idx = $this->input->post('branchIdx');

        $this->partners_affiliate_model->update_uniqueness($branch_idx, $uniqueness);

        echo json_encode(['result' => 1]);
    }
}
