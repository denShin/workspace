<?php
/**
 * PartnerinfoimageProc.php -파트너스 기업 이미지 등록처리
 */

class PartnerinfoimageProc   extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('partners/PartnerMember_model');
        $this->load->library('Customfunc');
        $this->load->library('Aws_s3');


        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        $demode =$this->input->get('emode');
        if($demode=="")  $demode =$this->input->post('emode');


        switch($demode) {

            case "imagepublish" : $this->setImagePublish(); break;
        }
    }

    
    // 이미지 배포처리 등록,수정
    public function setImagePublish(){


        $data["filename"] =$this->input->get('filename');
        $data["publish_yn"] =$this->input->get('publish_yn');

        $return_v = $this->PartnerMember_model->procPublishimage($data["publish_yn"] , $data["filename"] );

    }


}
