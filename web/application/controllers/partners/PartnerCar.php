<?php
/**
 * PartnerCar.php -파트너스 자동차 관리 ( 사용안함 X)
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class PartnerCar extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('partners/PartnerCar_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);

        switch($ptype) {
            case "w" : $this->loadPartnerCarWrite(); break;
            default : $this->loadPartnerCarList(); break;
        }
    }

    public function loadPartnerCarList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);
        $mem_stats = $this->input->get('mem_stats', TRUE);
        $listitle="차량 정보 관리";

        if(!$sp) $sp="";
        if(!$sv) $sv="";

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));


        $rowresult = $this->PartnerCar_model->getPartnerCarPageList($page, 20, $sp, $sv);
        $total = $this->PartnerCar_model->getPartnerCarCount($sp, $sv);

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/partners/PartnerCar";
        $config['suffix']     ="&mem_stats=".$mem_stats."&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];
        foreach($rowresult as $entry)
        {
            $arr_datalist["SERIAL"]=$entry->serial;
            $arr_datalist["NAME"]=$entry->name;
            $arr_datalist["OWNER"]=$entry->owner;
            $arr_datalist["ownertel"]=$entry->ownerTel;
            $arr_datalist["logo"]=$entry->logo;
            $arr_datalist["regdate"]=$entry->regdate;
            $arr_datalist["contact"]=$entry->contact;
            $arr_datalist["carmore"]=$entry->carmore;
            $arr_datalist["sub"]=$entry->sub;
            $arr_datalist["sub_email"]=$entry->sub_email;
            $arr_datalist["test_check"]=$entry->test_check;
            $arr_datalist["state"]=$entry->state;

            $regdate =$entry->regdate;
            $arr_datalist["regdatev"] =$regdate;
            $arr_datalist["regdate"] =$this->customfunc->get_dateformat($regdate);


            $arr_alldatalist[]=$arr_datalist;
        }

        $data["list"]=$arr_alldatalist;
        $data["mem_stats"]=$mem_stats;
        $data["per_page"]=$per_page;
        $data["listitle"]=$listitle;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->LOGINID;

        $this->load->view('partners/partnercarlist', array('data'=>$data));
        $this->load->view('footer');
    }

    public function loadPartnerCarWrite()
    {

        $pemail = $this->input->get('admin_email', TRUE);

        if($pemail !=""){
            $data["emode"]="edit";
            $arr_PartnerCardata = $this->PartnerCar_model->getPartnerCarDetail($pemail);

            $data["admin_name"] =$arr_PartnerCardata["admin_name"];
            $data["admin_email"] =$arr_PartnerCardata["admin_email"];
            $data["admin_pwd"]=$arr_PartnerCardata["admin_pwd"];
            $data["admin_tel"] =$arr_PartnerCardata["admin_tel"];
            $data["team_code"] =$arr_PartnerCardata["team_code"];
            $data["admin_stats"] =$arr_PartnerCardata["admin_stats"];
            $data["member_grade"] =$arr_PartnerCardata["member_grade"];
            $data["config_yn"] =$arr_PartnerCardata["config_yn"];
            $data["offwork_yn"] =$arr_PartnerCardata["offwork_yn"];
            $data["buyoffer_yn"] =$arr_PartnerCardata["buyoffer_yn"];
            $data["dmaster_yn"] =$arr_PartnerCardata["master_yn"];
            $data["buyofferdeposit_yn"] =$arr_PartnerCardata["buyofferdeposit_yn"];
            $data["upmoneyoffer_yn"] =$arr_PartnerCardata["upmoneyoffer_yn"];
            $data["ordermng_yn"] =$arr_PartnerCardata["ordermng_yn"];
            $data["calc_yn"] =$arr_PartnerCardata["calc_yn"];
            $data["teamoption"]= $this->customfunc->select_team($data["team_code"]);

        }else{
            $data["emode"]="new";

            $data["admin_name"] ="";
            $data["admin_email"] ="";
            $data["admin_pwd"]= "";
            $data["admin_tel"] ="";
            $data["team_code"] ="";
            $data["admin_stats"] ="";
            $data["member_grade"] ="";
            $data["config_yn"] ="";
            $data["offwork_yn"] ="";
            $data["buyoffer_yn"] ="";
            $data["dmaster_yn"] ="";
            $data["buyofferdeposit_yn"] ="";
            $data["upmoneyoffer_yn"] ="";
            $data["branch_code"] ="";
            $data["ordermng_yn"] ="";
            $data["calc_yn"] ="";

            $data["teamoption"]= $this->customfunc->select_team("");
        }

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('carmore/PartnerCarwrite', array('data'=>$data));
        $this->load->view('footer');
    }
}
