<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 050 결번관리 리스트 ,등록,수정,보기 컨트롤
 *
 * @property Customfunc            $customfunc
 * @property Workspace_common_func $workspace_common_func
 *
 * @property PartnerMember_model   $PartnerMember_model
 * @property Onse_model            $onse_model
 */

class Tel050manage extends WS_Controller {
    private $arr_permission;
    private $login_id;

    const PAGE_ROW = 20;

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->library('Workspace_common_func');
        $this->load->model('partners/PartnerMember_model');
        $this->load->model('partners/onse/onse_model');

        $this->login_id = $this->session->userdata('admin_id');

        if ($this->login_id === "")
        {
            echo "<script>location.href='/adminmanage/Login'</script>";

        }
    }

    /**
     * 상단메뉴 퍼미션
     *
     * @param  void
     * @return void
     */
    public function index()
    {
        $this->arr_permission = $this->customfunc->get_permissionArray($this->login_id);

        $this->load_050tel_inventory();

    }

    /**
     * 050 전체리스트, 결번리스트, 골드넘버
     *
     * @param  void
     * @return void
     */
    public function load_050tel_inventory()
    {
        $this->load->helper('url'); // load the helper first

        $per_page     = $this->input->get('per_page', TRUE);
        $search_param = $this->workspace_common_func->check_false_value($this->input->get('sp', TRUE), TRUE);
        $search_value = $this->workspace_common_func->check_false_value($this->input->get('sv', TRUE), TRUE);
        $tel_type     = $this->input->get('teltype', TRUE);    # 050 보여질 타입들 (결번/골드/전체 등..)

        if ($per_page < self::PAGE_ROW)
            $per_page = 0;

        $page = ($per_page/self::PAGE_ROW) +1;

        if ($tel_type === "")
            $tel_type = "expire";

        $this->load->view('head', ['data' => $this->arr_permission]);



        $total_cnt     = $this->PartnerMember_model->getTelmanageCount($tel_type, $search_param, $search_value);
        $ret_inventory = $this->PartnerMember_model->getTelmanagePageList($tel_type, $page, self::PAGE_ROW, $search_param, $search_value);

        # CI >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');

        $config = $this->config->item('pagination');
        $config['total_rows']            = $total_cnt;    # 게시물 총 수
        $config['per_page']              = self::PAGE_ROW;    # 게시물 출력수
        $config['base_url']              = $config['base_url']."/partners/Tel050manage?teltype=".$tel_type;
        $config['suffix']                = "&sp=".$search_param."&sv=".$search_value;
        $config['use_global_url_suffix'] = false;
        $config['reuse_query_string']    = false;

        $this->pagination->initialize($config);

        $pagination = $this->pagination->create_links();
        $inventory  = [];

        foreach ($ret_inventory as $entry)
        {
            $insert_obj = [];

            $fk_serial   = $entry->fk_serial;
            $tel050_part = $entry->tel050_part;

            $insert_obj['tel050_idx']        = $entry->tel050_idx;
            $insert_obj['tel050_deldate']    = $entry->tel050_deldate;
            $insert_obj['tel050_matnumber']  = $this->workspace_common_func->format_tel($entry->tel050_matnumber);
            $insert_obj['tel050_expiredate'] = date('Y-m-d', $entry->expire);
            $insert_obj['tel050_number']     = $this->workspace_common_func->format_tel($entry->tel050_number);

            if ($fk_serial !== "" && $tel050_part !== "")
            {
                $company_info = $this->PartnerMember_model->getTelcompanyinfBy050idx($fk_serial , $tel050_part);
                $branch_tel   = $this->workspace_common_func->format_tel($entry->tel050_matnumber);

                $insert_obj['serial']        = $entry->serial;
                $insert_obj['tel']           = $branch_tel;
                $insert_obj['state']         = "매칭완료";

                if ($tel050_part === "out")
                {
                    $insert_obj['business_name']    = $company_info['business_name'];
                    $insert_obj['service_city']     = $this->customfunc->get_areatxt($company_info['service_city'], "sido");
                    $insert_obj['service_location'] = $this->customfunc->get_areatxt($company_info['service_location'], "gugun");
                    $insert_obj['service_time']     = $company_info['service_time'];
                    $insert_obj['companytype']      = "<span style='color: red'>외부업체</span>";

                }
                elseif ($tel050_part === "in")
                {
                    $insert_obj['business_name'] = $company_info["company_name"];
                    $insert_obj['service_city']  = $company_info["address"];
                    $insert_obj['service_time']  = $company_info["openTime"]." ~ ".$company_info["closeTime"];
                    $insert_obj['companytype']   = "<span style='color: blue'>회원업체</span>";

                }
                elseif ($tel050_part === 'api')
                {
                    $insert_obj['business_name'] = $company_info["company_name"];
                    $insert_obj['service_city']  = $company_info["address"];
                    $insert_obj['service_time']  = $company_info["waab_open_time"]." ~ ".$company_info["waab_close_time"];
                    $insert_obj['companytype']   = "<span style='color: green'>API업체</span>";
                }

            }
            else
            {
                $insert_obj['business_name'] = "-";
                $insert_obj['state']         = "매칭대기";

            }

            array_push($inventory, $insert_obj);

        }

        $data['abnormalInventory'] = $this->load_050tel_abnormal_inventory();
        $data['list']              = $inventory;
        $data['per_page']          = $per_page;
        $data['pagination']        = $pagination;
        $data['loginid']           = $this->login_id;
        $data['teltype']           = $tel_type;

        if ($tel_type === "expire")
        {
            $data['pagetitle'] = "050 결번관리";
            $this->load->view("partners/tel050outlist", ['data'=>$data]);
        }
        elseif ($tel_type === "use")
        {
            $data['pagetitle'] = "050 전체리스트";
            $this->load->view("partners/tel050numberlist", ['data'=>$data]);
        }
        else
        {
            $data['pagetitle'] = "050 골드넘버";
            $this->load->view("partners/goldnumberlist", ['data'=>$data]);
        }

        $this->load->view('footer');

    }

    /**
     * 050 연결을 했지만 연결상태가 정상이 아닌 리스트
     *
     * @param  void
     * @return array
     */
    private function load_050tel_abnormal_inventory()
    {
        $abnormal_inventory = [];

        $active_inventory = $this->onse_model->load_onse_active_inventory();
        foreach ($active_inventory as $active_obj)
        {
            $onse_matching_branch_tel = $this->workspace_common_func->remain_only_number($active_obj['onseMathBranchTel']);
            $current_branch_tel       = $this->workspace_common_func->remain_only_number($active_obj['branchTel']);

            if ($onse_matching_branch_tel !== $current_branch_tel)
                array_push($abnormal_inventory, [
                    'affiName'          => $active_obj['affiName'],
                    'onseMathBranchTel' => $this->workspace_common_func->format_tel($active_obj['onseMathBranchTel']),
                    'branchTel'         => $this->workspace_common_func->format_tel($active_obj['branchTel'])
                ]);

        }

        return $abnormal_inventory;
    }
}
