<?php
/**
 * CallpartnerProc.php -전화걸기 업체등록,수정처리 컨트롤 
 */
class CallpartnerProc  extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('partners/PartnerMember_model');


    }

    public function index()
    {
        $demode =$this->input->get('emode');
        if($demode==""){
            $demode =$this->input->post('emode');
        }
        switch($demode) {

            case "setcallpartner" : $this->setcallpartner(); break;
            case "setallstats" : $this->setallstats(); break;
        }
    }

    // 전화걸기 업체 등록,수정처리
    public function setcallpartner()
    {


        $data["datamode"] =$this->input->post('datamode');
        $data["serial"] =$this->input->post('serial');
        $data["business_name"] =$this->input->post('business_name');
        $data["service_city"] =$this->input->post('service_city');
        $data["service_time"] =$this->input->post('service_time');
        $arrservice_location =$this->input->post('service_location');
        $data["service_location"] = join(",",$arrservice_location);
        $data["tel"] =$this->input->post('tel');
        $data["service_address"] =$this->input->post('service_address');
        $data["state"] =$this->input->post('state');


        $return_v = $this->PartnerMember_model->setcallpartner( $data);
        echo "<script>location.href='/partners/Callpartner'</script>";
        exit();

    }



    // 전화걸기 업체 상태 변경 처리
    public function setchangestats()
    {

        $data["emode"] =$this->input->post('emode');
        $data["comserial"] =$this->input->post('comserial');
        $data["stats"] =$this->input->post('stats');

        $return_v = $this->PartnerMember_model->chagecomstats( $data);
        echo "<script>location.href='/partners/PartnerMember'</script>";
        exit();
    }





}
