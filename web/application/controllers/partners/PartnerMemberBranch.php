<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 파트너스 기업지점 리스트,등록수정 컨트롤러
 *
 * @author Eric
 * @author Den <den@tm2.kr> 19.05.28
 *
 * @property CI_Session $session
 * @property WS_Input   $input
 *
 * @property Customfunc            $customfunc
 * @property Workspace_common_func $workspace_common_func
 *
 * @property PartnerMember_model $PartnerMember_model
 *
 */

class PartnerMemberBranch extends WS_Controller {
    private $permission_arr;
    private $login_id;


    function __construct()
    {
        parent::__construct();

        $this->load->database();

        $this->load->library('pagination');
        $this->load->library('Customfunc');

        $this->load->model('partners/PartnerMember_model');

        $this->login_id = (string)$this->session->userdata('admin_id');

        if ($this->login_id === "")
            echo "<script>location.href='/adminmanage/Login'</script>";

    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->permission_arr = $this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $company_idx = $this->customfunc->SQL_Injection($this->input->get('companyserial'));
        $branch_idx  = $this->customfunc->SQL_Injection($this->input->get('branchserial'));
        $ptype       = $this->input->get('ptype', TRUE);

        switch($ptype) {
            case "code":
                $this->getBranchdata($company_idx);
                break;
            case "branchdtllist":
                $this->loadBranchdtllist($branch_idx);
                break;
            case "initpwd":
                $this->setInitPwd();
                break;
            case "w":
                $this->loadPartnerMemberBranchWrite();
                break;
            default:
                $this->loadPartnerMemberBranchList();
                break;
        }
    }

    // 지점 로그인 비밀번호 초기화
    public function setInitPwd()
    {
        $mem_id = $this->input->get('mem_id', TRUE);
        $mem_pwd = password_hash("aaa111",PASSWORD_DEFAULT);
        $this->PartnerMember_model->setInitPwd($mem_id,$mem_pwd);

        $this->loadPartnerMemberBranchList();
    }

    // 지점 직원 관리-리스트 팝업
    public function loadBranchdtllist($branch_serial)
    {

        $returnarr = $this->PartnerMember_model->getBranchDtlList( $branch_serial);

        $arr_alldatalist=[];

        foreach($returnarr as $entry)
        {
            $arr_datalist["serial"]=$entry["serial"];
            $arr_datalist["rentCompany_serial"]=$entry["rentCompany_serial"];
            $arr_datalist["branchName"]=$entry["branchName"];
            $arr_datalist["post"]=$entry["post"];
            $arr_datalist["address"]=$entry["address"];
            $arr_datalist["detailAddress"]=$entry["detailAddress"];
            $arr_datalist["tel"]=$entry["tel"];


            $arr_datalist["authority"]=$entry["authority"];
            $arr_datalist["mem_serial"]=$entry["mem_serial"];
            $arr_datalist["mem_id"]=$entry["mem_id"];
            $arr_datalist["mem_name"]=$entry["mem_name"];
            $arr_datalist["rentCompany_branch_serial"]=$entry["rentCompany_branch_serial"];

            $arr_datalist["branchOwner"]=$entry["branchOwner"];
            $arr_datalist["branchOwnerTel"]=$this->customfunc->add_phonehyphen($entry["branchOwnerTel"]);

            $arr_datalist["reservation_receive_number"]=$entry["reservation_receive_number"];
            $arr_datalist["reservation_receive_email"]=$entry["reservation_receive_email"];
            $arr_datalist["regdate"]=$entry["regdate"];
            $arr_datalist["openTime"]=$entry["openTime"];
            $arr_datalist["closeTime"]=$entry["closeTime"];
            $arr_datalist["state"]=$entry["state"];
            $arr_datalist["service_area"]=$entry["service_area"];
            $arr_datalist["branch_authority"]=$entry["branch_authority"];
            $arr_datalist["latitude"]=$entry["latitude"];
            $arr_datalist["longitude"]=$entry["longitude"];
            $arr_datalist["delivery_oneway"]=$entry["delivery_oneway"];
            $arr_datalist["delivery_round"]=$entry["delivery_round"];
            $arr_datalist["delivery_area"]=$entry["delivery_area"];
            $arr_datalist["period_closed"]=$entry["period_closed"];
            $arr_datalist["period_closed_solar"]=$entry["period_closed_solar"];
            $arr_datalist["temporary_closed"]=$entry["temporary_closed"];
            $arr_datalist["fax"]=$entry["fax"];
            $arr_datalist["register_number"]=$entry["register_number"];
            $arr_datalist["bankbook"]=$entry["bankbook"];
            $arr_datalist["account_number"]=$entry["account_number"];
            $arr_datalist["bank"]=$entry["bank"];
            $arr_datalist["account_holder"]=$entry["account_holder"];
            $arr_datalist["work_time_open"]=$entry["work_time_open"];
            $arr_datalist["work_time_close"]=$entry["work_time_close"];
            $arr_datalist["monday"]=$entry["monday"];
            $arr_datalist["tuesday"]=$entry["tuesday"];
            $arr_datalist["wednesday"]=$entry["wednesday"];
            $arr_datalist["thursday"]=$entry["thursday"];
            $arr_datalist["friday"]=$entry["friday"];
            $arr_datalist["saturday"]=$entry["saturday"];
            $arr_datalist["sunday"]=$entry["sunday"];
            $arr_datalist["weekend"]=$entry["weekend"];
            $arr_datalist["jeju_flag"]=$entry["jeju_flag"];
            $arr_datalist["shuttle_address"]=$entry["shuttle_address"];
            $arr_datalist["shuttle_race_range"]=$entry["shuttle_race_range"];
            $arr_datalist["shuttle_spend_time"]=$entry["shuttle_spend_time"];
            $arr_datalist["special_location_cover"]=$entry["special_location_cover"];
            $arr_datalist["special_peakseason_minimum_reserve_date"]=$entry["special_peakseason_minimum_reserve_date"];
            $arr_datalist["special_peakseason_start"]=$entry["special_peakseason_start"];
            $arr_datalist["special_peakseason_end"]=$entry["special_peakseason_end"];
            $arr_datalist["pricetable_type"]=$entry["pricetable_type"];
            $arr_datalist["standard_term_modify_check"]=$entry["standard_term_modify_check"];
            $arr_datalist["settlement_rental_rate"]=$entry["settlement_rental_rate"];
            $arr_datalist["settlement_cdw_rate"]=$entry["settlement_cdw_rate"];
            $arr_datalist["company_name"]=$entry["company_name"];
            $arr_datalist["mem_regdate"]=$entry["mem_regdate"];


            if($entry->carmore_month_available=="1"){
                $arr_datalist["carmore_month_availablestr"]="승인";
            }else {
                $arr_datalist["carmore_month_availablestr"]="미승인";
            }


            $regdate =$entry->regdate;
            $arr_datalist["regdatev"] =$regdate;
            $arr_datalist["regdate"] =$this->customfunc->get_dateformat($regdate,"regs");

            if($entry->authority =="1"){
                $arr_datalist["authoritystr"] ="승인";
            }else{
                $arr_datalist["authoritystr"] ="<span style='color:red'>승인대기</span>";
            }

            $arr_alldatalist[]=$arr_datalist;
        }

        $data["list"]=$arr_alldatalist;
        $data["loginid"]=$this->login_id;

        $this->load->view('partners/partnerpopbranchlist', array('data'=>$data));
    }

    
    //기업 지점 관리 리스트 
    public function loadPartnerMemberBranchList()
    {

        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);
        $mem_stats = $this->input->get('mem_stats', TRUE);
        if(!$mem_stats) $mem_stats="y";
        if($mem_stats =="y"){
            $listitle="기업 지점 관리";
        }else if($mem_stats =="x"){
            $listitle="기업 탈퇴 관리";
        }else if($mem_stats =="n"){
            $listitle="기업 차단 회원관리";
        }

        if(!$sp) $sp="";
        if(!$sv) $sv="";


        $head_param = [
            'data'    => $this->permission_arr,
            'library' => ['toastr']
        ];

        $this->load->view('head', $head_param);


        $rowresult = $this->PartnerMember_model->getPartnerMemberBranchPageList($page, 20, $sp, $sv);
        $total = $this->PartnerMember_model->getPartnerMemberBranchCount($sp, $sv);

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/partners/PartnerMemberBranch?mem_stats=".$mem_stats."&sp=".$sp."&sv=".$sv;
        $config['suffix']     ="";
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];

        foreach($rowresult as $entry)
        {
            $arr_datalist["serial"]=$entry->serial;
            $arr_datalist["rentCompany_serial"]=$entry->rentCompany_serial;
            $arr_datalist["branchName"]=$entry->branchName;
            $arr_datalist["post"]=$entry->post;
            $arr_datalist["address"]=$entry->address;
            $arr_datalist["detailAddress"]=$entry->detailAddress;
            $arr_datalist["tel"]=$entry->tel;

            $arr_datalist["authority"]=$entry->authority;
            $arr_datalist["mem_serial"]=$entry->mem_serial;
            $arr_datalist["mem_id"]=$entry->mem_id;
            $arr_datalist["mem_name"]=$entry->mem_name;
            $arr_datalist["rentCompany_branch_serial"]=$entry->rentCompany_branch_serial;

            $arr_datalist["branchOwner"]=$entry->branchOwner;
            $arr_datalist["branchOwnerTel"]=$this->customfunc->add_phonehyphen($entry->branchOwnerTel);

            $arr_datalist["reservation_receive_number"]=$entry->reservation_receive_number;
            $arr_datalist["reservation_receive_email"]=$entry->reservation_receive_email;
            $arr_datalist["regdate"]=$entry->regdate;
            $arr_datalist["openTime"]=$entry->openTime;
            $arr_datalist["closeTime"]=$entry->closeTime;
            $arr_datalist["state"]=$entry->state;
            $arr_datalist["service_area"]=$entry->service_area;
            $arr_datalist["branch_authority"]=$entry->branch_authority;
            $arr_datalist["latitude"]=$entry->latitude;
            $arr_datalist["longitude"]=$entry->longitude;
            $arr_datalist["delivery_oneway"]=$entry->delivery_oneway;
            $arr_datalist["delivery_round"]=$entry->delivery_round;
            $arr_datalist["delivery_area"]=$entry->delivery_area;
            $arr_datalist["period_closed"]=$entry->period_closed;
            $arr_datalist["period_closed_solar"]=$entry->period_closed_solar;
            $arr_datalist["temporary_closed"]=$entry->temporary_closed;
            $arr_datalist["fax"]=$entry->fax;
            $arr_datalist["register_number"]=$entry->register_number;
            $arr_datalist["bankbook"]=$entry->bankbook;
            $arr_datalist["account_number"]=$entry->account_number;
            $arr_datalist["bank"]=$entry->bank;
            $arr_datalist["account_holder"]=$entry->account_holder;
            $arr_datalist["work_time_open"]=$entry->work_time_open;
            $arr_datalist["work_time_close"]=$entry->work_time_close;
            $arr_datalist["monday"]=$entry->monday;
            $arr_datalist["tuesday"]=$entry->tuesday;
            $arr_datalist["wednesday"]=$entry->wednesday;
            $arr_datalist["thursday"]=$entry->thursday;
            $arr_datalist["friday"]=$entry->friday;
            $arr_datalist["saturday"]=$entry->saturday;
            $arr_datalist["sunday"]=$entry->sunday;
            $arr_datalist["weekend"]=$entry->weekend;
            $arr_datalist["jeju_flag"]=$entry->jeju_flag;
            $arr_datalist["shuttle_address"]=$entry->shuttle_address;
            $arr_datalist["shuttle_race_range"]=$entry->shuttle_race_range;
            $arr_datalist["shuttle_spend_time"]=$entry->shuttle_spend_time;
            $arr_datalist["special_location_cover"]=$entry->special_location_cover;
            $arr_datalist["special_peakseason_minimum_reserve_date"]=$entry->special_peakseason_minimum_reserve_date;
            $arr_datalist["special_peakseason_start"]=$entry->special_peakseason_start;
            $arr_datalist["special_peakseason_end"]=$entry->special_peakseason_end;
            $arr_datalist["pricetable_type"]=$entry->pricetable_type;
            $arr_datalist["standard_term_modify_check"]=$entry->standard_term_modify_check;
            $arr_datalist["settlement_rental_rate"]=$entry->settlement_rental_rate;
            $arr_datalist["settlement_cdw_rate"]=$entry->settlement_cdw_rate;
            $arr_datalist["company_name"]=$entry->company_name;
            $arr_datalist["mem_regdate"]=$entry->mem_regdate;
            $arr_datalist["introcode"]=$entry->introcode;
            $arr_datalist["intromsg"]=$entry->intromsg;
            $arr_datalist["uniqueness"] = $entry->nrcb_etc_uniqueness;
            $arr_datalist["enterDate"] = $entry->nrcb_enter_carmore_date;

            $arr_datalist["mainimage"]=$entry->mainimage;
            if(strlen($arr_datalist["mainimage"]) < 3 ){
                $fullurl="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/partners/noimage.png";
            }else{
                $fullurl="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/partners/mainimg_".$entry->serial."/".$entry->mainimage;
            }

            $arr_datalist["fullurl"]=$fullurl;
            $arr_datalist["subcnt"]=$entry->subcnt;


            $origin050 = $this->customfunc->get_tel050number($entry->serial,"in");

            $arr_datalist["tel050"]=preg_replace("/([0-9]{4})([0-9]{4})([0-9]{4})/", "$1-$2-$3", $origin050);
            $arr_datalist["origintel050"]=  $origin050;

            if($entry->carmore_month_available=="1"){
                $arr_datalist["carmore_month_availablestr"]="승인";
            }else {
                $arr_datalist["carmore_month_availablestr"]="미승인";
            }

            if($entry->carmore_normal_available=="1"){
                $arr_datalist["carmore_normal_availablestr"]="승인";
            }else {
                $arr_datalist["carmore_normal_availablestr"]="미승인";
            }



            $regdate =$entry->mem_regdate;
            $arr_datalist["regdatev"] =$regdate;

            $arr_datalist["regdate"] =$this->customfunc->get_dateformat($regdate,"regs");

            if($entry->authority =="1"){
                $arr_datalist["authoritystr"] ="승인";
            }else{
                $arr_datalist["authoritystr"] ="<span style='color:red'>승인대기</span>";
            }

            $arr_alldatalist[]=$arr_datalist;
        }

        $data["list"]=$arr_alldatalist;
        $data["mem_stats"]=$mem_stats;
        $data["per_page"]=$per_page;
        $data["listitle"]=$listitle;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->login_id;
        $data["new050"] =$this->customfunc->get_suggest050number("in");

        $this->load->view('partners/partnermemberbranchlist', array('data'=>$data));
        $this->load->view('footer');
    }

    //기업 지점관리 등록,수정
    public function loadPartnerMemberBranchWrite()
    {

        $pemail = $this->input->get('admin_email', TRUE);

        if($pemail !=""){
            $data["emode"]="edit";
            $arr_PartnerMemberdata = $this->PartnerMember_model->getPartnerMemberDetail($pemail);

            $data["admin_name"] =$arr_PartnerMemberdata["admin_name"];
            $data["admin_email"] =$arr_PartnerMemberdata["admin_email"];
            $data["admin_pwd"]=$arr_PartnerMemberdata["admin_pwd"];
            $data["admin_tel"] =$arr_PartnerMemberdata["admin_tel"];
            $data["team_code"] =$arr_PartnerMemberdata["team_code"];
            $data["admin_stats"] =$arr_PartnerMemberdata["admin_stats"];
            $data["member_grade"] =$arr_PartnerMemberdata["member_grade"];
            $data["config_yn"] =$arr_PartnerMemberdata["config_yn"];
            $data["offwork_yn"] =$arr_PartnerMemberdata["offwork_yn"];
            $data["buyoffer_yn"] =$arr_PartnerMemberdata["buyoffer_yn"];
            $data["dmaster_yn"] =$arr_PartnerMemberdata["master_yn"];
            $data["buyofferdeposit_yn"] =$arr_PartnerMemberdata["buyofferdeposit_yn"];
            $data["upmoneyoffer_yn"] =$arr_PartnerMemberdata["upmoneyoffer_yn"];
            $data["ordermng_yn"] =$arr_PartnerMemberdata["ordermng_yn"];
            $data["calc_yn"] =$arr_PartnerMemberdata["calc_yn"];
            $data["teamoption"]= $this->customfunc->select_team($data["team_code"]);

        }else{
            $data["emode"]="new";

        }

        $this->load->view('head', array('data'=>$this->permission_arr));
        $this->load->view('partners/partnermemberbranchwrite', array('data'=>$data));
        $this->load->view('footer');
    }


    // 기업 지점 데이터 ajax 를 통해 받아오기 json print
    public function getBranchdata($companyserial){

        // get device detail
        $returnarr = $this->PartnerMember_model->getBranchList( $companyserial);
        echo  json_encode($returnarr);

    }



}
