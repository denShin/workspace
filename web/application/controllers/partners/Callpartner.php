<?php
/**
 * Callpartner.php -전화걸기 업체 리스트, 등록,수정 컨트롤러
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class Callpartner extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('partners/PartnerMember_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);

        switch($ptype) {
            case "getgugundata":$this->getgugundata();break;
            case "w" : $this->loadCallPartnerWrite(); break;
            default : $this->loadCallPartnerList(); break;
        }
    }

    // 전화걸기업체 리스트 
    public function loadCallPartnerList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;


        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);

        if(!$sp) $sp="";
        if(!$sv) $sv="";

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));


        $rowresult = $this->PartnerMember_model->getCallPartnerPageList($page, 20, $sp, $sv);
        $total = $this->PartnerMember_model->getCallPartnerCount($sp, $sv);

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/partners/Callpartner";
        $config['suffix']     ="&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];

        foreach($rowresult as $entry)
        {

            $arr_datalist["serial"]=$entry->serial;
            $arr_datalist["business_name"]=$entry->business_name;
            $arr_datalist["service_city"]= $this->customfunc->get_areatxt($entry->service_city,"sido");
            $arr_datalist["service_location"]= $this->customfunc->get_areatxt($entry->service_location,"gugun");

            $arr_datalist["tel050_matnumber"]=$this->customfunc->format_phone($entry->tel050_matnumber);
            $arr_datalist["tel050_number"]=$this->customfunc->format_phone($entry->tel050_number);
            $arr_datalist["tel050_useyn"]=$entry->tel050_useyn;
            $arr_datalist["tel050_goldyn"]=$entry->tel050_goldyn;
            $arr_datalist["service_address"]=$entry->service_address;
            $arr_datalist["service_time"]=$entry->service_time;
            $arr_datalist["tel"]=$entry->tel;
            $arr_datalist["state"]=$entry->state;

            if($entry->state=="0"){
                $arr_datalist["statestr"]= "<span style='color:red'>비활성</span>";
            }else if($entry->state=="1"){
                $arr_datalist["statestr"]= "<span style='color:blue'>활성</span>";
            }

            $arr_alldatalist[]=$arr_datalist;
        }

        $data["list"]=$arr_alldatalist;
        $data["per_page"]=$per_page;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->LOGINID;

        $data["new050"] =$this->customfunc->get_suggest050number("out");

        $this->load->view('partners/callpartnerlist', array('data'=>$data));
        $this->load->view('footer');
    }

    // 전화걸기 업체 등록,수정
    public function loadCallPartnerWrite()
    {
        $serial = $this->input->get('serial', TRUE);


        if($serial !=""){
            $data["datamode"]="edit";
            $data["serial"]=$serial;

            $entry = $this->PartnerMember_model->getCallPartnerDtl($serial);

            $data["serial"]=$entry->serial;
            $data["business_name"]=$entry->business_name;

            $data["service_location"]=$entry->service_location;

            $data["cityoption"] =$this->customfunc->get_areaselectoption($entry->service_location,'select','sido',"");
            $data["townoption"] =$this->customfunc->get_areaselectoption($entry->service_location,'option','gugun',$entry->service_city);

            $data["service_time"]=$entry->service_time;
            $data["service_address"]=$entry->service_address;
            $data["tel"]=$entry->tel;
            $data["state"]=$entry->state;

        }else{

            $data["datamode"]="new";

            $data["cityrow"] =$this->customfunc->get_areainfo('sido','');

            $data["cityoption"] =$this->customfunc->get_areaselectoption('','select','sido','');
        }

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('partners/callpartnerwrite', array('data'=>$data));
        $this->load->view('footer');
    }


    //시도,구군 ajax data
    public function getgugundata(){

        // get device detail

        $service_city = $this->input->get('service_city', TRUE);
        $service_location = $this->input->get('service_location', TRUE);
        $gugunoption =$this->customfunc->get_areaselectoption($service_location,'checbox','gugun',$service_city);

        echo  $gugunoption ;

    }

}
