<?php
/**
 * PartnerBoard.php -파트너스 공지사항 리스트,등록,수정,보기 컨트롤
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class PartnerNoticeBoard extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION;

    function __construct()
    {
        parent::__construct();

        $this->BOARD_CODE="notice";

        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('partners/PartnerBoard_model');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);
        if($ptype=="") $ptype = $this->input->post('ptype', TRUE);

        switch($ptype) {
            case "del" : $this->setPartnerNoticeDel(); break;
            case "proc" : $this->setPartnerNotice(); break;
            case "v" : $this->loadPartnerNoticeView(); break;
            case "w" : $this->loadPartnerNoticeWrite(); break;
            default : $this->loadPartnerNoticeList(); break;
        }

    }


    // 공지사항 삭제처리
    public function setPartnerNoticeDel()
    {

        $data["emode"] ="del";

        $data["pnb_idx"] =$this->input->get('pnb_idx');
        $data["pnb_title"] =$this->input->get('pnb_title');

        $return_v = $this->PartnerBoard_model->procPartnerNotice( $data);

        echo "<script>location.href='/partners/PartnerNoticeBoard'</script>";
        exit();

    }



    // 공지사항 등록,수정처리
    public function setPartnerNotice()
    {

        $data["emode"] =$this->input->post('emode');

        $data["pnb_idx"] =$this->input->post('pnb_idx');
        $data["pnb_title"] =$this->input->post('pnb_title');
        $data["pnb_state"] =$this->input->post('pnb_state');


        $data["pnb_content"] =$this->input->post('pnb_content');
        $data["pnb_register_am_id"] =$this->session->userdata('admin_id') ;

        $return_v = $this->PartnerBoard_model->procPartnerNotice( $data);

        echo "<script>location.href='/partners/PartnerNoticeBoard'</script>";
        exit();

    }



    // 공지사항 리스트 로드
    public function loadPartnerNoticeList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $arr_PartnerNoticedata = $this->PartnerBoard_model->getPartnerNoticePageList( $page,20,$sp,$sv);
 
        $total = $this->PartnerBoard_model->getPartnerNoticeCount($this->BOARD_CODE, $sp,$sv);

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/Partners/PartnerNotice";
        $config['suffix']     ="&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_allboardlist=[];
        foreach($arr_PartnerNoticedata as $entry)
        {

            $arr_boardlist["pnb_idx"] =$entry->pnb_idx;
            $arr_boardlist["pnb_title"] =$entry->pnb_title;
            $arr_boardlist["pnb_regdate"] =$entry->pnb_regdate;
            $arr_boardlist["pnb_state"] =$entry->pnb_state;
            $arr_boardlist["pnb_view"] =$entry->pnb_view;

            if($arr_boardlist["pnb_state"]=="1"){
                $arr_boardlist["pnb_statestr"]="개제";
            }else{
                $arr_boardlist["pnb_statestr"]="<span style='color:red'>삭제</span>";
            }


            $arr_allboardlist[]=$arr_boardlist;
        }
        $data["list"]=$arr_allboardlist;
        $data["per_page"]=$per_page;
        $data["pagination"]=$pagination;

        $this->load->view('partners/partnernoticelist', array('data'=>$data,'permission'=>$this->ARR_PERMISSION));
        $this->load->view('footer');
    }



    // 공지사항 등록,수정
    public function loadPartnerNoticeWrite()
    {
        $pnb_idx = $this->input->get('pnb_idx', TRUE);

        if($pnb_idx !=""){
            $data["emode"]="edit";
            $arr_PartnerNoticedata = $this->PartnerBoard_model->getPartnerNoticeDetail($pnb_idx);

            $data["pnb_idx"] =$arr_PartnerNoticedata["pnb_idx"];
            $data["pnb_title"] =$arr_PartnerNoticedata["pnb_title"];
            $data["pnb_content"]= $arr_PartnerNoticedata["pnb_content"];
            $data["pnb_regdate"] =$arr_PartnerNoticedata["pnb_regdate"];
            $data["pnb_state"] =$arr_PartnerNoticedata["pnb_state"];
        }else{

            $data["emode"]="new";

        }

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('partners/partnernoticewrite', array('data'=>$data));
        $this->load->view('footer');
    }


    // 공지사항 보기
    public function loadPartnerNoticeView()
    {
        $pnb_idx = $this->input->get('pnb_idx', TRUE);

        $data["emode"]="edit";
        $arr_PartnerNoticedata = $this->PartnerBoard_model->getPartnerNoticeDetail($pnb_idx);


        $data["pnb_idx"] =$arr_PartnerNoticedata["pnb_idx"];
        $data["pnb_title"] =$arr_PartnerNoticedata["pnb_title"];
        $data["pnb_content"]= $arr_PartnerNoticedata["pnb_content"];
        $data["pnb_regdate"] =$arr_PartnerNoticedata["pnb_regdate"];
        $data["pnb_state"] =$arr_PartnerNoticedata["pnb_state"];

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('partners/partnernoticeview', array('data'=>$data));
        $this->load->view('footer');
    }
}
