<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 파트너스 업체 관리
 *
 * @author Den <den@tm2.kr> 19.06.10
 *
 * @property CI_Session $session
 * @property WS_Input   $input
 *
 * @property Workspace_common_func $workspace_common_func
 *
 * @property Partners_affiliate_model $partners_affiliate_model
 */

class PartnersAffiliate extends WS_Controller {
    const ENTER_DATE = 'ed';    // 입점일
    const INTRO_MSG  = 'im';    // 업체한마디

    private $login_id;

    function __construct()
    {
        parent::__construct();

        $this->load->library('Workspace_common_func');

        $this->load->model('partners/partners_affiliate_model');

        $this->login_id = (string)$this->session->userdata('admin_id');

        if ($this->login_id === "")
            echo "<script>location.href='/adminmanage/Login'</script>";

    }

    public function index()
    {
        $crud = $this->input->get('crud', TRUE);

        switch ($crud)
        {
            case 'c':
                break;
            case 'r':
                break;
            case 'u':
                $this->update();
                break;
            case 'd':
                break;
        }

        //

    }

    private function create() {}

    private function read() {}

    /**
     * UPDATE 에 대한 처리
     *
     * @author Den <den@tm2.kr>
     *
     * @method PATCH
     */
    private function update()
    {
        $type = $this->input->get('type', TRUE);

        switch ($type)
        {
            case self::ENTER_DATE:
                $this->update_enter_date();
                break;
            case self::INTRO_MSG:
                $this->update_intro_msg();
                break;
            default:
                // 에러 로깅
                break;

        }

    }

    private function update_enter_date()
    {
        $affiliate_idx = $this->workspace_common_func->check_sql_injection($this->input->input_stream('affiIdx', TRUE), TRUE);
        $enter_date    = $this->workspace_common_func->check_sql_injection($this->input->input_stream('enterDate', TRUE), TRUE);

        $this->partners_affiliate_model->update_enter_date($affiliate_idx, $enter_date);

        echo json_encode([
            'result' => 1
        ]);
    }

    private function update_intro_msg()
    {
        $affiliate_idx = $this->workspace_common_func->check_sql_injection($this->input->input_stream('affiIdx', TRUE), TRUE);
        $intro_code    = $this->workspace_common_func->check_sql_injection($this->input->input_stream('introCode', TRUE), TRUE);
        $intro_msg     = $this->workspace_common_func->check_sql_injection($this->input->input_stream('introMsg', TRUE), TRUE);

        $this->partners_affiliate_model->update_intro_msg($affiliate_idx, $intro_code, $intro_msg);

        echo json_encode([
            'result' => 1
        ]);

    }

    private function delete() {}



}