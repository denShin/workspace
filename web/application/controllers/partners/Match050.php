<?php
/**
 * Match050.php -지점 050 번호 관리리스트 컨트롤
 */

defined('BASEPATH') OR exit('No direct script access allowed');


class Match050 extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('partners/PartnerMember_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);

        switch($ptype) {

            default : $this->loadPartnerMemberBranchList(); break;
        }
    }

    // 지점 050 번호 관리 리스트
    public function loadPartnerMemberBranchList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);

        if(!$sp) $sp="";
        if(!$sv) $sv="";

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));


        $rowresult = $this->PartnerMember_model->getPartnerMemberBranchPageList($page, 20, $sp, $sv);
        $total = $this->PartnerMember_model->getPartnerMemberBranchCount($sp, $sv);

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/partners/Match050";
        $config['suffix']     ="&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];

        foreach($rowresult as $entry)
        {
            $arr_datalist["serial"]=$entry->serial;
            $arr_datalist["rentCompany_serial"]=$entry->rentCompany_serial;
            $arr_datalist["branchName"]=$entry->branchName;
            $arr_datalist["post"]=$entry->post;
            $arr_datalist["address"]=$entry->address;
            $arr_datalist["detailAddress"]=$entry->detailAddress;

            $origintel =str_replace("-","",$entry->tel);
            $arr_datalist["tel"]= $origintel;

            $arr_datalist["authority"]=$entry->authority;
            $arr_datalist["mem_serial"]=$entry->mem_serial;
            $arr_datalist["mem_id"]=$entry->mem_id;
            $arr_datalist["mem_name"]=$entry->mem_name;
            $arr_datalist["rentCompany_branch_serial"]=$entry->rentCompany_branch_serial;

            $arr_datalist["branchOwner"]=$entry->branchOwner;
            $arr_datalist["branchOwnerTel"]=$this->customfunc->add_phonehyphen($entry->branchOwnerTel);

            $arr_datalist["reservation_receive_number"]=$entry->reservation_receive_number;
            $arr_datalist["reservation_receive_email"]=$entry->reservation_receive_email;
            $arr_datalist["regdate"]=$entry->regdate;
            $arr_datalist["openTime"]=$entry->openTime;
            $arr_datalist["closeTime"]=$entry->closeTime;
            $arr_datalist["state"]=$entry->state;
            $arr_datalist["service_area"]=$entry->service_area;
            $arr_datalist["branch_authority"]=$entry->branch_authority;
            $arr_datalist["latitude"]=$entry->latitude;
            $arr_datalist["longitude"]=$entry->longitude;
            $arr_datalist["delivery_oneway"]=$entry->delivery_oneway;
            $arr_datalist["delivery_round"]=$entry->delivery_round;
            $arr_datalist["delivery_area"]=$entry->delivery_area;
            $arr_datalist["period_closed"]=$entry->period_closed;
            $arr_datalist["period_closed_solar"]=$entry->period_closed_solar;
            $arr_datalist["temporary_closed"]=$entry->temporary_closed;
            $arr_datalist["fax"]=$entry->fax;
            $arr_datalist["register_number"]=$entry->register_number;
            $arr_datalist["bankbook"]=$entry->bankbook;
            $arr_datalist["account_number"]=$entry->account_number;
            $arr_datalist["bank"]=$entry->bank;
            $arr_datalist["account_holder"]=$entry->account_holder;
            $arr_datalist["work_time_open"]=$entry->work_time_open;
            $arr_datalist["work_time_close"]=$entry->work_time_close;
            $arr_datalist["monday"]=$entry->monday;
            $arr_datalist["tuesday"]=$entry->tuesday;
            $arr_datalist["wednesday"]=$entry->wednesday;
            $arr_datalist["thursday"]=$entry->thursday;
            $arr_datalist["friday"]=$entry->friday;
            $arr_datalist["saturday"]=$entry->saturday;
            $arr_datalist["sunday"]=$entry->sunday;
            $arr_datalist["weekend"]=$entry->weekend;
            $arr_datalist["jeju_flag"]=$entry->jeju_flag;
            $arr_datalist["shuttle_address"]=$entry->shuttle_address;
            $arr_datalist["shuttle_race_range"]=$entry->shuttle_race_range;
            $arr_datalist["shuttle_spend_time"]=$entry->shuttle_spend_time;
            $arr_datalist["special_location_cover"]=$entry->special_location_cover;
            $arr_datalist["special_peakseason_minimum_reserve_date"]=$entry->special_peakseason_minimum_reserve_date;
            $arr_datalist["special_peakseason_start"]=$entry->special_peakseason_start;
            $arr_datalist["special_peakseason_end"]=$entry->special_peakseason_end;
            $arr_datalist["pricetable_type"]=$entry->pricetable_type;
            $arr_datalist["standard_term_modify_check"]=$entry->standard_term_modify_check;
            $arr_datalist["settlement_rental_rate"]=$entry->settlement_rental_rate;
            $arr_datalist["settlement_cdw_rate"]=$entry->settlement_cdw_rate;
            $arr_datalist["company_name"]=$entry->company_name;
            $arr_datalist["mem_regdate"]=$entry->mem_regdate;
            $arr_datalist["tel050"]=preg_replace("/([0-9]{4})([0-9]{4})([0-9]{4})/", "$1-$2-$3", $entry->tel050);
            $arr_datalist["origintel050"]=  $entry->tel050 ;

            if($entry->carmore_month_available=="1"){
                $arr_datalist["carmore_month_availablestr"]="승인";
            }else {
                $arr_datalist["carmore_month_availablestr"]="미승인";
            }

            $regdate =$entry->regdate;
            $arr_datalist["regdatev"] =$regdate;
            $arr_datalist["regdate"] =$this->customfunc->get_dateformat($regdate,"regs");

            if($entry->authority =="1"){
                $arr_datalist["authoritystr"] ="승인";
            }else{
                $arr_datalist["authoritystr"] ="<span style='color:red'>승인대기</span>";
            }

            $arr_alldatalist[]=$arr_datalist;
        }

        $data["list"]=$arr_alldatalist;
        $data["per_page"]=$per_page;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->LOGINID;

        $this->load->view('partners/match050list', array('data'=>$data));
        $this->load->view('footer');
    }


}
