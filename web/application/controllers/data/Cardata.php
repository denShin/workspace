<?php
/**
 * Cardata.php - 렌터카 데이터 Ajax 검색 컨트롤러
 */
class Cardata    extends CI_Controller {


    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library('Customfunc');
        $this->load->model('carmore/Carinfomaster_model');

    }


    public function index()
    {
        $demode =$this->input->get('emode');
        if($demode=="")  $demode =$this->input->post('emode');


        switch($demode) {
            case "getsearchbranch": $this->getsearchbranch(); break;
            default : $this->getsearchcar(); break;
        }
    }

    // 자동차 리스트 json-data 로드
    public function getsearchcar(){

        header('Content-Type: application/json');

        $returnarr = $this->Carinfomaster_model->getsearchcardata();

        echo  json_encode($returnarr,JSON_UNESCAPED_UNICODE);
    }


    // 자동차 지점별 리스트 json-data 로드
    public function getsearchbranch(){

        header('Content-Type: application/json');

        $returnarr = $this->Carinfomaster_model->getsearchbranch();

        echo  json_encode($returnarr,JSON_UNESCAPED_UNICODE);
    }
}