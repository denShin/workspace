<?php
defined('BASEPATH') OR exit('No direct script access allowed');

error_reporting(E_ALL);

ini_set("display_errors", 1);

require_once $_SERVER["DOCUMENT_ROOT"]."/static/lib/aws-sdk-php-master/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"]."/application/config/account.php";

use Aws\S3\S3Client;
use Aws\Exception\AwsException;
use Aws\S3\Exception\S3Exception;
use Aws\Credentials\Credentials;

/**
 * 이미지 업로드 프로세스
 *
 * @author DEN
 *
 * @property CI_Session $session
 * @property WS_Input   $input
 *
 * @property Workspace_common_func  $workspace_common_func
 *
 * @property Admin_upload_file_model $admin_upload_file_model
 */

class ImgUpload extends WS_Controller {
    private $login_id;
    private $s3_obj;

    private $account_class;

    function __construct()
    {
        parent::__construct();

        $this->login_id = $this->session->userdata('admin_id');

        if ($this->login_id === "")
        {
            echo "<script>location.href='/adminmanage/Login'</script>";

        }

        $this->load->library('Workspace_common_func');
        $this->load->model('admin/admin_upload_file_model');

        $this->create_s3_object();
    }

    /**
     * s3 object 생성
     *
     * @author DEN
     */
    private function create_s3_object()
    {
        $this->account_class = new account();

        $aws_info = $this->account_class->get_aws_info();

        $this->s3_obj = new S3Client([
            'version'     => 'latest',
            'credentials' => new Credentials($aws_info['key'], $aws_info['secret']),
            'region'      => $this->account_class->get_s3_region()
        ]);

    }

    public function index()
    {
        $type = $this->input->get('type', TRUE);

        switch ($type)
        {
            case "u":
                $this->upload_img();
                break;
            case "d":
                break;
        }

    }

    private function upload_img()
    {
        $orig_img_folder = $_SERVER['DOCUMENT_ROOT']."/adminupload/files/";

        $board_code       = $this->input->post('boardCode', TRUE);
        $file_upload_type = $this->input->post('fileUpType', TRUE);
        $content_code     = $this->input->post('contentCode', TRUE);
        $bucket           = $this->workspace_common_func->check_false_value($this->input->post('bucket', TRUE), TRUE);

        if ($bucket === "")
            $bucket = "carmoreweb";

        $save_file_name = $content_code.'_'.$this->get_rand();

        if (is_uploaded_file($_FILES["uploadImg"]["tmp_name"]))
        {
            $origin_file_name = $_FILES["uploadImg"]["name"];
            $origin_file_size = $_FILES["uploadImg"]["size"];

            $size_kb = (int)($origin_file_size/1024);
            $ext     = strtolower(substr(strrchr($origin_file_name, '.'), 1));

            $compress_per = ($size_kb < 200)? 100 : (int)((199/ $size_kb)*100);

            $origin_file_name = "origin_".$save_file_name.".".$ext;
            $upload_file_name = $save_file_name.".".$ext;

            $origin_dest = $orig_img_folder.$origin_file_name;
            $upload_dest = $orig_img_folder.$upload_file_name;
            $s3_key      = $board_code."/".$content_code."/".$upload_file_name;

            if (move_uploaded_file($_FILES["uploadImg"]["tmp_name"], $origin_dest))
            {
                $this->compress($origin_dest, $upload_dest, $compress_per);

                try {
                    $result = $this->s3_obj->putObject(array(
                        'Bucket'       => $bucket,
                        'Key'          => $s3_key,
                        'SourceFile'   => $upload_dest,
                        'ContentType'  => mime_content_type($upload_dest),
                        'ACL'          => 'public-read',
                        'StorageClass' => 'REDUCED_REDUNDANCY'
                    ));

                    if ($result['@metadata']['statusCode'] === 200)
                    {

                        $bind_param = [
                            'contentCode'  => $content_code,
                            'fileOrgName'  => $origin_file_name,
                            'fileSaveName' => $upload_file_name,
                            'fileSize'     => $origin_file_size,
                            'fileType'     => $ext,
                            'fileUpType'   => $file_upload_type,
                            'memId'        => $this->login_id,
                            'boardCode'    => $board_code,
                            'publish'      => 'y'
                        ];

                        $this->admin_upload_file_model->upload_file($bind_param);

                        $return_object = new stdClass();
                        $return_info   = new stdClass();

                        $return_info->saveName = $upload_file_name;

                        $return_object->result = 1;
                        $return_object->info   = $return_info;

                        echo json_encode($return_object);

                    }
                    else
                    {

                    }

                } catch (S3Exception $e) {
                    print_r($e);

                } catch (AwsException $e) {
                    print_r($e);

                } catch (Exception $e) {
                    print_r($e);

                }

            }


        }



    }

    private function compress($source, $destination, $quality)
    {
        $info = getimagesize($source);

        $image = null;
        if ($info['mime'] == 'image/jpeg')
            $image = imagecreatefromjpeg($source);
        elseif ($info['mime'] == 'image/gif')
            $image = imagecreatefromgif($source);
        elseif ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source);

        imagejpeg($image, $destination, $quality);

        return $destination;
    }

    private function get_rand()
    {
        return mt_rand(100, 900);
    }


}