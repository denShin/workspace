<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property CI_Session $session
 * @property WS_Input   $input
 *
 * @property Affiliate_inventory_model $affiliate_inventory_model
 */

class AffiliateInventory extends WS_Controller {
    private $login_id;

    function __construct()
    {
        parent::__construct();

        $this->login_id = $this->session->userdata('admin_id');

        if ($this->login_id === "")
        {
            echo "<script>location.href='/adminmanage/Login'</script>";

        }

        $this->load->model('modal/affiliate/affiliate_inventory_model');
    }

    public function index()
    {
        $this->search_affiliate();
    }

    public function search_affiliate()
    {
        $search_param = (int)urldecode($this->input->get('searchParam', TRUE));
        $search_value = urldecode($this->input->get('searchValue', TRUE));

        echo json_encode($this->affiliate_inventory_model->search_affiliate($search_param, $search_value));
    }

}