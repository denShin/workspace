<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API 예약정보
 *
 * @author DEN
 *
 * @property CI_Session $session
 * @property WS_Input   $input
 *
 * @property Workspace_common_func $workspace_common_func
 * @property Grim_api              $grim_api
 * @property Reborn_api            $reborn_api
 * @property Ins_api               $ins_api
 *
 * @property Api_reserv_inventory_model   $api_reserv_inventory_model
 * @property Api_affiliate_branch_model   $api_affiliate_branch_model
 * @property Api_grim_reserv_item_model   $api_grim_reserv_item_model
 * @property Api_reborn_reserv_item_model $api_reborn_reserv_item_model
 * @property Api_ins_reserv_item_model    $api_ins_reserv_item_model
 */

class ReservItem extends WS_Controller {
    private $login_id;
    private $login_am_idx;
    private $head_param;

    function __construct()
    {
        parent::__construct();

        $this->load->library('Workspace_common_func');
        $this->load->library('api/grim/Grim_api');
        $this->load->library('api/reborn/Reborn_api');
        $this->load->library('api/ins/Ins_api');

        $this->load->model('apiAffiliate/reserv/api_reserv_inventory_model');
        $this->load->model('apiAffiliate/reserv/api_grim_reserv_item_model');
        $this->load->model('apiAffiliate/reserv/api_reborn_reserv_item_model');
        $this->load->model('apiAffiliate/reserv/api_ins_reserv_item_model');
        $this->load->model('apiAffiliate/api_affiliate_branch_model');

        $this->login_id = (string)$this->session->userdata('admin_id');

        if ($this->login_id === "")
        {
            echo "<script>location.href='/adminmanage/Login'</script>";

        }

        $this->login_am_idx = $this->get_admin_idx($this->login_id);

    }

    function index()
    {
        $this->head_param = [
            'library' => ['toastr']
        ];

        $api  = (int)base64_decode($this->input->get("api", TRUE));
        $crud = base64_decode($this->input->get("crud", TRUE));

        switch ($api)
        {
            case 1:    // 그림
                if ($crud === "r")
                    $this->load_grim_reserv_information();
                elseif ($crud === "d")
                    $this->cancel_grim_reserv_information();
                else
                {
                    log_message("error", "(ReservItem) 그림 없는 타입 | $this->login_id | $crud");
                    show_error("없는 타입", 0, "에러 발생");

                }

                break;
            case 2:    // 인스
                if ($crud === "r")
                    $this->load_ins_reserv_information();
                elseif ($crud === "d")
                    $this->cancel_ins_reserv_information();
                else
                {
                    log_message("error", "(ReservItem) 인스 없는 타입 | $this->login_id | $crud");
                    show_error("없는 타입", 0, "에러 발생");

                }

                break;
            case 3:    // 리본
                if ($crud === "r")
                    $this->load_reborn_reserv_information();
                elseif ($crud === "d")
                    $this->cancel_reborn_reserv_information();
                else
                {
                    log_message("error", "(ReservItem) 리본 없는 타입 | $this->login_id | $crud");
                    show_error("없는 타입", 0, "에러 발생");

                }

                break;
        }

    }

    /**
     * 그림 예약정보를 최신화 하고 정보 불러오기
     *
     * @author DEN
     */
    function load_grim_reserv_information()
    {
        $wari_idx        = (int)base64_decode($this->input->get("wariIdx", TRUE));
        $waab_idx        = (int)base64_decode($this->input->get("waabIdx", TRUE));
        $api_reserv_code = base64_decode($this->input->get("apiReservCode", TRUE));

        $waab_info_ret = $this->api_affiliate_branch_model->load_api_affiliate_branch_information($waab_idx);
        if ($waab_info_ret['result'] === 1)
        {
            $waab_info     = $waab_info_ret['info'];
            $waab_api_info = $waab_info['apiInfo'];

            $partner_id = $waab_api_info['info2'];
            $partner_pw = $waab_api_info['info3'];
            $waab_url   = $waab_api_info['info4'];

            $return_info = $this->grim_api->get_reserv_information($waab_url, $partner_id, $partner_pw, $api_reserv_code);
            $state       = $this->grim_api->get_state_flag($return_info['condition']);

            $update_info = [
                'amIdx'           => $this->login_am_idx,
                'reservationName' => $return_info['yeyak_name'],
                'carCode'         => $return_info['car_code'],
                'carName'         => $return_info['car_name'],
                'rentalStart'     => $return_info['sYear'].'-'.$return_info['sMonth'].'-'.$return_info['sDay'].' '.$return_info['sTime'].':00',
                'rentalEnd'       => $return_info['eYear'].'-'.$return_info['eMonth'].'-'.$return_info['eDay'].' '.$return_info['eTime'].':00'
            ];

            if ($state !== -1)
                 $update_info['state'] = $state;

             $update_ret = $this->api_grim_reserv_item_model->update_reserv_item($wari_idx, $update_info);

            if ($update_ret->result === 1)
            {
                $ret_info = $this->api_grim_reserv_item_model->read_reserv_item($wari_idx);

                if ($ret_info->result === 1)
                {
                    $return_data = [
                        'retInfo' => $ret_info
                    ];

                    $this->load->view('head', $this->head_param);
                    $this->load->view('apiAffiliate/reserv/reservItem', ["data" => $return_data]);
                    $this->load->view('footer');
                }
                elseif ($ret_info->result === 2)
                {
                    log_message("error", "(ReservItem) 그림 예약정보 로드에 오류 | $this->login_id | $wari_idx");
                    show_error("그림 예약정보 로드에 오류", 0, "에러 발생");

                }
                else
                {
                    log_message("error", "(ReservItem) 그림 예약정보 로드에 서버오류 | $this->login_id | ".json_encode($ret_info));
                    show_error("그림 예약정보 로드에 서버오류", 0, "에러 발생");

                }


            }
            else
            {
                log_message("error", "(ReservItem) 그림 예약정보 최산화에 오류 | $this->login_id | ".json_encode($update_ret));
                show_error("그림 예약정보 최산화에 오류", 0, "에러 발생");

            }


        }
        else
        {
            log_message("error", "(ReservItem) 지점정보를 가져오는데 오류 발생 | $this->login_id | $waab_idx");
            show_error("지점정보를 가져오는데 오류 발생", 0, "에러 발생");
        }
    }

    /**
     * 그림 예약정보를 취소
     *
     * @author DEN
     *
     */
    function cancel_grim_reserv_information()
    {
        $waab_idx        = (int)base64_decode($this->input->get("waabIdx", TRUE));
        $api_reserv_code = base64_decode($this->input->get("apiReservCode", TRUE));

        $waab_info_ret = $this->api_affiliate_branch_model->load_api_affiliate_branch_information($waab_idx);
        if ($waab_info_ret['result'] === 1)
        {
            $waab_info     = $waab_info_ret['info'];
            $waab_api_info = $waab_info['apiInfo'];

            $partner_id = $waab_api_info['info2'];
            $partner_pw = $waab_api_info['info3'];
            $waab_url   = $waab_api_info['info4'];

            $return_info = $this->grim_api->proc_cancel($waab_url, $partner_id, $partner_pw, $api_reserv_code);

            $return_string = $return_info['Cancel'];
            if ($return_string === "Ok")
                echo json_encode(['result' => 1]);
            elseif ($return_string === "Fail")
            {
                log_message("error", "(ReservItem) 리본 예약취소시 정상취소불가 | $this->login_id | $waab_idx | $api_reserv_code");

                echo json_encode([
                    'result'  => 2,
                    'errInfo' => [
                        'api'  => "그림",
                        'code' => "",
                        'msg'  => "취소 실패.<br>그림 API 취소 실패에 대한 코드는 제공받지 못해 렌트카업체에 문의해야합니다."
                    ]
                ]);
            }
            else
            {
                log_message("error", "(ReservItem) 그림 취소에 오류 발생 | $this->login_id | ".json_encode($return_info));
                show_error("그림 취소에 오류 발생", 0, "에러 발생");

            }
        }

    }

    /**
     * 리본 예약정보를 최신화 하고 정보 불러오기
     *
     * @author DEN
     */
    function load_reborn_reserv_information()
    {
        $wari_idx        = (int)base64_decode($this->input->get("wariIdx", TRUE));
        $waab_idx        = (int)base64_decode($this->input->get("waabIdx", TRUE));
        $api_reserv_code = base64_decode($this->input->get("apiReservCode", TRUE));

        $waab_info_ret = $this->api_affiliate_branch_model->load_api_affiliate_branch_information($waab_idx);
        if ($waab_info_ret['result'] === 1)
        {
            $waab_info     = $waab_info_ret['info'];
            $waab_api_info = $waab_info['apiInfo'];

            $api_key   = $waab_api_info['info1'];    // API 키
            $certi_key = $waab_api_info['info2'];    // API 인증

            $return_info = $this->reborn_api->get_reserv_information($api_key, $certi_key, $api_reserv_code);
            $state       = $this->reborn_api->get_state_flag($return_info['resveCanclYn']);

            $update_info = [
                'amIdx'       => $this->login_am_idx,
                'driverName'  => $return_info['drverNm'],
                'driverPhone' => $return_info['rsvctmCttpc1Encpt'],
                'carCode'     => $return_info['vhctyCode'],
                'carName'     => $return_info['vhctyNm'],
                'rentalStart' => substr($return_info['resveBeginDhm'], 0, 4).'-'.substr($return_info['resveBeginDhm'], 4, 2).'-'.substr($return_info['resveBeginDhm'], 6, 2).' '.substr($return_info['resveBeginDhm'], 8, 2).':'.substr($return_info['resveBeginDhm'], 10, 2).':00',
                'rentalEnd'   => substr($return_info['resveEndDhm'], 0, 4).'-'.substr($return_info['resveEndDhm'], 4, 2).'-'.substr($return_info['resveEndDhm'], 6, 2).' '.substr($return_info['resveEndDhm'], 8, 2).':'.substr($return_info['resveEndDhm'], 10, 2).':00',
                'cdwName'     => $return_info['insrncKndNm'],
                'memo'        => $return_info['cmmnEtcContents']
            ];

            if ($state !== -1)
                $update_info['state'] = $state;

            $update_ret = $this->api_reborn_reserv_item_model->update_reserv_item($wari_idx, $update_info);

            if ($update_ret->result === 1)
            {
                $ret_info = $this->api_reborn_reserv_item_model->read_reserv_item($wari_idx);

                if ($ret_info->result === 1)
                {
                    $return_data = [
                        'retInfo' => $ret_info
                    ];

                    $this->load->view('head', $this->head_param);
                    $this->load->view('apiAffiliate/reserv/reservItem', ["data" => $return_data]);
                    $this->load->view('footer');
                }
                elseif ($ret_info->result === 2)
                {
                    log_message("error", "(ReservItem) 리본 예약정보 로드에 오류 | $this->login_id | $wari_idx");
                    show_error("리본 예약정보 로드에 오류", 0, "에러 발생");

                }
                else
                {
                    log_message("error", "(ReservItem) 리본 예약정보 로드에 서버오류 | $this->login_id | ".json_encode($ret_info));
                    show_error("리본 예약정보 로드에 서버오류", 0, "에러 발생");

                }


            }
            else
            {
                log_message("error", "(ReservItem) 리본 예약정보 최산화에 오류 | $this->login_id | ".json_encode($update_ret));
                show_error("리본 예약정보 최산화에 오류", 0, "에러 발생");

            }

        }
    }

    /**
     * 리본 예약정보를 취소
     *
     * @author DEN
     */
    function cancel_reborn_reserv_information()
    {
        $waab_idx        = (int)base64_decode($this->input->get("waabIdx", TRUE));
        $api_reserv_code = base64_decode($this->input->get("apiReservCode", TRUE));

        $waab_info_ret = $this->api_affiliate_branch_model->load_api_affiliate_branch_information($waab_idx);
        if ($waab_info_ret['result'] === 1)
        {
            $waab_info     = $waab_info_ret['info'];
            $waab_api_info = $waab_info['apiInfo'];

            $api_key   = $waab_api_info['info1'];    // API 키
            $certi_key = $waab_api_info['info2'];    // API 인증

            $return_info = $this->reborn_api->proc_cancel($api_key, $certi_key, $api_reserv_code);

            $result_code = $return_info['resultCode'];
            if ($result_code === "OK")
                echo json_encode(['result' => 1]);
            else
            {
                $err_code = $return_info['resultCode'];
                $err_msg  = $return_info['resultMsg'];

                log_message("error", "(ReservItem) 리본 예약취소시 정상취소불가 | $this->login_id | $waab_idx | $api_reserv_code | $err_code | $err_msg");

                echo json_encode([
                    'result'  => 2,
                    'errInfo' => [
                        'api'  => "리본",
                        'code' => $err_code,
                        'msg'  => $err_msg
                    ]
                ]);
            }

        }
    }

    /**
     * 인스 예약정보를 최신화 하고 정보 불러오기
     *
     * @author DEN
     */
    function load_ins_reserv_information()
    {
        $wari_idx        = (int)base64_decode($this->input->get("wariIdx", TRUE));
        $waab_idx        = (int)base64_decode($this->input->get("waabIdx", TRUE));
        $api_reserv_code = base64_decode($this->input->get("apiReservCode", TRUE));

        $waab_info_ret = $this->api_affiliate_branch_model->load_api_affiliate_branch_information($waab_idx);
        if ($waab_info_ret['result'] === 1)
        {
            $waab_info     = $waab_info_ret['info'];
            $waab_api_info = $waab_info['apiInfo'];

            $rent_code = $waab_api_info['info1'];    // 렌트카코드

            $return_info = $this->ins_api->get_reserv_information($rent_code, $api_reserv_code);
            $state       = $this->ins_api->get_state_flag($return_info->R예약상태);

            $update_info = [
                'amIdx'       => $this->login_am_idx,
                'driverName'  => $return_info->R이름,
                'driverPhone' => $return_info->R전화번호,
                'carCode'     => $return_info->R모델번호,
                'carName'     => $return_info->R모델명,
                'rentalStart' => $return_info->R출발일시.':00',
                'rentalEnd'   => $return_info->R반납일시.':00',
                'cdwName'     => $return_info->R자차보험,
                'modifyAva'   => $return_info->R예약변경가능시간,
                'memo'        => $return_info->R전달사항
            ];

            if ($state !== -1)
                $update_info['state'] = $state;

            $update_ret = $this->api_ins_reserv_item_model->update_reserv_item($wari_idx, $update_info);

            if ($update_ret->result === 1)
            {
                $ret_info = $this->api_ins_reserv_item_model->read_reserv_item($wari_idx);

                if ($ret_info->result === 1)
                {
                    $return_data = [
                        'retInfo' => $ret_info
                    ];

                    $this->load->view('head', $this->head_param);
                    $this->load->view('apiAffiliate/reserv/reservItem', ["data" => $return_data]);
                    $this->load->view('footer');
                }
                elseif ($ret_info->result === 2)
                {
                    log_message("error", "(ReservItem) 인스 예약정보 로드에 오류 | $this->login_id | $wari_idx");
                    show_error("인스 예약정보 로드에 오류", 0, "에러 발생");

                }
                else
                {
                    log_message("error", "(ReservItem) 인스 예약정보 로드에 서버오류 | $this->login_id | ".json_encode($ret_info));
                    show_error("인스 예약정보 로드에 서버오류", 0, "에러 발생");

                }

            }
            else
            {
                log_message("error", "(ReservItem) 인스 예약정보 최산화에 오류 | $this->login_id | ".json_encode($update_ret));
                show_error("인스 예약정보 최산화에 오류", 0, "에러 발생");

            }



        }
    }

    function cancel_ins_reserv_information()
    {
        $waab_idx        = (int)base64_decode($this->input->get("waabIdx", TRUE));
        $api_reserv_code = base64_decode($this->input->get("apiReservCode", TRUE));

        $waab_info_ret = $this->api_affiliate_branch_model->load_api_affiliate_branch_information($waab_idx);
        if ($waab_info_ret['result'] === 1)
        {
            $waab_info     = $waab_info_ret['info'];
            $waab_api_info = $waab_info['apiInfo'];

            $rent_code = $waab_api_info['info1'];    // 렌트카코드

            $return_info = $this->ins_api->proc_cancel($rent_code, $api_reserv_code);

            $res_code = $return_info->resCode;

            if ($res_code === "0000")
                echo json_encode(['result' => 1]);
            else
            {
                $err_msg  = $return_info->resMsg;

                log_message("error", "(ReservItem) 리본 예약취소시 정상취소불가 | $this->login_id | $waab_idx | $api_reserv_code | $res_code | $err_msg");

                echo json_encode([
                    'result'  => 2,
                    'errInfo' => [
                        'api'  => "리본",
                        'code' => $res_code,
                        'msg'  => $err_msg
                    ]
                ]);
            }

        }
    }
}
