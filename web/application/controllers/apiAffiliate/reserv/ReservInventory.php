<?php
error_reporting(E_ALL);

ini_set("display_errors", 1);


defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API 예약목록
 *
 * @author DEN
 *
 * @property CI_Session $session
 * @property WS_Input   $input
 *
 * @property Workspace_common_func $workspace_common_func
 *
 * @property Api_reserv_inventory_model $api_reserv_inventory_model
 */

class ReservInventory extends WS_Controller {
    private $login_id;
    private $head_param;

    const PAGE_ROW = 20;

    function __construct()
    {
        parent::__construct();

        $this->load->library('pagination');
        $this->load->library('Workspace_common_func');

        $this->load->model('apiAffiliate/reserv/api_reserv_inventory_model');

        $this->login_id = (string)$this->session->userdata('admin_id');

        if ($this->login_id === "")
        {
            echo "<script>location.href='/adminmanage/Login'</script>";

        }
    }

    function index()
    {
        $this->head_param = [
            'library' => ['toastr']
        ];

        $this->read_reserv_inventory();

    }

    /**
     * 예약목록 가져오기
     */
    function read_reserv_inventory()
    {
        $this->load->helper('url');
        $this->config->load('bootstrap_pagination');

        $per_page        = (int)(($this->input->get('per_page', TRUE))/self::PAGE_ROW) + 1;
        $search_param    = (int)$this->workspace_common_func->check_false_value($this->input->get('sp', TRUE), TRUE);
        $search_value    = $this->workspace_common_func->check_false_value($this->input->get('sv', TRUE), TRUE);
        $search_state_u1 = (int)$this->workspace_common_func->check_false_value($this->input->get('stateu1', TRUE), TRUE);
        $search_state_0  = (int)$this->workspace_common_func->check_false_value($this->input->get('state0', TRUE), TRUE);
        $search_state_1  = (int)$this->workspace_common_func->check_false_value($this->input->get('state1', TRUE), TRUE);
        $search_state_10 = (int)$this->workspace_common_func->check_false_value($this->input->get('state10', TRUE), TRUE);
        $search_state_11 = (int)$this->workspace_common_func->check_false_value($this->input->get('state11', TRUE), TRUE);

        $state_param = [
            "-1" => $search_state_u1,
            "0"  => $search_state_0,
            "1"  => $search_state_1,
            "10" => $search_state_10,
            "11" => $search_state_11,
        ];

        $search_state_arr = $this->get_state_arr($state_param);

        if (empty($search_state_arr))
        {
            $search_state_arr = [0, 1, 10];

            $search_state_0  = 1;
            $search_state_1  = 1;
            $search_state_10 = 1;
        }

        $total_cnt = $this->api_reserv_inventory_model->load_api_reserv_total_count($search_state_arr, $search_param, $search_value);
        $inventory = $this->api_reserv_inventory_model->load_api_reserv_inventory($per_page, self::PAGE_ROW, $search_state_arr, $search_param, $search_value);

        $config = $this->config->item('pagination');
        $config['total_rows']            = $total_cnt;    # 게시물 총 수
        $config['per_page']              = self::PAGE_ROW;    # 게시물 출력수
        $config['base_url']              = $config['base_url']."/apiAffiliate/reserv/reservInventory?stateu1=".$search_state_u1."&state0=".$search_state_0."&state1=".$search_state_1."&state10=".$search_state_10."&state11=".$search_state_11;
        $config['suffix']                = "&sp=".$search_param."&sv=".$search_value;
        $config['use_global_url_suffix'] = false;
        $config['reuse_query_string']    = false;

        $this->pagination->initialize($config);

        $pagination = $this->pagination->create_links();

        $return_data = [
            'inventory'  => $inventory,
            'pagination' => $pagination
        ];

        $this->load->view('head', $this->head_param);
        $this->load->view('apiAffiliate/reserv/reservInventory', ["data" => $return_data]);
        $this->load->view('footer');

    }

    private function get_state_arr($param)
    {
        $return_arr = [];

        foreach ($param as $key => $arg)
        {
            if ($arg === 1)
                array_push($return_arr, (int)$key);

        }

        return $return_arr;
    }
}
