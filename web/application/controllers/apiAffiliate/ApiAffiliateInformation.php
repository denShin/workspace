<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 제주 API 업체 정보
 *
 * @author DEN
 *
 * @property Workspace_common_func $workspace_common_func
 *
 * @property Api_affiliate_model                $api_affiliate_model
 * @property Api_affiliate_branch_model         $api_affiliate_branch_model
 * @property Api_affiliate_branch_biztalk_model $api_affiliate_branch_biztalk_model
 * @property Onse_model                         $onse_model
 */

class ApiAffiliateInformation extends WS_Controller {
    private $login_id;
    private $login_am_idx;
    private $head_param;
    private $now;

    function __construct()
    {
        parent::__construct();

        $this->load->library('workspace_common_func');
        $this->load->model('apiAffiliate/api_affiliate_model');
        $this->load->model('apiAffiliate/api_affiliate_branch_model');
        $this->load->model('apiAffiliate/api_affiliate_branch_biztalk_model');
        $this->load->model('partners/onse/onse_model');

        $this->login_id = (string)$this->session->userdata('admin_id');

        if ($this->login_id === "")
        {
            echo "<script>location.href='/adminmanage/Login'</script>";

        }

        $this->login_am_idx = $this->get_admin_idx($this->login_id);
        $this->now          = date('Y-m-d H:i:s', strtotime('+9 hours'));
    }

    /**
     * @param  void
     * @return void
     */
    function index()
    {
        $this->head_param = [
            'css'     => ['api_affiliate_information'],
            'library' => ['toastr', 'dropzone']
        ];

        $type = (int)$this->input->get('type', TRUE);

        if ($type === -1)
            $this->init_register_affiliate();
        elseif ($type === 1)
            $this->load_affiliate_branch();
        elseif ($type === 0)
            $this->delete_affiliate_branch();
        else
        {
            log_message("error", "(ApiAffiliateInformation) 정의되지 않은 타입 | $this->login_id | $type");
            show_error("정의되지 않은 타입입니다", 0, "에러 발생");

        }

    }

    /**
     * 지점 신규등록하는 페이지 호출
     */
    function init_register_affiliate()
    {
        $data_param = [
            'type' => -1
        ];

        $this->load->view('head', $this->head_param);
        $this->load->view('apiAffiliate/apiAffiliateInformation', ['data' => $data_param]);
        $this->load->view('footer');
    }

    /**
     * 등록된 지점 데이터 불러오기
     */
    function load_affiliate_branch()
    {
        $affiliate_idx        = base64_decode($this->input->get('waai', TRUE));
        $affiliate_branch_idx = base64_decode($this->input->get('waabi', TRUE));

        $affiliate_branch_inventory   = $this->api_affiliate_model->load_api_affiliate_branch_inventory($affiliate_idx);
        $affiliate_branch_information = $this->api_affiliate_branch_model->load_api_affiliate_branch_information($affiliate_branch_idx);

        if ($affiliate_branch_information['result'] === 1)
        {
            $branch_info = $affiliate_branch_information['info'];

            $tel = $this->workspace_common_func->format_tel($branch_info['tel']);

            $data_param = [
                'type'              => 1,
                'branchInventory'   => $affiliate_branch_inventory,
                'branchInformation' => [
                    'waaIdx'        => $affiliate_idx,
                    'waabIdx'       => $affiliate_branch_idx,
                    'waaName'       => $branch_info['waaName'],
                    'waabName'      => $branch_info['waabName'],
                    'apiInfo'       => $branch_info['apiInfo'],
                    'mainAddr'      => $branch_info['mainAddress'],
                    'subAddr'       => $branch_info['subAddress'],
                    'tel'           => $tel,
                    'tel050'        => $branch_info['tel050'],
                    'maxReserv'     => $branch_info['maxReserv'],
                    'openTime'      => mb_substr($branch_info['openTime'], 0, 5),
                    'closeTime'     => mb_substr($branch_info['closeTime'], 0, 5),
                    'saleStartHour' => $branch_info['saleStartHour'],
                    'stName'        => $branch_info['shuttleName'],
                    'stArea'        => $branch_info['shuttleArea'],
                    'stAreaNum'     => $branch_info['shuttleAreaNum'],
                    'stRace'        => $branch_info['shuttleRace'],
                    'stSpend'       => $branch_info['shuttleSpend'],
                    'intro'         => $branch_info['intro'],
                    'uniqueness'    => $branch_info['uniqueness'],
                    'enterDate'     => $branch_info['enterDate'],
                    'logo'          => $branch_info['waabLogoInfo']['logo'],
                    'logoSize'      => $branch_info['waabLogoInfo']['size'],
                    'subImgs'       => $branch_info['waabSubImgs'],
                    'serviceString' => $branch_info['serviceString'],
                    'deliveryString'=> $branch_info['deliveryString'],
                    'specialLocation'=> $branch_info['specialLocation'],
                    'biztalkInven'  => $this->api_affiliate_branch_biztalk_model->load_api_affiliate_branch_biztalk_information($affiliate_branch_idx),
                    'recommendNum'  => ($tel === "")? "" : $this->onse_model->load_api_affi_recommend_num()
                ]
            ];

            $this->load->view('head', $this->head_param);
            $this->load->view('apiAffiliate/apiAffiliateInformation', ['data' => $data_param]);
            $this->load->view('footer');

        }
        else
        {
            log_message("error", "(ApiAffiliateInformation) 지점 정보 가져오는데 오류 발생 | $this->login_id | $affiliate_idx | $affiliate_branch_idx");
            show_error("지점 정보를 가져오는데 오류가 발생했습니다", 0, "에러 발생");

        }

    }

    function delete_affiliate_branch()
    {
        $affiliate_idx        = base64_decode($this->input->get('waai', TRUE));
        $affiliate_branch_idx = base64_decode($this->input->get('waabi', TRUE));

        $this->api_affiliate_branch_model->delete_api_affiliate_branch($affiliate_branch_idx, $this->login_am_idx, $this->now);

        $branch_count = $this->api_affiliate_model->count_api_affiliate_branch($affiliate_idx);

        if ($branch_count === 0)
            $this->api_affiliate_model->delete_api_affiliate($affiliate_idx);

         echo json_encode(['result' => 1]);

    }

}