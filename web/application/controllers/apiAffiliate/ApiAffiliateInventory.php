<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 제주 API 업체 목록 리스트
 *
 * @author DEN
 *
 * @property Workspace_common_func $workspace_common_func
 *
 * @property Api_affiliate_model   $api_affiliate_model
 */

class ApiAffiliateInventory extends WS_Controller {
    const PAGE_ROW = 20;

    private $login_id;

    function __construct()
    {
        parent::__construct();

        $this->load->library('pagination');
        $this->load->library('Workspace_common_func');
        $this->load->model('apiAffiliate/api_affiliate_model');

        $this->login_id = (string)$this->session->userdata('admin_id');

        if ($this->login_id === "")
        {
            echo "<script>location.href='/adminmanage/Login'</script>";

        }
    }

    /**
     * @param  void
     * @return void
     */
    function index()
    {
        $this->load_api_affiliates_inventory();

    }

    /**
     * API 업체 목록 불러오기
     *
     * @param  void
     * @return void
     */
    function load_api_affiliates_inventory()
    {
        $this->load->helper('url');

        $per_page     = $this->input->get('per_page', TRUE);
        $search_value = $this->workspace_common_func->check_false_value($this->input->get('searchValue', TRUE), TRUE);

        if ($per_page < self::PAGE_ROW)
            $per_page = 0;

        $page = ($per_page/self::PAGE_ROW) +1;

        $total_cnt     = $this->api_affiliate_model->load_api_affiliate_total_count($search_value);
        $ret_inventory = $this->api_affiliate_model->load_api_affiliate_inventory($page, self::PAGE_ROW, $search_value);

        # CI >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');

        $config = $this->config->item('pagination');
        $config['total_rows']            = $total_cnt;    # 게시물 총 수
        $config['per_page']              = self::PAGE_ROW;    # 게시물 출력수
        $config['base_url']              = $config['base_url']."/apiAffiliate/apiAffiliateInventory";
        $config['suffix']                = "&searchValue=".$search_value;
        $config['use_global_url_suffix'] = false;
        $config['reuse_query_string']    = false;

        $this->pagination->initialize($config);

        $pagination = $this->pagination->create_links();

        # 리스트 받아오기

        $data['inventory']  = $ret_inventory;
        $data['per_page']   = $per_page;
        $data['pagination'] = $pagination;
        $data['loginid']    = $this->login_id;

        $this->load->view('head');
        $this->load->view('apiAffiliate/apiAffiliateInventory', ['data' => $data]);
        $this->load->view('footer');

    }
}