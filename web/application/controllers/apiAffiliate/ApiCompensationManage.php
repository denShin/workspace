<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API 종합보험 관리
 *
 * @auhtor DEN
 *
 * @property Workspace_common_func $workspace_common_func
 *
 * @property Api_compensation_model $api_compensation_model
 */

class ApiCompensationManage extends WS_Controller {
    private $login_id;
    private $login_am_idx;
    private $head_param;
    private $now;

    function __construct()
    {
        parent::__construct();

        $this->login_id = (string)$this->session->userdata('admin_id');

        if ($this->login_id === "")
        {
            echo "<script>location.href='/adminmanage/Login'</script>";

        }

        $this->load->library('Workspace_common_func');
        $this->load->model('apiAffiliate/api_compensation_model');

        $this->login_am_idx = $this->get_admin_idx($this->login_id);
        $this->now          = date('Y-m-d H:i:s', strtotime('+9 hours'));
    }

    public function index()
    {
        $this->head_param = [
            'css'     => ['api_compensation_manage'],
            'library' => ['toastr']
        ];

        $type = $this->input->get('type', TRUE);

        switch ($type)
        {
            case "c":
                $this->load_api_compensation_inventory("c");
                break;
            case "r":
                $this->load_api_compensation_inventory("r");
                break;
            default:
                log_message("error", "(ApiCompensationManage) 일치하는 타입이 없습니다 | $this->login_id | $type");
                show_error("일치하는 타입이 없습니다", 0, "에러 발생");
                break;
        }

    }

    /**
     * 보험 정보 보기페이지
     *
     * @param $type string 생성하는건지 보는건지 [C, R]
     */
    public function load_api_compensation_inventory($type)
    {
        $compensation_inventory = $this->api_compensation_model->load_compensation_inventory();

        $data = [
            'type'      => $type,
            'inventory' => $compensation_inventory
        ];

        $wac_idx = $this->workspace_common_func->check_false_value($this->input->get('wac', TRUE), TRUE);

        if (count($compensation_inventory) !== 0 && $type === "r")
        {
            $find_wac_idx = ($wac_idx !== "")? base64_decode($wac_idx) : $compensation_inventory[0]['idx'];
            $wac_info     = $this->api_compensation_model->load_compensation_information($find_wac_idx);

            if ($wac_info['result'] === 1)
            {
                $data['selectedIdx'] = $find_wac_idx;
                $data['info']        = $wac_info['info'];

            }
            else
            {
                log_message("error", "(ApiCompensationManage) 종합보험 내용가져오는데 오류 | $this->login_id | $find_wac_idx");
                show_error("종합보험 내용 로드 오류", 0, "에러 발생");

            }

        }

        $this->load->view('head', $this->head_param);
        $this->load->view('apiAffiliate/apiCompensationManage', ['data' => $data]);
        $this->load->view('footer');
    }

}
