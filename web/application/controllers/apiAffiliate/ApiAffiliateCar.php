<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 제주 API 차량 목록
 *
 * @author Den
 *
 * @property Workspace_common_func $workspace_common_func
 * @property Reborn_api            $reborn_api
 * @property Grim_api              $grim_api
 * @property Ins_api               $ins_api
 *
 * @property Car_info_master_model      $car_info_master_model
 * @property Api_compensation_model     $api_compensation_model
 * @property Api_affiliate_branch_model $api_affiliate_branch_model
 * @property Api_affiliate_reborn_car   $api_affiliate_reborn_car
 * @property Api_affiliate_grim_car     $api_affiliate_grim_car
 * @property Api_affiliate_ins_car      $api_affiliate_ins_car
 */

class ApiAffiliateCar extends WS_Controller {
    private $login_id;
    private $login_am_idx;
    private $head_param;

    function __construct()
    {
        parent::__construct();

        $this->load->library('Workspace_common_func');
        $this->load->library('api/reborn/Reborn_api');
        $this->load->library('api/grim/Grim_api');
        $this->load->library('api/ins/Ins_api');
        $this->load->model('carmore/car_info_master_model');
        $this->load->model('apiAffiliate/api_compensation_model');
        $this->load->model('apiAffiliate/api_affiliate_branch_model');
        $this->load->model('apiAffiliate/api_affiliate_reborn_car');
        $this->load->model('apiAffiliate/api_affiliate_grim_car');
        $this->load->model('apiAffiliate/api_affiliate_ins_car');
        $this->login_id = (string)$this->session->userdata('admin_id');

        if ($this->login_id === "")
        {
            echo "<script>location.href='/adminmanage/Login'</script>";

        }

        $this->login_am_idx = $this->get_admin_idx($this->login_id);
    }

    public function index()
    {
        $this->head_param = [
            'css'     => ['api_affiliate_car'],
            'library' => ['toastr']
        ];

        $crud_type = $this->input->get("type");

        switch ($crud_type) {
            case "r":
                $this->load_information();
                break;
            case "u":
                $this->update_information();
                break;
        }

    }

    /**
     * 정보 로드
     */
    public function load_information()
    {
        $waab_inventory = $this->api_affiliate_branch_model->load_api_affiliate_branch();

        $waab_idx = base64_decode($this->workspace_common_func->check_false_value($this->input->get('waab', TRUE), TRUE));

        $return_data = [
            'waabInventory'            => $waab_inventory,
            'selectedWaabIdx'          => $waab_idx,
            'selectedWaabApi'          => 0,
            'selectedWaabCarInventory' => [],
            'masterCarInventory'       => $this->car_info_master_model->get_car_inventory_using_group(),
            'compensationInventory'    => $this->api_compensation_model->load_compensation_inventory()
        ];

        if (count($waab_inventory) !== 0)
        {
            $search_waab_obj = $this->get_search_waab_obj($waab_inventory, $waab_idx);

            if (!empty($search_waab_obj))
            {
                $waa_idx  = (int)$search_waab_obj['waaIdx'];
                $waab_idx = (int)$search_waab_obj['waabIdx'];
                $waab_api = (int)$search_waab_obj['waabApi'];

                $return_data['selectedWaabIdx'] = $waab_idx;
                $return_data['selectedWaabApi'] = $waab_api;

                if ($waab_api === 1)
                {
                    $api_ret_arr = $this->grim_api->get_affiliate_branch_car_inventory($search_waab_obj['waabApiInfo4']);
                    foreach ($api_ret_arr as $grim_obj)
                    {
                        $car_code = $grim_obj['code'];
                        $color    = $this->convert_array_to_string(",", $grim_obj['color']);

                        $wgci_idx = $this->api_affiliate_grim_car->chk_has_car($waab_idx, $car_code);
                        if (!$wgci_idx)
                        {

                            $bind_param = [
                                'waaIdx'   => $waa_idx,
                                'waabIdx'  => $waab_idx,
                                'carCode'  => $car_code,
                                'carName'  => $grim_obj['name'],
                                'gubun'    => $grim_obj['gubun'],
                                'gear'     => $grim_obj['gear'],
                                'color'    => $color,
                                'fuel'     => $grim_obj['fuel'],
                                'old'      => $grim_obj['old'],
                                'jeongwon' => $grim_obj['jeongwon'],
                                'permit'   => $grim_obj['permit'],
                                'baegi'    => $grim_obj['baegi'],
                                'option'   => $grim_obj['option']
                            ];

                            $this->api_affiliate_grim_car->insert_car_info($bind_param);
                        }
                        else
                        {
                            $bind_param = [
                                'carName'  => $grim_obj['name'],
                                'gubun'    => $grim_obj['gubun'],
                                'gear'     => $grim_obj['gear'],
                                'color'    => $color,
                                'fuel'     => $grim_obj['fuel'],
                                'old'      => $grim_obj['old'],
                                'jeongwon' => $grim_obj['jeongwon'],
                                'permit'   => $grim_obj['permit'],
                                'baegi'    => $grim_obj['baegi'],
                                'option'   => $grim_obj['option']
                            ];

                            $this->api_affiliate_grim_car->update_car_info($wgci_idx, $bind_param);
                        }

                        $car_inventory = $this->api_affiliate_grim_car->get_car_inventory($waab_idx);

                        $return_data['selectedWaabCarInventory'] = $car_inventory;

                    }
                }
                elseif ($waab_api === 2)
                {
                    $api_ret_arr = $this->ins_api->get_affiliate_branch_car_inventory($search_waab_obj['waabApiInfo1']);

                    foreach ($api_ret_arr as $ins_obj)
                    {
                        $car_code = $ins_obj->R모델번호;

                        $wici_idx = $this->api_affiliate_ins_car->chk_has_car($waab_idx, $car_code);

                        if (!$wici_idx)
                        {
                            $bind_param = [
                                'waaIdx'       => $waa_idx,
                                'waabIdx'      => $waab_idx,
                                'carCode'      => $car_code,
                                'carName'      => $ins_obj->R모델명,
                                'carOption'    => $ins_obj->R모델옵션,
                                'carFuel'      => $ins_obj->R연료,
                                'carPassenger' => $ins_obj->R승차인원,
                                'carPermitAge' => $ins_obj->R가능연령,
                                'carYear'      => $ins_obj->R연식,
                                'driverMethod' => $ins_obj->R구동방식,
                                'displacement' => $ins_obj->R배기량,
                                'gear'         => $ins_obj->R변속기,
                                'title'        => $ins_obj->R타이틀
                            ];

                            $this->api_affiliate_ins_car->insert_car_info($bind_param);
                        }
                        else
                        {
                            $bind_param = [
                                'carName'      => $ins_obj->R모델명,
                                'carOption'    => $ins_obj->R모델옵션,
                                'carFuel'      => $ins_obj->R연료,
                                'carPassenger' => $ins_obj->R승차인원,
                                'carPermitAge' => $ins_obj->R가능연령,
                                'carYear'      => $ins_obj->R연식,
                                'driverMethod' => $ins_obj->R구동방식,
                                'displacement' => $ins_obj->R배기량,
                                'gear'         => $ins_obj->R변속기,
                                'title'        => $ins_obj->R타이틀
                            ];

                            $this->api_affiliate_ins_car->update_car_info($wici_idx, $bind_param);
                        }

                        $car_inventory = $this->api_affiliate_ins_car->get_car_inventory($waab_idx);


                        $return_data['selectedWaabCarInventory'] = $car_inventory;
                    }

                }
                elseif ($waab_api === 3)
                {
                    $api_ret_arr = $this->reborn_api->get_affiliate_branch_car_inventory($search_waab_obj['waabApiInfo1'], $search_waab_obj['waabApiInfo2']);

                    foreach ($api_ret_arr as $reborn_obj)
                    {
                        $car_code = $reborn_obj['vhctyCode'];

                        $wrci_idx = $this->api_affiliate_reborn_car->chk_has_car($waab_idx, $car_code);

                        $brand_code              = $this->convert_array_to_string(",", $reborn_obj['makrClCode']);
                        $brand_name              = $this->convert_array_to_string(",", $reborn_obj['makrClNm']);
                        $option_safe_code        = $this->convert_array_to_string(",", $reborn_obj['optionSafeCode']);
                        $option_safe_name        = $this->convert_array_to_string(",", $reborn_obj['optionSafeNm']);
                        $option_convenience_code = $this->convert_array_to_string(",", $reborn_obj['optionCnvncCode']);
                        $option_convenience_name = $this->convert_array_to_string(",", $reborn_obj['optionCnvncNm']);
                        $option_sound_code       = $this->convert_array_to_string(",", $reborn_obj['optionSondCode']);
                        $option_sound_name       = $this->convert_array_to_string(",", $reborn_obj['optionSondNm']);

                        if (!$wrci_idx)
                        {

                            $bind_param = [
                                'waaIdx'         => $waa_idx,
                                'waabIdx'        => $waab_idx,
                                'carCode'        => $car_code,
                                'carName'        => $reborn_obj['vhctyNm'],
                                'cartypeCode'    => $reborn_obj['vhctyClCode'],
                                'cartypeName'    => $reborn_obj['vhctyClNm'],
                                'paClafCode'     => $reborn_obj['corpPeplcarrCode'],
                                'paClafName'     => $reborn_obj['corpPeplcarrNm'],
                                'brandCode'      => $brand_code,
                                'brandName'      => $brand_name,
                                'fuelCode'       => $reborn_obj['fuelCode'],
                                'fuelName'       => $reborn_obj['fuelNm'],
                                'passenger'      => $reborn_obj['passengers'],
                                'optionSafeCode' => $option_safe_code,
                                'optionSafeName' => $option_safe_name,
                                'optionCnCode'   => $option_convenience_code,
                                'optionCnName'   => $option_convenience_name,
                                'optionSnCode'   => $option_sound_code,
                                'optionSnName'   => $option_sound_name,
                                'yearMin'        => $reborn_obj['yemodelMin'],
                                'yearMax'        => $reborn_obj['yemodelMax'],
                                'yearStr'        => empty($reborn_obj['yemodel'])? 0 : $reborn_obj['yemodel'],
                                'licenseType'    => $reborn_obj['license'],
                            ];

                            $this->api_affiliate_reborn_car->insert_car_info($bind_param);
                        }
                        else
                        {
                            $bind_param = [
                                'carName'        => $reborn_obj['vhctyNm'],
                                'cartypeCode'    => $reborn_obj['vhctyClCode'],
                                'cartypeName'    => $reborn_obj['vhctyClNm'],
                                'paClafCode'     => $reborn_obj['corpPeplcarrCode'],
                                'paClafName'     => $reborn_obj['corpPeplcarrNm'],
                                'brandCode'      => $brand_code,
                                'brandName'      => $brand_name,
                                'fuelCode'       => $reborn_obj['fuelCode'],
                                'fuelName'       => $reborn_obj['fuelNm'],
                                'passenger'      => $reborn_obj['passengers'],
                                'optionSafeCode' => $option_safe_code,
                                'optionSafeName' => $option_safe_name,
                                'optionCnCode'   => $option_convenience_code,
                                'optionCnName'   => $option_convenience_name,
                                'optionSnCode'   => $option_sound_code,
                                'optionSnName'   => $option_sound_name,
                                'yearMin'        => $reborn_obj['yemodelMin'],
                                'yearMax'        => $reborn_obj['yemodelMax'],
                                'yearStr'        => empty($reborn_obj['yemodel'])? 0 : $reborn_obj['yemodel'],
                                'licenseType'    => $reborn_obj['license'],
                            ];

                            $this->api_affiliate_reborn_car->update_car_info($wrci_idx, $bind_param);
                        }

                    }

                    $car_inventory = $this->api_affiliate_reborn_car->get_car_inventory($waab_idx);


                    $return_data['selectedWaabCarInventory'] = $car_inventory;

                }
            }

        }

        $this->load->view('head', $this->head_param);
        $this->load->view('apiAffiliate/apiAffiliateCar', ["data" => $return_data]);
        $this->load->view('footer');

    }

    /**
     * 정보 업데이트
     *
     * @author DEN
     */
    public function update_information()
    {
        $selected_waab_api = (int)$this->input->post('selectedApi');
        $change_obj        = $this->input->post('changeObj');

        if ($selected_waab_api === 1)
        {
            foreach ($change_obj as $key => $value_obj)
            {
                $wgci_idx = explode("inherence", $key)[1];
                $this->api_affiliate_grim_car->update_car_info($wgci_idx, $value_obj);
            }
        }
        elseif ($selected_waab_api === 2)
        {
            foreach ($change_obj as $key => $value_obj)
            {
                $wici_idx = explode("inherence", $key)[1];
                $this->api_affiliate_ins_car->update_car_info($wici_idx, $value_obj);
            }
        }
        elseif ($selected_waab_api === 3)
        {
            foreach ($change_obj as $key => $value_obj)
            {
                $wrci_idx = explode("inherence", $key)[1];
                $this->api_affiliate_reborn_car->update_car_info($wrci_idx, $value_obj);
            }
        }

        echo json_encode([
            'result' => 1
        ]);
    }

    /**
     * API 통신해야할 객체 찾기
     *
     * @author DEN
     *
     * @param $inventory    array  데이터베이스에 입력된 업체들
     * @param $req_waab_idx string 요청온 waab_idx, 요청이 없다면 공백
     *
     * @return array
     */
    private function get_search_waab_obj($inventory, $req_waab_idx)
    {
        if ($req_waab_idx === "")
            return [];
        else
        {
            $req_waab_idx = (int)$req_waab_idx;

            foreach ($inventory as $obj)
            {
                if ((int)$obj['waabIdx'] === $req_waab_idx)
                    return $obj;
            }
        }
    }

    /**
     * 배열을 스트링으로 변환
     *
     * @author DEN
     *
     * @param $connect_str string 연결 스트링
     * @param $arr         array  변환할 배열
     *
     * @return string
     */
    private function convert_array_to_string($connect_str, $arr)
    {
        if (gettype($arr) === "array")
        {
            $return_str  = "";
            $is_it_first = true;

            foreach ($arr as $value)
            {
                if ($is_it_first)
                    $is_it_first = false;
                else
                    $return_str .= $connect_str;

                $return_str .= $value;

            }

            return $return_str;
        }
        else
            return $arr;

    }

}
