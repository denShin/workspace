<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API 자차 등록
 *
 * @author Den
 *
 * @property CI_Session $session
 * @property WS_Input   $input
 *
 * @property Workspace_common_func  $workspace_common_func
 * @property Grim_api               $grim_api
 * @property Ins_api                $ins_api
 *
 * @property Api_affiliate_branch_model $api_affiliate_branch_model
 * @property Api_cdw_model              $api_cdw_model
 */

class ApiCdwManage extends WS_Controller {
    private $login_id;
    private $head_param;

    const GRIM   = 1;
    const INS    = 2;
    const REBORN = 3;

    const CDW_INVENTORY = 'cdwInven';

    const AJAX_SUCCESS = 1;

    function __construct()
    {
        parent::__construct();

        $this->load->library('Workspace_common_func');
        $this->load->library('api/grim/Grim_api');
        $this->load->library('api/ins/Ins_api');
        $this->load->model('apiAffiliate/api_affiliate_branch_model');
        $this->load->model('apiAffiliate/api_cdw_model');

        $this->login_id = (string)$this->session->userdata('admin_id');

        if ($this->login_id === "")
        {
            echo "<script>location.href='/adminmanage/Login'</script>";

        }
    }

    public function index()
    {
        $this->head_param = [
            'css'     => [],
            'library' => ['toastr']
        ];

        $crud = $this->input->get('crud');

        switch ($crud)
        {
            case "c":
                $this->create_request();
                break;
            case "":
            case "r":
                $this->read_request();
                break;
            case "u":
                $this->update_request();
                break;
            case "d":
                $this->delete_request();
                break;
            default:
                $this->read_affiliate_branch();
        }
    }

    private function read_request()
    {
        $read_data = $this->input->get('data');

        switch ($read_data)
        {
            case self::CDW_INVENTORY:
                $this->read_affiliate_cdw_inventory();
                break;
            case "":
            default:
                $this->read_affiliate_branch();
        }
    }

    private function disunite_api_branch_for_api($inventory)
    {
        $std_class = new stdClass();

        $std_class->grim   = [];
        $std_class->ins    = [];
        $std_class->reborn = [];

        foreach ($inventory as $obj)
        {
            switch ((int)$obj['waabApi'])
            {
                case self::GRIM:
                    array_push($std_class->grim, $obj);
                    break;
                case self::INS:
                    array_push($std_class->ins, $obj);
                    break;
                case self::REBORN:
                    array_push($std_class->reborn, $obj);
                    break;

            }
        }

        return $std_class;
    }

    private function read_affiliate_branch()
    {
        $waab_inventory = $this->api_affiliate_branch_model->load_api_affiliate_branch();

        $this->load->view('head', $this->head_param);
        $this->load->view('apiAffiliate/apiCdwManage', ["data" => $this->disunite_api_branch_for_api($waab_inventory)]);
        $this->load->view('footer');

    }

    private function read_affiliate_cdw_inventory()
    {
        $api      = (int)$this->input->get('api');
        $waab_idx = base64_decode($this->input->get('waabIdx'));

        $affiliate_information_return_obj = $this->api_affiliate_branch_model->load_api_affiliate_branch_information($waab_idx);
        $mapping_inventory = $this->api_cdw_model->get_cdw_mapping($api, $waab_idx);

        if ($affiliate_information_return_obj['result'] === 1)
        {
            $api_info = $affiliate_information_return_obj['info']['apiInfo'];

            $db_api = (int)$api_info['api'];

            if ($api === $db_api)
            {
                if ($api === 1)
                {
                    $start = date("Y-m-d",strtotime("+1 month", time()));
                    $end = date("Y-m-d",strtotime("+1 day", strtotime($start)));

                    $api_ret = $this->grim_api->get_car_list_inventory($api_info['info4'], $api_info['info1'], $api_info['info2'], $api_info['info3'], $start, $end);
                    $car_list = $api_ret['carlist'];

                    $insert_code = [];
                    $cdw_inventory = [];
                    foreach ($car_list as $car_obj)
                    {
                        $api_cdw_inventory = $car_obj['bohum'];
                        foreach ($api_cdw_inventory as $api_cdw_obj)
                        {
                            if (!(in_array($api_cdw_obj['code'], $insert_code)))
                            {
                                array_push($insert_code, $api_cdw_obj['code']);
                                array_push($cdw_inventory, $api_cdw_obj);

                            }
                        }
                    }

                    echo json_encode([
                        'result' => self::AJAX_SUCCESS,
                        'inventory' => $cdw_inventory,
                        'mapping' => $mapping_inventory
                    ]);


                }
                elseif ($api === 2)
                {
                    $cdw_inventory = $this->ins_api->get_affiliate_branch_cdw_inventory($api_info['info1']);

                    $mapping_check_array = [];
                    foreach ($mapping_inventory as $key => $item) {
                        array_push($mapping_check_array, $key);
                    }
                    $diff_check_array = [];
                    foreach ($cdw_inventory as $cdw_item) {
                        array_push($diff_check_array, $cdw_item->R보험번호);
                    }

                    $diff_array = array_values(array_diff($mapping_check_array, $diff_check_array));
                    $this->api_cdw_model->unmatching_cdw_mapping_by_api_key($waab_idx, $diff_array);

                    echo json_encode([
                        'result'    => self::AJAX_SUCCESS,
                        'inventory' => $cdw_inventory,
                        'mapping'   => $mapping_inventory
                    ]);

                }
                else
                {
                    log_message("error", "(ApiCdwManage) api 범위가 아닙니다 | $this->login_id | $api");
                    show_error("api 범위가 아닙니다", 0, "에러 발생");

                }
            }
            else
            {
                log_message("error", "(ApiCdwManage) api 값이 일치하지 않습니다 | $this->login_id | $api | $waab_idx");
                show_error("api 가 일치하지 않습니다", 0, "에러 발생");

            }

        }
        else
        {
            log_message("error", "(ApiCdwManage) 지점정보를 가져오지 못했습니다 | $this->login_id | $api | $waab_idx");
            show_error("지점정보 load 오류", 0, "에러 발생");

        }

    }

    private function create_request()
    {
        $request_method = $this->input->server('REQUEST_METHOD');

        if ($request_method === 'POST')
        {
            $api     = (int)$this->workspace_common_func->check_sql_injection($this->input->input_stream('api', TRUE), TRUE);
            $api_idx = $this->workspace_common_func->check_sql_injection($this->input->input_stream('apiIdx', TRUE), TRUE);

            if ($api === self::GRIM || $api === self::INS || $api === self::REBORN || $api_idx !== "")
            {
                $cdw_obj = $this->input->input_stream('cdwObj', TRUE);

                $cdw_obj['api'] = $api;
                $cdw_obj['apiIdx'] = $api_idx;

                $ret = $this->api_cdw_model->create_cdw_mapping($cdw_obj);
                echo json_encode([
                    'result' => self::AJAX_SUCCESS,
                    'wcmIdx' => $ret['wcmIdx']
                ]);

            }
            else
            {
                log_message("error", "(ApiCdwManage) create 요청 정보 부족 | $this->login_id | $api | $api_idx");
                show_error("요청 타입 오류", 0, "에러 발생");
            }


        }
        else
        {
            log_message("error", "(ApiCdwManage) 적절하지 못한 create 요청입니다 | $this->login_id | $request_method");
            show_error("요청 타입 오류", 0, "에러 발생");

        }

    }

    private function update_request()
    {
        $request_method = $this->input->server('REQUEST_METHOD');

        if ($request_method === 'PATCH')
        {
            $api     = (int)$this->workspace_common_func->check_sql_injection($this->input->input_stream('api', TRUE), TRUE);
            $api_idx = $this->workspace_common_func->check_sql_injection($this->input->input_stream('apiIdx', TRUE), TRUE);

            if ($api === self::GRIM || $api === self::INS || $api === self::REBORN || $api_idx !== "")
            {
                $cdw_obj = $this->input->input_stream('cdwObj', TRUE);

                $cdw_obj['api'] = $api;
                $cdw_obj['apiIdx'] = $api_idx;

                $this->api_cdw_model->update_cdw_mapping($api, $api_idx, $cdw_obj);
                echo json_encode([
                    'result' => self::AJAX_SUCCESS,
                    'wcmIdx' => $api_idx
                ]);

            }
            else
            {
                log_message("error", "(ApiCdwManage) update 요청 정보 부족 | $this->login_id | $api | $api_idx");
                show_error("요청 타입 오류", 0, "에러 발생");
            }


        }
        else
        {
            log_message("error", "(ApiCdwManage) 적절하지 못한 update 요청입니다 | $this->login_id | $request_method");
            show_error("요청 타입 오류", 0, "에러 발생");

        }

    }

    private function delete_request()
    {
        $request_method = $this->input->server('REQUEST_METHOD');
        if ($request_method === 'DELETE')
        {
            $waab_idx = (int)$this->workspace_common_func->check_sql_injection($this->input->input_stream('waabIdx', TRUE), TRUE);
            $wcm_idx = $this->workspace_common_func->check_sql_injection($this->input->input_stream('wcmIdx', TRUE), TRUE);

            $this->api_cdw_model->unmatching_cdw_mapping_by_wcm_idx($waab_idx, $wcm_idx);
            echo json_encode([
                'result' => self::AJAX_SUCCESS,
            ]);
        }
        else
        {
            log_message("error", "(ApiCdwManage) 적절하지 못한 delete 요청입니다 | $this->login_id | $request_method");
            show_error("요청 타입 오류", 0, "에러 발생");

        }
    }

}