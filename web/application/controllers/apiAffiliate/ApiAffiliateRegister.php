<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * API 업체 등록
 *
 * @author Den
 *
 * @property Api_affiliate_model                $api_affiliate_model
 * @property Api_affiliate_branch_model         $api_affiliate_branch_model
 * @property Api_affiliate_branch_biztalk_model $api_affiliate_branch_biztalk_model
 * @property Admin_upload_file_model            $admin_upload_file_model
 */

class ApiAffiliateRegister extends WS_Controller {
    private $login_id;
    private $login_am_idx;
    private $now;

    function __construct()
    {
        parent::__construct();

        $this->login_id = (string)$this->session->userdata('admin_id');

        if ($this->login_id === "")
        {
            echo "<script>location.href='/adminmanage/Login'</script>";

        }

        $this->load->model('apiAffiliate/api_affiliate_model');
        $this->load->model('apiAffiliate/api_affiliate_branch_model');
        $this->load->model('apiAffiliate/api_affiliate_branch_biztalk_model');
        $this->load->model('admin/admin_upload_file_model');

        $this->login_am_idx = $this->get_admin_idx($this->login_id);
        $this->now          = date('Y-m-d H:i:s', strtotime("+9 Hours"));
    }

    /**
     * @param  void
     * @return void
     */
    function index()
    {
        $affiliate_branch_idx = (int)$this->input->post('affiBranchIdx');

        if ($affiliate_branch_idx === -1) {
            $this->register_new_branch();
        } else
            $this->update_branch($affiliate_branch_idx);

    }

    function register_new_affiliate()
    {
        $bind_param = [$this->input->post('affiName'), $this->now, 1];

        return $this->api_affiliate_model->insert_api_affiliate($bind_param);
    }

    function register_new_branch()
    {
        $affiliate_idx = (int)$this->input->post('affiIdx');

        if ($affiliate_idx === -1)
            $affiliate_idx = $this->register_new_affiliate();

        $api_info     = $this->input->post('apiInfo');
        $address_info = $this->input->post('addressInfo');
        $work_time    = $this->input->post('workTime');
        $shuttle_info = $this->input->post('shuttleInfo');
        $biztalk_arr  = $this->input->post('biztalk');

        $bind_param = [
            'waaIdx'         => $affiliate_idx,
            'name'           => $this->input->post('branchName'),
            'api'            => $api_info['api'],
            'apiInfo1'       => $api_info['info1'],
            'apiInfo2'       => $api_info['info2'],
            'apiInfo3'       => (isset($api_info['info3']))? $api_info['info3'] : "",
            'mainAddress'    => $address_info['mainAddress'],
            'subAddress'     => $address_info['subAddress'],
            'latitude'       => $address_info['latitude'],
            'longitude'      => $address_info['longitude'],
            'tel'            => $this->input->post('tel'),
            'maxReserv'      => $this->input->post('maxReservDate'),
            'openTime'       => $work_time['openTime'].':00',
            'closeTime'      => $work_time['closeTime'].':00',
            'saleStart'      => $this->input->post('saleStart'),
            'shuttleName'    => $shuttle_info['stName'],
            'shuttleArea'    => $shuttle_info['stArea'],
            'shuttleAreaNum' => $shuttle_info['stAreaNumber'],
            'shuttleRace'    => $shuttle_info['stRace'],
            'shuttleSpend'   => $shuttle_info['stSpendTime'],
            'intro'          => $this->input->post('intro'),
            'unique'         => $this->input->post('uniqueness'),
            'enterDate'      => $this->input->post('enterDate'),
            'serviceString'  => $this->input->post('serviceString'),
            'deliveryString' => $this->input->post('deliveryString'),
            'specialLocation' => $this->input->post('specialLocation'),
            'regAmIdx'       => $this->login_am_idx,
            'regDate'        => $this->now
        ];

        $affiliate_branch_idx = $this->api_affiliate_branch_model->insert_api_affiliate_branch($bind_param);
        $this->proc_biztalk_members($affiliate_idx, $affiliate_branch_idx, $biztalk_arr);

        $encode_affiliate_idx        = base64_encode($affiliate_idx);
        $encode_affiliate_branch_idx = base64_encode($affiliate_branch_idx);

        echo json_encode([
            'result'  => 1,
            'regInfo' => [
                'affiIdx'       => $affiliate_idx,
                'affiBranchIdx' => $affiliate_branch_idx,
                'waai'          => $encode_affiliate_idx,
                'waabi'         => $encode_affiliate_branch_idx
            ]
        ]);

    }

    function update_branch($affi_branch_idx)
    {
        $post_data   = $this->input->post();
        $post_keys   = array_keys($post_data);
        $keys_length = count($post_keys);

        $affi_idx = (isset($post_data['affiIdx']))? $post_data['affiIdx'] : 0;

        if ($affi_idx === 0)
        {
            $log_data = json_encode($post_data);

            log_message("error", "(ApiAffiliateRegister) 회사 인덱스가 없다 | $this->login_id | $log_data");
            show_error("회사 인덱스가 없다", 0, "에러 발생");

        }

        if ($keys_length < 3)
        {
            // 변경 사항이 없다

            echo json_encode([
                'result'        => 1,
                'regInfo'       => [
                    'affiIdx'       => $affi_idx,
                    'affiBranchIdx' => $affi_branch_idx,
                ]
            ]);
        }
        elseif ($keys_length === 3 && isset($post_data['biztalk']))
        {
            // 알림톡 명단만 변경
            $biztalk_arr = $post_data['biztalk'];
            $this->proc_biztalk_members($affi_idx, $affi_branch_idx, $biztalk_arr);

            echo json_encode([
                'result'        => 1,
                'regInfo'       => [
                    'affiIdx'       => $affi_idx,
                    'affiBranchIdx' => $affi_branch_idx,
                ]
            ]);
        }
        else
        {
            // 다른 정보들도 변경

            if (isset($post_data['affiName']))
            {
                $this->api_affiliate_model->update_api_affiliate($affi_idx, trim($post_data['affiName']));
                unset($post_data['affiName']);
            }

            if (isset($post_data['biztalk']))
            {
                $biztalk_arr = $post_data['biztalk'];
                $this->proc_biztalk_members($affi_idx, $affi_branch_idx, $biztalk_arr);
                unset($post_data['affiName']);
            }

            if (isset($post_data['isLogoDelete']))
            {
                $is_logo_delete = (int)$post_data['isLogoDelete'];
                if ($is_logo_delete === 1)
                {
                    $delete_bind_param = [
                        'boardCode'   => 'partners',
                        'fileUpType'  => 'api_logo',
                        'contentCode' => 'aplg_'.$affi_branch_idx,
                    ];

                    $this->admin_upload_file_model->delete_file($delete_bind_param);
                }

            }

            if (isset($post_data['subDelete']))
            {
                $sub_delete = $post_data['subDelete'];

                foreach ($sub_delete as $file_name)
                {
                    $this->admin_upload_file_model->delete_file_by_name($file_name);

                }

                unset($post_data['subDelete']);
            }

            unset($post_data['affiBranchIdx']);
            unset($post_data['affiIdx']);

            if (count($post_data) !== 0)
                $this->api_affiliate_branch_model->update_api_affiliate_branch($affi_branch_idx, $post_data);

            echo json_encode([
                'result'        => 1,
                'regInfo'       => [
                    'affiIdx'       => $affi_idx,
                    'affiBranchIdx' => $affi_branch_idx,
                ]
            ]);

        }


    }

    function proc_biztalk_members($affiliate_idx, $affiliate_branch_idx, $biztalk_arr)
    {
        foreach ($biztalk_arr as $biztalk_obj)
        {
            if (isset($biztalk_obj['isNew']))
            {
                $biztalk_bind_param = [
                    'waaIdx'  => $affiliate_idx,
                    'waabIdx' => $affiliate_branch_idx,
                    'name'    => $biztalk_obj['name'],
                    'tel'     => $biztalk_obj['tel'],
                    'amIdx'   => $this->login_am_idx,
                    'regDate' => $this->now
                ];

                $this->api_affiliate_branch_biztalk_model->insert_biztalk_members($biztalk_bind_param);
            }
            elseif (isset($biztalk_obj['isDelete']))
            {
                if ($biztalk_obj['isDelete'])
                    $this->api_affiliate_branch_biztalk_model->delete_biztalk_members($biztalk_obj['memberIdx']);
            }
            else
            {
                $biztalk_bind_param = [
                    'waabbIdx' => $biztalk_obj['memberIdx'],
                    'name'     => $biztalk_obj['name'],
                    'tel'      => $biztalk_obj['tel']
                ];

                $this->api_affiliate_branch_biztalk_model->update_biztalk_members($biztalk_bind_param);
            }

        }
    }
}
