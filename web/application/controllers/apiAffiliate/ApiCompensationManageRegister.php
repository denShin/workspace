<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Den
 *
 * @property Workspace_common_func  $workspace_common_func
 *
 * @property Api_compensation_model $api_compensation_model
 */

class ApiCompensationManageRegister extends WS_Controller {
    private $login_id;
    private $login_am_idx;
    private $now;

    function __construct()
    {
        parent::__construct();

        $this->login_id = (string)$this->session->userdata('admin_id');

        if ($this->login_id === "")
        {
            echo "<script>location.href='/adminmanage/Login'</script>";

        }

        $this->load->library('Workspace_common_func');
        $this->load->model('apiAffiliate/api_compensation_model');

        $this->login_am_idx = $this->get_admin_idx($this->login_id);
        $this->now          = date('Y-m-d H:i:s', strtotime('+9 hours'));
    }

    public function index()
    {
        $type = $this->input->get('type');
        switch ($type)
        {
            case "c":
                $this->insert_compensation();
                break;
            case "u":
                $this->update_compensation();
                break;
            case "d":
                $this->delete_compensation();
                break;
            default:
                log_message("error", "(ApiCompensationManageRegister) 일치하는 타입이 없습니다 | $this->login_id | $type");
                show_error("일치하는 타입이 없습니다", 0, "에러 발생");
                break;
        }

    }

    /**
     * 신규 종합보험 추가
     *
     * @author Den
     */
    public function insert_compensation()
    {
        $post_data = $this->input->post();

        $bind_param = [
            'amIdx' => $this->login_am_idx,
            'now'   => $this->now
        ];

        $type = 0;

        foreach ($post_data as $key => $value)
        {
            if ($key === "type")
            {
                continue;
            }
            else
            {
                if ($key === "compensationType")
                    $type = (int)$value;

                $bind_param[$key] = $value;
            }

        }

        $inserted_idx = $this->api_compensation_model->insert_compensation($type, $bind_param);

        echo json_encode([
            'result'  => 1,
            'regInfo' => [
                'wacIdx'  => $inserted_idx,
            ]
        ]);
    }

    /**
     * 종합보험 수정
     *
     * @author Den
     */
    public function update_compensation()
    {
        $post_data = $this->input->post();
        $wac_idx   = base64_decode($this->workspace_common_func->check_false_value($this->input->get('wac'), true));

        if (count($post_data) >= 1 && $wac_idx !== "")
        {
            $this->api_compensation_model->update_compensation($wac_idx, $post_data);
            echo json_encode([
                'result' => 1
            ]);

        }
    }

    /**
     * 종합보험 삭제
     *
     * @author Den
     */
    public function delete_compensation()
    {
        $wac_idx = base64_decode($this->workspace_common_func->check_false_value($this->input->get('wac'), true));

        if ($wac_idx !== "")
        {
            $this->api_compensation_model->delete_compensation($wac_idx);
            echo json_encode([
                'result' => 1
            ]);

        }

    }
}
