<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 위치정보이용 로그
 *
 * @property CI_Session $session
 * @property WS_Input   $input
 * @property CI_Config  $config
 *
 * @property Workspace_common_func $workspace_common_func
 *
 * @property Location_usage_model $location_usage_model
 */

class LocationUsage extends WS_Controller
{
    private $login_id;
    private $head_param;

    const PAGE_ROW = 10;

    function __construct()
    {
        parent::__construct();

        $this->load->model('carmore/log/location_usage_model');

        $this->login_id = (string)$this->session->userdata('admin_id');

        if ($this->login_id === "") {
            echo "<script>location.href='/adminmanage/Login'</script>";

        }

    }

    public function index()
    {
        $this->head_param = [
            'css'     => [],
            'library' => ['toastr', 'dropzone']
        ];

        $date = $this->input->get('date');

        if (empty($date))
            $date =  substr(date('Y-m-d H:i:s', strtotime('+9 hours')), 0, 10);

        $this->get_usage_inventory($date);
    }

    public function get_usage_inventory($date)
    {
        $usage_return = $this->location_usage_model->read_usage_inventory($date);
        if ($this->login_id !== '')
        $this->log_access_information();
        if ($usage_return->result)
        {
            $return_object = new stdClass();

            $return_object->date      = $date;
            $return_object->inventory = $usage_return->inventory;

            $this->load->view('head', $this->head_param);
            $this->load->view('carmore/log/locationUsage', ['data' => $return_object]);
            $this->load->view('footer');

        }
    }

    public function log_access_information()
    {
        $this->location_usage_model->log_usage_access($this->login_id);
    }

}