<?php
/**
 * CarmoreCallCate.php - 고객전화상담의 카테고리 관리
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class CarmoreCallCate extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION;

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/CarmoreCallBoard_model');

        if ($this->session->userdata('admin_id') == "") {
            echo "<script>location.href='/adminmanage/Login'</script>";
            exit();
        }

    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);
        if($ptype=="") $ptype = $this->input->post('ptype', TRUE);

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        switch($ptype) {
            case "setCallCateOrder" : $this->setCallCateOrder();break;
            case "setCallCateInfo" : $this->setCallCateInfo();break;
            case "del" : $this->delCallCateInfo(); break;
            case "edit"  : $this->setCallCateInfo(); break;
            case "w" : $this->loadCallCateWrite(); break;
            default : $this->loadCallCateList(); break;
        }
        $this->load->view('footer');
    }

    // 고객전화 카테고리 리스트 페이지 호출
    public function loadCallCateList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);

        $arr_data = $this->CarmoreCallBoard_model->getCallCateInfo("list","");

        $data["list"]=$arr_data;

        $this->load->view('carmore/callcatelist', array('data'=>$data));

    }

    //고객전화 카테고리정보 등록,수정 페이지 호출
    public function loadCallCateWrite()
    {

        $callboardcate_idx = $this->input->get('callboardcate_idx', TRUE);

        if($callboardcate_idx !=""){
            $data["emode"]="edit";
            $arr_data = $this->CarmoreCallBoard_model->getCallCateInfo("d",$callboardcate_idx);

            $data["callboardcate_idx"] =$arr_data["callboardcate_idx"];
            $data["callboardcate_name"] =$arr_data["callboardcate_name"];

        }else{
            $data["emode"]="new";
            $data["callboardcate_idx"] ="";
            $data["callboardcate_name"] ="";

        }
        $this->load->view('carmore/callcatewrite', array('data'=>$data));
    }


    // 카테고리정보 수정,등록
    public function setCallCateInfo()
    {

        $data["emode"] =$this->input->post('emode');
        $data["callboardcate_idx"] =$this->input->post('callboardcate_idx');
        $data["callboardcate_name"] =$this->input->post('callboardcate_name');

        $cateresult = $this->CarmoreCallBoard_model->setCallCateInfo($data);
        $result=$cateresult["result"];

        if($result=="n"){
            echo "<script>alert('*동일한 카테고리명이 등록되어 있습니다.');history.back();</script>";
            exit();
        }else if($result=="y"){
            echo "<script>location.href='/carmore/CarmoreCallCate'</script>";
            exit();
        }
     }

    // 카테고리정보 삭제 처리
    public function delCallCateInfo()
    {
        $data["emode"] ="del";
        $data["callboardcate_idx"] =$this->input->get('callboardcate_idx');

        $return_v = $this->CarmoreCallBoard_model->setCallCateInfo($data);
        echo "<script>location.href='/carmore/CarmoreCallCate'</script>";
        exit();
    }

    // 카테고리 정렬순서 수정
    public function setCallCateOrder(){
        $data["order"] =$this->input->post('order');

        $return_v = $this->CarmoreCallBoard_model->setCallCateOrder($data);
        exit();
    }

}
