<?php
/**
 * Carshortreturnproc.php - 자동차 단기 렌트 반환처리처리 컨트롤  ( 사용 안함 X)
 */
header('Content-Type: text/html; charset=UTF-8');


require_once '/home/workspace/web/static/NICEPAY/PC_TX_PHP/lib/nicepay/web/NicePayWEB.php';
require_once '/home/workspace/web/static/NICEPAY/PC_TX_PHP/lib/nicepay/core/Constants.php';
require_once '/home/workspace/web/static/NICEPAY/PC_TX_PHP/lib/nicepay/web/NicePayHttpServletRequestWrapper.php';

class Carshortreturnproc   extends CI_Controller {
    private $SLACK_TOKEN, $BIZTALK_TOKEN;

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library('Customfunc');
        $this->load->library('Aws_ses');

        $this->load->model('carmore/Reservation_model');

        $this->SLACK_TOKEN="xoxp-7624110066-320657195828-339839954532-adb425715a22edcac5b86f438f0b2807";
        $this->BIZTALK_TOKEN="7e7023c4890abb6ee693d9304283edb3bf6ad7d8";


        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        $demode =$this->input->get('emode');
        if($demode=="")  $demode =$this->input->post('emode');


        switch($demode) {
            case "changereturnaddr": $this->changereturnaddr();break;
            case "commentproc": $this->commentproc();break;
            default : $this->setCareturn(); break;
        }
    }

    public function changereturnaddr(){
        $data["return_addr"] =$this->input->get('return_addr');
        $data["crti_idx"] =$this->input->get('crti_idx');
        $data["f_reservationidx"] =$this->input->get('f_reservationidx');

        $result= $this->Reservation_model->changereturnaddr($data);


    }

    public function commentproc(){
        $data["datamode"] =$this->input->post('datamode');
        $data["commenttext"] =$this->input->post('commenttext');
        $data["crti_idx"] =$this->input->post('crti_idx');
        $data["mem_id"] =  $this->session->userdata('admin_id');
        $data["comment_idx"] =$this->input->post('comment_idx');

        $result= $this->Reservation_model->procAdmincomment($data);

        echo "<script>location.href='/carmore/Earlycareturn?ptype=v&crti_idx=".$data["crti_idx"]."'</script>";
        exit();
    }

    public function setCareturn()
    {
        //  http://workspace2.teamo2.kr/carmore/Careturnproc?ptype=setcareturn&crti_idx=992&cancelper=4&useprice=9600&canceldate=2018-11-15&cancelamt=400

        $crti_idx =$this->input->post('crti_idx');
        $cancelamt =$this->input->post('cancelamt');
        $cancelper =$this->input->post('cancelper');
        $canceldate =$this->input->post('canceldate');
        $useprice =$this->input->post('useprice');

        $arr_cancelinfo = $this->Reservation_model->get_cancelinfo($crti_idx);
        $arr_cancelinfo["realcancelper"]=$cancelper;
        $arr_cancelinfo["cancelamt"]=$cancelamt;
        $arr_cancelinfo["canceldate"]=$canceldate;

        //print_r($arr_cancelinfo);
        // 예약정보 상세 가져오기

        $arr_reservdtl = $this->Reservation_model->getEarlycareturndetail($crti_idx);
        $arr_reservdtl["cancelamt"]=$cancelamt;
        $arr_reservdtl["canceldate"]=$canceldate;
        $arr_reservdtl["useprice"]=$useprice;


        // 취소 금액이 0원 일때
        if($cancelamt=="0"){
            #####################################################
            # db 플래그,반납일 등 업데이트
            # tbl_reservation_list 에서 rentstatus = 2 변경
            #####################################################
            $refundresult= $this->Reservation_model->procCareturn($arr_cancelinfo);

            #####################################################
            ## 고객 sms 전송하기
            #####################################################
            $this->sendSmsMessage($arr_reservdtl);

            #####################################################
            ## slack 전송하기
            #####################################################
            $this->sendSlackMessage($arr_reservdtl);


            #####################################################
            ## aws ses 전송하기
            #####################################################
            $this->sendcancelEmail($arr_reservdtl);


            echo "<script>alert('처리가 완료되었습니다.');location.href='/carmore/Earlycareturn'</script>";
            exit();
        }


        // 취소 금액이 0원 보다 클때
        #####################################################
        ## 카드 결제 취소
        # 에러코드 2051 : 카드포인트 사용한 결제는 부분취소가 안된다.
        #####################################################
        $arr_nicedata= $this->cancelcardpay($arr_cancelinfo);
        $nice_result_code=$arr_nicedata["nice_result_code"];

        if($nice_result_code=="2001" || $nice_result_code=="2211"){

            // $arr_cancelinfo 에 결제 정보 데이터 담기


            $arr_cancelinfo["nice_result_code"]=$arr_nicedata["nice_result_code"];
            $arr_cancelinfo["nice_cancel_amt"]=$arr_nicedata["nice_cancel_amt"];
            $arr_cancelinfo["nice_result_msg"]=$arr_nicedata["nice_result_msg"];
            $arr_cancelinfo["nice_mid"]=$arr_nicedata["nice_mid"];
            $arr_cancelinfo["nice_tid"]=$arr_nicedata["nice_tid"];
            $arr_cancelinfo["nice_moid"]=$arr_nicedata["nice_moid"];


            #####################################################
            # db 플래그,반납일 등 업데이트
            # tbl_reservation_list 에서 rentstatus = 2 변경
            #####################################################
            $refundresult= $this->Reservation_model->procCareturn($arr_cancelinfo);

            #####################################################
            ## 고객 sms 전송하기
            #####################################################
            $this->sendSmsMessage($arr_reservdtl);

            #####################################################
            ## slack 전송하기
            #####################################################
            $this->sendSlackMessage($arr_reservdtl);

            #####################################################
            ## aws ses 전송하기
            #####################################################
            $this->sendcancelEmail($arr_reservdtl);

            echo "<script>alert('처리가 완료되었습니다.');location.href='/carmore/Earlycareturn'</script>";
            exit();
        }else{

            if($nice_result_code == "2051"){
                $errmsg ="카드포인트 사용한 결제는 부분취소가 안됩니다.";
            }else{
                $errmsg ="[Error:".$nice_result_code."] 오류가 발생하였습니다.";
            }

            echo "<script>alert('".$errmsg."');location.href='/carmore/Earlycareturn';</script>";
            exit();


        }

    }

    // send ses
    function sendcancelEmail($data){

        // tbl_reservation 에 f_driver 이메일
        $decrypt_driver_name =$this->customfunc->decrypt($data["driver_name_encrypt"] );
        $driver_phone_encrypt =$this->customfunc->decrypt($data["driver_phone_encrypt"]  );
        $driver_phone_encrypt =$this->customfunc->add_phonehyphen($driver_phone_encrypt);
        $driver_birthday_encrypt =$this->customfunc->decrypt($data["driver_birthday_encrypt"] );

        if($decrypt_driver_name=="팀오투테스트"){
            return;
        }

        $get_reservation_crti_idx =$data["f_reservationidx"];
        $view_start_str =$data["crti_start_date"];
        $view_end_date =$data["crti_end_date"];
        $f_delivpickaddr =$data["f_delivpickaddr"];
        $f_delivreturnaddr =$data["f_delivreturnaddr"];
        $f_driveremail =$data["f_driveremail"];

        $name =$data["name"];
        $brand =$data["brand"];
        $car_name =$brand." ".$data["model"];
        $db_cdw_name =$data["db_cdw_name"];
        $view_cdw_compensation =$data["view_cdw_compensation"];
        $rental_cost_format =$data["crti_rent_cost"];
        $cdw_cost_format =$data["crti_cdw_cost"];
        $deposit_cost_format =$data["crti_deposit"];
        $discount_format =$data["discount_format"];
        $db_payment =$data["crti_payment"];
        $get_cancel_amt =$data["cancelamt"];
        $crti_early_return_date =$data["crti_early_return_date"];
        $email_penalty =$db_payment -$get_cancel_amt;


        $emaildata["company"]=$name ;
        $emaildata["residx"]=$get_reservation_crti_idx;
        $emaildata["startdate"]=$view_start_str;
        $emaildata["enddate"]=$view_end_date;
        $emaildata["carmodel"]=$car_name;
        $emaildata["driver"]=$decrypt_driver_name;
        $emaildata["pickupaddr"]=$f_delivpickaddr;
        $emaildata["return_addr"]=$f_delivreturnaddr;
        $emaildata["origincost"]=number_format($rental_cost_format);
        $emaildata["dbpayment"]=number_format($db_payment);
        $emaildata["discount"]=number_format($discount_format);
        $emaildata["cancelamt"]=number_format($get_cancel_amt);
        $emaildata["emailpenalty"]=number_format($email_penalty);
        $emaildata["logdate"]=$crti_early_return_date;
        $emaildata["part"]="returncar";

        if($f_driveremail !=""){
            $content= $this->customfunc->get_emailhtml($emaildata);
            $title=" [카모아 - 취소되었습니다]'.$decrypt_driver_name.'님 취소완료 안내";
            $arr_dest=array($f_driveremail);
            $ses = $this->aws_ses->sendEmail($arr_dest,$title,$content);
        }
        return true;

    }

    // slack message send
    function sendSlackMessage($data){

        $channel="#카모아_예약이_월렌트";

        //  bookerPhone - tbl_usr_list phone_Type - a안드로이드 i 아이폰


        $decrypt_driver_name =$this->customfunc->decrypt($data["driver_name_encrypt"] );
        $driver_phone_decrypt =$this->customfunc->decrypt($data["driver_phone_encrypt"]  );
        $driver_phone_decrypt =$this->customfunc->add_phonehyphen($driver_phone_decrypt);
        $driver_birthday_encrypt =$this->customfunc->decrypt($data["driver_birthday_encrypt"] );

        if($decrypt_driver_name=="팀오투테스트"){
            return;
        }

        $get_reservation_crti_idx =$data["f_reservationidx"];
        $view_start_str =$data["crti_start_date"];
        $view_end_date =$data["crti_end_date"];
        $useday =$this->customfunc->get_daydiff($view_start_str,$view_end_date);

        $brand =$data["brand"];
        $car_name =$brand." ". $data["model"];
        $db_cdw_name =$data["db_cdw_name"];
        $view_cdw_compensation = $this->customfunc->getCdwTxt( $data["compensation"]);
        $rental_cost_format =$data["crti_rent_cost"];
        $cdw_cost_format =$data["crti_cdw_cost"];
        $deposit_cost_format =$data["crti_deposit"];
        $discount_format =$data["crti_use_point"] +$data["crti_use_coupon_value"];
        $db_payment =$data["crti_payment"];
        $company_name =$data["company_name"];
        $branch_name =$data["branch_name"];


        $get_cancel_amt =$data["cancelamt"];
        $email_penalty =$db_payment -$get_cancel_amt;


        $slackMessage = "[조기반납신청]
        예약번호 : ".$data['crti_reservation_idx'] ."
        구간번호 : ".$data['crti_idx']."
        예약자명 : ".$decrypt_driver_name." 
        예약자연락처 : ".preg_replace('/(0(?:2|[0-9]{2}))([0-9]+)([0-9]{4}$)/', '\\1-\\2-\\3', $driver_phone_decrypt )."
        예약기간 : ".$view_start_str ." ~ ".$view_end_date ." (".$useday.")
        업체 : ".$data['company_name']." ".$data['branch_name']."
        차량정보 : ".$car_name."
        결제금액 : ".number_format((int)$data['crti_payment'])."원 (대여료: ".number_format((int)$data['crti_rent_cost'])."원, 자차료: ".number_format((int)$data['crti_cdw_cost'])."원, 사용포인트: ".number_format((int)$data['f_usepoint'])."P, 쿠폰사용액: ".number_format((int)$data['crti_use_coupon_value'])."원)
        보증금 : ".number_format($data['crti_deposit'])."원 

<조기반납신청정보>
        신청일 : ".$data['canceldate']."
        이용예정일수 : ".$useday."일
        실사용금액 : ".number_format($email_penalty)."원
        환불금액 : ".number_format( $get_cancel_amt )."원 ";

        $this->customfunc->slacksend($this->SLACK_TOKEN,$channel,':morecar_why:','관리자',$slackMessage);

    }


    // 고객 sms message send
    function sendSmsMessage($data){

        // 업체 발송 번호 :new_branch_contact_member

        $decrypt_driver_name =$this->customfunc->decrypt($data["driver_name_encrypt"] );
        $driver_phone_decrypt =$this->customfunc->decrypt($data["driver_phone_encrypt"]  );
        $driver_phone_decrypt =$this->customfunc->add_phonehyphen($driver_phone_decrypt);
        $driver_birthday_encrypt =$this->customfunc->decrypt($data["driver_birthday_encrypt"] );

        if($decrypt_driver_name=="팀오투테스트"){
            return;
        }
        $get_reservation_crti_idx =$data["f_reservationidx"];
        $crti_idx =$data["crti_idx"];
        $view_start_str =$data["crti_start_date"];
        $view_end_date =$data["crti_end_date"];
        $useday =$this->customfunc->get_daydiff($view_start_str,$view_end_date);

        $useprice =$data["useprice"];
        $company_serial = $data["company_serial"];
        $branch_serial = $data["rentCompany_branch_serial"];
        $f_delivpickaddr = $data["f_delivpickaddr"];
        $f_delivreturnaddr = $data["f_delivreturnaddr"];


        $brand =$data["brand"];
        $car_name =$brand." ". $data["model"];
        $db_cdw_name =$data["db_cdw_name"];
        $view_cdw_compensation = $this->customfunc->getCdwTxt( $data["compensation"]);
        $rental_cost_format =$data["crti_rent_cost"];
        $cdw_cost_format =$data["crti_cdw_cost"];
        $deposit_cost_format =$data["crti_deposit"];
        $discount_format =$data["crti_use_point"] +$data["crti_use_coupon_value"];
        $db_payment =$data["crti_payment"];
        $get_cancel_amt =$data["cancelamt"];
        $email_penalty =$db_payment -$get_cancel_amt;


        $content="[카모아 - 조기 반납 완료되었습니다] 
                    
$decrypt_driver_name 님, 월렌트 조기반납 확정 안내 

예약번호 : ".$get_reservation_crti_idx."
예약자명 : ".$decrypt_driver_name."
예약시작 : ".$view_start_str."
예약종료 : ".$view_end_date."
이용일수 : ".$useday."일 
차량정보 : ".$car_name."
보증금: ".number_format($deposit_cost_format)."원 
결제금액 : ".number_format($db_payment)."원 (차량대여료 ".number_format($rental_cost_format)."원, 자차요금 ".number_format($cdw_cost_format)."원, 할인요금 ".number_format($discount_format)."원) 

조기반납일 : ".$data['canceldate']." 
실제이용일수 : ".$useday."일

[결제취소 예정정보] 
차량대여료: ".number_format($rental_cost_format)."원 
자차요금: ".number_format($cdw_cost_format)."원 
할인요금 : ".number_format($discount_format)."원 
총 결제요금 : ".number_format($db_payment)."원 

실제이용금액 : ".number_format($useprice)."원
총 취소/환불금액 : ".number_format($get_cancel_amt)."원 
취소수수료 차감 : ".number_format($email_penalty)."원 

환불은 카드사 영업일 기준 3-5일 정도 소요될 수 있습니다. 
카모아를 이용해주셔서 감사합니다.";

        //$driver_phone_decrypt="01030043970";

        $smsdata["biztalk_now"]= date('Y-m-d H:i:s');
        $smsdata["content"]=$content;
        $smsdata["get_reservation_crti_idx"]=$get_reservation_crti_idx."E".$crti_idx;
        $smsdata["decrypt_driver_phone"]=$driver_phone_decrypt;
        $smsdata["template_code"]="C00MRTD";

        $this->Reservation_model->procSmsinfo($smsdata);

        /* 렌트카 회사 발송
        $company_serial="10110";
        $branch_serial="198"; */
        $telitems =$this->Reservation_model->get_branchtel( $company_serial,$branch_serial );

        $companycontent="[카모아] 관리자님, 월렌트 $decrypt_driver_name / $car_name 조기반납 1건이 처리되었습니다. 
                            
예약번호 : ".$get_reservation_crti_idx."
예약자명 : ".$decrypt_driver_name."
연락처 : ".$driver_phone_decrypt."
예약시작 : ".$view_start_str."
예약종료 : ".$view_end_date."
이용일수 : ".$useday."일 
배차주소 : ".$f_delivpickaddr."
반차주소 : ".$f_delivreturnaddr."
차량정보 : ".$car_name."
자차: ".$db_cdw_name." (보상한도 ".$view_cdw_compensation.") 

조기반납신청일 : ".$data['canceldate']." 
실제이용일수 : ".$useday."일

[결제정보] 
차량대여료: ".number_format($rental_cost_format)."원 
자차요금: ".number_format($cdw_cost_format)."원 
보증금: ".number_format($deposit_cost_format)."원 
총 결제요금: ".number_format($db_payment)."원 

[결제취소 예정정보] 
실제이용금액 : ".number_format($useprice)."원
예상 취소수수료 차감 : ".number_format($email_penalty)."원 
총 취소/환불예정금액 : ".number_format($get_cancel_amt)."원
보증금: ".number_format($deposit_cost_format)."원  (보증금은 현장에서 직접 처리해주시면 됩니다.)";

        foreach ($telitems as $row) {

            //$row["phone"]="01030043970";

            $smsdata["biztalk_now"]= date('Y-m-d H:i:s');
            $smsdata["content"]=$companycontent;
            $smsdata["get_reservation_crti_idx"]=$get_reservation_crti_idx."E".$crti_idx;
            $smsdata["decrypt_driver_phone"]=$row["phone"];
            $smsdata["template_code"]="C00MRTD-M";
            $this->Reservation_model->procSmsinfo($smsdata);
        }


    }

    // card pay cancel
    function cancelcardpay($arr_cancelinfo){
        #####################################################
        ## 결제 취소,반납 진행
        #####################################################

        # pg 사 결제취소 진행
        /*
        * http://workspace2.teamo2.kr/carmore/Careturnproc?ptype=setcareturn&crti_idx=911&cancelamt=103730&_=1542094192667

        $tid - tbl_pay_log 에서 가져옴
        crti_extend_check : 1 - 연장, 0 - 기본
        crti_reservation_idx : 기본일때
        crti_reservation_idx + E + crti_idx

         */


        $cancelamt=$arr_cancelinfo["cancelamt"];
        $MID="teamo2000m";
        $TID= $arr_cancelinfo["tid"];
        $CancelAmt=$cancelamt;
        $CancelMsg="고객 요청";
        $CancelPwd="teamo2000";
        $Moid=$arr_cancelinfo["moid"];
        $PartialCancelCode ="1";

        $postData = array(
            'MID'    => $MID,
            'TID'  => $TID,
            'CancelAmt' => $CancelAmt,
            'CancelMsg' => $CancelMsg,
            'CancelPwd' => $CancelPwd,
            'PartialCancelCode' => $PartialCancelCode,
            'Moid'     => $Moid
        );

        /** 1. Request Wrapper 클래스를 등록한다.  */
        $httpRequestWrapper = new NicePayHttpServletRequestWrapper($postData);
        $_REQUEST = $httpRequestWrapper->getHttpRequestMap();

        /** 2. 소켓 어댑터와 연동하는 Web 인터페이스 객체를 생성한다.*/
        $nicepay_web_class = new NicePayWEB();

        /** 2-1. 로그 디렉토리 설정 */
        //$nicepay_web_class->setParam("NICEPAY_LOG_HOME", "paylog");

        /** 2-2. 로그 모드 설정(0: DISABLE, 1: ENABLE) */
        $nicepay_web_class->setParam("APP_LOG", "0");

        /** 2-3. 암호화플래그 설정(N: 평문, S:암호화) */
        $nicepay_web_class->setParam("EncFlag", "S");

        /** 2-4. 서비스모드 설정(결제 서비스 : PY0 , 취소 서비스 : CL0) */
        $nicepay_web_class->setParam("SERVICE_MODE", "CL0");

        /** 2-5 UTF-8 (den)*/
        $nicepay_web_class->setParam("CHARSET", "UTF8"); // utf-8 사용시 옵션 설정

        /** 3. 결제취소 요청 */
        $nice_response_obj = $nicepay_web_class->doService($_REQUEST);


        /** 4. 취소결과 */

        $arr_data["nice_result_code"] = trim($nice_response_obj->getParameter("ResultCode")); // 결과코드 (정상 :2001(취소성공), 2211(환불성공), 그 외 에러)
        $arr_data["nice_result_msg"] = $nice_response_obj->getParameter("ResultMsg");   // 결과메시지
        $arr_data["nice_cancel_amt"] = $nice_response_obj->getParameter("CancelAmt");   // 취소금액
        $arr_data["nice_mid"] = $nice_response_obj->getParameter("MID");              // 상점 ID
        $arr_data["nice_tid"] = $nice_response_obj->getParameter("TID");               // TID
        $arr_data["nice_moid"] = trim($nice_response_obj->getParameter("Moid"));  // 주문번호

        return $arr_data;

    }

}
