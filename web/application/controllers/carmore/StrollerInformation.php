<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 유모차 업체정보
 *
 * @author DEN
 *
 * @property CI_Session $session
 * @property WS_Input   $input
 *
 * @property Stroller_info_model     $stroller_info_model
 * @property Admin_upload_file_model $admin_upload_file_model
 */

class StrollerInformation extends WS_Controller {
    private $login_id;
    private $login_am_idx;
    private $head_param;
    private $now;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('workspace_common_func');

        $this->login_id = (string)$this->session->userdata('admin_id');

        if ($this->login_id === "")
        {
            echo "<script>location.href='/adminmanage/Login'</script>";

        }

        $this->load->model('carmore/stroller_info_model');
        $this->load->model('admin/admin_upload_file_model');

        $this->login_am_idx = $this->get_admin_idx($this->login_id);
        $this->now          = date('Y-m-d H:i:s', strtotime('+9 hours'));
    }

    /**
     * @param  void
     * @return void
     */
    public function index()
    {
        $this->head_param = [
            'library' => ['toastr', 'dropzone']
        ];

        $type = $this->input->get('crud', TRUE);

        switch ($type) {
            case "c":
                $this->create_stroller();
                break;
            case "r":
                $this->read_stroller();
                break;
            case "u":
                $this->update_stroller();
                break;
            case "d":
                $this->delete_stroller();
                break;
            default:
                log_message("error", "(StrollerInformation) 정의되지 않은 타입 | $this->login_id | $type");
                show_error("정의되지 않은 타입입니다", 0, "에러 발생");
                break;

        }

    }

    /**
     * 유모차업체 신규등록
     *
     * @author DEN
     */
    private function create_stroller()
    {
        $stroller_info = $this->input->post('strollerInfo');

        $inserted_obj = $this->stroller_info_model->create_stroller_information($stroller_info);

        if ($inserted_obj->result === 1)
        {
            $cs_idx = $inserted_obj->insertedIdx;
            if (isset($stroller_info['matchingAffiliate']))
                $this->stroller_info_model->matching_stroller($cs_idx, $stroller_info['matchingAffiliate']);

            echo json_encode($inserted_obj);
        }
        else
        {
            log_message("error", "(StrollerInformation) 신규 유모차업체등록에 문제가 발생했습니다 | $this->login_id | ".json_encode($inserted_obj)." | ".json_encode($stroller_info));
            show_error("신규 유모차업체등록에 문제가 발생했습니다", 0, "에러 발생");

        }

    }

    /**
     * 유모차업체 정보가져오기
     *
     * @author DEN
     */
    private function read_stroller()
    {
        $stroller_idx = (int)base64_decode($this->input->get('csIdx'));

        if ($stroller_idx === -1)
        {
            $data_param = [
                'csIdx' => -1
            ];

            $this->load->view('head', $this->head_param);
            $this->load->view('carmore/strollerInformation', ['data' => $data_param]);
            $this->load->view('footer');

        }
        else
        {
            $stroller_obj = $this->stroller_info_model->read_stroller_information($stroller_idx);
            if ($stroller_obj->result === 1)
            {
                $data_param = [
                    'csIdx' => $stroller_idx,
                    'info'  => $stroller_obj->info
                ];

                $this->load->view('head', $this->head_param);
                $this->load->view('carmore/strollerInformation', ['data' => $data_param]);
                $this->load->view('footer');

            }
            elseif ($stroller_obj->result === 2)
            {
                log_message("error", "(StrollerInformation) 유모차업체 불러오기에 문제가 발생했습니다 | $this->login_id | ".json_encode($stroller_obj)." | ".$stroller_idx);
                show_error("유모차업체 불러오기에 문제가 발생했습니다", 0, "에러 발생");

            }
            else
            {
                log_message("error", "(StrollerInformation) 유모차업체 불러오는데 서버에 문제가 발생했습니다 | $this->login_id | ".json_encode($stroller_obj)." | ".$stroller_idx);
                show_error("유모차업체 불러오기에 문제가 발생했습니다", 0, "에러 발생");

            }

        }

    }

    /**
     * 유모차업체 정보수정하기
     *
     * @author DEN
     */
    private function update_stroller()
    {
        $stroller_idx  = (int)base64_decode($this->input->get('csIdx', TRUE));
        $stroller_info = $this->input->post('strollerInfo', TRUE);
        $is_deleted    = (int)$this->input->post('isDelete', TRUE);

        if ($stroller_idx !== -1)
        {
            if ($is_deleted === 1)
            {
                $delete_bind_param = [
                    'boardCode'   => 'stroller',
                    'contentCode' => 'stlg_'.$stroller_idx,
                    'fileUpType'  => 'stroller_logo',
                ];

                $this->stroller_info_model->delete_logo_information($stroller_idx);
                $this->admin_upload_file_model->delete_file($delete_bind_param);

            }

            $updated_obj = $this->stroller_info_model->update_stroller_information($stroller_idx, $stroller_info);

            if ($updated_obj->result === 1)
            {
                if ($updated_obj->matchingChange)
                {
                    echo json_encode($this->stroller_info_model->matching_stroller($stroller_idx, $stroller_info['matchingAffiliate']));
                }
                else
                    echo json_encode($updated_obj);

            }
            else
            {
                log_message("error", "(StrollerInformation) 유모차정보를 업데이트하는데 오류가 발생했습니다. | $this->login_id | ".json_encode($updated_obj)." | ".$stroller_idx);
                show_error("유모차정보를 업데이트하는데 오류가 발생했습니다.", 0, "에러 발생");

            }
        }
        else
        {
            log_message("error", "(StrollerInformation) 유모차정보를 업데이트하는데 오류가 발생했습니다. | $this->login_id | ".$stroller_idx);
            show_error("유모차정보를 업데이트하는데 오류가 발생했습니다.", 0, "에러 발생");

        }

    }

    /**
     * 유모차업체 삭제하기
     *
     * @author DEN
     */
    private function delete_stroller()
    {
        $stroller_idx  = (int)base64_decode($this->input->get('csIdx', TRUE));
        $this->stroller_info_model->delete_stroller_information($stroller_idx);

        echo json_encode(['result' => 1]);
    }

}