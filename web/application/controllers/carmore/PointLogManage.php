<?php
/**
 * PointLogManage.php - 포인트 내역 관리  포인트 사용,지급, 회수정보 컨트롤
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class PointLogManage extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/PointManage_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);

        switch($ptype) {
            case "w" : $this->loadPointLogWrite(); break;
            default : $this->loadPointLogList(); break;
        }
    }

    // 카모아 포인트 사용,관리 내역 리스트
    public function loadPointLogList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        $ptype = $this->input->get('ptype', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);

        if(!$sp) $sp="";
        if(!$sv) $sv="";


        $rowresult = $this->PointManage_model->getPointlogPageList($page, 20, $sp, $sv);

        $total = $this->PointManage_model->getPointlogCount($sp, $sv);

        $startnum = $total-($page-1) * 20;

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/carmore/PointLogManage";
        $config['suffix']     ="&ptype=u&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];
        foreach($rowresult as $entry)
        {

            $arr_datalist["pointsrl"]=$entry->pointsrl;
            $arr_datalist["admin_name"] = $this->PointManage_model->getAdminname($entry->pointsrl);

            $arr_datalist["usrserial"]=$entry->usrserial;
            $arr_datalist["valpointstr"]= number_format( $entry->valpoint,0);
            $arr_datalist["valpoint"]= $entry->valpoint;
            $arr_datalist["pointtype"]=$entry->pointtype;
            $arr_datalist["destroydate"]=$entry->destroydate;
            $arr_datalist["pointmsg"]=$entry->pointmsg;
            $arr_datalist["logmsg"]=$entry->logmsg;
            $arr_datalist["f_reservationidx"]=$entry->f_reservationidx;
            $arr_datalist["kakaonickname"]=$entry->kakaonickname;
            $arr_datalist["usrname"]=$entry->usrname;

            if( $entry->pointtype =="2"){
                $arr_datalist["pointtypestr"]="사용";
                $arr_datalist["pointtypecolor"]="red";
            }else if( $entry->pointtype=="4"){
                $arr_datalist["pointtypestr"]="지급";
                $arr_datalist["pointtypecolor"]="";
            }else if( $entry->pointtype =="10"){
                $arr_datalist["pointtypestr"]="탈퇴회수";
                $arr_datalist["pointtypecolor"]="red";
            }else if( $entry->pointtype =="11"){
                $arr_datalist["pointtypestr"]="운영자회수";
                $arr_datalist["pointtypecolor"]="red";
            }

            $regdate =$entry->regdate;
            $arr_datalist["regdatev"] =$regdate;
            $arr_datalist["regdate"] =$this->customfunc->get_dateformat($regdate);


            $arr_alldatalist[]=$arr_datalist;
        }

        $data["list"]=$arr_alldatalist;
        $data["per_page"]=$per_page;
        $data["startnum"]=$startnum;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->LOGINID;

        if($ptype=="m"){

            $this->load->view('carmore/mpointloglist', array('data'=>$data));
        }else{

            $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
            $this->load->view('carmore/pointloglist', array('data'=>$data));
            $this->load->view('footer');
        }
    }


    public function loadPointLogWrite()
    {

        $pemail = $this->input->get('admin_email', TRUE);

        if($pemail !=""){
            $data["emode"]="edit";
            $arr_data = $this->Pay_model->getPayDetail($pemail);

            $data["admin_name"] =$arr_data["admin_name"];
            $data["admin_email"] =$arr_data["admin_email"];
            $data["admin_pwd"]=$arr_data["admin_pwd"];
            $data["admin_tel"] =$arr_data["admin_tel"];
            $data["team_code"] =$arr_data["team_code"];
            $data["admin_stats"] =$arr_data["admin_stats"];
            $data["member_grade"] =$arr_data["member_grade"];
            $data["config_yn"] =$arr_data["config_yn"];
            $data["offwork_yn"] =$arr_data["offwork_yn"];
            $data["buyoffer_yn"] =$arr_data["buyoffer_yn"];
            $data["dmaster_yn"] =$arr_data["master_yn"];
            $data["buyofferdeposit_yn"] =$arr_data["buyofferdeposit_yn"];
            $data["upmoneyoffer_yn"] =$arr_data["upmoneyoffer_yn"];
            $data["ordermng_yn"] =$arr_data["ordermng_yn"];
            $data["calc_yn"] =$arr_data["calc_yn"];
            $data["teamoption"]= $this->customfunc->select_team($data["team_code"]);

        }else{
            $data["emode"]="new";

            $data["admin_name"] ="";
            $data["admin_email"] ="";
            $data["admin_pwd"]= "";
            $data["admin_tel"] ="";
            $data["team_code"] ="";
            $data["admin_stats"] ="";
            $data["member_grade"] ="";
            $data["config_yn"] ="";
            $data["offwork_yn"] ="";
            $data["buyoffer_yn"] ="";
            $data["dmaster_yn"] ="";
            $data["buyofferdeposit_yn"] ="";
            $data["upmoneyoffer_yn"] ="";
            $data["branch_code"] ="";
            $data["ordermng_yn"] ="";
            $data["calc_yn"] ="";

            $data["teamoption"]= $this->customfunc->select_team("");
        }

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('carmore/pointlogwrite', array('data'=>$data));
        $this->load->view('footer');
    }

}
