<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class ReorderPieStaticManage extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/StaticManage_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));
        $this->loadStaticList();

    }

    public function loadStaticList()
    {
        $this->load->helper('url'); // load the helper first

        $startdate = $this->input->get('startdate', TRUE);
        $enddate = $this->input->get('enddate', TRUE);
        $stype = $this->input->get('stype', TRUE);

        $pagetitle ="신규/재주문통계";
        $graphtitle =$pagetitle;
        if(!$stype) $stype="orderpiecal";
        if(!$startdate) $startdate=date("Y-m-d",  strtotime("-1 week"));

        if(!$enddate) $enddate=date("Y-m-d");

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));

        $rowresult = $this->StaticManage_model->getStaticData($stype,$startdate,$enddate);

        $arr_chartdata[] =array($stype,'주문비율');


        foreach($rowresult as $entry)
        {
            $arr_datalist["resultval"]= $this->customfunc->get_dateformat($entry->caldate,"datestr");
            $chartdate = $this->customfunc->get_dateformat($entry->caldate,"st");

            $newcnt = $entry->sumdaycal1;
            $totalcount= $entry->sumtotal;
            $reordercnt= $entry->sumdaycal2+$entry->sumdaycal3+$entry->sumdaycal4;
            $reorderovercnt = $totalcount-$reordercnt-$newcnt;

            $arr_chartdata[] =array("신규주문" , intval( $newcnt) );
            $arr_chartdata[] =array("2~4재주문" , $reordercnt );
            $arr_chartdata[] =array("5~ 재주문" , $reorderovercnt );

        }


        $rowresult = $this->StaticManage_model->getStaticData("ordercal",$startdate,$enddate);


        foreach($rowresult as $entry)
        {
            $arr_datalist["resultval"]= $this->customfunc->get_dateformat($entry->caldate,"datestr");
            $chartdate = $this->customfunc->get_dateformat($entry->caldate,"st");

            $newcnt = $entry->daycal1;
            $totalcount= $entry->totalcount;
            $reordercnt= $entry->daycal2+$entry->daycal3+$entry->daycal4;
            $reorderovercnt = $totalcount-$reordercnt-$newcnt;

            $arr_datalist["newcnt"]=number_format( $newcnt,0);
            $arr_datalist["totalcount"]=number_format($totalcount,0);
            $arr_datalist["reordercnt"]=number_format( $reordercnt,0);
            $arr_datalist["reorderovercnt"]=number_format( $reorderovercnt,0);

            $arr_alldatalist[]=$arr_datalist;

        }


        $data["pagetitle"]=$pagetitle;
        $data["list"]=array_reverse ($arr_alldatalist);
        $data["startdate"]=$startdate;
        $data["enddate"]=$enddate;
        $data["graphtitle"]=$graphtitle;
        $data["stype"]=$stype;
        $data["loginid"]=$this->LOGINID;
        $data["chartdata"]=json_encode($arr_chartdata);

        $today=date("Y-m-d" );
        $week1=date("Y-m-d",  strtotime("-1 week"));
        $month1=date("Y-m-d",  strtotime("-1 month"));
        $month3=date("Y-m-d",  strtotime("-3 month"));
        $month6=date("Y-m-d",  strtotime("-6 month"));
        $year1=date("Y-m-d",  strtotime("-1 year"));
        $data["today"]=$today;
        $data["week1"]=$week1;
        $data["month1"]=$month1;
        $data["month3"]=$month3;
        $data["month6"]=$month6;
        $data["year1"]=$year1;


        $this->load->view('carmore/reorderpiestaticlist', array('data'=>$data));
        $this->load->view('footer');
    }

}
