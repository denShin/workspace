<?php
/**
 * LongrentStaticManage.php - 장기렌트_검색 통계 정보 검색,리스트 컨트롤
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class LongrentStaticManage extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/StaticManage_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));
        $this->loadStaticList();

    }

    // 통계 정보 리스트, 검색 로딩
    public function loadStaticList()
    {
        $this->load->helper('url'); // load the helper first

        $startdate = $this->input->get('startdate', TRUE);
        $enddate = $this->input->get('enddate', TRUE);

        $pagetitle ="장기렌트_검색 통계";
        $stype="rentcal";

        if($startdate !="" && $enddate !=""){
            $this->session->set_userdata(array('search_stdate'=> $startdate));
            $this->session->set_userdata(array('search_eddate'=> $enddate));
        }
        if($startdate =="" && $enddate ==""){
            $startdate = $this->session->userdata('search_stdate');
            $enddate = $this->session->userdata('search_eddate');
        }

        if(!$startdate) $startdate=date("Y-m-d",  strtotime("-1 year"));
        if(!$enddate) $enddate=date("Y-m-d");


        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));

        $rowresult = $this->StaticManage_model->getStaticData($stype,$startdate,$enddate,'2');

        foreach($rowresult as $entry)
        {
            $arr_datalist["areaname"]= $entry->area_name;
            $arr_datalist["cnt"]=number_format( $entry->sumcnt,0);

            $arr_alldatalist[]=$arr_datalist;
            $arr_chartdata[] =array($entry->company , intval($entry->cnt));
        }

        $data["pagetitle"]=$pagetitle;
        $data["list"]=$arr_alldatalist;
        $data["startdate"]=$startdate;
        $data["enddate"]=$enddate;


        $today=date("Y-m-d" );
        $week1=date("Y-m-d",  strtotime("-1 week"));
        $month1=date("Y-m-d",  strtotime("-1 month"));
        $month3=date("Y-m-d",  strtotime("-3 month"));
        $month6=date("Y-m-d",  strtotime("-6 month"));
        $year1=date("Y-m-d",  strtotime("-1 year"));
        $data["today"]=$today;
        $data["week1"]=$week1;
        $data["month1"]=$month1;
        $data["month3"]=$month3;
        $data["month6"]=$month6;
        $data["year1"]=$year1;

        $data["loginid"]=$this->LOGINID;
        $data["chartdata"]=json_encode($arr_chartdata);

        $this->load->view('carmore/rentstaticlist', array('data'=>$data));
        $this->load->view('footer');
    }

}
