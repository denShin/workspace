<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 유모차 목록
 *
 * @property Workspace_common_func $workspace_common_func
 *
 * @property Stroller_model   $stroller_model
 */

class StrollerInventory extends WS_Controller {
    const PAGE_ROW = 20;

    private $login_id;
    private $head_param;

    function __construct()
    {
        parent::__construct();

        $this->login_id = (string)$this->session->userdata('admin_id');

        if ($this->login_id === "")
        {
            echo "<script>location.href='/adminmanage/Login'</script>";

        }

        $this->load->library('pagination');
        $this->load->library('Workspace_common_func');
        $this->load->model('carmore/stroller_model');

    }

    public function index()
    {
        $this->head_param = [
            'library' => ['toastr']
        ];

        $this->load_inventory();
    }

    private function load_inventory()
    {
        $this->load->helper('url'); // load the helper first

        $per_page     = $this->input->get('per_page', TRUE);
        $search_param = $this->workspace_common_func->check_false_value($this->input->get('searchParam', TRUE), TRUE);
        $search_value = $this->workspace_common_func->check_false_value($this->input->get('searchValue', TRUE), TRUE);

        if ($per_page < self::PAGE_ROW)
            $per_page = 0;

        $page = ($per_page/self::PAGE_ROW) +1;

        $total_cnt     = $this->stroller_model->get_total_stroller_count($search_param, $search_value);
        $ret_inventory = $this->stroller_model->get_stroller_inventory($page, self::PAGE_ROW, $search_param, $search_value);

        # CI >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');

        $config = $this->config->item('pagination');
        $config['total_rows']            = $total_cnt;    # 게시물 총 수
        $config['per_page']              = self::PAGE_ROW;    # 게시물 출력수
        $config['base_url']              = $config['base_url']."/carmore/strollerInventory";
        $config['suffix']                = "&searchParam=".$search_param."&searchValue=".$search_value;
        $config['use_global_url_suffix'] = false;
        $config['reuse_query_string']    = false;

        $this->pagination->initialize($config);

        $pagination = $this->pagination->create_links();

        # 리스트 받아오기

        $data['searchParam'] = $search_param;
        $data['searchValue'] = $search_value;
        $data['inventory']   = $ret_inventory;
        $data['per_page']    = $per_page;
        $data['pagination']  = $pagination;
        $data['loginid']     = $this->login_id;

        $this->load->view('head');
        $this->load->view('carmore/strollerInventory', ['data' => $data]);
        $this->load->view('footer');
    }

}