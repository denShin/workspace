<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 알림톡 전송목록
 *
 * @author DEN
 *
 * @property CI_Session $session
 * @property WS_Input   $input
 * @property CI_Config  $config
 *
 * @property Workspace_common_func $workspace_common_func
 * @property Biztalk_template      $biztalk_template
 *
 * @property Biztalk_content_model $biztalk_content_model
 */

class BiztalkContent extends WS_Controller {
    private $login_id;

    function __construct()
    {
        parent::__construct();

        $this->load->library('Workspace_common_func');
        $this->load->library('Biztalk_template');

        $this->load->model('carmore/alarm/biztalk/biztalk_content_model');

        $this->login_id = (string)$this->session->userdata('admin_id');

        if ($this->login_id === "")
        {
            echo "<script>location.href='/adminmanage/Login'</script>";

        }

    }

    function index()
    {
        $type = $this->input->get('type');

        switch ($type)
        {
            case "rt":    // 재전송
                $this->retransmission_biztalk();
                break;
            case "c":    // 취소
                $this->cancel_biztalk();
                break;
            case "gtc":    // 템플릿 코드 가져오기
                $this->load_template_code();
                break;
            case "mc":
                $this->modify_biztalk(0);
                break;
            case "mcrt":
                $this->modify_biztalk(1);
                break;
        }
    }

    private function retransmission_biztalk()
    {
        $mt_pr = (int)$this->input->post('mtPr');
        if ($mt_pr !== 0)
            $this->biztalk_content_model->retransmission_biztalk($mt_pr);

        echo json_encode(['result' => 1]);
    }

    private function cancel_biztalk()
    {
        $mt_pr = (int)$this->input->post('mtPr');
        if ($mt_pr !== 0)
            $this->biztalk_content_model->cancel_biztalk($mt_pr);

        echo json_encode(['result' => 1]);
    }

    private function load_template_code()
    {
        $code     = $this->input->get('code');
        $template = $this->workspace_common_func->check_false_value(htmlspecialchars($this->biztalk_template->get_template($code)), TRUE);

        echo json_encode([
            'result'   => 1,
            'template' => $template
        ]);

    }

    private function modify_biztalk($retransmit)
    {
        $mt_pr         = (int)$this->input->post('mtPr');
        $content       = $this->input->post('content');
        $req_date_time = $this->input->post('reqTime');
        $rec_num       = $this->input->post('recNum');

        if ($mt_pr !== 0)
            $this->biztalk_content_model->modify_biztalk($mt_pr, $content, $req_date_time, $rec_num, $retransmit);

        echo json_encode(['result' => 1]);
    }

}
