<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 알림톡 전송오류목록
 *
 * @property CI_Session $session
 * @property WS_Input   $input
 * @property CI_Config  $config
 *
 * @property Workspace_common_func $workspace_common_func
 *
 * @property Biztalk_inventory_model $biztalk_inventory_model
 */

class BiztalkErrorInventory extends WS_Controller {
    private $login_id;
    private $head_param;

    function __construct()
    {
        parent::__construct();

        $this->load->model('carmore/alarm/biztalk/biztalk_inventory_model');

        $this->login_id = (string)$this->session->userdata('admin_id');

        if ($this->login_id === "")
        {
            echo "<script>location.href='/adminmanage/Login'</script>";

        }

    }

    public function index()
    {
        $this->head_param = [
            'css'     => [],
            'library' => ['toastr']
        ];

        $this->load_inventory();

    }

    private function load_inventory()
    {
        $inventory = $this->biztalk_inventory_model->load_send_error_inventory();

        $this->load->view('head', $this->head_param);
        $this->load->view('carmore/alarm/biztalk/biztalkErrorInventory', ['data' => $inventory]);
        $this->load->view('footer');
    }
}