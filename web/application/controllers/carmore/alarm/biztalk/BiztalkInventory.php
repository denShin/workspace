<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 알림톡 전송목록
 *
 * @author DEN
 *
 * @property CI_Session $session
 * @property WS_Input   $input
 * @property CI_Config  $config
 *
 * @property Workspace_common_func $workspace_common_func
 *
 * @property Biztalk_inventory_model $biztalk_inventory_model
 */

class BiztalkInventory extends WS_Controller {
    private $login_id;
    private $head_param;
    private $moid;
    private $rec_num;

    const PAGE_ROW = 10;

    function __construct()
    {
        parent::__construct();

        $this->load->library('pagination');
        $this->load->library('Workspace_common_func');

        $this->load->model('carmore/alarm/biztalk/biztalk_inventory_model');

        $this->login_id = (string)$this->session->userdata('admin_id');

        if ($this->login_id === "")
        {
            echo "<script>location.href='/adminmanage/Login'</script>";

        }

    }

    public function index()
    {
        $this->head_param = [
            'css'     => [],
            'library' => ['toastr']
        ];

        $this->moid    = $this->workspace_common_func->check_false_value($this->input->get('moid', TRUE), TRUE);
        $this->rec_num = $this->workspace_common_func->check_false_value($this->input->get('recNum', TRUE), TRUE);

        if ($this->moid === "" && $this->rec_num === "")
            $this->view_before_search();
        else
            $this->view_search_inventory();

    }

    public function view_before_search()
    {
        $this->load->view('head', $this->head_param);
        $this->load->view('carmore/alarm/biztalk/biztalkInventory', ["data" => []]);
        $this->load->view('footer');

    }

    public function view_search_inventory()
    {
        $this->load->helper('url');
        $this->config->load('bootstrap_pagination');

        $page = $this->input->get('per_page', TRUE);

        if ($page < self::PAGE_ROW)
            $page = 0;

        $page = ($page/self::PAGE_ROW) +1;

        $total_cnt = $this->biztalk_inventory_model->load_biztalk_total_count($this->moid, $this->rec_num);
        $inventory = $this->biztalk_inventory_model->load_biztalk_inventory($page, self::PAGE_ROW, $this->moid, $this->rec_num);

        $config = $this->config->item('pagination');
        $config['total_rows']            = $total_cnt;    # 게시물 총 수
        $config['per_page']              = self::PAGE_ROW;    # 게시물 출력수
        $config['base_url']              = $config['base_url']."/carmore/alarm/biztalk/BiztalkInventory?moid=".$this->moid."&recNum=".$this->rec_num;
        $config['suffix']                = "";
        $config['use_global_url_suffix'] = false;
        $config['reuse_query_string']    = false;

        $this->pagination->initialize($config);

        $pagination = $this->pagination->create_links();

        $return_data = [
            'inventory'  => $inventory,
            'pagination' => $pagination
        ];

        $this->load->view('head', $this->head_param);
        $this->load->view('carmore/alarm/biztalk/biztalkInventory', ["data" => $return_data]);
        $this->load->view('footer');
    }

}
