<?php
/**
 * EventManage.php - 이벤트 내역 관리 리스트,등록,수정,삭제 컨트롤
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class EventManage   extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION;

    function __construct()
    {
        parent::__construct();

        $this->BOARD_CODE="event";

        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/CarmoreBoard_model');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);

        switch($ptype) {
            case "n" : $this->loadEventWorkList(); break;
            case "v" : $this->loadEventManageView(); break;
            case "w" : $this->loadEventManageWrite(); break;
            default : $this->loadEventManageList(); break;
        }
    }


    // 진행중 이벤트 관리 리스트
    public function loadEventWorkList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);

        $sp="board_stats";
        $sv="y";

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $arr_Pushdata = $this->CarmoreBoard_model->getAdminBoardPageList($this->BOARD_CODE,$page,20,$sp,$sv,'ordernum');
        $total = $this->CarmoreBoard_model->getAdminBoardCount($this->BOARD_CODE, $sp,$sv);
        $startnum = $total-($page-1) * 20;

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/carmore/EventManage";
        $config['suffix']     ="&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_allboardlist=[];
        foreach($arr_Pushdata as $entry)
        {
            $arr_boardlist["board_idx"] =$entry->board_idx;
            $arr_boardlist["content_code"] =$entry->content_code;
            $arr_boardlist["startdate"]= $entry->startdate;
            $arr_boardlist["enddate"] =$entry->enddate;
            $arr_boardlist["starttime"] =$entry->starttime;
            $arr_boardlist["endtime"] =$entry->endtime;
            $arr_boardlist["board_title"] =$entry->board_title;
            $arr_boardlist["board_code"] =$this->BOARD_CODE ;
            $arr_boardlist["board_content"] =$entry->board_content;
            $arr_boardlist["mem_id"] =$entry->mem_id;
            $arr_boardlist["reply_cnt"] =$entry->reply_cnt;
            $arr_boardlist["mem_name"] =$entry->mem_name;
            $arr_boardlist["ordernum"] =$entry->ordernum;


            $arr_boardlist["board_stats"] =$entry->board_stats;
            if($entry->board_stats =="y"){
                $arr_boardlist["board_statstr"] ="On";
            }else{
                $arr_boardlist["board_statstr"] ="<span style='color:red'>Off</span>";
            }


            $arr_boardlist["filename"] =$entry->filename;

            if($entry->filename !=""){
                $file_url = 'https://s3.ap-northeast-2.amazonaws.com/carmoreweb/'.$arr_boardlist["board_code"]."/" .$arr_boardlist["content_code"]."/" . $entry->filename;
                $arr_boardlist["fileurl"] =$file_url;
            }else{
                $arr_boardlist["fileurl"]="";
            }

            $arr_boardlist["regdate"] =$this->customfunc->get_dateformat($entry->regdate);
            $arr_boardlist["contentype_str"] =$entry->contentype_str;

            $arr_allboardlist[]=$arr_boardlist;
        }
        $data["list"]=$arr_allboardlist;
        $data["per_page"]=$per_page;
        $data["pagination"]=$pagination;
        $data["startnum"]=$startnum;
        $data["board_code"]=$this->BOARD_CODE;

        $this->load->view('carmore/workingeventlist', array('data'=>$data,'permission'=>$this->ARR_PERMISSION));
        $this->load->view('footer');
    }


    // 이벤트 관리 리스트
    public function loadEventManageList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $arr_Pushdata = $this->CarmoreBoard_model->getAdminBoardPageList($this->BOARD_CODE,$page,20,$sp,$sv);
        $total = $this->CarmoreBoard_model->getAdminBoardCount($this->BOARD_CODE, $sp,$sv);
        $startnum = $total-($page-1) * 20;

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/carmore/EventManage";
        $config['suffix']     ="&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_allboardlist=[];
        foreach($arr_Pushdata as $entry)
        {
            $arr_boardlist["board_idx"] =$entry->board_idx;
            $arr_boardlist["content_code"] =$entry->content_code;
            $arr_boardlist["startdate"]= $entry->startdate;
            $arr_boardlist["enddate"] =$entry->enddate;
            $arr_boardlist["starttime"] =$entry->starttime;
            $arr_boardlist["endtime"] =$entry->endtime;
            $arr_boardlist["board_title"] =$entry->board_title;
            $arr_boardlist["board_code"] =$this->BOARD_CODE ;
            $arr_boardlist["board_content"] =$entry->board_content;
            $arr_boardlist["mem_id"] =$entry->mem_id;
            $arr_boardlist["reply_cnt"] =$entry->reply_cnt;
            $arr_boardlist["mem_name"] =$entry->mem_name;
            $arr_boardlist["board_stats"] =$entry->board_stats;
            if($entry->board_stats =="y"){
                $arr_boardlist["board_statstr"] ="On";
            }else{
                $arr_boardlist["board_statstr"] ="<span style='color:red'>Off</span>";
            }


            $arr_boardlist["filename"] =$entry->filename;

            if($entry->filename !=""){
                $file_url = 'https://s3.ap-northeast-2.amazonaws.com/carmoreweb/'.$arr_boardlist["board_code"]."/" .$arr_boardlist["content_code"]."/" . $entry->filename;
                $arr_boardlist["fileurl"] =$file_url;
            }else{
                $arr_boardlist["fileurl"]="";
            }

            $arr_boardlist["regdate"] =$this->customfunc->get_dateformat($entry->regdate);
            $arr_boardlist["contentype_str"] =$entry->contentype_str;

            $arr_allboardlist[]=$arr_boardlist;
        }
        $data["list"]=$arr_allboardlist;
        $data["per_page"]=$per_page;
        $data["pagination"]=$pagination;
        $data["startnum"]=$startnum;
        $data["board_code"]=$this->BOARD_CODE;

        $this->load->view('carmore/eventmanagelist', array('data'=>$data,'permission'=>$this->ARR_PERMISSION));
        $this->load->view('footer');
    }

    // 이벤트 등록,수정 
    public function loadEventManageWrite()
    {
        $board_idx = $this->input->get('board_idx', TRUE);

        if($board_idx !=""){
            $data["emode"]="edit";
            $arr_data = $this->CarmoreBoard_model->getAdminBoardDetail($board_idx);

            $data["board_idx"] =$arr_data["board_idx"];
            $data["content_code"] =$arr_data["content_code"];
            $data["startdate"]= $arr_data["startdate"];
            $data["enddate"] =$arr_data["enddate"];
            $data["starttime"] =$arr_data["starttime"];
            $data["endtime"] =$arr_data["endtime"];
            $data["board_title"] =$arr_data["board_title"];
            $data["board_code"] =$arr_data["board_code"];
            $data["board_content"] =$arr_data["board_content"];
            $data["board_part"] =$arr_data["board_part"];
            $data["mem_id"] =$arr_data["mem_id"];
            $data["readcnt"] =$arr_data["readcnt"];
            $data["regdate"] =$arr_data["regdate"];
            $data["content_type"] =$arr_data["content_type"];
        }else{

            $data["emode"]="new";
            $data["board_idx"] ="";
            $data["content_code"]= $this->customfunc->get_contentcode($this->BOARD_CODE);
            $data["startdate"]= "";
            $data["enddate"] ="";
            $data["starttime"] ="";
            $data["endtime"] ="";
            $data["board_title"] ="";
            $data["board_code"] =$this->BOARD_CODE;
            $data["board_content"] ="";
            $data["board_part"] ="";
            $data["mem_id"] ="";
            $data["readcnt"] ="";
            $data["regdate"] ="";
            $data["content_type"] ="";

        }

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('carmore/eventmanagewrite', array('data'=>$data));
        $this->load->view('footer');
    }

    //이벤트 상세 내용보기
    public function loadEventManageView()
    {
        $board_idx = $this->input->get('board_idx', TRUE);

        $data["emode"]="edit";
        $arr_data = $this->CarmoreBoard_model->getAdminBoardDetail($board_idx);

        $data["board_idx"] =$arr_data["board_idx"];
        $data["content_code"] =$arr_data["content_code"];
        $data["startdate"]= $arr_data["startdate"];
        $data["enddate"] =$arr_data["enddate"];
        $data["board_title"] =$arr_data["board_title"];
        $data["board_stats"] =$arr_data["board_stats"];
        $data["board_code"] =$arr_data["board_code"];
        $data["board_content"] =$arr_data["board_content"];
        $data["mem_id"] =$arr_data["mem_id"];
        $data["readcnt"] =$arr_data["readcnt"];
        $data["regdate"] =$arr_data["regdate"];
        $data["per_page"] = $this->input->get('per_page', TRUE);

        $data["filelist"]=$this->CarmoreBoard_model->get_BoardFileList($data["content_code"],"");
        $data["filecnt"] = sizeof($data["filelist"]);

        $arr_PushCommentdata=$this->CarmoreBoard_model->getBoardReplyList($data["board_code"],$board_idx);
        $arr_allboardcommentlist=[];
        foreach($arr_PushCommentdata as $entry) {
            $arr_boardcomlist["board_idx"] =$entry->board_idx;
            $arr_boardcomlist["board_code"] =$entry->board_code;
            $arr_boardcomlist["mem_id"] =$entry->mem_id;
            $arr_boardcomlist["regdate"] =  $this->customfunc->get_dateformat($entry->regdate);
            $arr_boardcomlist["commenttext"] =$entry->commenttext;
            $arr_boardcomlist["comment_idx"] =$entry->comment_idx;
            $arr_boardcomlist["mem_name"] =$entry->mem_name;
            $arr_boardcomlist["wauth"] =$this->customfunc->get_writeauth($this->session->userdata('admin_id'),$arr_boardcomlist["mem_id"],$this->session->userdata('master_yn'));
            $arr_allboardcommentlist[]=$arr_boardcomlist;
        }

        $data["commentlist"] =$arr_allboardcommentlist;

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('carmore/eventmanageview', array('data'=>$data));
        $this->load->view('footer');
    }
}
