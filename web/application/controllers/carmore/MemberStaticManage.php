<?php
/**
 * MemberStaticManage.php - 기간별 회원가입(일별,월별) 통계 정보컨트롤
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class MemberStaticManage extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/StaticManage_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));
        $this->loadStaticList();

    }

    // 일별 월별 -기간별 회원가입 통계 리스트
    public function loadStaticList()
    {
        $this->load->helper('url'); // load the helper first

        $startdate = $this->input->get('startdate', TRUE);
        $enddate = $this->input->get('enddate', TRUE);
        $stype = $this->input->get('stype', TRUE);

        if($startdate !="" && $enddate !=""){
            $this->session->set_userdata(array('search_stdate'=> $startdate));
            $this->session->set_userdata(array('search_eddate'=> $enddate));
        }
        if($startdate =="" && $enddate ==""){
            $startdate = $this->session->userdata('search_stdate');
            $enddate = $this->session->userdata('search_eddate');
        }


        $pagetitle ="기간별 회원가입 통계";

        if(!$stype) $stype="membermonthcal";

        if($stype=="membermonthcal"){
            if(!$startdate) $startdate=date("Y-m-d",  strtotime("-1 year"));
            $graphtitle ="월별 가입 통계";
        }else if($stype=="memberdatecal"){
            if(!$startdate) $startdate=date("Y-m-d",  strtotime("-2 months"));
            $graphtitle ="일별 가입 통계";
        }

        if(!$enddate) $enddate=date("Y-m-d");

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $membertotal=  $this->StaticManage_model->getTotalmemcount();
        $rowresult = $this->StaticManage_model->getStaticData($stype,$startdate,$enddate);

        $arr_chartdata[] =array($stype,'가입자');


        foreach($rowresult as $entry)
        {
            if($stype=="membermonthcal"){
                $arr_datalist["resultval"] =$this->customfunc->get_dateformat($entry->monthval."01","month");
                $chartdate = $this->customfunc->get_dateformat($entry->monthval."01","months");
            }else{
                $arr_datalist["resultval"]= $this->customfunc->get_dateformat($entry->dateval,"datestr");
                $chartdate = $this->customfunc->get_dateformat($entry->dateval,"st");
            }
            $arr_datalist["totalcount"]=number_format( $entry->totalcount,0);
            $arr_alldatalist[]=$arr_datalist;
            $arr_chartdata[] =array($chartdate , intval($entry->totalcount));
        }
        $data["membertotal"]=number_format( $membertotal,0);
        $data["pagetitle"]=$pagetitle;
        $data["list"]=$arr_alldatalist;
        $data["startdate"]=$startdate;
        $data["enddate"]=$enddate;
        $data["graphtitle"]=$graphtitle;
        $data["stype"]=$stype;



        $today=date("Y-m-d" );
        $week1=date("Y-m-d",  strtotime("-1 week"));
        $month1=date("Y-m-d",  strtotime("-1 month"));
        $month3=date("Y-m-d",  strtotime("-3 month"));
        $month6=date("Y-m-d",  strtotime("-6 month"));
        $year1=date("Y-m-d",  strtotime("-1 year"));
        $data["today"]=$today;
        $data["week1"]=$week1;
        $data["month1"]=$month1;
        $data["month3"]=$month3;
        $data["month6"]=$month6;
        $data["year1"]=$year1;

        $data["loginid"]=$this->LOGINID;
        $data["chartdata"]=json_encode($arr_chartdata);


        $this->load->view('carmore/memberstaticlist', array('data'=>$data));
        $this->load->view('footer');
    }

}
