<?php
/**
 * PeakseasonManage.php -성수기 관리 리스트,등록,수정 컨트롤
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class PeakseasonManage extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/Reservation_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);

        switch($ptype) {

            case "changeshowyn" : $this->changeshowyn(); break;
            case "setpeakseason" : $this->setpeakseason(); break;
            default : $this->loadPeakseasonList(); break;
        }
    }

    // 앱 노출여부 수정,선택
    public function changeshowyn(){
        $stats = $this->input->get('stats', TRUE);
        $saveparam = $this->input->get('saveparam', TRUE);

        $data["stats"] =$stats;
        $data["saveparam"] = $saveparam;
        $data["datamode"] = "changeshowyn";

        $rowresult = $this->Reservation_model->procPeakseason($data);

    }

    // 성수기시즌 리스트
    public function loadPeakseasonList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = "title";
        $sv = $this->input->get('sv', TRUE);
        $startdate = $this->input->get('startdate', TRUE);
        $enddate = $this->input->get('enddate', TRUE);

        if(!$sp) $sp="";
        if(!$sv) $sv="";



        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));


        $rowresult = $this->Reservation_model->getPeakseasonList($page, 20, $sp, $sv );

        $total = $this->Reservation_model->getPeakseasonCount($sp, $sv);

        $startnum = $total-($page-1) * 20;

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/carmore/PeakseasonManage";
        $config['suffix']     ="&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];
        foreach($rowresult as $entry)
        {
            $arr_datalist["peakseason_idx"]=$entry->peakseason_idx;
            $arr_datalist["peakseason_year"]=$entry->peakseason_year;

            $arr_datalist["mem_id"]=$entry->mem_id;
            $arr_datalist["show_yn"]=$entry->show_yn;
            if($entry->show_yn=="y"){
                $arr_datalist["show_ynstr"]="보임";
            }else{
                $arr_datalist["show_ynstr"]="숨김";
            }

            $arr_datalist["mem_name"]=$entry->mem_name;

            $stdate = date("Y-m-d",strtotime($entry->peakseason_startdate));
            $eddate = date("Y-m-d",strtotime($entry->peakseason_enddate));
            $arr_datalist["peakseason_startdate"]=$stdate;
            $arr_datalist["peakseason_enddate"]=$eddate;
            $arr_alldatalist[]=$arr_datalist;
        }

        $data["list"]=$arr_alldatalist;
        $data["per_page"]=$per_page;
        $data["startnum"]=$startnum;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->LOGINID;

        $this->load->view('carmore/peakseasonlist', array('data'=>$data));
        $this->load->view('footer');
    }


    // 성수기 시즌 정보 등록,수정,삭제
    public function setpeakseason(){

        $peakseason_idx = $this->input->get('peakseason_idx', TRUE);
        $peakseason_startdate = $this->input->get('peakseason_startdate', TRUE);
        $peakseason_enddate = $this->input->get('peakseason_enddate', TRUE);
        $datamode = $this->input->get('datamode', TRUE);

        $data["datamode"] =$datamode;
        if($datamode=="new"){
            $data["peakseason_startdate"] =str_replace("-","",$peakseason_startdate);
            $data["peakseason_enddate"] =str_replace("-","",$peakseason_enddate);
            $data["peakseason_year"] = substr($peakseason_startdate, 0, 4);
        }else{

            $data["peakseason_idx"] = $peakseason_idx;
        }

        $data["mem_id"]=  $this->session->userdata('admin_id') ;

        $rowresult = $this->Reservation_model->procPeakseason($data);
        echo $rowresult;
    }

}
