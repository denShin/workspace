<?php
/**
 * AppUser.php - 카모아 앱 회원 관리 리스트,검색 컨트롤
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class AppUser extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/AppUser_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);

        switch($ptype) {
            default : $this->loadAppUserList(); break;
        }
    }

    // 카모아 앱 회원 리스트
    public function loadAppUserList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);
        $mem_stats = $this->input->get('mem_stats', TRUE);
        if(!$mem_stats) $mem_stats="y";
        if($mem_stats =="y"){
            $listitle="앱 회원관리";
        }else if($mem_stats =="x"){
            $listitle="탈퇴 회원관리";
        }else if($mem_stats =="n"){
            $listitle="차단 회원관리";
        }

        if(!$sp) $sp="";
        if(!$sv) $sv="";

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));


        $rowresult = $this->AppUser_model->getAppUserPageList($page, 20, $sp, $sv);
        $total = $this->AppUser_model->getAppUserCount($sp, $sv);
        $startnum = $total-($page-1) * 20;

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/carmore/AppUser";
        $config['suffix']     ="&mem_stats=".$mem_stats."&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];
        foreach($rowresult as $entry)
        {

            $arr_datalist["usrserial"]=$entry->usrserial;
            $arr_datalist["email"]=$entry->email;
            $arr_datalist["pwd"]=$entry->pwd;
            $arr_datalist["phone"]=$entry->phone;
            $arr_datalist["jdate"]=$entry->jdate;
            $arr_datalist["usrname"]=$entry->usrname;
            $arr_datalist["birthday"]=$entry->birthday;
            $arr_datalist["certikey"]=$entry->certikey;
            $arr_datalist["complaincnt"]=$entry->complaincnt;

            $arr_datalist["paycnt"]=$entry->paycnt;
            $arr_datalist["paysum"]=number_format($entry->paysum);
            $arr_datalist["couponcnt"]=$entry->couponcnt;
            $arr_datalist["cancelcnt"]=$entry->cancelcnt;
            $arr_datalist["cancelsum"]=number_format($entry->cancelsum);

            $arr_datalist["usrpoint"]=$entry->usrpoint;
            $arr_datalist["usrpoint"] = number_format($arr_datalist["usrpoint"],0);
            $arr_datalist["usrpic"]=$entry->usrpic;
            $arr_datalist["phonetype"]=$entry->phonetype;
            $arr_datalist["phoneid"]=$entry->phoneid;
            $arr_datalist["usrstatus"]=$entry->usrstatus;
            if( $entry->usrstatus =="1"){
                $arr_datalist["pushlog_typestr"]="";
                $arr_datalist["usercolor"]="";

            }else if( $entry->usrstatus =="8"){
                $arr_datalist["pushlog_typestr"]="(블락)";
                $arr_datalist["usercolor"]="blue";
            }else if( $entry->usrstatus =="9"){
                $arr_datalist["pushlog_typestr"]="(탈퇴)";
                $arr_datalist["usercolor"]="red";
            }


            $arr_datalist["tarcomplaincnt"]=$entry->tarcomplaincnt;
            if( $entry->tarcomplaincnt > 0){
                $arr_datalist["tarcomplaincntstr"]="<span style='color:red'>(컴플레인)</span>";

            }else  {
                $arr_datalist["tarcomplaincntstr"]="";
            }

            $arr_datalist["domain"]=$entry->domain;
            $arr_datalist["dealerflag"]=$entry->dealerflag;
            $arr_datalist["kakaoid"]=$entry->kakaoid;
            $arr_datalist["phonecertikey"]=$entry->phonecertikey;
            $arr_datalist["picthumbnail"]=$entry->picthumbnail;
            $arr_datalist["kakaonickname"]=$entry->kakaonickname;
            $arr_datalist["fcmstatus"]=$entry->fcmstatus;
            $arr_datalist["password_hash"]=$entry->password_hash;
            $arr_datalist["device_name"]=$entry->device_name;
            $arr_datalist["last_certificate_number"]=$entry->last_certificate_number;
            $arr_datalist["use_agree"]=$entry->use_agree;
            $arr_datalist["use_agree_check_date"]=$entry->use_agree_check_date;
            $arr_datalist["private_info_agree"]=$entry->private_info_agree;
            $arr_datalist["private_info_agree_check_date"]=$entry->private_info_agree_check_date;
            $arr_datalist["marketing_sms_agree"]=$entry->marketing_sms_agree;
            $arr_datalist["marketing_sms_agree_check_date"]=$entry->marketing_sms_agree_check_date;
            $arr_datalist["marketing_email_agree"]=$entry->marketing_email_agree;
            $arr_datalist["marketing_email_agree_check_date"]=$entry->marketing_email_agree_check_date;
            $arr_datalist["marketing_push_agree"]=$entry->marketing_push_agree;
            $arr_datalist["marketing_push_agree_check_date"]=$entry->marketing_push_agree_check_date;

            $regdate =$entry->jdate;
            $arr_datalist["regdatev"] =$regdate;
            $arr_datalist["regdate"] =$this->customfunc->get_dateformat($regdate,"regs");


            $arr_alldatalist[]=$arr_datalist;
        }

        $data["list"]=$arr_alldatalist;
        $data["mem_stats"]=$mem_stats;
        $data["per_page"]=$per_page;
        $data["startnum"]=$startnum;
        $data["listitle"]=$listitle;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->LOGINID;

        $this->load->view('carmore/appuserlist', array('data'=>$data));
        $this->load->view('footer');
    }

}
