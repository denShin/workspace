<?php
/**
 * DateGapManage.php - 예약일/대여일 차이 정보 리스트 컨트롤
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class DateGapManage extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/StaticManage_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }


    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));
        $this->loadStaticList();
    }

    // 예약일/대여일 차이 정보 리스트
    public function loadStaticList()
    {
        $this->load->helper('url'); // load the helper first

        $startdate = $this->input->get('startdate', TRUE);
        $enddate = $this->input->get('enddate', TRUE);
        $stype = "rdategap";

        $pagetitle ="예약일/대여일 차이";
        $graphtitle ="예약일/대여일 차이";
        $stypestr=" 일";

        if(!$startdate) $startdate=date("Y-m-d",  strtotime("-1 year"));
        if(!$enddate) $enddate=date("Y-m-d");

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));

        $rowresult = $this->StaticManage_model->getStaticData($stype,$startdate,$enddate);

        $arr_chartdata[] =array($stypestr,'예약수');
        foreach($rowresult as $entry)
        {
            $arr_datalist["rdategap"] = $entry->rdategap;
            $arr_datalist["cnt"]= $entry->cnt;
            $arr_alldatalist[]=$arr_datalist;
            $arr_chartdata[] =array($entry->rdategap."일 " ,(int)$entry->cnt);
        }

        $data["pagetitle"]=$pagetitle;
        $data["list"]=$arr_alldatalist;
        $data["startdate"]=$startdate;
        $data["enddate"]=$enddate;
        $data["graphtitle"]=$graphtitle;
        $data["stype"]=$stype;

        $data["loginid"]=$this->LOGINID;
        $data["chartdata"]=json_encode($arr_chartdata);


        $this->load->view('carmore/dategapinfo', array('data'=>$data));
        $this->load->view('footer');
    }

}
