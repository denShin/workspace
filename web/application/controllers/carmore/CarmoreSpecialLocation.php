<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 유모차 업체정보
 *
 * @author DEN
 *
 * @property CI_Session $session
 * @property WS_Input   $input
 *
 * @property Special_location_model $special_location_model
 */

class CarmoreSpecialLocation extends WS_Controller {
    const RESULT_SUCCESS = 1;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('carmore/special_location_model');

    }

    public function index()
    {
        $this->read_special_location();
    }

    private function read_special_location()
    {
        echo json_encode([
            'result'                   => self::RESULT_SUCCESS,
            'specialLocationInventory' => $this->special_location_model->get_special_location_inventory()
        ]);

    }
}