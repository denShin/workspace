<?php
/**
 * CarinfomasterProc.php -차종 마스터정보 이미지 퍼블리싱,매칭, 자동차정보 등록,삭제 처리 컨트롤
 */
class CarinfomasterProc   extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('carmore/Carinfomaster_model');
        $this->load->library('Customfunc');
        $this->load->library('Aws_s3');


        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        $demode =$this->input->get('emode');
        if($demode=="")  $demode =$this->input->post('emode');


        switch($demode) {

            case "imagepublish" : $this->setImagePublish(); break;
            case "match" : $this->setCarmatch(); break;
            case "edit" : $this->setCarInfo(); break;
            case "del" : $this->delCarInfo(); break;
            default : $this->setCarInfo(); break;
        }
    }

    // 선택한 이미지를 퍼블리시 세팅
    public function setImagePublish(){


        $data["filename"] =$this->input->get('filename');
        $data["publish_yn"] =$this->input->get('publish_yn');

        $return_v = $this->Carinfomaster_model->procPublishimage($data["publish_yn"] , $data["filename"] );
    }


    // 차종 정보 매칭 등록처리  
    public function setCarmatch()
    {

        $data["saveparam"] =$this->input->get('saveparam');
        $data["rentCompany_serial"] =$this->input->get('rentCompany_serial');
        $data["rentCompany_branch_serial"] =$this->input->get('rentCompany_branch_serial');

        $return_v = $this->Carinfomaster_model->procCarmatch($data);

    }

    // 자동차 마스터 정보 등록처리  
    public function setCarInfo()
    {

        $data["emode"] =$this->input->post('emode');


        $data["carinfokey"] =$this->input->post('carinfokey');
        $data["carinfomst_brand"] =$this->input->post('carinfomst_brand');
        $data["carinfomst_model"] =$this->input->post('carinfomst_model');
        $data["carinfomst_memo"] =$this->input->post('carinfomst_memo');
        $data["carinfomst_type"] =$this->input->post('carinfomst_type');
        $data["min_fuel_efficiency"] =$this->input->post('min_fuel_efficiency') ;
        $data["max_fuel_efficiency"] =$this->input->post('max_fuel_efficiency') ;

        $return_v = $this->Carinfomaster_model->procCarinfomst( $data["emode"] ,$data);

        $backpage="/carmore/Carinfomaster";

        echo "<script>location.href='$backpage'</script>";
        exit();

    }


    // 자동차 마스터 정보 삭제처리
    public function delCarInfo()
    {
        $data["emode"] =$this->input->get('emode');
        $data["carinfokey"] =$this->input->get('carinfokey');

        $return_v = $this->Carinfomaster_model->procCarinfomst( $data["emode"] ,$data);

        //s3 객체 삭제
        $this->aws_s3->del_S3Folder("carmoreweb","carmst/".$data["carinfokey"]);

        $backpage="/carmore/Carinfomaster";
        echo "<script>location.href='$backpage'</script>";
        exit();
    }


}
