<?php
/**
 * Dashboard.php - 운영툴 전체 대시 보드 메인 
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library('Customfunc');
        $this->load->model('carmore/DashBoard_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }

    }

    public function index()
    {
        $this->loadDashboard();
    }

    // 대시보드 메인 정보 로드
    public function loadDashboard()
    {

        $thisyear = date("Y");
        $thismonth =date("m");
        $thisquarter = ceil(date('n') / 3);


        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $today = date("Ymd");
        $rowresult = $this->DashBoard_model->getDashBoardData($today);

        // 상단 데이터
        $data["today"]= date("Y.m.d");
        

        $data["yeartotalprice"]= number_format( $rowresult["yeartotalprice"],0);
        $data["yeartotalcount"]=number_format($rowresult["yeartotalcount"],0);
        $data["yearcancelcount"]=number_format($rowresult["yearcancelcount"],0);
        $data["yearcancelprice"]=number_format($rowresult["yearcancelprice"],0);

        $data["monthtotalprice"]= number_format( $rowresult["monthtotalprice"],0);
        $data["monthtotalcount"]=number_format($rowresult["monthtotalcount"],0);
        $data["monthcancelcount"]=number_format($rowresult["monthcancelcount"],0);
        $data["monthcancelprice"]=number_format($rowresult["monthcancelprice"],0);

        $data["qtotalprice"]= number_format( $rowresult["qtotalprice"],0);
        $data["qtotalcount"]=number_format($rowresult["qtotalcount"],0);
        $data["qcancelcount"]=number_format($rowresult["qcancelcount"],0);
        $data["qcancelprice"]=number_format($rowresult["qcancelprice"],0);
        
        
        
        
        $data["todaytotalprice"]= number_format( $rowresult["todaytotalprice"],0);
        $data["todaytotalcount"]=number_format($rowresult["todaytotalcount"],0);
        $data["todaycancelcount"]=number_format($rowresult["todaycancelcount"],0);
        $data["todaycancelprice"]=number_format($rowresult["todaycancelprice"],0);
        $data["todaymembercnt"]=number_format($rowresult["todaymembercnt"],0);


        //차트데이터
        $peroidsale_result =$rowresult["peroidsale_result"];
        $arr_chartdata[] =array('datecal','매출액','1주일');
        foreach($peroidsale_result as $entry)
        {

            $arr_datalist["resultval"]= $this->customfunc->get_dateformat($entry->dateval,"datestr");
            $arr_datalist["totalprice"]=number_format( $entry->totalprice,0);
            $chartdate = $this->customfunc->get_dateformat($entry->dateval,"st");


            $weekavgval = $this->DashBoard_model->weekavg($entry->dateval);

            $arr_alldatalist[]=$arr_datalist;
            $arr_chartdata[] =array($chartdate , intval($entry->totalprice), intval($weekavgval));
        }

        $data["chartdata"]=json_encode($arr_chartdata);


        //고객 상담 데이터
        $complain_result=$rowresult["complain_result"];
        $arr_allcomplainlist=[];
        foreach($complain_result as $entry)
        {
            $arr_boardlist["board_idx"] =$entry->board_idx;
            $arr_boardlist["content_code"] =$entry->content_code;
            $arr_boardlist["board_title"] =$entry->board_title;
            $arr_boardlist["board_code"] =$this->BOARD_CODE ;
            $arr_boardlist["board_content"] =$entry->board_content;
            $arr_boardlist["mem_id"] =$entry->mem_id;
            $arr_boardlist["reply_cnt"] =$entry->reply_cnt;
            $arr_boardlist["mem_name"] =$entry->mem_name;

            $arr_boardlist["usrserial"] =$entry->usrserial;
            $arr_boardlist["board_part"] =$entry->board_part;
            $arr_boardlist["kakaonickname"] =$entry->kakaonickname;
            if( $entry->kakaonickname ==""){
                $arr_boardlist["kakaonickname"]="비회원";
            }
            $arr_boardlist["board_partstr"]  =$this->customfunc->get_complaincodestr($entry->board_part);
            $arr_boardlist["regdate"] =$this->customfunc->get_dateformat($entry->regdate);

            $arr_allcomplainlist[]=$arr_boardlist;
        }
        $data["complainlist"]=$arr_allcomplainlist;


        //주문  데이터
        $order_result=$rowresult["order_result"];
        $arr_allorderlist=[];
        foreach($order_result as $entry)
        {
            $arr_datalist["f_reservationidx"]=$entry->f_reservationidx;
            $arr_datalist["f_bookingid"]=$entry->f_bookingid;

            if($entry->reservation_user_status =="0"){
                $reservation_user_status ="비회원";
            }else if($entry->reservation_user_status =="1"){
                $reservation_user_status ="회원";
            }

            $arr_datalist["reservation_user_status"]=$reservation_user_status;
            $arr_datalist["f_usrserial"]=$entry->f_usrserial;
            $arr_datalist["f_loginkey"]=$entry->f_loginkey;
            $arr_datalist["f_phonetype"]=$entry->f_phonetype;
            $arr_datalist["f_phoneid"]=$entry->f_phoneid;
            $f_rentstartdate = $this->customfunc->get_dateformat($entry->f_rentstartdate,"ns");
            $f_rentenddate = $this->customfunc->get_dateformat($entry->f_rentenddate,"ns");

            $arr_datalist["f_rentstartdate"]=$f_rentstartdate;
            $arr_datalist["f_rentenddate"]=$f_rentenddate;
            $arr_datalist["company_serial"]=$entry->company_serial;
            $arr_datalist["f_companycode"]=$entry->f_companycode;
            $arr_datalist["f_carserial"]=$entry->f_carserial;
            $arr_datalist["f_delivpickaddr"]=$entry->f_delivpickaddr;
            $arr_datalist["f_delivreturnaddr"]=$entry->f_delivreturnaddr;
            $arr_datalist["pickup_new_address"]=$entry->pickup_new_address;
            $arr_datalist["pickup_latitude"]=$entry->pickup_latitude;
            $arr_datalist["pickup_longitude"]=$entry->pickup_longitude;
            $arr_datalist["return_new_address"]=$entry->return_new_address;
            $arr_datalist["return_latitude"]=$entry->return_latitude;
            $arr_datalist["return_longitude"]=$entry->return_longitude;
            $arr_datalist["original_price"]=$entry->original_price;
            $arr_datalist["f_totalprice"]=$entry->f_totalprice;
            $arr_datalist["f_rentprice"]=$entry->f_rentprice;
            $arr_datalist["f_insuserial"]=$entry->f_insuserial;
            $arr_datalist["f_insuprice"]=$entry->f_insuprice;
            $arr_datalist["f_delivprice"]=$entry->f_delivprice;
            $arr_datalist["f_couponserial"]=$entry->f_couponserial;
            $arr_datalist["f_usepoint"]=$entry->f_usepoint;

            if($entry->f_rentstatus =="0"){
                $f_rentstatus ="출고전";
            }else if($entry->f_rentstatus =="1"){
                $f_rentstatus ="대여중";
            }else if($entry->f_rentstatus =="2"){
                $f_rentstatus ="반납";
            }

            $arr_datalist["f_rentstatus"]=$f_rentstatus;

            if($entry->f_paystatus =="0"){
                $f_paystatus ="검색";
            }else if($entry->f_paystatus =="1"){
                $f_paystatus ="결제완료";
            }else if($entry->f_paystatus =="2"){
                $f_paystatus ="취소";
            }

            $arr_datalist["f_paystatus"]=$f_paystatus;

            $driver_name_encrypt =$this->customfunc->decrypt($entry->driver_name_encrypt);
            $arr_datalist["driver_name_encrypt"]=$driver_name_encrypt;
            $driver_phone_encrypt =$this->customfunc->decrypt($entry->driver_phone_encrypt);
            $driver_phone_encrypt =$this->customfunc->add_phonehyphen($driver_phone_encrypt);
            $arr_datalist["driver_phone_encrypt"]=$driver_phone_encrypt;
            $driver_birthday_encrypt =$this->customfunc->decrypt($entry->driver_birthday_encrypt);
            $arr_datalist["driver_birthday_encrypt"]=$driver_birthday_encrypt;

            $arr_datalist["driver_license_type"]=$entry->driver_license_type;
            $arr_datalist["driver_license"]=$entry->driver_license;
            $arr_datalist["f_driveremail"]=$entry->f_driveremail;
            $arr_datalist["review"]=$entry->review;
            $arr_datalist["review_popup"]=$entry->review_popup;
            $arr_datalist["f_regdate"]=$entry->f_regdate;
            $arr_datalist["user_type"]=$entry->user_type;
            $arr_datalist["f_usecarnumber"]=$entry->f_usecarnumber;
            $arr_datalist["f_drivername"]=$entry->f_drivername;
            $arr_datalist["f_driverphone"]=$entry->f_driverphone;
            $arr_datalist["f_driverbirthday"]=$entry->f_driverbirthday;
            $arr_datalist["cdw_number"]=$entry->cdw_number;
            $arr_datalist["f_reviewflag"]=$entry->f_reviewflag;
            $arr_datalist["f_usecarserial"]=$entry->f_usecarserial;
            $arr_datalist["brand"]=$entry->brand;
            $arr_datalist["carType"]=$entry->carType;
            $arr_datalist["model"]=$entry->model;
            $arr_datalist["car_number1"]=$entry->car_number1;
            $arr_datalist["car_number2"]=$entry->car_number2;
            $arr_datalist["car_number3"]=$entry->car_number3;

            $arr_datalist["age_year"]=$entry->age_year;
            $arr_datalist["age_month"]=$entry->age_month;
            $arr_datalist["age_info"]=$entry->age_year."/".$entry->age_month;

            $arr_datalist["oil"]=$entry->oil;
            $arr_datalist["color"]=$entry->color;
            $arr_datalist["name"]=$entry->name;
            $arr_datalist["branchName"]=$entry->branchName;
            $arr_datalist["model"]=$entry->model;
            $arr_datalist["brancharea"]=$entry->brancharea;

            $arr_datalist["coupon_title"]=$entry->coupon_title;
            $arr_datalist["coupon_price"]=number_format( $entry->coupon_price,0);
            $arr_datalist["original_price"]=number_format( $entry->original_price,0);
            $arr_datalist["f_totalprice"]= number_format( $entry->f_totalprice,0);
            $arr_datalist["f_rentprice"]=number_format( $entry->f_rentprice,0);
            $arr_datalist["f_insuprice"]=number_format( $entry->f_insuprice,0);
            $arr_datalist["f_delivprice"]=number_format( $entry->f_delivprice,0);
            $arr_datalist["f_usepoint"]=number_format( $entry->f_usepoint,0);
            $regdate =$entry->f_regdate;
            $arr_datalist["regdatev"] =$regdate;
            $arr_datalist["regdate"] =$this->customfunc->get_dateformat($regdate,"ns");


            $arr_allorderlist[]=$arr_datalist;
        }

        $data["orderlist"]=$arr_allorderlist;

        $this->load->view('/carmore/dashboard', array('data'=>$data));
        $this->load->view('footer');
    }

}
