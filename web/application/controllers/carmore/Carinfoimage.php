<?php
/**
 * Carinfoimage.php - 자동차 이미지 관리 컨트롤
 */

class Carinfoimage extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION;

    function __construct()
    {
        parent::__construct();


        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/Carinfomaster_model');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);


        switch($ptype) {
            case "sub" : $this->loadSubimage(); break;
            case "main" : $this->loadMainimage(); break;
        }

    }

    // 메인이미지 리스트 컨트롤
    public function loadMainimage()
    {
        $this->load->helper('url'); // load the helper first
        $carinfokey = $this->input->get('carinfokey', TRUE);

        $arr_data = $this->Carinfomaster_model->getCarinfoImage('main',$carinfokey);

        foreach($arr_data as $entry) {

            $data["content_code"] = $entry["content_code"];
            $data["fileSaveName"] = $entry["fileSaveName"];
            $data["publish_yn"] =$entry["publish_yn"];

            if($data["publish_yn"]=="y"){
                $data["publish_ynstr"]="배포완료";
            }else{
                $data["publish_ynstr"]="<span style='color:red'>배포대기</span>";
            }
        }

        if(strlen($data["fileSaveName"]) < 3 ){
            $fullurl="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/carmst/default-no-car-pic.png";
        }else{
            $fullurl="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/carmst/".$carinfokey."/".$data["fileSaveName"];
        }

        $data["mainimage"]=$fullurl;
        $data["carinfokey"]=$carinfokey;
        $data["content_code"]=$carinfokey;
        $data["fileuptype"]="main";

        $data["pagetitle"]="차종 메인 이미지";

        $this->load->view('carmore/carmainimage', array('data'=>$data));

    }

    // 서브 이미지 리스트 컨트롤
    public function loadSubimage()
    {
        $this->load->helper('url'); // load the helper first
        $carinfokey = $this->input->get('carinfokey', TRUE);

        $arr_data = $this->Carinfomaster_model->getCarinfoImage('sub',$carinfokey);


        $arr_alllist=[];
        foreach($arr_data as $entry)
        {
            $arr_imageinfo["content_code"] =$entry["content_code"];
            $arr_imageinfo["fileSaveName"] =$entry["fileSaveName"];
            $arr_imageinfo["publish_yn"] =$entry["publish_yn"];


            if($arr_imageinfo["publish_yn"]=="y"){
                $arr_imageinfo["publish_ynstr"]="배포완료";
            }else{
                $arr_imageinfo["publish_ynstr"]="배포대기";
            }
            if(strlen($entry->fileSaveName) < 3 ){
                $fullurl="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/carmst/default-no-car-pic.png";
            }else{
                $fullurl="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/carmst/".$carinfokey."/".$entry->fileSaveName;
            }
            $arr_imageinfo["fullurl"] =$fullurl;

            $arr_alllist[]=$arr_imageinfo;
        }
        $data["list"]=$arr_alllist;
        $data["carinfokey"]=$carinfokey;
        $data["content_code"]=$carinfokey;
        $data["fileuptype"]="main";

        $data["pagetitle"]="차종 메인 이미지";

        $this->load->view('carmore/carsubimage', array('data'=>$data));

    }

}