<?php
/**
 * PayManage.php -결제 내역 관리 리스트,취소 컨트롤
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class PayManage extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/Pay_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);
        $emode = $this->input->post('emode', TRUE);

        if($emode=="newpay"){
            $this->procPay();
        }
        else{
            switch($ptype) {
                case "w" : $this->loadPayWrite(); break;
                default : $this->loadPayList(); break;
            }
        }

    }

    // 신규 결제 내역 등록하기
    public function procPay(){
        $data["f_logstatus"] =$this->input->post('f_logstatus');
        $data["f_resultcode"] =$this->input->post('f_resultcode');
        $data["f_usrserial"] =$this->input->post('f_usrserial');
        $data["f_reservationidx"] =$this->input->post('f_reservationidx');
        $data["f_bookingid"] =$this->input->post('f_bookingid');
        $data["f_Amt"] =$this->input->post('f_Amt');

        $return_v = $this->Pay_model->procPay( $data);

        if($return_v=="e"){
            echo "<script>alert('오류가 발생하였습니다.');history.back();</script>";
            exit();
        }else if($return_v=="y"){
            echo "<script>location.href='/carmore/PayManage'</script>";
            exit();
        }
    }


    // 결제 내역 리스트 
    public function loadPayList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);

        if(!$sp) $sp="";
        if(!$sv) $sv="";

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));


        $rowresult = $this->Pay_model->getPaylogPageList($page, 20, $sp, $sv);
        $total = $this->Pay_model->getPaylogCount($sp, $sv);

        $startnum = $total-($page-1) * 20;

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/carmore/PayManage";
        $config['suffix']     ="&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];
        foreach($rowresult as $entry)
        {

            $arr_datalist["f_drivername"]=$entry->f_drivername;
            $arr_datalist["kakaonickname"]=$entry->kakaonickname;
            $arr_datalist["f_paylogidx"]=$entry->f_paylogidx;
            $arr_datalist["f_reservationidx"]=$entry->f_reservationidx;
            $arr_datalist["f_bookingid"]=$entry->f_bookingid;
            $arr_datalist["f_usrserial"]=$entry->f_usrserial;
            $arr_datalist["f_logdate"]=$entry->f_logdate;
            $arr_datalist["f_logstatus"]=$entry->f_logstatus;
            $arr_datalist["f_resultcode"]=$entry->f_resultcode;
            $arr_datalist["f_resultmsg"]=$entry->f_resultmsg;
            $arr_datalist["f_AuthDate"]=$entry->f_AuthDate;
            $arr_datalist["f_AuthCode"]=$entry->f_AuthCode;
            $arr_datalist["f_BuyerName"]=$entry->f_BuyerName;
            $arr_datalist["usrname"]=$entry->usrname;
            $arr_datalist["f_MallUserID"]=$entry->f_MallUserID;
            $arr_datalist["f_GoodsName"]=$entry->f_GoodsName;
            $arr_datalist["f_MID"]=$entry->f_MID;
            $arr_datalist["f_TID"]=$entry->f_TID;
            $arr_datalist["f_Moid"]=$entry->f_Moid;
            $arr_datalist["f_Amt"]= number_format( $entry->f_Amt,0);
            $arr_datalist["f_CardQuota"]=$entry->f_CardQuota;
            $arr_datalist["f_CardCode"]=$entry->f_CardCode;
            $arr_datalist["f_CardName"]=$entry->f_CardName;
            $arr_datalist["f_CardNo"]=$entry->f_CardNo;
            $arr_datalist["f_logmsg"]=$entry->f_logmsg;

            $regdate =$entry->f_logdate;
            $arr_datalist["regdatev"] =$regdate;
            $arr_datalist["regdate"] =$this->customfunc->get_dateformat($regdate);


            $arr_alldatalist[]=$arr_datalist;
        }

        $data["list"]=$arr_alldatalist;
        $data["per_page"]=$per_page;
        $data["startnum"]=$startnum;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->LOGINID;

        $this->load->view('carmore/paymanagelist', array('data'=>$data));
        $this->load->view('footer');
    }

    public function loadPayWrite()
    {

        $data["emode"]="new";


        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('carmore/paymanagewrite', array('data'=>$data));
        $this->load->view('footer');
    }


}
