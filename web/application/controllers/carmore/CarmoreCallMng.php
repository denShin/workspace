<?php
/**
 * CarmoreBoard.php -고객 문의 상담 리스트,등록,수정,삭제 컨트롤
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class CarmoreCallMng extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION;

    function __construct()
    {
        parent::__construct();



        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/CarmoreCallBoard_model');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);
        if($ptype=="") $ptype = $this->input->post('ptype', TRUE);

        switch($ptype) {
            case "del": $this->setdelcallinfo();break;
            case "search": $this->getsearch();break;
            case "setcallinfo" : $this->setcallinfo(); break;
            case "v" : $this->loadCallMngView(); break;
            case "w" : $this->loadCallMngWrite(); break;
            default : $this->loadCallMngList(); break;
        }
    }

    // 기존 예약내역,상담내역 검색 json 가져오기
    public function getsearch(){
        $sv = $this->customfunc->SQL_Injection($this->input->get("sv"));


        // get device detail
        $returnarr = $this->CarmoreCallBoard_model->getsearchList($sv);
        $reservrow =$returnarr["reservrow"];
        $callrow =$returnarr["callrow"];

        $arr_resultlist=[];
        $arr_reserv=[];
        foreach($reservrow as $entry)
        {
            $paystats =$entry["paystats"];
            $car_model =$entry["car_model"];
            $renttype =$entry["renttype"];
            $crti_start_date =$entry["crti_start_date"];
            $crti_end_date =$entry["crti_end_date"];
            $company_name =$entry["company_name"];
            $branch_name =$entry["branch_name"];

            $arr_reserv["name"] =$entry["driver_name"];
            $arr_reserv["tel"] =$entry["driver_phone"];
            $arr_reserv["date"] =$entry["f_regdate"];
            $arr_reserv["desc"] =$car_model."(".$renttype." ".$paystats.")/".$company_name." ".$branch_name.'<br>'.$crti_start_date."~".$crti_end_date;
            $arr_resultlist[]=$arr_reserv;
        }

        $arr_call=[];
        foreach($callrow as $entry)
        {
            $call_ques =$entry["call_ques"];
            $call_response=$entry["call_response"];

            $arr_call["name"] =$entry["call_name"];
            $arr_call["tel"] =$entry["call_number"];
            $arr_call["date"] =$entry["regdate"];
            $arr_call["desc"] =$call_ques."<br>-".$call_response;
            $arr_resultlist[]=$arr_call;
        }

        echo  json_encode($arr_resultlist);
    }


    //전화내역 정보를 삭제
    public function setdelcallinfo(){
        $data["call_idx"] = $this->customfunc->SQL_Injection($this->input->get("call_idx"));
        $data["emode"] = "del";
        $return_v = $this->CarmoreCallBoard_model->procCallBoard( $data);
        echo "<script>location.href='/carmore/CarmoreCallMng'</script>";
        exit();
    }

    //전화 내역 정보를 저장,수정,삭제함
    public function setcallinfo(){

        $data["call_idx"] = $this->customfunc->SQL_Injection($this->input->post("call_idx"));
        $data["mem_name"] =$this->session->userdata('admin_name');
        $data["mem_id"] = $this->session->userdata('admin_id');

        $data["emode"]  = $this->customfunc->SQL_Injection($this->input->post("emode"));
        $data["call_name"]  = $this->customfunc->SQL_Injection($this->input->post("call_name"));
        $data["call_number"]  = $this->customfunc->SQL_Injection($this->input->post("call_number"));
        $data["call_reservnum"]  = $this->customfunc->SQL_Injection($this->input->post("call_reservnum"));
        $data["call_date"] = $this->customfunc->SQL_Injection($this->input->post("call_date"));
        $data["call_time"]  = $this->customfunc->SQL_Injection($this->input->post("call_time"));
        $data["call_part"]  = $this->customfunc->SQL_Injection($this->input->post("call_part"));
        $data["callboardcate_idx"]  = $this->customfunc->SQL_Injection($this->input->post("callboardcate_idx"));
        $data["call_ques"] = $this->customfunc->SQL_Injection($this->input->post("call_ques"));
        $data["call_response"]  = $this->customfunc->SQL_Injection($this->input->post("call_response"));
        $data["call_etc"]  = $this->customfunc->SQL_Injection($this->input->post("call_etc"));

        $return_v = $this->CarmoreCallBoard_model->procCallBoard( $data);
        echo "<script>location.href='/carmore/CarmoreCallMng'</script>";
        exit();
    }


    //고객 전화내역  리스트
    public function loadCallMngList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);
        $callboardcate_idx = $this->input->get('callboardcate_idx', TRUE);
        if(!$callboardcate_idx)$callboardcate_idx="";

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $arr_CallMngdata = $this->CarmoreCallBoard_model->getCallMngPageList($page,20,$sv);
        $total = $this->CarmoreCallBoard_model->getCallMngCount( $sv);

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/carmore/CarmoreBoard";
        $config['suffix']     ="&board_code=complain&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_allboardlist=[];
        foreach($arr_CallMngdata as $entry)
        {

            $arr_boardlist["call_idx"] =$entry->call_idx;
            $arr_boardlist["mem_name"] =$entry->mem_name;
            $arr_boardlist["call_name"] =$entry->call_name;
            $arr_boardlist["call_number"] =$entry->call_number;
            $arr_boardlist["call_reservnum"] =$entry->call_reservnum;
            $arr_boardlist["call_date"] =$entry->call_date;
            $arr_boardlist["call_time"] =$entry->call_time;
            $arr_boardlist["call_part"] =$entry->call_part;
            $arr_boardlist["call_cate"] =$entry->call_cate;
            $arr_boardlist["call_ques"] =$entry->call_ques;
            $arr_boardlist["call_response"] =$entry->call_response;
            $arr_boardlist["call_etc"] =$entry->call_etc;
            $arr_boardlist["callboardcate_name"] =$entry->callboardcate_name;

            $arr_allboardlist[]=$arr_boardlist;
        }
        $data["list"]=$arr_allboardlist;
        $data["cate"]=$this->CarmoreCallBoard_model->select_callcate($callboardcate_idx);
        $data["callboardcate_idx"]=$callboardcate_idx;
        $data["per_page"]=$per_page;
        $data["board_code"]=$this->BOARD_CODE;
        $data["pagination"]=$pagination;

        $this->load->view('carmore/carmorecallboardlist', array('data'=>$data,'permission'=>$this->ARR_PERMISSION));
        $this->load->view('footer');
    }

    //고객 전화내역 등록하기
    public function loadCallMngWrite()
    {
        $call_idx = $this->input->get('call_idx', TRUE);
        date_default_timezone_set("Asia/Seoul");

        if($call_idx !=""){
            $data["emode"]="edit";
            $arr_CallMngdata = $this->CarmoreCallBoard_model->getCallMngDetail($call_idx);

            $data["call_idx"] =$arr_CallMngdata["call_idx"];
            $data["mem_name"] =$arr_CallMngdata["mem_name"];
            $data["call_name"]  =$arr_CallMngdata["call_name"];
            $data["call_number"]  =$arr_CallMngdata["call_number"];
            $data["call_reservnum"]  =$arr_CallMngdata["call_reservnum"];
            $data["call_date"] =$arr_CallMngdata["call_date"];
            $data["call_time"]  =$arr_CallMngdata["call_time"];
            $data["call_part"]  =$arr_CallMngdata["call_part"];
            $data["call_cate"]  =$arr_CallMngdata["call_cate"];
            $data["call_ques"] =$arr_CallMngdata["call_ques"];
            $data["call_response"]  =$arr_CallMngdata["call_response"];
            $data["call_etc"]  =$arr_CallMngdata["call_etc"];
            $data["callboardcate_idx"]  =$arr_CallMngdata["callboardcate_idx"];
            $callboardcate_idx=$arr_CallMngdata["callboardcate_idx"];

        }else{

            $data["emode"]="new";
            $data["call_idx"] ="";
            $data["mem_name"]  =$this->session->userdata('admin_name');
            $data["call_name"]  ="";
            $data["call_number"] ="";
            $data["call_reservnum"]  ="";
            $data["call_date"]  =date("Y-m-d");
            $data["call_time"]  =date("H:i");
            $data["call_part"]  ="";
            $data["call_cate"]   ="";
            $data["call_ques"]  ="";
            $data["call_response"]   ="";
            $data["call_etc"]   ="";
            $callboardcate_idx="";


        }
        $data["cate"]=$this->CarmoreCallBoard_model->select_callcate($callboardcate_idx);

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('carmore/carmorecallboardwrite', array('data'=>$data));
        $this->load->view('footer');
    }



}
