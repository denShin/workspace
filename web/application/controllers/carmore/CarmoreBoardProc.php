<?php
/**
 * CarmoreBoardProc.php - 고객 게시판 코멘트 등록, 상태변경, 고객문의상담 처리
 */
class CarmoreBoardProc  extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('carmore/CarmoreBoard_model');
        $this->load->library('Customfunc');
        $this->load->library('Aws_s3');


        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        $demode =$this->input->get('emode');
        if($demode=="")  $demode =$this->input->post('emode');


        switch($demode) {

            case "commentproc" : $this->commentproc(); break;
            case "changestats" : $this->setstats(); break;
            case "setallstats" : $this->setallstats(); break;
            case "setorderajax" : $this->setorderajax(); break;
            case "setcomplainajax" : $this->setcomplainajax(); break;
            case "edit" : $this->setAdminBoardInfo(); break;
            case "del" : $this->delAdminBoardInfo(); break;
            default : $this->setAdminBoardInfo(); break;
        }
    }

    // 코멘트 등록처리 
    public function commentproc(){
        $data["datamode"] =$this->input->post('datamode');
        $data["commenttext"] =$this->input->post('commenttext');
        $data["board_idx"] =$this->input->post('board_idx');
        $data["mem_id"] =  $this->session->userdata('admin_id');
        $data["comment_idx"] =$this->input->post('comment_idx');
        $data["board_code"] ="complain";

        $result= $this->CarmoreBoard_model->procAdmincomment($data);

        echo "<script>location.href='/carmore/CarmoreBoard?ptype=v&board_idx=".$data["board_idx"]."'</script>";
        exit();
    }


    // 전체 게시물 상태 변경
    public function setallstats(){
        $data["emode"] =$this->input->get('emode');
        $data["saveparam"] =$this->input->get('saveparam');

        $this->CarmoreBoard_model->procChangeallstats( $data);

        exit();

    }

    // 개별 게시물 상태 변경 
    public function setstats(){
        $data["emode"] =$this->input->post('emode');
        $data["board_idx"] =$this->input->post('board_idx');
        $data["board_stats"] =$this->input->post('board_stats');
        $data["board_code"] =$this->input->post('board_code');


        $this->CarmoreBoard_model->procChangestats( $data);

        switch($data["board_code"]) {
            case "complain" : $backpage="/carmore/CarmoreBoard?board_code=complain"; break;
            case "event" : $backpage="/carmore/EventManage"; break;
        }

        echo "<script>location.href='$backpage'</script>";
        exit();

    }

    //게시물 등록,수정처리
    public function setAdminBoardInfo()
    {

        $data["emode"] =$this->input->post('emode');

        $data["board_idx"] =$this->input->post('board_idx');
        $data["content_code"] =$this->input->post('content_code');
        $data["board_title"] =$this->input->post('board_title');
        $data["board_code"] =$this->input->post('board_code');
        $data["board_content"] =$this->input->post('board_content');
        $data["board_part"] =$this->input->post('board_part');
        $data["mem_id"] =$this->session->userdata('admin_id') ;
        $data["usrserial"] =$this->input->post('usrserial') ;

        $data["startdate"] =$this->input->post('startdate') ;
        $data["enddate"] =$this->input->post('enddate') ;
        $data["starttime"] =$this->input->post('starttime') ;
        $data["endtime"] =$this->input->post('endtime') ;


        $return_v = $this->CarmoreBoard_model->procAdminBoard( $data["emode"] ,$data);

        switch($data["board_code"]) {
            case "complain" : $backpage="/carmore/CarmoreBoard?board_code=complain"; break;
            case "event" : $backpage="/carmore/EventManage"; break;
        }

        echo "<script>location.href='$backpage'</script>";
        exit();

    }


    // 개별 게시물 순서변경 처리
    public function setorderajax(){
        $data["emode"] ="new";
        $data["order"] =$this->input->get('order');


        $this->CarmoreBoard_model->procChangeordernum( $data);

        exit();

    }


    // 개별 게시물 등록,수정
    public function setcomplainajax()
    {
        $data["emode"] ="new";
        $data["board_code"] ="complain";
        $data["content_code"] =$this->customfunc->get_contentcode( $data["board_code"] );
        $data["board_part"] =$this->input->get('board_part');

        $data["mem_id"] =$this->session->userdata('admin_id') ;
        $data["usrserial"] =$this->input->get('usrserial') ;
        $data["board_title"] =$this->input->get('board_title');
        $data["board_content"] =$this->input->get('message');
        $data["workmode"] =$this->input->get('workmode');
        $data["f_paylogidx"] =$this->input->get('f_paylogidx');
        $data["complain_yn"] =$this->input->get('complain_yn');
        $data["pointsrl"] =$this->input->get('pointsrl');
        $data["pointtype"] =$this->input->get('pointtype');
        $data["valpoint"] =$this->input->get('valpoint');

        $lastid=$this->CarmoreBoard_model->procComplainWork( $data);
        $data["lastid"] =$lastid;

        if($data["board_content"] !=""){
            $return_v = $this->CarmoreBoard_model->procAdminBoard( $data["emode"] ,$data);
        }

        exit();

    }

    // 개별 게시물 삭제
    public function delAdminBoardInfo()
    {
        $data["emode"] =$this->input->get('emode');
        $data["board_idx"] =$this->input->get('board_idx');
        $data["board_code"] =$this->input->get('board_code');
        $data["content_code"] =$this->input->get('content_code');

        $return_v = $this->CarmoreBoard_model->procAdminBoard( $data["emode"] ,$data);

        //s3 객체 삭제
        $this->aws_s3->del_S3Folder("carmoreweb",$data["board_code"]."/".$data["content_code"]);

        switch($data["board_code"]) {
            case "complain" : $backpage="/carmore/CarmoreBoard?board_code=complain"; break;
            case "event" : $backpage="/carmore/EventManage"; break;
        }
        echo "<script>location.href='$backpage'</script>";
        exit();
    }



}
