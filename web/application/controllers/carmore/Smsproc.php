<?php
/**
 * Smsproc.php - SMS 등록, 외부 발송처리 컨트롤 ( 사용 안함 X)
 */
include "/home/workspace/web/static/lib/smsapi/api.class.php";

class Smsproc extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library('Customfunc');


        if ($this->session->userdata('admin_id') == "") {
            echo "<script>location.href='/adminmanage/Login'</script>";
            exit();
        }
    }

    public function index()
    {
        $demode = $this->input->get('emode');
        if ($demode == "") $demode = $this->input->post('emode');


        switch ($demode) {

            case "sendsms" :
                $this->sendsms();
                break;
        }
    }

    public function sendsms(){
        $api = new gabiaSmsApi('ddakcar','d6170c487f40d9d2d9d02a1444ba7cb6');

        $phone = $this->input->get('smstel');
        $message = $this->input->get('message');


        // 발송시에 _REF_KEY_는 나중에 개별적인 발송 결과를 확인하고자 할 때 사용되는 값입니다.
        // 고객 내부의 규칙에 따른 40byte 이내의 unique한 값을 넣어주시면 됩니다.
        //$r = $api->sms_send($p, "_CALL_BACK_","_MESSAGE_","_REF_KEY_","_RESERVE_DATE_");			//단문 전송

        $r = $api->sms_send($phone, "070-4077-0505", $message, "1", "0");	//단문전송
        echo ($r);
        echo "----". gabiaSmsApi::$RESULT_OK."<BR>";
        //$r = $api->lms_send($p, $dealerPhone__, $message, "", "1", "0");			//장문 전송		//장문 전송
        // //$r = $api->mms_send($p, "_CALL_BACK_", $file_path, "_MESSAGE_", "_TITLE_", "_REF_KEY_", "_RESERVE_DATE_");		//이미지 전송
        if ($r == gabiaSmsApi::$RESULT_OK)
        {
            echo($phone . " eeee: " . $api->getResultMessage() . "<br>");
            echo("이전 : " . $api->getBefore() . "<br>");
            echo("이후 : " . $api->getAfter() . "<br>");
        }
       else {
           echo("error : " . $phone . " - " . $api->getResultCode() . " - " . $api->getResultMessage() . "<br>");
       }

        // 발송 결과 값을 알고자 하는 ref key 값 설정.
        $result = $api->get_status_by_ref("_REF_KEY_");
        echo $result;

    }

}