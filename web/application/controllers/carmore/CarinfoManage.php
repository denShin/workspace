<?php

/**
 * CarinfoManage.php - 자동차 정보 리스트,등록,수정 컨트롤 (사용안함 X )
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class CarinfoManage extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/CarinfoManage_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));
        $ptype = $this->input->get('ptype', TRUE);

        switch($ptype) {
            case "w" : $this->loadCarinfoWrite(); break;
            default : $this->loadCarinfoList(); break;
        }

    }



    public function loadCarinfoList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);
        $listitle="차량 관리";

        if(!$sp) $sp="";
        if(!$sv) $sv="";

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));


        $rowresult = $this->CarinfoManage_model->getCarinfoPageList($page, 20, $sp, $sv);
        $total = $this->CarinfoManage_model->getCarinfoCount($sp, $sv);
        $startnum = $total-($page-1) * 20;

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/carmore/CarinfoManage";
        $config['suffix']     ="&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];
        foreach($rowresult as $entry)
        {

            $arr_datalist["serial"]=$entry->serial;
            $arr_datalist["brand"]=$entry->brand;
            $arr_datalist["carType"]=$entry->carType;
            $arr_datalist["carType_flag"]=$entry->carType_flag;
            $arr_datalist["model"]=$entry->model;
            $arr_datalist["carmore_model"]=$entry->carmore_model;
            $arr_datalist["min_fuel_efficiency"]=$entry->min_fuel_efficiency;
            $arr_datalist["max_fuel_efficiency"]=$entry->max_fuel_efficiency;

            $regdate =$entry->jdate;
            $arr_datalist["regdatev"] =$regdate;
            $arr_datalist["regdate"] =$this->customfunc->get_dateformat($regdate,"regs");


            $arr_alldatalist[]=$arr_datalist;
        }

        $data["list"]=$arr_alldatalist;
        $data["per_page"]=$per_page;
        $data["startnum"]=$startnum;
        $data["listitle"]=$listitle;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->LOGINID;

        $this->load->view('carmore/carinfomanagelist', array('data'=>$data));
        $this->load->view('footer');
    }

    public function loadCarinfoWrite()
    {

        $serial = $this->input->get('serial', TRUE);

        if($serial !=""){
            $data["emode"]="edit";
            $arr_Appdata = $this->CarinfoManage_model->getCarinfoDetail($serial);
            $data["serial"] =$arr_Appdata["serial"];
            $data["brand"] =$arr_Appdata["brand"];
            $data["cartype"] =$arr_Appdata["cartype"];
            $data["cartype_flag"] =$arr_Appdata["cartype_flag"];
            $data["model"] =$arr_Appdata["model"];
            $data["carmore_model"] =$arr_Appdata["carmore_model"];
            $data["min_fuel_efficiency"] =$arr_Appdata["min_fuel_efficiency"];
            $data["max_fuel_efficiency"] =$arr_Appdata["max_fuel_efficiency"];


        }else{
            $data["emode"]="new";

        }

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('carmore/carinfomanagewrite', array('data'=>$data));
        $this->load->view('footer');
    }
}
