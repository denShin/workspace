<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CouponManage.php - 쿠폰 마스터 정보 리스트, 등록,수정
 *
 * @property Workspace_common_func $workspace_common_func
 *
 * @property CouponManage_model $CouponManage_model
 */

class CouponManage extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->library('Workspace_common_func');
        $this->load->model('carmore/CouponManage_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);

        switch($ptype) {
            case "ul" : $this->loadCouponWrite(); break;
            case "u" : $this->loadCouponUseList(); break;
            case "w" : $this->loadCouponWrite(); break;
            case "uc": $this->procUseCoupon(); break;
            default : $this->loadCouponList(); break;
        }
    }

    // 카모아 쿠폰마스터 정보 리스트
    public function loadCouponList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = "title";
        $sv = $this->input->get('sv', TRUE);
        $startdate = $this->input->get('startdate', TRUE);
        $enddate = $this->input->get('enddate', TRUE);

        if(!$sp) $sp="";
        if(!$sv) $sv="";

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));


        $rowresult = $this->CouponManage_model->getCouponMstPageList($page, 20, $sp, $sv, $startdate, $enddate);
        $total = $this->CouponManage_model->getCouponMstCount($sp, $sv, $startdate, $enddate);

        $startnum = $total-($page-1) * 20;

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/carmore/CouponManage";
        $config['suffix']     ="&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];
        foreach($rowresult as $entry)
        {
            $arr_datalist["seq"]=$entry->serial;
            $arr_datalist["coupontitle"]=$entry->title;
            $arr_datalist["TYPE"]=$entry->TYPE;
            $arr_datalist["value_type"]=$entry->value_type;

            $arr_datalist["couponprice"]= number_format( $entry->value,0);
            $arr_datalist["deadline_type"]=$entry->deadline_type;
            $arr_datalist["start_date"]=$entry->start_date;
            $arr_datalist["end_date"]=$entry->end_date;

            $arr_datalist["start_date"]=substr($arr_datalist["start_date"], 0, 10);
            $arr_datalist["end_date"]=substr($arr_datalist["end_date"], 0, 10);

            $arr_datalist["deadline_after_register"]=$entry->deadline_after_register;
            $arr_datalist["quantity"]=$entry->quantity;
            $arr_datalist["certificate_key"]=$entry->certificate_key;
            $arr_datalist["limit_pay"]=$entry->limit_pay;
            $arr_datalist["description"]=$entry->description;
            $arr_datalist["memo"]=$entry->memo;
            $arr_datalist["STATUS"]=$entry->STATUS;

            $regdate =$entry->register_date;
            $arr_datalist["regdatev"] =$regdate;
            $arr_datalist["regdate"] =$this->customfunc->get_dateformat($regdate);

            $cntarr = $this->CouponManage_model->getCouponusecnt($entry->serial);
            $arr_datalist["tcnt"]=$cntarr["tcnt"];
            $arr_datalist["notusecnt"]=$cntarr["notusecnt"];
            $arr_datalist["usecnt"]=$cntarr["usecnt"];


            $arr_alldatalist[]=$arr_datalist;
        }

        $data["list"]=$arr_alldatalist;
        $data["per_page"]=$per_page;
        $data["startnum"]=$startnum;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->LOGINID;

        $this->load->view('carmore/couponmanagelist', array('data'=>$data));
        $this->load->view('footer');
    }

    // 카모아 쿠폰마스터 정보 등록,수정
    public function loadCouponWrite()
    {

        $seq = $this->input->get('seq', TRUE);

        if($seq !=""){
            $data["emode"]="edit";
            $entry = $this->CouponManage_model->getCouponMstInfo($seq);

            $data["serial"]=$entry["serial"];
            $data["title"]=$entry["title"];
            $data["type"]=$entry["type"];
            $data["type2"]=$entry["type2"];
            $data["value_type"]=$entry["value_type"];
            $data["value"]=$entry["value"];
            $data["deadline_type"]=$entry["deadline_type"];
            $data["start_date"]=substr($entry["start_date"], 0, 10);
            $data["end_date"]=substr($entry["end_date"], 0, 10);
            $data["deadline_after_register"]=$entry["deadline_after_register"];

            $data["quantity"]=$entry["quantity"];
            if($entry["quantity"]=="-1"){
                $data["quantity"]="";
            }
            $data["certificate_key"]=$entry["certificate_key"];
            $data["limit_pay"]=$entry["limit_pay"];
            $data["description"]=$entry["description"];
            $data["memo"]=$entry["memo"];
            $data["register_date"]=$entry["register_date"];
            $data["status"]=$entry["status"];
            $data["target_company_coupon"]=$entry["target_company_coupon"];
            $data["target_com_idx"]=$entry["target_com_idx"];
            $data["target_bra_idx"]=$entry["target_bra_idx"];
            $target_bra_idx=$entry["target_bra_idx"];
            $data["target_com_first_location"]=$entry["target_com_first_location"];
            $data["target_location_coupon"]=$entry["target_location_coupon"];
            $data["target_location_str"]=$entry["target_location_str"];
            $data["target_reserv_term_coupon"]=$entry["target_reserv_term_coupon"];

            $data["target_reserv_term_start"]=substr($entry["target_reserv_term_start"], 0, 10);
            $data["target_reserv_term_end"]=substr($entry["target_reserv_term_end"], 0, 10);
            $data["target_reserv_period_coupon"]=$entry["target_reserv_period_coupon"];
            $data["target_reserv_minimum_period"]=$entry["target_reserv_minimum_period"];
            $data["target_rent_type_coupon"]=$entry["target_rent_type_coupon"];
            $data["target_rent_type"]=$entry["target_rent_type"];
            $data["target_carinfo_coupon"]=$entry["target_carinfo_coupon"];
            $data["target_carinfo_idx"]=$entry["target_carinfo_idx"];
            $target_carinfo_idx=$entry["target_carinfo_idx"];

            $city = substr($data["target_com_first_location"],0,1);
            $pcity = substr($data["target_location_str"],0,1);

            $data["companyname"]=$this->customfunc->getcompanyname($target_bra_idx);
            $data["cartitle"]=$this->customfunc->getcarname($target_carinfo_idx);

            $data["select_city"] =$this->customfunc->select_newcity($city);
            $data["pselect_city"] =$this->customfunc->select_newcity($pcity);
        }else{
            $data["emode"]="new";

        }


        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('carmore/couponmanagewrite', array('data'=>$data));
        $this->load->view('footer');
    }


    // 카모아 쿠폰 사용 리스트
    public function loadCouponUseList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);

        if(!$sp) $sp="";
        if(!$sv) $sv="";

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));


        $rowresult = $this->CouponManage_model->getCouponUsePageList($page, 20, $sp, $sv);
        $total = $this->CouponManage_model->getCouponUseCount($sp, $sv);

        $startnum = $total-($page-1) * 20;

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/carmore/CouponManage";
        $config['suffix']     ="&ptype=u&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];
        foreach($rowresult as $entry)
        {
            $arr_datalist["seq"]=$entry->SERIAL;
            $arr_datalist["coupontitle"]=$entry->title;
            $arr_datalist["TYPE"]=$entry->TYPE;
            $arr_datalist["value_type"]=$entry->value_type;

            $arr_datalist["couponprice"]= number_format( $entry->value,0);
            $arr_datalist["deadline_type"]=$entry->deadline_type;
            $arr_datalist["start_date"]=$entry->start_date;
            $arr_datalist["end_date"]=$entry->end_date;
            $arr_datalist["deadline_after_register"]=$entry->deadline_after_register;
            $arr_datalist["quantity"]=$entry->quantity;
            $arr_datalist["certificate_key"]=$entry->certificate_key;
            $arr_datalist["limit_pay"]=$entry->limit_pay;
            $arr_datalist["description"]=$entry->description;
            $arr_datalist["memo"]=$entry->memo;
            $arr_datalist["cstatus"]=$entry->cstatus;

            $arr_datalist["kakaonickname"]=$entry->kakaonickname;
            $arr_datalist["usrname"]=$entry->usrname;
            $arr_datalist["destroy_date"]=$entry->destroy_date;
            $regdate =$entry->cregdate;
            $arr_datalist["regdatev"] =$regdate;
            $arr_datalist["regdate"] =$this->customfunc->get_dateformat($regdate);


            $arr_alldatalist[]=$arr_datalist;
        }

        $data["list"]=$arr_alldatalist;
        $data["per_page"]=$per_page;
        $data["startnum"]=$startnum;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->LOGINID;

        $this->load->view('carmore/couponloglist', array('data'=>$data));
        $this->load->view('footer');
    }

    /**
     * 쿠폰 사용처리
     */
    public function procUseCoupon()
    {
        $coupon_idx = $this->workspace_common_func->check_false_value($this->input->get('couponIdx', TRUE), TRUE);

        if ($coupon_idx !== "")
        {
            $this->CouponManage_model->procCouponUse($coupon_idx);

            echo json_encode(['result' => 1]);
        }
        else
            echo json_encode(['result' => 2]);
    }

}
