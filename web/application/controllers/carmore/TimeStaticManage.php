<?php
/**
 * TimeStaticManage.php - 요일/시간별 통계 정보 검색,리스트 컨트롤
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class TimeStaticManage extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/StaticManage_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));
        $this->loadStaticList();

    }

    // 요일 , 시간별 매출 통계정보 리스팅
    public function loadStaticList()
    {
        $this->load->helper('url'); // load the helper first

        $startdate = $this->input->get('startdate', TRUE);
        $enddate = $this->input->get('enddate', TRUE);
        $stype = $this->input->get('stype', TRUE);

        if($startdate !="" && $enddate !=""){
            $this->session->set_userdata(array('search_stdate'=> $startdate));
            $this->session->set_userdata(array('search_eddate'=> $enddate));
        }
        if($startdate =="" && $enddate ==""){
            $startdate = $this->session->userdata('search_stdate');
            $enddate = $this->session->userdata('search_eddate');
        }

        $pagetitle ="요일/시간별 통계";

        if(!$stype) $stype="yoilcal";

        if($stype=="yoilcal"){
            $graphtitle ="요일별 매출액 통계";
            $stypestr="요일";
            
        }else if($stype=="hourcal"){
            $graphtitle ="시간별 매출액 통계";
            $stypestr="시간대";
        }

        if(!$startdate) $startdate=date("Y-m-d",  strtotime("-1 year"));
        if(!$enddate) $enddate=date("Y-m-d");

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));

        $rowresult = $this->StaticManage_model->getStaticData($stype,$startdate,$enddate);

        $arr_chartdata[] =array($stypestr,'매출액'); 
        foreach($rowresult as $entry)
        {
            if($stype=="yoilcal"){
                $arr_datalist["resultval"] =$this->customfunc->getyoilstr($entry->week_n);
                $chartdate = $this->customfunc->getyoilstr($entry->week_n);
            }else{
                $arr_datalist["resultval"]= $entry->hourval."시";
                $chartdate =$entry->hourval."시";

            }
            $arr_datalist["totalcnt"]=number_format( $entry->totalcnt,0);
            $arr_datalist["totalprice"]=number_format( $entry->totalprice,0);
            $arr_alldatalist[]=$arr_datalist;
            $arr_chartdata[] =array($chartdate , intval($entry->totalprice));
        }

        $data["pagetitle"]=$pagetitle;
        $data["list"]=$arr_alldatalist;
        $data["startdate"]=$startdate;
        $data["enddate"]=$enddate;
        $data["graphtitle"]=$graphtitle;
        $data["stype"]=$stype;

        $data["loginid"]=$this->LOGINID;
        $data["chartdata"]=json_encode($arr_chartdata);

        $today=date("Y-m-d" );
        $week1=date("Y-m-d",  strtotime("-1 week"));
        $month1=date("Y-m-d",  strtotime("-1 month"));
        $month3=date("Y-m-d",  strtotime("-3 month"));
        $month6=date("Y-m-d",  strtotime("-6 month"));
        $year1=date("Y-m-d",  strtotime("-1 year"));
        $data["today"]=$today;
        $data["week1"]=$week1;
        $data["month1"]=$month1;
        $data["month3"]=$month3;
        $data["month6"]=$month6;
        $data["year1"]=$year1;

        $this->load->view('carmore/timestaticlist', array('data'=>$data));
        $this->load->view('footer');
    }

}
