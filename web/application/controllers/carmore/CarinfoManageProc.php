<?php
/**
 * CarinfoManageProc.php - 자동차 정보 등록처리 컨트롤 (사용안함 X )
 */
class CarinfoManageProc   extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('carmore/CarinfoManage_model');
        $this->load->library('Customfunc');
        $this->load->library('Aws_s3');


        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        $demode =$this->input->get('emode');
        if($demode=="")  $demode =$this->input->post('emode');


        switch($demode) {

            case "edit" : $this->setCarInfo(); break;
            case "del" : $this->delCarInfo(); break;
            default : $this->setCarInfo(); break;
        }
    }

    public function setCarInfo()
    {

        $data["emode"] =$this->input->post('emode');


        $data["serial"] =$this->input->post('serial');
        $data["brand"] =$this->input->post('brand');
        $data["cartype_flag"] =$this->input->post('cartype_flag');
        $data["cartype"] = $this->customfunc->get_cartypestr( $data["cartype_flag"] );

        $data["model"] =$this->input->post('model');
        $data["carmore_model"] =$this->input->post('carmore_model');
        $data["min_fuel_efficiency"] =$this->input->post('min_fuel_efficiency') ;
        $data["max_fuel_efficiency"] =$this->input->post('max_fuel_efficiency') ;

        $return_v = $this->CarinfoManage_model->procCarinfo( $data["emode"] ,$data);
        $backpage="/carmore/CarinfoManage";

        echo "<script>location.href='$backpage'</script>";
        exit();

    }



}
