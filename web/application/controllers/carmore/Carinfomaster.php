<?php
/**
 * Carinfomaster.php -차종 마스터정보 리스트,등록,수정,삭제 컨트롤
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class Carinfomaster extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/Carinfomaster_model');
        $this->load->model('carmore/CarmoreBoard_model');

        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));
        $ptype = $this->input->get('ptype', TRUE);

        $companyserial =$this->customfunc->SQL_Injection($this->input->get('companyserial'));
        $branchserial =$this->customfunc->SQL_Injection($this->input->get('branchserial'));
        $carinfomst_brand =$this->customfunc->SQL_Injection($this->input->get('carinfomst_brand'));
        $carinfomst_type =$this->customfunc->SQL_Injection($this->input->get('carinfomst_type'));

        switch($ptype) {
            case "code" : $this->getbranchrentcardata($companyserial,$branchserial); break;
            case "mstcode" : $this->getmstcardata($carinfomst_brand,$carinfomst_type); break;
            case "w" : $this->loadCarinfomstWrite(); break;
            case "v" : $this->loadCarinfomstView(); break;
            default : $this->loadCarinfomstList(); break;
        }

    }


    // 차종 마스터 정보 ajax 를 통해 받아오기 json print
    public function getmstcardata($carinfomst_brand,$carinfomst_type){

        // get device detail
        $returnarr = $this->Carinfomaster_model->getmstcardataList($carinfomst_brand,$carinfomst_type);
        echo  json_encode($returnarr);

    }


    // 지점별 렌터카정보 ajax 를 통해 받아오기 json print
    public function getbranchrentcardata($companyserial,$branchserial){

        // get device detail
        $returnarr = $this->Carinfomaster_model->getBranchrentcarList( $companyserial,$branchserial);
        echo  json_encode($returnarr);

    }

    // 자동차 마스터 정보 리스트 로딩
    public function loadCarinfomstList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);
        $listitle="차량 관리";

        if($sp=="clear"){
            $this->session->set_userdata(array('carinfomaster_sp'=> ""));
            $this->session->set_userdata(array('carinfomaster_sv'=> ""));
        }

        if($sp !="" && $sv !=""){
            $this->session->set_userdata(array('carinfomaster_sp'=> $sp));
            $this->session->set_userdata(array('carinfomaster_sv'=> $sv));
        }
        if($sp =="" && $sv ==""){
            $sp = $this->session->userdata('carinfomaster_sp');
            $sv = $this->session->userdata('carinfomaster_sv');
        }


        if(!$sp) $sp="";
        if(!$sv) $sv="";

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));


        $rowresult = $this->Carinfomaster_model->getCarinfomstPageList($page, 20, $sp, $sv);
        $total = $this->Carinfomaster_model->getCarinfomstCount($sp, $sv);
        $startnum = $total-($page-1) * 20;

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/carmore/Carinfomaster";
        $config['suffix']     ="&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];
        foreach($rowresult as $entry)
        {

            $arr_datalist["carinfo_idx"]=$entry->carinfo_idx;
            $arr_datalist["carinfokey"]=$entry->carinfokey;
            $arr_datalist["carinfomst_brand"]=$entry->carinfomst_brand;
            $arr_datalist["carinfomst_model"]=$entry->carinfomst_model;
            $arr_datalist["carinfomst_memo"]=$entry->carinfomst_memo;
            $arr_datalist["carinfomst_type"]=$entry->carinfomst_type;
            $arr_datalist["mainimage"]=$entry->mainimage;


            if(strlen($arr_datalist["mainimage"]) < 3 ){
                $fullurl="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/carmst/default-no-car-pic.png";
            }else{
                $fullurl="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/carmst/".$entry->carinfokey."/".$entry->mainimage;
            }


            $arr_datalist["subcnt"]=$entry->subcnt;
            $arr_datalist["fullurl"]=$fullurl;
            $arr_datalist["carinfomst_typestr"]=$this->customfunc->get_cartypestr($entry->carinfomst_type);
            $arr_datalist["carinfomst_titleimg"]=$entry->carinfomst_titleimg;
            $arr_datalist["min_fuel_efficiency"]=$entry->min_fuel_efficiency;
            $arr_datalist["max_fuel_efficiency"]=$entry->max_fuel_efficiency;

            $regdate =$entry->jdate;
            $arr_datalist["regdatev"] =$regdate;
            $arr_datalist["regdate"] =$this->customfunc->get_dateformat($regdate,"regs");

            $arr_datalist["filelist"]=$this->CarmoreBoard_model->get_BoardFileList($arr_datalist["carinfokey"],"");
            $arr_datalist["filecnt"] = sizeof($arr_datalist["filelist"]);

            $arr_datalist["infocheck"]="y";

            if($arr_datalist["carinfomst_brand"] ==""
                || $arr_datalist["carinfomst_model"] ==""
                || $arr_datalist["carinfomst_memo"] ==""
                || $arr_datalist["carinfomst_type"] ==""
                || $arr_datalist["mainimage"] ==""
                || $arr_datalist["filecnt"] < 1
            ){
                $arr_datalist["infocheck"]="n";
            }

            $arr_alldatalist[]=$arr_datalist;
        }

        $data["sp"]=$sp;
        $data["sv"]=$sv;
        $data["list"]=$arr_alldatalist;
        $data["per_page"]=$per_page;
        $data["startnum"]=$startnum;
        $data["listitle"]=$listitle;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->LOGINID;

        $this->load->view('carmore/Carinfomstlist', array('data'=>$data));
        $this->load->view('footer');
    }

    // 자동차 마스터 정보 등록,수정
    public function loadCarinfomstWrite()
    {

        $carinfokey = $this->input->get('carinfokey', TRUE);

        if($carinfokey !=""){
            $data["emode"]="edit";
            $arr_Appdata = $this->Carinfomaster_model->getCarinfomstDetail($carinfokey);



            $data["carinfokey"] =$arr_Appdata["carinfokey"];
            $data["carinfomst_brand"] =$arr_Appdata["carinfomst_brand"];
            $data["carinfomst_model"] =$arr_Appdata["carinfomst_model"];
            $data["carinfomst_memo"] =$arr_Appdata["carinfomst_memo"];
            $data["min_fuel_efficiency"] =$arr_Appdata["min_fuel_efficiency"];
            $data["max_fuel_efficiency"] =$arr_Appdata["max_fuel_efficiency"];
            $data["carinfomst_type"] =$arr_Appdata["carinfomst_type"];


        }else{
            $data["emode"]="new";
            $data["carinfokey"]= $this->customfunc->get_contentcode("CARMST");
        }

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('carmore/carinfomstwrite', array('data'=>$data));
        $this->load->view('footer');
    }

    // 자동차 마스터 정보 상세
    function loadCarinfomstView(){
        $carinfokey = $this->input->get('carinfokey', TRUE);

        $arr_Appdata = $this->Carinfomaster_model->getCarinfomstDetail($carinfokey);

        $data["carinfokey"] =$arr_Appdata["carinfokey"];
        $data["carinfomst_brand"] =$arr_Appdata["carinfomst_brand"];
        $data["carinfomst_model"] =$arr_Appdata["carinfomst_model"];
        $data["carinfomst_memo"] =$arr_Appdata["carinfomst_memo"];
        $data["min_fuel_efficiency"] =$arr_Appdata["min_fuel_efficiency"];
        $data["max_fuel_efficiency"] =$arr_Appdata["max_fuel_efficiency"];
        $data["carinfomst_type"] =$arr_Appdata["carinfomst_type"];
        $data["board_code"] ="carmst";

        $data["carinfomst_typestr"]=$this->customfunc->get_cartypestr($data["carinfomst_type"]);


        $data["filelist"]=$this->CarmoreBoard_model->get_BoardFileList($data["carinfokey"],"");
        $data["filecnt"] = sizeof($data["filelist"]);

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('carmore/carinfomstview', array('data'=>$data));
        $this->load->view('footer');
    }
}
