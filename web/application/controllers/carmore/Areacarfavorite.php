<?php
/**
 * Areacarfavorite.php -지역차종즐겨찾기 리스트, 매칭,검색 컨트롤
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class Areacarfavorite extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/Carinfomaster_model');
        $this->load->model('carmore/CarmoreBoard_model');

        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));
        $ptype = $this->input->get('ptype', TRUE);

        $companyserial =$this->customfunc->SQL_Injection($this->input->get('companyserial'));
        $branchserial =$this->customfunc->SQL_Injection($this->input->get('branchserial'));
        $Areafavorcar_brand =$this->customfunc->SQL_Injection($this->input->get('Areafavorcar_brand'));
        $Areafavorcar_type =$this->customfunc->SQL_Injection($this->input->get('Areafavorcar_type'));

        switch($ptype) {

            case "newtownshipcode" : $this->getnewtownshopcode(); break;
            case "townshipcode" : $this->gettownshopcode(); break;
            case "addfavor":  $this->faverproc('addfavor'); break;
            case "delfavor":  $this->faverproc('delfavor'); break;
            case "setfavororder" : $this->favororderproc(); break;
            case "w" : $this->loadAreafavorcarWrite(); break;
            case "v" : $this->loadAreafavorcarView(); break;
            default : $this->loadAreafavorcarList(); break;
        }

    }

    // 지역 코드 json-data 로딩
    public function gettownshopcode(){

        $city= $this->input->get('city', TRUE);
        // get device detail
        $returnarr = $this->Carinfomaster_model->gettownshopcode($city);
        echo  json_encode($returnarr);

    }

    // 신규지역 코드 json-data 로딩
    public function getnewtownshopcode(){

        $city= $this->input->get('city', TRUE);
        // get device detail
        $returnarr = $this->Carinfomaster_model->getnewtownshopcode($city);
        echo  json_encode($returnarr);

    }


    // 지역 차종 즐겨찾기 순서정보 저장
    public function favororderproc(){

        $data["township"] = $this->input->get('township', TRUE);
        $data["city"] = $this->input->get('city', TRUE);
        $data["order"] =$this->input->get('order', TRUE);

        $result = $this->Carinfomaster_model->procfaverorder($data);
    }


    // 지역 차종 즐겨찾기 차종정보 저장
    public function faverproc($ptype){
        $data["ptype"] = $ptype;
        $data["carinfo_idx"] = $this->input->get('carinfo_idx', TRUE);
        $data["township"] = $this->input->get('township', TRUE);
        $data["city"] = $this->input->get('city', TRUE);

        $result = $this->Carinfomaster_model->procfaver($data);
    }


    // 지역별 차종즐겨찾기 리스트 로드
    public function loadAreafavorcarList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        $township = $this->input->get('township', TRUE);
        $city = $this->input->get('city', TRUE);

        $listitle="지역차종 즐겨찾기";

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));


        $allrow = $this->Carinfomaster_model->getAreacarlist($city, $township);
        $favorlist = $this->Carinfomaster_model->getAreafavorcarlist($city, $township);

        $arr_alldatalist=[];
        $arr_allfavorlist=[];
        foreach($allrow as $entry)
        {

            $arr_datalist["carinfo_idx"]=$entry->carinfo_idx;
            $arr_datalist["carinfokey"]=$entry->carinfokey;
            $arr_datalist["carinfomst_brand"]=$entry->carinfomst_brand;
            $arr_datalist["carinfomst_model"]=$entry->carinfomst_model;
            $arr_datalist["carinfomst_memo"]=$entry->carinfomst_memo;
            $arr_datalist["carinfomst_type"]=$entry->carinfomst_type;
            $arr_datalist["mainimage"]=$entry->mainimage;


            if(strlen($arr_datalist["mainimage"]) < 3 ){
                $fullurl="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/carmst/default-no-car-pic.png";
            }else{
                $fullurl="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/carmst/".$entry->carinfokey."/".$entry->mainimage;
            }


            $arr_datalist["subcnt"]=$entry->subcnt;
            $arr_datalist["fullurl"]=$fullurl;

            $arr_alldatalist[]=$arr_datalist;
        }

        foreach($favorlist as $entry)
        {

            $arr_favordatalist["areafavor_idx"]=$entry->areafavor_idx;
            $arr_favordatalist["carinfo_idx"]=$entry->carinfo_idx;
            $arr_favordatalist["carinfokey"]=$entry->carinfokey;
            $arr_favordatalist["carinfomst_brand"]=$entry->carinfomst_brand;
            $arr_favordatalist["carinfomst_model"]=$entry->carinfomst_model;
            $arr_favordatalist["carinfomst_memo"]=$entry->carinfomst_memo;
            $arr_favordatalist["carinfomst_type"]=$entry->carinfomst_type;
            $arr_favordatalist["mainimage"]=$entry->mainimage;


            if(strlen($arr_favordatalist["mainimage"]) < 3 ){
                $fullurl="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/carmst/default-no-car-pic.png";
            }else{
                $fullurl="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/carmst/".$entry->carinfokey."/".$entry->mainimage;
            }


            $arr_favordatalist["subcnt"]=$entry->subcnt;
            $arr_favordatalist["fullurl"]=$fullurl;

            $arr_allfavorlist[]=$arr_favordatalist;
        }

        $data["city"]=$city;
        $data["township"]=$township;
        $data["select_city"] =$this->customfunc->select_city($city);
        $data["alllist"]=$arr_alldatalist;
        $data["favorlist"]=$arr_allfavorlist;
        $data["loginid"]=$this->LOGINID;

        $this->load->view('carmore/areacarfavoritelist', array('data'=>$data));
        $this->load->view('footer');
    }

    public function loadAreafavorcarWrite()
    {

        $carinfokey = $this->input->get('carinfokey', TRUE);

        if($carinfokey !=""){
            $data["emode"]="edit";
            $arr_Appdata = $this->Carinfomaster_model->getAreafavorcarDetail($carinfokey);



            $data["carinfokey"] =$arr_Appdata["carinfokey"];
            $data["Areafavorcar_brand"] =$arr_Appdata["Areafavorcar_brand"];
            $data["Areafavorcar_model"] =$arr_Appdata["Areafavorcar_model"];
            $data["Areafavorcar_memo"] =$arr_Appdata["Areafavorcar_memo"];
            $data["min_fuel_efficiency"] =$arr_Appdata["min_fuel_efficiency"];
            $data["max_fuel_efficiency"] =$arr_Appdata["max_fuel_efficiency"];
            $data["Areafavorcar_type"] =$arr_Appdata["Areafavorcar_type"];


        }else{
            $data["emode"]="new";
            $data["carinfokey"]= $this->customfunc->get_contentcode("CARMST");
        }

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('carmore/Areafavorcarwrite', array('data'=>$data));
        $this->load->view('footer');
    }

    function loadAreafavorcarView(){
        $carinfokey = $this->input->get('carinfokey', TRUE);

        $arr_Appdata = $this->Carinfomaster_model->getAreafavorcarDetail($carinfokey);

        $data["carinfokey"] =$arr_Appdata["carinfokey"];
        $data["Areafavorcar_brand"] =$arr_Appdata["Areafavorcar_brand"];
        $data["Areafavorcar_model"] =$arr_Appdata["Areafavorcar_model"];
        $data["Areafavorcar_memo"] =$arr_Appdata["Areafavorcar_memo"];
        $data["min_fuel_efficiency"] =$arr_Appdata["min_fuel_efficiency"];
        $data["max_fuel_efficiency"] =$arr_Appdata["max_fuel_efficiency"];
        $data["Areafavorcar_type"] =$arr_Appdata["Areafavorcar_type"];
        $data["board_code"] ="carmst";

        $data["Areafavorcar_typestr"]=$this->customfunc->get_cartypestr($data["Areafavorcar_type"]);


        $data["filelist"]=$this->CarmoreBoard_model->get_BoardFileList($data["carinfokey"],"");
        $data["filecnt"] = sizeof($data["filelist"]);

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('carmore/Areafavorcarview', array('data'=>$data));
        $this->load->view('footer');
    }
}
