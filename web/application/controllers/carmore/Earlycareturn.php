<?php
/**
 * Earlycareturn.php -차량 조기반납 관리 리스트 컨트롤
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Earlycareturn extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/Reservation_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {

        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);
        $startdate =$this->input->get('startdate', TRUE);
        $enddate =$this->input->get('enddate', TRUE);
        $targetprice =$this->input->get('targetprice', TRUE);
        $targetdate =$this->input->get('targetdate', TRUE);
        $usepoint =$this->input->get('usepoint', TRUE);
        $jsonyn =$this->input->get('jsonyn', TRUE);


        switch($ptype) {
            case "getcancelprice" : $this->getcalcelprice($targetprice,$startdate,$enddate, $targetdate,$usepoint,$jsonyn); break;
            case "v" : $this->loadReturnView(); break;
            case "w" : $this->loadReturnWrite(); break;
            default : $this->loadReturnList(); break;
        }
    }

    // 차량 조기반납 환불가격 가져오기 - jsondata
    public function getcalcelprice($targetprice,$startdate,$enddate, $targetdate,$usepoint,$jsonyn){

        // http://workspace2.teamo2.kr/carmore/Earlycareturn?ptype=getcancelprice&startdate=20181107&enddate=20181130&targetprice=253000&usepoint=0&targetdate=20181123&jsonyn=y


        if($startdate==""){$startdate =$this->input->get('startdate', TRUE);}
        if($enddate==""){$enddate =$this->input->get('enddate', TRUE);}



        if($targetdate==""){
            $targetdate =date("Ymd");
        }

        // $resultprice - 환불 금액
        /* 총결제금액 25만원, 포인트 결제금액이 15만원 ,카드결제 10만원
         * 환불금액이 11만원일때(사용료 14만원) = 0만원
         * 환불금액이 15만원(사용료 10만원) = 0만원
         * 환불금액이 16만원(사용료 9만원) = 1만원
         * (사용료 >= 카드결제액?) 0:카드결제액-사용료
        */

        // 포인트 사용금액을 제외한 카드결제액
        $cardpayprice = $targetprice-$usepoint;

        // 취소에 따른 할인 포인트


        $canceldiscountper = $this->customfunc->get_canceldiscountper($startdate,$enddate,$targetdate);

        // 취소시점 까지의 사용금액
        $useprice =$targetprice-$targetprice *$canceldiscountper /100;

        $refundprice=0;
        if($useprice < $cardpayprice){
            $refundprice = $cardpayprice -$useprice ;
        }else{
            $refundprice=0;
        }

        $arr_return = Array("refundprice"=>$refundprice,"useprice"=>$useprice, "cancelper"=>$canceldiscountper);

        if($jsonyn==""){

            return $arr_return;

        }else{

            echo  json_encode($arr_return);
        }
    }

    
    //차량 조기반납 신청리스트
    public function loadReturnList()
    {
        $ptype = $this->input->get('ptype', TRUE);
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);
        $sf_paystatus = $this->input->get('sf_paystatus', TRUE);


        if(!$sp) $sp="";
        if(!$sv) $sv="";
        if(!$sf_paystatus) $sf_paystatus="1";


        if($sf_paystatus =="1"){
            $listitle="/ 결제완료";
        }else if($sf_paystatus =="2"){
            $listitle="/ 취소";
        }else if($sf_paystatus =="0"){
            $listitle="/ 검색";
        }else {
            $listitle="";
        }

        // 차감율 계산필요

        $rowresult = $this->Reservation_model->getEarlycareturnList($page, 20, $sp, $sv,$sf_paystatus);
        $total = $this->Reservation_model->getEarlycareturnCount($sp, $sv,$sf_paystatus);



        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/carmore/Earlycareturn";
        $config['suffix']     ="&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];
        foreach($rowresult as $entry)
        {

            $arr_datalist["f_reservationidx"]=$entry->f_reservationidx;
            $arr_datalist["f_bookingid"]=$entry->f_bookingid;

            if($entry->reservation_user_status =="0"){
                $reservation_user_status ="비회원";
            }else if($entry->reservation_user_status =="1"){
                $reservation_user_status ="회원";
            }

            $arr_datalist["reservation_user_status"]=$reservation_user_status;
            $arr_datalist["f_usrserial"]=$entry->f_usrserial;
            $arr_datalist["f_loginkey"]=$entry->f_loginkey;
            $arr_datalist["f_phonetype"]=$entry->f_phonetype;
            $arr_datalist["f_phoneid"]=$entry->f_phoneid;
            $f_rentstartdate = $this->customfunc->get_dateformat($entry->f_rentstartdate,"ns");
            $f_rentenddate = $this->customfunc->get_dateformat($entry->f_rentenddate,"ns");

            $arr_datalist["f_rentstartdate"]=$f_rentstartdate;
            $arr_datalist["f_rentenddate"]=$f_rentenddate;
            $arr_datalist["company_serial"]=$entry->company_serial;
            $arr_datalist["f_companycode"]=$entry->f_companycode;
            $arr_datalist["f_carserial"]=$entry->f_carserial;
            $arr_datalist["f_delivpickaddr"]=$entry->f_delivpickaddr;
            $arr_datalist["f_delivreturnaddr"]=$entry->f_delivreturnaddr;
            $arr_datalist["pickup_new_address"]=$entry->pickup_new_address;
            $arr_datalist["pickup_latitude"]=$entry->pickup_latitude;
            $arr_datalist["pickup_longitude"]=$entry->pickup_longitude;
            $arr_datalist["return_new_address"]=$entry->return_new_address;
            $arr_datalist["return_latitude"]=$entry->return_latitude;
            $arr_datalist["return_longitude"]=$entry->return_longitude;
            $arr_datalist["original_price"]=$entry->original_price;
            $arr_datalist["f_totalprice"]=$entry->f_totalprice;
            $arr_datalist["f_rentprice"]=$entry->f_rentprice;
            $arr_datalist["f_insuserial"]=$entry->f_insuserial;
            $arr_datalist["f_insuprice"]=$entry->f_insuprice;
            $arr_datalist["f_delivprice"]=$entry->f_delivprice;
            $arr_datalist["f_couponserial"]=$entry->f_couponserial;
            $arr_datalist["f_usepoint"]=$entry->f_usepoint;
            $arr_datalist["branchOwner"]=$entry->branchOwner;
            $arr_datalist["branchOwnerTel"]=$entry->branchOwnerTel;
            $arr_datalist["crti_early_return_apply"]=$entry->crti_early_return_apply;
            $arr_datalist["crti_early_return_confirm_date"]=$entry->crti_early_return_confirm_date;



            $cstartdate = $this->customfunc->get_dateformat($entry->crti_start_date,"Ymd");
            $cenddate = $this->customfunc->get_dateformat($entry->crti_end_date,"Ymd");
            $canceljson = $this->getcalcelprice($entry->f_totalprice,$cstartdate,$cenddate, '',$entry->f_usepoint,'');


            $arr_datalist["cancelrefundprice"]=$canceljson["refundprice"] ;
            $arr_datalist["cancelper"]=$canceljson->cancelper ;
            $arr_datalist["canceldate"]= date("Y-m-d");


            $arr_datalist["crti_start_date"]=$cstartdate;
            $arr_datalist["crti_end_date"]=$cenddate;
            $arr_datalist["targetprice"]=$entry->f_totalprice;
            $arr_datalist["usepoint"]=$entry->f_usepoint;
            $arr_datalist["crti_idx"]=$entry->crti_idx;


            if($entry->f_rentstatus =="0"){
                $f_rentstatus ="출고전";
            }else if($entry->f_rentstatus =="1"){
                $f_rentstatus ="대여중";
            }else if($entry->f_rentstatus =="2"){
                $f_rentstatus ="반납";
            }

            $arr_datalist["f_rentstatus"]=$f_rentstatus;

            if($entry->f_paystatus =="0"){
                $f_paystatus ="검색";
            }else if($entry->f_paystatus =="1"){
                $f_paystatus ="결제완료";
            }else if($entry->f_paystatus =="2"){
                $f_paystatus ="취소";
            }

            $arr_datalist["f_paystatus"]=$f_paystatus;

            $driver_name_encrypt =$this->customfunc->decrypt($entry->driver_name_encrypt);
            $arr_datalist["driver_name_encrypt"]=$driver_name_encrypt;
            $driver_phone_encrypt =$this->customfunc->decrypt($entry->driver_phone_encrypt);
            $driver_phone_encrypt =$this->customfunc->add_phonehyphen($driver_phone_encrypt);
            $arr_datalist["driver_phone_encrypt"]=$driver_phone_encrypt;
            $driver_birthday_encrypt =$this->customfunc->decrypt($entry->driver_birthday_encrypt);
            $arr_datalist["driver_birthday_encrypt"]=$driver_birthday_encrypt;

            $arr_datalist["driver_license_type"]=$entry->driver_license_type;
            $arr_datalist["driver_license"]=$entry->driver_license;
            $arr_datalist["f_driveremail"]=$entry->f_driveremail;
            $arr_datalist["review"]=$entry->review;
            $arr_datalist["review_popup"]=$entry->review_popup;
            $arr_datalist["f_regdate"]=$entry->f_regdate;
            $arr_datalist["user_type"]=$entry->user_type;
            $arr_datalist["f_usecarnumber"]=$entry->f_usecarnumber;
            $arr_datalist["f_drivername"]=$entry->f_drivername;
            $arr_datalist["f_driverphone"]=$entry->f_driverphone;
            $arr_datalist["f_driverbirthday"]=$entry->f_driverbirthday;
            $arr_datalist["cdw_number"]=$entry->cdw_number;
            $arr_datalist["f_reviewflag"]=$entry->f_reviewflag;
            $arr_datalist["f_usecarserial"]=$entry->f_usecarserial;
            $arr_datalist["brand"]=$entry->brand;
            $arr_datalist["carType"]=$entry->carType;
            $arr_datalist["model"]=$entry->model;



            $arr_datalist["car_number1"]=$entry->car_number1;
            $arr_datalist["car_number2"]=$entry->car_number2;
            $arr_datalist["car_number3"]=$entry->car_number3;

            $arr_datalist["age_year"]=$entry->age_year;
            $arr_datalist["age_month"]=$entry->age_month;
            $arr_datalist["age_info"]=$entry->age_year."/".$entry->age_month;

            $arr_datalist["oil"]=$entry->oil;
            $arr_datalist["color"]=$entry->color;
            $arr_datalist["name"]=$entry->name;
            $arr_datalist["branchName"]=$entry->branchName;
            $arr_datalist["model"]=$entry->model;
            $arr_datalist["crti_early_return_date"]=$entry->crti_early_return_date;

            $arr_datalist["coupon_title"]=$entry->coupon_title;
            $arr_datalist["coupon_price"]=number_format( $entry->coupon_price,0);


            $arr_datalist["original_price"]=number_format( $entry->original_price,0);
            $arr_datalist["f_totalprice"]= number_format( $entry->f_totalprice,0);
            $arr_datalist["f_rentprice"]=number_format( $entry->f_rentprice,0);
            $arr_datalist["f_insuprice"]=number_format( $entry->f_insuprice,0);
            $arr_datalist["f_delivprice"]=number_format( $entry->f_delivprice,0);
            $arr_datalist["f_usepoint"]=number_format( $entry->f_usepoint,0);

            $regdate =$entry->f_regdate;
            $arr_datalist["regdatev"] =$regdate;
            $arr_datalist["regdate"] =$this->customfunc->get_dateformat($regdate,"ns");
            $arr_alldatalist[]=$arr_datalist;
        }


        $data["list"]=$arr_alldatalist;
        $data["per_page"]=$per_page;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->LOGINID;
        $data["listitle"]=$listitle;


        if($ptype =="m"){

            $this->load->view('carmore/mreservlist', array('data'=>$data));
        }else{

            $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
            $this->load->view('carmore/careturnlist', array('data'=>$data));
            $this->load->view('footer');
        }
    }

    public function loadReturnView()
    {
        $crti_idx = $this->input->get('crti_idx', TRUE);
        $data["commentlist"]=  $this->Reservation_model->getAdmincommentlist($crti_idx);
        $data["crti_idx"]=  $crti_idx;
        $data["now_id"]=  $this->session->userdata('admin_id');

        // tbl_reservation 에 f_driver 이메일

        $arr_reservdtl = $this->Reservation_model->getEarlycareturndetail($crti_idx);


        $decrypt_driver_name =$this->customfunc->decrypt($arr_reservdtl["driver_name_encrypt"] );
        $driver_phone_encrypt =$this->customfunc->decrypt($arr_reservdtl["driver_phone_encrypt"]  );
        $driver_phone_encrypt =$this->customfunc->add_phonehyphen($driver_phone_encrypt);
        $driver_birthday_encrypt =$this->customfunc->decrypt($arr_reservdtl["driver_birthday_encrypt"] );

        $get_reservation_crti_idx =$arr_reservdtl["f_reservationidx"];
        $view_start_str =$arr_reservdtl["crti_start_date"];
        $view_end_date =$arr_reservdtl["crti_end_date"];
        $f_delivpickaddr =$arr_reservdtl["f_delivpickaddr"];
        $f_delivreturnaddr =$arr_reservdtl["f_delivreturnaddr"];
        $f_driveremail =$arr_reservdtl["f_driveremail"];

        $name =$arr_reservdtl["name"];
        $brand =$arr_reservdtl["brand"];
        $car_name =$brand." ".$arr_reservdtl["model"];
        $db_cdw_name =$arr_reservdtl["db_cdw_name"];
        $view_cdw_compensation =$arr_reservdtl["view_cdw_compensation"];
        $rental_cost_format =$arr_reservdtl["crti_rent_cost"];
        $cdw_cost_format =$arr_reservdtl["crti_cdw_cost"];
        $deposit_cost_format =$arr_reservdtl["crti_deposit"];
        $discount_format =$arr_reservdtl["crti_use_point"]+$arr_reservdtl["crti_use_coupon_value"];
        $db_payment =$arr_reservdtl["crti_payment"];
        $get_cancel_amt =$arr_reservdtl["cancelamt"];
        $crti_early_return_date =$arr_reservdtl["crti_early_return_date"];
        $email_penalty =$db_payment -$get_cancel_amt;

        $data["f_delivreturnaddr"]=$f_delivreturnaddr ;
        $data["company"]=$name ;
        $data["residx"]=$get_reservation_crti_idx;
        $data["startdate"]=$view_start_str;
        $data["enddate"]=$view_end_date;
        $data["carmodel"]=$car_name;
        $data["driver"]=$decrypt_driver_name;
        $data["pickupaddr"]=$f_delivpickaddr;
        $data["return_addr"]=$f_delivreturnaddr;
        $data["origincost"]=number_format($rental_cost_format);
        $data["dbpayment"]=number_format($db_payment);
        $data["discount"]=number_format($discount_format);
        $data["cancelamt"]=number_format($get_cancel_amt);
        $data["emailpenalty"]=number_format($email_penalty);
        $data["logdate"]=$crti_early_return_date;
        $data["crti_use_point"]=$arr_reservdtl["crti_use_point"];;
        $data["part"]="returncar";
        $data["crti_idx"]=$arr_reservdtl["crti_idx"];
        $data["crti_principal"]=$arr_reservdtl["crti_principal"];
        $data["f_reservationidx"]=$arr_reservdtl["f_reservationidx"];
        $data["crti_early_return_apply"]=$arr_reservdtl["crti_early_return_apply"];


        $cstartdate = $this->customfunc->get_dateformat( $view_start_str ,"Ymd");
        $cenddate = $this->customfunc->get_dateformat($view_end_date,"Ymd");

        $canceljson = $this->getcalcelprice($data["crti_principal"],$cstartdate,$cenddate, $crti_early_return_date,$data["crti_use_point"],'');


        $data["cancelrefundprice"]=$canceljson["refundprice"] ;
        $data["emailpenalty"]=$data["crti_principal"]-$canceljson["refundprice"] ;
        $data["cancelper"]=$canceljson["cancelper"] ;
        $data["useprice"]=$canceljson["useprice"] ;
        $data["cstartdate"]=$cstartdate;
        $data["cenddate"]=$cenddate;


        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('carmore/careturnview' , array('data'=>$data));
        $this->load->view('footer');

    }

}
