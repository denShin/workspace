<?php
/**
 * Carinfomatching.php -카모아 등록차량과 업체등록차량의 정보를 매칭시키는 컨트롤
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class Carinfomatching extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/Carinfomaster_model');
        $this->load->model('carmore/CarmoreBoard_model');

        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));
        $ptype = $this->input->get('ptype', TRUE);

        switch($ptype) {
            default : $this->loadCarmatching(); break;
        }

    }


    //자동차 매칭정보 리스트 로드
    public function loadCarmatching()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $companyserial = $this->input->get('companyserial', TRUE);
        $sv = $this->input->get('sv', TRUE);
        $listitle="차량 관리";


        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));


        $rowresult = $this->Carinfomaster_model->getCarinfomstPageList($page, 20, '', '');
        $total = $this->Carinfomaster_model->getCarinfomstCount('', '');
        $startnum = $total-($page-1) * 20;

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/carmore/Carinfomaster";
        $config['suffix']     ="&sp&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];
        foreach($rowresult as $entry)
        {

            $arr_datalist["carinfo_idx"]=$entry->carinfo_idx;
            $arr_datalist["carinfokey"]=$entry->carinfokey;
            $arr_datalist["carinfomst_brand"]=$entry->carinfomst_brand;
            $arr_datalist["carinfomst_model"]=$entry->carinfomst_model;
            $arr_datalist["carinfomst_memo"]=$entry->carinfomst_memo;
            $arr_datalist["carinfomst_type"]=$entry->carinfomst_type;

            $arr_datalist["carinfomst_typestr"]=$this->customfunc->get_cartypestr($entry->carinfomst_type);
            $arr_datalist["carinfomst_titleimg"]=$entry->carinfomst_titleimg;
            $arr_datalist["min_fuel_efficiency"]=$entry->min_fuel_efficiency;
            $arr_datalist["max_fuel_efficiency"]=$entry->max_fuel_efficiency;

            $regdate =$entry->jdate;
            $arr_datalist["regdatev"] =$regdate;
            $arr_datalist["regdate"] =$this->customfunc->get_dateformat($regdate,"regs");

            $arr_datalist["filelist"]=$this->CarmoreBoard_model->get_BoardFileList($arr_datalist["carinfokey"],"");
            $arr_datalist["filecnt"] = sizeof($arr_datalist["filelist"]);



            $arr_alldatalist[]=$arr_datalist;
        }

        $data["list"]=$arr_alldatalist;
        $data["per_page"]=$per_page;
        $data["startnum"]=$startnum;
        $data["listitle"]=$listitle;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->LOGINID;
        $data["select_company"]=$this->customfunc->select_company($companyserial);

        $this->load->view('carmore/carinfomatching', array('data'=>$data));
        $this->load->view('footer');
    }

}
