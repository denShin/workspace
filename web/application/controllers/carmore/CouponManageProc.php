<?php
/**
 * CouponManageProc.php - 쿠폰 마스터 정보 등록,수정,삭제처리
 */
class CouponManageProc  extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('carmore/CouponManage_model');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        $demode =$this->input->get('emode');
        if($demode=="") $demode =$this->input->post('emode');


        switch($demode) {
            case "del" : $this->delCouponMstInfo(); break;
            default : $this->setCouponMstInfo(); break;
        }
    }


    //쿠폰 마스터 정보 등록,수정 처리 
    public function setCouponMstInfo()
    {
        $data["emode"] =$this->input->post('emode');
        $data["title"] =$this->input->post('title');
        $data["start_date"] =$this->input->post('start_date');
        $data["end_date"] =$this->input->post('end_date');
        $data["type2"] =$this->input->post('type2');
        $data["value_type"] =$this->input->post('value_type');
        $data["value"] =$this->input->post('value');
        $data["price"] =$this->input->post('price');
        $data["serial"] =$this->input->post('serial');

        $data["quantity"] =$this->input->post('quantity');
        $data["city"] =$this->input->post('city');
        $data["township"] =$this->input->post('township');
        $data["pcity"] =$this->input->post('pcity');
        $data["ptownship"] =$this->input->post('ptownship');
        $data["companyinfo"] =$this->input->post('companyinfo');
        $data["carinfo"] =$this->input->post('carinfo');

        $data["deadline_type"] =$this->input->post('deadline_type');
        $data["deadline_after_register"] =$this->input->post('deadline_after_register');
        if($data["deadline_type"]=="1"){$data["deadline_after_register"]="0";}
        if($data["deadline_after_register"]==""){$data["deadline_after_register"]="0";}
        $data["target_reserv_minimum_period"] =$this->input->post('target_reserv_minimum_period');
        $data["limit_pay"] =$this->input->post('limit_pay');
        $data["target_rent_type"] =$this->input->post('target_rent_type');

        $data["certificate_key"] =$this->input->post('certificate_key');
        $data["description"] =$this->input->post('description');
        $data["memo"] =$this->input->post('memo');
        $data["status"] =$this->input->post('status');

        $data["target_com_first_location"] =$data["township"];
        $data["target_location_str"] =$this->input->post('ptownship');
        if($data["target_location_str"] =="0" || $data["target_location_str"] =="" ){
            $data["target_location_coupon"] ='0';
        }else{
            $data["target_location_coupon"] ='1';
        }

        $data["target_com_idx"] =$this->input->post('target_com_idx');
        $data["target_bra_idx"] =$this->input->post('target_bra_idx');
        if($data["target_com_idx"] =="0" || $data["target_com_idx"] =="" ){
            $data["target_company_coupon"] ='0';
        }else{
            $data["target_company_coupon"] ='1';
        }

        $data["target_carinfo_idx"] =$this->input->post('target_carinfo_idx');
        if($data["target_carinfo_idx"] =="0" || $data["target_carinfo_idx"] =="" ){
            $data["target_carinfo_coupon"] ='0';
        }else{
            $data["target_carinfo_coupon"] ='1';
        }

        $data["target_reserv_term_start"] =$this->input->post('target_reserv_term_start');
        $data["target_reserv_term_end"] =$this->input->post('target_reserv_term_end');
        if($data["target_reserv_term_start"] =="" || $data["target_reserv_term_end"] =="" ){
            $data["target_reserv_term_coupon"] ='0';
        }else{
            $data["target_reserv_term_coupon"] ='1';
        }


        $data["target_reserv_minimum_period"] =$this->input->post('target_reserv_minimum_period');
        if($data["target_reserv_minimum_period"] =="0" || $data["target_reserv_minimum_period"] =="" ){
            $data["target_reserv_period_coupon"] ='0';
        }else{
            $data["target_reserv_period_coupon"] ='1';
        }

        $data["target_rent_type"] =$this->input->post('target_rent_type');
        if($data["target_rent_type"] =="0"){
            $data["target_rent_type_coupon"] ='0';
        }else{
            $data["target_rent_type_coupon"] ='1';
        }

        // type 가져오기
        // 1:등록O/제한/고유발급
        //2:등록O/제한/공통발급(선착순)
        //3:등록O/무제한/공통발급
        //4:등록X/제한/고유발급
        //5:등록X/제한/공통발급
        //6:등록X/무제한/공통발급
        if($data["quantity"] ==""){$data["quantity"] ="-1";}

        if($data["certificate_key"] ==""){
            // 쿠폰 코드미등록

            if($data["quantity"]=="-1"){
                //6:등록X/무제한/공통발급
                $data["type"]="6";

            }else{
                //4:등록X/제한/고유발급
                $data["type"]="4";
            }

        }else{
            // 쿠폰 코드등록

            if($data["quantity"]=="-1"){
                //3:등록O/무제한/공통발급
                $data["type"]="3";

            }else{
                // 1:등록O/제한/고유발급
                $data["type"]="1";
            }

        }

        $return_v = $this->CouponManage_model->setCouponMstInfo( $data);

        echo "<script>location.href='/carmore/CouponManage'</script>";
        exit();
    }

    // 쿠폰 마스터 정보 삭제처리
    public function delCouponMstInfo()
    {

    }

}
