<?php
/**
 * CompanyReorderStaticManage.php 신규/재주문통계 회사별 정보 검색,리스트 컨트롤
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class CompanyReorderStaticManage extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/StaticManage_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));
        $this->loadStaticList();

    }

    // 회사별 신규,재주문 통계 정보로딩
    public function loadStaticList()
    {
        $this->load->helper('url'); // load the helper first

        $startdate = $this->input->get('startdate', TRUE);
        $enddate = $this->input->get('enddate', TRUE);
        $stype = $this->input->get('stype', TRUE);


        if($startdate !="" && $enddate !=""){
            $this->session->set_userdata(array('search_stdate'=> $startdate));
            $this->session->set_userdata(array('search_eddate'=> $enddate));
        }
        if($startdate =="" && $enddate ==""){
            $startdate = $this->session->userdata('search_stdate');
            $enddate = $this->session->userdata('search_eddate');
        }


        $pagetitle ="신규/재주문통계(회사별)";
        $graphtitle =$pagetitle;
        if(!$stype) $stype="ordercompanycal";
        if(!$startdate) $startdate=date("Y-m-d",  strtotime("-1 week"));

        if(!$enddate) $enddate=date("Y-m-d");

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));

        $rowresult = $this->StaticManage_model->getStaticData($stype,$startdate,$enddate);

        $arr_chartdata[] =array( '회사명', '신규주문',array("role"=>'annotation') ,'재주문',array("role"=>'annotation'));


        foreach($rowresult as $entry)
        {
            $arr_datalist["resultval"]= $this->customfunc->get_dateformat($entry->caldate,"datestr");
            $chartdate = $this->customfunc->get_dateformat($entry->caldate,"st");
            
            //company_serial,NAME,neworder,reder
            $company_name =  $entry->company_name;
            $neworder = intval( $entry->neworder);
            $reorder = intval( $entry->reorder);
            $totalcount= $reorder+$neworder;

            $newper =intval ( ($neworder/$totalcount)*100);

            $arr_datalist["newcnt"]=number_format( $neworder,0);
            $arr_datalist["totalcount"]=number_format($totalcount,0);
            $arr_datalist["reordercnt"]=number_format( $reorder,0);
            $arr_datalist["newper"]=$newper;
            $arr_datalist["company_name"]=$company_name;

            $arr_alldatalist[]=$arr_datalist;
            $arr_chartdata[] =array($company_name,$neworder,$arr_datalist["newcnt"],$reorder,$arr_datalist["reordercnt"] );
        }

        $data["pagetitle"]=$pagetitle;
        $data["list"]=$arr_alldatalist;
        $data["startdate"]=$startdate;
        $data["enddate"]=$enddate;
        $data["graphtitle"]=$graphtitle;
        $data["stype"]=$stype;
        $data["loginid"]=$this->LOGINID;
        $data["chartdata"]=json_encode($arr_chartdata);


        $today=date("Y-m-d" );
        $week1=date("Y-m-d",  strtotime("-1 week"));
        $month1=date("Y-m-d",  strtotime("-1 month"));
        $month3=date("Y-m-d",  strtotime("-3 month"));
        $month6=date("Y-m-d",  strtotime("-6 month"));
        $year1=date("Y-m-d",  strtotime("-1 year"));
        $data["today"]=$today;
        $data["week1"]=$week1;
        $data["month1"]=$month1;
        $data["month3"]=$month3;
        $data["month6"]=$month6;
        $data["year1"]=$year1;

        $this->load->view('carmore/companyreorderstaticlist', array('data'=>$data));
        $this->load->view('footer');
    }

}
