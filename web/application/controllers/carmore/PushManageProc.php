<?php
/**
 * PushManageProc.php - 앱 푸시 리스트,등록,수정 처리 컨트롤  ( 사용 안함 X)
 */

class PushManageProc  extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library('Customfunc');
        $this->load->model('carmore/PushManage_model');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        $this->setPushInfo();
    }

    public function setPushInfo() {
        $data["emode"]=$this->input->post('emode');
        $data["pushlog_idx"] =$this->customfunc->SQL_Injection( $this->input->post('pushlog_idx', TRUE) );
        $data["pushlog_type"] =$this->customfunc->SQL_Injection( $this->input->post('pushlog_type', TRUE) );
        $data["pushlog_plat"] =$this->customfunc->SQL_Injection( $this->input->post('pushlog_plat', TRUE) );
        $data["pushlog_stats"] =$this->customfunc->SQL_Injection( $this->input->post('pushlog_stats', TRUE) );
        $data["pushlog_text"] =$this->customfunc->SQL_Injection( $this->input->post('pushlog_text', TRUE) );

        $return_v = $this->PushManage_model->setPushLogInfo( $data);

        if($return_v=="e"){
            echo "<script>alert('오류가 발생하였습니다.');history.back();</script>";
            exit();
        }else if($return_v=="y"){
            echo "<script>location.href='/carmore/PushManage'</script>";
            exit();
        }
    }



}
