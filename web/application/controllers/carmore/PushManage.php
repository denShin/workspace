<?php
/**
 * PushManage.php - 앱 푸시 리스트,등록,수정 컨트롤  ( 사용 안함 X)
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class PushManage  extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION;

    function __construct()
    {
        parent::__construct();

        $this->BOARD_CODE="notice";

        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/PushManage_model');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);

        switch($ptype) {
            case "v" : $this->loadPushView(); break;
            case "w" : $this->loadPushWrite(); break;
            default : $this->loadPushList(); break;
        }

    }

    public function loadPushList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $arr_Pushdata = $this->PushManage_model->getPushlogPageList($page,20,$sp,$sv);
        $total = $this->PushManage_model->getPushlogCount( $sp,$sv);
        $startnum = $total-($page-1) * 20;

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/carmore/PushManage";
        $config['suffix']     ="&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_allboardlist=[];
        foreach($arr_Pushdata as $entry)
        {
            $arr_boardlist["pushlog_idx"] =$entry->pushlog_idx;
            $arr_boardlist["pushlog_type"] =$entry->pushlog_type;
            $arr_boardlist["pushlog_plat"]= $entry->pushlog_plat;
            $arr_boardlist["pushlog_stats"] =$entry->pushlog_stats;
            $arr_boardlist["pushlog_text"] =$entry->pushlog_text;
            $arr_boardlist["regdate"] =$entry->regdate;

            if($arr_boardlist["pushlog_type"]=="ad"){
                $arr_boardlist["pushlog_typestr"]="광고 푸시";
            }else{
                $arr_boardlist["pushlog_typestr"]="전체 푸시";
            }
            if($arr_boardlist["pushlog_plat"]=="all"){
                $arr_boardlist["pushlog_platstr"]="전체전송";
            }else if($arr_boardlist["pushlog_plat"]=="iphone"){
                $arr_boardlist["pushlog_platstr"]="아이폰";
            }else if($arr_boardlist["pushlog_plat"]=="android"){
                $arr_boardlist["pushlog_platstr"]="안드로이드폰";
            }

            if($arr_boardlist["pushlog_stats"]=="n"){
                $arr_boardlist["pushlog_statstr"]="발송대기";
            }else if($arr_boardlist["pushlog_stats"]=="i"){
                $arr_boardlist["pushlog_statstr"]="발송진행";
            }else if($arr_boardlist["pushlog_stats"]=="y"){
                $arr_boardlist["pushlog_statstr"]="발송완료";
            }

            $arr_boardlist["regdate"] =$this->customfunc->get_dateformat($entry->regdate);

            $arr_allboardlist[]=$arr_boardlist;
        }

        $data["list"]=$arr_allboardlist;
        $data["per_page"]=$per_page;
        $data["pagination"]=$pagination;
        $data["startnum"]=$startnum;

        $this->load->view('carmore/pushmanagelist', array('data'=>$data,'permission'=>$this->ARR_PERMISSION));
        $this->load->view('footer');
    }

    public function loadPushWrite()
    {
        $board_idx = $this->input->get('board_idx', TRUE);

        if($board_idx !=""){
            $data["emode"]="edit";
            $arr_Pushdata = $this->PushManage_model->getPushDetail($board_idx);

            $data["pushlog_idx"] =$arr_Pushdata["pushlog_idx"];
            $data["pushlog_type"] =$arr_Pushdata["pushlog_type"];
            $data["pushlog_plat"]= $arr_Pushdata["pushlog_plat"];
            $data["pushlog_stats"] =$arr_Pushdata["pushlog_stats"];
            $data["pushlog_text"] =$arr_Pushdata["pushlog_text"];
            $data["regdate"] =$arr_Pushdata["regdate"];
        }else{

            $data["emode"]="new";
            $data["pushlog_idx"] = "";
            $data["pushlog_type"] ="";
            $data["pushlog_plat"]= "";
            $data["pushlog_stats"] ="";
            $data["pushlog_text"] ="";
            $data["regdate"] = "";

        }

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('carmore/pushmanagewrite', array('data'=>$data));
        $this->load->view('footer');
    }

    public function loadPushView()
    {
        $board_idx = $this->input->get('board_idx', TRUE);

        $data["emode"]="edit";
        $arr_Pushdata = $this->CarmoreBoard_model->getPushDetail($board_idx);

        $data["board_idx"] =$arr_Pushdata["board_idx"];
        $data["content_code"] =$arr_Pushdata["content_code"];
        $data["startdate"]= $arr_Pushdata["startdate"];
        $data["enddate"] =$arr_Pushdata["enddate"];
        $data["board_title"] =$arr_Pushdata["board_title"];
        $data["board_code"] =$arr_Pushdata["board_code"];
        $data["board_content"] =$arr_Pushdata["board_content"];
        $data["board_part"] =$this->customfunc->get_notice_type($arr_Pushdata["board_part"]);
        $data["mem_id"] =$arr_Pushdata["mem_id"];
        $data["readcnt"] =$arr_Pushdata["readcnt"];
        $data["contentype_str"] =$arr_Pushdata["contentype_str"];
        $data["regdate"] =$arr_Pushdata["regdate"];
        $data["wauth"]=$this->customfunc->get_writeauth($this->session->userdata('admin_id'),$data["mem_id"],$this->session->userdata('master_yn'));
        $data["per_page"] = $this->input->get('per_page', TRUE);

        $arr_PushCommentdata=$this->CarmoreBoard_model->getBoardReplyList($data["board_code"],$board_idx);
        $arr_allboardcommentlist=[];
        foreach($arr_PushCommentdata as $entry) {
            $arr_boardcomlist["board_idx"] =$entry->board_idx;
            $arr_boardcomlist["board_code"] =$entry->board_code;
            $arr_boardcomlist["mem_id"] =$entry->mem_id;
            $arr_boardcomlist["regdate"] =  $this->customfunc->get_dateformat($entry->regdate);
            $arr_boardcomlist["commenttext"] =$entry->commenttext;
            $arr_boardcomlist["comment_idx"] =$entry->comment_idx;
            $arr_boardcomlist["mem_name"] =$entry->mem_name;
            $arr_boardcomlist["wauth"] =$this->customfunc->get_writeauth($this->session->userdata('admin_id'),$arr_boardcomlist["mem_id"],$this->session->userdata('master_yn'));
            $arr_allboardcommentlist[]=$arr_boardcomlist;
        }

        $data["commentlist"] =$arr_allboardcommentlist;

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('carmore/Pushview', array('data'=>$data));
        $this->load->view('footer');
    }
}
