<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['pagination']['base_url'] =APP_URL."/";
$config['pagination']['total_rows'] = ""; // Count total rows in the query
$config['pagination']['full_tag_open'] = '<ul class="pagination">';
$config['pagination']['full_tag_close'] = '</ul>';
$config['pagination']['per_page'] = "";
$config['pagination']['num_links'] = 5;
$config['pagination']['page_query_string'] = TRUE;
$config['pagination']['prev_link'] = '&lt; Prev';
$config['pagination']['prev_tag_open'] = '<li>';
$config['pagination']['prev_tag_close'] = '</li>';
$config['pagination']['next_link'] = 'Next &gt;';
$config['pagination']['next_tag_open'] = '<li>';
$config['pagination']['next_tag_close'] = '</li>';
$config['pagination']['cur_tag_open'] = '<li class="active"><a href="#">';
$config['pagination']['cur_tag_close'] = '</a></li>';
$config['pagination']['num_tag_open'] = '<li>';
$config['pagination']['num_tag_close'] = '</li>';
$config['pagination']['first_link'] = FALSE;
$config['pagination']['last_link'] = FALSE;

// --------------------------------------------------------------------------

/* End of file pagination.php */
/* Location: ./bookymark/application/config/pagination.php */