<?php
/**
 * Class Affiliate_inventory_model
 *
 * @author DEN
 *
 * @property CI_Loader load
 *
 * @property Workspace_common_func  $workspace_common_func
 */

class Affiliate_inventory_model extends WS_Model {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('Workspace_common_func');
    }

    /**
     * 업체명 검색하기
     *
     * @param $search_param int    검색파라미터 (1:파트너스업체, 2:API 업체)
     * @param $search_value string
     *
     * @return stdClass
     */
    public function search_affiliate($search_param, $search_value)
    {
        if ($search_param === 1)
            return $this->search_partners_affiliate($search_value);
        elseif ($search_param === 2)
            return $this->search_api_affiliate($search_value);
        else
        {
            $return_info = new stdClass();

            $return_info->result = 2;

            return $return_info;

        }

    }

    /**
     * 파트너스 업체 검색하기
     *
     * @param $search_value string
     *
     * @return stdClass
     */
    private function search_partners_affiliate($search_value)
    {
        $return_info = new stdClass();
        $inventory   = [];

        $name_like = "%".$search_value."%";

        $query = "
        SELECT
            branch_info.*,
            IFNULL(c050i.tel050_number, branch_info.tel) branch_tel
        FROM
            (
            SELECT
                nrc.serial company_idx, nrc.name,
                nrcb.serial branch_idx, nrcb.branchName, TRIM(CONCAT(address, ' ', detailAddress)) addr, tel
            FROM
                new_rentCompany nrc INNER JOIN
                new_rentCompany_branch nrcb
                ON
                    nrc.serial=nrcb.rentCompany_serial
            WHERE
                nrc.state=1 AND nrc.test_check <> 1 AND nrcb.state=1 AND nrc.name LIKE ?  
            ) branch_info LEFT OUTER JOIN
            carmore_050info c050i
            ON
                c050i.tel050_part='in' AND branch_info.branch_idx=c050i.fk_serial AND tel050_useyn='y'";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$name_like]);
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            $affiliate_obj = new stdClass();

            $affiliate_obj->type        = 1;
            $affiliate_obj->companyIdx  = (int)$stmt['company_idx'];
            $affiliate_obj->branchIdx   = (int)$stmt['branch_idx'];
            $affiliate_obj->companyName = $stmt['name'];
            $affiliate_obj->branchName  = $stmt['branchName'];
            $affiliate_obj->address     = $stmt['addr'];
            $affiliate_obj->tel         = $this->workspace_common_func->format_tel($stmt['branch_tel']);

            array_push($inventory, $affiliate_obj);

        }

        $return_info->result    = 1;
        $return_info->inventory = $inventory;

        return $return_info;

    }

    /**
     * API 업체 검색하기
     *
     * @param $search_value string
     *
     * @return stdClass
     */
    private function search_api_affiliate($search_value)
    {
        $return_info = new stdClass();
        $inventory   = [];

        $name_like = "%".$search_value."%";

        $query = "
        SELECT
            waa_idx, waa_name,
            waab_idx, IFNULL(waab_name, '') waab_name, CONCAT(waab_main_address, ' ', waab_sub_address) addr, waab_tel 
        FROM
            workspace_api_affiliate waa INNER JOIN
            workspace_api_affiliate_branch waab
            ON
                waa_idx = waab_waa_idx
        WHERE
            waa_state=1 AND waab_state=1 AND waa_name LIKE ?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$name_like]);
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            $affiliate_obj = new stdClass();

            $affiliate_obj->type        = 2;
            $affiliate_obj->companyIdx  = (int)$stmt['waa_idx'];
            $affiliate_obj->branchIdx   = (int)$stmt['waab_idx'];
            $affiliate_obj->companyName = $stmt['waa_name'];
            $affiliate_obj->branchName  = $stmt['waab_name'];
            $affiliate_obj->address     = $stmt['addr'];
            $affiliate_obj->tel         = $this->workspace_common_func->format_tel($stmt['waab_tel']);

            array_push($inventory, $affiliate_obj);
        }

        $return_info->result    = 1;
        $return_info->inventory = $inventory;

        return $return_info;

    }

}
