<?php
/**
 * 파트너스 제휴사 관련 모델
 *
 * @property CI_Loader load
 * @property CI_DB_pdo_mysql_driver $db
 *
 * @property Workspace_common_func $workspace_common_func
 */
class Partners_affiliate_model extends WS_Model {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Workspace_common_func');
    }

    /**
     * 업체 특이사항 변경
     *
     * @param $branch_idx int
     * @param $uniqueness   string
     */
    public function update_uniqueness($branch_idx, $uniqueness)
    {
        $query = "UPDATE new_rentCompany_branch SET nrcb_etc_uniqueness=? WHERE serial=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$uniqueness, $branch_idx]);

    }

    /**
     * 업체 입점일 변경
     *
     * @param $branch_idx int
     * @param $enter_date datetime
     */
    public function update_enter_date($branch_idx, $enter_date)
    {
        $branch_idx = $this->workspace_common_func->check_sql_injection($branch_idx, TRUE);
        $enter_date = $this->workspace_common_func->check_sql_injection($enter_date, TRUE);

        $query = "UPDATE new_rentCompany_branch SET nrcb_enter_carmore_date=? WHERE serial=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$enter_date, $branch_idx]);
    }

    /**
     * 업체 한마디 변경
     *
     * @param $branch_idx int
     * @param $intro_code int
     * @param $intro_msg  string
     */
    public function update_intro_msg($branch_idx, $intro_code, $intro_msg)
    {
        $branch_idx = $this->workspace_common_func->check_sql_injection($branch_idx, TRUE);
        $intro_code = $this->workspace_common_func->check_sql_injection($intro_code, TRUE);
        $intro_msg  = $this->workspace_common_func->check_sql_injection($intro_msg, TRUE);

        $query = "UPDATE new_rentCompany_branch SET introcode=?, intromsg=? WHERE serial=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$intro_code, $intro_msg, $branch_idx]);

    }
}
