<?php
/**
 * 050 전화 관련
 *
 * Created by PhpStorm.
 * User: teamo2_server
 * Date: 2019-04-10
 * Time: 11:13
 */

require_once $_SERVER['DOCUMENT_ROOT'].'/application/models/partners/Partners_affiliate_model.php';

class Onse_model extends Partners_affiliate_model {
    const API_AFFI_START = "7777";

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 온세 (050) 와 연결된 업체 정보와 번호정보
     *
     * @return array
     */
    public function load_onse_active_inventory()
    {
        $inventory = [];

        $query = "
        SELECT *
        FROM
            (
                (
                SELECT
                    nrcb.tel, c050i.tel050_number, c050i.tel050_matnumber, tel050_part, CONCAT(name, ' ', branchName) affi_name
                FROM
                    carmore_050info c050i INNER JOIN
                    new_rentCompany_branch nrcb INNER JOIN
                    new_rentCompany nrc
                    ON
                        c050i.fk_serial=nrcb.serial AND tel050_part='in' AND tel050_useyn='y'
                        AND nrcb.rentCompany_serial=nrc.serial
                ) UNION ALL
                (
                SELECT
                    nccb.tel, c050i.tel050_number, c050i.tel050_matnumber, tel050_part, nccb.business_name
                FROM
                    carmore_050info c050i INNER JOIN
                    new_carmore_call_business nccb
                    ON
                        c050i.fk_serial=nccb.serial AND tel050_part='out' AND tel050_useyn='y'
                )
            ) affi_inven";
        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute();
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            array_push($inventory, [
                'part'              => $stmt['tel050_part'],        # 업체타입 (in : 내부업체, out: 전화걸기업체)
                'affiName'          => $stmt['affi_name'],
                'branchTel'         => $stmt['tel'],                # 현재 업체 번호
                'onseTel'           => $stmt['tel050_number'],      # 매칭번호
                'onseMathBranchTel' => $stmt['tel050_matnumber']    # 매칭당시 업체연락처
            ]);

        }

        return $inventory;

    }

    /**
     * API 업체의 추천 전화번호 가져오기
     *
     * @return string
     */
    public function load_api_affi_recommend_num()
    {
        $query = "SELECT tel050_shortnum FROM carmore_050info c050i WHERE tel050_shortnum >= ".self::API_AFFI_START." AND tel050_useyn='n' AND tel050_goldyn='n' ORDER BY RAND() LIMIT 1";

        $sql = $this->slave->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute();
        if ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            return $stmt['tel050_shortnum'];
        }
        else
            return "";
    }

}
