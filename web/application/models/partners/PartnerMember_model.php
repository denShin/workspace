<?php
/**
 * PartnerMember_model - 파트너 회원정보 관련 db
 */

class PartnerMember_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    //파트너 기업 갯수
    function getPartnerMemberCount($sp="",$sv=""){
        $sql="select count(*) as cnt from  new_rentCompany where 1=1 ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    //파트너 기업 리스트페이지
    function getPartnerMemberPageList($page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select *
      ,(select authority from new_member where rentCompany_serial=a.SERIAL limit 1) as authority
        ,(select filesavename from admin_uploadfile
          where content_code= concat('mainimg_',a.serial)  and  fileuptype='main' and publish_yn='y' ) as mainimage 
             ,(select count(*) from admin_uploadfile
          where content_code= concat('subimg_',a.serial) and  fileuptype='sub'  and publish_yn='y') as subcnt 
       
       from   new_rentCompany a where state <>'0' ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by regdate desc limit $start,20";

        $query = $this->db->query($sql);

        return $query->result();
    }


    //파트너 기업지점 리스트 카운트
    function getPartnerMemberBranchCount($sp="",$sv=""){

        /*
         *   $sql="select count(*) as cnt from   new_rentCompany_branch a
        inner join new_rentCompany b inner join new_member c
        on a.rentCompany_serial=b.SERIAL and  a.SERIAL =c.rentCompany_branch_serial  where 1=1 ";
         */
        $sql="SELECT COUNT(*) AS cnt
         FROM (SELECT a.*, b.name AS company_name FROM   new_rentCompany_branch a INNER JOIN new_rentCompany b
        ON a.rentCompany_serial=b.SERIAL  where b.state<>'0' and a.state=1  ) a LEFT JOIN new_member c 
          ON  a.SERIAL =c.rentCompany_branch_serial 
      WHERE  c.state <>'9'   "; //and c.authority='1'
        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }


    //파트너 기업지점 리스트
    function getPartnerMemberBranchPageList($page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;

        $sql="
        SELECT
            a.*,
            c.SERIAL mem_serial, c.id mem_id, c.regdate mem_regdate, c.name mem_name, c.authority authority, c.rentCompany_branch_serial rentCompany_branch_serial,
            (SELECT filesavename FROM admin_uploadfile WHERE content_code = concat('mainimg_', a.serial) AND fileuptype='main' AND publish_yn='y') mainimage,
            (SELECT count(*) FROM admin_uploadfile WHERE content_code= concat('subimg_', a.serial) AND fileuptype='sub' AND publish_yn='y') subcnt 
        FROM
            (
            SELECT
                a.*, b.name company_name
            FROM
                new_rentCompany_branch a INNER JOIN
                new_rentCompany b
                ON
                    a.rentCompany_serial=b.SERIAL
            WHERE b.state=1 and a.state=1) a LEFT JOIN
            (SELECT * FROM new_member WHERE state <> 9 GROUP BY rentCompany_branch_serial) c
            ON
                a.SERIAL = c.rentCompany_branch_serial";
        if($sp!="" && $sv!=""){
            $sql.=" WHERE $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by SERIAL desc limit $start,20";

        $query = $this->db->query($sql);

        return $query->result();
    }


    //파트너 기업별 지점 리스트
    function getBranchList($companyserial){

        $sql="select * 
          from   new_rentCompany_branch a  where rentCompany_serial='$companyserial' and state='1'";

        $query = $this->db->query($sql)->result_array();

        return $query ;
    }

    //파트너 기업지점 상세 리스트
    function getBranchDtlList($branchserial){

        $sql="select * 
          from   new_member a  where rentCompany_branch_serial='$branchserial' and state='1'";

        $sql="SELECT a.*, c.SERIAL AS mem_serial ,c.id AS mem_id, c.regdate as mem_regdate
         ,c.name AS mem_name,c.authority AS authority,c.rentCompany_branch_serial AS rentCompany_branch_serial
         FROM (SELECT a.*, b.name AS company_name FROM   new_rentCompany_branch a INNER JOIN new_rentCompany b
        ON a.rentCompany_serial=b.SERIAL   ) a
          inner JOIN new_member c ON  a.SERIAL =c.rentCompany_branch_serial where c.rentCompany_branch_serial='$branchserial'";
        $query = $this->db->query($sql)->result_array();

        return $query ;
    }


    //파트너 기업회원 비밀번호 초기화
    function setInitPwd($mem_id,$mem_pwd){
        $sql="update new_member set passwordHash='$mem_pwd' where id='$mem_id'";
        $query = $this->db->query($sql);
        return 'y';
    }


    //파트너 기업 상태변경
    function chagecomstats($data){
        $stats =$data["stats"];
        $arr_comserial =$data["comserial"];

        foreach ($arr_comserial as $s){
            $sql="update new_rentCompany set state='".$stats."' where SERIAL='".$s."'";

            $this->db->query($sql);
        }
    }

    //파트너 기업지점 탈퇴처리
    function setpartnerout($data){
        $companyserial = $data["companyserial"];
        $sql="update new_rentCompany  set state='0' where serial='$companyserial'";

        $query = $this->db->query($sql);
        return 'y';
    }


    //파트너 기업지점 풀,심플 ,승인,차단처리
    public function chageallcomstats($data){
        $saveparam =$data["saveparam"];
        $stats =$data["stats"];
        $upart =$data["upart"];
        $arr_comserial = explode(",",$saveparam);

        for($i=0;$i < sizeof($arr_comserial);$i++){
            $rentCompany_serial = $arr_comserial[$i];

            if($upart=="stats"){
                if($stats=="y"){
                    $authority="1";
                }else{
                    $authority="50";
                }
                $sql="update new_member set authority ='$authority' WHERE  rentCompany_serial=".$rentCompany_serial ;
            }else if($upart=="ver"){

                if($stats=="f"){
                    $simple_ver="0";
                }else{
                    $simple_ver="1";
                }
                $sql="update new_rentCompany set simple_ver ='$simple_ver' WHERE  serial=".$rentCompany_serial ;
            }else if($upart=="month"){

                if($stats=="y"){
                    $authority="1";
                }else{
                    $authority="0";
                }
                $sql="update new_rentCompany_branch set carmore_month_available ='$authority' WHERE  serial=".$rentCompany_serial ;
            }else if($upart=="normal"){

                if($stats=="y"){
                    $authority="1";
                }else{
                    $authority="0";
                }
                $sql="update new_rentCompany_branch set carmore_normal_available ='$authority' WHERE  serial=".$rentCompany_serial ;
            }

            $this->db->query($sql);
        }
    }


    //파트너 050 번호 정보가져오기
    function get050telCount($sendnewtelnum){
        // carmore_050info tel050_useyn tel050_number
        $sql="select expire ,tel050_useyn from  carmore_050info where  tel050_number ='$sendnewtelnum' ";
        $tel050_useyn = $this->db->query($sql)->row()->tel050_useyn;
        $expire = $this->db->query($sql)->row()->expire;

        if($tel050_useyn =="y"){
            $cnt =1;
        }else{
            if($expire !="0"){
                $cnt =1;
            }else{
                $cnt =0;
            }
        }

        return $cnt;
    }


    //050 전화번호 신규등록
    public function proctel050($data){

        $datamode =$data["datamode"];
        $serial  =$data["bserial"];
        $sendnewtelnum  =$data["sendnewtelnum"];

        if($datamode =="new"){

            // tel050_matnumber fk_serial tel050_part
            $tel050_matnumber  =$data["branch_tel"];
            $fk_serial  =$data["fk_serial"];
            $tel050_part  =$data["tel050_part"];
            $sql="update carmore_050info set tel050_matnumber ='$tel050_matnumber',tel050_useyn='y'
                                            ,tel050_part='$tel050_part', fk_serial='$fk_serial',expire='0'
                    WHERE   tel050_number ='$sendnewtelnum'" ;
        }else if($datamode =="del"){
            $expire = strtotime("+3 months", strtotime(date("y-m-d")));
            $deltel =$data["deltel"];
            $deltel=str_replace("-","",$deltel);

            $sql="update carmore_050info set expire='$expire',tel050_useyn='n' ,tel050_deldate=now() WHERE   tel050_number ='$deltel'" ;

        }else if($datamode =="comdel"){
            $expire = strtotime("+3 months", strtotime(date("y-m-d")));
            $deltel =$data["deltel"];
            $deltel=str_replace("-","",$deltel);

            $sql="update carmore_050info set expire='',tel050_useyn='n',tel050_matnumber='',tel050_deldate='',fk_serial='0',tel050_part='' WHERE   tel050_number ='$deltel'" ;

        }else if($datamode =="restart"){
            $expire = strtotime("+3 months", strtotime(date("y-m-d")));
            $sendnewtelnum =$data["sendnewtelnum"];
            $sendnewtelnum=str_replace("-","",$sendnewtelnum);

            $sql="update carmore_050info set expire='',tel050_useyn='y' ,tel050_deldate=''  WHERE   tel050_number ='$sendnewtelnum'" ;

        }

        $this->db->query($sql);

    }



    //050 번호 & 기업리스트 카운트
    function getCallPartnerCount($sp="",$sv=""){
        $sql="select count(*) as cnt from  new_carmore_call_business   a left join
  (select tel050_number, fk_serial from   carmore_050info where tel050_useyn='y' and tel050_part='out' ) b 
  on a.serial = b.fk_serial where 1=1 ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }


    //050 번호 & 기업리스트 리스트
    function getCallPartnerPageList($page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select *   from   new_carmore_call_business  a left join
  (select tel050_number, fk_serial from   carmore_050info where tel050_useyn='y' and tel050_part='out' ) b
  on a.serial = b.fk_serial
        where 1=1 ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by serial desc limit $start,20";

        $query = $this->db->query($sql);

        return $query->result();
    }



    //전화 연결 회사정보상세
    function getCallPartnerDtl($serial){

        $sql="select * from  new_carmore_call_business where serial='$serial' ";
        $query = $this->db->query($sql);
        $result=$query->result();
        return $result[0];
    }

    // 전화연결 회사 정보 등록,수정하기
    public function setcallpartner($data){

        $datamode =$data["datamode"];
        $serial =$data["serial"];
        $business_name=$data["business_name"];
        $service_city=$data["service_city"];
        $service_location=$data["service_location"];
        $service_address=$data["service_address"];
        $service_time =$data["service_time"];
        $tel=$data["tel"];
        $state=$data["state"];

        if($datamode =="new"){

            $state="0";

            $sql="insert into new_carmore_call_business(business_name,service_city,service_location,service_time,tel,state,service_address)
                  values('$business_name','$service_city','$service_location','$service_time','$tel','$state','$service_address')";

        }else if($datamode =="edit") {

            $sql = "update new_carmore_call_business set business_name ='$business_name',service_city='$service_city' ,state='$state' 
             ,service_location='$service_location',service_time='$service_time',tel='$tel', service_address='$service_address'
                          WHERE  serial='" . $serial . "'";

        }else if($datamode=="changestats"){

            $sql = "update new_carmore_call_business set state ='$state'   WHERE  serial='" . $serial . "'";
        }


        $this->db->query($sql);
    }

    //050 골드 번호 저장하기
    function proctelgoldnum($data){
        $datamode =$data["datamode"];
        $sendnewtelnum =$data["sendnewtelnum"];
        $sendnewtelnum ="05037961".$sendnewtelnum;

        if($datamode=="new"){
            //  carmore_050info tel050_number tel050_goldyn
            $sql="select count(*) as cnt from  carmore_050info where  tel050_number ='$sendnewtelnum' and tel050_goldyn='n' " ;

            $cnt = $this->db->query($sql)->row()->cnt;

            if($cnt >0){

                $sql="update carmore_050info set tel050_goldyn='y' where  tel050_number ='$sendnewtelnum' ";
                $this->db->query($sql);
                return 0;

            }else{

                return 1;
            }

        }
        else if($datamode=="del"){
            $tel050_idx =$data["tel050_idx"];
            $sql="update carmore_050info set tel050_goldyn='n' where  tel050_idx ='$tel050_idx' ";
            $this->db->query($sql);
            return 0;
        }
    }
    
    //050 번호 관리 카운트
    function getTelmanageCount($teltype, $sp="",$sv=""){
        $tblstr ="SELECT *
                      ,CASE WHEN tel050_part='in' THEN (SELECT b.name AS company_name FROM   new_rentCompany_branch a INNER JOIN new_rentCompany b      ON a.rentCompany_serial=b.SERIAL  WHERE a.SERIAL=f.fk_serial)
                        WHEN tel050_part='out' THEN (SELECT business_name FROM new_carmore_call_business WHERE SERIAL =f.fk_serial)
                        END AS 'company_name' FROM carmore_050info f ";

        if($teltype=="gold"){
            $sql="select count(*) as cnt from   ($tblstr) a  where tel050_goldyn='y' ";
        }else if($teltype=="expire"){
            $sql="select count(*) as cnt from   ($tblstr) a  where  tel050_useyn='n' and expire >'0' ";
        }else if($teltype=="use"){
            $sql="select  count(*) as cnt  from   ($tblstr) a  where tel050_useyn='y'  ";
        }

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }


    //050 번호 관리 리스트
    function getTelmanagePageList($teltype,$page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;

        $tblstr ="SELECT *
                      ,CASE WHEN tel050_part='in' THEN (SELECT b.name AS company_name FROM   new_rentCompany_branch a INNER JOIN new_rentCompany b      ON a.rentCompany_serial=b.SERIAL  WHERE a.SERIAL=f.fk_serial)
                        WHEN tel050_part='out' THEN (SELECT business_name FROM new_carmore_call_business WHERE SERIAL =f.fk_serial)
                        END AS 'company_name' FROM carmore_050info f ";

        if($teltype=="gold"){
            $sql="SELECT * FROM ($tblstr) a where tel050_goldyn='y' ";
        }else if($teltype=="expire"){
            $sql="select * from   ($tblstr) a  where tel050_useyn='n' and expire >'0'  ";
        }else if($teltype=="use"){
            $sql="select * from   ($tblstr) a  where tel050_useyn='y'  ";
        }

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by tel050_idx desc limit $start,20";

        $query = $this->db->query($sql);

        return $query->result();
    }


    //전화 연결 파트너 정보 리스트 가져오기
    function getTelcompanyinfBy050idx( $fk_serial , $tel050_part){

        if($tel050_part=="in"){
            $sql=" SELECT a.*, b.name AS company_name FROM   new_rentCompany_branch a INNER JOIN new_rentCompany b
        ON a.rentCompany_serial=b.SERIAL  where a.SERIAL='$fk_serial'";
        }else if($tel050_part=="out"){
            $sql="select * from new_carmore_call_business where serial ='$fk_serial'";
        } elseif ($tel050_part === 'api') {
            $sql = "
            SELECT
                TRIM(CONCAT(waa_name, ' ', IFNULL(waab_name, ''))) company_name,
                TRIM(CONCAT(waab_main_address, ' ', waab_sub_address)) address, waab_open_time, waab_close_time
            FROM
                workspace_api_affiliate waa INNER JOIN
                workspace_api_affiliate_branch waab
                ON
                    waa_idx=waab_waa_idx
            WHERE
                waab_idx='".$fk_serial."'";
        }

        $query = $this->db->query($sql)->result_array();
        return $query[0];
    }

    //050 번호 등록하기
    function insnumber(){

        // 0503-7961
        for($i=0;$i < 10000 ;$i++){
            $tval = "0000".$i;
            $tval = substr($tval, (strlen($tval) - 4), strlen($tval));
            $telnum ="05037961".$tval;
            $sql="insert into carmore_050info(tel050_number) values('$telnum')";
            $query = $this->db->query($sql);
            echo $telnum."<BR>";
        }

    }

    // 각 파트너 기업의 이미지 가져오기
    function getPartnerinfoImage($fileuptype,$content_code){


        $sql="select * from admin_uploadfile where content_code='$content_code'
                and  fileuptype='$fileuptype' ";

        $query = $this->db->query($sql)->result_array();

        return $query ;
    }

    // 파트너 기업의 이미지 publish 하기
    function procPublishimage($publish_yn,$filename){
        $sql="UPDATE  admin_uploadfile SET  publish_yn = '".$publish_yn."'    WHERE fileSaveName ='".$filename."' ";

        $this->db->query($sql);
    }

    //각 지점별 소개글 등록하기
    function setbranchintro($data){


        $serial =$data["serial"];
        $introcode =$data["introcode"];
        $intromsg =$data["intromsg"];

        $sql="update new_rentCompany_branch set  introcode='$introcode',  intromsg='$intromsg' where  serial ='$serial' ";

        $this->db->query($sql);
        return ;
    }

    // 기업 리뷰 전체 카운트가져오기
    function getReviewallcount(){
        $sql="select count(*) as cnt from new_review_list where  opinion <>'' ";
        $data["totalcnt"] = $this->db->query($sql)->row()->cnt;

        $sql="select count(*) as cnt from new_review_list a inner join carmore_review_reply b
            on a.reservation_idx =b.crr_reservation_idx where a.opinion <>''  ";
        $data["replycnt"]  = $this->db->query($sql)->row()->cnt;


        return $data;
    }

    // 기업 리뷰 카운트가져오기
    function getReviewCount($sp="",$sv=""){
        $sql="select count(*) as cnt
from new_review_list a inner join  new_rentCompany_branch b
  INNER JOIN new_rentCompany c
  inner join
  (select driver_name,f_reservationidx from tbl_reservation_list p inner join  new_rentSchedule q
    on p.f_bookingid=q.serial
    where   p.f_usrserial not in (2888, 1131, 2568, 1067, 6974, 1166, 1158, 6972, 967, 34897) )d

on a.company_serial=c.serial and a.branch_serial=b.serial
     and a.reservation_idx=d.f_reservationidx";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    // 기업 리뷰 정보리스트
    function getReviewPageList($page=1,$pageline=20,$sp="",$sv="",$pageorderby=""){

        $arrorder =explode("-",$pageorderby);
        $ordfield =$arrorder[0];
        $orderby =$arrorder[1];

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select  a.*,b.branchName as branchname , c.name as companyname,d.driver_name,e.*
,d.f_rentstartdate,d.f_rentenddate,d.driver_phone_encrypt,d.f_usrserial
,(select count(*) as cnt  from tbl_reservation_list   where f_usrserial=d.f_usrserial and f_paystatus='1' ) as revcnt
,(select count(*) as cnt  from carmore_review_reply   where crr_reservation_idx=a.reservation_idx  ) as replycnt
from new_review_list a inner join  new_rentCompany_branch b
  INNER JOIN new_rentCompany c
  inner join
  (select driver_name,f_reservationidx,f_carserial,f_rentstartdate,f_rentenddate,driver_phone_encrypt,p.f_usrserial from tbl_reservation_list p inner join  new_rentSchedule q
    on p.f_bookingid=q.serial
    where   p.f_usrserial not in (2888, 1131, 2568, 1067, 6974, 1166, 1158, 6972, 967, 34897) )d
    INNER JOIN (SELECT a.* , b.brand,  b.carType,   b.carType_flag, b.model, 
 b.min_fuel_efficiency, b.max_fuel_efficiency FROM new_rentCar a INNER JOIN new_car b ON a.car_serial=b.SERIAL) e

on a.company_serial=c.serial and a.branch_serial=b.serial and   d.f_carserial =e.SERIAL  
     and a.reservation_idx=d.f_reservationidx ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by $ordfield $orderby limit $start,20";

        $query = $this->db->query($sql);

        return $query->result();
    }

    // 예약건에 따른 리뷰가져오기
    function getreservereview($rdata){
        $reservation_idx =$rdata["reservation_idx"];
        $sql="update new_review_list set admincheck_yn='y'  where reservation_idx='$reservation_idx'";
        $this->db->query($sql);

        $sql="select  a.*,b.branchName as branchname , c.name as companyname,d.driver_name,e.*
,d.f_rentstartdate,d.f_rentenddate,d.driver_phone_encrypt,d.f_usrserial
,(select count(*) as cnt  from tbl_reservation_list   where f_usrserial=d.f_usrserial and f_paystatus='1' ) as revcnt
,(select count(*) as cnt  from carmore_review_reply   where crr_reservation_idx=a.reservation_idx  ) as replycnt
from new_review_list a inner join  new_rentCompany_branch b
  INNER JOIN new_rentCompany c
  inner join
  (select driver_name,f_reservationidx,f_carserial,f_rentstartdate,f_rentenddate,driver_phone_encrypt,p.f_usrserial from tbl_reservation_list p inner join  new_rentSchedule q
    on p.f_bookingid=q.serial
    where   p.f_usrserial not in (2888, 1131, 2568, 1067, 6974, 1166, 1158, 6972, 967, 34897) )d
    INNER JOIN (SELECT a.* , b.brand,  b.carType,   b.carType_flag, b.model, 
 b.min_fuel_efficiency, b.max_fuel_efficiency FROM new_rentCar a INNER JOIN new_car b ON a.car_serial=b.SERIAL) e

on a.company_serial=c.serial and a.branch_serial=b.serial and   d.f_carserial =e.SERIAL  
     and a.reservation_idx=d.f_reservationidx  WHERE  a.reservation_idx='$reservation_idx'";
        $data["review"] = $this->db->query($sql)->result_array();

        $sql="SELECT * FROM carmore_review_reply WHERE crr_reservation_idx='$reservation_idx'";
        $data["answer"] = $this->db->query($sql)->result_array();


        return $data ;
    }

    // 관리자의 리뷰 답변 작성하기
    function setreviewanswer($data){
        $crr_reservation_idx =$data["reservation_idx"];
        $crr_reply_content =$data["crr_reply_content"];

        $sql="select count(*) as cnt from   carmore_review_reply  where crr_reservation_idx='$crr_reservation_idx' ";
        $cnt = $this->db->query($sql)->row()->cnt;

        if($cnt >0){
            $sql="update carmore_review_reply set crr_reply_content='$crr_reply_content'  where crr_reservation_idx='$crr_reservation_idx'";
        }else{
            $sql="INSERT INTO carmore_review_reply (  crr_reservation_idx,  crr_reply_content,  crr_register_member,  crr_register_date)
            VALUES  (    '$crr_reservation_idx',    '$crr_reply_content',    '0',    NOW()  );";
        }

        $query = $this->db->query($sql);

        return ;
    }

    //리뷰 블라인드처리하기
    function setreviewblind($data){
        $work =$data["work"];

        if($work=="blind"){
            $status="2";
        }else{
            $status="1";
        }

        $reservation_idx =$data["reservation_idx"];
        $blind_code =$data["blind_code"];
        $blind_codetext =$data["blind_codetext"];

        $sql="update new_review_list set status='$status',blind_code='$blind_code', blind_codetext='$blind_codetext'  where reservation_idx='$reservation_idx'";
        $query = $this->db->query($sql);
        echo $sql;
        exit();
        return ;
    }
}