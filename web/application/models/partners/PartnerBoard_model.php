<?php
/**
 * PartnerBoard_model - 파트너 게시판 관리 db
 */

class PartnerBoard_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    // 파트너 게시판 카운트
    function getPartnerBoardCount($board_code="",$sp="",$sv=""){
        $sql="select count(*) as cnt from  admin_board where board_code='$board_code'  ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    //파트너 게시판 리스트 
    function getPartnerBoardPageList($board_code,$page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select *
            ,(SELECT admin_name FROM admin_member WHERE  admin_email=a.mem_id)AS mem_name
            ,(SELECT admin_name FROM admin_member WHERE  admin_email=a.confirm_id)AS confirm_name 

            from   admin_board a where board_code='$board_code'  ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by board_idx desc limit $start,20";
        $query = $this->db->query($sql);

        return $query->result();
    }

    //파트너 게시판 상세정보
    function getPartnerBoardDetail($board_idx)
    {
        $sql="select *
            ,(SELECT admin_name FROM admin_member WHERE  admin_email=a.mem_id)AS mem_name
            ,(SELECT admin_name FROM admin_member WHERE  admin_email=a.confirm_id)AS confirm_name 
        from admin_board a where board_idx='$board_idx'";

        $query = $this->db->query($sql)->result_array();
        $row = $query[0];
        return $row;
    }

    // 파트너 게시판 등록,수정,삭제
    public function procAdminBoard($emode,$data)
    {
        $return_v="";

        if($emode=="new"){

            $nowtime =date("YmdHis");
            $sql = "INSERT INTO admin_board 	(	content_code, 	board_title,
				board_content, mem_id, 	regdate, 	board_code,board_part
				,startdate , enddate ,content_type,price)
				VALUES('".$data["content_code"]."'
				, 	'".$this->db->escape_str($data["board_title"])."'
				,'".$this->db->escape_str($data["board_content"])."'
				,'".$data["mem_id"]."'
				,'$nowtime'
				,'".$data["board_code"]."'
				,'".$data["board_part"]."'
				,'".$data["startdate"]."'
				,'".$data["enddate"]."'
				,'".$data["content_type"]."'
				,'".$this->db->escape_str($data["price"])."');";

            $this->db->query($sql);
            $return_v="y";

        }else if($emode=="edit"){
            $sql = "update  admin_board set
                  board_title='".$this->db->escape_str($data["board_title"])."'
                  ,board_content='".$this->db->escape_str($data["board_content"])."'
               ,content_type='".$data["content_type"]."'
               ,price='".$this->db->escape_str($data["price"])."'
               ,startdate ='".$data["startdate"]."'
               ,enddate='".$data["enddate"]."'
             where board_idx='".$data["board_idx"]."'";

            $this->db->query($sql);
            $return_v="y";
        }else if($emode=="del"){



            $sql = "delete from admin_board where board_idx='".$this->db->escape_str($data["board_idx"])."'";
            $this->db->query($sql);

            $sql = "delete from admin_comment where board_idx='".$this->db->escape_str($data["board_idx"])."'";
            $this->db->query($sql);
            $return_v="y";
        }else if($emode=="agree_yn"){

            if($data["isConfirm"]=="1"){
                $agree_yn="y";
            }else{
                $agree_yn="x";
            }
            //$login_id
            $arr_confirmlist=$data["confirm_list"];
            $confirm_id=$data["confirm_id"];

            for($i=0;$i < sizeof($arr_confirmlist); $i++)
            {
                $board_idx = $arr_confirmlist[$i];
                $sql="update admin_board set agree_yn='$agree_yn',confirm_id='$confirm_id'   where board_idx='$board_idx' ";
                $this->db->query($sql);
            }
        }
        return "y";
    }


    //파트너스 공지사항 게시판 카운트
    function getPartnerNoticeCount($sp="",$sv=""){
        $sql="select count(*) as cnt from  partners_notice_board where 1=1 ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    //파트너스 공지사항 게시판 리스트
    function getPartnerNoticePageList($page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select *
             
            from   partners_notice_board a where 1=1";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by pnb_idx desc limit $start,20";
        $query = $this->db->query($sql);

        return $query->result();
    }


    //파트너스 공지사항 게시판 상세
    function getPartnerNoticeDetail($pnb_idx)
    {
        $sql="select *   from partners_notice_board a where pnb_idx='$pnb_idx'";

        $query = $this->db->query($sql)->result_array();
        $row = $query[0];
        return $row;
    }

    // 파트너스공지사항 수정,등록,삭제
    public function procPartnerNotice($data)
    {
        $return_v="";
        $emode = $data["emode"];

        if($emode=="new"){
            $sql = "INSERT INTO partners_notice_board 	(	pnb_title,pnb_content,pnb_regdate,pnb_register_am_id,pnb_state)
				VALUES( '".$this->db->escape_str($data["pnb_title"])."'
				,'".$this->db->escape_str($data["pnb_content"])."'
				,NOW() 
				,'".$this->db->escape_str($data["pnb_register_am_id"])."'
				,'1');";

            $this->db->query($sql);
            $return_v="y";

        }else if($emode=="edit"){
            $sql = "update  partners_notice_board set
                  pnb_title='".$this->db->escape_str($data["pnb_title"])."'
                  ,pnb_content='".$this->db->escape_str($data["pnb_content"])."' 
             where pnb_idx='".$data["pnb_idx"]."'";

            $this->db->query($sql);
            $return_v="y";
        }else if($emode=="del"){

            $sql = "update partners_notice_board set pnb_state='0' where pnb_idx='".$this->db->escape_str($data["pnb_idx"])."'";
            $this->db->query($sql);

            $return_v="y";
        }

        return "y";
    }


}