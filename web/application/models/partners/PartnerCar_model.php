<?php
/**
 * PartnerCar_model - 예약정보 로그 관련 db (사용안함 X)
 */

class PartnerCar_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function getPartnerCarCount($sp="",$sv=""){
        $sql="select count(*) as cnt from  new_rentCompany where 1=1 ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }


    function getPartnerCarPageList($page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select *  from   new_rentCompany a where 1=1 ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by regdate desc limit $start,20";
        $query = $this->db->query($sql);

        return $query->result();
    }



}