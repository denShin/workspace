<?php
/**
 * API 자차관련
 *
 * @author Den <den@tm2.kr> 19.06.26
 *
 * @property CI_Loader load
 * @property CI_DB_pdo_mysql_driver $db
 *
 * @property Workspace_common_func $workspace_common_func
 */

class Api_cdw_model extends WS_Model {
    private $key_to_database = [
        'branch'       => 'wcm_waab_idx',
        'api'          => 'wcm_api',
        'apiIdx'       => 'wcm_api_idx',
        'category'     => 'wcm_cdw_category',
        'compensation' => 'wcm_compensation',
        'deductible1'  => 'wcm_deductible1',
        'deductible2'  => 'wcm_deductible2',
        'age'          => 'wcm_age',
        'career'       => 'wcm_career',
        'description'  => 'wcm_description'
    ];
    private $now;

    public function __construct()
    {
        parent::__construct();

        $this->now = date('Y-m-d H:i:s', strtotime('+9 hours'));
        $this->load->library('Workspace_common_func');
    }

    public function get_cdw_mapping($api, $waab_idx)
    {
        $mapping_inventory = new stdClass();

        $query = "
        SELECT
            wcm_idx, wcm_api_idx, wcm_cdw_category, wcm_compensation, wcm_deductible1, wcm_deductible2,
            wcm_age, wcm_career, wcm_description
        FROM workspace_cdw_mapping WHERE wcm_api = ? AND wcm_waab_idx = ?";

        $sql = $this->slave->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$api, $waab_idx]);
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            $std_class = new stdClass();

            $api_idx = htmlspecialchars($stmt['wcm_api_idx']);

            $std_class->wcmIdx       = $stmt['wcm_idx'];
            $std_class->apiIdx       = $api_idx;
            $std_class->category     = htmlspecialchars($stmt['wcm_cdw_category']);
            $std_class->compensation = htmlspecialchars($stmt['wcm_compensation']);
            $std_class->deductible1  = htmlspecialchars($stmt['wcm_deductible1']);
            $std_class->deductible2  = htmlspecialchars($stmt['wcm_deductible2']);
            $std_class->age          = htmlspecialchars($stmt['wcm_age']);
            $std_class->career       = htmlspecialchars($stmt['wcm_career']);
            $std_class->description  = htmlspecialchars($stmt['wcm_description']);

            $mapping_inventory->$api_idx = $std_class;

        }

        return $mapping_inventory;

    }

    public function create_cdw_mapping($obj)
    {
        $obj['registerDate'] = $this->now;

        $query = "
        INSERT INTO workspace_cdw_mapping
            (wcm_waab_idx, wcm_api, wcm_api_idx, wcm_cdw_category, wcm_compensation, wcm_deductible1, wcm_deductible2,
            wcm_age, wcm_career, wcm_description, wcm_register_date)
        VALUE
            (:branch, :api, :apiIdx, :category, :compensation, :deductible1, :deductible2,
            :age, :career, :description, :registerDate)";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute($obj);

        return [
            'result' => true,
            'wcmIdx' => $this->db->conn_id->lastInsertId()
        ];

    }

    public function update_cdw_mapping($api, $api_idx, $obj)
    {
        $is_it_first = true;
        $set         = "";

        foreach ($obj as $key => $obj_value)
        {
            if (isset($this->key_to_database[$key]))
            {
                if ($is_it_first)
                    $is_it_first = false;
                else
                    $set .= ", ";

                $set .= $this->key_to_database[$key]."='".$obj_value."'";

            }

        }

        if (isset($obj['branch']))
        {
            $query = "UPDATE workspace_cdw_mapping SET ".$set." WHERE wcm_api =? AND wcm_api_idx = ? AND wcm_waab_idx = ?";

            $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $sql->execute([$api, $api_idx, $obj['branch']]);

        }

    }

    public function unmatching_cdw_mapping_by_api_key($waab_idx, $inventory)
    {
        if (!empty($inventory)) {
            $is_it_first = true;
            $key_string = "(";
            foreach ($inventory as $key) {
                if ($is_it_first)
                    $is_it_first = false;
                else
                    $key_string .= " OR ";

                $key_string .= " wcm_api_idx='".$key."' ";
            }
            $key_string .= ")";

            if ($key_string !== '()') {
                $query = "DELETE FROM workspace_cdw_mapping WHERE wcm_waab_idx=? AND ".$key_string;

                $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
                $sql->execute([$waab_idx]);

            }

        }

    }

    public function unmatching_cdw_mapping_by_wcm_idx($waab_idx, $wcm_idx)
    {
        if (!empty($wcm_idx)) {
            $query = "DELETE FROM workspace_cdw_mapping WHERE wcm_waab_idx=? AND wcm_idx=?";

            $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $sql->execute([$waab_idx, $wcm_idx]);

        }
    }

}