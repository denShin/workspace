<?php
/**
 * Created by PhpStorm.
 * User: teamo2_server
 * Date: 2019-04-15
 * Time: 19:44
 *
 * @property Workspace_common_func  $workspace_common_func
 */

class Api_affiliate_branch_biztalk_model extends WS_Model {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Workspace_common_func');
    }

    /**
     * API 업체 지점 알림톡 등록
     *
     * @param $bind_param array
     *
     * @return int
     */
    public function insert_biztalk_members($bind_param)
    {
        $query = "
        INSERT INTO workspace_api_affiliate_branch_biztalk
            (waabb_waa_idx, waabb_waab_idx, waabb_name, waabb_tel,
            waabb_register_am_idx, waabb_register_date, waabb_delete_am_idx, waabb_delete_date, waabb_state)
        VALUES
            (:waaIdx, :waabIdx, :name, :tel,
            :amIdx, :regDate, 0, '0000-00-00 00:00:00', 1)";


        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute($bind_param);

        return $this->db->conn_id->lastInsertId();

    }

    /**
     * 알림톡 명단 내용 수정
     *
     * @param $bind_param array
     */
    public function update_biztalk_members($bind_param)
    {
        $query = "
        UPDATE workspace_api_affiliate_branch_biztalk
        SET waabb_name=:name, waabb_tel=:tel
        WHERE waabb_idx=:waabbIdx";
        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute($bind_param);
    }

    /**
     * 알림톡 명단 삭제
     *
     * @param $idx int
     */
    public function delete_biztalk_members($idx)
    {
        $query = "UPDATE workspace_api_affiliate_branch_biztalk SET waabb_state=0 WHERE waabb_idx=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$idx]);
    }

    /**
     * 업체의 알림톡 명단 받아오기
     *
     * @param $waab_idx int
     *
     * @return array
     */
    public function load_api_affiliate_branch_biztalk_information($waab_idx)
    {
        $inventory = [];

        $query = "SELECT waabb_idx, waabb_name, waabb_tel FROM workspace_api_affiliate_branch_biztalk WHERE waabb_waab_idx=? AND waabb_state=1";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$waab_idx]);
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            array_push($inventory, [
                'idx'  => $stmt['waabb_idx'],
                'name' => $stmt['waabb_name'],
                'tel'  => $this->workspace_common_func->format_tel($stmt['waabb_tel'])
            ]);

        }

        return $inventory;

    }
}
