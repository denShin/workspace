<?php
/**
 * API 종합보험 관련 모델
 *
 * Created by PhpStorm.
 * User: teamo2_server
 * Date: 2019-04-18
 * Time: 17:14
 */

class Api_compensation_model extends WS_Model {

    private $key_to_database = [
        'compensationName' => 'wac_name',
        'compensationType' => 'wac_type',
        'compensation1'    => 'wac_compensation1',
        'compensation2'    => 'wac_compensation2',
        'compensation3'    => 'wac_compensation3',
        'compensation4'    => 'wac_compensation4',
        'compensation5'    => 'wac_compensation5',
        'compensation6'    => 'wac_compensation6',
        'fee1'             => 'wac_fee1',
        'fee2'             => 'wac_fee2',
        'fee3'             => 'wac_fee3',
        'fee4'             => 'wac_fee4',
        'fee5'             => 'wac_fee5',
        'fee6'             => 'wac_fee6',
    ];

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * 종합보험 리스트 가져오기
     *
     * @return array
     */
    public function load_compensation_inventory()
    {
        $inventory = [];

        $query = "SELECT wac_idx, wac_name FROM workspace_api_compensation WHERE wac_state=1 ORDER BY wac_idx ASC";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute();
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            array_push($inventory, [
                'idx'  => $stmt['wac_idx'],
                'name' => $stmt['wac_name']
            ]);

        }

        return $inventory;

    }

    /**
     * 종합보험 정보 가져오기
     *
     * @param $wac_idx int
     *
     * @return array
     */
    public function load_compensation_information($wac_idx)
    {
        $query = "
        SELECT
            wac_idx, wac_name, wac_type,
            wac_compensation1, wac_compensation2, wac_compensation3, wac_compensation4, wac_compensation5, wac_compensation6,
            wac_fee1, wac_fee2, wac_fee3, wac_fee4, wac_fee5, wac_fee6
        FROM workspace_api_compensation WHERE wac_idx=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$wac_idx]);
        if ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            return [
                'result' => 1,
                'info'   => [
                    'idx'           => $stmt['wac_idx'],
                    'name'          => $stmt['wac_name'],
                    'type'          => $stmt['wac_type'],
                    'compensation1' => $stmt['wac_compensation1'],
                    'compensation2' => $stmt['wac_compensation2'],
                    'compensation3' => $stmt['wac_compensation3'],
                    'compensation4' => $stmt['wac_compensation4'],
                    'compensation5' => $stmt['wac_compensation5'],
                    'compensation6' => $stmt['wac_compensation6'],
                    'fee1' => $stmt['wac_fee1'],
                    'fee2' => $stmt['wac_fee2'],
                    'fee3' => $stmt['wac_fee3'],
                    'fee4' => $stmt['wac_fee4'],
                    'fee5' => $stmt['wac_fee5'],
                    'fee6' => $stmt['wac_fee6']
                ]
            ];

        }
        else
            return ['result' => 2];
    }

    /**
     * 종합보험 신규 추가
     *
     * @param $type       int 종합보험 타입
     * @param $bind_param array
     *
     * @return int
     */
    public function insert_compensation($type, $bind_param)
    {
        if ($type === 1)
        {
            $query = "
            INSERT INTO workspace_api_compensation
                (wac_name, wac_type,
                wac_register_am_idx, wac_register_date)
            VALUE
                (?, ?, ?, ?)";

            $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $sql->execute([$bind_param['compensationName'], $bind_param['compensationType'], $bind_param['amIdx'], $bind_param['now']]);

            return $this->db->conn_id->lastInsertId();
        }
        else
        {
            $query = "
            INSERT INTO workspace_api_compensation
                (wac_name, wac_type,
                wac_compensation1, wac_compensation2, wac_compensation3,
                wac_compensation4, wac_compensation5, wac_compensation6,
                wac_fee1, wac_fee2, wac_fee3, wac_fee4, wac_fee5, wac_fee6,
                wac_register_am_idx, wac_register_date)
            VALUE
                (:compensationName, :compensationType,
                :compensation1, :compensation2, :compensation3,
                :compensation4, :compensation5, :compensation6,
                :fee1, :fee2, :fee3, :fee4, :fee5, :fee6,
                :amIdx, :now)";

            $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $sql->execute($bind_param);

            return $this->db->conn_id->lastInsertId();

        }

    }

    /**
     * 종합보험 정보 수정
     * @param $wac_idx int    종합보험 아이디값
     * @param $obj     object
     */
    public function update_compensation($wac_idx, $obj)
    {
        $is_it_first = true;
        $set         = "";

        foreach ($obj as $key => $obj_value)
        {

            if (isset($this->key_to_database[$key]))
            {
                if ($is_it_first)
                    $is_it_first = false;
                else
                    $set .= ", ";

                $set .= $this->key_to_database[$key]."='".$obj_value."'";

            }

        }

        $query = "UPDATE workspace_api_compensation SET ".$set." WHERE wac_idx=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$wac_idx]);
    }

    /**
     * 종합보험 정보 삭제
     *
     * @param $wac_idx int 종합보험 아이디값
     */
    public function delete_compensation($wac_idx)
    {
        $query = "UPDATE workspace_api_compensation SET wac_state=0 WHERE wac_idx=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$wac_idx]);
    }

}
