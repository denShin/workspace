<?php
/**
 * API 업체 지점 관련 모델
 *
 * @author Den <den@tm2.kr> 19.06.11
 *
 * @property CI_Loader load
 * @property CI_DB_pdo_mysql_driver $db
 *
 * @property Workspace_common_func $workspace_common_func
 */

class Api_affiliate_branch_model extends WS_Model {

    private $key_to_database = [
        'branchName'    => 'waab_name',
        'api'           => 'waab_api',
        'info1'         => 'waab_api_info1',
        'info2'         => 'waab_api_info2',
        'info3'         => 'waab_api_info3',
        'info4'         => 'waab_api_info4',
        'mainAddress'   => 'waab_main_address',
        'subAddress'    => 'waab_sub_address',
        'latitude'      => 'waab_latitude',
        'longitude'     => 'waab_longitude',
        'tel'           => 'waab_tel',
        'maxReservDate' => 'waab_max_reserv',
        'openTime'      => 'waab_open_time',
        'closeTime'     => 'waab_close_time',
        'saleStart'     => 'waab_sale_start_hour',
        'stName'        => 'waab_shuttle_name',
        'stArea'        => 'waab_shuttle_area',
        'stAreaNumber'  => 'waab_shuttle_area_num',
        'stRace'        => 'waab_shuttle_race',
        'stSpendTime'   => 'waab_shuttle_spend',
        'intro'         => 'waab_intro_msg',
        'uniqueness'    => 'waab_uniqueness',
        'enterDate'     => 'waab_enter_date',
        'serviceString'  => 'waab_service',
        'deliveryString' => 'waab_delivery',
        'specialLocation' => 'waab_special_location',
        'logo'          => 'waab_logo'
    ];

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Workspace_common_func');
    }

    /**
     * API 업체 지점 등록
     *
     * @param $bind_param array
     *
     * @return int
     */
    public function insert_api_affiliate_branch($bind_param)
    {
        $query = "
        INSERT INTO workspace_api_affiliate_branch
            (waab_waa_idx, waab_name, waab_api, waab_api_info1, waab_api_info2, waab_api_info3,
            waab_main_address, waab_sub_address, waab_latitude, waab_longitude,
            waab_tel, waab_max_reserv, waab_open_time, waab_close_time, waab_sale_start_hour,
            waab_shuttle_name, waab_shuttle_area, waab_shuttle_area_num, waab_shuttle_race, waab_shuttle_spend,
            waab_intro_msg, waab_uniqueness, waab_enter_date, waab_service, waab_delivery, waab_special_location,
            waab_register_am_idx, waab_register_date, waab_delete_am_idx, waab_delete_date, waab_state)
        VALUES
            (:waaIdx, :name, :api, :apiInfo1, :apiInfo2, :apiInfo3,
            :mainAddress, :subAddress, :latitude, :longitude,
            :tel, :maxReserv, :openTime, :closeTime, :saleStart, 
            :shuttleName, :shuttleArea, :shuttleAreaNum, :shuttleRace, :shuttleSpend,
            :intro, :unique, :enterDate, :serviceString, :deliveryString, :specialLocation,
            :regAmIdx, :regDate, 0, '0000-00-00 00:00:00', 1)";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute($bind_param);

        return $this->db->conn_id->lastInsertId();

    }

    /**
     * API 업체 수정
     *
     * @param $waab_idx int
     * @param $obj      array
     */
    public function update_api_affiliate_branch($waab_idx, $obj)
    {
        $is_it_first = true;
        $set         = "";

        foreach ($obj as $key => $obj_value)
        {
            if (gettype($obj[$key]) === "array")
            {
                foreach ($obj_value as $obj_value_key => $val)
                {
                    if (isset($this->key_to_database[$obj_value_key]))
                    {
                        if ($is_it_first)
                            $is_it_first = false;
                        else
                            $set .= ", ";

                        $set .= $this->key_to_database[$obj_value_key]."='".$val."'";

                    }

                }

            }
            elseif ($key === "isLogoDelete")
            {
                if ($is_it_first)
                    $is_it_first = false;
                else
                    $set .= ", ";

                $set .= "waab_logo=''";
            }
            else
            {
                if (isset($this->key_to_database[$key]))
                {
                    if ($is_it_first)
                        $is_it_first = false;
                    else
                        $set .= ", ";

                    $set .= $this->key_to_database[$key]."='".html_entity_decode($obj_value)."'";

                }

            }

        }

        $query = "UPDATE workspace_api_affiliate_branch SET ".$set." WHERE waab_idx=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$waab_idx]);
    }

    /**
     * 지점목록 가져오기
     *
     * @return array
     */
    public function load_api_affiliate_branch()
    {
        $inventory = [];

        $query = "
        SELECT
            waa_idx, waa_name,
            waab_idx, IFNULL(waab_name, '') waab_name, waab_api, waab_api_info1, waab_api_info2, waab_api_info3, waab_api_info4
        FROM
            workspace_api_affiliate waa INNER JOIN
            workspace_api_affiliate_branch waab
            ON
                waa_idx=waab_waa_idx
        WHERE
            waa_state=1 AND waab_state=1";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute();
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            array_push($inventory, [
                'waaIdx'       => $stmt['waa_idx'],
                'waaName'      => $stmt['waa_name'],
                'waabIdx'      => $stmt['waab_idx'],
                'waabName'     => $stmt['waab_name'],
                'waabApi'      => $stmt['waab_api'],
                'waabApiInfo1' => $stmt['waab_api_info1'],
                'waabApiInfo2' => $stmt['waab_api_info2'],
                'waabApiInfo3' => $stmt['waab_api_info3'],
                'waabApiInfo4' => $stmt['waab_api_info4']
            ]);

        }

        return $inventory;
    }

    /**
     * 지점정보 가져오기
     *
     * @param $waab_idx int 지점 아이디값
     *
     * @return array
     */
    public function load_api_affiliate_branch_information($waab_idx)
    {
        $query = "
        SELECT
            waa_name,
            waab_name, waab_api, waab_api_info1, waab_api_info2, waab_api_info3, waab_api_info4,
            waab_main_address, waab_sub_address, waab_tel, waab_max_reserv, waab_open_time, waab_close_time, waab_sale_start_hour,
            waab_shuttle_name, waab_shuttle_area, waab_shuttle_area_num, waab_shuttle_race, waab_shuttle_spend, waab_tel050,
            waab_intro_msg, waab_uniqueness, waab_enter_date, waab_logo, waab_service, waab_delivery, waab_special_location,
            IFNULL(
                (SELECT fileSize FROM admin_uploadfile
                 WHERE
                     content_code=CONCAT('aplg_', :idx) AND fileuptype='api_logo' AND board_code='api' AND fileSaveName=waab_logo), '') AS file_size
        FROM
            workspace_api_affiliate waa INNER JOIN
            workspace_api_affiliate_branch waab
            ON
                waa_idx=waab_waa_idx
        WHERE
            waab_idx=:idx";
        
        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->bindParam(':idx', $waab_idx);
        $sql->execute();
        if ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            $logo = $stmt['waab_logo'];
            if ($logo !== "")
                $logo = "https://s3.ap-northeast-2.amazonaws.com/carmoreweb/partners/aplg_".$waab_idx."/".$logo;

            return [
                'result' => 1,
                'info'   => [
                    'waaName'        => $stmt['waa_name'],
                    'waabName'       => $stmt['waab_name'],
                    'apiInfo'        => [
                        'api'   => $stmt['waab_api'],
                        'info1' => $stmt['waab_api_info1'],
                        'info2' => $stmt['waab_api_info2'],
                        'info3' => $stmt['waab_api_info3'],
                        'info4' => $stmt['waab_api_info4']
                    ],
                    'mainAddress'    => $stmt['waab_main_address'],
                    'subAddress'     => $stmt['waab_sub_address'],
                    'tel'            => $stmt['waab_tel'],
                    'tel050'         => $stmt['waab_tel050'],
                    'maxReserv'      => $stmt['waab_max_reserv'],
                    'openTime'       => $stmt['waab_open_time'],
                    'closeTime'      => $stmt['waab_close_time'],
                    'saleStartHour'  => $stmt['waab_sale_start_hour'],
                    'shuttleName'    => $stmt['waab_shuttle_name'],
                    'shuttleArea'    => $stmt['waab_shuttle_area'],
                    'shuttleAreaNum' => $stmt['waab_shuttle_area_num'],
                    'shuttleRace'    => $stmt['waab_shuttle_race'],
                    'shuttleSpend'   => $stmt['waab_shuttle_spend'],
                    'intro'          => $stmt['waab_intro_msg'],
                    'uniqueness'     => $stmt['waab_uniqueness'],
                    'enterDate'      => $stmt['waab_enter_date'],
                    'serviceString'  => $stmt['waab_service'],
                    'deliveryString' => $stmt['waab_delivery'],
                    'specialLocation' => $stmt['waab_special_location'],
                    'waabLogoInfo'   => [
                        'logo' => $logo,
                        'size' => $stmt['file_size']
                    ],
                    'waabSubImgs'    => $this->load_api_affiliate_sub_imgs($waab_idx)
                ]
            ];

        }
        else
            return ['result' => 2];
    }

    /**
     * 지점삭제
     *
     * @param $waab_idx int
     * @param $am_idx   int
     * @param $now      string
     */
    public function delete_api_affiliate_branch($waab_idx, $am_idx, $now)
    {
        $query = "UPDATE workspace_api_affiliate_branch SET waab_state=0, waab_delete_am_idx=?, waab_delete_date=? WHERE waab_idx=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$am_idx, $now, $waab_idx]);
    }

    /**
     * 050번호 업데이트 하기
     *
     * @param $waab_idx int
     * @param $tel050   string
     */
    public function update_tel050($waab_idx, $tel050)
    {
        $waab_idx = $this->workspace_common_func->check_sql_injection($waab_idx, TRUE);
        $tel050   = $this->workspace_common_func->check_sql_injection($tel050, TRUE);

        $query = "UPDATE workspace_api_affiliate_branch SET waab_tel050=? WHERE waab_idx=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$tel050, $waab_idx]);
    }

    /**
     * 업체의 서브이미지들 가져오기
     *
     * @param $waab_idx
     *
     * @return array
     */
    private function load_api_affiliate_sub_imgs($waab_idx)
    {
        $ret_arr = [];

        $query = "SELECT fileSaveName, fileSize FROM admin_uploadfile WHERE content_code='apsb_".$waab_idx."' AND fileuptype='api_".$waab_idx."' AND publish_yn='y'";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute();
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            $obj = new stdClass();

            $file_name = $stmt['fileSaveName'];

            $obj->name = $file_name;
            $obj->size = $stmt['fileSize'];
            $obj->url  = "https://s3.ap-northeast-2.amazonaws.com/carmoreweb/partners/apsb_".$waab_idx."/".$file_name;

            array_push($ret_arr, $obj);

        }

        return $ret_arr;

    }
}
