<?php
/**
 * API 업체 관련 모델
 *
 * Created by PhpStorm.
 * User: teamo2_server
 * Date: 2019-04-11
 * Time: 14:59
 */

class Api_affiliate_model extends WS_Model {

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * 검색 조건에 따른 총 API 제휴 업체수
     *
     * @param string $search_value 검색텍스트
     *
     * @return int 총제휴업체수
     */
    public function load_api_affiliate_total_count($search_value = "")
    {
        $query = "SELECT count(*) cnt FROM workspace_api_affiliate WHERE waa_state=1";
        if ($search_value !== "")
            $query .= " AND waa_name LIKE '%".$search_value."%'";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute();
        if ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
            return (int)$stmt['cnt'];
        else
            return 0;

    }

    /**
     * 검색 조건에 따른 API 제휴 업체 목록
     *
     * @param int    $page         현재 페이지
     * @param int    $max_row      한 페이지당 최대 로우수
     * @param string $search_value 검색텍스트
     *
     *
     * @return array
     */
    public function load_api_affiliate_inventory($page, $max_row, $search_value = "")
    {
        $inventory = [];

        if (!$page)
            $page = 1;

        $limit_start = ($page-1) * $max_row;

        $query = "
        SELECT waa_idx, waab_idx, waa_name, waa_register_date, waab_api
        FROM
            workspace_api_affiliate waa INNER JOIN
            (SELECT waab_waa_idx, waab_idx, waab_api FROM workspace_api_affiliate_branch WHERE waab_state=1 GROUP BY waab_waa_idx) waab
            ON
                waab_waa_idx=waa_idx
        WHERE waa_state=1";

        if ($search_value !== "")
            $query .= " AND waa_name LIKE '%".$search_value."%'";

        $query .= "
        ORDER BY waa_idx DESC
        LIMIT ".$limit_start.", ".$max_row;

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute();
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            array_push($inventory, [
                'waaIdx'     => $stmt['waa_idx'],
                'repWaabIdx' => $stmt['waab_idx'],
                'waaName'    => $stmt['waa_name'],
                'waaRegDate' => $stmt['waa_register_date'],
                'api'        => (int)$stmt['waab_api']
            ]);

        }

        return $inventory;

    }

    /**
     * API 업체 등록
     *
     * @param $bind_param array
     *
     * @return int
     */
    public function insert_api_affiliate($bind_param)
    {
        $query = "
        INSERT INTO workspace_api_affiliate
            (waa_name, waa_register_date, waa_state)
        VALUES
            (?, ?, ?)";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute($bind_param);

        return $this->db->conn_id->lastInsertId();

    }

    /**
     * API 업체 수정
     *
     * @param $affi_idx  int
     * @param $affi_name string
     */
    public function update_api_affiliate($affi_idx, $affi_name)
    {
        $query = "UPDATE workspace_api_affiliate SET waa_name=? WHERE waa_idx=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$affi_name, $affi_idx]);
    }

    /**
     * 회사의 지점 목록가져오기
     *
     * @param int    $waa_idx   회사 아이디값
     * @param int    $type      지점 가져올 타입 (0:전체, 1:$added_val 를(브랜치 값) 제외하고
     * @param string $added_val
     *
     * @return array
     */
    public function load_api_affiliate_branch_inventory($waa_idx, $type = 0, $added_val = '')
    {
        $inventory = [];

        $added_where = ($type === 1)? " AND waab_idx <> ".$added_val : "";

        $query = "SELECT waab_idx, waab_name FROM workspace_api_affiliate_branch WHERE waab_waa_idx=? AND waab_state=1 ".$added_where;

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$waa_idx]);
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            array_push($inventory, [
                'waabIdx'  => $stmt['waab_idx'],
                'waabName' => $stmt['waab_name']
            ]);
        }

        return $inventory;
    }

    /**
     * 회사의 지점 목록 수
     *
     * @param $waa_idx int
     *
     * @return int
     */
    public function count_api_affiliate_branch($waa_idx)
    {
        $query = "SELECT count(*) cnt FROM workspace_api_affiliate_branch WHERE waab_waa_idx=? AND waab_state=1";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$waa_idx]);
        if ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
            return (int)$stmt['cnt'];
        else
            return 0;
    }

    /**
     * 회사 삭제
     *
     * @param $waa_idx int
     */
    public function delete_api_affiliate($waa_idx)
    {
        $query = "UPDATE workspace_api_affiliate SET waa_state=0 WHERE waa_idx=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$waa_idx]);
    }
}