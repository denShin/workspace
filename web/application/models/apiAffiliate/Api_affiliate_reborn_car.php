<?php
/**
 * Created by PhpStorm.
 * User: teamo2_server
 * Date: 2019-04-22
 * Time: 16:26
 */

class Api_affiliate_reborn_car extends WS_Model {
    private $key_to_database = [
        'carName'        => 'wrci_car_name',
        'cartypeCode'    => 'wrci_car_type_code',
        'cartypeName'    => 'wrci_car_type_name',
        'paClafCode'     => 'wrci_passenger_classification_code',
        'paClafName'     => 'wrci_passenger_classification_name',
        'brandCode'      => 'wrci_brand_code',
        'brandName'      => 'wrci_brand_name',
        'fuelCode'       => 'wrci_fuel_code',
        'fuelName'       => 'wrci_fuel_name',
        'passenger'      => 'wrci_passengers',
        'optionSafeCode' => 'wrci_option_safe_code',
        'optionSafeName' => 'wrci_option_safe_name',
        'optionCnCode'   => 'wrci_option_convenience_code',
        'optionCnName'   => 'wrci_option_convenience_name',
        'optionSnCode'   => 'wrci_option_sound_code',
        'optionSnName'   => 'wrci_option_sound_name',
        'yearMin'        => 'wrci_year_min',
        'yearMax'        => 'wrci_year_max',
        'yearStr'        => 'wrci_year_string',
        'licenseType'    => 'wrci_license_type',
        'model'          => 'wrci_cimaster_idx',
        'compensation'   => 'wrci_wac_idx'
    ];

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * 차량 있는지 체크
     *
     * @param $waab_idx int    지점아이디값
     * @param $car_code string 차종코드
     *
     * @return bool
     */
    public function chk_has_car($waab_idx, $car_code)
    {
        $query = "SELECT wrci_idx FROM workspace_reborn_car_info WHERE wrci_waab_idx=? AND wrci_car_code=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$waab_idx, $car_code]);
        if ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
            return (int)$stmt['wrci_idx'];
        else
            return false;
    }

    /**
     * 차량정보 넣기
     *
     * @param $bind_param
     */
    public function insert_car_info($bind_param)
    {
        $query = "
        INSERT INTO workspace_reborn_car_info
            (wrci_waa_idx, wrci_waab_idx, wrci_car_code, wrci_car_name, wrci_car_type_code, wrci_car_type_name,
            wrci_passenger_classification_code, wrci_passenger_classification_name, wrci_brand_code, wrci_brand_name,
            wrci_fuel_code, wrci_fuel_name, wrci_passengers,
            wrci_option_safe_code, wrci_option_safe_name, wrci_option_convenience_code, wrci_option_convenience_name, wrci_option_sound_code, wrci_option_sound_name,
            wrci_year_min, wrci_year_max, wrci_year_string, wrci_license_type,
            wrci_cimaster_idx, wrci_wac_idx)
        VALUE
            (:waaIdx, :waabIdx, :carCode, :carName, :cartypeCode, :cartypeName,
            :paClafCode, :paClafName, :brandCode, :brandName,
            :fuelCode, :fuelName, :passenger,
            :optionSafeCode, :optionSafeName, :optionCnCode, :optionCnName, :optionSnCode, :optionSnName,
            :yearMin, :yearMax, :yearStr, :licenseType,
            0, 0)";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute($bind_param);
    }

    /**
     * 매칭정보 수정하기
     *
     * @param $wrci_idx int    INT 아이디값
     * @param $obj      array
     */
    public function update_car_info($wrci_idx, $obj)
    {
        $is_it_first = true;
        $set         = "";

        foreach ($obj as $key => $value)
        {
            if (isset($this->key_to_database[$key]))
            {
                if ($is_it_first)
                    $is_it_first = false;
                else
                    $set .= ", ";

                $set .= $this->key_to_database[$key]."='".$value."'";

            }
        }

        $query = "UPDATE workspace_reborn_car_info SET ".$set." WHERE wrci_idx=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$wrci_idx]);

    }

    /**
     * 리본업체의 차량목록 받아오기
     *
     * @param $waab_idx int
     *
     * @return array
     */
    public function get_car_inventory($waab_idx)
    {
        $inventory = [];

        $query = "
        SELECT
            wrci_idx, wrci_car_code, wrci_car_name, wrci_cimaster_idx, wrci_wac_idx,
            IFNULL(cimaster.carinfomst_type, '') carinfomst_type, IFNULL(cimaster.carinfomst_model, '') carinfomst_model,
            IFNULL((SELECT wac_name FROM workspace_api_compensation WHERE wac_idx=wrci_wac_idx), '') wac_name
        FROM
            workspace_reborn_car_info wrci LEFT OUTER JOIN
            carinfo_master cimaster
            ON
                wrci_cimaster_idx=cimaster.carinfo_idx
        WHERE wrci_waab_idx=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$waab_idx]);
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            array_push($inventory, [
                'idx'             => $stmt['wrci_idx'],
                'carCode'         => $stmt['wrci_car_code'],
                'carName'         => $stmt['wrci_car_name'],
                'cimasterIdx'     => $stmt['wrci_cimaster_idx'],
                'cimasterModel'   => $stmt['carinfomst_model'],
                'cimasterCarType' => $stmt['carinfomst_type'],
                'wacIdx'          => $stmt['wrci_wac_idx'],
                'wacName'         => $stmt['wac_name']
            ]);

        }


        return $inventory;
    }
}
