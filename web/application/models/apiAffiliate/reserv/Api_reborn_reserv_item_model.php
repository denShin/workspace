<?php
/**
 * 그림 예약정보
 *
 * @property CI_Loader load
 * @property CI_DB_pdo_mysql_driver $db
 *
 * @property Workspace_common_func  $workspace_common_func
 *
 * @property Api_affiliate_branch_biztalk_model $api_affiliate_branch_biztalk_model
 */

class Api_reborn_reserv_item_model extends WS_Model {
    private $now;
    private $keys_to_database = [
        'amIdx'           => 'wari_recent_update_am_idx',
        'driverName'      => 'wari_driver_name',
        'driverPhone'     => 'wari_driver_phone',
        'carCode'         => 'wari_car_code',
        'carName'         => 'wari_car_name',
        'rentalStart'     => 'wari_rental_start',
        'rentalEnd'       => 'wari_rental_end',
        'cdwName'         => 'wari_cdw_name',
        'memo'            => 'wari_memo',
        'state'           => 'wari_state'
    ];

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Workspace_common_func');

        $this->load->model('apiAffiliate/api_affiliate_branch_biztalk_model');

        $this->now = date('Y-m-d H:i:s', strtotime('+9 hours'));
    }

    /**
     * 리본 예약정보 가져오기
     *
     * @param $wari_idx int
     *
     * @return stdClass
     */
    public function read_reserv_item($wari_idx)
    {
        $return_std = new stdClass();

        $query = "
        SELECT
            TRIM(CONCAT(waa_name, ' ', IFNULL(waab_name, ''))) affi_name, TRIM(CONCAT(waab_main_address, ' ', IFNULL(waab_sub_address, ''))) affi_address,
            waa_idx, waab_idx, waab_api,
            waab_tel, waab_open_time, waab_close_time,
               
            wari_reserv_idx, wari_api_reserv_code, wari_rental_start, wari_rental_end, wari_compensation_age, wari_car_name, wari_car_age, wari_car_fuel,
            wari_driver_name, wari_driver_phone, wari_driver_birthday, wari_driver_gender,
            wari_pickup_address, wari_return_address,
            wari_principal, wari_payment, wari_rental_cost, wari_cdw_name, wari_cdw_cost, wari_deliv_cost,
            wari_use_coupon_idx, wari_use_coupon_value, wari_use_point, wari_memo, wari_state,
               
            wac_name, wac_type,
            wac_compensation1, wac_fee1, wac_compensation2, wac_fee2, wac_compensation3, wac_fee3,
            wac_compensation4, wac_fee4, wac_compensation5, wac_fee5, wac_compensation6, wac_fee6
            
        FROM
            workspace_api_reservation_inventory wari INNER JOIN
            workspace_api_affiliate waa INNER JOIN
            workspace_api_affiliate_branch waab INNER JOIN
            workspace_api_compensation wac
            ON
                wari_waa_idx=waa_idx AND wari_waab_idx=waab_idx AND waab_waa_idx=waa_idx AND wari_wac_idx=wac_idx
        WHERE
            wari_idx=?";

        $sql = $this->slave->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$wari_idx]);
        if ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            $std = new stdClass();

            $affi_info   = new stdClass();
            $rental_info = new stdClass();
            $wac_info    = new stdClass();

            $waab_idx   = $stmt['waab_idx'];
            $coupon_idx = (int)$stmt['wari_use_coupon_idx'];
            $state_flag = $stmt['wari_state'];
            $api_code   = $stmt['waab_api'];

            $coupon_name = "";

            if ($coupon_idx !== 0)
            {
                $coupon_info_ret = $this->get_coupon_info($coupon_idx);
                if ($coupon_info_ret->result === 1)
                    $coupon_name = $coupon_info_ret->info->name;

            }

            $affi_info->waaIdx         = $stmt['waa_idx'];
            $affi_info->waabIdx        = $waab_idx;
            $affi_info->name           = $stmt['affi_name'];
            $affi_info->apiCode        = $api_code;
            $affi_info->api            = $this->workspace_common_func->api_flag_to_value($api_code);
            $affi_info->address        = $stmt['affi_address'];
            $affi_info->tel            = $this->workspace_common_func->format_tel($stmt['waab_tel']);
            $affi_info->openTime       = $stmt['waab_open_time'];
            $affi_info->closeTime      = $stmt['waab_close_time'];
            $affi_info->biztalkMembers = $this->api_affiliate_branch_biztalk_model->load_api_affiliate_branch_biztalk_information($waab_idx);

            $rental_info->apiReservCode   = $stmt['wari_api_reserv_code'];
            $rental_info->reservationIdx  = $stmt['wari_reserv_idx'];
            $rental_info->rentalStart     = $stmt['wari_rental_start'];
            $rental_info->rentalEnd       = $stmt['wari_rental_end'];
            $rental_info->compensationAge = $stmt['wari_compensation_age'];
            $rental_info->carName         = $stmt['wari_car_name'];
            $rental_info->carAge          = $stmt['wari_car_age'];
            $rental_info->carFuel         = $stmt['wari_car_fuel'];
            $rental_info->driverName      = $stmt['wari_driver_name'];
            $rental_info->driverPhone     = $this->workspace_common_func->format_tel($stmt['wari_driver_phone']);
            $rental_info->driverBirthday  = $stmt['wari_driver_birthday'];
            $rental_info->driverGender    = $this->workspace_common_func->gender_flag_to_string($stmt['wari_driver_gender']);
            $rental_info->pickupAddress   = $stmt['wari_pickup_address'];
            $rental_info->returnAddress   = $stmt['wari_return_address'];
            $rental_info->principal       = (int)$stmt['wari_principal'];
            $rental_info->payment         = (int)$stmt['wari_payment'];
            $rental_info->rentalCost      = (int)$stmt['wari_rental_cost'];
            $rental_info->cdwName         = $stmt['wari_cdw_name'];
            $rental_info->cdwCost         = (int)$stmt['wari_cdw_cost'];
            $rental_info->delivCost       = (int)$stmt['wari_deliv_cost'];
            $rental_info->useCouponIdx    = $coupon_idx;
            $rental_info->useCouponValue  = (int)$stmt['wari_use_coupon_value'];
            $rental_info->useCouponName   = $coupon_name;
            $rental_info->usePoint        = (int)$stmt['wari_use_point'];
            $rental_info->memo            = $stmt['wari_memo'];
            $rental_info->stateFlag       = $state_flag;
            $rental_info->state           = $this->workspace_common_func->api_reserv_flag_to_string($state_flag);

            $wac_info->name          = $stmt['wac_name'];
            $wac_info->type          = $stmt['wac_type'];
            $wac_info->compensation1 = $stmt['wac_compensation1'];
            $wac_info->fee1          = $stmt['wac_fee1'];
            $wac_info->compensation2 = $stmt['wac_compensation2'];
            $wac_info->fee2          = $stmt['wac_fee2'];
            $wac_info->compensation3 = $stmt['wac_compensation3'];
            $wac_info->fee3          = $stmt['wac_fee3'];
            $wac_info->compensation4 = $stmt['wac_compensation4'];
            $wac_info->fee4          = $stmt['wac_fee4'];
            $wac_info->compensation5 = $stmt['wac_compensation5'];
            $wac_info->fee5          = $stmt['wac_fee5'];
            $wac_info->compensation6 = $stmt['wac_compensation6'];
            $wac_info->fee6          = $stmt['wac_fee6'];

            $std->affiInfo   = $affi_info;
            $std->rentalInfo = $rental_info;
            $std->wacInfo    = $wac_info;

            $return_std->result = 1;
            $return_std->info   = $std;

            return $return_std;
        }

    }

    /**
     * 예약정보 최신화
     *
     * @param $wari_idx
     * @param $obj
     *
     * @return stdClass
     */
    public function update_reserv_item($wari_idx, $obj)
    {
        $set         = "";
        $is_it_first = true;

        foreach ($obj as $key => $value)
        {
            if (isset($this->keys_to_database[$key]))
            {
                if ($is_it_first)
                    $is_it_first = false;
                else
                    $set .= ", ";

                $set .= $this->keys_to_database[$key]."='".$value."'";
            }
        }

        $return_std = new stdClass();

        if ($set !== "")
        {
            $set .= ", wari_recent_update_date='".$this->now."'";

            $query = "UPDATE workspace_api_reservation_inventory SET ".$set." WHERE wari_idx=?";

            $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $sql->execute([$wari_idx]);

            $return_std->result = 1;

            return $return_std;

        }
        else
        {
            $return_std->result = 2;

            return $return_std;

        }
    }
}