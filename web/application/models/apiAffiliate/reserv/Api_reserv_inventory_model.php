<?php
/**
 * 예약목록 가져오기
 *
 * @property CI_Loader load
 * @property CI_DB_pdo_mysql_driver $db
 *
 * @property Workspace_common_func $workspace_common_func
 */

class Api_reserv_inventory_model extends WS_Model {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Workspace_common_func');
    }

    /**
     * 상태 조건을 원하는 쿼리로 변환해주는 함수
     *
     * @param $prefix       string 테이블 Alias
     * @param $search_state array
     *
     * @return string
     */
    private function get_wari_state_query($prefix, $search_state)
    {
        $is_it_first   = TRUE;
        $return_string = "(";

        foreach ($search_state as $state)
        {
            if ($is_it_first)
                $is_it_first = FALSE;
            else
                $return_string .= " OR ";

            $return_string .= $prefix.".wari_state=".$state;
        }

        return ($is_it_first)? "" : $return_string.")";

    }

    /**
     * 검색 조건을 원하는 쿼리로 변환해주는 함수
     *
     * @param $prefix       string 테이블 Alias
     * @param $search_param int    검색조건
     * @param $search_value string 검색텍스트
     *
     * @return string
     */
    private function get_wari_search_query($prefix, $search_param, $search_value)
    {
        switch ($search_param)
        {
            case 1:    // 예약번호
                return $prefix.".wari_reserv_idx='".$search_value."'";
            case 2:    // 운전자명
                return $prefix.".wari_driver_name LIKE '%".$search_value."%'";
            case 3:    // 운전자 연락처
                return $prefix.".wari_driver_phone LIKE '%".$search_value."%'";
            default:
                return "-1";
        }
    }

    /**
     * 검색 조건에 따른 API 총 목록개수 가져오기
     *
     * @author DEN
     *
     * @param array  $search_state 찾아볼 상태 조건들
     * @param int    $search_param 검색조건
     * @param string $search_value 검색텍스트
     *
     * @return int 총제휴업체수
     */
    public function load_api_reserv_total_count($search_state = [], $search_param = 0, $search_value = "")
    {
        if (empty($search_state))
            return 0;

        $state_query  = $this->get_wari_state_query("wari", $search_state);
        $search_query = ($search_param === 0)? "" : $this->get_wari_search_query("wari", $search_param, $search_value);

        if ($search_query === "-1")
            return -1;
        elseif ($search_query !== "")
            $search_query = " AND ".$search_query;

        $query = "SELECT count(*) cnt FROM workspace_api_reservation_inventory wari WHERE ".$state_query.$search_query;

        $sql = $this->slave->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute();
        if ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
            return (int)$stmt['cnt'];
        else
            return 0;

    }

    /**
     * 검색 조건에 따른 API 목록 가져오기
     *
     * @param $page
     * @param $page_row
     * @param array $search_state
     * @param int $search_param
     * @param string $search_value
     *
     * @return array|int
     */
    public function load_api_reserv_inventory($page, $page_row, $search_state = [], $search_param = 0, $search_value = "")
    {
        if (empty($search_state))
            return 0;

        if (!$page)
            $page = 1;

        $return_object = [];

        $limit_start = ($page-1) * $page_row;

        $state_query  = $this->get_wari_state_query("wari", $search_state);
        $search_query = ($search_param === 0)? "" : $this->get_wari_search_query("wari", $search_param, $search_value);

        if ($search_query === "-1")
            return -1;
        elseif ($search_query !== "")
            $search_query = " AND ".$search_query;

        $query = "
        SELECT
            waa_name,
               
            waab_idx, waab_name,
            
            wari_idx, wari_api, wari_api_reserv_code, wari_area_code, wari_reserv_idx, wari_user_idx, wari_rental_start, wari_rental_end, wari_call_reserv, wari_call_reserver_name, wari_user_grade,
            wari_compensation_age, wari_car_name, wari_car_fuel, wari_car_age, wari_pickup_address, wari_return_address,
            wari_driver_name, wari_driver_phone, wari_driver_birthday, wari_driver_gender,
            wari_principal, wari_payment, wari_rental_cost, wari_cdw_name, wari_cdw_cost, wari_deliv_cost,
            wari_use_coupon_idx, wari_use_coupon_value, wari_use_point, wari_memo,
            wari_register_date, wari_pay_date, wari_recent_update_date, wari_recent_update_am_idx, wari_state,
            wari_cdw_idx, wari_cdw_compensation, wari_cdw_self_fee1, wari_cdw_self_fee2, 
               
            wac_name
        FROM
            workspace_api_reservation_inventory wari INNER JOIN
            workspace_api_affiliate waa INNER JOIN
            workspace_api_affiliate_branch waab INNER JOIN
            workspace_api_compensation wac
            ON
                wari_waa_idx=waa_idx AND wari_waab_idx=waab_idx AND waab_waa_idx=waa_idx AND wari_wac_idx=wac_idx
        WHERE
            waa_state=1 AND waab_state=1 AND ".$state_query.$search_query."
        ORDER BY
            wari_idx DESC
        LIMIT
            ".$limit_start.", ".$page_row;

        $sql = $this->slave->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute();
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            $std = new stdClass();

            $use_coupon_idx = (int)$stmt['wari_use_coupon_idx'];
            $api            = (int)$stmt['wari_api'];
            $reserv_state   = (int)$stmt['wari_state'];
            $user_idx       = (int)$stmt['wari_user_idx'];
            $user_grade     = (int)$stmt['wari_user_grade'];
            $deductible1 = $stmt['wari_cdw_self_fee1'];
            $deductible2 = $stmt['wari_cdw_self_fee2'];

            $deductible = ($deductible2 === '-')? $deductible1 : $deductible1.' ~ '.$deductible2;

            $coupon_name = "";
            if ($use_coupon_idx !== 0)
            {
                $coupon_info = $this->get_coupon_info($use_coupon_idx);

                if ($coupon_info->result === 1)
                    $coupon_name = $coupon_info->info->name;

            }

            $kakao_id     = "";
            $use_count    = 0;
            $review_count = 0;
            if ($user_grade === 1)
            {
                $user_info_ret = $this->get_carmore_user_info($user_idx);

                if ($user_info_ret->result === 1)
                {
                    $user_info = $user_info_ret->info;

                    $kakao_id     = $user_info->kakaoId;
                    $use_count    = $user_info->useCnt;
                    $review_count = $user_info->reviewCnt;

                }

            }

            $std->wariIdx          = $stmt['wari_idx'];
            $std->waaName          = $stmt['waa_name'];
            $std->waabName         = $stmt['waab_name'];
            $std->waabIdx          = $stmt['waab_idx'];
            $std->api              = $api;
            $std->apiString        = $this->workspace_common_func->api_flag_to_value($api);
            $std->callReserv       = $stmt['wari_call_reserv'];
            $std->callReserverName = $stmt['wari_call_reserver_name'];
            $std->reservApiCode    = $stmt['wari_api_reserv_code'];
            $std->reservCode       = $stmt['wari_area_code'];
            $std->reservationIdx   = $stmt['wari_reserv_idx'];
            $std->carmoreUserIdx   = $user_idx;
            $std->carmoreKakaoId   = $kakao_id;
            $std->carmoreUseCnt    = $use_count;
            $std->carmoreReviewCnt = $review_count;
            $std->userGrade        = $user_grade;
            $std->rentalStart      = $stmt['wari_rental_start'];
            $std->rentalEnd        = $stmt['wari_rental_end'];
            $std->compensationAge  = $stmt['wari_compensation_age'];
            $std->carName          = $stmt['wari_car_name'];
            $std->carFuel          = $stmt['wari_car_fuel'];
            $std->carAge           = $stmt['wari_car_age'];
            $std->driverName       = $stmt['wari_driver_name'];
            $std->driverPhone      = $this->workspace_common_func->format_tel($stmt['wari_driver_phone']);
            $std->driverBirthday   = $stmt['wari_driver_birthday'];
            $std->driverGender     = $this->workspace_common_func->gender_flag_to_string((int)$stmt['wari_driver_gender']);
            $std->pickupAddress    = $stmt['wari_pickup_address'];
            $std->returnAddress    = $stmt['wari_return_address'];
            $std->principal        = $stmt['wari_principal'];
            $std->payment          = $stmt['wari_payment'];
            $std->rentalCost       = $stmt['wari_rental_cost'];
            $std->cdwCost          = $stmt['wari_cdw_cost'];
            $std->delivCost        = $stmt['wari_deliv_cost'];
            $std->cdwName          = $stmt['wari_cdw_name'];
            $std->useCouponName    = $coupon_name;
            $std->useCouponValue   = (int)$stmt['wari_use_coupon_value'];
            $std->usePoint         = $stmt['wari_use_point'];
            $std->payDate          = $this->workspace_common_func->check_false_value($stmt['wari_pay_date'], TRUE);
            $std->recentUpdateDate = $this->workspace_common_func->check_false_value($stmt['wari_recent_update_date'], TRUE);
            $std->state            = $reserv_state;
            $std->stateString      = $this->workspace_common_func->api_reserv_flag_to_string($reserv_state);
            $std->cdwType          = ($api === 3)? (int)$stmt['wari_cdw_idx'] : '';
            $std->cdwTypeName      = ($std->cdwType === 1)? '일반자차' : (($std->cdwType === 2)? '완전자차' : '');
            $std->cdwTypeCompensation      = $stmt['wari_cdw_compensation'];
            $std->deductible      = $deductible;

            array_push($return_object, $std);

        }

        return $return_object;

    }

}