<?php
/**
 * Created by PhpStorm.
 * User: teamo2_server
 * Date: 2019-04-25
 * Time: 16:53
 */

class Api_affiliate_grim_car extends WS_Model {
    private $key_to_database = [
        'carName'      => 'wgci_car_name',
        'gubun'        => 'wgci_car_gubun',
        'gear'         => 'wgci_car_gear',
        'color'        => 'wgci_color',
        'fuel'         => 'wgci_fuel',
        'old'          => 'wgci_old',
        'jeongwon'     => 'wgci_jeongwon',
        'permit'       => 'wgci_permit',
        'baegi'        => 'wgci_baegi',
        'option'       => 'wgci_option',
        'model'        => 'wgci_cimaster_idx',
        'compensation' => 'wgci_wac_idx'
    ];

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * 차량 있는지 체크
     *
     * @param $waab_idx int    지점아이디값
     * @param $car_code string 차종코드
     *
     * @return bool
     */
    public function chk_has_car($waab_idx, $car_code)
    {
        $query = "SELECT wgci_idx FROM workspace_grim_car_info WHERE wgci_waab_idx=? AND wgci_car_code=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$waab_idx, $car_code]);
        if ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
            return (int)$stmt['wgci_idx'];
        else
            return false;
    }

    /**
     * 차량정보 넣기
     *
     * @param $bind_param
     */
    public function insert_car_info($bind_param)
    {
        $query = "
        INSERT INTO workspace_grim_car_info
            (wgci_waa_idx, wgci_waab_idx,
            wgci_car_code, wgci_car_name, wgci_car_gubun, wgci_car_gear, wgci_color, wgci_fuel,
            wgci_old, wgci_jeongwon, wgci_permit, wgci_baegi, wgci_option,
            wgci_cimaster_idx, wgci_wac_idx)
        VALUE
            (:waaIdx, :waabIdx,
            :carCode, :carName, :gubun, :gear, :color, :fuel,
            :old, :jeongwon, :permit, :baegi, :option,
            0, 0)";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute($bind_param);
    }

    /**
     * 차량정보, 매칭정보 수정하기
     *
     * @param $wgci_idx int    INT 아이디값
     * @param $obj      array
     */
    public function update_car_info($wgci_idx, $obj)
    {
        $is_it_first = true;
        $set         = "";

        foreach ($obj as $key => $value)
        {
            if (isset($this->key_to_database[$key]))
            {
                if ($is_it_first)
                    $is_it_first = false;
                else
                    $set .= ", ";

                $set .= $this->key_to_database[$key]."='".$value."'";

            }
        }

        $query = "UPDATE workspace_grim_car_info SET ".$set." WHERE wgci_idx=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$wgci_idx]);

    }

    /**
     * 그림업체의 차량목록 받아오기
     *
     * @param $waab_idx int
     *
     * @return array
     */
    public function get_car_inventory($waab_idx)
    {
        $inventory = [];

        $query = "
        SELECT
            wgci_idx, wgci_car_code, wgci_car_name, wgci_cimaster_idx, wgci_wac_idx,
            IFNULL(cimaster.carinfomst_type, '') carinfomst_type, IFNULL(cimaster.carinfomst_model, '') carinfomst_model,
            IFNULL((SELECT wac_name FROM workspace_api_compensation WHERE wac_idx=wgci_wac_idx), '') wac_name
        FROM
            workspace_grim_car_info wgci LEFT OUTER JOIN
            carinfo_master cimaster
            ON
                wgci_cimaster_idx=cimaster.carinfo_idx
        WHERE wgci_waab_idx=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$waab_idx]);
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            array_push($inventory, [
                'idx'             => $stmt['wgci_idx'],
                'carCode'         => $stmt['wgci_car_code'],
                'carName'         => $stmt['wgci_car_name'],
                'cimasterIdx'     => $stmt['wgci_cimaster_idx'],
                'cimasterModel'   => $stmt['carinfomst_model'],
                'cimasterCarType' => $stmt['carinfomst_type'],
                'wacIdx'          => $stmt['wgci_wac_idx'],
                'wacName'         => $stmt['wac_name']
            ]);

        }


        return $inventory;
    }

}