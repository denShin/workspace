<?php
/**
 * Created by PhpStorm.
 * User: teamo2_server
 * Date: 2019-04-25
 * Time: 20:01
 */

class Api_affiliate_ins_car extends WS_Model {
    private $key_to_database = [
        'carName'      => 'wici_r_car_name',
        'carOption'    => 'wici_r_car_option',
        'carFuel'      => 'wici_r_car_fuel',
        'carPassenger' => 'wici_r_car_passenger',
        'carPermitAge' => 'wici_r_permit_age',
        'carYear'      => 'wici_r_car_year',
        'driverMethod' => 'wici_r_drive_method',
        'displacement' => 'wici_r_displacement',
        'gear'         => 'wici_r_gear',
        'title'        => 'wici_r_title',
        'model'        => 'wici_cimaster_idx',
        'compensation' => 'wici_wac_idx'
    ];

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * 차량 있는지 체크
     *
     * @param $waab_idx int    지점아이디값
     * @param $car_code string 차종코드
     *
     * @return bool
     */
    public function chk_has_car($waab_idx, $car_code)
    {
        $query = "SELECT wici_idx FROM workspace_ins_car_info WHERE wici_waab_idx=? AND wici_r_car_code=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$waab_idx, $car_code]);
        if ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
            return (int)$stmt['wici_idx'];
        else
            return false;
    }

    /**
     * 차량정보 넣기
     *
     * @param $bind_param
     */
    public function insert_car_info($bind_param)
    {
        $query = "
        INSERT INTO workspace_ins_car_info
            (wici_waa_idx, wici_waab_idx,
            wici_r_car_code, wici_r_car_name, wici_r_car_option, wici_r_car_fuel, wici_r_car_passenger, wici_r_permit_age,
            wici_r_car_year, wici_r_drive_method, wici_r_displacement, wici_r_gear, wici_r_title,
            wici_cimaster_idx, wici_wac_idx)
        VALUE
            (:waaIdx, :waabIdx,
            :carCode, :carName, :carOption, :carFuel, :carPassenger, :carPermitAge,
            :carYear, :driverMethod, :displacement, :gear, :title,
            0, 0)";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute($bind_param);
    }

    /**
     * 차량정보, 매칭정보 수정하기
     *
     * @param $wici_idx int    INT 아이디값
     * @param $obj      array
     */
    public function update_car_info($wici_idx, $obj)
    {
        $is_it_first = true;
        $set         = "";

        foreach ($obj as $key => $value)
        {
            if (isset($this->key_to_database[$key]))
            {
                if ($is_it_first)
                    $is_it_first = false;
                else
                    $set .= ", ";

                $set .= $this->key_to_database[$key]."='".$value."'";

            }
        }

        $query = "UPDATE workspace_ins_car_info SET ".$set." WHERE wici_idx=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$wici_idx]);

    }

    /**
     * 그림업체의 차량목록 받아오기
     *
     * @param $waab_idx int
     *
     * @return array
     */
    public function get_car_inventory($waab_idx)
    {
        $inventory = [];

        $query = "
        SELECT
            wici_idx, wici_r_car_code, wici_r_car_name, wici_cimaster_idx, wici_wac_idx,
            IFNULL(cimaster.carinfomst_type, '') carinfomst_type, IFNULL(cimaster.carinfomst_model, '') carinfomst_model,
            IFNULL((SELECT wac_name FROM workspace_api_compensation WHERE wac_idx=wici_wac_idx), '') wac_name
        FROM
            workspace_ins_car_info wici LEFT OUTER JOIN
            carinfo_master cimaster
            ON
                wici_cimaster_idx=cimaster.carinfo_idx
        WHERE wici_waab_idx=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$waab_idx]);
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            array_push($inventory, [
                'idx'             => $stmt['wici_idx'],
                'carCode'         => $stmt['wici_r_car_code'],
                'carName'         => $stmt['wici_r_car_name'],
                'cimasterIdx'     => $stmt['wici_cimaster_idx'],
                'cimasterModel'   => $stmt['carinfomst_model'],
                'cimasterCarType' => $stmt['carinfomst_type'],
                'wacIdx'          => $stmt['wici_wac_idx'],
                'wacName'         => $stmt['wac_name']
            ]);

        }


        return $inventory;
    }
}