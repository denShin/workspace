<?php
/**
 * Created by PhpStorm.
 * User: 전철환
 * Date: 2017-09-19
 * Time: 오후 5:50
 */
class Marketlist_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function getMarketlistCount($sp = "", $sv = "")
    {
        $sql = "select count(*) as cnt from  admin_marketarget where 1=1 ";

        if ($sp != "" && $sv != "") {
            $sql .= " and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }


    function getMarketlistPageList($page = 1, $pageline = 20, $sp = "", $sv = "")
    {

        if (!$page) $page = 1;
        $start = ($page - 1) * $pageline;
        $sql = "select *  from   admin_marketarget a where 1=1  ";

        if ($sp != "" && $sv != "") {
            $sql .= " and $sp like '%" . $this->db->escape_str($sv) . "%'";
        }
        $sql .= " order by target_idx desc limit $start,20";
        $query = $this->db->query($sql);

        return $query->result();

    }

    function getMarketlistDetail($target_idx)
    {
        $sql = "select *   from admin_marketarget a where target_idx='$target_idx'";

        $query = $this->db->query($sql)->result_array();
        $row = $query[0];
        return $row;
    }

     public   function procMarketlist($emode, $data)
    {
        $return_v = "";

        if ($emode == "new") {

            $nowtime = date("YmdHis");
            $sql = "INSERT INTO admin_marketarget 	(	target_name, 	target_addr,target_tel, target_content, 	contact_check,target_per)
            VALUES('" . $this->db->escape_str($data["target_name"]) . "'
            ,'" . $this->db->escape_str($data["target_addr"]) . "'
            ,'" . $this->db->escape_str($data["target_tel"]) . "'
            ,'" . $this->db->escape_str($data["target_content"]) . "'
            ,'" . $this->db->escape_str($data["contact_check"]) . "'
            ,'" . $this->db->escape_str($data["target_per"]) . "'
            );";

            $this->db->query($sql);
            $return_v = "y";

        } else if ($emode == "edit") {
            $sql = "update  admin_marketarget set
              target_name='" . $this->db->escape_str($data["target_name"]) . "'
              ,target_addr='" . $this->db->escape_str($data["target_addr"]) . "'
           ,target_tel='" . $data["target_tel"] . "'
           ,target_content='" . $this->db->escape_str($data["target_content"]) . "'
           ,contact_check ='" . $data["contact_check"] . "'
           ,target_per ='" . $data["target_per"] . "'
         where target_idx='" . $data["target_idx"] . "'";

            $this->db->query($sql);
            $return_v = "y";
        } else if ($emode == "del") {

            // 팀하위에 멤버가 있을 경우 삭제 불가능

            $sql = "delete from admin_marketarget where target_idx='" . $this->db->escape_str($data["target_idx"]) . "'";

            $this->db->query($sql);

            $return_v = "y";
        } else if ($emode == "contact_check") {

            $sql = "update  admin_marketarget set contact_check ='" . $data["contact_check"] . "'
         where target_idx='" . $data["target_idx"] . "'";

            $this->db->query($sql);
        }
        return "y";
    }
}
