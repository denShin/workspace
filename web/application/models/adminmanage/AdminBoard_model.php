<?php
/**
 * AdminBoard_model - 운영툴 공지사항 관련
 */
class AdminBoard_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    // 관리자 게시판 게시물수 가져오기
    function getAdminBoardCount($board_code="",$sp="",$sv=""){
        $sql="select count(*) as cnt from  admin_board where board_code='$board_code'  ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    // 관리자 게시판 게시물 가져오기
    function getAdminBoardPageList($board_code,$page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select *
            ,(SELECT admin_name FROM admin_member WHERE  admin_email=a.mem_id)AS mem_name
            ,(SELECT admin_name FROM admin_member WHERE  admin_email=a.confirm_id)AS confirm_name 

            from   admin_board a where board_code='$board_code'  ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by board_idx desc limit $start,20";
        $query = $this->db->query($sql);

        return $query->result();
    }

    // 관리자 게시판 상세페이지 가져오기
    function getAdminBoardDetail($board_idx)
    {
        $sql="select *
            ,(SELECT admin_name FROM admin_member WHERE  admin_email=a.mem_id)AS mem_name
            ,(SELECT admin_name FROM admin_member WHERE  admin_email=a.confirm_id)AS confirm_name 
        from admin_board a where board_idx='$board_idx'";

        $query = $this->db->query($sql)->result_array();
        $row = $query[0];
        return $row;
    }

    // 게시판 수정,등록,삭제
    public function procAdminBoard($emode,$data)
    {
        $return_v="";

        if($emode=="new"){

            $nowtime =date("YmdHis");
            $sql = "INSERT INTO admin_board 	(	content_code, 	board_title,
				board_content, mem_id, 	regdate, 	board_code,board_part
				,startdate , enddate ,content_type,price)
				VALUES('".$data["content_code"]."'
				, 	'".$this->db->escape_str($data["board_title"])."'
				,'".$this->db->escape_str($data["board_content"])."'
				,'".$data["mem_id"]."'
				,'$nowtime'
				,'".$data["board_code"]."'
				,'".$data["board_part"]."'
				,'".$data["startdate"]."'
				,'".$data["enddate"]."'
				,'".$data["content_type"]."'
				,'".$this->db->escape_str($data["price"])."');";

            $this->db->query($sql);
            $return_v="y";

        }else if($emode=="edit"){
            $sql = "update  admin_board set
                  board_title='".$this->db->escape_str($data["board_title"])."'
                  ,board_content='".$this->db->escape_str($data["board_content"])."'
               ,content_type='".$data["content_type"]."'
               ,price='".$this->db->escape_str($data["price"])."'
               ,startdate ='".$data["startdate"]."'
               ,enddate='".$data["enddate"]."'
             where board_idx='".$data["board_idx"]."'";

            $this->db->query($sql);
            $return_v="y";
        }else if($emode=="del"){
 
            $sql = "delete from admin_board where board_idx='".$this->db->escape_str($data["board_idx"])."'";
            $this->db->query($sql);

            $sql = "delete from admin_comment where board_idx='".$this->db->escape_str($data["board_idx"])."'";
            $this->db->query($sql);
            $return_v="y";
        }else if($emode=="agree_yn"){

            if($data["isConfirm"]=="1"){
                $agree_yn="y";
            }else{
                $agree_yn="x";
            }
            //$login_id
            $arr_confirmlist=$data["confirm_list"];
            $confirm_id=$data["confirm_id"];

            for($i=0;$i < sizeof($arr_confirmlist); $i++)
            {
                $board_idx = $arr_confirmlist[$i];
                $sql="update admin_board set agree_yn='$agree_yn',confirm_id='$confirm_id'   where board_idx='$board_idx' ";
                $this->db->query($sql);
            }
        }
        return "y";
    }

    // 댓글 리스트 가져오기
    public function getBoardReplyList($board_code,$board_idx)
    {
        $sql="SELECT *
      ,(SELECT admin_name FROM admin_member WHERE  admin_email=a.mem_id)AS mem_name
        FROM admin_comment a where board_code='$board_code' and board_idx='$board_idx' order by board_idx asc ";
        $query = $this->db->query($sql);

        return $query->result();
    }

    // 댓글등록,삭제
    public function procBoardReply($emode,$data)
    {
        if($emode=="new"){

            $nowtime =date("YmdHis");
            $replyText=$this->db->escape_str($data["replyText"]);
            $board_idx=$data["board_idx"];
            $board_code=$data["board_code"];
            $login_id=$data["mem_id"];

            $sql=" INSERT INTO admin_comment 	(	`commenttext`, 	`member_idx`, `idx`, `regdate`,board_code)
					VALUES	(	'$replyText', 	'$login_id', '$board_idx', '$nowtime','$board_code')";
            $this->db->query($sql);
            $return_v="y";

        }else if($emode=="del"){

            $login_id=$data["mem_id"];
            $comment_idx=$data["comment_idx"];

            $sql="delete from admin_comment   where mem_id='$login_id' and comment_idx='$comment_idx' ";

            $this->db->query($sql);
            $return_v="y";
        }
    }

    //게시물 관련 파일 가져오기
    public function get_BoardFileList($content_code)
    {
        $sql="SELECT content_code,  fileOrgName,  fileSaveName,  fileType
                              FROM admin_uploadfile where content_code='$content_code'";

        return $this->db->query($sql)->result_array();
    }

}