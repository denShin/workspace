<?php
/**
 * Adminmember_model - 관리자 리스트 관리 
 */
class Adminmember_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    // 관리자 숫자 가져오기
    function getAdminMemberCount($sp="",$sv=""){
        $sql="select count(*) as cnt from admin_member where admin_stats in('y','n') ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    // 관리자 회원 리스트
    function getAdminMemberPageList($page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select * from admin_member where admin_stats in('y','n') ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by admin_idx desc limit $start,20";
        $query = $this->db->query($sql);
        return $query->result();
    }

    // 관리자 정보 상세
    function getAdminMemberDetail($admin_email)
    {
        $sql="select * from admin_member where admin_email='$admin_email'";
        $query = $this->db->query($sql)->result_array();
        $row = $query[0];
        return $row;
    }

    // 관리자 팀 정보 가져오기
    function getAdminTeamInfo($itype,$team_code)
    {
        if($itype =="list")
        {
            $sql="select *
                  ,(select count(*) From admin_member where team_code=a.team_code) as teamtotalcnt
                from admin_team  a";
            $query = $this->db->query($sql);

            return $query->result();
        }
        else{
            $sql="select * from admin_team where team_code='$team_code' ";
            $row = $this->db->query($sql)->result_array();
            return $row[0];
        }
    }

    // 아이디에 따른 로그인 정보가져오기
    function getLoginResult($id)
    {

        $sql="select count(*) as cnt from admin_member where admin_email='$id' and admin_stats='y'";
        $cntrow =$this->db->query($sql)->row();
        $tcnt = $cntrow->cnt;

        if($tcnt >0){
            $sql="select * from admin_member where admin_email='$id'";
            $memrow =$this->db->query($sql)->row();
            $data["return_v"]="y";
            $data["admin_name"]=$memrow->admin_name;
            $data["admin_email"]=$memrow->admin_email;
            $data["admin_pwd"]=$memrow->admin_pwd;
            $data["master_yn"]=$memrow->master_yn;
            $data["config_yn"]=$memrow->config_yn;
            $data["offwork_yn"]=$memrow->offwork_yn;
            $data["paycancel_yn"]=$memrow->paycancel_yn;
            $data["pointauth_yn"]=$memrow->pointauth_yn;
            $data["calc_yn"]=$memrow->calc_yn;
            $data["ordermng_yn"]=$memrow->ordermng_yn;
            $data["team_code"]=$memrow->team_code;
            $data["member_grade"]=$memrow->member_grade;
            $data["kakaoid"]=$memrow->kakaoid;

        }else {
            $data["return_v"]="e";
        }
        return $data;
    }


    // 팀명 등록,수정,삭제
    public function procAdminTeam($emode,$data)
    {
        $return_v="";
        $team_code =$data["team_code"];
        $team_name =$data["team_name"];

        if($emode=="new"){

            $sql="select count(*) as cnt from admin_team where team_code='$team_code'";
            $cntrow =$this->db->query($sql)->row();
            $tcnt = $cntrow->cnt;

            if($tcnt >0){
                $return_v="e";
            }else {
                $sql = "INSERT INTO admin_team (team_code, team_name, team_cnt)
				VALUES('$team_code', '$team_name', 0);";
                $this->db->query($sql);
                $return_v="y";
            }
        }else if($emode=="edit"){
            $sql = "update  admin_team set team_name='$team_name' where team_code='$team_code'";
            $this->db->query($sql);
            $return_v="y";
        }else if($emode=="del"){



            $sql = "delete from admin_team where team_code='$team_code'";
            $this->db->query($sql);
            $return_v="y";
        }

        return $return_v;
    }

    public function procChangePassword($data)
    {
        $admin_email =$data["admin_email"];
        $admin_pwd =$data["admin_pwd"];

        $sql="update admin_member set admin_pwd='".$admin_pwd."'  where admin_email='".$admin_email."'";

        $this->db->query($sql);
    }

    // 관리자 등록,수정,삭제
    public function procAdminMember($emode,$data)
    {
        $return_v="";

        $admin_name=$data["admin_name"];
        $team_code=$data["team_code"];
        $member_grade=$data["member_grade"];
        $admin_email=$data["admin_email"];
        $admin_pwd=$data["admin_pwd"];
        $admin_tel=$data["admin_tel"];
        $admin_stats=$data["admin_stats"];
        $config_yn=$data["config_yn"];
        $paycancel_yn=$data["paycancel_yn"];
        $pointauth_yn=$data["pointauth_yn"];
        $calc_yn=$data["calc_yn"];
        $ordermng_yn=$data["ordermng_yn"];
        $master_yn=$data["master_yn"];
        $kakaoid =$data["kakaoid"];


        if($emode=="new"){

            $sql="select count(*) as cnt from admin_member where admin_email='$admin_email'";
            $cntrow =$this->db->query($sql)->row();

            $tcnt = $cntrow->cnt;

            if($tcnt >0){
                $return_v="e";
            }else {
                $sql="INSERT INTO admin_member (admin_name,team_code,member_grade,admin_email,admin_pwd
                ,admin_tel,admin_stats,config_yn,paycancel_yn,pointauth_yn,calc_yn,ordermng_yn,master_yn,kakaoid)
				VALUES
				('$admin_name', '$team_code', '$member_grade', '$admin_email' ,'$admin_pwd', '$admin_tel', '$admin_stats'
				, '$config_yn','$paycancel_yn'	,'$pointauth_yn', '$calc_yn','$ordermng_yn','$master_yn','$kakaoid')";
                $this->db->query($sql);
                $return_v="y";
            }
        }else if($emode=="edit"){
            $sql="update admin_member set  admin_name='$admin_name' , admin_stats='$admin_stats',team_code='$team_code'
					,member_grade='$member_grade',config_yn='$config_yn', paycancel_yn='$paycancel_yn'
					,pointauth_yn='$pointauth_yn', calc_yn='$calc_yn',ordermng_yn='$ordermng_yn'
					,master_yn='$master_yn' ,kakaoid='$kakaoid'
			where admin_email='$admin_email' ";

            $this->db->query($sql);
            $return_v="y";
        }else if($emode=="del"){



            $sql="update admin_member set admin_stats='x' 	where admin_email='$admin_email' ";
            $this->db->query($sql);
            $return_v="y";
        }

        return $return_v;
    }

}