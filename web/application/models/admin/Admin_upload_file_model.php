<?php
/**
 * Created by PhpStorm.
 * User: teamo2_server
 * Date: 2019-05-03
 * Time: 18:42
 */

class Admin_upload_file_model extends WS_Model {
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * 업로드 파일 저장
     *
     * @param $bind_param
     */
    public function upload_file($bind_param)
    {
        $query = "
        INSERT INTO admin_uploadfile
            (content_code, fileOrgName, fileSaveName, fileSize, fileType, fileuptype, mem_id, board_code, publish_yn)
        VALUE
            (:contentCode, :fileOrgName, :fileSaveName, :fileSize, :fileType, :fileUpType, :memId, :boardCode, :publish)";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute($bind_param);
    }

    public function delete_file($bind_param)
    {
        $query = "DELETE FROM admin_uploadfile WHERE content_code=:contentCode AND fileuptype=:fileUpType AND board_code=:boardCode";
        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute($bind_param);
    }

    public function delete_file_by_name($file_save_name)
    {
        if ($file_save_name !== "")
        {
            $query = "UPDATE admin_uploadfile SET publish_yn='n' WHERE fileSaveName=?";

            $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $sql->execute([$file_save_name]);
        }
    }

}