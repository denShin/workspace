<?php
/**
 * Admin 모델
 */

class Admin_model extends WS_Model {
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * admin email 로 아이디 값 가져오기
     *
     * @param string $email 검색 이메일
     *
     * @return int 아이디값
     */
    public function get_admin_idx($email)
    {
        $query = "SELECT admin_idx FROM admin_member WHERE admin_email=?";
        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$email]);
        if ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
            return (int)$stmt['admin_idx'];
        else
            return 0;

    }

}
