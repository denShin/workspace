<?php
/**
 * Created by PhpStorm.
 * User: 전철환
 * Date: 2017-09-19
 * Time: 오후 5:50
 */
class Jsoncompanycalendar_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function JsoncompanycalendarList($start,$end){

        $sql="SELECT board_idx,board_title,startdate,enddate
              ,(select admin_name from admin_member where admin_email=a.mem_id) as mem_name
              FROM
                (select * from admin_board where board_code='calendar'  ) a  WHERE   ((startdate like '$start%') or  (enddate like '$end%'))";

        $query = $this->db->query($sql);

        $i=0;
        foreach ($query->result_array() as $drow)
        {
            $board_idx = $drow["board_idx"];
            $mem_name =   $drow["mem_name"];

            $board_title= $drow["board_title"];
            $startdate= $drow["startdate"];
            $enddate= $drow["enddate"];
            $enddate=date('Y-m-d', strtotime($enddate. ' + 1 days'));

            $board_title="[$mem_name] $board_title";

            $url="/etcmanage/CompanyCalendar?ptype=v&board_idx=".$board_idx;
            $check=$i%5;


            $color =$this->get_color($check);
            $rows[]=array("start"=>$startdate,"end"=>$enddate,"title"=>$board_title,"color"=>$color,"url"=>$url);
            $i++;
        }

        return $rows;
    }

    function get_color($i)
    {
        $remain  = $i %5;
        switch ($remain) {
            case 0 :
                $returnstr = "#DB7BAB";
                break;
            case 1  :
                $returnstr = "#65D35D";
                break;
            case 2  :
                $returnstr = "#F70AD8";
                break;
            case 3  :
                $returnstr = "#7B7BDB";
                break;
            case 4  :
                $returnstr = "#F7350A";
                break;
        }
        return $returnstr;
    }

}
