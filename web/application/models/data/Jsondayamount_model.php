<?php
/**
 * Created by PhpStorm.
 * User: 전철환
 * Date: 2017-09-19
 * Time: 오후 5:50
 */
class Jsondayamount_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function getJsondayamountList($start,$end){
        $start=str_replace("-","",$start);
        $end=str_replace("-","",$end);

        $sql="SELECT DATE_FORMAT( pdate ,\"%Y-%m-%d\")	as rpdate
            ,	SUM(ordertotal) as psum FROM av_saleslog WHERE pdate BETWEEN '".$start."' AND '".$end."'
	          GROUP BY pdate	ORDER BY pdate ASC";

        $query = $this->db->query($sql);

        foreach ($query->result_array() as $drow)
        {
            $dayamount = number_format($drow["psum"],0);
            $rows[]=array("start"=>$drow['rpdate'],"title"=>"매출 : ".$dayamount." 원");
        }

        return $rows;
    }


}
