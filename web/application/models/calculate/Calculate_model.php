<?php
/**
 * Calculate_model - 정산 관련 db 처리 
 */

class Calculate_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    // 정산 마스터 개수 가져오기
    function getCalmstCount($sp="",$sv=""){
        $sql="select count(*) as cnt from  caculate_calmainmst where 1=1";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    // 정산 마스터 리스트 가져오기
    function getCalmstList($page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select *   from   caculate_calmainmst a where 1=1  ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by calmainmst_idx desc limit $start,20";

        $query = $this->db->query($sql)->result_array();

        return $query ;
    }

    // 정산 마스터 정보 상세가져오기
    function getCalmaininfo($calmainmst_code)
    {
        $sql="select *   from   caculate_calmainmst a where calmainmst_code='$calmainmst_code'";
        $arr_data = $this->db->query($sql)->result_array();
        return $arr_data[0];
    }

    // 정산 마스터,마스터에 관련된 지점의 정산정보가져오기
    function getCalinfoDetail($calmainmst_code, $calmaindtl_branch){

        // 정산 마스터 정보 1건
        $sql="select *   from   caculate_calmainmst a where calmainmst_code='$calmainmst_code'";
        $arr_mst = $this->db->query($sql)->result_array();

        if($calmaindtl_branch==""){

            $sql="SELECT calmaindtl_branch FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' GROUP BY calmaindtl_branch  order by  calmaindtl_branch asc LIMIT 1 ";
            $calmaindtl_branch = $this->db->query($sql)->row()->calmaindtl_branch;
        }

        // 정산대상 업체정보 - 좌측 리스트

        $sql="SELECT  b.name AS companyname, a.branchName as branchname,  a.serial as branchserial,a.rentCompany_serial as companyserial  ,c.calcnt 
                  , c.calculate_yn,  c.bankins_yn,  c.taxmail_yn
              FROM   new_rentCompany_branch  a INNER JOIN new_rentCompany b
              INNER JOIN (SELECT calmaindtl_branch ,dtlcnt AS calcnt, calculate_yn,  bankins_yn,  taxmail_yn FROM caculate_calbranch WHERE calmainmst_code = '$calmainmst_code' GROUP BY calmaindtl_branch)  c
                            ON a.rentCompany_serial =b.serial AND a.serial =c.calmaindtl_branch
               WHERE a.serial IN(SELECT calmaindtl_branch FROM caculate_calbranch WHERE calmainmst_code = '$calmainmst_code' GROUP BY calmaindtl_branch )   order by  companyname asc";
        $arr_listdtl = $this->db->query($sql)->result_array();

        // 업체별 정산 상세정보 - 우측 리스트
        $sql="SELECT * FROM caculate_calmaindtl   
            WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch' ";
        $arr_dtl = $this->db->query($sql)->result_array();

        // 업체별 정산 비율
        $sql="select rentCompany_serial,settlement_rental_rate from new_rentCompany_branch where serial='$calmaindtl_branch'";
        $settlement_rental_rate = $this->db->query($sql)->row()->settlement_rental_rate;
        $rentCompany_serial = $this->db->query($sql)->row()->rentCompany_serial;

        // 업체별 정산 비율
        $sql="select * from caculate_calbranch where  calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch'";
        $arr_calbranchdtl = $this->db->query($sql)->result_array();

        $data["mst"]=$arr_mst;
        $data["list"]=$arr_listdtl;
        $data["dtl"]=$arr_dtl;
        $data["calbranchdtl"]=$arr_calbranchdtl;
        $data["settlement_rental_rate"]=$settlement_rental_rate;
        $data["rentCompany_serial"]=$rentCompany_serial;
        $data["calmaindtl_branch"]=$calmaindtl_branch;
        $data["sumdata"]=$this->getBranchsumdata($calmainmst_code, $calmaindtl_branch);

        return $data;

    }

    // 각 지점의 정산 합산 정보가져오기
    function getBranchsumdata($calmainmst_code, $calmaindtl_branch){
        //-- 정상결제
        $sql="SELECT COUNT(*) AS cnt , IFNULL(SUM(calmaindtl_totalprice ),0)  AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch' and calmaindtl_gubn='1'  GROUP BY calmaindtl_branch";
        $result["cnt1"] = $this->db->query($sql)->row()->cnt;
        $result["total1"] = $this->db->query($sql)->row()->total;


        //-- 단기렌트
        $sql="SELECT COUNT(*) AS cnt ,IFNULL( SUM(calmaindtl_totalprice ) ,0) AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch' and calmaindtl_gubn='1' AND calmaindtl_period='1'  GROUP BY calmaindtl_branch";
        $result["cnt2"] = $this->db->query($sql)->row()->cnt;
        $result["total2"] = $this->db->query($sql)->row()->total;


        //-- 월렌트
        $sql="SELECT COUNT(*) AS cnt , IFNULL( SUM(calmaindtl_totalprice ) ,0) AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch'  and calmaindtl_gubn='1' AND calmaindtl_period='2'  GROUP BY calmaindtl_branch";
        $result["cnt3"] = $this->db->query($sql)->row()->cnt;
        $result["total3"] = $this->db->query($sql)->row()->total;


        // -- 취소수수료
        $sql=" SELECT COUNT(*) AS cnt , IFNULL( SUM(calmaindtl_totalprice ) ,0) AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch' AND calmaindtl_gubn='2'  GROUP BY calmaindtl_branch";
        $result["cnt4"] = $this->db->query($sql)->row()->cnt;
        $result["total4"] = $this->db->query($sql)->row()->total;


        //-- 총매출
        $sql="SELECT   COUNT(*) AS cnt ,  IFNULL( SUM(calmaindtl_totalprice ) ,0) AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch'   GROUP BY calmaindtl_branch";
        $result["cnt5"] = $this->db->query($sql)->row()->cnt;
        $result["total5"] = $this->db->query($sql)->row()->total;


        //-- 총 정산
        $sql="SELECT  COUNT(*) AS cnt , IFNULL( SUM(calmaindtl_calamount ) ,0) AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch'   GROUP BY calmaindtl_branch";
        $result["cnt6"] = $this->db->query($sql)->row()->cnt;
        $result["total6"] = $this->db->query($sql)->row()->total;

        return $result;
    }

    // 사업자등록증 번호로  정산 마스터,마스터에 관련된 지점의 정산정보가져오기
    function getCalinfoDetailByregisternumber($calmainmst_code, $registration_number)
    {
        // 정산 마스터 정보 1건
        $sql="select *   from   caculate_calmainmst a where calmainmst_code='$calmainmst_code'";
        $arr_mst = $this->db->query($sql)->result_array();


        // 업체별 정산 상세정보 - 우측 리스트
        $sql="SELECT * FROM caculate_calmaindtl  a INNER JOIN tbl_reservation_list b
             INNER JOIN (SELECT CONCAT(car_number1,car_number2,car_number3) AS car_number,model,new_rentCar.serial AS carserial FROM new_rentCar INNER JOIN new_car ON new_rentCar.car_serial=new_car.serial ) c
             inner join new_rentSchedule d 
              ON a.f_reservationidx=b.f_reservationidx AND c.carserial=b.f_carserial and b.f_bookingid=d.serial
            WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch in(select branch_serial from calculate_taxinfo where registration_number='$registration_number')  ";
        $arr_dtl = $this->db->query($sql)->result_array();


        $data["mst"]=$arr_mst;
        $data["dtl"]=$arr_dtl;
        $data["sumdata"]=$this->getBranchsumdataByregisternumber($calmainmst_code, $registration_number);

        return $data;

    }

    // 사업자등록증 번호로   각 지점의 정산 합산 정보가져오기
    function getBranchsumdataByregisternumber($calmainmst_code, $registration_number){
        //-- 정상결제
        $sql="SELECT COUNT(*) AS cnt , IFNULL(SUM(calmaindtl_totalprice ),0)  AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND
 calmaindtl_branch  in(select branch_serial from calculate_taxinfo where registration_number='$registration_number')  ";

        $result["cnt1"] = $this->db->query($sql)->row()->cnt;
        $result["total1"] = $this->db->query($sql)->row()->total;


        //-- 단기렌트
        $sql="SELECT COUNT(*) AS cnt ,IFNULL( SUM(calmaindtl_totalprice ) ,0) AS total  FROM caculate_calmaindtl
 WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch  in(select branch_serial from calculate_taxinfo where registration_number='$registration_number')  AND calmaindtl_period='1' ";
        $result["cnt2"] = $this->db->query($sql)->row()->cnt;
        $result["total2"] = $this->db->query($sql)->row()->total;


        //-- 월렌트
        $sql="SELECT COUNT(*) AS cnt , IFNULL( SUM(calmaindtl_totalprice ) ,0) AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' 
AND calmaindtl_branch  in(select branch_serial from calculate_taxinfo where registration_number='$registration_number')  AND calmaindtl_period='2'  ";
        $result["cnt3"] = $this->db->query($sql)->row()->cnt;
        $result["total3"] = $this->db->query($sql)->row()->total;


        // -- 취소수수료
        $sql=" SELECT COUNT(*) AS cnt , IFNULL( SUM(calmaindtl_totalprice ) ,0) AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' 
 AND calmaindtl_branch  in(select branch_serial from calculate_taxinfo where registration_number='$registration_number')  AND calmaindtl_gubn='2'  ";
        $result["cnt4"] = $this->db->query($sql)->row()->cnt;
        $result["total4"] = $this->db->query($sql)->row()->total;


        //-- 총매출
        $sql="SELECT   IFNULL( SUM(calmaindtl_totalprice ) ,0) AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch
   in(select branch_serial from calculate_taxinfo where registration_number='$registration_number')   ";
        $result["cnt5"] = $this->db->query($sql)->row()->cnt;
        $result["total5"] = $this->db->query($sql)->row()->total;


        //-- 총 정산
        $sql="SELECT  COUNT(*) AS cnt , IFNULL( SUM(calmaindtl_calamount ) ,0) AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' 
AND calmaindtl_branch   in(select branch_serial from calculate_taxinfo where registration_number='$registration_number')  AND calmaindtl_gubn='1' ";
        $result["cnt6"] = $this->db->query($sql)->row()->cnt;
        $result["total6"] = $this->db->query($sql)->row()->total;

        return $result;
    }



    // 브랜치 시리얼 번호로  정산 마스터,마스터에 관련된 지점의 정산정보가져오기
    function getCalinfoDetailByBranchcode($calmainmst_code, $calmaindtl_branch)
    {
        // 정산 마스터 정보 1건
        $sql="select *   from   caculate_calmainmst a where calmainmst_code='$calmainmst_code'";
        $arr_mst = $this->db->query($sql)->result_array();


        // 업체별 정산 상세정보 - 우측 리스트
        $sql="SELECT * FROM caculate_calmaindtl      WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch'  ";
        $arr_dtl = $this->db->query($sql)->result_array();

        // 메모
        $sql="select * from caculate_calbranch where  calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch'";
        $arr_calbranchdtl = $this->db->query($sql)->result_array();

        $data["mst"]=$arr_mst;
        $data["dtl"]=$arr_dtl;
        $data["sumdata"]=$this->getBranchsumdataByBranchcode($calmainmst_code, $calmaindtl_branch);
        $data["calbranchdtl"]=$arr_calbranchdtl;

        return $data;

    }

    // 브랜치 시리얼 번호로   각 지점의 정산 합산 정보가져오기
    function getBranchsumdataByBranchcode($calmainmst_code, $calmaindtl_branch){
        //-- 정상결제
        $sql="SELECT COUNT(*) AS cnt , IFNULL(SUM(calmaindtl_totalprice ),0)  AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch'";

        $result["cnt1"] = $this->db->query($sql)->row()->cnt;
        $result["total1"] = $this->db->query($sql)->row()->total;


        //-- 단기렌트
        $sql="SELECT COUNT(*) AS cnt ,IFNULL( SUM(calmaindtl_totalprice ) ,0) AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch' AND calmaindtl_period='1' ";
        $result["cnt2"] = $this->db->query($sql)->row()->cnt;
        $result["total2"] = $this->db->query($sql)->row()->total;


        //-- 월렌트
        $sql="SELECT COUNT(*) AS cnt , IFNULL( SUM(calmaindtl_totalprice ) ,0) AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch'  AND calmaindtl_period='2'  ";
        $result["cnt3"] = $this->db->query($sql)->row()->cnt;
        $result["total3"] = $this->db->query($sql)->row()->total;


        // -- 취소수수료
        $sql=" SELECT COUNT(*) AS cnt , IFNULL( SUM(calmaindtl_totalprice ) ,0) AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch'   AND calmaindtl_gubn='2'  ";
        $result["cnt4"] = $this->db->query($sql)->row()->cnt;
        $result["total4"] = $this->db->query($sql)->row()->total;


        //-- 총매출
        $sql="SELECT   IFNULL( SUM(calmaindtl_totalprice ) ,0) AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch'   ";
        $result["cnt5"] = $this->db->query($sql)->row()->cnt;
        $result["total5"] = $this->db->query($sql)->row()->total;


        //-- 총 정산
        $sql="SELECT  COUNT(*) AS cnt , IFNULL( SUM(calmaindtl_calamount ) ,0) AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch'  AND calmaindtl_gubn='1' ";
        $result["cnt6"] = $this->db->query($sql)->row()->cnt;
        $result["total6"] = $this->db->query($sql)->row()->total;

        return $result;
    }

    //브랜치 시리얼  번호에 따른 상세 정산리스트 가져오기
    function getcalbranchdtlByBranchcode($calmainmst_code,$calmaindtl_branch){
        $sql="SELECT  b.name AS companyname, a.branchName AS branchname  ,d.*
FROM   new_rentCompany_branch  a INNER JOIN new_rentCompany b
INNER JOIN (SELECT calmaindtl_branch ,dtlcnt AS calcnt, calculate_yn,  bankins_yn,  taxmail_yn FROM caculate_calbranch WHERE calmainmst_code='$calmainmst_code'  GROUP BY calmaindtl_branch)  c 
INNER JOIN (select * from calculate_taxinfo where branch_serial='$calmaindtl_branch') d  
ON a.rentCompany_serial =b.serial AND a.serial =c.calmaindtl_branch AND c.calmaindtl_branch= d.branch_serial
WHERE a.serial =$calmaindtl_branch  LIMIT 1 ";

        $arr_dtl =$this->db->query($sql)->result_array();
        return $arr_dtl[0];
    }


    // 브랜치 시리얼 번호로 정산 대산 지점 회사정보 리스트 가져오기
    function getcalbranchcompanyinfoByBranchcode($calmaindtl_branch){
        $sql="SELECT  b.name AS companyname, a.branchName AS branchname  ,d.*
FROM   new_rentCompany_branch  a INNER JOIN new_rentCompany b
INNER JOIN calculate_taxinfo d  
ON a.rentCompany_serial =b.serial AND a.serial  = d.branch_serial WHERE a.serial=$calmaindtl_branch LIMIT 1 ";

        $arr_dtl =$this->db->query($sql)->result_array();
        return $arr_dtl[0];
    }


    // 정산 데이터 정보의 지점간 이동처리 - 이동대상지점의 company_code 가 업데이트 되어야함
    function changebranch($data)
    {
        $calmainmst_code=$data["calmainmst_code"];
        $selectbranch=$data["selectbranch"];
        $nowbranch=$data["nowbranch"];
        $strcalmaindtl_idx=$data["strcalmaindtl_idx"];

        // 이동대상의 company code 가져오기
        $sql="SELECT rentCompany_serial FROM new_rentCompany_branch WHERE SERIAL='$selectbranch'";
        $calmaindtl_company = $this->db->query($sql)->row()->rentCompany_serial;

        $arrcalmaindtl_idx =explode("-",$strcalmaindtl_idx);
        $joinstr = join(",",$arrcalmaindtl_idx);
        $sql="update caculate_calmaindtl set calmaindtl_company='$calmaindtl_company', calmaindtl_branch='$selectbranch' where calmaindtl_idx in($joinstr)";
        $this->db->query($sql);

        $sql="select 
        (select count(*) from caculate_calmaindtl where calmainmst_code='$calmainmst_code'  and  calmaindtl_company='$calmaindtl_company' and  calmaindtl_branch='$selectbranch') as selectcnt 
        ,(select count(*) from caculate_calmaindtl where calmainmst_code='$calmainmst_code' and  calmaindtl_branch='$nowbranch') as nowcnt ";
        $selectcnt = $this->db->query($sql)->row()->selectcnt;
        $nowcnt = $this->db->query($sql)->row()->nowcnt;


        $sql="update caculate_calbranch set dtlcnt='$selectcnt' where calmainmst_code='$calmainmst_code' and  calmaindtl_company='$calmaindtl_company'  and  calmaindtl_branch='$selectbranch' ";
        $this->db->query($sql);
        $sql="update caculate_calbranch set dtlcnt='$nowcnt' where calmainmst_code='$calmainmst_code' and  calmaindtl_branch='$nowbranch' ";
        $this->db->query($sql);
    }

    // 정산 데이터별 수정처리
    function changedtlrow($data){

        $calmaindtl_idx = $data["calmaindtl_idx"];
        $calmaindtl_totalprice = $data["calmaindtl_totalprice"];
        $calmaindtl_calamount = $data["calmaindtl_calamount"];
        $calmaindtl_rentfee = $data["calmaindtl_rentfee"];

        $car_model = $data["car_model"];
        $rentcar_number = $data["rentcar_number"];

        $sql="update caculate_calmaindtl set  calmaindtl_totalprice='$calmaindtl_totalprice'
            ,calmaindtl_calamount='$calmaindtl_calamount', calmaindtl_rentfee='$calmaindtl_rentfee'
            ,  car_model='$car_model', rentcar_number='$rentcar_number'
             where calmaindtl_idx='$calmaindtl_idx'";

        $this->db->query($sql);

    }

    //정산 정보 취소 ,삭제처리
    function calcelcalculate($data){
        $calmaindtl_idx = $data["calmaindtl_idx"];
        $calmainmst_code = $data["calmainmst_code"];

        $sql="delete from caculate_calmainmst where calmainmst_code='$calmainmst_code'";
        $this->db->query($sql);
        $sql="delete from caculate_calmaindtl where calmainmst_code='$calmainmst_code'";
        $this->db->query($sql);
        $sql="delete from caculate_calbranch  where calmainmst_code='$calmainmst_code'";
        $this->db->query($sql);
    }

    // 각 지점 개별 정산  완료 처리
    function calculatecomplete($data){
        $calmainmst_code = $data["calmainmst_code"];
        $calmaindtl_branch = $data["calmaindtl_branch"];

        $sql="update caculate_calmaindtl set calmaindtl_stats='complete'
            where calmainmst_code='$calmainmst_code' and calmaindtl_branch='$calmaindtl_branch'";
        $this->db->query($sql);

        $sql="update caculate_calbranch set calculate_yn='y'
            where calmainmst_code='$calmainmst_code' and calmaindtl_branch='$calmaindtl_branch'";
        $this->db->query($sql);
    }

    // 전체 지점  정산  완료 처리
    function allcalculatecomplete($data){
        $calmainmst_code = $data["calmainmst_code"];

        $sql="update caculate_calmaindtl set calmaindtl_stats='complete' where calmainmst_code='$calmainmst_code'";
        $this->db->query($sql);
        $sql="update caculate_calbranch set calculate_yn='y'  where calmainmst_code='$calmainmst_code'";
        $this->db->query($sql);
        $sql="update caculate_calmainmst set calmainmst_step='step3'  where calmainmst_code='$calmainmst_code'";
        $this->db->query($sql);

        // 정산내역 업데이트
        $sql="SELECT calmainmst_code,calmaindtl_company, calmaindtl_branch
            , SUM(calmaindtl_totalprice) AS sumtotalprice,SUM(calmaindtl_calamount) AS sumcalamount
            ,SUM(calmaindtl_rentfee) AS sumrentfee,SUM(calmaindtl_discount) AS sumdiscount
            FROM caculate_calmaindtl WHERE calmainmst_code='$calmainmst_code'
             GROUP BY calmaindtl_company, calmaindtl_branch ";

        $arr_cal = $this->db->query($sql)->result_array();


        foreach($arr_cal as $entry)
        {
            $calmainmst_code =$entry["calmainmst_code"];
            $calmaindtl_company =$entry["calmaindtl_company"];
            $calmaindtl_branch =$entry["calmaindtl_branch"];
            $sumtotalprice =$entry["sumtotalprice"];
            $sumcalamount =$entry["sumcalamount"];
            $sumrentfee =$entry["sumrentfee"];
            $sumdiscount =$entry["sumdiscount"];

            //-- 총 정산
            $sql="SELECT  COUNT(*) AS cnt  FROM caculate_calbranch  
                    where  calmainmst_code='$calmainmst_code'  and calmaindtl_company='$calmaindtl_company' 
                                      and  calmaindtl_branch='$calmaindtl_branch'  ";
            $cnt= $this->db->query($sql)->row()->cnt;

            if($cnt <1){
                $sql="INSERT INTO caculate_calbranch(calmainmst_code,calmaindtl_company,calmaindtl_branch,dtlcnt) 
                      SELECT calmainmst_code,calmaindtl_company,calmaindtl_branch,COUNT(*) AS cnt FROM caculate_calmaindtl WHERE  calmainmst_code='$calmainmst_code'
                         AND    calmaindtl_company='$calmaindtl_company'   AND  calmaindtl_branch='$calmaindtl_branch'   GROUP BY calmainmst_code,calmaindtl_company,calmaindtl_branch ";
                $this->db->query($sql);
            }

            $sql="update caculate_calbranch  set sumtotalprice='$sumtotalprice',  sumcalamount='$sumcalamount'
                ,sumrentfee='$sumrentfee',  sumdiscount='$sumdiscount' 
               where  calmainmst_code='$calmainmst_code'  and calmaindtl_company='$calmaindtl_company' 
                                  and  calmaindtl_branch='$calmaindtl_branch' ";
            $this->db->query($sql);



        }

        exit();
    }

    //세금계산서, 입금처리를 위한 리스트 가져오기 
    function taxbanklist($calmainmst_code){
        
        // 정산할 내역을 가져온다.
        $sql="SELECT  b.name AS companyname, a.branchName AS branchname,  a.serial AS branchserial
              ,a.rentCompany_serial AS companyserial ,d.*
              ,c.calbranch_idx,c.calmainmst_code,c.calmaindtl_branch
              ,c.dtlcnt,c.calculate_yn,c.bankins_yn,c.taxmail_yn,c.tax_msg
              ,c.sumtotalprice AS sumtotalprice,c.sumcalamount AS sumcalamount 
              ,c.sumrentfee AS sumrentfee,c.sumdiscount AS sumdiscount 
              FROM   new_rentCompany_branch  a INNER JOIN new_rentCompany b
              INNER JOIN (SELECT * FROM caculate_calbranch WHERE calmainmst_code = '$calmainmst_code' GROUP BY calmaindtl_branch)  c
              INNER JOIN calculate_taxinfo d   
               ON a.rentCompany_serial =b.serial AND a.serial =c.calmaindtl_branch  AND d.branch_serial=c.calmaindtl_branch
               WHERE a.serial IN(SELECT calmaindtl_branch FROM caculate_calbranch WHERE calmainmst_code = '$calmainmst_code' GROUP BY calmaindtl_branch )  
                 ORDER BY  calmaindtl_branch ASC";


        $arr_calist = $this->db->query($sql)->result_array();
        return $arr_calist;

    }



    // 세무사 제출용 데이터 리스트 가져오기
    function alltaxbanklist($calmainmst_code){

        // 정산할 내역을 가져온다.
        $sql="SELECT  b.name AS companyname, a.branchName AS branchname,  a.serial AS branchserial
              ,a.rentCompany_serial AS companyserial ,d.*
              ,c.calbranch_idx,c.calmainmst_code,c.calmaindtl_branch
              ,c.dtlcnt,c.calculate_yn,c.bankins_yn,c.taxmail_yn,c.tax_msg
              ,c.sumtotalprice AS sumtotalprice,c.sumcalamount AS sumcalamount 
              ,c.sumrentfee AS sumrentfee,c.sumdiscount AS sumdiscount 
              FROM   new_rentCompany_branch  a INNER JOIN new_rentCompany b
              INNER JOIN (SELECT * FROM caculate_calbranch WHERE calmainmst_code = '$calmainmst_code' and   calculate_yn='y' 
              and   bankins_yn='y' and  taxmail_yn='y' GROUP BY calmaindtl_branch)  c
              INNER JOIN calculate_taxinfo d   
               ON a.rentCompany_serial =b.serial AND a.serial =c.calmaindtl_branch  AND d.branch_serial=c.calmaindtl_branch
               WHERE a.serial IN(SELECT calmaindtl_branch FROM caculate_calbranch WHERE calmainmst_code = '$calmainmst_code' GROUP BY calmaindtl_branch )  
                 ORDER BY  calmaindtl_branch ASC";

        $arr_calist = $this->db->query($sql)->result_array();
        return $arr_calist;

    }



    // 인터넷 뱅킹 일괄 처리를 위한 입금 내역 리스트
    function getranscopylist($data){

        $strcalbranch_idx =$data["strcalbranch_idx"];
        $arrcalbranch_idx =explode("-",$strcalbranch_idx );
        $joinstr = join(",",$arrcalbranch_idx);

        $sql="SELECT bankinfo,bankaccount,sumcalamount,CONCAT(alias,'-정산') AS mytxt 
            ,(SELECT  calmainmst_title fROM  caculate_calmainmst where calmainmst_code=a.calmainmst_code ) as  calmainmst_title
                      FROM caculate_calbranch a
             INNER JOIN  calculate_taxinfo b ON a.calmaindtl_branch=b.branch_serial WHERE sumcalamount>0 
             and calbranch_idx in ($joinstr)";

        $arr_calist = $this->db->query($sql)->result_array();
        return $arr_calist;
    }

    // 정산 대산 지점 회사정보 리스트 가져오기
    function getcalbranchcompanyinfo($calbranch_idx){
        $sql="SELECT  b.name AS companyname, a.branchName AS branchname  ,d.*
FROM   new_rentCompany_branch  a INNER JOIN new_rentCompany b
INNER JOIN (SELECT calmaindtl_branch ,dtlcnt AS calcnt, calculate_yn,  bankins_yn,  taxmail_yn FROM caculate_calbranch WHERE calbranch_idx='$calbranch_idx' GROUP BY calmaindtl_branch)  c 
INNER JOIN calculate_taxinfo d  
ON a.rentCompany_serial =b.serial AND a.serial =c.calmaindtl_branch AND c.calmaindtl_branch= d.branch_serial
WHERE a.serial IN(SELECT calmaindtl_branch FROM caculate_calbranch WHERE calbranch_idx='$calbranch_idx' GROUP BY calmaindtl_branch )  LIMIT 1 ";

        $arr_dtl =$this->db->query($sql)->result_array();
        return $arr_dtl[0];
    }

    // 사업자등록번호로 정산 대산 지점 회사정보 리스트 가져오기
    function getcalbranchcompanyinfobyreginumber($registration_number){
        $sql="SELECT  b.name AS companyname, a.branchName AS branchname  ,d.*
FROM   new_rentCompany_branch  a INNER JOIN new_rentCompany b
INNER JOIN calculate_taxinfo d  
ON a.rentCompany_serial =b.serial AND a.serial  = d.branch_serial
WHERE a.serial IN   (select branch_serial from calculate_taxinfo where registration_number='$registration_number') LIMIT 1 ";

        $arr_dtl =$this->db->query($sql)->result_array();
        return $arr_dtl[0];
    }




    //사업자등록증 번호에 따른 상세 정산리스트 가져오기
    function getcalbranchdtl($calmainmst_code,$registration_number){
        $sql="SELECT  b.name AS companyname, a.branchName AS branchname  ,d.*
FROM   new_rentCompany_branch  a INNER JOIN new_rentCompany b
INNER JOIN (SELECT calmaindtl_branch ,dtlcnt AS calcnt, calculate_yn,  bankins_yn,  taxmail_yn FROM caculate_calbranch WHERE calmainmst_code='$calmainmst_code'  GROUP BY calmaindtl_branch)  c 
INNER JOIN (select * from calculate_taxinfo where registration_number='$registration_number') d  
ON a.rentCompany_serial =b.serial AND a.serial =c.calmaindtl_branch AND c.calmaindtl_branch= d.branch_serial
WHERE a.serial IN  (select branch_serial from calculate_taxinfo where registration_number='$registration_number')   LIMIT 1 ";

        $arr_dtl =$this->db->query($sql)->result_array();
        return $arr_dtl[0];
    }

    // 정산 데이터 정보 상세 삭제
    function delcalmaindtlinfo($calmaindtl_idx){
        $sql="delete from   caculate_calmaindtl where calmaindtl_idx='$calmaindtl_idx'";
        $this->db->query($sql);
    }

    // 각 지점 정산 데이터 정보 개수 갱신 
    function updatecalbranchcnt($calmainmst_code,$calmaindtl_branch){
        $sql ="select count(*) as cnt from caculate_calmaindtl where  calmainmst_code='$calmainmst_code'
            and calmaindtl_branch='$calmaindtl_branch' ";
        $cnt = $this->db->query($sql)->row()->cnt;


        $sql="update caculate_calbranch set dtlcnt='$cnt' where  calmainmst_code='$calmainmst_code'
            and calmaindtl_branch='$calmaindtl_branch' ";
        $this->db->query($sql);
    }

    // 관리자의 신규 정산 정보 데이터 등록처리
    function setnewcaldata($data){
        $calmainmst_code = $data["calmainmst_code"];
        $calmaindtl_company = $data["calmaindtl_company"];
        $calmaindtl_branch  = $data["calmaindtl_branch"];
        $calmaindtl_gubn  =$data["calmaindtl_gubn"];
        $calmaindtl_period  = $data["calmaindtl_period"];
        $driver_name  = $data["driver_name"];
        $calmaindtl_rentstart  = $data["calmaindtl_rentstart"];
        $calmaindtl_rentend  =$data["calmaindtl_rentend"];
        $car_model = $data["car_model"];
        $rentcar_number  = $data["rentcar_number"];
        $calmaindtl_totalprice  = $data["calmaindtl_totalprice"];
        $calmaindtl_calamount  = $data["calmaindtl_calamount"];
        $calmaindtl_rentfee = $data["calmaindtl_rentfee"];
        $calmaindtl_discount = $data["calmaindtl_discount"];


         $sql=" INSERT INTO caculate_calmaindtl (    calmainmst_code,  calmaindtl_company,  calmaindtl_branch,
         calmaindtl_gubn,  calmaindtl_period,  calmaindtl_userserial,  calmaindtl_rentstart,  calmaindtl_rentend,
          calmaindtl_carserial,  calmaindtl_totalprice,  calmaindtl_calamount,  calmaindtl_rentfee,  calmaindtl_discount,
          regdate,  f_reservationidx,  calmaindtl_stats,  driver_name,  rentcar_number,  car_model,  admin_idx)
        VALUES
          (    '$calmainmst_code',    '$calmaindtl_company',    '$calmaindtl_branch',    '$calmaindtl_gubn',
            '$calmaindtl_period',    '0',    '$calmaindtl_rentstart',    '$calmaindtl_rentend',
            '0',    '$calmaindtl_totalprice',    '$calmaindtl_calamount',    '$calmaindtl_rentfee',
            '$calmaindtl_discount',    now(),    '0',    'ready',    '$driver_name',
            '$rentcar_number',    '$car_model',    '0'  );";

        $this->db->query($sql);

        $this->updatecalbranchcnt($calmainmst_code,$calmaindtl_branch);

    }

    // 각정산 정보의 상태 변경
    function setcaculatecalbranchstats($calbranch_idx,$part,$stats){

        $sql="update caculate_calbranch set $part='$stats'   where calbranch_idx='$calbranch_idx'";
        $this->db->query($sql);

    }

    // 각 지점별 정산 정보의 메모 저장하기
    function savecalmemo($calmainmst_code,$calmaindtl_branch,$calmemo){

        $sql="update caculate_calbranch set calmemo='$calmemo'   where  calmainmst_code='$calmainmst_code' and  calmaindtl_branch='$calmaindtl_branch'";
        $this->db->query($sql);

    }

    // 세금계산서 발행을 위해 선택 데이터의 taxmail_yn을 w 로 세팅
    function settaxoffer($data){
        $calmainmst_code =$data["calmainmst_code"];
        $strcalbranch_idx =$data["strcalbranch_idx"];
        $arrcalbranch_idx =explode("-",$strcalbranch_idx );

        for($i=0;$i< sizeof($arrcalbranch_idx);$i++){
            $calbranch_idx = $arrcalbranch_idx[$i];
            $sql="update  caculate_calbranch set taxmail_yn='w' where calbranch_idx='$calbranch_idx'";
            $this->db->query($sql);
        }

        $sql="update caculate_calmainmst set calmainmst_step='step4'  where calmainmst_code='$calmainmst_code'";
        $this->db->query($sql);

    }

    // 정산진행시 - 신규 지점 수동등록하기
    function addcalbranch($calmainmst_code,$calmaindtl_company,$calmaindtl_branch){
        $sql="select count(*) as cnt from  caculate_calbranch where calmainmst_code='$calmainmst_code' 
                and  calmaindtl_company='$calmaindtl_company' and calmaindtl_branch='$calmaindtl_branch'";

        $cnt = $this->db->query($sql)->row()->cnt;

        if($cnt >0){
            $returnarr["result"]="n";
        }else{
            $sql="INSERT INTO caculate_calbranch (  calmainmst_code,  calmaindtl_company,  calmaindtl_branch,  dtlcnt,
                calculate_yn,  bankins_yn,  taxmail_yn,  sumtotalprice,  sumcalamount,  sumrentfee,  sumdiscount
                  ,  calmemo,  tax_code,  tax_msg)
                    VALUES  (    '$calmainmst_code',    '$calmaindtl_company',    '$calmaindtl_branch',    '0',
                        'n',    'n',    'n',    '0',    '0',    '0',    '0',    '',    '',    ''  );";

            $this->db->query($sql);
            $returnarr["result"]="y";
        }

        return $returnarr;
    }



}