<?php
/**
 * Taxinfo_model - 세금계산서 발행, 사업자등록증 관련 처리
 */

class Taxinfo_model extends CI_Model {

    const SEARCH_PARAMETER_COMPANY_NAME = 'company_name';

    function __construct()
    {
        parent::__construct();
    }

    // 정산 환경설정 가져오기
    function getCalculateconfig(){
        $sql="select * from calculate_configinfo limit 1  ";
        $query = $this->db->query($sql)->result_array();
        $row = $query[0];
        return $row;
    }

    // 이메일 발송 html 정보 수정하기
    function setemailform($data){
        $emailtitle =$data["emailtitle"];
        $emailform =$data["emailform"];

        $sql="update calculate_configinfo set  emailtitle='$emailtitle',emailform='$emailform'";
        $this->db->query($sql);

    }

    // 팀오투 사업자등록증 정보 설정하기
    function setaxform($data){
 
        $company_name =$data["company_name"];
        $ceo_name =$data["ceo_name"];
        $addr =$data["addr"];
        $biztype =$data["biztype"];
        $bizclass =$data["bizclass"];
        $tax_email =$data["tax_email"];
        $company_number =$data["company_number"];

        $sql="update calculate_configinfo set  company_name='$company_name',ceo_name='$ceo_name',addr='$addr'
            ,biztype='$biztype',bizclass='$bizclass',tax_email='$tax_email',company_number='$company_number'";
        $this->db->query($sql);
    }

    //세금계산서 발행 로그 카운트
    function getTaxpaperlogCount($sp="",$sv=""){
        $sql="select count(*) as cnt from  calculate_taxpaperlog where 1=1";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    // 세금계산서 발행 정보 리스트
    function getTaxpaperlogList($page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select *   from   calculate_taxpaperlog a where 1=1  ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by taxpaperlog_idx desc limit $start,20";

        $query = $this->db->query($sql);

        return $query->result();
    }

    // 세금계산서 정보 등록하기 
    function setaxpaperlog($data){

        $taxkey =$data["taxkey"];
        $mailtitle=$data["mailtitle"];
        $mycorpnum=$data["mycorpnum"];
        $mycorpname=$data["mycorpname"];
        $myceoname=$data["myceoname"];
        $myaddr=$data["myaddr"];
        $myemail=$data["myemail"];
        $amounttotal=$data["amounttotal"];
        $vattotal=$data["vattotal"];
        $totalamount=$data["totalamount"];
        $clientcorpnum=$data["clientcorpnum"];
        $clientcorpname=$data["clientcorpname"];
        $clientceoname=$data["clientceoname"];
        $clientcontactname=$data["clientcontactname"];
        $clientaddr=$data["clientaddr"];
        $clientbiztype=$data["clientbiztype"];
        $clientbizclass=$data["clientbizclass"];
        $stats=$data["stats"];

        
        $sql="INSERT INTO calculate_taxpaperlog (   taxkey,  mailtitle,  mycorpnum,  mycorpname,  myceoname,  myaddr,
  myemail,  amounttotal,  totalamount,  clientcorpnum,  clientcorpname,  clientceoname,  clientcontactname,  clientaddr,
  clientbiztype,  clientbizclass,  stats,  regdate,  vattotal)
VALUES  (    '$taxkey',    '$mailtitle',    '$mycorpnum',    '$mycorpname',    '$myceoname',    '$myaddr',    '$myemail',
    '$amounttotal',    '$totalamount',    '$clientcorpnum',   '$clientcorpname',    '$clientceoname',    '$clientcontactname',
        '$clientaddr',    '$clientbiztype',    '$clientbizclass',    '$stats',   now(),    '$vattotal'  )";


        $query = $this->db->query($sql);

    }


    //////////////////////// branch tax paper info  ////////////////////////

    //  업체별 사업자등록증 리스트 숫자
    function getBranchTaxinfoCount ($sp = "", $sv = "")
    {
        $added_query = ($sp !== '' && $sv !== '')? " AND $sp LIKE '%$sv%'" : "";

        $sql="
        SELECT count(*) cnt
        FROM
            (
            SELECT
                branchName,
                c.serial mem_serial ,c.id mem_id, c.regdate mem_regdate, 
                c.name mem_name,c.authority authority,c.rentCompany_branch_serial rentCompany_branch_serial
            FROM
                (
                SELECT nrcb.serial, branchName, nrc.name company_name
                FROM
                    new_rentCompany_branch nrcb INNER JOIN
                    new_rentCompany nrc
                    ON nrcb.rentCompany_serial=nrc.serial
                WHERE nrc.state <> '0'
                ) a LEFT JOIN
                new_member c 
                ON
                    a.SERIAL =c.rentCompany_branch_serial 
            WHERE  c.state <>'9' ".$added_query."
            GROUP BY
                rentCompany_serial, rentCompany_branch_serial
            ) inventory";

        $cnt = $this->db->query($sql)->row()->cnt;

        if ($sp !== '' && $sv !== '') {
            switch ($sp) {
                case self::SEARCH_PARAMETER_COMPANY_NAME:
                    $added_query = " AND waa_name LIKE '%$sv%' ";
                    break;
                default:
                    $added_query = "";
                    break;
            }

        }

        $query = "
        SELECT
            count(*) cnt
        FROM
            workspace_api_affiliate waa INNER JOIN
            workspace_api_affiliate_branch waab
            ON
                waa_idx=waab_waa_idx
        WHERE waa_state=1 AND waab_state=1 ".$added_query;

        $api_cnt = $this->db->query($query)->row()->cnt;

        return (int)$cnt + (int)$api_cnt;
    }

    // 업체별 사업자등록증 리스트
    function getBranchTaxinfoPageList($page=1, $pageline=20, $sp="", $sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;

        $added_query = ($sp !== '' && $sv !== '')? " AND $sp LIKE '%$sv%'" : "";
        $added2_query = "";
        if ($sp !== '' && $sv !== '') {
            switch ($sp) {
                case self::SEARCH_PARAMETER_COMPANY_NAME:
                    $added2_query = " AND waa_name LIKE '%$sv%' ";
                    break;
                default:
                    $added2_query = "";
                    break;
            }

        }

//
        $sql="
        SELECT
            *
        FROM
            (
                (
                SELECT
                    branch_data.*,
                    nm.name mem_name,
                    (SELECT COUNT(*) FROM calculate_taxinfo WHERE branch_serial=branch_data.serial) taxinfocnt
                FROM
                    (
                    SELECT
                        nrc.name company_name, nrc.serial rentCompany_serial,
                        nrcb.serial, nrcb.branchName, nrcb.regdate
                    FROM
                        new_rentCompany nrc INNER JOIN
                        new_rentCompany_branch nrcb
                        ON
                            nrcb.rentCompany_serial=nrc.serial
                    WHERE
                        nrc.state <> '0' AND nrcb.state <> '0'
                    ) branch_data LEFT JOIN
                    new_member nm
                    ON
                        branch_data.SERIAL = nm.rentCompany_branch_serial  ".$added_query."
                WHERE nm.state <>'9'
                GROUP BY rentCompany_serial, rentCompany_branch_serial
                ) UNION ALL
                (
                SELECT
                    waa_name, CONCAT('AP', waab_idx) serial, CONCAT('AP', waa_idx) rentCompany_serial, IFNULL(waab_name, '') branchName, waab_register_date,
                    '', 
                    (SELECT COUNT(*) FROM calculate_taxinfo WHERE branch_serial=CONCAT('API', waab_idx)) taxinfocnt   
                FROM
                    workspace_api_affiliate waa INNER JOIN
                    workspace_api_affiliate_branch waab
                    ON
                        waa_idx=waab_waa_idx
                WHERE  
                    waa_state=1 AND waab_state=1 ".$added2_query."
                ) 
            ) data
        ORDER BY
            regdate DESC
        LIMIT
          ".$start.", 20";

        $query = $this->db->query($sql);

        return $query->result();
    }

    // 각 지점별 사업자등록증 상세가져오기
    function getBranchTaxinfoDetail($branch_serial){
        $sql="select * from calculate_taxinfo where branch_serial='$branch_serial'";
        return $this->db->query($sql)->row();
    }

    //각 기업별 사업자등록증 정보 등록,수정하기
    function savetaxinfo($data){
        $branch_serial=$data["branch_serial"] ;
        $registration_number=$data["registration_number"];
        $company_name=$data["company_name"];
        $ceo_name=$data["ceo_name"];
        $addr=$data["addr"] ;
        $biztype=$data["biztype"];
        $bizclass=$data["bizclass"];
        $tax_email=$data["tax_email"];
        $cal_email=$data["cal_email"];
        $tax_email2=$data["tax_email2"];
        $cal_email2=$data["cal_email2"];
        $contactname=$data["contactname"];


        $bankinfo=$data["bankinfo"];
        $bankaccount=$data["bankaccount"];

        $alias=$data["alias"];

        $sql="select count(*) as cnt from  calculate_taxinfo where branch_serial='$branch_serial'";
        $cnt = $this->db->query($sql)->row()->cnt;

        if($cnt >0){
            $sql="update calculate_taxinfo set registration_number='$registration_number',company_name='$company_name'
                ,ceo_name='$ceo_name',  addr='$addr',  bizclass='$bizclass',  biztype='$biztype'
                ,tax_email='$tax_email',cal_email='$cal_email',tax_email2='$tax_email2',cal_email2='$cal_email2'
                ,bankaccount='$bankaccount',  bankinfo='$bankinfo' ,alias='$alias',contactname='$contactname'
                where branch_serial='$branch_serial'";

        }else{
            $sql="INSERT INTO calculate_taxinfo (  branch_serial,  registration_number,  company_name,
                  ceo_name,  addr,   tax_email,cal_email,   tax_email2,cal_email2,  bankinfo, bankaccount
                  ,alias,contactname,bizclass,biztype)
                  VALUES  (    '$branch_serial',    '$registration_number',    '$company_name',
                      '$ceo_name',    '$addr',   '$tax_email','$cal_email',   '$tax_email2','$cal_email2'
                      ,    '$bankinfo','$bankaccount' ,'$alias' ,'$contactname' ,'$bizclass','$biztype');";
        }

        $this->db->query($sql);
    }

}