<?php
/**
 * AppUser_model - 카모아 앱 회원 정보
 */
class AppUser_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    //카모아 회원 숫자
    function getAppUserCount($sp="",$sv=""){
        $sql="select count(*) as cnt from  tbl_usr_list where usrstatus in ('1','5','8','9')  ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    //카모아 회원 정보리스트
    function getAppUserPageList($page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select *  
                ,(SELECT count(*) FROM new_coupon_list where user_serial=a.usrserial and status='0'  )AS couponcnt
                ,(SELECT count(*) FROM admin_board where usrserial=a.usrserial and complain_yn='y' )AS tarcomplaincnt
                ,(SELECT count(*) FROM tbl_pay_log x inner join tbl_reservation_list y on x.f_reservationidx=y.f_reservationidx
                 WHERE  x.f_usrserial=a.usrserial and y.f_paystatus='1' )AS paycnt
                ,(SELECT sum(f_Amt) FROM tbl_pay_log x inner join tbl_reservation_list y on x.f_reservationidx=y.f_reservationidx
                 WHERE  x.f_usrserial=a.usrserial and y.f_paystatus='1')AS paysum
                ,(SELECT count(*) FROM tbl_pay_log x inner join tbl_reservation_list y on x.f_reservationidx=y.f_reservationidx
                 WHERE  x.f_usrserial=a.usrserial and y.f_paystatus='2' )AS cancelcnt
                ,(SELECT sum(f_Amt) FROM tbl_pay_log x inner join tbl_reservation_list y on x.f_reservationidx=y.f_reservationidx
                 WHERE  x.f_usrserial=a.usrserial and y.f_paystatus='2'  )AS cancelsum
          ,(SELECT count(*) FROM admin_board WHERE  usrserial=a.usrserial  )AS complaincnt
          from   tbl_usr_list a where usrstatus in ('1','5','8','9')  ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by jdate desc limit $start,20";

        $query = $this->db->query($sql);

        return $query->result();
    }

}