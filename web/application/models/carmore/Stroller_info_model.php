<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Stroller_info_model
 *
 * @author DEN
 *
 * @property CI_Loader load
 *
 * @property Workspace_common_func  $workspace_common_func
 */
class Stroller_info_model extends WS_Model {
    private $key_to_database = [
        'name'      => 'cs_name',
        'type'      => 'cs_type',
        'plusId'    => 'cs_kakao_plus_friend_id',
        'plusUrlId' => 'cs_kakao_plus_friend_url_id',
        'logo'      => 'cs_logo',
        'address'   => 'cs_address',
        'tel'       => 'cs_contact',
        'hashTag1'  => 'cs_hash_tag1',
        'hashTag2'  => 'cs_hash_tag2',
        'hashTag3'  => 'cs_hash_tag3',
        'hashTag4'  => 'cs_hash_tag4',
        'hashTag5'  => 'cs_hash_tag5',
        'memo'      => 'cs_memo'
    ];

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Workspace_common_func');
    }

    /**
     * 유모차 업체 추가하기
     *
     * @param $obj
     *
     * @return stdClass
     */
    public function create_stroller_information($obj)
    {
        $return_object = new stdClass();

        $is_it_first = true;
        $col         = "";
        $set         = "";

        foreach ($obj as $key => $value)
        {
            if ($key === "matchingAffiliate")
                continue;

            if (isset($this->key_to_database[$key]))
            {
                if ($is_it_first)
                    $is_it_first = false;
                else
                {
                    $col .= ", ";
                    $set .= ", ";
                }

                $col .= $this->key_to_database[$key];
                $set .= "'".$value."'";

            }
        }

        if ($col !== "" && $set !== "")
        {
            $query = "INSERT INTO carmore_stroller (".$col.") VALUE (".$set.")";

            $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $sql->execute();

            $return_object->result = 1;
            $return_object->insertedIdx = $this->db->conn_id->lastInsertId();
        }

        return $return_object;
    }

    /**
     * 유모차 업체 정보 가져오기
     *
     * @param $stroller_idx int
     *
     * @return stdClass
     */
    public function read_stroller_information($stroller_idx)
    {
        $return_object = new stdClass();

        $query = "
        SELECT
            cs_name, cs_type, cs_kakao_plus_friend_id, cs_kakao_plus_friend_url_id,
            cs_contact, cs_logo, cs_address,
            cs_hash_tag1, cs_hash_tag2, cs_hash_tag3, cs_hash_tag4, cs_hash_tag5, cs_memo,
            IFNULL(
              (SELECT fileSize FROM admin_uploadfile
              WHERE
                  content_code=CONCAT('stlg_', :idx) AND fileuptype='stroller_logo' AND board_code='stroller' AND fileSaveName=cs_logo), '') AS file_size
        FROM carmore_stroller WHERE cs_idx=:idx AND cs_state=1";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->bindParam(':idx', $stroller_idx);
        $sql->execute();
        if ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            $logo_info = new stdClass();

            $logo_info->logo = htmlspecialchars($stmt['cs_logo']);
            $logo_info->size = htmlspecialchars($stmt['file_size']);

            if ($logo_info->logo !== "")
                $logo_info->logo = "https://s3.ap-northeast-2.amazonaws.com/carmoreweb/stroller/stlg_$stroller_idx/".$logo_info->logo;

            $hash_tag_arr = [];
            for ($i = 1; $i <= 5; $i++)
            {
                $hash_tag = $stmt['cs_hash_tag'.$i];

                if ($hash_tag !== "")
                    array_push($hash_tag_arr, [
                        'key'   => $i,
                        'value' => $hash_tag
                    ]);
            }

            $info_object = new stdClass();

            $info_object->name              = htmlspecialchars($stmt['cs_name']);
            $info_object->type              = (int)$stmt['cs_type'];
            $info_object->plusId            = htmlspecialchars($stmt['cs_kakao_plus_friend_id']);
            $info_object->plusUrlId         = htmlspecialchars($stmt['cs_kakao_plus_friend_url_id']);
            $info_object->contact           = $this->workspace_common_func->format_tel(htmlspecialchars($stmt['cs_contact']));
            $info_object->logoInfo          = $logo_info;
            $info_object->address           = htmlspecialchars($stmt['cs_address']);
            $info_object->hashTag           = $hash_tag_arr;
            $info_object->memo              = htmlspecialchars($this->workspace_common_func->replace_br_to_front(($stmt['cs_memo'])));
            $info_object->matchingInventory = $this->get_matching_affiliate_inventory($stroller_idx);
            $return_object->result = 1;
            $return_object->info   = $info_object;

            return $return_object;
        }
        else
        {
            $return_object->result = 2;

            return $return_object;
        }
    }

    /**
     * 유모차 업체 수정하기
     *
     * @param $cs_idx int
     * @param $obj
     *
     * @return stdClass
     */
    public function update_stroller_information($cs_idx, $obj)
    {
        $return_object = new stdClass();

        $is_it_first               = true;
        $set                       = "";
        $change_affiliate_matching = false;

        foreach ($obj as $key => $value)
        {
            if ($key === "matchingAffiliate")
            {
                $change_affiliate_matching = true;
                continue;
            }

            if (isset($this->key_to_database[$key]))
            {
                if ($is_it_first)
                    $is_it_first = false;
                else
                    $set .= ", ";

                $set .= $this->key_to_database[$key]."='".$value."'";

            }
        }

        if ($set !== "")
        {
            $query = "UPDATE carmore_stroller SET ".$set." WHERE cs_idx=?";

            $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $sql->execute([$cs_idx]);
        }

        $return_object->result         = 1;
        $return_object->matchingChange = $change_affiliate_matching;

        return $return_object;
    }

    /**
     * 유모차 업체 삭제하기
     *
     * @param $cs_idx int
     */
    public function delete_stroller_information($cs_idx)
    {
        $query = "UPDATE carmore_stroller SET cs_state=0 WHERE cs_idx=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$cs_idx]);
    }

    /**
     * 로고 삭제
     *
     * @author DEN
     *
     * @param $cs_idx
     */
    public function delete_logo_information($cs_idx)
    {
        $query = "UPDATE carmore_stroller SET cs_logo='' WHERE cs_idx=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$cs_idx]);
    }

    /**
     * 업체 매칭작업
     *
     * @param $cs_idx int 유모차아이디
     * @param $obj
     *
     * @return stdClass
     */
    public function matching_stroller($cs_idx, $obj)
    {
        $ret_information = new stdClass();

        if (isset($obj['newInven']))
            $this->matching_stroller_affiliate($cs_idx, $obj['newInven']);


        if (isset($obj['delInven']))
            $this->unmatching_stroller_affiliate($cs_idx, $obj['delInven']);

        $ret_information->result = 1;

        return $ret_information;
    }

    /**
     * 매칭리스트 가져오기
     *
     * @param $cs_idx
     *
     * @return array
     */
    public function get_matching_affiliate_inventory($cs_idx)
    {
        $inventory = [];

        $query = "
        SELECT
            *
        FROM
            (
                (
                SELECT
                    csb_associate_type, csb_associate_idx,
                    TRIM(CONCAT(nrc.name, ' ', nrcb.branchName)) name
                FROM
                    carmore_stroller_binding csb INNER JOIN
                    new_rentCompany_branch nrcb INNER JOIN
                    new_rentCompany nrc
                    ON
                        csb_associate_idx=nrcb.serial AND nrcb.rentCompany_serial=nrc.serial
                WHERE csb_cs_idx=:csIdx AND csb_associate_type='1'
                ) UNION ALL
                (
                SELECT
                    csb_associate_type, csb_associate_idx,
                    TRIM(CONCAT(waa_name, ' ', IFNULL(waab_name, ''))) name
                FROM
                    carmore_stroller_binding csb INNER JOIN
                    workspace_api_affiliate_branch waab INNER JOIN
                    workspace_api_affiliate waa
                    ON
                        csb_associate_idx=waab.waab_idx AND waab_waa_idx=waa.waa_idx
                WHERE csb_cs_idx=:csIdx AND csb_associate_type='2'
                )
            ) data";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->bindParam(':csIdx', $cs_idx);
        $sql->execute();
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            $affi_obj = new stdClass();

            $affi_obj->type = (int)$stmt['csb_associate_type'];
            $affi_obj->idx  = (int)$stmt['csb_associate_idx'];
            $affi_obj->name = $stmt['name'];

            array_push($inventory, $affi_obj);
        }

        return $inventory;
    }

    /**
     * 매칭시키기
     *
     * @param $cs_idx
     * @param $matching_obj
     */
    private function matching_stroller_affiliate($cs_idx, $matching_obj)
    {
        $is_it_first = TRUE;
        $value       = "";

        foreach ($matching_obj as $obj)
        {
            $type = $obj['type'];
            $idx  = $obj['idx'];

            if ($is_it_first)
                $is_it_first = FALSE;
            else
                $value .= ", ";

            $value .= "($cs_idx, $type, $idx)";

            $this->unmatching_stroller_affiliate($cs_idx, [
                [
                    'type' => $type,
                    'idx'  => $idx
                ]
            ]);
        }

        $query = "INSERT INTO carmore_stroller_binding (csb_cs_idx, csb_associate_type, csb_associate_idx) VALUES ".$value;

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute();
    }

    /**
     * 매칭해제하기     *
     * @param $cs_idx
     * @param $unmatching_obj
     */
    private function unmatching_stroller_affiliate($cs_idx, $unmatching_obj)
    {
        foreach ($unmatching_obj as $obj)
        {
            $query = "DELETE FROM carmore_stroller_binding WHERE csb_cs_idx=? AND csb_associate_type=? AND csb_associate_idx=?";

            $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $sql->execute([$cs_idx, $obj['type'], $obj['idx']]);

        }

    }

}
