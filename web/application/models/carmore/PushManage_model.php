<?php
/**
 * Created by PhpStorm.
 * User: mrclick
 * Date: 2018-07-05
 * Time: 오후 6:40
 */

class PushManage_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function getPushlogCount($sp="",$sv=""){
        $sql="select count(*) as cnt from  tbl_push_log where  pushlog_stats in('n','i','y') ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }


    function getPushlogPageList($page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select *  from   tbl_push_log    where pushlog_stats in('n','i','y') ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by pushlog_idx desc limit $start,20";

        $query = $this->db->query($sql);

        return $query->result();
    }


    function setPushLogInfo($data){
        $emode=$data["emode"];

        if($emode=="new"){

            $nowtime =date("YmdHis");
            $sql = "INSERT INTO tbl_push_log 	(pushlog_type,  pushlog_plat,  pushlog_stats,  pushlog_text,  regdate)
				VALUES('".$data["pushlog_type"]."'	,'".$data["pushlog_plat"]."','n'
				,'".$data["pushlog_text"]."',now())";

            $this->db->query($sql);
            $return_v="y";

        }else if($emode=="edit"){

            $sql="UPDATE  tbl_push_log SET  pushlog_type = '".$data["pushlog_type"]."',pushlog_plat = '".$data["pushlog_plat"]."' 
                ,  pushlog_stats = '".$data["pushlog_stats"]."',pushlog_text = '".$data["pushlog_text"]."' 
                 WHERE pushlog_idx ='".$data["pushlog_idx"]."' ";
            $this->db->query($sql);
            $return_v="y";

        }else if($emode=="del"){

            $sql="UPDATE  tbl_push_log SET    pushlog_stats = 'd'    WHERE pushlog_idx ='".$data["pushlog_idx"]."' ";

            $this->db->query($sql);
            $return_v="y";
        }

        return $return_v;
    }


}