<?php
/**
 * Pay_model - 결제 로그 관련 db
 */
class Pay_model   extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    // 결제 로그 카운트
    function getPaylogCount($sp="",$sv=""){
        $sql="SELECT count(*) as cnt FROM  tbl_pay_log   a INNER JOIN tbl_usr_list b 
              inner join  tbl_reservation_list c 
              ON a.f_usrserial=b.usrserial and a.f_reservationidx=c.f_reservationidx  where 1=1 ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    //결제로그 페이지 리스트
    function getPaylogPageList($page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select *  from   tbl_pay_log  a INNER JOIN tbl_usr_list b 
              inner join  tbl_reservation_list c 
              ON a.f_usrserial=b.usrserial and a.f_reservationidx=c.f_reservationidx where 1=1 ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" ORDER BY f_paylogidx DESC  limit $start,20";

        $query = $this->db->query($sql);

        return $query->result();
    }

    //결제 로그 등록처리
    function procPay($data){
        $nowtime =date("YmdHis");
        $sql = "INSERT INTO tbl_pay_log 	(f_reservationidx,  f_bookingid,  f_usrserial,  f_logdate,  f_logstatus,
                  f_resultcode,  f_resultmsg,  f_AuthDate,  f_AuthCode,  f_BuyerName,  f_MallUserID,  f_GoodsName,  f_MID,  f_TID,
                  f_Moid,  f_Amt,  f_CardQuota,  f_CardCode,  f_CardName,  f_CardNo,  f_logmsg)
				VALUES('".$data["f_reservationidx"]."'	,'".$data["f_bookingid"]."','".$data["f_usrserial"]."',now()			
				,'".$data["f_logstatus"]."','".$data["f_resultcode"]."','','','','','','','','','','".$data["f_Amt"]."'
				,'','','','','');";

        $this->db->query($sql);
        $return_v="y";
        return $return_v;
    }

}