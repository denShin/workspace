<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Stroller_info_model
 *
 * @author DEN
 *
 * @property CI_Loader load
 */

class Special_location_model extends WS_Model {
    const AIRPORT      = 1;
    const KTX          = 2;
    const SRT          = 3;
    const BUS_TERMINAL = 4;
    const AREA         = 5;

    public function __construct()
    {
        parent::__construct();

    }

    private function create_special_location_object()
    {
        $location_object = new stdClass();

        $location_object->airport      = [];
        $location_object->ktx          = [];
        $location_object->srt          = [];
        $location_object->bus_terminal = [];
        $location_object->area         = [];

        return $location_object;
    }

    private function allocate_special_location_to_object(&$special_location_object, $location_name, $location_idx, $location_type)
    {
        $location_object = new stdClass();

        $location_object->idx = $location_idx;
        $location_object->name = $location_name;

        switch ((int)$location_type)
        {
            case self::AIRPORT:
                array_push($special_location_object->airport, $location_object);
                break;
            case self::KTX:
                array_push($special_location_object->ktx, $location_object);
                break;
            case self::SRT:
                array_push($special_location_object->srt, $location_object);
                break;
            case self::BUS_TERMINAL:
                array_push($special_location_object->bus_terminal, $location_object);
                break;
            case self::AREA:
                array_push($special_location_object->area, $location_object);
                break;
        }

    }

    public function get_special_location_inventory()
    {
        $location_object = $this->create_special_location_object();

        $query = "SELECT serial, name, nsl_cslc_idx FROM new_special_location WHERE state=1";

        $sql = $this->slave->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute();
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC)) {
            $this->allocate_special_location_to_object($location_object, $stmt['name'], $stmt['serial'], $stmt['nsl_cslc_idx']);

        }

        return $location_object;
    }
}