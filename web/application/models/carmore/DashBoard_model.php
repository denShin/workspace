<?php
/**
 * DashBoard_model -대시보드 db 처리
 */

class DashBoard_model  extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    // 7일간 매출 총합 업데이트
    function weekavg($date){

        $startdate =date("Ymd", strtotime("$date -8 day"));
        $enddate = date("Ymd", strtotime("$date -1 day"));

        $sql="SELECT   (SUM(sellamount)/7)    AS totalprice FROM admin_daycal
              WHERE dateval BETWEEN '$startdate' AND '$enddate' and calcode='datecal'   ";
        $row =$this->db->query($sql)->row();
        $totalprice = $row->totalprice;
        return  $totalprice;
    }



    // 대시보드 페이지 데이터 리스트
    function getDashBoardData($today){

        if($today=="") $today =date("Ymd");

        $startdate=date("Ymd",   strtotime("$today -10 days"));
        $enddate=$today;

        //year, month, quarter data
        $thisyear = date("Y");
        $thismonth =date("Ym");
        $thisquarter = ceil(date('n') / 3);
        $quartercode = $thisyear."q".$thisquarter;

        //this year calculate
        $sql=" SELECT  * from admin_daycal  WHERE calcode='year' and dateval='$thisyear'";
        $todayrow = $this->db->query($sql)->row();
        $data["yeartotalprice"] =$todayrow->sellamount;
        $data["yeartotalcount"] =$todayrow->sellcount;
        $data["yearcancelprice"] =$todayrow->cancelamount;
        $data["yearcancelcount"] =$todayrow->cancelcount;

        //this month calculate
        $sql=" SELECT  * from admin_daycal  WHERE calcode='month' and dateval='$thismonth'";
        $todayrow = $this->db->query($sql)->row();
        $data["monthtotalprice"] =$todayrow->sellamount;
        $data["monthtotalcount"] =$todayrow->sellcount;
        $data["monthcancelprice"] =$todayrow->cancelamount;
        $data["monthcancelcount"] =$todayrow->cancelcount;

        //this quarter calculate
        $sql=" SELECT  * from admin_daycal  WHERE dateval='$quartercode'";
        $todayrow = $this->db->query($sql)->row();
        $data["qtotalprice"] =$todayrow->sellamount;
        $data["qtotalcount"] =$todayrow->sellcount;
        $data["qcancelprice"] =$todayrow->cancelamount;
        $data["qcancelcount"] =$todayrow->cancelcount;

        //오늘 매출액,건수, 취소 건수,액수
        $sql=" SELECT  * from admin_daycal  WHERE dateval='$today'";
        $todayrow = $this->db->query($sql)->row();
        $data["todaytotalprice"] =$todayrow->sellamount;
        $data["todaytotalcount"] =$todayrow->sellcount;
        $data["todaycancelprice"] =$todayrow->cancelamount;
        $data["todaycancelcount"] =$todayrow->cancelcount;

        //오늘의 가입 회원수 
        $sql=" SELECT COUNT(*) as todaymembercnt FROM tbl_usr_list WHERE jdate LIKE '$today%' AND usrstatus='1'";
        $data["todaymembercnt"] = $this->db->query($sql)->row()->todaymembercnt;

        //기간별 매출추이
        $sql="SELECT dateval, sellamount AS totalprice FROM admin_daycal
              WHERE dateval BETWEEN '$startdate' AND '$enddate' and calcode='datecal'    GROUP BY dateval ORDER BY dateval asc";
        $data["peroidsale_result"] = $this->db->query($sql)->result();


        //고객문의사항 top 5
        $sql="select *
            ,(SELECT admin_name FROM admin_member WHERE  admin_email=a.mem_id)AS mem_name
            ,(SELECT kakaonickname FROM tbl_usr_list WHERE  usrserial=a.usrserial limit 1)AS kakaonickname  
            ,(SELECT fileSaveName  FROM admin_uploadfile where content_code=a.content_code limit 1  ) as filename  
            from   admin_board a where board_code='complain'   order by board_idx desc limit 0,8";
        $data["complain_result"] = $this->db->query($sql)->result();



        //최근주문 top 10
        $sql="SELECT * FROM tbl_reservation_list a 
                INNER JOIN new_rentCompany b
                INNER JOIN new_rentCompany_branch c
                INNER JOIN (SELECT a.* , b.brand,  b.carType,   b.carType_flag, b.model, 
                 b.min_fuel_efficiency, b.max_fuel_efficiency FROM new_rentCar a INNER JOIN new_car b ON a.car_serial=b.SERIAL) d 
                 INNER JOIN  (SELECT a.* , b.title AS coupon_title ,  b.VALUE AS  coupon_price
                  FROM new_coupon_list  a INNER JOIN new_coupon_info b ON a.info_serial =b.SERIAL)   e
                INNER JOIN new_rentSchedule f
                ON a.company_serial=b.SERIAL
                AND a.f_companycode =c.SERIAL
                AND a.f_carserial =d.SERIAL  
                AND a.f_couponserial =e.SERIAL
                
                AND a.f_bookingid =f.SERIAL   order by f_reservationidx desc limit 0,5";
        $data["order_result"] = $this->db->query($sql)->result();

        return $data;
    }


}