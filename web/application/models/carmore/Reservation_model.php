<?php
/**
 * Reservation_model - 예약정보 로그 관련 db
 */


class Reservation_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    
    // 예약 갯수
    function getAppReservationCount($sp="",$sv="",$f_paystatus="" ){
        $sql="select count(*) as cnt FROM tbl_reservation_list a 
                INNER JOIN new_rentCompany b
                INNER JOIN new_rentCompany_branch c
                INNER JOIN (SELECT a.* , b.brand,  b.carType,   b.carType_flag, b.model, 
                 b.min_fuel_efficiency, b.max_fuel_efficiency FROM new_rentCar a INNER JOIN new_car b ON a.car_serial=b.SERIAL) d
                inner join new_rentSchedule f            
                ON a.company_serial=b.SERIAL
                AND a.f_companycode =c.SERIAL
                AND a.f_carserial =d.SERIAL  
                and a.f_bookingid =f.SERIAL
                
                where 1=1 ";

        if($f_paystatus !=""){
            $sql.=" and a.f_paystatus= '$f_paystatus'";
        }
        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    // 예약시 사용 쿠폰정보
    function getAppReservationCouponinfo($f_couponserial){
        $sql="SELECT a.* , b.title as coupon_title ,  b.VALUE as  coupon_price
                  FROM new_coupon_list  a INNER JOIN new_coupon_info b ON a.info_serial =b.SERIAL
                where a.SERIAL='$f_couponserial' limit 1 ";

        $query = $this->db->query($sql);

        return $query->result();
    }

    //예약 정보리스트
    function getAppReservationPageList($page=1,$pageline=20,$sp="",$sv="",$f_paystatus){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="SELECT *,g.kakaoid
                ,(select admin_name from admin_member where kakaoid=g.kakaoid limit 1) as admin_name
                FROM tbl_reservation_list a 
                INNER JOIN new_rentCompany b
                INNER JOIN new_rentCompany_branch c
                INNER JOIN (SELECT a.* , b.brand,  b.carType,   b.carType_flag, b.model, 
                 b.min_fuel_efficiency, b.max_fuel_efficiency FROM new_rentCar a INNER JOIN new_car b ON a.car_serial=b.SERIAL) d 
              
                inner join new_rentSchedule f
                inner join tbl_usr_list g
                ON a.company_serial=b.SERIAL
                AND a.f_companycode =c.SERIAL
                AND a.f_carserial =d.SERIAL  
                and a.f_bookingid =f.SERIAL
                and a.f_usrserial =g.usrserial
                where 1=1 ";


        if($f_paystatus !=""){
            $sql.=" and f_paystatus= '$f_paystatus'";
        }
        if($sp!="" && $sv!=""){
            if($sp=="a.f_usrserial"){
                $sql.=" and a.f_usrserial = '".$this->db->escape_str($sv)."'";
            }else{
                $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
            }

        }
        $sql.=" order by f_reservationidx desc limit $start,20";

        $query = $this->db->query($sql);
 
        return $query->result();
    }


    //조기반납 숫자 
    function getEarlycareturnCount($sp="",$sv="",$f_paystatus="" ){
        $sql="select count(*) as cnt FROM tbl_reservation_list a 
                INNER JOIN new_rentCompany b
                INNER JOIN new_rentCompany_branch c
                INNER JOIN (SELECT a.* , b.brand,  b.carType,   b.carType_flag, b.model, 
                 b.min_fuel_efficiency, b.max_fuel_efficiency FROM new_rentCar a INNER JOIN new_car b ON a.car_serial=b.SERIAL) d 
                 inner join  (SELECT a.* , b.title as coupon_title ,  b.VALUE as  coupon_price
                  FROM new_coupon_list  a INNER JOIN new_coupon_info b ON a.info_serial =b.SERIAL)   e
                inner join new_rentSchedule f
            
                ON a.company_serial=b.SERIAL
                AND a.f_companycode =c.SERIAL
                AND a.f_carserial =d.SERIAL  
                and a.f_couponserial =e.SERIAL
                and a.f_bookingid =f.SERIAL
                
                where 1=1 
                 and a.f_reservationidx  in( select  crti_reservation_idx from carmore_reservation_term_information
                  where crti_early_return_apply in('1','2'))";

        if($f_paystatus !=""){
            $sql.=" and a.f_paystatus= '$f_paystatus'";
        }
        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    //조기반납 리스트
    function getEarlycareturnList($page=1,$pageline=20,$sp="",$sv="",$f_paystatus){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="SELECT * FROM tbl_reservation_list a 
                INNER JOIN new_rentCompany b
                INNER JOIN new_rentCompany_branch c
                INNER JOIN (SELECT a.* , b.brand,  b.carType,   b.carType_flag, b.model, 
                 b.min_fuel_efficiency, b.max_fuel_efficiency FROM new_rentCar a INNER JOIN new_car b ON a.car_serial=b.SERIAL) d 
                  INNER JOIN (SELECT * FROM carmore_reservation_term_information  
                      where crti_early_return_apply in('1','2')) e
                  INNER JOIN   partners_month_cdw_item AS f   
                ON a.company_serial=b.SERIAL
                AND a.f_companycode =c.SERIAL
                AND a.f_carserial =d.SERIAL   
                AND a.f_reservationidx =e.crti_reservation_idx   
   and e.crti_cdw_idx=f.serial
                WHERE 1=1
                 and a.f_reservationidx  in( select  crti_reservation_idx from carmore_reservation_term_information
                  where crti_early_return_apply in('1','2'))";


        if($f_paystatus !=""){
            $sql.=" and f_paystatus= '$f_paystatus'";
        }
        if($sp!="" && $sv!=""){
            if($sp=="a.f_usrserial"){
                $sql.=" and a.f_usrserial = '".$this->db->escape_str($sv)."'";
            }else{
                $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
            }

        }
        $sql.=" order by f_reservationidx desc limit $start,20";

        $query = $this->db->query($sql);

        return $query->result();
    }

    //조기반납 상세정보 
    function getEarlycareturndetail($crti_idx){



        $sql="SELECT *,f.name as db_cdw_name ,b.name as company_name 
                   , c.branchName as branch_name FROM tbl_reservation_list a 
                INNER JOIN new_rentCompany b
                INNER JOIN new_rentCompany_branch c
                INNER JOIN (SELECT a.* , b.brand,  b.carType,   b.carType_flag, b.model, 
                 b.min_fuel_efficiency, b.max_fuel_efficiency FROM new_rentCar a INNER JOIN new_car b ON a.car_serial=b.SERIAL) d 
                  INNER JOIN (SELECT * FROM carmore_reservation_term_information  
                      where crti_early_return_apply in('1','2')) e
                 INNER JOIN   partners_month_cdw_item AS f
                ON a.company_serial=b.SERIAL
                AND a.f_companycode =c.SERIAL
                AND a.f_carserial =d.SERIAL   
                AND a.f_reservationidx =e.crti_reservation_idx   
                and e.crti_cdw_idx=f.serial
                WHERE  a.f_reservationidx  in( select  crti_reservation_idx from carmore_reservation_term_information
                  where crti_early_return_apply in('1','2'))
                and e.crti_idx='$crti_idx' ";

        $resultarr = $this->db->query($sql)->result_array();

        return $resultarr[0];


    }

        
    // 반납 예약정보 가져오기
    function getCareturndetail($crti_idx){



        $sql="SELECT *  ,b.name as company_name , c.branchName as branch_name
            FROM tbl_reservation_list a INNER JOIN new_rentCompany b
                                        INNER JOIN new_rentCompany_branch c
                                        INNER JOIN (SELECT a.* , b.brand, b.carType, b.carType_flag
                                                        , b.model, b.min_fuel_efficiency, b.max_fuel_efficiency
                                                    FROM new_rentCar a INNER JOIN new_car b ON a.car_serial=b.SERIAL) d
                                        INNER JOIN carmore_reservation_term_information e
                     ON a.company_serial=b.SERIAL AND a.f_companycode =c.SERIAL AND a.f_carserial =d.SERIAL
                          AND a.f_reservationidx =e.crti_reservation_idx
                            where e.crti_idx='$crti_idx' ";

        $resultarr = $this->db->query($sql)->result_array();

        $dtlresult = $resultarr[0];


        $reserv_rent_type= $dtlresult["reserv_rent_type"];
        $crti_cdw_idx = $dtlresult["crti_cdw_idx"];

        // 단기 , 장기 렌트
        if($reserv_rent_type=="1"){
            $sql="select  name, insurance_compensation  From new_cdw_information where serial='$crti_cdw_idx'";
            $faketmparray = $this->db->query($sql)->result_array();

            if( sizeof($faketmparray) >0 ){
                $tmparray =$faketmparray[0];
                $cdw_name=$tmparray["name"];
                $cdw_compensation =$tmparray["insurance_compensation"];
            }


        }else if($reserv_rent_type=="2"){
            $sql="select  name, compensation  From partners_month_cdw_item where serial='$crti_cdw_idx'";
            $faketmparray = $this->db->query($sql)->result_array();

            if( sizeof($faketmparray) >0 ){
                $tmparray =$faketmparray[0];
                $cdw_name=$tmparray["name"];
                $cdw_compensation =$tmparray["compensation"];
            }

        }

        $dtlresult["cdw_name"]=$cdw_name;
        $dtlresult["cdw_compensation"]=$cdw_compensation;


        return $dtlresult;


    }

    // 결제 취소 코드 가져오기
    function get_cancelinfo($crti_idx){

        $sql="select * from carmore_reservation_term_information where crti_idx='$crti_idx'";
        $faketmparray = $this->db->query($sql)->result_array();

        $tmparray =$faketmparray[0];

        $crti_extend_check =$tmparray["crti_extend_check"];
        if($crti_extend_check=="0"){
            $moid =$tmparray["crti_reservation_idx"];
        }else if($crti_extend_check=="1"){
            $moid =$tmparray["crti_reservation_idx"]."E".$crti_idx ;
        }

        // get tid   from tbl_pay_log
        $sql="select f_TID as tid from tbl_pay_log where f_Moid='$moid'";
        $tid = $this->db->query($sql)->row()->tid;

        $tmparray["moid"]=$moid;
        $tmparray["tid"]=$tid;

        return $tmparray;
    }


    // 조기 반납 처리 
    function procCareturn($data){

        # db 플래그 업데이트
        $crti_early_return_confirm_date=$data["canceldate"];
        $cancel_per=$data["realcancelper"];
        $crti_reservation_idx=$data["crti_reservation_idx"];
        $crti_payment=$data["crti_payment"];
        $crti_idx=$data["crti_idx"];

        // 로그업데이트
        $cancel_per =$cancel_per/100;
        $sql="update carmore_reservation_term_information set 
                    cancel_per='$cancel_per',
                    crti_early_return_apply='2',
                    crti_state='2',
                    crti_early_return_confirm_date='$crti_early_return_confirm_date'
                    where crti_idx='$crti_idx'";
        $query = $this->db->query($sql);


        // 예약 정보 업데이트
        $sql ="update tbl_reservation_list set 
                    f_rentstatus='2' 
                    where f_reservationidx='$crti_reservation_idx'";
        $query = $this->db->query($sql);

        // 취소 로그 등록

        $sql="select * from tbl_reservation_list where  f_reservationidx='$crti_reservation_idx'";
        $faketmparray = $this->db->query($sql)->result_array();
        $tmparray =$faketmparray[0];

        $f_bookingid =$tmparray["f_bookingid"];
        $f_usrserial =$tmparray["f_usrserial"];
        $f_resultcode =$data["nice_result_code"];
        $f_resultmsg =$data["nice_result_msg"];
        $f_cancelamt =$data["nice_cancel_amt"];
        $f_MID =$data["nice_mid"];
        $f_TID =$data["nice_tid"];

        if($crti_payment==$f_cancelamt){
            //전체취소
            $f_PartialCancelCode="0";
        }else{
            //부분취소
            $f_PartialCancelCode="1";
        }


        $moid =$crti_reservation_idx;

        $sql =  "INSERT INTO tbl_cancel_log( f_reservationidx, f_bookingid, f_usrserial, f_regdate
              , f_resultcode, f_resultmsg, f_cancelamt, f_MID, f_TID, f_PartialCancelCode, moid )
               VALUES ('$crti_reservation_idx', '$f_bookingid', '$f_usrserial', now(), '$f_resultcode'
               , '$f_resultmsg', '$f_cancelamt', '$f_MID', '$f_TID','$f_PartialCancelCode', '$moid')";
        $query = $this->db->query($sql);

        return ;
    }

    //예약 취소 - 단기렌트, 월렌트
    function proReservecancel($data){
        /*
         *  * tbl_reservation_list 의 f_paystatus = 2
         * carmore_reservation_ter_information의 crti_state = 2
         * tbl_cancel_log에 정보입력
         * tbl_account_info에 f_reservationidx를 기준으로 (해당column은 moid이다) status 변경
           status : 2(취소수수료가발생한경우)
                    3(취소수수료가 없는 경우)

         * tbl_usr_list에 usrpoint를 사용한 포인트만큼 복구시키기
         * tbl_point_log에 인서트 (pointmsg/logmsg는 "취소로 인한 포인트 복구")
         * new_rentSchedule의 state를 11로 (tbl_reservation_list의 f_bookingid가 해당 테이블의 serial)
         * new_coupon_list의 사용한쿠폰을 status=0 으로
         * ata_mmt_tran의 msg_status가 1이고 etc_text_1 = moid 인거를 msg_status=0 으로


         */

        $crti_idx=$data["crti_idx"];
        $crti_reservation_idx=$data["crti_reservation_idx"];
        $crti_reserv_rent_type = $data["crti_reserv_rent_type"];
        $moid = $data["moid"];
        $returnprice = $data["returnprice"];
        $crti_payment=$data["crti_payment"];
        $crti_principal = $data["crti_principal"];
        $cardcancelprice  = $data["cardcancelprice"];
        $returnpoint  = $data["returnpoint"];
        $f_usrserial  = $data["f_usrserial"];
        $f_bookingid  = $data["f_bookingid"];
        $crti_use_coupon_idx = $data["crti_use_coupon_idx"];

        // 단기렌트일 경우 moid =$crti_reservation_idx
        // 월렌트일 경우 crti 테이블의 moid
        if($crti_reserv_rent_type=="1"){
            $moid = $crti_reservation_idx;
        }


        //   * tbl_reservation_list 의 f_paystatus = 2
        $sql="update tbl_reservation_list set f_paystatus='2' where f_reservationidx='$crti_reservation_idx'";
        $query = $this->db->query($sql);


        // * carmore_reservation_ter_information의 crti_state = 2
        $sql="update carmore_reservation_term_information set  crti_state='2'    where crti_idx='$crti_idx'";
        $query = $this->db->query($sql);

        //  tbl_cancel_log에 정보입력
        $sql="select * from tbl_reservation_list where  f_reservationidx='$crti_reservation_idx'";
        $faketmparray = $this->db->query($sql)->result_array();
        $tmparray =$faketmparray[0];

        $f_bookingid =$tmparray["f_bookingid"];
        $f_usrserial =$tmparray["f_usrserial"];
        $f_resultcode =$data["nice_result_code"];
        $f_resultmsg =$data["nice_result_msg"];
        $f_cancelamt =$data["nice_cancel_amt"];
        $f_MID =$data["nice_mid"];
        $f_TID =$data["nice_tid"];
        $moid =$crti_reservation_idx;

        if($crti_payment==$f_cancelamt){
            //전체취소
            $f_PartialCancelCode="0";
        }else{
            //부분취소
            $f_PartialCancelCode="1";
        }

        $sql =  "INSERT INTO tbl_cancel_log( f_reservationidx, f_bookingid, f_usrserial, f_regdate
              , f_resultcode, f_resultmsg, f_cancelamt, f_MID, f_TID, f_PartialCancelCode, moid )
               VALUES ('$crti_reservation_idx', '$f_bookingid', '$f_usrserial', now(), '$f_resultcode'
               , '$f_resultmsg', '$f_cancelamt', '$f_MID', '$f_TID','$f_PartialCancelCode', '$moid')";
        $query = $this->db->query($sql);


        // tbl_account_info  status : 2(취소수수료가발생한경우) ,  3(취소수수료가 없는 경우)
        $status ="2";
        if($returnprice==$crti_principal){
            $status ="3";
        }
        $sql="update tbl_account_info set      status='$status'  where f_reservationidx='$crti_reservation_idx'";
        $query = $this->db->query($sql);


        // usrpoint  복구 (사용포인트)
        if($returnpoint >0){
            $usrpoint=$returnpoint;
            $usrserial=$f_usrserial;
            $logmsg ="취소로 인한 포인트 복구";


            $sql="INSERT INTO tbl_point_log (    usrserial,  valpoint,  regdate,  pointtype,  destroydate,
                pointmsg, logmsg,  f_reservationidx)
                VALUES  (  '$usrserial',    '$usrpoint',   now(),    '4',
                    DATE_add(NOW(), INTERVAL 1 YEAR),    '$logmsg',    '$logmsg',    '-1'  );";
            $this->db->query($sql);

            $sql="update tbl_usr_list set usrpoint=usrpoint+$usrpoint WHERE  usrserial='$usrserial'" ;
            $this->db->query($sql);
        }



        //  * new_rentSchedule의 state를 11로 (tbl_reservation_list의 f_bookingid가 해당 테이블의 serial)
        $sql="update new_rentSchedule set state='11' WHERE  serial='$f_bookingid'" ;
        $this->db->query($sql);

        // coupon serial
        $sql = "UPDATE new_coupon_list SET status='0' WHERE serial='$crti_use_coupon_idx'";
        $this->db->query($sql);

        // ata_mmt_tran의 msg_status가 1이고 etc_text_1 = moid 인거를 msg_status=0 으로
        $sql = "UPDATE ata_mmt_tran  SET msg_status='0' WHERE  msg_status='1' and etc_text_1='$moid'";
        $this->db->query($sql);
    }


    // 연장취소 - 월렌트에서 연장취소했을떄 ( 시작된후 취소는 조기반납 프로세스)
    function proReservenolonger($data){
        /*
        *      * carmore_reservation_ter_information의 crti_state = 2
        * tbl_cancel_log에 정보입력
        * tbl_account_info에 f_reservationidx를 기준으로 (해당column은 moid이다) status 변경
        status : 2(취소수수료가발생한경우)
                3(취소수수료가 없는 경우)
        * new_contract_rental_period의 ncrp_crti_idx를 기준으로 state를 0으로   (그 기준이 되는게 crti_idx, 주의)해당 값이 0이면 해당쿼리 실행하면안됨)
        * new_coupon_list의 사용한쿠폰을 status=0 으로
        * tbl_point_log에 인서트 (pointmsg/logmsg는 "취소로 인한 포인트 복구")
        * ata_mmt_tran의 msg_status가 4이고 etc_text_1 = 직전moid 인거를 msg_status=1 으로


        */


        $crti_idx=$data["crti_idx"];
        $crti_reservation_idx=$data["crti_reservation_idx"];
        $crti_reserv_rent_type = $data["crti_reserv_rent_type"];
        $moid = $data["moid"];
        $returnprice = $data["returnprice"];
        $crti_principal = $data["crti_principal"];
        $crti_payment=$data["crti_payment"];
        $cardcancelprice  = $data["cardcancelprice"];
        $returnpoint  = $data["returnpoint"];
        $f_usrserial  = $data["f_usrserial"];
        $f_bookingid  = $data["f_bookingid"];
        $crti_use_coupon_idx = $data["crti_use_coupon_idx"];

        // * carmore_reservation_ter_information의 crti_state = 2
        $sql="update carmore_reservation_term_information set  crti_state='2'    where crti_idx='$crti_idx'";
        $query = $this->db->query($sql);

        //  tbl_cancel_log에 정보입력
        $sql="select * from tbl_reservation_list where  f_reservationidx='$crti_reservation_idx'";
        $faketmparray = $this->db->query($sql)->result_array();
        $tmparray =$faketmparray[0];

        $f_bookingid =$tmparray["f_bookingid"];
        $f_usrserial =$tmparray["f_usrserial"];
        $f_resultcode =$data["nice_result_code"];
        $f_resultmsg =$data["nice_result_msg"];
        $f_cancelamt =$data["nice_cancel_amt"];
        $f_MID =$data["nice_mid"];
        $f_TID =$data["nice_tid"];
        if($crti_payment==$f_cancelamt){
            //전체취소
            $f_PartialCancelCode="0";
        }else{
            //부분취소
            $f_PartialCancelCode="1";
        }
        $moid =$crti_reservation_idx;

        $sql =  "INSERT INTO tbl_cancel_log( f_reservationidx, f_bookingid, f_usrserial, f_regdate
              , f_resultcode, f_resultmsg, f_cancelamt, f_MID, f_TID, f_PartialCancelCode, moid )
               VALUES ('$crti_reservation_idx', '$f_bookingid', '$f_usrserial', now(), '$f_resultcode'
               , '$f_resultmsg', '$f_cancelamt', '$f_MID', '$f_TID','$f_PartialCancelCode', '$moid')";
        $query = $this->db->query($sql);

        // tbl_account_info  status : 2(취소수수료가발생한경우) ,  3(취소수수료가 없는 경우)
        $status ="2";
        if($returnprice==$crti_principal){
            $status ="3";
        }
        $sql="update tbl_account_info set      status='$status'  where f_reservationidx='$crti_reservation_idx'";
        $query = $this->db->query($sql);

        //       * new_contract_rental_period의 ncrp_crti_idx를 기준으로 state를 0으로
        ////    (그 기준이 되는게 crti_idx, 주의)해당 값이 0이면 해당쿼리 실행하면안됨)
        $sql= "update new_contract_rental_period set state='0' where crti_idx='$crti_idx' and crti_idx <>'0'  ";
        $query = $this->db->query($sql);

        // usrpoint  복구 (사용포인트)
        if($returnpoint >0){
            $usrpoint=$returnpoint;
            $usrserial=$f_usrserial;
            $logmsg ="취소로 인한 포인트 복구";

            $sql="INSERT INTO tbl_point_log (    usrserial,  valpoint,  regdate,  pointtype,  destroydate,
                pointmsg, logmsg,  f_reservationidx)
                VALUES  (  '$usrserial',    '$usrpoint',   now(),    '4',
                    DATE_add(NOW(), INTERVAL 1 YEAR),    '$logmsg',    '$logmsg',    '-1'  );";
            $this->db->query($sql);

            $sql="update tbl_usr_list set usrpoint=usrpoint+$usrpoint WHERE  usrserial=".$usrserial ;
            $this->db->query($sql);
        }


        // coupon serial
        $sql = "UPDATE new_coupon_list SET status='0' WHERE serial='$crti_use_coupon_idx'";
        $this->db->query($sql);

        //        * ata_mmt_tran의 msg_status가 4이고 etc_text_1 = 직전moid 인거를 msg_status=1 으로
        $sql="SELECT crti_idx, crti_extend_check FROM carmore_reservation_term_information
               WHERE crti_reservation_idx='$crti_reservation_idx' AND crti_idx < '$crti_idx'
              AND crti_state=1 AND crti_reserv_rent_type=2 LIMIT 0, 1";
        $faketmparray = $this->db->query($sql)->result_array();
        $tmparray =$faketmparray[0];

        $crti_extend_check =$tmparray["crti_extend_check"];
        if($crti_extend_check=="1"){
            $moid =$crti_reservation_idx."E".$crti_idx;
        }else{
            $moid =$crti_reservation_idx ;
        }

        $sql = "UPDATE ata_mmt_tran  SET msg_status='1' WHERE  msg_status='4' and etc_text_1='$moid'";
        $this->db->query($sql);

    }

    // 자동차 반납주소 변경
    function changereturnaddr($data){

        $return_addr=$data["return_addr"];
        $f_reservationidx=$data["f_reservationidx"];

        // 로그업데이트
        $sql="update tbl_reservation_list set 
                    f_delivreturnaddr='$return_addr'
                    where f_reservationidx='$f_reservationidx'";

        $query = $this->db->query($sql);
        return ;

    }

    // 지점의 전화번호 가져오기
    function get_branchtel( $company_serial,$branch_serial ){

        $sql="select phone from new_branch_contact_member
                  where company_serial='$company_serial' and branch_serial='$branch_serial'";

        $resultarr = $this->db->query($sql)->result_array();
        return $resultarr;
    }

    // sms 보내기 등록
    function procSmsinfo($data){


        $content=$data["content"];
        $biztalk_now=$data["biztalk_now"];
        $decrypt_driver_phone=$data["decrypt_driver_phone"];
        $biztalk_sender_key='7e7023c4890abb6ee693d9304283edb3bf6ad7d8';
        $get_reservation_crti_idx=$data["get_reservation_crti_idx"];
        $template_code=$data["template_code"];


        $sql = "INSERT INTO ata_mmt_tran (date_client_req, subject, content, callback, msg_status, recipient_num
              ,msg_type, sender_key, template_code, etc_text_1)
              VALUES ('$biztalk_now', '', '$content', '', '1', '$decrypt_driver_phone'
              , '1008', '$biztalk_sender_key', '$template_code', '$get_reservation_crti_idx')";

        $query = $this->db->query($sql);
        return ;

    }

    //관리자 코멘트 리스트 
    function getAdmincommentlist($crti_idx){

        $sql="SELECT *  ,(SELECT admin_name FROM admin_member WHERE  admin_email=a.mem_id)AS writer_name 
      FROM admin_comment a where board_idx='$crti_idx' order by regdate desc";

        $resultarr = $this->db->query($sql)->result_array();
        return $resultarr;
    }

    // 관리자 코멘트 등록처리
    function procAdmincomment($data){
        $datamode = $data["datamode"];
        $regdate  =date("YmdHis");

        if($datamode=="new"){
            $mem_id =$data["mem_id"];
            $board_idx =$data["crti_idx"];
            $commenttext =$data["commenttext"];

            $sql = "INSERT INTO admin_comment (commenttext,  mem_id,  board_idx,  regdate,  board_code)
              VALUES ('$commenttext', '$mem_id', '$board_idx', '$regdate', 'careturn')";

        }else if($datamode=="del"){
            $comment_idx=$data["comment_idx"];
            $sql = "delete from admin_comment where comment_idx='$comment_idx'";
        }

        $query = $this->db->query($sql);
        return ;
    }


    // 성수기 게시물 숫자가져오기
    function getPeakseasonCount($sp="",$sv=""){
        $sql="select count(*) as cnt FROM carmore_peakseason   
                where 1=1  ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    // 성수기 정보 리스트
    function getPeakseasonList($page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="SELECT *
              ,(SELECT admin_name FROM admin_member WHERE  admin_email=a.mem_id)AS mem_name 
              FROM carmore_peakseason  a  WHERE 1=1";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by peakseason_startdate desc limit $start,20";

        $query = $this->db->query($sql);

        return $query->result();
    }

    // 성수기 등록처리 
    function procPeakseason($data){
        $datamode = $data["datamode"];

        if($datamode=="new"){
            $mem_id =$data["mem_id"];
            $peakseason_year =$data["peakseason_year"];
            $peakseason_startdate =$data["peakseason_startdate"];
            $peakseason_enddate =$data["peakseason_enddate"];

            $sql = "INSERT INTO carmore_peakseason (mem_id,peakseason_year, peakseason_startdate,peakseason_enddate,show_yn)
              VALUES ('$mem_id', '$peakseason_year', '$peakseason_startdate', '$peakseason_enddate','n')";
            $query = $this->db->query($sql);

        }else if($datamode=="changeshowyn"){

            $saveparam =$data["saveparam"];
            $stats =$data["stats"];
            $arr_idx = explode(",",$saveparam);

            for($i=0;$i < sizeof($arr_idx);$i++){
                $peakseason_idx = $arr_idx[$i];

                $sql="update carmore_peakseason set show_yn ='$stats' WHERE  peakseason_idx=".$peakseason_idx ;


                $this->db->query($sql);
            }


        }else if($datamode=="del"){
            $peakseason_idx=$data["peakseason_idx"];
            $sql = "delete from carmore_peakseason where peakseason_idx='$peakseason_idx'";
            $query = $this->db->query($sql);
        }


        return ;
    }

    //가장 최근 등록한 예약 정보 코드가져오기
    function getLastcrtidx($f_reservationidx){

        $sql="select crti_idx as  crti_idx FROM carmore_reservation_term_information   
                where crti_reservation_idx='$f_reservationidx'  order by crti_idx desc limit 1 ";

        $crti_idx = $this->db->query($sql)->row()->crti_idx;
        return $crti_idx;
    }

 

}