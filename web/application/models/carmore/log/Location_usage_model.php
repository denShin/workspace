<?php
/**
 * Created by PhpStorm.
 * User: teamo2_server
 * Date: 2019-07-03
 * Time: 15:22
 */

class Location_usage_model extends WS_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function read_usage_inventory($date)
    {
        $usage_ret       = new stdClass();
        $usage_inventory = [];

        $usage_ret->result = true;

        $start = $date.' 00:00:00';
        $end   = $date.' 23:59:59';

        $query = "
        SELECT
            cliul_social_id, cliul_acquisition_path, cliul_offer_service, cliul_offer_object, cliul_register_date
        FROM carmore_location_information_usage_log WHERE cliul_register_date >= :start AND cliul_register_date <= :end";

        $sql = $this->slave->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->bindParam(':start', $start);
        $sql->bindParam(':end', $end);
        $sql->execute();
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            $log_object = new stdClass();

            $log_object->socialId     = $stmt['cliul_social_id'];
            $log_object->path         = $stmt['cliul_acquisition_path'];
            $log_object->offerService = $stmt['cliul_offer_service'];
            $log_object->offerObject  = $stmt['cliul_offer_object'];
            $log_object->registerDate = $stmt['cliul_register_date'];

            array_push($usage_inventory, $log_object);

        }

        $usage_ret->inventory = $usage_inventory;

        return $usage_ret;
    }

    public function log_usage_access($id)
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $date = date('Y-m-d H:i:s');

        $query = "
        INSERT INTO carmore_location_information_usage_access_log
            (cliual_id, cliual_ip, cliual_access_date)
        VALUE
            (?, ?, ?)";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$id, $ip, $date]);
    }

}

