<?php
/**
 * CarmoreBoard_model - 고객 문의 상담 로그 관련 db
 */

class CarmoreBoard_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    // 고객 문의 카운트
    function getAdminBoardCount($board_code="",$sp="",$sv=""){
        $sql="select count(*) as cnt  from   admin_board a inner join tbl_usr_list b  on a.usrserial=b.usrserial  where board_code='$board_code'  ";

        if($sp!="" && $sv!=""){
            if($sp=="usrserial"){

                $sql.=" and a.usrserial='".$sv."'";
            }else{

                $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
            }
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    // 고객 문의 게시판 리스트
    function getAdminBoardPageList($board_code,$page=1,$pageline=20,$sp="",$sv="",$order=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select *
            ,(SELECT admin_name FROM admin_member WHERE  admin_email=a.mem_id)AS mem_name 
            ,(SELECT fileSaveName  FROM admin_uploadfile where content_code=a.content_code limit 1  ) as filename
  
            from   admin_board a inner join tbl_usr_list b  on a.usrserial=b.usrserial where board_code='$board_code'  ";

        if($sp!="" && $sv!=""){

            if($sp=="usrserial"){

                $sql.=" and a.usrserial='".$sv."'";
            }else{

                $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
            }

        }

        if($order !=""){
            $sql.=" order by $order asc limit $start,100";
        }else{
            $sql.=" order by board_idx desc limit $start,20";
        }

        $query = $this->db->query($sql);


        return $query->result();
    }

    // 고객 문의 게시판 상세
    function getAdminBoardDetail($board_idx)
    {
        $sql="select *
            ,(SELECT admin_name FROM admin_member WHERE  admin_email=a.mem_id)AS mem_name
            ,(SELECT kakaonickname FROM tbl_usr_list WHERE  usrserial=a.usrserial limit 1)AS kakaonickname  
        from admin_board a where board_idx='$board_idx'";

        $query = $this->db->query($sql)->result_array();
        $row = $query[0];
        return $row;
    }


    //  게시물정보 수정,등록,삭제
    public function procAdminBoard($emode,$data)
    {
        $return_v="";

        if($emode=="new"){

            $nowtime =date("YmdHis");
            $sql = "INSERT INTO admin_board 	(	content_code, 	board_title,
				board_content, mem_id, 	regdate, 	board_code,board_part,usrserial,startdate,enddate,pointsrl,complain_yn)
				VALUES('".$data["content_code"]."'
				, 	'".$this->db->escape_str($data["board_title"])."'
				,'".$this->db->escape_str($data["board_content"])."'
				,'".$data["mem_id"]."'
				,'$nowtime'
				,'".$data["board_code"]."'
				,'".$data["board_part"]."'
				,'".$data["usrserial"] ."'
				,'".$data["startdate"] ."'
				,'".$data["enddate"] ."','".$data["lastid"] ."','".$data["complain_yn"] ."');";

            $this->db->query($sql);
            $return_v="y";

        }else if($emode=="edit"){
            $sql = "update  admin_board set
                  board_title='".$this->db->escape_str($data["board_title"])."'
                  ,board_content='".$this->db->escape_str($data["board_content"])."' 
                  ,usrserial='".$this->db->escape_str($data["usrserial"])."' 
                   ,startdate='".$this->db->escape_str($data["startdate"])."' 
                    ,enddate='".$this->db->escape_str($data["enddate"])."' 
             where board_idx='".$data["board_idx"]."'";

            $this->db->query($sql);
            $return_v="y";
        }else if($emode=="del"){

            $sql = "delete from admin_board where board_idx='".$this->db->escape_str($data["board_idx"])."'";
            $this->db->query($sql);

            $sql = "delete from admin_comment where board_idx='".$this->db->escape_str($data["board_idx"])."'";
            $this->db->query($sql);

            $sql = "delete from admin_uploadfile where content_code='".$this->db->escape_str($data["content_code"])."'";
            $this->db->query($sql);
            $return_v="y";
        }
        return "y";
    }

    //게시물 리플 리스트
    public function getBoardReplyList($board_code,$board_idx)
    {
        $sql="SELECT *
      ,(SELECT admin_name FROM admin_member WHERE  admin_email=a.mem_id)AS mem_name
        FROM admin_comment a where board_code='$board_code' and board_idx='$board_idx' order by board_idx asc ";
        $query = $this->db->query($sql);

        return $query->result();
    }

    // 게시물 리플 등록.수정,삭제
    public function procBoardReply($emode,$data)
    {
        if($emode=="new"){

            $nowtime =date("YmdHis");
            $replyText=$this->db->escape_str($data["replyText"]);
            $board_idx=$data["board_idx"];
            $board_code=$data["board_code"];
            $login_id=$data["mem_id"];

            $sql=" INSERT INTO admin_comment 	(	`commenttext`, 	`member_idx`, `idx`, `regdate`,board_code)
					VALUES	(	'$replyText', 	'$login_id', '$board_idx', '$nowtime','$board_code')";
            $this->db->query($sql);
            $return_v="y";

        }else if($emode=="del"){

            $login_id=$data["mem_id"];
            $comment_idx=$data["comment_idx"];

            $sql="delete from admin_comment   where mem_id='$login_id' and comment_idx='$comment_idx' ";

            $this->db->query($sql);
            $return_v="y";
        }
    }


    // 게시물 코멘트 등록,수정처리
    function procAdmincomment($data){
        $datamode = $data["datamode"];
        $regdate  =date("YmdHis");

        if($datamode=="new"){
            $mem_id =$data["mem_id"];
            $board_idx =$data["board_idx"];
            $commenttext =$data["commenttext"];
            $board_code =$data["board_code"];

            $sql = "INSERT INTO admin_comment (commenttext,  mem_id,  board_idx,  regdate,  board_code)
              VALUES ('$commenttext', '$mem_id', '$board_idx', '$regdate', '$board_code')";

        }else if($datamode=="del"){
            $comment_idx=$data["comment_idx"];
            $sql = "delete from admin_comment where comment_idx='$comment_idx'";
        }

        $query = $this->db->query($sql);
        return ;
    }


    // 게시물 파일리스트가져오기
    public function get_BoardFileList($content_code)
    {
        $sql="SELECT content_code,  fileOrgName,  fileSaveName,  fileType
                              FROM admin_uploadfile where content_code='$content_code' order by uploadfile_infoidx asc";

        return $this->db->query($sql)->result_array();
    }

    // 게시물 상태변경
    public function procChangestats($data)
    {
        $board_idx =$data["board_idx"];
        $board_stats =$data["board_stats"];

        $sql="update admin_board set board_stats='".$board_stats."'  where board_idx='".$board_idx."'";

        $this->db->query($sql);
    }

    // 선택된 게시물 상태변경
    public function procChangeallstats($data){
        $saveparam =$data["saveparam"];
        $arr_board_Idx = explode(",",$saveparam);

        for($i=0;$i < sizeof($arr_board_Idx);$i++){
            $board_idx = $arr_board_Idx[$i];
            $sql="update admin_board set board_stats ='y' WHERE  board_idx=".$board_idx ;
            $this->db->query($sql);
        }
    }

    // 게시물 순서 변경 
    public function procChangeordernum($data){
        $order =$data["order"];
        $arr_ordernum = explode(",",$order);

        for($i=0;$i < sizeof($arr_ordernum);$i++){
            $ordernum = $i+1;
            $board_idx = $arr_ordernum[$i];
            $sql="update admin_board set ordernum ='$ordernum' WHERE  board_idx=".$board_idx ;
            $this->db->query($sql);
        }
    }

    // 고객문의 상담 등록처리
    public function procComplainWork($data){
        $f_paylogidx =$data["f_paylogidx"] ;
        $usrserial =$data["usrserial"] ;
        $workmode =$data["workmode"] ;
        $board_part =$data["board_part"]  ;
        $pointsrl =$data["pointsrl"]  ;
        $pointtype =$data["pointtype"]  ;
        $valpoint =$data["valpoint"]  ;

        if($board_part=="member"){
            if($workmode=="block"){
                $sql="update tbl_usr_list set usrstatus='8' WHERE  usrserial=".$usrserial ;
            }else if($workmode=="out"){
                $sql="update tbl_usr_list set usrstatus='9' WHERE  usrserial=".$usrserial ;
            }else if($workmode=="normal"){
                $sql="update tbl_usr_list set usrstatus='1' WHERE  usrserial=".$usrserial ;
            }
            $this->db->query($sql);
        }
        if($board_part=="point"){
            if($workmode=="back"){

                $sql="update tbl_point_log set pointtype='$pointtype' WHERE  pointsrl=".$pointsrl ;
                $this->db->query($sql);

                $sql="update tbl_usr_list set usrpoint=usrpoint-$valpoint WHERE  usrserial=".$usrserial ;
                $this->db->query($sql);
            }else if($workmode=="init"){

                $logmsg ="운영진 포인트 지급";

                $sql="INSERT INTO tbl_point_log (    usrserial,  valpoint,  regdate,  pointtype,  destroydate,
                pointmsg, logmsg,  f_reservationidx)
                VALUES  (  '$usrserial',    '$valpoint',   now(),    '4',
                    DATE_add(NOW(), INTERVAL 1 YEAR),    '$logmsg',    '$logmsg',    '-1'  );";
                $this->db->query($sql);
                $lastid = $this->db->insert_id();


                $sql="update tbl_usr_list set usrpoint=usrpoint+$valpoint WHERE  usrserial=".$usrserial ;
                $this->db->query($sql);
                return $lastid;
            }

        }
        if($board_part=="pay"){

            if($workmode=="cancel"){
                $cancel_date = date('Y-m-d H:i:s');

                $sql = "SELECT f_reservationidx,f_usrserial FROM tbl_pay_log WHERE f_paylogidx='".$f_paylogidx."'";
                $query = $this->db->query($sql)->result_array();
                $frow = $query[0];
                $f_reservationidx = $frow["f_reservationidx"];
                $f_usrserial = $frow["f_usrserial"];


                // get data
                $sql = "SELECT f_TID, f_Amt, f_resultcode, f_logdate FROM tbl_pay_log WHERE f_reservationidx='$f_reservationidx'";
                $query = $this->db->query($sql)->result_array();
                $row = $query[0];
                $tid = $row['f_TID'];
                $cancel_amt = $row['f_Amt'];
                $result_code = $row['f_resultcode'];
                $paid_date = $row['f_logdate'];

                $date2 = strtotime($cancel_date);
                $date1 = strtotime($paid_date);
                $paid_diff = (int)($date2 - $date1);

                // get data
                $sql = "SELECT reserv_area_code, f_reservationidx, f_bookingid, f_usrserial, f_rentstartdate,
            f_rentenddate, f_usepoint, f_rentprice, f_insuprice, f_delivprice, f_couponserial FROM tbl_reservation_list 
            WHERE f_reservationidx='$f_reservationidx'";
                $query = $this->db->query($sql)->result_array();
                $row1 = $query[0];
                $reserv_area_code = $row1['reserv_area_code'];
                $bookId = $row1['f_bookingid'];
                $post_user_serial = $row1['f_usrserial'];
                $rentstart = $row1['f_rentstartdate'];
                $rentend = $row1['f_rentenddate'];
                $use_point = $row1['f_usepoint'];
                $rental_cost = $row1['f_rentprice'];
                $cdw_cost = $row1['f_insuprice'];
                $deliv_cost = $row1['f_delivprice'];
                $idx = $row1['f_reservationidx'];

                $coupon_serial = $row1['f_couponserial'];

                $date2 = strtotime($rentstart);
                $date1 = strtotime($cancel_date);
                $diff = (int)($date2 - $date1);

                $user_return_pointer = 0;
                $pay_all_point_flag = 0;
                $account_info_cancel_val = 0;

                $cancel_per = 1;


                if ($reserv_area_code == ULLENG_AREA_CODE) {    //울릉도
                    if ($diff >= 604800 || $paid_diff <= 3600) {  //일주일 이전
                        $cancel_able_flag = 1;
                        $partial_cancel_code = 0;

                        $cancel_amt = (int)trim($cancel_amt);
                        $user_return_pointer = $use_point;

                        if ($result_code == '3000') {    //전부 포인트로 결제한것 > 나이스페이 안감
                            $pay_all_point_flag = 1;
                            $account_info_cancel_val = 3;
                        }
                    } elseif ($diff < 604800 && $diff >= 259200) {  //일주일~3일 사이
                        $cancel_able_flag = 1;
                        $partial_cancel_code = 1;
                        $cancel_amt = (int)trim($cancel_amt);

                        $cancel_per = 0.7;

                        $tmp_total_cost = (int)$rental_cost + (int)$cdw_cost + (int)$deliv_cost;    //총 렌트비
                        $tmp_refund_pay = (int)($tmp_total_cost * 0.7);    //환불비
                        $tmp_penalty_pay = $tmp_total_cost - $tmp_refund_pay;

                        if ($use_point >= $tmp_penalty_pay) {
                            //사용한 포인트가 위약금보다 많은 경우
                            //(사용한포인트-위약금)=>a 포인트복구 // 결제한금액 모두 취소
                            $user_return_pointer = (int)$use_point-(int)$tmp_penalty_pay;
                            $partial_cancel_code = 0;
                        } else {
                            //환불금이 내가 사용한 포인트보다 많은경우
                            //포인트 0원이 되고  토탈에서 90%가 취소금액 (어떠한경우에도)
                            $user_return_pointer = 0;
                            $cancel_amt = (int)$cancel_amt-((int)$tmp_penalty_pay-(int)$use_point);

                        }

                        if ($result_code == '3000') {    //전부 포인트로 결제한것 > 나이스페이 안감
                            $pay_all_point_flag = 1;
                            $account_info_cancel_val = 2;
                        }
                    } elseif ($diff < 259200 && $diff >= 86400) {    //3일이내~ 당일사이(24시간)
                        $cancel_able_flag = 1;
                        $partial_cancel_code = 1;
                        $cancel_amt = (int)trim($cancel_amt);

                        $cancel_per = 0.5;

                        $tmp_total_cost = (int)$rental_cost + (int)$cdw_cost + (int)$deliv_cost;    //총 렌트비
                        $tmp_refund_pay = (int)($tmp_total_cost * 0.5);    //환불비
                        $tmp_penalty_pay = $tmp_total_cost - $tmp_refund_pay;

                        if ($use_point >= $tmp_penalty_pay) {
                            //사용한 포인트가 위약금보다 많은 경우
                            //(사용한포인트-위약금)=>a 포인트복구 // 결제한금액 모두 취소
                            $user_return_pointer = (int)$use_point-(int)$tmp_penalty_pay;
                            $partial_cancel_code = 0;
                        } else {
                            //환불금이 내가 사용한 포인트보다 많은경우
                            //포인트 0원이 되고  토탈에서 90%가 취소금액 (어떠한경우에도)
                            $user_return_pointer = 0;
                            $cancel_amt = (int)$cancel_amt-((int)$tmp_penalty_pay-(int)$use_point);

                        }

                        if ($result_code == '3000') {    //전부 포인트로 결제한것 > 나이스페이 안감
                            $pay_all_point_flag = 1;
                            $account_info_cancel_val = 2;
                        }
                    } elseif ($diff < 86400)    //당일취소 & 시간지나서 취소 (취소불가)
                        $cancel_able_flag = "0";
                } else {
                    $rentstart_date = explode(' ', $rentstart)[0];
                    $rentend_date = explode(' ', $rentend)[0];
                    // $is_peakseason = $peakseason_viewer->chkIsInPeakseason($rentstart_date, $rentend_date);
                    $is_peakseason=false;

                    if ($is_peakseason) {   //성수기
                        if ($diff < 345600 && $diff >= 259200 || $paid_diff <= 3600) {  //4일~3일
                            $cancel_able_flag = 1;
                            $partial_cancel_code = 0;

                            $cancel_amt = (int)trim($cancel_amt);
                            $user_return_pointer = $use_point;

                            if ($result_code == '3000') {    //전부 포인트로 결제한것 > 나이스페이 안감
                                $pay_all_point_flag = 1;
                                $account_info_cancel_val = 3;
                            }
                        } elseif ($diff < 259200 && $diff >= 172800) {  //3일~2일
                            $cancel_able_flag = 1;
                            $partial_cancel_code = 1;
                            $cancel_amt = (int)trim($cancel_amt);

                            $cancel_per = 0.9;

                            $tmp_total_cost = (int)$rental_cost + (int)$cdw_cost + (int)$deliv_cost;    //총 렌트비
                            $tmp_refund_pay = (int)($tmp_total_cost * 0.9);    //환불비
                            $tmp_penalty_pay = $tmp_total_cost - $tmp_refund_pay;

                            if ($use_point >= $tmp_penalty_pay) {
                                //사용한 포인트가 위약금보다 많은 경우
                                //(사용한포인트-위약금)=>a 포인트복구 // 결제한금액 모두 취소
                                $user_return_pointer = (int)$use_point-(int)$tmp_penalty_pay;
                                $partial_cancel_code = 0;
                            } else {
                                //환불금이 내가 사용한 포인트보다 많은경우
                                //포인트 0원이 되고  토탈에서 90%가 취소금액 (어떠한경우에도)
                                $user_return_pointer = 0;
                                $cancel_amt = (int)$cancel_amt-((int)$tmp_penalty_pay-(int)$use_point);

                            }

                            if ($result_code == '3000') {    //전부 포인트로 결제한것 > 나이스페이 안감
                                $pay_all_point_flag = 1;
                                $account_info_cancel_val = 2;
                            }
                        } elseif ($diff < 172800 && $diff >= 86400) {   //2일~1일
                            $cancel_able_flag = 1;
                            $partial_cancel_code = 1;
                            $cancel_amt = (int)trim($cancel_amt);

                            $cancel_per = 0.8;

                            $tmp_total_cost = (int)$rental_cost + (int)$cdw_cost + (int)$deliv_cost;    //총 렌트비
                            $tmp_refund_pay = (int)($tmp_total_cost * 0.8);    //환불비
                            $tmp_penalty_pay = $tmp_total_cost - $tmp_refund_pay;

                            if ($use_point >= $tmp_penalty_pay) {
                                //사용한 포인트가 위약금보다 많은 경우
                                //(사용한포인트-위약금)=>a 포인트복구 // 결제한금액 모두 취소
                                $user_return_pointer = (int)$use_point-(int)$tmp_penalty_pay;
                                $partial_cancel_code = 0;
                            } else {
                                //환불금이 내가 사용한 포인트보다 많은경우
                                //포인트 0원이 되고  토탈에서 90%가 취소금액 (어떠한경우에도)
                                $user_return_pointer = 0;
                                $cancel_amt = (int)$cancel_amt-((int)$tmp_penalty_pay-(int)$use_point);

                            }

                            if ($result_code == '3000') {    //전부 포인트로 결제한것 > 나이스페이 안감
                                $pay_all_point_flag = 1;
                                $account_info_cancel_val = 2;
                            }
                        } elseif ($diff < 86400 && $diff >= 0) {  //24시간 이내 취소(당일취소)
                            $cancel_able_flag = 1;
                            $partial_cancel_code = 1;
                            $cancel_amt = (int)trim($cancel_amt);

                            $cancel_per = 0.7;

                            $tmp_total_cost = (int)$rental_cost + (int)$cdw_cost + (int)$deliv_cost;    //총 렌트비
                            $tmp_refund_pay = (int)($tmp_total_cost * 0.7);    //환불비
                            $tmp_penalty_pay = $tmp_total_cost - $tmp_refund_pay;

                            if ($use_point >= $tmp_penalty_pay) {
                                //사용한 포인트가 위약금보다 많은 경우
                                //(사용한포인트-위약금)=>a 포인트복구 // 결제한금액 모두 취소
                                $user_return_pointer = (int)$use_point-(int)$tmp_penalty_pay;
                                $partial_cancel_code = 0;
                            } else {
                                //환불금이 내가 사용한 포인트보다 많은경우
                                //포인트 0원이 되고  토탈에서 90%가 취소금액 (어떠한경우에도)
                                $user_return_pointer = 0;
                                $cancel_amt = (int)$cancel_amt-((int)$tmp_penalty_pay-(int)$use_point);

                            }

                            if ($result_code == '3000') {    //전부 포인트로 결제한것 > 나이스페이 안감
                                $pay_all_point_flag = 1;
                                $account_info_cancel_val = 2;
                            }
                        } elseif ($diff < 0)
                            $cancel_able_flag = "0";
                    } else {    //비수기
                        if ($diff >= 86400 || $paid_diff <= 3600) {
                            $cancel_able_flag = 1;
                            $partial_cancel_code = 0;

                            $cancel_amt = (int)trim($cancel_amt);
                            $user_return_pointer = $use_point;

                            if ($result_code == '3000') {    //전부 포인트로 결제한것 > 나이스페이 안감
                                $pay_all_point_flag = 1;
                                $account_info_cancel_val = 3;
                            }
                        } elseif ($diff < 86400 && $diff >= 0) {
                            $cancel_able_flag = 1;
                            $partial_cancel_code = 1;
                            $cancel_amt = (int)trim($cancel_amt);

                            $cancel_per = 0.9;

                            $tmp_total_cost = (int)$rental_cost + (int)$cdw_cost + (int)$deliv_cost;    //총 렌트비
                            $tmp_refund_pay = (int)($tmp_total_cost * 0.9);    //환불비
                            $tmp_penalty_pay = $tmp_total_cost - $tmp_refund_pay;

                            if ($use_point >= $tmp_penalty_pay) {
                                //사용한 포인트가 위약금보다 많은 경우
                                //(사용한포인트-위약금)=>a 포인트복구 // 결제한금액 모두 취소
                                $user_return_pointer = (int)$use_point-(int)$tmp_penalty_pay;
                                $partial_cancel_code = 0;
                            } else {
                                //환불금이 내가 사용한 포인트보다 많은경우
                                //포인트 0원이 되고  토탈에서 90%가 취소금액 (어떠한경우에도)
                                $user_return_pointer = 0;
                                $cancel_amt = (int)$cancel_amt-((int)$tmp_penalty_pay-(int)$use_point);

                            }

                            if ($result_code == '3000') {    //전부 포인트로 결제한것 > 나이스페이 안감
                                $pay_all_point_flag = 1;
                                $account_info_cancel_val = 2;
                            }
                        } elseif ($diff < 0)    //취소 안됨
                            $cancel_able_flag = "0";
                    }
                }


                if ($cancel_able_flag == '1') {

                    $sql = "UPDATE tbl_reservation_list SET cancel_per='".$cancel_per."' WHERE f_reservationidx='".$f_reservationidx."'";
                    $this->db->query($sql);

                    if ($pay_all_point_flag == '1') {    //전체포인트결제의 취소처리
                        $sql = "UPDATE tbl_reservation_list SET f_paystatus = '2' WHERE f_reservationidx='".$f_reservationidx."'";
                        $this->db->query($sql);

                        $sql = "UPDATE carmore_reservation_term_information SET crti_state=2 WHERE crti_reservation_idx=?";
                        $this->db->query($sql);

                        // $cancel_log_controller->respondCancel($post_user_serial, $reservation_idx);

                        $sql = "INSERT INTO tbl_cancel_log (f_reservationidx, f_bookingid, f_usrserial, f_regdate, f_resultcode
                              , f_resultmsg, f_cancelamt, f_MID, f_TID, f_PartialCancelCode)
                               VALUES ('".$f_reservationidx."', '".$bookId."', '".$f_usrserial."', '".$cancel_date."', '2000'
                               , 'ALL POINT', '0', '', '', '1')";
                        $this->db->query($sql);

                        $sql = "UPDATE tbl_account_info SET status='".$account_info_cancel_val."' WHERE f_reservationidx='".$f_reservationidx."'";
                        $this->db->query($sql);

                        $sql = "UPDATE new_rentSchedule SET state='11' WHERE serial='".$bookId."'";
                        $this->db->query($sql);

                        $sql = "SELECT pointsrl FROM tbl_point_log WHERE pointmsg='취소로 인한 포인트 복구' AND f_reservationidx='".$f_reservationidx."'";
                        $query = $this->db->query($sql)->result_array();

                        if ($query=="") {

                        } else {
                            $row1 = $query[0];
                            $query = "INSERT INTO tbl_point_log(usrserial, valpoint, regdate, pointtype, destroydate, pointmsg, logmsg, f_reservationidx)
                            VALUES ('".$f_usrserial."', '".$user_return_pointer."', '".$cancel_date."', '4', '0000-00-00 00:00:00'
                            , '취소로 인한 포인트 복구', '취소로 인한 포인트 복구', '".$f_reservationidx."')";
                            $this->db->query($sql);

                            $query = "UPDATE tbl_usr_list SET usrpoint=usrpoint+".$user_return_pointer." WHERE usrserial='".$post_user_serial."'";
                            $this->db->query($sql);
                        }

                        $sql = "UPDATE new_coupon_list SET status='0' WHERE serial='".$coupon_serial."'";
                        $this->db->query($sql);
                    }
                }




                $sql="update tbl_pay_log set f_logstatus='2' WHERE  f_paylogidx=".$f_paylogidx ;

            }else if($workmode=="recover"){
                $sql="update tbl_pay_log set f_logstatus='1' WHERE   f_paylogidx=".$f_paylogidx ;
            }
            $this->db->query($sql);
        }

        return 1;

    }
}