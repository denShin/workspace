<?php
/**
 * CarmoreCallBoard_model - 고객 전화문의 관리
 */

class CarmoreCallBoard_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    // 고객 전화상담갯수
    function getCallMngCount($sv=""){
        $sql="select count(*) as cnt  from   admin_callboard   where stats='y'  ";

        if($sv!=""){
            $sql.=" and totalcontent like '%".$this->db->escape_str($sv)."%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    //고객 전화상담 리스트
    function getCallMngPageList($page=1,$pageline=20,$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select *
          ,(select callboardcate_name  from admin_callboardcate where callboardcate_idx=a.callboardcate_idx) as callboardcate_name
            from   admin_callboard a  where stats='y'  ";

        if($sv!=""){
            $sql.=" and totalcontent like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by call_idx desc limit $start,20";
        $query = $this->db->query($sql);

        return $query->result();
    }

    // 고객 전화상담 상세
    function getCallMngDetail($call_idx)
    {
        $sql="select *  from admin_callboard a where call_idx='$call_idx'";

        $query = $this->db->query($sql)->result_array();
        $row = $query[0];
        return $row;
    }

    // 고객 상담내역 검색
    function getsearchList($sv){

        // 예약내역검색 - 전화번호 ,이름
        $sql="SELECT  
		CASE 
		WHEN trl.f_paystatus='1' THEN '결제완료' 
		WHEN trl.f_paystatus='2' THEN '취소'
		WHEN trl.f_paystatus='0' THEN '검색'
		END AS paystats				
         , CONCAT(brand,' ', model)  AS car_model,
		CASE 
		WHEN crti_reserv_rent_type='1' THEN '단기' 
		WHEN crti_reserv_rent_type='2' THEN '장기' 
		END AS renttype				
                 
          , nrs.driver_name, nrs.driver_phone 
        , crti_start_date, crti_end_date, company.name AS company_name, branch.branchName AS branch_name
        ,trl.f_regdate 
        
           FROM
               carmore_reservation_term_information crti INNER JOIN
               tbl_reservation_list trl INNER JOIN
               new_rentSchedule nrs INNER JOIN
               tbl_usr_list tul INNER JOIN
               new_rentCar rentcar INNER JOIN
               new_car car INNER JOIN
               new_rentCompany company INNER JOIN
               new_rentCompany_branch branch
               ON
                   crti_reservation_idx=trl.f_reservationidx AND trl.f_bookingid=nrs.serial AND tul.usrserial=trl.f_usrserial AND
                   trl.f_carserial=rentcar.serial AND rentcar.car_serial=car.serial AND trl.company_serial=company.serial
		   AND trl.f_companycode=branch.serial
		   
         WHERE driver_name like '%$sv%' or  driver_phone like '%$sv%'  or  tul.kakaoid like '%$sv%'  or  trl.f_reservationidx like '%$sv%' 
         order by f_regdate desc limit 100 ";

        $reservrow = $this->db->query($sql)->result_array();

        $sql="select * from admin_callboard where   call_name like '%$sv%' or  call_number like '%$sv%'  order by regdate desc limit 100 ";
        $callrow = $this->db->query($sql)->result_array();

        $result["reservrow"]=$reservrow;
        $result["callrow"]=$callrow;
        return $result ;

    }

    //  정보 수정,등록,삭제
    public function procCallBoard($data)
    {
        $return_v="";
        $emode =$data["emode"];

        if($emode=="new"){

            $totalcontent =join(",",$data);
            $nowtime =date("YmdHis");
            $sql = "INSERT INTO admin_callboard 	(	mem_id,  mem_name,  call_name,  call_number,  call_reservnum,
                      call_date,  call_time,  call_part,  callboardcate_idx,  call_ques
                      ,  call_response,  call_etc,totalcontent,  regdate)
				VALUES('".$data["mem_id"]."'
				, 	'".$data["mem_name"]."'
				,'".$data["call_name"]."'
				,'".$data["call_number"]."'
				,'".$data["call_reservnum"]."'
				,'".$data["call_date"]."'
				,'".$data["call_time"]."'
				,'".$data["call_part"] ."'
				,'".$data["callboardcate_idx"] ."'
				,'".$data["call_ques"] ."','".$data["call_response"] ."','".$data["call_etc"] ."','$totalcontent',now());";

            $this->db->query($sql);
            $return_v="y";

        }else if($emode=="edit"){

            $totalcontent =join(",",$data);
            $sql = "update  admin_callboard set
                  call_name='".$this->db->escape_str($data["call_name"])."'
                  ,call_number='".$this->db->escape_str($data["call_number"])."' 
                  ,call_reservnum='".$this->db->escape_str($data["call_reservnum"])."' 
                   ,call_date='".$this->db->escape_str($data["call_date"])."' 
                    ,call_time='".$this->db->escape_str($data["call_time"])."' 
                    ,call_part='".$this->db->escape_str($data["call_part"])."' 
                    ,callboardcate_idx='".$this->db->escape_str($data["callboardcate_idx"])."' 
                    ,call_ques='".$this->db->escape_str($data["call_ques"])."' 
                    ,call_response='".$this->db->escape_str($data["call_response"])."' 
                    ,call_etc='".$this->db->escape_str($data["call_etc"])."' 
                    ,totalcontent='$totalcontent'
             where call_idx='".$data["call_idx"]."'";


            $this->db->query($sql);
            $return_v="y";
        }else if($emode=="del"){

            $sql = "update admin_callboard  set stats='x'    where call_idx='".$this->db->escape_str($data["call_idx"])."'";
            $this->db->query($sql);

        }
        return "y";
    }

    // 관리자 팀 정보 가져오기
    function getCallCateInfo($itype,$callboardcate_idx)
    {
        if($itype =="list")
        {
            $sql="select *  from admin_callboardcate  a where stats='y' order by ordernum asc ";
            $query = $this->db->query($sql);

            return $query->result();
        }
        else{
            $sql="select * from admin_callboardcate where  callboardcate_idx='$callboardcate_idx' ";
            $row = $this->db->query($sql)->result_array();
            return $row[0];
        }
    }


    function setCallCateInfo($data){
        $emode=$data["emode"];
        $callboardcate_idx=$data["callboardcate_idx"];
        $callboardcate_name=$data["callboardcate_name"];

        if($emode=="new"){
            $sql="select count(*) as cnt  from   admin_callboardcate  where callboardcate_name='$callboardcate_name'";

            $cnt = $this->db->query($sql)->row()->cnt;

            if($cnt >0){
                $result["result"]="n";
            }else{
                $sql="INSERT INTO admin_callboardcate ( callboardcate_name)VALUES  (    '$callboardcate_name'  );";
                $query = $this->db->query($sql);

                $result["result"]="y";
            }
        }else if($emode=="edit"){
            $sql="update admin_callboardcate set  callboardcate_name='$callboardcate_name' where callboardcate_idx='$callboardcate_idx'";
            $query = $this->db->query($sql);
            $result["result"]="y";
        }else if($emode=="del"){
            $sql="update admin_callboardcate set  stats='x' where callboardcate_idx='$callboardcate_idx'";
            $query = $this->db->query($sql);
            $result["result"]="y";
        }

        return $result;

    }

    //카테고리 정렬순서 변경
    function setCallCateOrder($data){
        $arr_order= $data["order"];

        for($i=0;$i < sizeof($arr_order);$i++){
            $callboardcate_idx = $arr_order[$i];
            $sql="update admin_callboardcate set  ordernum='$i' where callboardcate_idx='$callboardcate_idx'";

            $query = $this->db->query($sql);
        }
    }

    public function select_callcate($tmp_cate)
    {
        $sql=" select callboardcate_idx,callboardcate_name from admin_callboardcate where stats='y' order by ordernum asc";
        $applistrs = $this->db->query($sql);

        $return_str="";
        foreach($applistrs->result_array() AS $row)
        {
            $callboardcate_idx	=$row['callboardcate_idx'];
            $callboardcate_name	=$row['callboardcate_name'];

            $return_str .= "<option value='".$callboardcate_idx."'";
            if($tmp_cate ==$callboardcate_idx)
            {
                $return_str.= " selected";
            }
            $return_str.= " >".$callboardcate_name."</option>";
        }
        return $return_str;
    }



}