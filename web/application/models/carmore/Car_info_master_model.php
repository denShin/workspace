<?php
/**
 * Created by PhpStorm.
 * User: teamo2_server
 * Date: 2019-04-22
 * Time: 20:37
 */

class Car_info_master_model extends WS_Model {
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * 차종 마스터 리스트 그룹별로 받아오기
     *
     * @return array
     */
    public function get_car_inventory_using_group()
    {
        $inventory = [
            'type0' => [],
            'type1' => [],
            'type2' => [],
            'type3' => [],
            'type4' => [],
            'type5' => [],
            'type6' => [],
            'type7' => []
        ];

        $query = "SELECT carinfo_idx, carinfomst_brand, carinfomst_model, carinfomst_type FROM carinfo_master ORDER BY carinfomst_type ASC, carinfomst_brand ASC, carinfomst_model ASC";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute();
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            $type = $stmt['carinfomst_type'];

            array_push($inventory['type'.$type], [
                'carInfoIdx' => $stmt['carinfo_idx'],
                'brand'      => $stmt['carinfomst_brand'],
                'model'      => $stmt['carinfomst_model']
            ]);
        }

        return $inventory;
    }

}
