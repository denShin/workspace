<?php
/**
 * CarinfoManage_model - 카모아 자동차 정보
 */
class CarinfoManage_model  extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    // 자동차 상세정보가져오기
    function getCarinfoDetail($serial){
        $sql="select *      from new_car a where  serial='$serial'";

        $query = $this->db->query($sql)->result_array();
        $row = $query[0];
        return $row;
    }

    // 자동차 리스트 카운트
    function getCarinfoCount($sp="",$sv=""){
        $sql="select count(*) as cnt from  new_car where 1=1";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    // 자동차 리스트 정보
    function getCarinfoPageList($page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select *     from   new_car a where 1=1 ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by SERIAL desc limit $start,20";

        $query = $this->db->query($sql);

        return $query->result();
    }

    // 자동차 정보 등록,업데이트
    function procCarinfo($emode,$data){

        if($emode=="new"){

            $nowtime =date("YmdHis");
            $sql = "INSERT INTO new_car 	(brand,  cartype,  cartype_flag,  model,  carmore_model,
                  min_fuel_efficiency,  max_fuel_efficiency)
				VALUES('".$data["brand"]."'	,'".$data["cartype"]."','".$data["cartype_flag"]."','".$data["model"]."'				
				,'".$data["carmore_model"]."','".$data["min_fuel_efficiency"]."','".$data["max_fuel_efficiency"]."');";

            $this->db->query($sql);
            $return_v="y";

        }else if($emode=="edit"){

            $sql="UPDATE  new_car SET  brand = '".$data["brand"]."',cartype = '".$data["cartype"]."' 
                ,  cartype_flag = '".$data["cartype_flag"]."',model = '".$data["model"]."',carmore_model = '".$data["carmore_model"]."' 
                 , min_fuel_efficiency = '".$data["min_fuel_efficiency"]."', max_fuel_efficiency = '".$data["max_fuel_efficiency"]."'
                  WHERE serial ='".$data["serial"]."' ";

        }else if($emode=="del"){

        }
    }

}