<?php
/**
 * Carinfomaster_model - 카모아 자동차 마스터 관련 db 처리
 */
class Carinfomaster_model  extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    // 자동차 마스터 상세
    function getCarinfomstDetail($carinfokey){
        $sql="select *      from carinfo_master a where  carinfokey='$carinfokey'";

        $query = $this->db->query($sql)->result_array();
        $row = $query[0];
        return $row;
    }

    // 각 자동차별 이미지
    function getCarinfoImage($fileuptype,$carinfokey){


        $sql="select * from admin_uploadfile where content_code='$carinfokey'
                and  fileuptype='$fileuptype' ";

        $query = $this->db->query($sql)->result_array();

        return $query ;
    }

    // 자동차 마스터 카운트
    function getCarinfomstCount($sp="",$sv=""){
        $sql="select count(*) as cnt from  carinfo_master where 1=1";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    // 이미지 publish 변경
    function procPublishimage($publish_yn,$filename){
        $sql="UPDATE  admin_uploadfile SET  publish_yn = '".$publish_yn."'    WHERE fileSaveName ='".$filename."' ";

        $this->db->query($sql);
    }

    //지점별 렌터카정보
    function getBranchrentcarList( $companyserial,$branchserial){

        $sql="SELECT * from new_car  a   LEFT JOIN
                  (select  a.carserial,c.* from carinfo_match a inner join carinfo_master c on a.carinfo_idx =c.carinfo_idx
                where rentCompany_serial='$companyserial' and
                    rentCompany_branch_serial ='$branchserial') B

                  ON a.serial =B.carserial
                    WHERE SERIAL IN (
                    select  distinct(car_serial) FROM   new_rentCar a
                                  where a.rentCompany_serial='$companyserial' and
                                        a.rentCompany_branch_serial ='$branchserial' and state not in(9,11) )
                    ";

        $query = $this->db->query($sql)->result_array();

        return $query ;
    }

    // 차종 마스터 정보
    function getmstcardataList($carinfomst_brand,$carinfomst_type){

        $sql="select * from carinfo_master WHERE carinfomst_brand='$carinfomst_brand' and carinfomst_type='$carinfomst_type'";

        $query = $this->db->query($sql)->result_array();

        return $query ;
    }


    //자동차 리스트
    function getsearchcardata(){

        $sql="select CONCAT(carinfomst_brand,' ', carinfomst_model)  as title, carinfo_idx  from carinfo_master  ";

        $rowresult = $this->db->query($sql)->result_array();

        foreach($rowresult as $entry)
        {
            $title = $entry["title"];
            $carinfo_idx = $entry["carinfo_idx"];

            $data["text"] = $title;
            $data["value"] =$title.";".$carinfo_idx;
            $arr_alldatalist[]=$data;
        }

        return $arr_alldatalist ;
    }

    // 지점별 자동차 리스트
    function getsearchbranch()
    {


        $sql="select * from new_rentCompany where test_check='0'
               and state='1'
                 AND SERIAL IN (SELECT DISTINCT(rentcompany_serial) FROM new_rentCompany_branch
                   WHERE carmore_normal_available  =1 OR carmore_month_available  =1)
             order by regdate desc";

        $rowresult = $this->db->query($sql)->result_array();

        foreach($rowresult as $entry)
        {
            $companyserial = $entry["serial"];
            $name = $entry["name"];
            $sql = "select * from   new_rentCompany_branch a  where rentCompany_serial='$companyserial' and state='1'";
            $dtlrow = $this->db->query($sql)->row() ;
            $branchserial = $dtlrow->serial;
            $branchName = $dtlrow->branchName;

            $title =$name." ".$branchName;
            $data["text"] = $title;
            $data["value"] =$title.";".$companyserial.";".$branchserial;
            $arr_alldatalist[]=$data;
        }

        return $arr_alldatalist ;
    }

    // 자동차 마스터 리스트
    function getCarinfomstPageList($page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select *   ,(select filesavename from admin_uploadfile
          where content_code=a.carinfokey and  fileuptype='main' and publish_yn='y' ) as mainimage 
             ,(select count(*) from admin_uploadfile
          where content_code=a.carinfokey and  fileuptype='sub'  and publish_yn='y') as subcnt 
           from   carinfo_master a where 1=1 ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by carinfo_idx desc limit $start,20";

        $query = $this->db->query($sql);

        return $query->result();
    }

    // 자동차 마스터와 지점별 자동차 매칭정보 등록
    function procCarmatch($data){


       $saveparam= $data["saveparam"];
       $rentCompany_serial= $data["rentCompany_serial"];
       $rentCompany_branch_serial = $data["rentCompany_branch_serial"];
       $arrparam = explode("-",$saveparam);

       for($i=0;$i < sizeof($arrparam); $i++){

            $arrdtlparam  = explode(":",$arrparam[$i]);
            $carserial =$arrdtlparam[0];
            $modelselect =$arrdtlparam[3];
           $sql="delete from  carinfo_match where carserial='$carserial' 
                       and rentCompany_serial='$rentCompany_serial' and rentCompany_branch_serial='$rentCompany_branch_serial'";

           $this->db->query($sql);

           $sql="insert into carinfo_match(carserial,carinfo_idx,rentCompany_serial,rentCompany_branch_serial )
                  values('$carserial','$modelselect','$rentCompany_serial','$rentCompany_branch_serial')";


           $this->db->query($sql);
       }
    }

    // 자동차 마스터 정보 등록,수정 ,삭제
    function procCarinfomst($emode,$data){

        if($emode=="new"){

            $nowtime =date("YmdHis");
            $sql = "INSERT INTO carinfo_master 	(carinfokey,  carinfomst_brand,  carinfomst_model,  carinfomst_memo,
                                    carinfomst_type,  min_fuel_efficiency,  max_fuel_efficiency,  regdate)
				VALUES('".$data["carinfokey"]."'	,'".$data["carinfomst_brand"]."','".$data["carinfomst_model"]."'
				,'".$data["carinfomst_memo"]."'	,'".$data["carinfomst_type"]."','".$data["min_fuel_efficiency"]."'
				,'".$data["max_fuel_efficiency"]."',now() );";

            $this->db->query($sql);
            $return_v="y";

        }else if($emode=="edit"){

            $sql="UPDATE  carinfo_master SET  carinfomst_brand = '".$data["carinfomst_brand"]."',carinfomst_model = '".$data["carinfomst_model"]."' 
                ,  carinfomst_memo = '".$data["carinfomst_memo"]."'  ,min_fuel_efficiency = '".$data["min_fuel_efficiency"]."'  
                 , max_fuel_efficiency = '".$data["max_fuel_efficiency"]."',carinfomst_type='".$data["carinfomst_type"]."'
                  WHERE carinfokey ='".$data["carinfokey"]."' ";

            $this->db->query($sql);
            $return_v="y";
        }else if($emode=="del"){

            $sql="delete from   carinfo_master    WHERE carinfokey ='".$data["carinfokey"]."' ";
            $this->db->query($sql);
            $sql="delete from   admin_uploadfile    WHERE content_code ='".$data["carinfokey"]."' ";
            $this->db->query($sql);
            $return_v="y";


        }
 
    }

    //즐겨찾기 등록,삭제
    function procfaver($data){

        $ptype =$data["ptype"];
        $carinfo_idx =$data["carinfo_idx"];
        $township =$data["township"];
        $city =$data["city"];

        if($ptype=="addfavor"){
            $sql="select count(*) as cnt from  carinfo_areafavor where township='".$township."' and city='".$city."'
                    and carinfo_idx='".$carinfo_idx."'";
            $cnt = $this->db->query($sql)->row()->cnt;

            if($cnt <1){
                $nowtime = time();
                $sql = "INSERT INTO carinfo_areafavor 	( carinfo_idx,  township,  city,ordernum)
				VALUES( '".$data["carinfo_idx"]."','".$data["township"]."'
				,'".$data["city"]."','".$nowtime."');";

                $this->db->query($sql);
            }

        }else if($ptype=="delfavor"){

            $sql="delete from   carinfo_areafavor where township='".$township."' and city='".$city."'
                    and carinfo_idx='".$carinfo_idx."'";
            $this->db->query($sql);
            $return_v="y";

        }

    }

    //즐겨찾기 순서 저장
    function procfaverorder($data){
        $order =$data["order"];
        $arr_ordernum = explode(",",$order);

        for($i=0;$i < sizeof($arr_ordernum);$i++){
            $ordernum = $i+1;
            $areafavor_idx = $arr_ordernum[$i];
            $sql="update carinfo_areafavor set ordernum ='$ordernum' WHERE  areafavor_idx=".$areafavor_idx ;
            $this->db->query($sql);
        }
    }

    //각 지역별 자동차 리스트 
    function getAreacarlist($city, $township){
        $sql="select *   ,(select filesavename from admin_uploadfile
          where content_code=a.carinfokey and  fileuptype='main' and publish_yn='y' ) as mainimage 
          
           from   carinfo_master a where 1=1 ";

        if($city!="" &&  $township!=""  ){
            $sql.=" and carinfo_idx not in(select carinfo_idx from carinfo_areafavor where
                city ='".$city."' and township='".$township."')";
        }

        $sql.=" order by carinfo_idx desc  ";

        $query = $this->db->query($sql);

        return $query->result();
    }

    //각 지역별 즐겨찾기 차종리스트
    function getAreafavorcarlist($city, $township){
        $sql="select *   ,(select filesavename from admin_uploadfile
          where content_code=a.carinfokey and  fileuptype='main' and publish_yn='y' ) as mainimage 
          
           from   carinfo_master a inner join carinfo_areafavor b on a.carinfo_idx=b.carinfo_idx
            where 1=1 ";

        if($city!="" &&  $township!=""  ){
            $sql.=" and city ='".$city."' and township='".$township."'";
        }

        $sql.=" order by b.ordernum asc  ";

        $query = $this->db->query($sql);

        return $query->result();
    }

    // 구군 코드 가져오기
    function gettownshopcode($city){

        $sql="select distinct(township) from new_area where city ='".$city."'";
        $query = $this->db->query($sql)->result_array();

        return $query ;
    }

    //  신규지역 코드
    function getnewtownshopcode($city){
        $sql="select ct_value , township  from new_area where city_value='".$city."' group by ct_value";
        $query = $this->db->query($sql)->result_array();

        return $query ;
    }


}