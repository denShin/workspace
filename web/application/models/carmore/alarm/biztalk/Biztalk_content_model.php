<?php
/**
 * 비즈톡 목록 가져오기
 *
 * @property CI_Loader load
 * @property CI_DB_pdo_mysql_driver $db
 *
 * @property Workspace_common_func $workspace_common_func
 */

class Biztalk_content_model extends WS_Model {
    private $utc_now;

    public function __construct()
    {
        parent::__construct();

        $this->utc_now = date("Y-m-d H:i:s");
        $this->load->library('Workspace_common_func');
    }

    public function retransmission_biztalk($mt_pr)
    {
        $query = "UPDATE ata_mmt_tran SET date_client_req=?, msg_status=1, ata_id='', date_mt_sent = NULL, date_rslt = NULL, date_mt_report = NULL, rs_id = NULL, report_code = NULL WHERE mt_pr=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$this->utc_now, $mt_pr]);
    }

    public function cancel_biztalk($mt_pr)
    {
        $query = "UPDATE ata_mmt_tran SET msg_status=0 WHERE mt_pr=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$mt_pr]);
    }

    public function modify_biztalk($mt_pr, $content, $req_time, $rec_num, $retransmission)
    {
        $set = "";
        if ($req_time !== "")
            $set = ", date_client_req='".$this->workspace_common_func->kr_to_utc($req_time)."'";

        $query = "UPDATE ata_mmt_tran SET content=?, recipient_num=?".$set." WHERE mt_pr=?";

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$content, $rec_num, $mt_pr]);

        if ($retransmission === 1)
            $this->retransmission_biztalk($mt_pr);
    }

}
