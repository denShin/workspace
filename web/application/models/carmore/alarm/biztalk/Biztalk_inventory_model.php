<?php
/**
 * 비즈톡 목록 가져오기
 *
 * @property CI_Loader load
 * @property CI_DB_pdo_mysql_driver $db
 *
 * @property Workspace_common_func $workspace_common_func
 */

class Biztalk_inventory_model extends WS_Model {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('Workspace_common_func');
    }

    /**
     * 알림톡 상태를 한글로
     *
     * @param $status
     *
     * @return string
     */
    private function msg_status_convert($status)
    {
        switch ((int)$status)
        {
            case 0:
                return "취소";
            case 1:
                return "전송대기";
            case 2:
                return "전송중";
            case 3:
                return "전송완료";
            case 4:
                return "월렌트 연장으로 취소";
            default:
                return "";
        }
    }

    private function report_code_convert($code)
    {
        switch ($code)
        {
            case "":
                return "";
            case "1000":
                return "정상 전송";
            default:
                return "전송 오류";
        }
    }

    /**
     * 검색조건에 다른 쿼리 리턴
     *
     * @param $moid
     * @param $rec_num
     *
     * @return string
     */
    private function get_where_query($moid, $rec_num)
    {
        $return_string = "";

        if ($moid !== "")
            $return_string .= " AND etc_text_1='".$moid."' ";

        if ($rec_num !== "")
            $return_string .= " AND recipient_num='".$rec_num."' ";

        return $return_string;
    }

    public function load_biztalk_total_count($moid, $rec_num)
    {
        $where_query = $this->get_where_query($moid, $rec_num);

        if ($where_query !== "")
        {
            $query = "SELECT count(*) total_cnt FROM ata_mmt_tran WHERE msg_type='1008' ".$where_query;

            $sql = $this->slave->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $sql->execute();
            if ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
                return (int)$stmt['total_cnt'];
            else
                return 0;
        }
        else
            return 0;

    }

    public function load_biztalk_inventory($per_page, $rows, $moid, $rec_num)
    {
        $where_query = $this->get_where_query($moid, $rec_num);

        if ($where_query !== "")
        {
            if (!$per_page)
                $per_page = 1;

            $return_object = [];

            $limit_start = ($per_page-1) * $rows;

            $query = "
            SELECT
                mt_pr, date_client_req, recipient_num, content, msg_status, IFNULL(report_code, '') report_code, template_code, etc_text_1
            FROM ata_mmt_tran WHERE msg_type='1008' ".$where_query."
            ORDER BY mt_pr DESC LIMIT ".$limit_start.", ".$rows;

            $sql = $this->slave->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $sql->execute();
            while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
            {
                $std = new stdClass();

                $report_code = $stmt['report_code'];
                $msg_status  = (int)$stmt['msg_status'];

                $std->mtPr         = $stmt['mt_pr'];
                $std->moid         = $stmt['etc_text_1'];
                $std->reqDate      = $this->workspace_common_func->utc_to_kr($stmt['date_client_req']);
                $std->recNum       = $this->workspace_common_func->format_tel($stmt['recipient_num']);
                $std->content      = $stmt['content'];
                $std->status       = $msg_status;
                $std->msgStatus    = $this->msg_status_convert($msg_status);
                $std->reportCode   = $report_code;
                $std->reportStr    = $this->report_code_convert($report_code);
                $std->templateCode = $stmt['template_code'];

                array_push($return_object, $std);
            }

            return $return_object;

        }
        else
            return [];

    }

    public function load_send_error_inventory()
    {
        $inventory = [];

        $before_24hour = date('Y-m-d H:i:s', strtotime('-1 days'));

        $query = "
        SELECT
            etc_text_1, recipient_num, date_client_req
        FROM
            ata_mmt_tran
        WHERE
            date_client_req > ? AND report_code='3050' AND etc_text_1 <> ''
        GROUP BY etc_text_1, recipient_num";

        $sql = $this->slave->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$before_24hour]);
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            $information = new stdClass();

            $information->reservationIdx = trim($stmt['etc_text_1']);
            $information->phoneNumber    = trim($stmt['recipient_num']);
            $information->requestDate    = date('Y-m-d H:i:s', strtotime("+9 hours", strtotime($stmt['date_client_req'])));

            array_push($inventory, $information);
        }

        return $inventory;
    }
}
