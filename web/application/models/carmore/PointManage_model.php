<?php
/**
 * PointManage_model - 포인트 로그 관련 db
 */

class PointManage_model  extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    // 포인트 로그 개수
    function getPointlogCount($sp="",$sv=""){
        $sql="select count(*) as cnt from  tbl_point_log  a   INNER JOIN tbl_usr_list c
          ON  a.usrserial=c.usrserial  where 1=1 ";


        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }

        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    // 포인트 로그 페이지 정보
    function getPointlogPageList($page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select *   from   tbl_point_log a   INNER JOIN tbl_usr_list c
            ON  a.usrserial=c.usrserial  where 1=1 ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by pointsrl desc limit $start,20";

        $query = $this->db->query($sql);


        return $query->result();
    }

    // 포인트 부여한 관리자 이름
    function getAdminname($pointsrl){

        $sql="select admin_name from admin_member where admin_email in( select mem_id from admin_board where pointsrl='$pointsrl')";

        $admin_name = $this->db->query($sql)->row()->admin_name;
        return $admin_name;

    }
}