<?php
/**
 * Created by PhpStorm.
 * User: teamo2_server
 * Date: 2019-04-28
 * Time: 19:09
 */

class Stroller_model extends WS_Model {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 유모차 총 업체수 가져오기
     *
     * @author DEN
     *
     * @param $search_param int    검색조건 (0: 없음, 1: 업체명)
     * @param $search_value string 검색스트링
     *
     * @return int
     */
    public function get_total_stroller_count($search_param, $search_value)
    {
        $query = "SELECT count(*) cnt FROM carmore_stroller WHERE cs_state=1 ";

        switch ($search_param)
        {
            case 0:
                break;
            case 1:
                $query .= " AND cs_name LIKE '%$search_value%'";
        }

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute();
        if ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
            return (int)$stmt['cnt'];
        else
            return 0;
    }

    /**
     * 검색조건에 따른 유모차업체 목록 가져오기
     *
     * @author DEN
     *
     * @param $page         int    뷰 페이지
     * @param $max_row      int    한 페이지당 최대로우
     * @param $search_param int    검색조건 (0: 없음, 1: 업체명)
     * @param $search_value string 검색스트링
     *
     * @return array
     */
    public function get_stroller_inventory($page, $max_row, $search_param, $search_value)
    {
        $inventory   = [];

        if (!$page)
            $page = 1;

        $limit_start = ($page-1) * $max_row;

        $query = "SELECT cs_idx, cs_name, cs_type FROM carmore_stroller WHERE cs_state=1";

        switch ($search_param)
        {
            case 0:
                break;
            case 1:
                $query .= " AND cs_name LIKE '%$search_value%'";
        }

        $query .= "
        ORDER BY cs_idx DESC
        LIMIT ".$limit_start.", ".$max_row;

        $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute();
        while ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            array_push($inventory, [
                'idx'  => $stmt['cs_idx'],
                'type' => (int)$stmt['cs_type'],
                'name' => $stmt['cs_name']
            ]);

        }

        return $inventory;

    }

}