<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class CouponLogManage extends CI_Controller {

    private $BOARD_CODE,$ARR_PERMISSION,$LOGINID;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('pagination');
        $this->load->library('Customfunc');
        $this->load->model('carmore/CouponManage_model');
        $this->LOGINID =$this->session->userdata('admin_id');

        if( $this->session->userdata('admin_id') == ""){
            echo "<script>location.href='/adminmanage/Login'</script>";exit();
        }
    }

    public function index()
    {
        // 상단메뉴 퍼미션
        $this->ARR_PERMISSION=$this->customfunc->get_permissionArray($this->session->userdata('admin_id'));

        $ptype = $this->input->get('ptype', TRUE);

        switch($ptype) {

            case "pop" : $this->loadCouponLogPopList(); break;
            case "w" : $this->loadCouponLogWrite(); break;
            default : $this->loadCouponLogList(); break;
        }
    }

    public function loadCouponLogPopList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);

        if(!$sp) $sp="";
        if(!$sv) $sv="";

         // postcurldata($data)

        $rowresult = $this->CouponManage_model->getCouponUsePageList($page, 20, $sp, $sv);
        $total = $this->CouponManage_model->getCouponUseCount($sp, $sv);

        $startnum = $total-($page-1) * 20;

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/carmore/CouponLogManage";
        $config['suffix']     ="&ptype=u&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];
        foreach($rowresult as $entry)
        {
            $arr_datalist["SERIAL"]=$entry->SERIAL;
            $arr_datalist["coupontitle"]=$entry->title;
            $arr_datalist["TYPE"]=$entry->TYPE;
            $arr_datalist["value_type"]=$entry->value_type;

            $arr_datalist["couponprice"]= number_format( $entry->value,0);
            $arr_datalist["deadline_type"]=$entry->deadline_type;
            $arr_datalist["start_date"]=$entry->start_date;
            $arr_datalist["end_date"]=$entry->end_date;
            $arr_datalist["deadline_after_register"]=$entry->deadline_after_register;
            $arr_datalist["quantity"]=$entry->quantity;
            $arr_datalist["certificate_key"]=$entry->certificate_key;
            $arr_datalist["limit_pay"]=$entry->limit_pay;
            $arr_datalist["description"]=$entry->description;
            $arr_datalist["memo"]=$entry->memo;
            $arr_datalist["cstatus"]=$entry->cstatus;

            if($entry->cstatus =="0"){
                $arr_datalist["cstatustr"]="미사용";
            }else if($entry->cstatus =="1"){
                $arr_datalist["cstatustr"]="사용";
            }else if($entry->cstatus =="2"){
                $arr_datalist["cstatustr"]="미리발급";
            }else if($entry->cstatus =="9"){
                $arr_datalist["cstatustr"]="기간만료";
            }

            $arr_datalist["kakaonickname"]=$entry->kakaonickname;
            $arr_datalist["usrname"]=$entry->usrname;
            $arr_datalist["destroy_date"]=$entry->destroy_date;
            $regdate =$entry->cregdate;
            $arr_datalist["regdatev"] =$regdate;
            $arr_datalist["regdate"] =$this->customfunc->get_dateformat($regdate);


            $arr_alldatalist[]=$arr_datalist;
        }

        $data["list"]=$arr_alldatalist;
        $data["per_page"]=$per_page;
        $data["startnum"]=$startnum;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->LOGINID;

        $this->load->view('carmore/couponlogpoplist', array('data'=>$data));
     }


    public function loadCouponLogList()
    {
        $this->load->helper('url'); // load the helper first
        $per_page = $this->input->get('per_page', TRUE);
        if($per_page <20 ) $per_page=0;

        $page = ($per_page/20) +1;
        $sp = $this->input->get('sp', TRUE);
        $sv = $this->input->get('sv', TRUE);

        if(!$sp) $sp="";
        if(!$sv) $sv="";

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        // postcurldata($data)

        $rowresult = $this->CouponManage_model->getCouponUsePageList($page, 20, $sp, $sv);
        $total = $this->CouponManage_model->getCouponUseCount($sp, $sv);

        $startnum = $total-($page-1) * 20;

        ## codeigniter >> 페이지네이션 이동
        $this->config->load('bootstrap_pagination');
        $config = $this->config->item('pagination');
        $config['total_rows']     = $total; // 게시물총수
        $config['per_page']       = "20";  // 게시물출력수
        $config['base_url']     =$config['base_url']."/carmore/CouponLogManage";
        $config['suffix']     ="&ptype=u&sp=".$sp."&sv=".$sv;
        $config['use_global_url_suffix']     =false;
        $config['reuse_query_string']     =false;

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();

        $arr_alldatalist=[];
        foreach($rowresult as $entry)
        {
            $arr_datalist["SERIAL"]=$entry->SERIAL;
            $arr_datalist["coupontitle"]=$entry->title;
            $arr_datalist["TYPE"]=$entry->TYPE;
            $arr_datalist["value_type"]=$entry->value_type;

            $arr_datalist["couponprice"]= number_format( $entry->value,0);
            $arr_datalist["deadline_type"]=$entry->deadline_type;
            $arr_datalist["start_date"]=$entry->start_date;
            $arr_datalist["end_date"]=$entry->end_date;
            $arr_datalist["deadline_after_register"]=$entry->deadline_after_register;
            $arr_datalist["quantity"]=$entry->quantity;
            $arr_datalist["certificate_key"]=$entry->certificate_key;
            $arr_datalist["limit_pay"]=$entry->limit_pay;
            $arr_datalist["description"]=$entry->description;
            $arr_datalist["memo"]=$entry->memo;
            $arr_datalist["cstatus"]=$entry->cstatus;

            if($entry->cstatus =="0"){
                $arr_datalist["cstatustr"]="미사용";
            }else if($entry->cstatus =="1"){
                $arr_datalist["cstatustr"]="사용";
            }else if($entry->cstatus =="2"){
                $arr_datalist["cstatustr"]="미리발급";
            }else if($entry->cstatus =="9"){
                $arr_datalist["cstatustr"]="기간만료";
            }

            $arr_datalist["kakaonickname"]=$entry->kakaonickname;
            $arr_datalist["usrname"]=$entry->usrname;
            $arr_datalist["destroy_date"]=$entry->destroy_date;
            $regdate =$entry->cregdate;
            $arr_datalist["regdatev"] =$regdate;
            $arr_datalist["regdate"] =$this->customfunc->get_dateformat($regdate);


            $arr_alldatalist[]=$arr_datalist;
        }

        $data["list"]=$arr_alldatalist;
        $data["per_page"]=$per_page;
        $data["startnum"]=$startnum;
        $data["pagination"]=$pagination;
        $data["loginid"]=$this->LOGINID;

        $this->load->view('carmore/couponloglist', array('data'=>$data));
        $this->load->view('footer');
    }



    public function loadCouponLogWrite()
    {

        $pemail = $this->input->get('admin_email', TRUE);

        if($pemail !=""){
            $data["emode"]="edit";
            $arr_data = $this->Pay_model->getPayDetail($pemail);

            $data["admin_name"] =$arr_data["admin_name"];
            $data["admin_email"] =$arr_data["admin_email"];
            $data["admin_pwd"]=$arr_data["admin_pwd"];
            $data["admin_tel"] =$arr_data["admin_tel"];
            $data["team_code"] =$arr_data["team_code"];
            $data["admin_stats"] =$arr_data["admin_stats"];
            $data["member_grade"] =$arr_data["member_grade"];
            $data["config_yn"] =$arr_data["config_yn"];
            $data["offwork_yn"] =$arr_data["offwork_yn"];
            $data["buyoffer_yn"] =$arr_data["buyoffer_yn"];
            $data["dmaster_yn"] =$arr_data["master_yn"];
            $data["buyofferdeposit_yn"] =$arr_data["buyofferdeposit_yn"];
            $data["upmoneyoffer_yn"] =$arr_data["upmoneyoffer_yn"];
            $data["ordermng_yn"] =$arr_data["ordermng_yn"];
            $data["calc_yn"] =$arr_data["calc_yn"];
            $data["teamoption"]= $this->customfunc->select_team($data["team_code"]);

        }else{
            $data["emode"]="new";

            $data["admin_name"] ="";
            $data["admin_email"] ="";
            $data["admin_pwd"]= "";
            $data["admin_tel"] ="";
            $data["team_code"] ="";
            $data["admin_stats"] ="";
            $data["member_grade"] ="";
            $data["config_yn"] ="";
            $data["offwork_yn"] ="";
            $data["buyoffer_yn"] ="";
            $data["dmaster_yn"] ="";
            $data["buyofferdeposit_yn"] ="";
            $data["upmoneyoffer_yn"] ="";
            $data["branch_code"] ="";
            $data["ordermng_yn"] ="";
            $data["calc_yn"] ="";

            $data["teamoption"]= $this->customfunc->select_team("");
        }

        $this->load->view('head', array('data'=>$this->ARR_PERMISSION));
        $this->load->view('carmore/couponmanagewrite', array('data'=>$data));
        $this->load->view('footer');
    }



}
