<?php
/**
 * StaticManage_model - 통계정보 관련 db
 */
class StaticManage_model  extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    // 회원 전체 숫자
    function getTotalmemcount(){

        $sql ="select count(*) as cnt from tbl_usr_list ";

        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;

    }
    
    //통계정보 가져오기
    function getStaticData($stype,$startdate,$enddate,$dategroup=""){

        $startdate =str_replace("-","",$startdate);
        $enddate =str_replace("-","",$enddate);

        $startmonth = substr($startdate, 0, 6);
        $endmonth = substr($enddate, 0, 6);

        $caltbl =" SELECT f_totalprice,f_regdate ,DATEDIFF(f_rentstartdate ,f_regdate ) AS rdategap
              ,REPLACE(LEFT(f_regdate, 7),'-','') AS monthval
              ,REPLACE(LEFT(f_regdate,10),'-','') AS dateval ,HOUR(f_regdate) AS hourval ,DAYOFWEEK(f_regdate) AS week_n
               ,company_serial FROM tbl_reservation_list WHERE f_paystatus='1'    AND f_totalprice>0";


        if($stype=="monthcal"){

            $sql="SELECT dateval as monthval, sellamount AS totalprice FROM admin_daycal
              WHERE calcode='month' and  dateval BETWEEN '$startmonth' AND '$endmonth'     ORDER BY dateval asc";

        }else if($stype=="datecal") {

            $sql = "SELECT dateval, sellamount AS totalprice FROM admin_daycal  a
              WHERE  calcode='datecal' and dateval BETWEEN '$startdate' AND '$enddate'    GROUP BY dateval ORDER BY dateval asc";
        }else if($stype=="membermonthcal"){

            $sql="SELECT calmonth as monthval, ifnull( sum(joincount),0) AS totalcount FROM admin_membercal
              WHERE   calmonth BETWEEN '$startmonth' AND '$endmonth'   group by calmonth    ORDER BY calmonth asc";

        }else if($stype=="memberdatecal"){

            $sql="SELECT dateval, joincount AS totalcount FROM admin_membercal  a
              WHERE    dateval BETWEEN '$startdate' AND '$enddate'     ORDER BY dateval asc";
        }else if($stype=="rdategap"){

            $sql="SELECT  COUNT(*) AS cnt , rdategap  FROM ($caltbl) a
                 GROUP BY rdategap order by cnt desc limit 20 ";
        }else if($stype=="yoilcal"){

            $sql="SELECT week_n, SUM(f_totalprice) AS totalprice,count(*) as totalcnt  FROM ($caltbl) a
              WHERE dateval BETWEEN '$startdate' AND '$enddate'    GROUP BY week_n ORDER BY week_n asc";
        }else if($stype=="hourcal"){

            $sql="SELECT hourval, SUM(f_totalprice) AS totalprice,count(*) as totalcnt FROM ($caltbl) a
              WHERE dateval BETWEEN '$startdate' AND '$enddate'    GROUP BY hourval ORDER BY hourval asc";

        }else if($stype=="companycal"){

            $arrorder =explode("-",$dategroup);
            $ordfield =$arrorder[0];
            $orderby =$arrorder[1];

            $sql="select * from (SELECT b.regdate as company_regdate,a.company_serial,b.NAME as companyname
            ,  SUM(f_totalprice) AS totalprice , count(*) AS totalcount
              FROM ($caltbl) a inner join new_rentCompany b       on a.company_serial=b.SERIAL
              WHERE dateval BETWEEN '$startdate' AND '$enddate'    GROUP BY company_serial ) a ORDER BY  $ordfield $orderby 
              limit 30 ";
        }else if($stype=="cartypecal"){

            $sql="SELECT carType_flag, SUM(f_totalprice) AS totalprice FROM (
                        SELECT f_totalprice,f_regdate
                        ,REPLACE(LEFT(f_regdate, 7),'-','') AS monthval
                        ,REPLACE(LEFT(f_regdate,10),'-','') AS dateval 
                        ,HOUR(f_regdate) AS hourval ,DAYOFWEEK(f_regdate) AS week_n
                        ,company_serial
                        ,f_carserial
                        ,car_serial
                        ,carType_flag
                        FROM tbl_reservation_list a INNER JOIN 
                        (SELECT a.serial,a.car_serial,b.carType_flag FROM new_rentCar a INNER JOIN new_car b ON a.car_serial=b.serial) b
                        ON a.f_carserial  =b.serial
                        WHERE f_paystatus='1'    AND f_totalprice>0
                    ) a
              WHERE dateval BETWEEN '$startdate' AND '$enddate'   GROUP BY carType_flag   ";
        }else if($stype=="carmodelcal"){

            $sql="SELECT carmodel, SUM(f_totalprice) AS totalprice FROM (
                        SELECT f_totalprice,f_regdate
                        ,REPLACE(LEFT(f_regdate, 7),'-','') AS monthval
                        ,REPLACE(LEFT(f_regdate,10),'-','') AS dateval 
                        ,HOUR(f_regdate) AS hourval ,DAYOFWEEK(f_regdate) AS week_n
                        ,company_serial
                        ,f_carserial
                        ,car_serial
                        ,carType_flag
                        ,carmodel
                        FROM tbl_reservation_list a INNER JOIN 
                        (SELECT a.serial,a.car_serial,b.carType_flag,b.model AS carmodel FROM new_rentCar a INNER JOIN new_car b ON a.car_serial=b.serial) b
                        ON a.f_carserial  =b.serial
                        WHERE f_paystatus='1'    AND f_totalprice>0 
                    ) a
              WHERE dateval BETWEEN '$startdate' AND '$enddate'     GROUP BY carmodel ORDER BY totalprice DESC limit 20";
        }else if($stype=="ordercal"){

            if($dategroup=="date"){
                $sql="SELECT * FROM admin_ordcalresv WHERE caldate BETWEEN  '$startdate' AND '$enddate'   AND  caltype='calog' ";
            }else if($dategroup=="week"){
                $sql="SELECT WEEK(caldate) as weekstr,caldate,sum(daycal1) as daycal1 ,sum(totalcount) as totalcount  FROM admin_ordcalresv 
        WHERE caldate BETWEEN  '$startdate' AND '$enddate'   AND  caltype='calog' GROUP BY WEEK(caldate), LEFT(caldate,4) ORDER BY caldate ASC";
            }else if($dategroup=="month"){

                $sql="SELECT  REPLACE(LEFT(STR_TO_DATE(caldate,'%Y%m%d'),7),'-','.') as monthstr,caldate,sum(daycal1) as daycal1 ,sum(totalcount) as totalcount  FROM admin_ordcalresv WHERE caldate BETWEEN  '$startdate' AND '$enddate'   AND  caltype='calog'  GROUP BY  LEFT(caldate,6)";
            }


        }else if($stype=="ordercompanycal"){
            $sql="SELECT company_serial,NAME as company_name,neworder,reorder FROM (
                    SELECT company_serial 
                    ,(SELECT COUNT(*) FROM admin_ordcalresv WHERE caldate
                     BETWEEN  '$startdate' AND '$enddate'  AND caltype='orderlog' AND companyordernum=0 AND company_serial=a.company_serial) AS neworder
                    
                    ,(SELECT COUNT(*) FROM admin_ordcalresv WHERE caldate
                     BETWEEN  '$startdate' AND '$enddate'  AND caltype='orderlog' AND companyordernum>0 AND company_serial=a.company_serial) AS reorder
                      FROM admin_ordcalresv a WHERE caldate
                     BETWEEN  '$startdate' AND '$enddate'  AND caltype='orderlog'
                    GROUP BY company_serial ) a  INNER JOIN new_rentCompany b ON a.company_serial=b.serial order by reorder desc ";

        }else if($stype=="orderpiecal"){
            $sql="SELECT SUM(daycal1) AS sumdaycal1,SUM(daycal2) AS sumdaycal2,SUM(daycal3) AS sumdaycal3,SUM(daycal4) AS sumdaycal4
                  ,SUM(daycal5) AS sumdaycal5,SUM(totalcount) AS sumtotal 
                  FROM admin_ordcalresv WHERE caldate BETWEEN  '$startdate' AND '$enddate'   AND  caltype='memberlog' ";
        }else if($stype=="callcal"){
            $sql="SELECT SUM(scount) AS callcnt,caldate FROM admin_callsearchcal WHERE caltype='callcal' AND  caldate BETWEEN  '$startdate' AND '$enddate' GROUP BY caldate ORDER BY caldate asc";
        }else if($stype=="calldtlcal"){
            $sql="SELECT SUM(scount) AS callcnt,business_name,area_name FROM admin_callsearchcal WHERE caltype='callcal'  AND caldate BETWEEN  '$startdate' AND '$enddate' GROUP BY business_name ORDER BY callcnt DESC";
        }else if($stype=="rentcal"){
            $rent_type =$dategroup;
            $sql="SELECT SUM(scount) AS sumcnt,area_name FROM admin_callsearchcal WHERE caltype='rentcal'  AND caldate BETWEEN  '$startdate' AND '$enddate' and rent_type='$rent_type' GROUP BY area_name ORDER BY sumcnt DESC";
        }

        $result = $this->db->query($sql)->result();

        return $result;
    }


}