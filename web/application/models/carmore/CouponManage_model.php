<?php
/**
 * CouponManage_model - 쿠폰 관리 db
 */

class CouponManage_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    // 쿠폰 마스터 카운트
    function getCouponMstCount($sp="",$sv="",$startdate="",$enddate=""){
        $sql="select count(*) as cnt from  new_coupon_info where 1=1 ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        if($startdate !="" && $enddate !=""){
            $sql.=" and start_date between '$startdate' and '$enddate'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    // 쿠폰 마스터 리스트 
    function getCouponMstPageList($page=1,$pageline=20,$sp="",$sv="",$startdate="",$enddate=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select *  from   new_coupon_info a where 1=1 ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        if($startdate !="" && $enddate !=""){
            $sql.=" and start_date between '$startdate' and '$enddate'";
        }
        $sql.=" order by SERIAL desc limit $start,20";
        $query = $this->db->query($sql);

        return $query->result();
    }

    //쿠폰 사용 리스트 수
    function getCouponUseCount($sp="",$sv=""){
        $sql="SELECT count(*) as cnt FROM  new_coupon_list a INNER JOIN new_coupon_info b  INNER JOIN tbl_usr_list c
            ON a.info_serial =b.serial AND a.user_serial=c.usrserial where 1=1 ";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '$sv%'";
        }
        $cnt = $this->db->query($sql)->row()->cnt;
        return $cnt;
    }

    // 쿠폰 사용 리스트
    function getCouponUsePageList($page=1,$pageline=20,$sp="",$sv=""){

        if(!$page) $page=1;
        $start = ($page-1)* $pageline  ;
        $sql="select   c.kakaonickname,c.usrname , a.user_serial,  a.destroy_date,   a.register_date AS cregdate,
                        a.certificate_key AS cerkey, a.status  AS cstatus,   b.*, a.serial as coupon_list_idx
                        FROM  new_coupon_list a INNER JOIN new_coupon_info b  INNER JOIN tbl_usr_list c
            ON a.info_serial =b.serial AND a.user_serial=c.usrserial where 1=1";

        if($sp!="" && $sv!=""){
            $sql.=" and $sp like '%".$this->db->escape_str($sv)."%'";
        }
        $sql.=" order by b.serial desc limit $start,20";
        $query = $this->db->query($sql);

        return $query->result();
    }

    // 쿠폰 마스터정보
    function getCouponMstInfo($seq)
    {
        $sql="select *    from new_coupon_info a where SERIAL='$seq'";

        $query = $this->db->query($sql)->result_array();
        $row = $query[0];
        return $row;
    }

    //쿠폰 사용 카운트 정보
    function getCouponusecnt($seq){
        $sql="SELECT (SELECT COUNT(*) FROM new_coupon_list WHERE info_serial='$seq') AS tcnt
            ,(SELECT COUNT(*) FROM new_coupon_list WHERE info_serial='$seq' AND STATUS='0') AS notusecnt
            ,(SELECT COUNT(*) FROM new_coupon_list WHERE info_serial='$seq' AND STATUS='1') AS usecnt";
        $cntrow =$this->db->query($sql)->row();
        $data["tcnt"] = $cntrow->tcnt;
        $data["notusecnt"] = $cntrow->notusecnt;
        $data["usecnt"] = $cntrow->usecnt;

        return $data;

    }

    //쿠폰 마스터 정보 등록,수정,삭제
    function setCouponMstInfo($data){
        $emode=$data["emode"];

        if($emode=="new"){

            $certificate_key =$data["certificate_key"];
            $sql="select count(*) as cnt from new_coupon_info where certificate_key='$certificate_key'";

            $cntrow =$this->db->query($sql)->row();
            $tcnt = $cntrow->cnt;

            if($tcnt >0){
                $return_v="e";
            }else {

                $nowtime = date("YmdHis");
                $sql = "INSERT INTO new_coupon_info 	(title,  type,  type2,  value_type,  value,  deadline_type,  start_date,
                        end_date,  deadline_after_register,  quantity,  certificate_key,  limit_pay,  description,  memo,  register_date,
                          status,  target_company_coupon,  target_com_idx,  target_bra_idx,  target_com_first_location,  target_location_coupon,
                            target_location_str,  target_reserv_term_coupon,  target_reserv_term_start,  target_reserv_term_end
                            ,  target_reserv_period_coupon,  target_reserv_minimum_period,  target_rent_type_coupon,  target_rent_type,
                              target_carinfo_coupon,  target_carinfo_idx)
				VALUES('" . $data["title"] . "'	,'" . $data["type"] . "','" . $data["type2"] . "','" . $data["value_type"] . "'				
				,'" . $data["value"] . "','" . $data["deadline_type"] . "','" . $data["start_date"] . "','" . $data["end_date"] . "'				
				 ,'" . $data["deadline_after_register"] . "','" . $data["quantity"] . "','" . $data["certificate_key"] . "'	
				 ,'" . $data["limit_pay"] . "','" . $data["description"] . "','" . $data["memo"] . "'	
				 ,now(),'" . $data["status"] . "','" . $data["target_company_coupon"] . "','" . $data["target_com_idx"] . "'	
				 ,'" . $data["target_bra_idx"] . "','" . $data["target_com_first_location"] . "','" . $data["target_location_coupon"] . "'	
				 ,'" . $data["target_location_str"] . "','" . $data["target_reserv_term_coupon"] . "','" . $data["target_reserv_term_start"] . "'	
				 ,'" . $data["target_reserv_term_end"] . "','" . $data["target_reserv_period_coupon"] . "','" . $data["target_reserv_minimum_period"] . "'	
				 ,'" . $data["target_rent_type_coupon"] . "','" . $data["target_rent_type"] . "','" . $data["target_carinfo_coupon"] . "'	
				 ,'" . $data["target_carinfo_idx"] . "');";

                $this->db->query($sql);
                $return_v = "y";
            }

        }else if($emode=="edit"){

            $sql="UPDATE  new_coupon_info SET  title = '".$data["title"]."',type = '".$data["type"]."' 
                ,  type2 = '".$data["type2"]."',value_type = '".$data["value_type"]."',value = '".$data["value"]."' 
                 , deadline_type = '".$data["deadline_type"]."', start_date = '".$data["start_date"]."'
                  , end_date = '".$data["end_date"]."'  , deadline_after_register = '".$data["deadline_after_register"]."' 
                  ,quantity = '".$data["quantity"]."',certificate_key = '".$data["certificate_key"]."'
                 , limit_pay ='".$data["limit_pay"]."',description = '".$data["description"]."' ,memo = '".$data["memo"]."' 
                 , status ='".$data["status"]."',target_company_coupon = '".$data["target_company_coupon"]."' 
                 ,target_com_idx = '".$data["target_com_idx"]."' 
                 , target_bra_idx ='".$data["target_bra_idx"]."',target_com_first_location = '".$data["target_com_first_location"]."'
                  ,target_location_coupon = '".$data["target_location_coupon"]."' 
                 , target_location_str ='".$data["target_location_str"]."',target_reserv_term_coupon = '".$data["target_reserv_term_coupon"]."' 
                 ,target_reserv_term_start = '".$data["target_reserv_term_start"]."' 
                 , target_reserv_term_end ='".$data["target_reserv_term_end"]."',target_reserv_period_coupon = '".$data["target_reserv_period_coupon"]."'
                  ,target_reserv_minimum_period = '".$data["target_reserv_minimum_period"]."' 
                 , target_rent_type_coupon ='".$data["target_rent_type_coupon"]."',target_rent_type = '".$data["target_rent_type"]."' 
                 ,target_carinfo_coupon = '".$data["target_carinfo_coupon"]."' 
                 , target_carinfo_idx ='".$data["target_carinfo_idx"]."' 
                    WHERE SERIAL ='".$data["serial"]."' ";
            $this->db->query($sql);
            $return_v = "y";
        }else if($emode=="del"){

        }
        return $return_v;

    }


    function setCouponUseInfo($data){
        $emode=$data["emode"];

        if($emode=="new"){

            $nowtime =date("YmdHis");
            $sql = "INSERT INTO new_coupon_list 	(info_serial,  user_serial,  destroy_date,  register_date,  certificate_key,  STATUS)
				VALUES('".$data["info_serial"]."'	,'".$data["user_serial"]."','".$data["destroy_date"]."',now()			
				,'".$data["certificate_key"]."','".$data["STATUS"]."');";

            $this->db->query($sql);
            $return_v="y";

        }else if($emode=="edit"){

            $sql="UPDATE  new_coupon_list SET  info_serial = '".$data["info_serial"]."',user_serial = '".$data["user_serial"]."' 
                ,  destroy_date = '".$data["destroy_date"]."',certificate_key = '".$data["certificate_key"]."',STATUS = '".$data["STATUS"]."'  
                  WHERE SERIAL ='".$data["serial"]."' ";

        }else if($emode=="del"){

        }


    }

    /**
     * 쿠폰 사용처리
     *
     * @param $coupon_idx int
     */
    function procCouponUse($coupon_idx)
    {
        if (!empty($coupon_idx))
        {
            $query = "UPDATE new_coupon_list SET status=1 WHERE serial=?";

            $sql = $this->db->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
            $sql->execute([$coupon_idx]);
        }

    }

}