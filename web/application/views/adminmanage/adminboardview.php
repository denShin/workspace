
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>

    <div class="page-header">
        <h2>공지사항</h2>
    </div>


    <fieldset>
        <form id="updateTodo" class="form-horizontal" role="form" method="get" action="/datashare/update_company_notice">
            <div class="form-group">
                <label for="boardTitle" class="col-md-2 control-label">제목</label>
                <div class="col-md-10">
                    <p class="form-control-static">(<?=$data["board_part"]?>) <?=$data["board_title"]?></p>
                </div>
            </div>

            <hr>
            <div class="form-group">
                <label for="boardText" class="col-md-2 control-label">내용</label>
                <div class="col-md-10">
                    <p class="form-control-static"><?=$data["board_content"]?></p>
                </div>
            </div>



            <br/>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">
                        <?if($data["wauth"]=="y"){?>
                            <div class="col-md-4">
                                <button type="button" onclick="location.href='/etcmanage/AdminNotice?ptype=w&board_idx=<?=$data["board_idx"]?>'" class="btn btn-primary btn-block">수정하기</button>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-danger btn-block" id="deleteBtn">삭제하기</button>
                            </div>
                        <?}?>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-success btn-block" style="background-color: #aaa; border-color: #aaa;" onclick="location.href='/etcmanage/AdminNotice'" >목록가기</button>
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">
                        <div class="col-md-12">
                            <style>
                                #adminReply, .adminReply, .adminReplyInsertBtn { margin-top:10px;}
                                .adminReplyRow { padding:7px 0 7px 0;}
                                .gi-2x{font-size: 2em;}
                                .gi-3x{font-size: 3em;}
                                .gi-4x{font-size: 4em;}
                                .gi-5x{font-size: 5em;}
                                .replyContent {overflow:hidden; word-wrap:break-word;}
                            </style>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </fieldset>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <div class="row">
                <div class="col-md-12">
                    <style>
                        #adminReply, .adminReply, .adminReplyInsertBtn { margin-top:10px;}
                        .adminReplyRow { padding:7px 0 7px 0;}
                        .gi-2x{font-size: 2em;}
                        .gi-3x{font-size: 3em;}
                        .gi-4x{font-size: 4em;}
                        .gi-5x{font-size: 5em;}
                        .replyContent {overflow:hidden; word-wrap:break-word;}
                    </style>

                    <div class="adminReply">
                        <form id="adminReplyForm" method="post" action="/etcmanage/AdminCommentProc" onsubmit="return actionSubmit();">
                            <fieldset>
                                <input type="hidden" name="board_code" value="notice">
                                <input type="hidden" name="emode" value="new">
                                <input type="hidden" name="board_idx" value="<?=$data["board_idx"]?>">
                                <input type="hidden" name="per_page" value="<?=$data["per_page"]?>">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label for="adminReply" class="hidden-xs col-sm-2 col-md-1 control-label">
                                            <span class="glyphicon glyphicon-comment gi-3x"></span>
                                        </label>
                                        <div class="col-xs-9 col-sm-8 col-md-10">
                                            <textarea class="form-control" id="adminReply" name="replyText" placeholder="댓글을 입력하세요..."></textarea>
                                        </div>
                                        <div class="col-xs-2 col-sm-2 col-md-1">
                                            <button class="btn btn-warning adminReplyInsertBtn" type="submit">입력</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <div class="adminReplyList">
                <?php
                foreach($data["commentlist"] as $entry) {
                    ?>
                    <div class="adminReplyRow row" id="adminReply29">
                        <div class="hidden-xs col-sm-1 col-md-1"></div>

                        <div class="col-xs-7 col-sm-8 col-md-9 adminReplyText">
                            <div><strong><?=$entry["mem_name"]?></strong> <span><small> (<?=$entry["regdate"]?>)</small></span></div>
                            <div class="replyContent"><?=$entry["commenttext"]?> <span class="glyphicon glyphicon-remove-sign text-danger"  style="cursor: pointer" onclick="removeAdminReply('<?=$entry["comment_idx"]?>')"></span></div>
                        </div>

                    </div>
                <?}?>
            </div>
            </fieldset>

            <script type="text/javascript">
                $(function() {
                    $("#deleteBtn").click(function() {
                        if (!confirm("정말 삭제하시겠습니까?")) {
                            return false;
                        }
                        $(location).attr('href','/etcmanage/AdminBoardProc?emode=del&board_code=<?=$data["board_code"]?>&board_idx=<?=$data["board_idx"]?>&content_code=<?=$data["content_code"]?>');
                    });
                });

                // 폼체크
                function actionSubmit() {
                    if ($('textarea[name=replyText]').val()=="") {
                        $('textarea[name=replyText]').focus();
                        alert('내용을 입력해주세요.');
                        return false;
                    }
                }
                // 댓글 삭제
                function removeAdminReply(comment_idx) {
                    if ( confirm("댓글을 삭제 하시겠습니까?")) {
                        location.href="/etcmanage/AdminCommentProc?comment_idx="+comment_idx+"&emode=del&board_code=<?=$data["board_code"]?>&board_idx=<?=$data["board_idx"]?>&per_page=<?=$data["per_page"]?>";
                    }
                }
            </script></div>
