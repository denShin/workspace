
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <form id="updateConfirm">
        <input type="hidden" name="isConfirm" value="" form="updateConfirm"/>
        <input type="hidden" name="emode" value="agree_yn" form="updateConfirm"/>

        <div class="page-header clearfix">
            <h2 class="pull-left">관리자 관리</h2>
            <div class="pull-right" style="padding-top: 30px">


                <a href="?ptype=w" class="btn btn-info">등록하기</a>
            </div>
        </div>



        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">Check</th>
                    <th class="text-center col-md-2">팀이름</th>
                    <th class="text-center col-md-2">이름</th>
                    <th class="text-center col-md-2">이메일</th>
                    <th class="text-center col-md-2">휴대폰</th>
                    <th class="text-center col-md-1">상태</th>
                    <th class="text-center col-md-2">카카오아이디</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($data["list"] as $entry) {
                    ?>
                    <tr class="text-center row">
                        <td><input type="checkbox" name="confirm_list[]" value="<?=$entry->admin_email?>" form="updateConfirm">
                        </td>
                        <td class="col-md-2"><?= $entry->admin_email ?></td>
                        <td class="col-md-2" style="padding-left: 20px;">
                            <a href="?ptype=w&admin_email=<?= $entry->admin_email ?>"><?= $entry->admin_name ?></a>
                        </td>
                        <td class="col-md-2"><?= $entry->admin_email ?></td>
                        <td class="col-md-2"><?= $entry->admin_tel ?></td>
                        <td class="col-md-1"><?= $entry->admin_stats ?></td>
                        <td class="col-md-2"><?= $entry->kakaoid ?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </form>
    <script type="text/javascript">
        $(function() {
            $("#confirmYes").click(function() {
                $("input[name='isConfirm']").val("1");
                if (!$("input[name='confirm_list[]']").is(":checked")) {
                    alert("메일발송 대상건의 체크박스를 선택하세요.");
                    return false;
                }

                if (!confirm("메일 발송하시겠습니까?")) {
                    return false;
                }
                $("#updateConfirm").attr({action:'member_proc.php', method:'post'}).submit();
            });

        });
    </script>
</div>

<div class="text-center">
    <?=$data['pagination']?>
</div>


</body>
</html>
