<script src="/static/lib/tinymce/tinymce.min.js"></script>

<script>
    tinymce.init({
        selector: 'textarea.number1',
        height: 200,
        width: 800,
        menubar: false,
        plugins: [
            "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons template textcolor paste  textcolor  "
        ],

        toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
        toolbar2: "|  bullist numlist | outdent indent blockquote | undo redo | link unlink  image media code | forecolor backcolor | hr removeformat",


        // without images_upload_url set, Upload tab won't show up
        images_upload_url: '/application/views/data/uploadtinymce.php',

        // we override default upload handler to simulate successful upload
        images_upload_handler: function (blobInfo, success, failure) {
            var xhr, formData;

            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', "/application/views/data/uploadtinymce.php");

            xhr.onload = function() {
                var json;

                if (xhr.status != 200) {
                    failure("HTTP Error: " + xhr.status);
                    return;
                }

                json = JSON.parse(xhr.responseText);

                if (!json || typeof json.location != "string") {
                    failure("Invalid JSON: " + xhr.responseText);
                    return;
                }

                success(json.location);
            };

            formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());
            formData.append('board_code', "<?=$data["board_code"]?>");
            formData.append('content_code', "<?=$data["content_code"]?>");

            xhr.send(formData);
        }
    });

</script>
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">

    <style>
        .double-input .form-control {
            width: 50%;
        }
    </style>

    <div class="page-header">
        <h2>공지사항</h2>
    </div>



    <form id="inputform" class="form-horizontal" role="form" method="post" enctype="multipart/form-data"  action="/etcmanage/AdminBoardProc">
        <input type="hidden" name="emode" value="<?=$data["emode"]?>">
        <input type="hidden" name="board_code" value="<?=$data["board_code"]?>">
        <input type="hidden" name="board_idx" value="<?=$data["board_idx"]?>">
        <input type="hidden" name="content_code" value="<?=$data["content_code"]?>">

        <fieldset>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">제목</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" name="board_title" placeholder="제목을 입력하세요" value="<?=$data["board_title"]?>" autofocus/>
                </div>
            </div>

            <div class="form-group">
                <label for="shareCategory" class="col-md-2 control-label">분류</label>
                <div class="col-md-5">
                    <select name="board_part" class="form-control">
                        <option value="1" <?if($data["board_part"]=="1"){ echo "selected";}?> >일반</option>
                        <option value="2" <?if($data["board_part"]=="2"){ echo "selected";}?> >정보</option>
                        <option value="3" <?if($data["board_part"]=="3"){ echo "selected";}?> >긴급</option>
                        <option value="4" <?if($data["board_part"]=="4"){ echo "selected";}?> >축하</option>
                    </select>
                </div>
            </div>
            <hr>

            <div class="form-group">
                <label for="shareContent" class="col-md-2 control-label">내용</label>
                <div class="col-md-10">
                    <textarea  class="number1" id="elm1" name="board_content"><?=$data["board_content"]?></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary btn-block">저장하기</button>
                        </div>
                        <div class="col-md-6">
                            <button type="reset" class="btn btn-warning btn-block">다시작성</button>
                        </div>
                    </div>

                </div>
            </div>
        </fieldset>
    </form>

    <script type="text/javascript">
        $(function() {
            $("#inputform").submit(function() {

                if ($("input[name='board_title']").val() =="") {
                    alert("제목을 입력하세요.");
                    $("input[name='board_title']").focus();
                    return false;
                }

                tinymce.triggerSave();
                if (tinymce.get('elm1').getContent()=="" ) {
                    alert("내용을 입력하세요.");
                    return false;
                }

                if (!confirm("정말 등록하시겠습니까?")) {

                    return false;
                }
            });

        });


    </script></div>
