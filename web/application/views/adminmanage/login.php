<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Team O2</title>

    <!-- Bootstrap core CSS -->
    <link href="/static/lib/bootstrap/css2/bootstrap.min.css" rel="stylesheet">
    <link href="/static/lib/bootstrap/css/bootstrap-theme.css" rel="stylesheet">
    <link href="/static/lib/bootstrap/css/my-login.css" rel="stylesheet">

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="/static/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
</head>


<body class="my-login-page">
<section class="h-100">
    <div class="container h-100">
        <div class="row justify-content-md-center h-100">
            <div class="card-wrapper">
                <div class="brand">
                    <img src="/static/images/logo.png" width="100">
                </div>
                <div class="card fat">
                    <div class="card-body">
                        <h4 class="card-title">Team O2 관리자 로그인</h4>
                        <form method="POST" role="form" method="post" action="/adminmanage/Login">
                            <input type="hidden" name="emode" value="login">

                            <div class="form-group">
                                <label for="email">E-Mail Address</label>

                                <input id="email" name="admin_id"  type="email" class="form-control" name="email" value="" required autofocus>
                            </div>

                            <div class="form-group">
                                <label for="password">Password

                                </label>
                                <input id="password"name="admin_pwd"  type="password" class="form-control" name="password" required data-eye>
                            </div>


                            <div class="form-group no-margin">
                                <button type="submit"   class="btn btn-primary btn-block">
                                    Login
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="footer">
                    Copyright &copy; Team O2 2018
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>

<script type="text/javascript">
    $(function() {
        $("form[name='loginForm']").submit(function() {
            if ($("#admin_id").val()=="") {
                alert("아이디를 입력하세요.");
                $("#admin_id").focus();
                return false;
            }

            if ($("#admin_pw").val()=="") {
                alert("비밀번호를 입력하세요.");
                $("#admin_pw").focus();
                return false;
            }
        });
    });
</script>