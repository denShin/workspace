
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>

    <div class="page-header clearfix">
        <h2 class="pull-left">공지사항</h2>
            <div class="pull-right" style="padding-top: 30px">
                <a href="?ptype=w" class="btn btn-info">등록하기</a>
            </div>
    </div>
    <div id="searchdiv">
        <form id="searchCompanyNotice" class="form-horizontal" role="form" method="get" >
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <select class="form-control" name="sp">
                                <option value="board_title" >제목</option>
                                <option value="board_content" >내용</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="sv" value="">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </fieldset>
        </form>
        <hr/>
    </div>


    <form id="updateConfirm">



        <div class="clearfix"></div>
        <br/>


        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">No</th>
                    <th class="text-center col-md-1">분류</th>
                    <th class="text-center col-md-7">제목</th>
                    <th class="text-center col-md-1">등록자</th>
                    <th class="text-center col-md-2">등록일</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($data["list"] as $entry) {
                    ?>
                    <tr class="row">
                        <td  class="small text-center"><?=$entry["board_idx"]?> </td>
                        <td   class="small text-center"  ><?=$entry["board_part"]?></td>
                        <td  style="padding-left: 10px;align:left"  class="small">  <a href="?ptype=v&board_idx=<?=$entry["board_idx"]?>&per_page=<?=$data["per_page"]?>"><?=$entry["board_title"]?></a> <?if($entry["reply_cnt"] >0){?>(<b><?=$entry["reply_cnt"]?>)</b><?}?></td>
                        <td   class="small text-center"><?=$entry["mem_name"]?> </td>
                        <td   class="small text-center"><?=$entry["regdate"]?></td>
                    </tr>
                <?}?>

                </tbody>
            </table>
        </div>
    </form>


    <div class="text-center">
        <?=$data['pagination']?>
    </div>
