

<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>

    <div class="page-header clearfix">
        <h2 class="pull-left">팀 리스트</h2>
        <div class="pull-right" style="padding-top: 30px">
            <a href="?ptype=w" class="btn btn-info">등록하기</a>
        </div>
    </div>



    <div class="table-responsive">
        <table class="table table-striped table-hover table-bordered">
            <thead>
            <tr class="info row">
                <th class="text-center col-md-3">팀코드</th>
                <th class="text-center col-md-5">팀명</th>
                <th class="text-center col-md-2">인원</th>
                <th class="text-center col-md-2">수정/삭제</th>
            </tr>
            </thead>
            <tbody>
            <?php

            foreach($data["list"] as $entry) {
                ?>

                <tr class="text-center row">
                    <td class="col-md-3"><?=$entry->team_code?></td>
                    <td class="col-md-5"><?=$entry->team_name?> </td>
                    <td class="col-md-2" style="padding-left: 20px;"><?=$entry->teamtotalcnt?>명 </td>
                    <td class="col-md-2"><button type="button" onclick="location.href='?ptype=w&team_code=<?=$entry->team_code?>&emode=edit'" class="btn btn-success  btn-xs">수정</button>
                        <button type="button" onclick="deleteteam('<?=$entry->team_code?>')" class="btn btn-danger  btn-xs">삭제</button></td>
                </tr>
                <?
            }
            ?>

            </tbody>
        </table>
    </div>

    <script language="JavaScript">

        function deleteteam(team_code)
        {
            if ( confirm("팀을 삭제 하시겠습니까?")) {
                location.href="/adminmanage/TeamManageProc?emode=del&team_code="+team_code ;
            }

        }

    </script>
