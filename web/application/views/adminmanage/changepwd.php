<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <div class="page-header">
        <h2>비밀번호 변경</h2>
    </div>
    <form id="insertCompanyNotice" name="af" class="form-horizontal" role="form" method="post" action="/adminmanage/AdminUserManageProc">
        <input type="hidden" name="emode"  id="emode" value="setnewpwd">
        <fieldset>

            <div class="form-group">
                <label for="admin_pwd" class="col-md-2 control-label">기존 비밀번호</label>
                <div class="col-md-10">
                    <input type="password" class="form-control" name="admin_pwd" placeholder="비밀번호를 입력하세요"   autofocus />
                </div>
            </div>

            <div class="form-group">
                <label for="admin_pwd" class="col-md-2 control-label">신규 비밀번호</label>
                <div class="col-md-10">
                    <input type="password" class="form-control" name="newpwd" placeholder="비밀번호를 입력하세요"  autofocus />
                </div>
            </div>

            <div class="form-group">
                <label for="admin_pwd" class="col-md-2 control-label">비밀번호 확인 </label>
                <div class="col-md-10">
                    <input type="password" class="form-control" name="newpwd1" placeholder="비밀번호를 입력하세요"  autofocus />
                </div>
            </div>



            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary btn-block">저장하기</button>
                        </div>
                        <div class="col-md-4">
                            <?if($data["emode"]=="edit"){?>
                                <button type="button" class="btn btn-warning btn-block" onclick="deletemem('<?=$data["admin_email"]?>')">삭제</button>
                            <?}else{?>
                                <button type="reset" class="btn btn-warning btn-block">다시작성</button>
                            <?}?>
                        </div>

                    </div>
                </div>
            </div>
        </fieldset>
    </form>

    <script type="text/javascript">


        $(function() {


            $("#insertCompanyNotice").submit(function() {


                if ($("input[name='admin_pwd']").val() =="") {
                    alert("기존 비밀번호를 입력하세요.");
                    $("input[name='admin_pwd']").focus();
                    return false;
                }
                if ($("input[name='newpwd']").val() =="") {
                    alert("신규 비밀번호를 입력하세요.");
                    $("input[name='newpwd']").focus();
                    return false;
                }

                if ($("input[name='newpwd1']").val() =="") {
                    alert("신규비밀번호를 입력하세요.");
                    $("input[name='newpwd1']").focus();
                    return false;
                }
                if ($("input[name='newpwd']").val() != $("input[name='newpwd1']").val()) {
                    alert("비밀번호를 확인해주세요.");
                    $("input[name='newpwd']").focus();
                    return false;
                }

                if (!confirm("비밀번호를 변경하시겠습니까?")) {
                    return false;
                }

            });
        });

    </script></div>