
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <div class="page-header">
        <h2>팀 관리</h2>
    </div>


    <form id="insertTeam" class="form-horizontal" role="form" method="post" action="/adminmanage/TeamManageProc">
        <input type="hidden" name="emode" value="<?=$data["emode"]?>">
        <fieldset>
            <div class="form-group">
                <label for="team_code" class="col-md-2 control-label">팀코드</label>
                <div class="col-md-10">
                    <?if($data["team_code"] ==""){?>
                        <input type="text" class="form-control" name="team_code" placeholder="팀코드를 입력하세요" autofocus />
                    <?}else{?>
                        <input type="text" class="form-control" name="team_code" value="<?=$data["team_code"]?>" readonly />
                    <?}?>
                </div>
            </div>

            <div class="form-group">
                <label for="team_name" class="col-md-2 control-label">팀이름</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="team_name" placeholder="팀이름을 입력하세요" value="<?=$data["team_name"]?>" autofocus />
                </div>
            </div>


            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary btn-block">저장하기</button>
                        </div>

                        <div class="col-md-6">
                            <button type="button" class="btn btn-success btn-block" style="background-color: #aaa; border-color: #aaa;" onclick="history.go(-1)();">목록가기</button>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>

    <script type="text/javascript">

        $(function() {
            $("#insertTeam").submit(function() {

                if ($("input[name='team_code']").val() =="") {
                    alert("팀코드를 입력하세요.");
                    $("input[name='team_code']").focus();
                    return false;
                }

                if ($("select[name='team_name']").val() == '') {
                    alert("팀이름을 입력하세요.");
                    $("select[name='team_name']").focus();
                    return false;
                }


                if (!confirm("등록하시겠습니까?")) {
                    return false;
                }

            });
        });


    </script></div>