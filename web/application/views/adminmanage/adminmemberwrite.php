<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <div class="page-header">
        <h2>관리자 관리</h2>
    </div>
    <form id="insertCompanyNotice" name="af" class="form-horizontal" role="form" method="post" action="/adminmanage/AdminUserManageProc">
        <input type="hidden" name="emode"  id="emode" value="<?=$data["emode"]?>">
        <fieldset>

            <?if($data["emode"]=="new"){?>
            <div class="form-group">
                <label for="admin_email" class="col-md-2 control-label">이메일</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="admin_email" placeholder="이메일을 입력하세요" value="<?=$data["admin_email"]?>"
                        <?if($data["emode"]=="edit")echo "readonly"?> autofocus />
                </div>
            </div>

            <div class="form-group">
                <label for="admin_pwd" class="col-md-2 control-label">비밀번호</label>
                <div class="col-md-10">
                    <input type="password" class="form-control" name="admin_pwd" placeholder="비밀번호를 입력하세요" value="<?=$data["admin_pwd"]?>" autofocus />
                </div>
            </div>
            <?}else{?>
                <div class="form-group">
                    <label for="admin_name" class="col-md-2 control-label">이메일</label>
                    <div class="col-md-10">
                        <p class="form-control-static"><?=$data["admin_email"]?></p>
                        <input type="hidden"   name="admin_email" value="<?=$data["admin_email"]?>"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="admin_pwd" class="col-md-2 control-label">비밀번호</label>
                    <div class="col-md-2">
                        <input type="password" class="form-control" name="admin_pwd"    autofocus />
                    </div>
                    <div class="col-md-2">
                        <button type="button" onclick="changepwd()" class="btn btn-danger btn-block">비밀번호 변경</button>
                    </div>
                </div>
            <?}?>

            <div class="form-group">
                <label for="admin_name" class="col-md-2 control-label">이름</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="admin_name" value="<?=$data["admin_name"]?>" placeholder="이름을 입력하세요" autofocus />
                </div>
            </div>
            <div class="form-group">
                <label for="admin_name" class="col-md-2 control-label">카카오아이디</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="kakaoid" value="<?=$data["kakaoid"]?>" placeholder="카카오아이디 입력하세요" autofocus />
                </div>
            </div>
            <div class="form-group">
                <label for="shareCategory" class="col-md-2 control-label">팀선택</label>
                <div class="col-md-10">
                    <select name="team_code" class="form-control">
                        <option value="">-선택하세요-</option>
                        <?=$data["teamoption"]?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="shareCategory" class="col-md-2 control-label">직급</label>
                <div class="col-md-10">
                    <select name="member_grade" class="form-control">
                        <option value="">-선택하세요-</option>
                        <option value="0" <?if($data["member_grade"]=="0"){echo "selected";}?> >아르바이트</option>
                        <option value="1" <?if($data["member_grade"]=="1"){echo "selected";}?> >사원</option>
                        <option value="2" <?if($data["member_grade"]=="2"){echo "selected";}?> >대리</option>
                        <option value="3" <?if($data["member_grade"]=="3"){echo "selected";}?> >과장</option>
                        <option value="4" <?if($data["member_grade"]=="4"){echo "selected";}?> >차장</option>
                        <option value="5" <?if($data["member_grade"]=="5"){echo "selected";}?> >임원</option>

                    </select>
                </div>
            </div>


            <div class="form-group">
                <label for="admin_tel" class="col-md-2 control-label">휴대폰</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="admin_tel" placeholder="휴대폰을 입력하세요" value="<?=$data["admin_tel"]?>" autofocus />
                </div>
            </div>
            <hr>

            <div class="form-group">
                <label for="shareCategory" class="col-md-2 control-label">상태</label>
                <div class="col-md-10">
                    <select name="admin_stats" class="form-control">
                        <option value="y" <?if($data["admin_stats"]=="y"){echo "selected";}?>>정상</option>
                        <option value="n" <?if($data["admin_stats"]=="n"){echo "selected";}?>>차단</option>
                    </select>
                </div>
            </div>

            <br><hr><br> <h3>권한 관리</h3>

 


            <div class="form-group">
                <label for="config_yn" class="col-md-2 control-label">포인트쿠폰 권한</label>
                <div class="col-md-10">

                    <select name="pointauth_yn" class="form-control">
                        <option value="y" <?if($data["pointauth_yn"]=="y"){echo "selected";}?>>예</option>
                        <option value="n" <?if($data["pointauth_yn"]=="n" || $data["pointauth_yn"]==""){echo "selected";}?>>아니오</option>
                    </select>
                </div>
            </div>


            <div class="form-group">
                <label for="config_yn" class="col-md-2 control-label">매출관리</label>
                <div class="col-md-10">

                    <select name="calc_yn" class="form-control">
                        <option value="y" <?if($data["calc_yn"]=="y"){echo "selected";}?>>예</option>
                        <option value="n" <?if($data["calc_yn"]=="n" || $data["calc_yn"]==""){echo "selected";}?>>아니오</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="config_yn" class="col-md-2 control-label">주문관리</label>
                <div class="col-md-10">

                    <select name="ordermng_yn" class="form-control">
                        <option value="y" <?if($data["ordermng_yn"]=="y"){echo "selected";}?>>예</option>
                        <option value="n" <?if($data["ordermng_yn"]=="n" || $data["ordermng_yn"]==""){echo "selected";}?>>아니오</option>
                    </select>
                </div>
            </div>


            <div class="form-group">
                <label for="config_yn" class="col-md-2 control-label">사이트 마스터</label>
                <div class="col-md-10">

                    <select name="master_yn" class="form-control">
                        <option value="y" <?if($data["dmaster_yn"]=="y"){echo "selected";}?>>예</option>
                        <option value="n" <?if($data["dmaster_yn"]=="n" || $data["dmaster_yn"]==""){echo "selected";}?>>아니오</option>
                    </select>
                </div>
            </div>


            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary btn-block">저장하기</button>
                        </div>
                        <div class="col-md-4">
                            <?if($data["emode"]=="edit"){?>
                                <button type="button" class="btn btn-warning btn-block" onclick="deletemem('<?=$data["admin_email"]?>')">삭제</button>
                            <?}else{?>
                                <button type="reset" class="btn btn-warning btn-block">다시작성</button>
                            <?}?>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-success btn-block" style="background-color: #aaa; border-color: #aaa;" onclick="location.href='/adminmanage/AdminUserManage'">목록가기</button>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>

    <script type="text/javascript">

        function changepwd(){
            if ($("input[name='admin_pwd']").val() =="") {
                alert("비밀번호를 입력하세요.");
                $("input[name='admin_pwd']").focus();
                return false;
            }

            if (!confirm("비밀번호를 변경하시겠습니까?")) {
                return false;
            }else{
                $("#emode").val("password");
                document.af.submit();
            }
        }


        function deletemem(admin_email)
        {
            if ( confirm("멤버를 삭제 하시겠습니까?")) {
                location.href="/adminmanage/AdminUserManageProc?emode=del&admin_email="+admin_email ;
            }
        }


        $(function() {


            $("#insertCompanyNotice").submit(function() {

                <?if($data["emode"]=="new"){?>
                if ($("input[name='admin_email']").val() =="") {
                    alert("이메일을 입력하세요.");
                    $("input[name='admin_email']").focus();
                    return false;
                }
                if ($("input[name='admin_pwd']").val() =="") {
                    alert("비밀번호를 입력하세요.");
                    $("input[name='admin_pwd']").focus();
                    return false;
                }
                <?}?>

                if ($("input[name='admin_name']").val() =="") {
                    alert("이름을 입력하세요.");
                    $("input[name='admin_name']").focus();
                    return false;
                }

                if (!confirm("등록하시겠습니까?")) {
                    return false;
                }

            });
        });

    </script></div>