<div class="container-fluid" style="padding-top: 50px;padding-bottom: 70px;">

    <div class="page-header clearfix">
        <h3 class="pull-left"><?=$data["pagetitle"]?></h3>
        <?if($data["teltype"]=="gold"){?>
            <div class="pull-right"  >
                <a   class="btn btn-primary" onclick="shownewtelmodal()">골드넘버등록</a>
            </div>
        <?}?>
    </div>
    <div id="searchdiv" >
        <form id="searchform" class="form-horizontal" role="form" method="get" >
            <input type="hidden" name="teltype" value="<?=$data["teltype"]?>">
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <select class="form-control" name="sp">
                                <option value="company_name" >회사명</option>
                                <option value="tel050_number" >가상번호</option>
                                <option value="tel050_matnumber" >착신번호</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="sv" value="">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </fieldset>
        </form>

    </div>
    <div class="clearfix"></div>


    <br>
    <div class="clearfix" style="padding-top: 20px"></div>
    <form name="updateConfirm" method="post">
        <input type="hidden" name="emode" value="changestats">
        <input type="hidden" name="stats" >

        <div class="table-responsive">
            <table class="table   table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-2">회사명</th>

                    <th class="text-center col-md-1">분류</th>
                    <th class="text-center col-md-1">전화번호</th>
                    <th class="text-center col-md-2">050번호</th>
                    <th class="text-center col-md-2">지역</th>
                    <th class="text-center col-md-1">해지일</th>
                    <th class="text-center col-md-1">삭제예정</th>
                    <th class="text-center col-md-2">관리</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($data["list"] as $entry) {

                    ?>
                    <tr class="text-center row">
                        <td class="small text-center"   ><?= $entry["business_name"] ?> </td>
                        <td class="small text-center"><?= $entry["companytype"] ?></td>
                        <td class="small text-center" > <?= $entry["tel"] ?> </td>
                        <td class="small text-center" >
                            <?=$entry["tel050_number"]?>
                        </td>
                        <td class="small text-left"   ><?= $entry["service_city"] ?>   <?= $entry["service_location"] ?> </td>
                        <td class="small text-center" > <?= $entry["tel050_deldate"] ?> </td>
                        <td class="small text-center" > <?= $entry["tel050_expiredate"] ?> </td>
                        <td class="small text-center" >

                            <button type="button" class="btn btn-danger btn-sm " onclick="delnewtel('<?= $entry["serial"] ?>','<?= $entry["tel050_number"] ?>','<?= $entry["tel050_matnumber"] ?>')"  >삭제</button>
                            <button type="button" class="btn btn-success btn-sm" onclick="restarttel('<?= $entry["serial"] ?>','<?= $entry["tel050_number"] ?>','<?= $entry["tel050_matnumber"] ?>')" >활성화</button>
                        </td>

                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="text-center">
            <?=$data['pagination']?>
        </div>
    </form>




</div>


<div id="goldModal" class="modal fade bs-modified-modal-sm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-danger ">골드넘버 추가하기</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2">
                        <p class="form-control-static text-right">050번호</p>
                    </div>
                    <div class="col-md-2">
                        <p class="form-control-static text-right">05037961</p>
                    </div>
                    <div class="col-md-5">
                        <input type="number" id="sendnewtelnum"  class="form-control"  maxlength="4" oninput="maxLengthCheck(this)" onpress="isNumber(event)" value="">
                    </div>
                    <div class="col-md-1"></div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"  onclick="sendnewtel()"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="pconfirmBtn">번호 추가</span></button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
            </div>
        </div>
    </div>
</div>




</body>
</html>


<script language="JavaScript">

    var branch_serial="";
    var branch_tel="";

    function maxLengthCheck(object){
        if (object.value.length > object.maxLength){
            object.value = object.value.slice(0, object.maxLength);
        }
    }

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        let charCode = (evt.which) ? evt.which : evt.keyCode;
        if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
            evt.preventDefault();
        } else {
            return true;
        }
    }

    function shownewtelmodal(){

        $("#goldModal").modal().on("hidden.bs.modal", function() {

        });
    }

    function delnewtel(bserial,deltel,branch_tel){


        if(confirm("050 번호를  삭제 하시겠습니까?")){
            $.ajax({
                type: "get",
                url: "/partners/PartnerMemberProc?emode=sendelnewtel&datamode=comdel&bserial="+bserial
                    +"&deltel="+deltel+"&branch_tel="+branch_tel,

                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log("response:%o",response);
                     branch_serial="";
                   location.reload();
                }
            });
        }
    }


    function restarttel(bserial,retel,branch_tel){


        if(confirm("050 번호를 다시 활성화 하시겠습니까?")){
            $.ajax({
                type: "get",
                url: "/partners/PartnerMemberProc?emode=sendrestarttel&datamode=restart&bserial="+bserial
                    +"&retel="+retel+"&branch_tel="+branch_tel,

                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log("response:%o",response);
                    branch_serial="";
                    location.reload();
                }
            });
        }
    }

    function sendnewtel(){

        // http://workspace.teamo2.kr/partners/PartnerMemberProc?emode=sendnewtel&datamode=new&bserial=568&sendnewtelnum=1111&branch_tel=
        var sendnewtelnum= $("#sendnewtelnum").val();

        if(sendnewtelnum===""){
            alert('골드번호를 기입 등록해주세요');

            return;
        }

        if(confirm("050 골드넘버를 등록 하시겠습니까?")){
            $.ajax({
                type: "get",
                url: "/partners/PartnerMemberProc?emode=sendnewgoldtel&datamode=new&sendnewtelnum="+sendnewtelnum  ,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log("response:"+response);
                    var json = JSON.parse(response);

                    var result = json["result"];
                    if(result==="y"){


                    }else if(result ==="n"){
                        alert('이미 등록된 골드번호 입니다. 다른번호를 기입해주세요');
                        return;
                    }

                }
            });
        }
    }

    function gonumpage(teltype){
        location.href="/partners/Tel050manage?teltype="+teltype;
    }
</script>