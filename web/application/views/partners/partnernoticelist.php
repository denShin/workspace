
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>

    <div class="page-header clearfix">
        <h2 class="pull-left">파트너스 공지사항</h2>
        <div class="pull-right" style="padding-top: 30px">
            <a href="?ptype=w" class="btn btn-info">등록하기</a>
        </div>
    </div>
    <div id="searchdiv">
        <form id="searchCompanyNotice" class="form-horizontal" role="form" method="get" >
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <select class="form-control" name="sp">
                                <option value="pnb_title" >제목</option>
                                <option value="pnb_content" >내용</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="sv" value="">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </fieldset>
        </form>
        <hr/>
    </div>


    <form id="updateConfirm">



        <div class="clearfix"></div>


        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">No</th>
                    <th class="text-center col-md-7">제목</th>
                    <th class="text-center col-md-1">조회</th>
                    <th class="text-center col-md-1">상태</th>
                    <th class="text-center col-md-2">등록일</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($data["list"] as $entry) {
                    ?>
                    <tr class="row">
                        <td  class="small text-center"><?=$entry["pnb_idx"]?> </td>
                        <td  style="padding-left: 10px;align:left"  class="small">  <a href="?ptype=v&pnb_idx=<?=$entry["pnb_idx"]?>&per_page=<?=$data["per_page"]?>"><?=$entry["pnb_title"]?></a>  </td>
                        <td   class="small text-center"><?=$entry["pnb_view"]?> </td>
                        <td   class="small text-center"><?=$entry["pnb_statestr"]?> </td>
                        <td   class="small text-center"><?=$entry["pnb_regdate"]?></td>
                    </tr>
                <?}?>

                </tbody>
            </table>
        </div>
    </form>


    <div class="text-center">
        <?=$data['pagination']?>
    </div>
