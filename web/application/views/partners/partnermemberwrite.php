
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">

    <style>
        .double-input .form-control {
            width: 50%;
        }
    </style>

    <div class="page-header">
        <h2>쿠폰 등록 관리</h2>
    </div>


    <form id="inputform" class="form-horizontal" role="form" method="post" enctype="multipart/form-data"  action="/carmore/CouponManageProc">
        <input type="hidden" name="emode" value="<?=$data["emode"]?>">

        <fieldset>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">쿠폰 제목</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" name="coupontitle" placeholder="쿠폰 제목""  value="<?=$data["coupontitle"]?>"   autofocus/>
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">기간</label>
                <div class="col-md-3">
                    <input type="date" class="form-control" name="start_date" placeholder="시작일""  value="<?=$data["start_date"]?>"   autofocus/>
                </div>
                <div class="col-md-3">
                    <input type="date" class="form-control" name="end_date" placeholder="마감일""  value="<?=$data["end_date"]?>"   autofocus/>
                </div>
            </div>
            <div class="form-group">
                <label for="shareCategory" class="col-md-2 control-label">쿠폰 종류</label>
                <div class="col-md-5">
                    <select name="TYPE" class="form-control">
                        <option value="1" <?if($data["TYPE"]=="1"){ echo "selected";}?> >개별 쿠폰</option>
                        <option value="2" <?if($data["TYPE"]=="2"){ echo "selected";}?> >이벤트 쿠폰</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">할인 방법</label>
                <div class="col-md-2">
                    <input type="text" class="form-control" name="store_ver" placeholder="할인 방법"  value="<?=$data["store_ver"]?>"   autofocus/>
                </div>
                <div class="col-md-2">
                    <select name="value_type" class="form-control">
                        <option value="1" <?if($data["value_type"]=="1"){ echo "selected";}?> >원</option>
                        <option value="2" <?if($data["value_type"]=="2"){ echo "selected";}?> >%</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">사용가능금액</label>
                <div class="col-md-2">
                    <input type="text" class="form-control" name="tvalue" placeholder="사용가능금액"  value="<?=$data["value"]?>"   autofocus/>
                </div>
                <div class="col-md-5">
                    ( 0원은 전체 사용가능)
                </div>
            </div>

            <hr>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">쿠폰코드</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" name="certificate_key" placeholder="쿠폰코드""  value="<?=$data["certificate_key"]?>"   autofocus/>
                </div>
            </div>
            <div class="form-group">
                <label for="shareContent" class="col-md-2 control-label">쿠폰 설명</label>
                <div class="col-md-10">
                    <textarea   class="form-control"  name="description"  style="width: 60%;height: 80px"  id="description"><?=$data["description"]?></textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary btn-block">저장하기</button>
                        </div>
                        <div class="col-md-6">
                            <button type="reset" class="btn btn-warning btn-block">다시작성</button>
                        </div>
                    </div>

                </div>
            </div>
        </fieldset>
    </form>

    <script type="text/javascript">
        $(function() {
            $("#inputform").submit(function() {

                if ($("input[name='coupontitle']").val() =="") {
                    alert("쿠폰 제목을 입력하세요.");
                    $("input[name='coupontitle']").focus();
                    return false;
                }

                if ($("input[name='start_date']").val() =="") {
                    alert("쿠폰 시작일을 입력하세요.");
                    $("input[name='start_date']").focus();
                    return false;
                }

                if ($("input[name='end_date']").val() =="") {
                    alert("쿠폰 마감일을 입력하세요.");
                    $("input[name='end_date']").focus();
                    return false;
                }

                if ($("input[name='value']").val() =="") {
                    alert("쿠폰 사용가능금액을 입력하세요.");
                    $("input[name='coupontitle']").focus();
                    return false;
                }

                if ($("input[name='certificate_key']").val() =="") {
                    alert("쿠폰 코드를 입력하세요.");
                    $("input[name='certificate_key']").focus();
                    return false;
                }

                if (!confirm("정말 등록하시겠습니까?")) {

                    return false;
                }
            });

        });


    </script></div>
