
<script src="<?=asset_url()?>/js/image-compressor.min.js"></script>
<script src="<?=asset_url()?>/js/commonCompressImg.min.js"></script>
<script src="/static/lib/dropzone/dropzone.js"></script>
<link href="/static/lib/dropzone/dropzone.css"rel="stylesheet">
<div class="container" style="padding-top: 0px;padding-bottom: 70px;">

    <style>
        .double-input .form-control {
            width: 50%;
        }
    </style>




    <fieldset>

        <div class="col-md-10">
            <label for="shareTitle" class="col-md-2 control-label"><?=$data["pagetitle"]?></label>
            <div class="col-md-7">
                <div class="col-md-11"><img  src="<?=$data["mainimage"]?>"  id="mainimage" width="300"    alt=""></div>

            </div>
        </div>
        <div class="clearfix" style="padding-top: 40px"></div>
        <div class="col-md-10">
            <label for="shareTitle" class="col-md-2 control-label"> </label>
            <div class="col-md-7">

                <div class="col-md-5"><span id="publish_yn"><?=$data["publish_ynstr"]?></span></div>

            </div>
        </div>
        <div class="col-md-10">
            <label for="shareTitle" class="col-md-2 control-label"> </label>
            <div class="col-md-7">

                <div class="col-md-5"> <button class="btn btn-danger btn-sm mainimageshow" onclick="delimage('mainimage')">삭제하기</button></div>
                <div class="col-md-5"> <button class="btn btn-primary btn-sm mainimageshow"  onclick="publishmainimage('mainimage')">배포하기</button></div>
            </div>
        </div>
        <div class="clearfix" style="padding-top: 40px"></div>
        <form id="inputform" class="form-horizontal" role="form" method="post" enctype="multipart/form-data" >
            <br>
            <div class="col-md-8">
                <label class="col-md-2 control-label">업로드</label>
                <div class="col-md-8">
                    <div class="dropzone dz-clickable" id="myDrop">
                        <div class="dz-default dz-message" data-dz-message="">
                            <span>여기에 드래그앤드랍하세요</span>
                        </div>
                    </div>
                </div>
            </div>

            <br/>

            <div id="fileOrgName"></div>
            <div id="fileSaveName"></div>
            <div id="fileDir"></div>
            <div id="fileSize"></div>
            <div id="fileType"></div>
        </form>

    </fieldset>

    <script type="text/javascript">


        function setimage(targetid,filename){
            $("#"+targetid).attr('src','https://s3.ap-northeast-2.amazonaws.com/carmoreweb/partners/<?=$data["content_code"]?>/'+filename);

        }

        function publishmainimage(targetid) {

            var url= $("#"+targetid).attr('src');
            var filename = url.split('/').pop().split('#')[0].split('?')[0];
            if(filename==="noimage.png"){
                return ;
            }

            if (confirm("이미지를 배포 하시겠습니까?")) {



                //  http://workspace2.teamo2.kr//carmore/CarinfomasterProc?emode=imagepublish&publish_yn=y&filename=20180927053129_716_CARMST_603.png
                console.log("/partners/CarinfomasterProc?emode=imagepublish&publish_yn=y&filename="+filename)
                $.ajax({
                    type: "get",
                    url: "/partners/PartnerinfoimageProc?emode=imagepublish&publish_yn=y&filename="+filename,
                    success: function (response) {
                        console.log("response:"+response);
                        $("#publish_yn").text('배포완료');
                    },
                    error:function(request,status,error){
                        console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
                    },
                    complete:  function() {
                        console.log("complete");

                    }

                });
            }
        }

        function delimage(targetid){
            var url= $("#"+targetid).attr('src');

            var filename = url.split('/').pop().split('#')[0].split('?')[0];


            if(filename==="noimage.png"){
                return ;
            }

            if(confirm("*선택한 이미지를 삭제하시겠습니까?\n한번 삭제하시면 복구할 수 없습니다.")){


                $.ajax({
                    type: "get",
                    url: "/application/views/data/deletedropzone.php?board_code=partners&content_code=<?=$data["content_code"]?>&deltype=modal&fileSaveName="+filename,
                    success: function (response) {
                        console.log("response:"+response);
                        $("#"+targetid).attr('src','https://s3.ap-northeast-2.amazonaws.com/carmoreweb/partners/noimage.png');
                        $("#publish_yn").text('');
                    },
                    error:function(request,status,error){
                        console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
                    },
                    complete:  function() {
                        console.log("complete");

                    }

                });
            }



        }


        var boolean = true;

        var myDropZone;
        $(document).ready(function() {
            Dropzone.autoDiscover = false;
            myDropZone = new Dropzone("#myDrop", {
                url : "/application/views/data/uploaddropzone.php",
                addRemoveLinks : true,
                maxFilesize: 100,
                maxFiles:2,
                acceptedFiles: 'image/*',
                autoProcessQueue : false,
                paramName: "file1234",
                parallelUploads : 100,
                init: function() {

                    this.on("addedfile", function(file) {
                        var onThis = this;

                        if (boolean) {
                            commonCompressImage(file, 1280, 200, function(_callbackFile) {
                                onThis.removeFile(file);
                                boolean = false;

                                onThis.addFile(_callbackFile);
                                sendCompressImg();
                            })
                        } else
                            boolean = true;
                    });
                },
                sending:function(file, xhr, formData){
                    formData.append('board_code', 'partners');
                    formData.append('fileuptype', '<?=$data["fileuptype"]?>');
                    formData.append('content_code', '<?=$data["content_code"]?>');
                    formData.append('mem_id', '');
                },
                success: function( file, response ) {

                    console.log("response:%o",response);
                    obj = JSON.parse(response);

                    file.fileDir = obj.fileDir;
                    file.fileSaveName = obj.fileSaveName;
                    file.fileidx = obj.uploadfile_infoidx;

                    var input1 = $("<input>");
                    input1.attr({"type" : "hidden", "name" : "fileOrgName[]", "value" : obj.fileOrgName});
                    var input2 = $("<input>");
                    input2.attr({"type" : "hidden", "name" : "fileSaveName[]", "value" : obj.fileSaveName});
                    var input4 = $("<input>");
                    input4.attr({"type" : "hidden", "name" : "fileSize[]", "value" : obj.fileSize});
                    var input5 = $("<input>");
                    input5.attr({"type" : "hidden", "name" : "fileType[]", "value" : obj.fileType});

                    $("#fileOrgName").append(input1);
                    $("#fileSaveName").append(input2);
                    $("#fileSize").append(input4);
                    $("#fileType").append(input5);


                    setimage('mainimage',obj.fileSaveName);
                    $("#publish_yn").html("<span style='color:red'>배포대기</span>");
                    var _ref;
                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;

                },

                removedfile: function(file) {

                    if (file.fileSaveName) {
                        $.post("/application/views/data/deletedropzone.php?board_code=partners&content_code=<?=$data["content_code"]?>&fileSaveName="+file.fileSaveName);
                        $("." + file.fileSaveName.replace('.', '')).remove();
                    }
                    var _ref;
                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                }
            });

        });

        function sendCompressImg() {
            myDropZone.processQueue();
        }






    </script></div>

</body>
</html>
