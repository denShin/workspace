<div class="container-fluid" style="padding-top: 50px;padding-bottom: 70px;">

    <div class="page-header clearfix">
        <h3 class="pull-left">전화걸기 업체관리</h3>
        <div class="pull-right"  >
            <a href="?ptype=w&board_code=<?=$data["board_code"]?>" class="btn btn-primary">등록하기</a>
        </div>
    </div>
    <div id="searchdiv" >
        <form id="searchform" class="form-horizontal" role="form" method="get" >
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <select class="form-control" name="sp">
                                <option value="business_name" >회사명</option>
                                <option value="tel050_number" >가상번호</option>
                                <option value="tel" >착신번호</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="sv" value="">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </fieldset>
        </form>

    </div>
    <div class="clearfix"></div>

    <br>
    <div class="clearfix" style="padding-top: 10px"></div>
    <form name="updateConfirm" method="post">
        <input type="hidden" name="emode" value="changestats">
        <input type="hidden" name="stats" >

        <div class="table-responsive">
            <table class="table   table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">Check</th>
                    <th class="text-center col-md-2">지역</th>
                    <th class="text-center col-md-2">회사명</th>
                    <th class="text-center col-md-1">영업시간</th>
                    <th class="text-center col-md-1">착신번호</th>
                    <th class="text-center col-md-2">050번호</th>
                    <th class="text-center col-md-2">주소</th>
                    <th class="text-center col-md-1">상태</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($data["list"] as $entry) {


                    ?>
                    <tr class="text-center row">
                        <td><?= $entry["serial"] ?></td>
                        <td class="small text-left"   ><?= $entry["service_city"] ?>   <?= $entry["service_location"] ?> </td>
                        <td class="small text-center"   >
                            <a href="?ptype=w&serial=<?=$entry["serial"]?>&per_page=<?=$data["per_page"]?>">
                                <?= $entry["business_name"] ?> </a></td>
                        <td class="small text-center" > <?= $entry["service_time"] ?> </td>
                        <td class="small text-center" > <?= $entry["tel"] ?> </td>
                        <td class="small text-center" >
                            <?if($entry["tel050_number"]==""){?>
                                <a href="#" onclick="showtelmodal('<?= $entry["serial"] ?>','<?= $entry["tel"] ?>')" class="btn btn-info btn-sm">등록하기</a>
                            <?}else{?>
                                <?=$entry["tel050_number"]?>  <a href="#" onclick="delnewtel('<?= $entry["serial"] ?>','<?= $entry["tel050_number"] ?>','<?= $entry["tel"] ?>')" class="btn btn-danger">삭제</a>
                            <?}?>

                        </td>
                        <td class="small text-center"><?= $entry["service_address"] ?></td>
                        <td class="small text-center"><?= $entry["statestr"] ?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="text-center">
            <?=$data['pagination']?>
        </div>
    </form>


    <div  id="viewmodal" class="modal fade  " style="z-index: 3200">

        <div class="modal-dialog  modal-lg ">

            <div class="modal-content "  style="background-color: white;">

                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger viewtitle">지점직원 관리 </h4>

                </div>
                <div class="modal-body" style="background-color: white;">
                </div>

            </div>
        </div>
    </div>

    <div id="telModal" class="modal fade bs-modified-modal-sm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger ">050 번호 추가(5000~9999)</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2">
                            <p class="form-control-static text-right">050번호</p>
                        </div>
                        <div class="col-md-3">
                            <p class="form-control-static text-right">0503-7961-</p>
                        </div>
                        <div class="col-md-4">
                            <input type="number" id="sendnewtelnum"  class="form-control"  maxlength="4" oninput="maxLengthCheck(this)" onpress="isNumber(event)" value="<?=$data["new050"]?>">
                        </div>
                        <div class="col-md-1"></div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"  onclick="sendnewtel()"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="pconfirmBtn">번호 추가</span></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">



        function viewcontentmodal(branchserial){
            var url="";
            url ="/partners/PartnerMemberBranch?ptype=branchdtllist&branchserial="+branchserial ;

            $(".viewtitle").text("지점 직원 관리");

            $.ajax({
                url: url,
                type: 'GET',
                cache: false,
            }).done(function(result){

                $('#viewmodal .modal-body').html(result)
                $('#viewmodal').modal('show')
            });
        }

        function closeviewmodal(){
            $("#viewmodal").modal("hide");
        }


        function setinitpwd(mem_id){

            var confirmtxt ="선택된 회원의 비밀번호를 초기화 하시겠습니까?";

            if(confirm(confirmtxt)){
                location.href="?ptype=initpwd&mem_id="+mem_id;
            }
        }


    </script>
</div>




</body>
</html>

<script language="JavaScript">

    var branch_serial="";
    var branch_tel="";

    function maxLengthCheck(object){
        if (object.value.length > object.maxLength){
            object.value = object.value.slice(0, object.maxLength);
        }
    }

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        let charCode = (evt.which) ? evt.which : evt.keyCode;
        if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
            evt.preventDefault();
        } else {
            return true;
        }
    }

    function showtelmodal(bserial ,btel){
        branch_serial =bserial;
        branch_tel =btel;

        if(btel===""){
            alert('변환 할수 있는 번호정보가 없습니다');
            return ;
        }
        $("#telModal").modal().on("hidden.bs.modal", function() {

        });
    }

    function delnewtel(bserial,deltel,branch_tel){

        console.log( "/partners/PartnerMemberProc?emode=sendelnewtel&datamode=del&bserial="+bserial
            +"&deltel="+deltel+"&branch_tel="+branch_tel);
        if(confirm("050 번호를  삭제 하시겠습니까?")){
            $.ajax({
                type: "get",
                url: "/partners/PartnerMemberProc?emode=sendelnewtel&datamode=del&bserial="+bserial
                    +"&deltel="+deltel+"&branch_tel="+branch_tel,

                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    branch_serial="";
                  location.reload();
                }
            });
        }
    }

    function sendnewtel(){

        // http://workspace.teamo2.kr/partners/PartnerMemberProc?emode=sendnewtel&datamode=new&bserial=568&sendnewtelnum=1111&branch_tel=
        var sendnewtelnum= $("#sendnewtelnum").val();

        if(sendnewtelnum===""){
            alert('번호를 기입 등록해주세요');

            return;
        }
        var intsendnewtelnum =parseInt(sendnewtelnum);
        if(intsendnewtelnum <5000 || intsendnewtelnum > 9999){

            alert('5000 번에서 9999 번 사이의 번호만 기입할 수 있습니다.');

            return;
        }

        var tmpstr ="fk_serial :"+branch_serial+", branch_tel:"+branch_tel+", sendnewtelnum:"+sendnewtelnum;

        if(confirm("050 번호를 등록 하시겠습니까?")){
            $.ajax({
                type: "get",
                url: "/partners/PartnerMemberProc?emode=sendnewtel&datamode=new&tel050_part=out&fk_serial="+branch_serial
                    +"&sendnewtelnum="+sendnewtelnum+"&branch_tel="+branch_tel  ,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log("response:"+response);
                    var json = JSON.parse(response);

                    var result = json["result"];
                    if(result==="y"){
                        branch_serial="";
                        location.reload();
                    }else if(result ==="tel_exists"){
                        alert('이미 등록된 번호 입니다. 다른번호를 기입해주세요');
                        return;
                    }

                }
            });
        }
    }


    function setstorememstats(upart,stats){

        var objcomserial =  $('[name="comserial"]');
        var objlength =objcomserial.length;
        var saveparam=new Array();
        var cnt=0;



        for(var i=0;i < objlength ;i++){
            var obj =objcomserial[i];
            if(obj.checked){
                var comserial =objcomserial[i]["value"];

                if(comserial !=="" ){
                    saveparam.push(comserial);
                    cnt++;
                }
            }

        }

        if(cnt===0){
            alert('선택된 정보가 없습니다.');
            return;
        }
        saveparam = saveparam.join(",");

        var confirmtxt ="";
        if(upart==="month"){
            if(stats=="y"){
                confirmtxt ="선택된 회원을 월렌트 승인하시겠습니까?";
            }else{
                confirmtxt ="선택된 회원을 월렌트 미승인하시겠습니까?";
            }
        }


        if(confirm(confirmtxt)){
            $.ajax({
                type:"get",contentType: "application/json",
                url:"/partners/PartnerMemberProc?emode=setallstats&upart="+upart+"&stats="+stats+"&saveparam="+saveparam,
                datatype: "json",
                success: function(data) {
                    alert("수정이 완료되었습니다.");
                    location.reload();

                },
                error: function(x, o, e) {

                }
            });
        }


    }

</script>