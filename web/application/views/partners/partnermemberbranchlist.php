<?php
$recommend_050num = $data["new050"];

?>

<div class="container workspace-basic-container">
    <div class="page-header clearfix">
        <h3 class="pull-left"><?=$data["listitle"]?></h3>

    </div>

    <!--검색 파트-->
    <div>
        <form class="form-horizontal" role="form" method="get">
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-2">
                            <select class="form-control workspace-select" name="sp">
                                <option value="company_name" >회사명</option>
                                <option value="c.name" >대표자</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="sv" value="" />
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>

    </div>

    <div class="clearfix"></div>

    <div class="pull-right" style="margin-bottom: .2em">
        <button type="button" class="btn btn-primary btn-xs" onclick="setstorememstats('month','y')">월렌트승인</button>
        <button type="button" class="btn btn-danger btn-xs" onclick="setstorememstats('month','n')">월렌트미승인</button>
        <button type="button" class="btn btn-primary btn-xs" onclick="setstorememstats('normal','y')">단기렌트승인</button>
        <button type="button" class="btn btn-danger btn-xs" onclick="setstorememstats('normal','n')">단기렌트미승인</button>

    </div>

    <div class="clearfix" style="padding-top: 10px"></div>

    <form name="updateConfirm" method="post">
        <input type="hidden" name="emode" value="changestats" />
        <input type="hidden" name="stats" />

        <div>
            <table class="table table-bordered">
                <colgroup>
                    <col style="width: 1%"/>    <!---->
                    <col style="width: 12%"/>   <!--회사/지점-->
                    <col style="width: 3%"/>    <!--월-->
                    <col style="width: 3%"/>    <!--단기-->
                    <col style="width: 6%"/>   <!--대표자-->
                    <col style="width: 14%"/>   <!--한마디-->
                    <col style="width: 15%"/>   <!--업체특이사항-->
                    <col style="width: 8%"/>   <!--대표사진-->
                    <col style="width: 7%"/>    <!--업체사진-->
                    <col style="width: 6%"/>   <!--휴대폰-->
                    <col style="width: 5%"/>    <!--050-->
                    <col style="width: 7%"/>    <!--로그인/초기화-->
                    <col style="width: 5%"/>    <!--가입일-->
                    <col style="width: 9%"/>
                </colgroup>
                <thead>
                    <tr class="info">
                        <th class="text-center"></th>
                        <th class="text-center">회사/지점명<br>(계정아이디)</th>
                        <th class="text-center">월<br>렌트</th>
                        <th class="text-center">단기<br>렌트</th>
                        <th class="text-center">대표자</th>
                        <th class="text-center">한마디</th>
                        <th class="text-center">업체<br>특이사항</th>
                        <th class="text-center">대표사진</th>
                        <th class="text-center">업체사진</th>
                        <th class="text-center">휴대폰</th>
                        <th class="text-center">050번호</th>
                        <th class="text-center">로그인/초기화</th>
                        <th class="text-center">가입일</th>
                        <th class="text-center">입점일</th>

                    </tr>
                </thead>
                <tbody>
<?php
foreach ($data['list'] as $entry)
{
    $branch_idx    = $entry['serial'];
    $branch_tel    = $entry['tel'];
    $branch_050tel = (string)$entry['tel050'];
    $origin_050tel = (isset($entry['origintel050']))? $entry['origintel050'] : '';

?>
                    <tr class="text-center">
                        <td style="vertical-align: middle"><input type="checkbox" name="comserial" value="<?=$branch_idx?>" /></td>
                        <td class="small text-left" style="vertical-align: middle"><?= $entry["company_name"] ?> > <?= $entry["branchName"] ?><br>(<?=$entry['mem_id']?>)</td>
                        <td class="small text-center" style="vertical-align: middle"><?=$entry["carmore_month_availablestr"]?></td>
                        <td class="small text-center" style="vertical-align: middle"><?=$entry["carmore_normal_availablestr"]?></td>
                        <td class="small text-center" style="vertical-align: middle">
                            <a href="#"onclick="viewcontentmodal('<?=$branch_idx?>')"><?= $entry["mem_name"] ?></a>
                            <span class="glyphicon glyphicon-info-sign" style="cursor: pointer" onclick="viewcontentmodal('<?=$branch_idx?>')"></span>
                        </td>
<?php
    $is_wrote  = (empty($entry['intromsg']))? 0 : 1;
    $intro_msg = ($is_wrote === 0)? '' : $entry['intromsg'];

?>
                        <td class="small text-left">
                            <span class="pmbl-intro-msg-span"><?=$intro_msg?></span><br>
                            <button type="button" class="btn btn-primary btn-xs pmbl-intro-msg-btn" data-affi-part="in" data-affi-idx="<?=$branch_idx?>" data-is-wrote="<?=$is_wrote?>">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </button>
                        </td>
                        <td class="small text-left pmbl-uniqueness-td">
                            <span class="pmbl-uniqueness-span"><?=$entry['uniqueness']?></span><br>
                            <button type="button" class="btn btn-primary btn-xs pmbl-uniqueness-btn" data-branch-idx="<?=$branch_idx?>">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </button>
                        </td>

                        <td class="small text-center" style="vertical-align: middle">
                            <span style="padding-left: 5px">
                                <img src="<?=$entry["fullurl"]?>" width="40">
                            </span>
                            <br>
                            <button type="button" class="btn btn-outline-primary btn-xs" onclick="partnerimgaemodal('main','<?=$branch_idx?>')" >
                                <span class="glyphicon glyphicon-picture"></span> 메인이미지
                            </button>
                        </td>
                        <td class="small text-center" style="vertical-align: middle">


                            <button type="button" class="btn  btn-outline-primary   btn-xs" onclick="partnerimgaemodal('sub','<?=$branch_idx?>')" >
                                <span class="glyphicon glyphicon-picture"></span> <?=$entry["subcnt"]?>개 이미지
                            </button>

                        </td>
                        <td class="small text-center" style="vertical-align: middle"> <?= $entry["branchOwnerTel"] ?></td>
                        <td class="small text-center" style="vertical-align: middle">
<?php
    if ($branch_050tel === "")
    {

?>
                                <a class="btn btn-info btn-xs pmbl-tel050-a" href="#" data-a-type="1" data-affiliate-idx="<?=$branch_idx?>" data-tel="<?=$branch_tel?>">등록하기</a>
<?php
    }
    else
    {

?>
                                <?=$branch_050tel?>
                                <a class="btn btn-danger btn-xs pmbl-tel050-a" href="#" data-a-type="2" data-affiliate-idx="<?=$branch_idx?>" data-origin050-tel="<?=$origin_050tel?>" data-tel="<?=$branch_tel?>">삭제</a>
<?php
    }

?>
                        </td>
                        <td class="small text-center">
                            <a href="https://partners.carmore.kr/partners/autoLogin.html?authority=<?= $entry["authority"] ?>&name=<?= $entry["mem_name"] ?>&rentCompanyBranchSerial=<?= $entry["rentCompany_branch_serial"] ?>&rentCompanySerial=<?= $entry["rentCompany_serial"] ?>&serial=<?= $entry["mem_serial"] ?>" target="<?= $entry["rentCompany_branch_serial"] ?>">[로그인]</a>
                            <br> <a href="http://ec2-52-78-247-231.ap-northeast-2.compute.amazonaws.com/partners/autoLogin.html?authority=<?= $entry["authority"] ?>&name=<?= $entry["mem_name"] ?>&rentCompanyBranchSerial=<?= $entry["rentCompany_branch_serial"] ?>&rentCompanySerial=<?= $entry["rentCompany_serial"] ?>&serial=<?= $entry["mem_serial"] ?>" target="<?= $entry["rentCompany_branch_serial"] ?>">[TEST 로그인]</a>
                            <br>    <A href="#" onclick="setinitpwd('<?= $entry["mem_id"] ?>')">[비번초기화]</A></td>
                        <td class="small text-center" style="vertical-align: middle"><?= $entry["regdate"] ?></td>
                        <td class="small text-center">
                            <div style="display: flex;">

                                <div style="flex: 3">
                                    <input type="date" class="form-control pmbl-enter-date" value="<?=$entry['enterDate']?>" min="2017-01-01" max="9999-12-31" style="width: 100%"/><br>
                                </div>
                                <div style="flex: 1">&nbsp;</div>
                                <div style="flex: 1">
                                    <button type="button" class="btn btn-xs btn-info" id="pmbl_enter_date_btn" data-affiliate-idx="<?=$branch_idx?>">변경</button>
                                </div>
                            </div>


                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="text-center">
            <?=$data['pagination']?>
        </div>
    </form>


    <div  id="viewmodal" class="modal fade  " style="z-index: 3200">

        <div class="modal-dialog  modal-lg ">

            <div class="modal-content "  style="background-color: white;">

                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger viewtitle">지점직원 관리 </h4>

                </div>
                <div class="modal-body" style="background-color: white;">
                </div>

            </div>
        </div>
    </div>

    <div  id="viewmodal" class="modal fade "  >

        <div class="modal-dialog viewmodalwidth ">

            <div class="modal-content "  style="background-color: white;">
                <div class="modal-header">

                    <span class="h2">파트너 이미지 관리</span>

                    <button type="button" class="btn  btn-outline-black  btn close"  onclick="closeviewmodal()" >
                        <span class="h2">X</span>
                    </button><br>

                </div>
                <div class="modal-body" style="text-align: left;background-color: white;">
                </div>

                <!-- Content will be loaded here from "remote.php" file -->
            </div>
        </div>
    </div>

    <div id="pmbl_affiliate_intro_modal_container"></div>
    <div id="pmbl_affiliate_uniqueness_modal_container"></div> <!--업체특이사항 모달컨테이너-->
    <div id="pmbl_tel050_matching_container"></div> <!--050 번호-->

    <script src="<?=base_url()?>static/lib/toastr/toastr.min.js">s</script>
    <script src="<?=asset_url()?>js/common_workspace.min.js?ver=190529"></script>
    <script src="<?=asset_url()?>modal/js/common_modal.min.js"></script>

    <script type="text/javascript">

        function viewcontentmodal(branchserial){
            var url="";
            url ="/partners/PartnerMemberBranch?ptype=branchdtllist&branchserial="+branchserial ;

            $(".viewtitle").text("지점 직원 관리");

            $.ajax({
                url: url,
                type: 'GET',
                cache: false,
            }).done(function(result){

                $('#viewmodal .modal-body').html(result)
                $('#viewmodal').modal('show')
            });
        }

        function closeviewmodal(){
            $("#viewmodal").modal("hide");
        }


        function setinitpwd(mem_id){

            var confirmtxt ="선택된 회원의 비밀번호를 초기화 하시겠습니까?";

            if(confirm(confirmtxt)){
                location.href="?ptype=initpwd&mem_id="+mem_id;
            }
        }


    </script>
    <script language="JavaScript">
        function partnerimgaemodal(ptype, serial){

            var headurl ="/partners/Partnerinfoimage?ptype="+ptype+"&serial="+serial;

            $.ajax({
                url: headurl,
                type: 'GET',
                cache: false,
            }).done(function(result){

                $('#viewmodal .modal-body').html(result)
                $('#viewmodal').modal('show')
            });


        }

        function setstorememstats(upart,stats){

            var objcomserial =  $('[name="comserial"]');
            var objlength =objcomserial.length;
            var saveparam=new Array();
            var cnt=0;



            for(var i=0;i < objlength ;i++){
                var obj =objcomserial[i];
                if(obj.checked){
                    var comserial =objcomserial[i]["value"];

                    if(comserial !=="" ){
                        saveparam.push(comserial);
                        cnt++;
                    }
                }

            }

            if(cnt===0){
                alert('선택된 정보가 없습니다.');
                return;
            }
            saveparam = saveparam.join(",");

            var confirmtxt ="";
            if(upart==="month"){
                if(stats=="y"){
                    confirmtxt ="선택된 회원을 월렌트 승인하시겠습니까?";
                }else{
                    confirmtxt ="선택된 회원을 월렌트 미승인하시겠습니까?";
                }
            }else if(upart==="normal"){
                if(stats=="y"){
                    confirmtxt ="선택된 회원을 단기렌트 승인하시겠습니까?";
                }else{
                    confirmtxt ="선택된 회원을 단기렌트 미승인하시겠습니까?";
                }
            }


            if(confirm(confirmtxt)){
                $.ajax({
                    type:"get",contentType: "application/json",
                    url:"/partners/PartnerMemberProc?emode=setallstats&upart="+upart+"&stats="+stats+"&saveparam="+saveparam,
                    datatype: "json",
                    success: function(data) {
                        alert("수정이 완료되었습니다.");
                        location.reload();

                    },
                    error: function(x, o, e) {

                    }
                });
            }


        }

    </script>

    <script>
        var assetUrl = '<?=asset_url()?>';
        var mPmblRecommendNum = '<?=$recommend_050num?>';
    </script>

    <script src="<?=asset_url()?>js/partner_member_branch_list.min.js?ver=190612"></script>
    <script src="<?=asset_url()?>modal/js/affiliate_uniqueness_modal.min.js"></script>
    <script src="<?=asset_url()?>modal/js/matching_050_modal.min.js?ver=190611"></script>
    <script src="<?=asset_url()?>modal/js/affiliate_intro_modal.min.js"></script>

</div>