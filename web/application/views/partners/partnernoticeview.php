
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>

    <div class="page-header">
        <h2>파트너스 공지사항</h2>
    </div>


    <fieldset>
        <form id="updateTodo" class="form-horizontal" role="form" method="get" action="/datashare/update_company_notice">
            <div class="form-group">
                <label for="boardTitle" class="col-md-2 control-label">제목</label>
                <div class="col-md-10">
                    <p class="form-control-static">  <?=$data["pnb_title"]?></p>
                </div>
            </div>

            <hr>
            <div class="form-group">
                <label for="boardText" class="col-md-2 control-label">내용</label>
                <div class="col-md-10">
                    <p class="form-control-static"><?=$data["pnb_content"]?></p>
                </div>
            </div>



            <br/>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">

                            <div class="col-md-4">
                                <button type="button" onclick="location.href='/partners/PartnerNoticeBoard?ptype=w&pnb_idx=<?=$data["pnb_idx"]?>'" class="btn btn-primary btn-block">수정하기</button>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-danger btn-block" id="deleteBtn">삭제하기</button>
                            </div>

                        <div class="col-md-4">
                            <button type="button" class="btn btn-success btn-block" style="background-color: #aaa; border-color: #aaa;" onclick="location.href='/partners/PartnerNoticeBoard'" >목록가기</button>
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">
                        <div class="col-md-12">
                            <style>
                                #adminReply, .adminReply, .adminReplyInsertBtn { margin-top:10px;}
                                .adminReplyRow { padding:7px 0 7px 0;}
                                .gi-2x{font-size: 2em;}
                                .gi-3x{font-size: 3em;}
                                .gi-4x{font-size: 4em;}
                                .gi-5x{font-size: 5em;}
                                .replyContent {overflow:hidden; word-wrap:break-word;}
                            </style>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </fieldset>
    <script type="text/javascript">
        $(function() {
            $("#deleteBtn").click(function() {
                if (!confirm("정말 삭제하시겠습니까?")) {
                    return false;
                }
                $(location).attr('href','/partners/PartnerNoticeBoard?ptype=del&emode=del&pnb_idx=<?=$data["pnb_idx"]?>');
            });
        });

    </script>