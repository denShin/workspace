
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">

    <style>
        .double-input .form-control {
            width: 50%;
        }
    </style>

    <div class="page-header">
        <h2>전화걸기 업체등록</h2>
    </div>


    <form id="inputform" name="af" class="form-horizontal" role="form" method="post" enctype="multipart/form-data"
          action="/partners/CallpartnerProc">
        <input type="hidden" name="emode" value="setcallpartner">
        <input type="hidden" name="datamode" value="<?=$data["datamode"]?>">
        <input type="hidden" name="serial" value="<?=$data["serial"]?>">

        <fieldset>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">업체이름</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" name="business_name" placeholder="업체이름""  value="<?=$data["business_name"]?>"   autofocus/>
                </div>
            </div>


            <div class="form-group">
                <label for="shareCategory" class="col-md-2 control-label">시/군</label>
                <div class="col-md-5">
                    <select name="service_city" id="service_city" class="form-control" onchange="setgugun()">
                       <?=$data["cityoption"]?>

                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="shareCategory" class="col-md-2 control-label">지역</label>
                <div class="col-md-7" id="gugun">
                    <?=$data["townoption"]?>
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">영업시간</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="service_time" placeholder="영업시간""  value="<?=$data["service_time"]?>"   autofocus/>
                </div>

            </div>

            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">착신번호</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="tel" placeholder="착신번호"  value="<?=$data["tel"]?>"   autofocus/>
                </div>

            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">주소</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" name="service_address" placeholder="주소""  value="<?=$data["service_address"]?>"   autofocus/>
                </div>

            </div>
            <div class="form-group">
                <label for="shareCategory" class="col-md-2 control-label">상태</label>
                <div class="col-md-5">
                    <select name="state" id="state" class="form-control" >
                        <option value="">==== 선택 ====</option>
                        <option value="0" <?if($data["state"]=="0"){echo"selected";}?>>비활성화</option>
                        <option value="1" <?if($data["state"]=="1"){echo"selected";}?>>활성화</option>

                    </select>
                </div>
            </div>

            <hr>


            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="button" onclick="sendinfo()" class="btn btn-primary btn-block">저장하기</button>
                        </div>
                        <div class="col-md-6">
                            <button type="reset" class="btn btn-warning btn-block">다시작성</button>
                        </div>
                    </div>

                </div>
            </div>
        </fieldset>
    </form>

    <script type="text/javascript">

        function setgugun(){
            var service_city = $("#service_city").val();
            var service_location="<?=$data["service_location"]?>";
            console.log("/partners/Callpartner?ptype=getgugundata&datamode=del&service_city="+service_city);
            $.ajax({
                type: "get",
                url: "/partners/Callpartner?ptype=getgugundata&datamode=del&service_city="+service_city
                    +"&service_location="+service_location,

                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $("#gugun").html(response);
                    console.log(response);

                }
            });
        }

       // // business_name tel service_city service_time  service_address
        function sendinfo(){

            var form=document.af;

            if(form.business_name.value===""){
                alert('업체명을 기입해주세요');
                form.business_name.focus();
                return;
            }
            if(form.service_city.value===""){
                alert('시도를 선택해주세요');
                return;
            }
            if(form.service_time.value===""){
                alert('영업시간을 기입해주세요');
                return;
            }
            if(form.service_address.value===""){
                alert('주소를 기입 해주세요');
                return;
            }

            if(confirm('전화걸기 업체정보를 등록/수정하시겠습니까?')){
                form.submit();
            }

        }


    </script></div>
