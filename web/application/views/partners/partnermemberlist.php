<div class="container-fluid" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }

        #viewmodal{
            overflow:auto !important;
            text-align: center;
            padding: 0!important;
        }
        .viewmodalwidth{
            min-width: 70%;
            margin: 0;
            display: inline-block; vertical-align: middle;
        }

    </style>
    <div class="page-header clearfix">
        <h3 class="pull-left"><?=$data["listitle"]?></h3>
        <?if($data["mem_stats"]=="y"){?>

        <?}?>
    </div>

    <div id="searchdiv" >
        <form id="searchform" class="form-horizontal" role="form" method="get" >
            <input type="hidden" name="mem_stats" value="<?=$data["mem_stats"]?>">
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <select class="form-control" name="sp">
                                <option value="NAME" >회사명</option>
                                <option value="OWNER" >대표자</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="sv" value="">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </fieldset>
        </form>
        <hr/>
    </div>

    <div class="pull-left">
        <button type="button" class="btn btn-primary  btn-xs"onclick="setstorememstats('stats','y')">승인하기</button>
        <button type="button" class="btn btn-danger  btn-xs"onclick="setstorememstats('stats','n')">승인대기</button>



    </div>

    <div class="pull-right">
        <button type="button" class="btn btn-primary btn-xs"onclick="setstorememstats('ver','f')">풀버전</button>
        <button type="button" class="btn btn-danger  btn-xs"onclick="setstorememstats('ver','s')">심플버전</button>


    </div>

    <div class="clearfix" style="padding-top: 10px"></div>
    <br>
    <form name="updateConfirm" method="post">
        <input type="hidden" name="emode" value="changestats">
        <input type="hidden" name="stats" >

        <div class="table-responsive">
            <table class="table   table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">Check</th>
                    <th class="text-center col-md-3">회사명</th>
                    <th class="text-center col-md-3">대표자</th>
                    <th class="text-center col-md-2">휴대폰</th>
                    <!--
                    <th class="text-center col-md-1">대표사진</th>
                    <th class="text-center col-md-1">업체사진</th>
                    -->
                    <th class="text-center col-md-1">탈퇴</th>
                    <th class="text-center col-md-2">가입일</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($data["list"] as $entry) {
                    ?>
                    <tr class="text-center row">
                        <td><input type="checkbox" name="comserial" value="<?= $entry["serial"] ?>"></td>
                        <td class="small text-left"   ><?= $entry["NAME"] ?>/<b><?= $entry["simple_verstr"] ?></b>/<?= $entry["authoritystr"] ?> </td>
                        <td class="small text-center" ><?= $entry["OWNER"] ?></td>
                        <td class="small text-center" > <?= $entry["ownertel"] ?></td>
<!--
                        <td class="small text-center">


                            <span style="padding-left: 5px">
                                <img  src="<?=$entry["fullurl"]?>" width="40">
                            </span>

                            <button type="button" class="btn  btn-outline-primary   btn-xs" onclick="partnerimgaemodal('main','<?=$entry["serial"]?>')" >
                                <span class="glyphicon glyphicon-picture"></span> 메인이미지
                            </button>

                        </td>
                        <td class="small text-center">


                            <button type="button" class="btn  btn-outline-primary   btn-xs" onclick="partnerimgaemodal('sub','<?=$entry["serial"]?>')" >
                                <span class="glyphicon glyphicon-picture"></span> <?=$entry["subcnt"]?>개 이미지
                            </button>

                        </td>
-->
                        <td class="small text-center"><a href="#" onclick="showpartnerout('<?= $entry["serial"] ?>')" >탈퇴하기</a></td>
                        <td class="small text-center"><?= $entry["regdate"] ?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="text-center">
            <?=$data['pagination']?>
        </div>
    </form>


    <div id="msgModal" class="modal fade bs-modified-modal-sm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger ">업체한마디 추가하기</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2">
                            <p class="form-control-static text-right">선택</p>
                        </div>
                        <div class="col-md-9">

                            <select class="form-control" onchange="set_intromsg(this.value)">
                                <option value="0">직접입력</option>
                                <option value="1">1번 카드내용</option>
                                <option value="2">2번 카드내용</option>
                                <option value="3">3번 카드내용</option>
                                <option value="4">4번 카드내용</option>
                                <option value="5">5번 카드내용</option>
                            </select>

                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <br>
                    <div class="row" id="txt_div" >
                        <div class="col-md-2">
                            <p class="form-control-static text-right">내용</p>
                        </div>
                        <div class="col-md-9">
                            <textarea   class="form-control" id="memomsg" placeholder="업체한마디를 입력하세요"  /></textarea>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="msgSubmit"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="pconfirmBtn">한마디 추가</span></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
                </div>
            </div>
        </div>
    </div>


    <!--* modal start *-->
    <div id="outModal" class="modal fade bs-modified-modal-sm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger modalTitle"></h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="padding-top: 10px">
                        <div class="col-md-8">
                            <p class="form-control-static text-left">* 관리자 비밀번호를 인증해주세요!</p>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="row" style="padding-top: 10px">
                        <div class="col-md-2">
                            <p class="form-control-static text-right">아이디</p>
                        </div>
                        <div class="col-md-4">
                            <p class="form-control-static text-left"><?=$data["loginid"]?></p>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="row" style="padding-top: 10px">
                        <div class="col-md-2">
                            <p class="form-control-static text-right">비밀번호</p>
                        </div>
                        <div class="col-md-4">
                            <input type="password" class="form-control" name="admin_pwd"  id="admin_pwd" value="">
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-primary" onclick="setpartnerout()"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="confirmBtn"></span></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        var comserial ="";

        function set_intromsg(val){

            var introtxt="";
            if(val==="1"){
                introtxt="안녕하세요, 고객님. 저희는 고객님의 기분 좋은 렌트카 이용을 위해 깨끗한 차량 제공을 최우선으로 생각합니다.";
            }else if(val==="2"){
                introtxt="고객님의 차량 인수/반납을 꼼꼼히 도와드리겠습니다.  저희 업체가 가장 잘 하고, 자신있는 부분입니다.";
            }else if(val==="3"){
                introtxt="저희 업체의 차량은 자차보험 가입으로 안심하고 이용하실 수 있습니다.  항상 노력하는 저희 업체가 되겠습니다.";
            }else if(val==="4"){
                introtxt="꼼꼼한 차량 정비/세차는 저희 업체의 자랑입니다.  깨끗하고 안전한 렌트카로 고객님을 맞이하겠습니다.";
            }else if(val==="5"){
                introtxt="안녕하세요, 고객님. 저희는 친절함을 최우선의 가치로 생각합니다.  고객님의 기분 좋은 이용을 위해 항상 노력하겠습니다.";
            }

            $("#memomsg").val(introtxt);

        }

        function showpartnerout(idx){
            comserial=idx;
            $("#confirmBtn").text("탈퇴 하기");
            $(".modalTitle").text("파트너 탈퇴진행");

            $("#outModal").modal().on("hidden.bs.modal", function() {
                $("#confirmBtn").text("");
                $(".modalTitle").text("");
                tempctype="";
                tempidx="";
            });
        }

        function setpartnerout(){
            var admin_pwd=$("#admin_pwd").val();

            if (admin_pwd ==="") {
                alert("비밀번호를 기입해주세요.");
                return false;
            }else {


                $.ajax({
                    type: "get",
                    url: "/partners/PartnerMemberProc?emode=setpartnerout&admin_pwd="+admin_pwd+"&companyserial="+comserial  ,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        console.log("response:"+response);
                        response = JSON.parse(response);
                        var result =response.result;
                        var code =response.code;

                        if(result==="y"){
                            alert('탈퇴처리가 완료되었습니다.');
                            $('#outModal').modal('hide');
                            $("#message").val("");
                            comserial="";
                            location.reload();
                        }else{
                            if(code==="pwd_error"){
                                alert('비밀번호를 잘못 기입하셨습니다.');
                                $("#admin_pwd").focus();
                                return ;
                            }else{
                                alert('탈퇴 진행시 오류가 발생하였습니다.');
                                return ;
                            }
                        }

                    }
                });

            }
        }


        function setstorememstats(upart,stats){

            var objcomserial =  $('[name="comserial"]');
            var objlength =objcomserial.length;
            var saveparam=new Array();
            var cnt=0;



            for(var i=0;i < objlength ;i++){
                var obj =objcomserial[i];
                if(obj.checked){
                    var comserial =objcomserial[i]["value"];

                    if(comserial !=="" ){
                        saveparam.push(comserial);
                        cnt++;
                    }
                }

            }

            if(cnt===0){
                alert('선택된 정보가 없습니다.');
                return;
            }
            saveparam = saveparam.join(",");

            var confirmtxt ="";
            if(upart==="stats"){
                if(stats=="y"){
                    confirmtxt ="선택된 회원을 승인으로 변경하시겠습니까?";
                }else{
                    confirmtxt ="선택된 회원을 승인대기로 변경하시겠습니까?";
                }
            }else if(upart==="ver"){
                if(stats=="f"){
                    confirmtxt ="선택된 회원계정을 풀버전으로 변경하시겠습니까?";
                }else{
                    confirmtxt ="선택된 회원계정을 심플버전으로 변경하시겠습니까?";
                }
            }


            if(confirm(confirmtxt)){
                $.ajax({
                    type:"get",contentType: "application/json",
                    url:"/partners/PartnerMemberProc?emode=setallstats&upart="+upart+"&stats="+stats+"&saveparam="+saveparam,
                    datatype: "json",
                    success: function(data) {
                        alert("수정이 완료되었습니다.");
                        location.reload();

                    },
                    error: function(x, o, e) {

                    }
                });
            }


        }


        function partnerimgaemodal(ptype, serial){

            var headurl ="/partners/Partnerinfoimage?ptype="+ptype+"&serial="+serial;

            $.ajax({
                url: headurl,
                type: 'GET',
                cache: false,
            }).done(function(result){

                $('#viewmodal .modal-body').html(result)
                $('#viewmodal').modal('show')
            });


        }

        function closeviewmodal(){
            $("#viewmodal").modal("hide");
            location.reload();
        }


        function showmsgmodal(idx){
            comserial=idx;

            $("#msgModal").modal().on("hidden.bs.modal", function() {

            });
        }


    </script>
</div>

<div  id="viewmodal" class="modal fade "  >

    <div class="modal-dialog viewmodalwidth ">

        <div class="modal-content "  style="background-color: white;">
            <div class="modal-header">

                <span class="h2">파트너 이미지 관리</span>

                <button type="button" class="btn  btn-outline-black  btn close"  onclick="closeviewmodal()" >
                    <span class="h2">X</span>
                </button><br>

            </div>
            <div class="modal-body" style="text-align: left;background-color: white;">
            </div>

            <!-- Content will be loaded here from "remote.php" file -->
        </div>
    </div>
</div>


</body>
</html>
