<div class="container" style="padding-top: 20px;padding-bottom: 10px;">
    <form name="updateConfirm" method="post">
        <input type="hidden" name="emode" value="changestats">
        <input type="hidden" name="stats" >

        <div class="table-responsive">
            <table class="table   table-bordered"  style="width: 800px">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-3">회사/지점명</th>
                    <th class="text-center col-md-3">대표자</th>
                    <th class="text-center col-md-3">휴대폰</th>
                    <th class="text-center col-md-3">로그인/초기화</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($data["list"] as $entry) {

                    ?>
                    <tr class="text-center row">
                        <td class="small text-left"   ><?= $entry["company_name"] ?> > <?= $entry["branchName"] ?> </td>
                        <td class="small text-center" ><?= $entry["mem_name"] ?>
                        </td>
                        <td class="small text-center" > <?= $entry["branchOwnerTel"] ?></td>
                        <td class="small text-center">
                            <a href="https://partners.carmore.kr/partners/autoLogin.html?authority=<?= $entry["authority"] ?>&name=<?= $entry["mem_name"] ?>&rentCompanyBranchSerial=<?= $entry["rentCompany_branch_serial"] ?>&rentCompanySerial=<?= $entry["rentCompany_serial"] ?>&serial=<?= $entry["mem_serial"] ?>" target="<?= $entry["rentCompany_branch_serial"] ?>">[로그인]</a>
                            / <A href="#" onclick="setinitpwd('<?= $entry["mem_id"] ?>')">[비번초기화]</A></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>

    </form>

</div>

