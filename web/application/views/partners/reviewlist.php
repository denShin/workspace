<div class="container-fluid" style="padding-top: 50px;padding-bottom: 70px;">

    <div class="page-header clearfix">
        <h3 class="pull-left">리뷰관리 리스트</h3>

    </div>
    <div id="searchdiv" >
        <form id="searchform" class="form-horizontal" role="form" method="get" >
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <select class="form-control" name="sp" id="sp">
                                <option value="c.name" <?if($data["sp"]=="c.name") {echo "selected";}?> >회사명</option>
                                <option value="d.driver_name"  <?if($data["sp"]=="d.driver_name") {echo "selected";}?>  >고객명</option>
                                <option value="a.reservation_idx" <?if($data["sp"]=="a.reservation_idx") {echo "selected";}?>  >예약번호</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="sv" id="sv" value="<?=$data["sv"]?>">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </fieldset>
        </form>

    </div>
    <div class="clearfix"></div>
    * 답변안된 리뷰수 <?=$data["gapcnt"]?>개 (<?=$data["gapper"]?>%)
    <div class="pull-right">
        <select class="form-control" name="pageorderby"  id="pageorderby" onchange="sendorderby()">
            <option value="register_date-desc" <?if($data["pageorderby"]=="register_date-desc"){echo "selected";}?>  >최신순</option>
            <option value="branch_evaluation-asc" <?if($data["pageorderby"]=="branch_evaluation-asc"){echo "selected";}?>   >평점 낮은 순</option>
            <option value="opinion-asc" <?if($data["pageorderby"]=="opinion-asc"){echo "selected";}?>   >가나다 순 </option>
        </select>
    </div>

    <div class="clearfix" style="padding-top: 20px"></div>
    <form name="updateConfirm" method="post">
        <input type="hidden" name="emode" value="changestats">
        <input type="hidden" name="stats" >

        <div class="table-responsive">
            <table class="table   table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">예약번호</th>
                    <th class="text-center col-md-2">고객명</th>
                    <th class="text-center col-md-2">회사/지점명</th>
                    <th class="text-center col-md-4">리뷰</th>
                    <th class="text-center col-md-2">평가</th>
                    <th class="text-center col-md-1">등록일</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($data["list"] as $entry) {


                    ?>
                    <tr class="text-center row">
                        <td><?= $entry["reservation_idx"] ?></td>
                        <td class="small text-center"><?= $entry["driver_name"] ?>/<?= $entry["revcnt"] ?> 회<br><?= $entry["driver_phone_encrypt"] ?></td>
                        <td class="small text-center"><?= $entry["companyname"] ?> > <?= $entry["branchname"] ?><br><?= $entry["model"] ?> </td>
                        <td class="small text-left">

                            <?if($entry["admincheck_yn"] !="y"){?>
                            <span style="color:<?=$entry["admincheck_yncolor"] ?>"> <?=$entry["opinion"] ?></span>
                            <?}else{?>
                                <b> <?=$entry["opinion"] ?></b>
                            <?}?>
                              <br>
                              <?
                              if ($entry["replycnt"] > 0) { ?>
                                  <button type="button" class="btn  btn-primary   btn-xs"
                                          onclick="showmsgmodal('<?= $entry["reservation_idx"] ?>')">
                                      답변보기
                                  </button>
                              <?
                              } else { ?>
                                  <button type="button" class="btn  btn-info   btn-xs"
                                          onclick="showmsgmodal('<?= $entry["reservation_idx"] ?>')">
                                      미답변
                                  </button>
                              <?
                              } ?>
                              &nbsp;&nbsp;&nbsp;
                              <?
                              if ($entry["blindstatus"] != "2") { ?>
                                  <button type="button" class="btn  btn-warning   btn-xs"
                                          onclick="showblindmodal('<?= $entry["reservation_idx"] ?>')">
                                      블라인드처리
                                  </button>
                              <?
                              } else { ?>
                                  <button type="button" class="btn  btn-danger   btn-xs"
                                          onclick="showblindmodal('<?= $entry["reservation_idx"] ?>')">
                                      해제하기
                                  </button>
                              <?
                              }
                          ?>
                        </td>
                        <td class="small text-center">차량상태 <b><?= $entry["car_evaluation"]?></b> ,친절 <b><?= $entry["branch_evaluation"] ?></b> ,절차 <b><?= $entry["take_evaluation"]?></b></td>

                        <td class="small text-center"><?= $entry["regdate"] ?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="text-center">
            <?=$data['pagination']?>
        </div>
    </form>


    <input type="hidden" name="crr_reservation_idx" id="crr_reservation_idx">

    <div id="reviewModal" class="modal fade bs-modified-modal-sm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger ">고객이 남긴 리뷰</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-11">
                            렌트카이용내역: <span id="usestr"></span>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-11">
                            남긴날짜: <span id="datestr"></span>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-11">
                            <span id="evalstr"></span>
                        </div>
                    </div>

                    <hr>

                    <div class="row" id="txt_div" >
                        <div class="col-md-2">
                            <p class="form-control-static text-right">리뷰</p>
                        </div>
                        <div class="col-md-9">
                            <span id="opinionstr"></span>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <hr>
                    <div class="row" id="txt_div" >
                        <div class="col-md-2">
                            <p class="form-control-static text-right">답변</p>
                        </div>
                        <div class="col-md-9">
                            <textarea   class="form-control" style="height: 150px" id="crr_reply_content" placeholder="답변을 입력하세요"  ></textarea>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="msgSubmit" onclick="sendreviewreply()"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="pconfirmBtn">답변저장</span></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
                </div>
            </div>
        </div>
    </div>



    <div id="blindModal" class="modal fade bs-modified-modal-sm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger ">리뷰 블라인드</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-11">
                           블라인드 이유를 선택해주세요
                            <br> 카모아 트러스트 리뷰정책(2018.12.21기준)
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-11">
                            <select class="form-control" name="blind_code"  id="blind_code" >
                                <option value="1" >1. 목적이나 의미가 분명하지 않은 리뷰</option>
                                <option value="2" >2.사용자 또는 동반 이용자의 경험 외의 내용을 담은 리뷰</option>
                                <option value="3" >3.타인에 대한 명예훼손, 비방 내용이 담긴 리뷰</option>
                                <option value="4" >4.광고성 정보를 포함하는 리뷰</option>
                                <option value="5" >5.타인 ,다른 계정 또는 단체를 사칭한 리뷰</option>
                                <option value="6" >6.욕설,음란어를 사용하여 타인에게 불쾌감을 주는 리뷰</option>
                                <option value="7" >7.사실관계에 대한 확인이 필요한 리뷰</option>
                                <option value="8" >8.실명, 전화번호 등 개인정보가 포함된 리뷰</option>
                                <option value="9" >9.과도한 문자의 반복이나 난해한 오타가 있는 리뷰</option>
                                <option value="10" >10.기타 에티켓을 위반한 리뷰</option>
                                <option value="111" >11.그 외 상황</option>
                            </select>
                        </div>
                    </div>


                    <hr>

                    <div class="row"  >
                        <div class="col-md-2">
                            <p class="form-control-static text-right">사유</p>
                        </div>
                        <div class="col-md-9">
                            <textarea   class="form-control" style="height: 150px" id="blind_codetext" placeholder="담당자 사유작성 "   ></textarea>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="blindbtn" onclick="sendreviewblind()"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="pconfirmBtn">블라인드처리</span></button>
                    <button type="button" class="btn btn-danger" id="notblindbtn" onclick="clearblind()" style="display: none">블라인드 해제</button>

                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        var branchserial="";
        var bintrocode="";
        var bintromsg="";

        function sendorderby(){
            var sp=$("#sp").val();
            var sv=$("#sv").val();
            var pageorderby=$("#pageorderby").val();
            location.href="/partners/Reviewmanage?sp="+sp+"&sv="+sv+"&pageorderby="+pageorderby;
        }


        function showmsgmodal(reservation_idx){

            var headurl ="/partners/Reviewmanage?ptype=popreview&reservation_idx="+reservation_idx;

            $.ajax({
                url: headurl,
                type: 'GET',
                cache: false,
            }).done(function(result){


                var json = $.parseJSON(result);
                var review = json["review"][0];
                var answer = json["answer"][0];
                console.log("result:%o",answer)

                var f_rentstartdate =review["f_rentstartdate"];
                var f_rentenddate =review["f_rentenddate"];
                var opinion =review["opinion"];
                var driver_name =review["driver_name"];
                var model =review["model"];
                var take_evaluation =review["take_evaluation"];
                var branch_evaluation =review["branch_evaluation"];
                var car_evaluation =review["car_evaluation"];
                var register_date =review["register_date"];
                var crr_reply_content = "";

                if(answer){
                    crr_reply_content = answer["crr_reply_content"];
                    var regex = /<br\s*[\/]?>/gi;
                    crr_reply_content =crr_reply_content.replace(regex, "\n");
                }

                var usestr =f_rentstartdate+"~"+f_rentenddate+" / <b>"+model+"</b>";
                var datestr = register_date;
                var evalstr = "차량관리상태: "+car_evaluation+" | 직원친절도: "+branch_evaluation+" |  이용절차: "+take_evaluation;


                $("#crr_reply_content").val(crr_reply_content);
                $("#crr_reservation_idx").val(reservation_idx);
                $("#usestr").html(usestr);
                $("#datestr").html(datestr);
                $("#evalstr").html(evalstr);
                $("#opinionstr").html(opinion);
                $("#reviewModal").modal().on("hidden.bs.modal", function() {

                    branchserial="";
                    bintrocode="";
                    bintromsg="";
                });
            });


            $("#introcode").val(bintrocode);
            $("#intromsg").val(bintromsg);


        }
        function nl2br(str){
            return str.replace(/\n/g, "<br />");
        }

        function sendreviewreply(){
            var reservation_idx =$("#crr_reservation_idx").val();
            var crr_reply_content =nl2br($("#crr_reply_content").val());
            if(crr_reply_content===""){
                alert("답변을 입력해주세요");
                $("#crr_reply_content").focus();
                return;
            }


            if(confirm("답변을 기입하시겠습니까?")){
                var procurl ="/partners/Reviewmanage?ptype=popreview&reservation_idx="+reservation_idx;

                 $.ajax({
                    type: "get",
                    url: "/partners/Reviewmanage?ptype=setreviewanswer&reservation_idx="+reservation_idx+"&crr_reply_content="+crr_reply_content,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        console.log("response:"+response);
                        $('#reviewModal').modal('hide');
                        $("#crr_reply_content").val("");
                        $("#crr_reservation_idx").val("");

                        location.reload();
                    }
                });

            }
        }

        function clearblind(){

            var reservation_idx =$("#crr_reservation_idx").val();


            if(confirm("블라인드를 해제 하시겠습니까?")){
                var procurl ="/partners/Reviewmanage?ptype=setreviewblind&reservation_idx="+reservation_idx;

                $.ajax({
                    type: "get",
                    url: "/partners/Reviewmanage?ptype=setreviewblind&work=clear&reservation_idx="+reservation_idx+"&blind_code=&blind_codetext=",
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        console.log("response:"+response);
                        $('#reviewModal').modal('hide');
                        $("#crr_reply_content").val("");
                        $("#reservation_idx").val("");

                        location.reload();
                    }
                });

            }
        }

        function showblindmodal(reservation_idx){

            var headurl ="/partners/Reviewmanage?ptype=popreview&reservation_idx="+reservation_idx;

            $.ajax({
                url: headurl,
                type: 'GET',
                cache: false,
            }).done(function(result){


                var json = $.parseJSON(result);
                var review = json["review"][0];
                console.log("result:%o",review)

                var originblind_code =review["blind_code"];
                var blind_codetext =review["blind_codetext"];

                if(originblind_code==='0'){
                    $("#blindbtn").show();
                    $("#notblindbtn").hide();
                }else{
                    $("#blindbtn").hide();
                    $("#notblindbtn").show();
                }

                var blind_code=originblind_code;
                if(originblind_code===false || originblind_code===""|| originblind_code==="0"){
                    blind_code="1";
                }
                if(!blind_codetext) blind_codetext="";

                console.log("blind_code:"+blind_code)
                $('#blind_code').val(blind_code);
                $("#blind_codetext").val(blind_codetext);

                $("#crr_reservation_idx").val(reservation_idx);

                $("#blindModal").modal().on("hidden.bs.modal", function() {

                });
            });

        }


        function sendreviewblind(){

            var blind_code =  $("#blind_code").val();

            var reservation_idx =$("#crr_reservation_idx").val();


            var blind_codetext =$("#blind_codetext").val();
            if(blind_code==="11" &&  blind_codetext===""){
                alert("블라인드사유를 입력해주세요");
                $("#blind_codetext").focus();
                return;
            }

            if(confirm("블라인드를 등록하시겠습니까?")){
                blind_codetext =nl2br(blind_codetext);
                var procurl ="/partners/Reviewmanage?ptype=setreviewblind&reservation_idx="+reservation_idx;

                $.ajax({
                    type: "get",
                    url: "/partners/Reviewmanage?ptype=setreviewblind&work=blind&reservation_idx="+reservation_idx+"&blind_code="+blind_code+"&blind_codetext="+blind_codetext,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        console.log("response:"+response);
                        $('#reviewModal').modal('hide');
                        $("#crr_reply_content").val("");
                        $("#reservation_idx").val("");

                        location.reload();
                    }
                });

            }
        }

    </script>
</div>




</body>
</html>
