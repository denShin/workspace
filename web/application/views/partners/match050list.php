<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <form id="updateConfirm">
        <input type="hidden" name="isConfirm" value="" form="updateConfirm"/>
        <input type="hidden" name="emode" value="agree_yn" form="updateConfirm"/>

        <div class="page-header clearfix">
            <h2 class="pull-left">지점 050 번호 관리</h2>
        </div>

        <div class="clearfix" style="padding-top: 10px"></div>
        <br>
        <div class="table-responsive">
            <table class="table   table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">No</th>
                    <th class="text-center col-md-3">회사/지점명</th>
                    <th class="text-center col-md-2">대표자</th>
                    <th class="text-center col-md-3">전화번호</th>
                    <th class="text-center col-md-3">050 번호</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($data["list"] as $entry) {


                    ?>
                    <tr class="text-center row">
                        <td><?= $entry["serial"] ?></td>
                        <td class="small text-left"   ><?= $entry["company_name"] ?> > <?= $entry["branchName"] ?> </td>
                        <td class="small text-center" ><?= $entry["mem_name"] ?></td>
                        <td class="small text-center" > <?= $entry["tel"] ?></td>
                        <td class="small text-center" >
                            <?if($entry["tel050"]==""){?>
                                <a href="#" onclick="showtelmodal('<?= $entry["serial"] ?>','<?= $entry["tel"] ?>')" class="btn btn-info">등록하기</a>
                            <?}else{?>
                            <?=$entry["tel050"]?>  <a href="#" onclick="delnewtel('<?= $entry["serial"] ?>','<?= $entry["origintel050"] ?>','<?= $entry["tel"] ?>')" class="btn btn-danger">삭제</a>
                            <?}?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="text-center">
            <?=$data['pagination']?>
        </div>


</div>

<div id="telModal" class="modal fade bs-modified-modal-sm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-danger ">050 번호 추가하기</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2">
                        <p class="form-control-static text-right">050번호</p>
                    </div>
                    <div class="col-md-2">
                        <p class="form-control-static text-right">05037961</p>
                    </div>
                    <div class="col-md-5">
                        <input type="number" id="sendnewtelnum"  class="form-control"  maxlength="4" oninput="maxLengthCheck(this)" onpress="isNumber(event)" value="">
                    </div>
                    <div class="col-md-1"></div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"  onclick="sendnewtel()"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="pconfirmBtn">번호 추가</span></button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
            </div>
        </div>
    </div>
</div>

<script language="JavaScript">

    var branch_serial="";
    var branch_tel="";

    function maxLengthCheck(object){
        if (object.value.length > object.maxLength){
            object.value = object.value.slice(0, object.maxLength);
        }
    }

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        let charCode = (evt.which) ? evt.which : evt.keyCode;
        if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
            evt.preventDefault();
        } else {
            return true;
        }
    }

    function showtelmodal(bserial ,btel){
        branch_serial =bserial;
        branch_tel =btel;

        if(btel===""){
            alert('변환 할수 있는 번호정보가 없습니다');
            return ;
        }
        $("#telModal").modal().on("hidden.bs.modal", function() {

        });
    }

    function delnewtel(bserial,deltel,branch_tel){

        console.log( "/partners/PartnerMemberProc?emode=sendelnewtel&datamode=del&bserial="+bserial
            +"&deltel="+deltel+"&branch_tel="+branch_tel);
        if(confirm("050 번호를  삭제 하시겠습니까?")){
            $.ajax({
                type: "get",
                url: "/partners/PartnerMemberProc?emode=sendelnewtel&datamode=del&bserial="+bserial
                    +"&deltel="+deltel+"&branch_tel="+branch_tel,

                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    branch_serial="";
                    location.reload();
                }
            });
        }
    }

    function sendnewtel(){

        // http://workspace.teamo2.kr/partners/PartnerMemberProc?emode=sendnewtel&datamode=new&bserial=568&sendnewtelnum=1111&branch_tel=
        var sendnewtelnum= $("#sendnewtelnum").val();

        if(sendnewtelnum===""){
            alert('번호를 기입 등록해주세요');

            return;
        }

        if(confirm("050 번호를 등록 하시겠습니까?")){
            $.ajax({
                type: "get",
                url: "/partners/PartnerMemberProc?emode=sendnewtel&datamode=new&bserial="+branch_serial
                    +"&sendnewtelnum="+sendnewtelnum+"&branch_tel="+branch_tel  ,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log("response:"+response);
                    var json = JSON.parse(response);

                    var result = json["result"];
                    if(result==="y"){
                        branch_serial="";
                        location.reload();
                    }else if(result ==="tel_exists"){
                        alert('이미 등록된 번호 입니다. 다른번호를 기입해주세요');
                        return;
                    }

                }
            });
        }
    }

</script>
</body>
</html>
