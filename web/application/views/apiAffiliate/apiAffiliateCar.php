<?php
$waab_inventory              = $data['waabInventory'];
$selected_waab_idx           = ($data['selectedWaabIdx'] === "")? $data['selectedWaabIdx'] : (int)$data['selectedWaabIdx'];
$selected_waab_api           = (int)$data['selectedWaabApi'];
$selected_waab_car_inventory = $data['selectedWaabCarInventory'];
$master_car_inventory        = $data['masterCarInventory'];
$compensation_inventory      = $data['compensationInventory'];

$waab_count     = count($waab_inventory);
$waab_car_count = count($selected_waab_car_inventory);

$selected_name = "";
$selected_api  = "";

?>

<div class="container workspace-basic-container col-md-12">
    <div class="page-header clearfix">
        <h3 class="pull-left">API 업체 차량관리</h3>
    </div>

    <div class="clearfix"></div>

    <div class="col-md-12 alert alert-info" id="aac_alert_div">
        <strong>업체를 선택해주세요</strong>
    </div>

    <div class="col-md-4">
        <div class="row col-md-12">
            <div class="input-group">

                <input type="text" class="form-control" id="aac_search_input" placeholder="회사명, 지점명 검색"/>
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
        </div>

        <div class="row col-md-12" style="margin-top: 10px">
<?php
if ($waab_count !== 0)
{
?>

            <div class="col-md-12 well">
<?php
    foreach ($waab_inventory as $waab_obj)
    {
        $waab_idx = (int)$waab_obj['waabIdx'];

        $is_selected = $selected_waab_idx === $waab_idx;

        $btn_type = $is_selected? "btn-success" : "btn-default";
        $branch_name = ($waab_obj['waabName'] === "")? $waab_obj['waaName'] : $waab_obj['waaName']." ".$waab_obj['waabName'];

        if ($is_selected)
        {
            $selected_name = $branch_name;
            $selected_api  = api_flag_to_val($waab_obj['waabApi']);

        }

?>
                <button type="button" class="col-md-12 btn <?=$btn_type?> aac-affiliate-btn" data-waab-idx="<?=$waab_idx?>"><?=$branch_name?></button>
<?php
    }

?>

            </div>

<?php
}
else
{

?>
            <div class="col-md-12 well text-center" style="font-weight: bold">
                업체목록(검색결과) 이(가) 없습니다.
            </div>
<?php
}

?>

        </div>
    </div>

    <div class="col-md-8">

<?php
if ($waab_car_count !== 0)
{
?>
        <div class="row col-md-12">
            <div class="col-md-12 well">

                <div class="row col-md-12" style="margin-bottom: 1em">

                    <div class="col-md-4">
                        <label>업체명 : </label>
                        <span><?=$selected_name?></span>
                    </div>

                    <div class="col-md-offset-4 col-md-4 text-right">
                        <label>선택된 API : </label>
                        <span><?=$selected_api?></span>
                    </div>

                </div>

<?php
    foreach ($selected_waab_car_inventory as $car_obj)
    {
        $inherence_idx     = $car_obj['idx'];
        $body_idx          = "aac_panel_inherence".$selected_api."_".$inherence_idx;
        $cimaster_idx      = (int)$car_obj['cimasterIdx'];
        $cimaster_car_type = ($car_obj['cimasterCarType'] === "")? "" : (int)$car_obj['cimasterCarType'];
        $cimaster_model    = $car_obj['cimasterModel'];
        $compensation_idx  = (int)$car_obj['wacIdx'];
        $compensation_name = $car_obj['wacName'];

        $is_it_matching_complete = ($cimaster_idx === 0 || $compensation_idx === 0)? false : true;

        $model_name      = $car_obj['carName']. " (".$car_obj['carCode'].")";
        $matching_string = ($is_it_matching_complete)? "매칭완료($cimaster_model / $compensation_name)" : "매칭 안 됨";
        $panel_type      = ($is_it_matching_complete)? "panel-default" : "panel-danger";

?>
                <div class="row col-md-12 panel-group aac-match-panel-group" data-inherence-idx="<?=$inherence_idx?>" data-orig-cimaster-idx="<?=$cimaster_idx?>">
                    <div class="panel <?=$panel_type?>">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <div data-toggle="collapse" href="#<?=$body_idx?>" style="width: 100%; cursor: pointer">
                                    <span><?=$model_name?></span>
                                    <span class="pull-right"><?=$matching_string?></span>
                                </div>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="<?=$body_idx?>">
                            <div class="panel-body">

                                <div class="row aac-match-panel-body">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>매칭 등급</label>
                                            <select class="workspace-select aac-master-car-type-select" style="margin-bottom: .5em">
                                                <option value="" <?=($cimaster_car_type === "")? "selected": ""?>>등급선택</option>
                                                <option value="0" <?=($cimaster_car_type === 0)? "selected": ""?>>경형</option>
                                                <option value="1" <?=($cimaster_car_type === 1)? "selected": ""?>>소형</option>
                                                <option value="2" <?=($cimaster_car_type === 2)? "selected": ""?>>준중형</option>
                                                <option value="3" <?=($cimaster_car_type === 3)? "selected": ""?>>중형</option>
                                                <option value="4" <?=($cimaster_car_type === 4)? "selected": ""?>>대형</option>
                                                <option value="5" <?=($cimaster_car_type === 5)? "selected": ""?>>수입</option>
                                                <option value="6" <?=($cimaster_car_type === 6)? "selected": ""?>>RV</option>
                                                <option value="7" <?=($cimaster_car_type === 7)? "selected": ""?>>SUV</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label>매칭 모델</label>
                                            <select class="workspace-select aac-master-car-model-select" style="margin-bottom: .5em"></select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>매칭 종합보험</label>
                                    <select class="workspace-select aac-compensation-select">
                                        <option value="">종합보험 선택</option>
<?php
        foreach ($compensation_inventory as $compensation_obj)
        {
            $option_wac_idx = (int)$compensation_obj['idx'];
            $selected = $option_wac_idx === $compensation_idx? "selected" : "";
?>
                                        <option value="<?=$option_wac_idx?>" <?=$selected?>><?=$compensation_obj['name']?></option>
<?php
        }

?>
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
<?php
    }

?>


                <div class="col-md-12">
                    <div class=" pull-right">
                        <button type="button" class="btn btn-primary" id="aac_save_btn">저장</button>
                    </div>
                </div>


            </div>
        </div>
<?php
}
else
{

?>
        <div class="row col-md-12">
        <div class="col-md-12 well text-center" style="font-weight: bold">
            불러올 수 있는 차종이 없습니다.
        </div>
    </div>
<?php
}

?>


    </div>
</div>

<?php
function api_flag_to_val($flag)
{
    switch ((int)$flag)
    {
        case 1:
            return "그림";
        case 2:
            return "인스";
        case 3:
            return "리본";
        default:
            return "";
    }
}
?>

<script>
    var mAacAffiliateInventory = JSON.parse('<?=json_encode($waab_inventory)?>');
    var mAacMasterCarInventory = JSON.parse('<?=json_encode($master_car_inventory)?>');
    var mAacSelectedIdx = <?=($selected_waab_idx === "")? 0 : $selected_waab_idx?>;
    var mAacSelectedApi = <?=$selected_waab_api?>;

</script>
<script src="<?=base_url()?>static/lib/toastr/toastr.min.js"></script>
<script src="<?=asset_url()?>js/common_workspace.min.js?ver=190529"></script>
<script src="<?=asset_url()?>js/api_affiliate_car.min.js?ver=190510"></script>
