<?php
$crud_type              = $data['type'];
$compensation_inventory = $data['inventory'];
$wac_idx                = isset($data['selectedIdx'])? (int)$data['selectedIdx'] : 0;
$wac_info               = isset($data['info'])? $data['info'] : [];

?>

<div class="container workspace-basic-container col-md-12">
    <div class="page-header clearfix">
        <h3 class="pull-left">API 종합보험 설정</h3>
    </div>

    <div class="clearfix"></div>

    <div class="col-md-4">
        <h4 class="col-md-10">종합보험 리스트</h4>
        <div class="col-md-2">
            <button type="button" class="btn btn-primary" id="acw_add_btn">추가하기</button>
        </div>

        <div class="well col-md-12" id="acw_compensation_btn_container"></div>

        <div class="well text-center col-md-12" id="acw_compensation_empty_container">
            <h4>등록된 종합보험이 없습니다</h4>
        </div>
    </div>

    <div class="col-md-8">
        <h4 class="pull-left">종합보험 정보</h4>

        <div class="well col-md-12" id="acw_compensation_info_container">

<?php
$name_value        = (isset($wac_info['name']))? $wac_info['name'] : "";
$compensation_type = (isset($wac_info['type']))? (int)$wac_info['type'] : 0;

$type1_checked = "";
$type2_checked = "";

if ($compensation_type !== 0)
{
    $type1_checked = ($compensation_type === 1)? "checked" : "";
    $type2_checked = ($compensation_type === 2)? "checked" : "";

}

?>
            <div class="col-md-12 form-group">
                <label>종합보험명</label>
                <div class="col-md-12">
                    <input type="text" class="form-control" id="acm_compensation_name" value="<?=$name_value?>"/>
                </div>
            </div>

            <div class="col-md-12 form-group">
                <label>카모아 노출종류 설정</label>
                <div class="col-md-12" id="" style="font-size: 120%">&nbsp;
                    <label class="radio-inline"><input type="radio" class="acm-compensation-type-radio" name="acm_compensation_type_radio" value="2" checked/>설정한 정보 노출</label>
                </div>
            </div>

<?php
$compensation1 = (isset($wac_info['compensation1']))? (int)$wac_info['compensation1'] : "";
$compensation2 = (isset($wac_info['compensation2']))? (int)$wac_info['compensation2'] : "";
$compensation3 = (isset($wac_info['compensation3']))? (int)$wac_info['compensation3'] : "";
$compensation4 = (isset($wac_info['compensation4']))? (int)$wac_info['compensation4'] : "";
$compensation5 = (isset($wac_info['compensation5']))? (int)$wac_info['compensation5'] : "";
$compensation6 = (isset($wac_info['compensation6']))? (int)$wac_info['compensation6'] : "";
$fee1          = (isset($wac_info['fee1']))? (int)$wac_info['fee1'] : "";
$fee2          = (isset($wac_info['fee2']))? (int)$wac_info['fee2'] : "";
$fee3          = (isset($wac_info['fee3']))? (int)$wac_info['fee3'] : "";
$fee4          = (isset($wac_info['fee4']))? (int)$wac_info['fee4'] : "";
$fee5          = (isset($wac_info['fee5']))? (int)$wac_info['fee5'] : "";
$fee6          = (isset($wac_info['fee6']))? (int)$wac_info['fee6'] : "";


?>
            <!-- 대인/대물 파트 -->
            <!-- 대인1 -->
            <div class="col-md-4 form-group">
                <label>대인 1 보상범위 / 면책금</label>
                <select class="workspace-select acm-compensation-range-select" id="acm_compensation1_select" data-key="compensation1" <?=($compensation_type === 1)? "disabled" : ""?> style="<?=($compensation_type === 1)? "background: rgb(235, 235, 235)" : ""?>">
                    <option <?=($compensation1 === "")? "selected": ""?> value=""></option>
                    <option <?=($compensation1 === 1)? "selected": ""?> value="1">무한</option>
                    <option <?=($compensation1 === 2)? "selected": ""?> value="2">자배법 시행령에서 정한 금액</option>
                    <option <?=($compensation1 === 5)? "selected": ""?> value="5">1억원</option>
                </select>
                <div class="input-group">
                    <input type="tel" class="form-control text-right acm-compensation-fee-input" id="acm_fee1_input" value="<?=$fee1?>" data-key="fee1" <?=($compensation_type === 1)? "readonly" : ""?>/>
                    <span class="input-group-addon">만원</span>
                </div>
            </div>

            <!-- 대인2 -->
            <div class="col-md-4 form-group">
                <label>대인 2 보상범위 / 면책금</label>
                <select class="workspace-select acm-compensation-range-select" id="acm_compensation2_select" data-key="compensation2" <?=($compensation_type === 1)? "disabled" : ""?> style="<?=($compensation_type === 1)? "background: rgb(235, 235, 235)" : ""?>">
                    <option <?=($compensation2 === "")? "selected": ""?> value=""></option>
                    <option <?=($compensation2 === 1)? "selected": ""?> value="1">무한</option>
                    <option <?=($compensation2 === 3)? "selected": ""?> value="3">3억원</option>
                    <option <?=($compensation2 === 4)? "selected": ""?> value="4">2억원</option>
                    <option <?=($compensation2 === 5)? "selected": ""?> value="5">1억원</option>
                    <option <?=($compensation2 === 8)? "selected": ""?> value="8">5천만원</option>
                </select>
                <div class="input-group">
                    <input type="tel" class="form-control text-right acm-compensation-fee-input" id="acm_fee2_input" value="<?=$fee2?>" data-key="fee2" <?=($compensation_type === 1)? "readonly" : ""?>/>
                    <span class="input-group-addon">만원</span>
                </div>
            </div>

            <!-- 대물 -->
            <div class="col-md-4 form-group">
                <label>대물 보상범위 / 면책금</label>
                <select class="workspace-select acm-compensation-range-select" id="acm_compensation3_select" data-key="compensation3" <?=($compensation_type === 1)? "disabled" : ""?> style="<?=($compensation_type === 1)? "background: rgb(235, 235, 235)" : ""?>">
                    <option <?=($compensation3 === "")? "selected": ""?> value=""></option>
                    <option <?=($compensation3 === 3)? "selected": ""?> value="3">3억원</option>
                    <option <?=($compensation3 === 4)? "selected": ""?> value="4">2억원</option>
                    <option <?=($compensation3 === 5)? "selected": ""?> value="5">1억원</option>
                    <option <?=($compensation3 === 6)? "selected": ""?> value="6">8천만원</option>
                    <option <?=($compensation3 === 7)? "selected": ""?> value="7">7천만원</option>
                    <option <?=($compensation3 === 8)? "selected": ""?> value="8">5천만원</option>
                    <option <?=($compensation3 === 9)? "selected": ""?> value="9">3천만원</option>
                    <option <?=($compensation3 === 10)? "selected": ""?> value="10">2천만원</option>
                    <option <?=($compensation3 === 11)? "selected": ""?> value="11">1천5백만원</option>
                    <option <?=($compensation3 === 12)? "selected": ""?> value="12">1천만원</option>
                </select>
                <div class="input-group">
                    <input type="tel" class="form-control text-right acm-compensation-fee-input" id="acm_fee3_input" value="<?=$fee3?>" data-key="fee3" <?=($compensation_type === 1)? "readonly" : ""?>/>
                    <span class="input-group-addon">만원</span>
                </div>
            </div>

            <!-- 자손 파트 -->
            <!-- 사망시 -->
            <div class="col-md-4 form-group">
                <label>자손(사망시) 보상범위 / 면책금</label>
                <select class="workspace-select acm-compensation-range-select" id="acm_compensation4_select" data-key="compensation4" <?=($compensation_type === 1)? "disabled" : ""?> style="<?=($compensation_type === 1)? "background: rgb(235, 235, 235)" : ""?>">
                    <option <?=($compensation4 === "")? "selected": ""?> value=""></option>
                    <option <?=($compensation4 === 5)? "selected": ""?> value="5">1억원</option>
                    <option <?=($compensation4 === 8)? "selected": ""?> value="8">5천만원</option>
                    <option <?=($compensation4 === 9)? "selected": ""?> value="9">3천만원</option>
                    <option <?=($compensation4 === 10)? "selected": ""?> value="10">2천만원</option>
                    <option <?=($compensation4 === 11)? "selected": ""?> value="11">1천5백만원</option>
                </select>
                <div class="input-group">
                    <input type="tel" class="form-control text-right acm-compensation-fee-input" id="acm_fee4_input" value="<?=$fee4?>" data-key="fee4" <?=($compensation_type === 1)? "readonly" : ""?>/>
                    <span class="input-group-addon">만원</span>
                </div>
            </div>

            <!-- 부상시 -->
            <div class="col-md-4 form-group">
                <label>자손(부상시) 보상범위 / 면책금</label>
                <select class="workspace-select acm-compensation-range-select" id="acm_compensation5_select" data-key="compensation5" <?=($compensation_type === 1)? "disabled" : ""?> style="<?=($compensation_type === 1)? "background: rgb(235, 235, 235)" : ""?>">
                    <option <?=($compensation5 === "")? "selected": ""?> value=""></option>
                    <option <?=($compensation5 === 5)? "selected": ""?> value="5">1억원</option>
                    <option <?=($compensation5 === 8)? "selected": ""?> value="8">5천만원</option>
                    <option <?=($compensation5 === 9)? "selected": ""?> value="9">3천만원</option>
                    <option <?=($compensation5 === 10)? "selected": ""?> value="10">2천만원</option>
                    <option <?=($compensation5 === 11)? "selected": ""?> value="11">1천5백만원</option>
                    <option <?=($compensation5 === 13)? "selected": ""?> value="13">5백만원</option>
                </select>
                <div class="input-group">
                    <input type="tel" class="form-control text-right acm-compensation-fee-input" id="acm_fee5_input" value="<?=$fee5?>" data-key="fee5" <?=($compensation_type === 1)? "readonly" : ""?>/>
                    <span class="input-group-addon">만원</span>
                </div>
            </div>

            <!-- 후유장애시 -->
            <div class="col-md-4 form-group">
                <label>자손(후유장애시) 보상범위 / 면책금</label>
                <select class="workspace-select acm-compensation-range-select" id="acm_compensation6_select" data-key="compensation6" <?=($compensation_type === 1)? "disabled" : ""?> style="<?=($compensation_type === 1)? "background: rgb(235, 235, 235)" : ""?>">
                    <option <?=($compensation6 === "")? "selected": ""?> value=""></option>
                    <option <?=($compensation6 === 5)? "selected": ""?> value="5">1억원</option>
                    <option <?=($compensation6 === 8)? "selected": ""?> value="8">5천만원</option>
                    <option <?=($compensation6 === 9)? "selected": ""?> value="9">3천만원</option>
                    <option <?=($compensation6 === 10)? "selected": ""?> value="10">2천만원</option>
                    <option <?=($compensation6 === 11)? "selected": ""?> value="11">1천5백만원</option>
                    <option <?=($compensation6 === 13)? "selected": ""?> value="13">5백만원</option>
                </select>
                <div class="input-group">
                    <input type="tel" class="form-control text-right acm-compensation-fee-input" id="acm_fee6_input" value="<?=$fee6?>" data-key="fee6" <?=($compensation_type === 1)? "readonly" : ""?>/>
                    <span class="input-group-addon">만원</span>
                </div>
            </div>

            <!-- -->
            <div class="col-md-12">
                <div class="pull-right">
                    <button type="button" class="btn btn-danger" id="acm_delete_btn">삭제</button>
                    <button type="button" class="btn btn-primary" id="acm_save_btn">저장</button>
                </div>
            </div>
        </div>

        <div class="well text-center col-md-12" id="acw_compensation_info_empty_container">
            <h4>선택된 종합보험이 없습니다</h4>
        </div>
    </div>
</div>

<script>
    var mAcmInventory   = JSON.parse('<?=json_encode($compensation_inventory)?>');
    var mAcmCrudType    = '<?=$crud_type?>';
    var mAcmSelectedIdx = Number('<?=$wac_idx?>');

</script>
<script src="<?=base_url()?>static/lib/toastr/toastr.min.js"></script>
<script src="<?=asset_url()?>js/common_workspace.min.js?ver=190529"></script>
<script src="<?=asset_url()?>js/api_compensation_manage.min.js?ver=190517"></script>
