<?php
$is_it_new          = (int)$data['type'] === -1;    # true : 신규 업체
$branch_inventory   = ($is_it_new)? [] : $data['branchInventory'];
$branch_information = ($is_it_new)? [] : $data['branchInformation'];


?>
<div class="container workspace-basic-container col-md-12">
    <div class="page-header clearfix">
        <h3 class="pull-left">API 업체 설정</h3>
    </div>

    <div class="clearfix"></div>

    <!--업체-지점 추가 섹션-->
<?php
$affiliate_idx        = ($is_it_new)? 0 : $branch_information['waaIdx'];
$affiliate_branch_idx = ($is_it_new)? 0 : $branch_information['waabIdx'];
$affiliate_name       = ($is_it_new)? "" : $branch_information['waaName'];


?>
    <div class="row col-md-offset-2 col-md-8">
        <div class="col-md-6" style="padding: 1em">
            <div class="form-group">
                <label for="aainfo_name_input">업체명</label>
                <input type="text" class="form-control" id="aainfo_name_input" placeholder="가나다렌트카" value="<?=$affiliate_name?>"/>
            </div>
        </div>
        <div class="col-md-6" style="padding: 1em">
            <div class="form-group">
                <label>지점명 (옵션)</label>
                <div id="aainfo_branch_wrapper">
<?php
if ($is_it_new)
{

?>
                    <div class="input-group aainfo-branch-name-input-container" data-branch-idx="-1">
                        <input type="text" class="form-control aainfo-branch-name-input" placeholder="본사"/>
                        <span class="input-group-btn">
                            <button class="btn btn-default aainfo-branch-select-btn" type="button">선택 됨</button>
                        </span>
                    </div>
<?php
}
else
{
    foreach ($branch_inventory as $branch_inven_obj)
    {
        $branch_idx = $branch_inven_obj['waabIdx'];
        $selected_branch = $branch_idx === $affiliate_branch_idx;

        $btn_bootstrap_type = ($selected_branch)? "btn-default" : "btn-primary";
        $btn_text           = ($selected_branch)? "선택 됨" : "선택";

?>
                    <div class="input-group aainfo-branch-name-input-container" data-branch-idx="<?=$branch_idx?>">
                        <input type="text" class="form-control aainfo-branch-name-input" placeholder="본사" value="<?=$branch_inven_obj['waabName']?>"/>
                        <span class="input-group-btn">
                            <button class="btn <?=$btn_bootstrap_type?> aainfo-branch-select-btn" type="button"><?=$btn_text?></button>
                        </span>
                    </div>
<?php
    }

?>
<?php
}

?>
                </div>
<?php
if (!$is_it_new)
{

?>
                <div class="pull-right">
                    <button type="button" class="btn btn-success" onclick="aainfoBranchAddBtn()">지점 추가</button>
                </div>

<?php
}

?>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <!--지점 정보 섹션-->
    <div class="row col-md-offset-1 col-md-10" style="margin-top: 5em; margin-bottom: 10em">

        <!--API 선택 섹션-->
<?php
$api_info = ($is_it_new)? [] : $branch_information['apiInfo'];

$api = ($is_it_new)? 0 : (int)$api_info['api'];

$radio1 = ($is_it_new)? "" : (($api === 1)? "checked" : "");
$radio2 = ($is_it_new)? "" : (($api === 2)? "checked" : "");
$radio3 = ($is_it_new)? "" : (($api === 3)? "checked" : "");

?>
        <div class="form-group col-md-12">
            <label>API 선택</label>
            <div class="col-md-12" id="aainfo_api_radio_container" style="font-size: 120%">
                <label class="radio-inline"><input type="radio" class="aainfo-api-radio" name="aainfo_api_radio" value="1" <?=$radio1?>/>그림</label>&nbsp;
                <label class="radio-inline"><input type="radio" class="aainfo-api-radio" name="aainfo_api_radio" value="2" <?=$radio2?>/>인스</label>&nbsp;
                <label class="radio-inline"><input type="radio" class="aainfo-api-radio" name="aainfo_api_radio" value="3" <?=$radio3?>/>리본</label>
            </div>
        </div>

        <!-- API 정보 섹션 (리본) -->
        <div class="row col-md-12 aainfo-api-info-div aainfo-reborn-div" id="aainfo_grim_div" style="display: none">
            <div class="form-group col-md-6">
                <label>API 키</label>
                <input type="text" class="form-control aainfo-api-info-input1" data-api-info-num="1" />
            </div>

            <div class="form-group col-md-6">
                <label>인증키</label>
                <input type="text" class="form-control aainfo-api-info-input2" data-api-info-num="2" />
            </div>
        </div>

        <!-- API 정보 섹션 (인스) -->
        <div class="row col-md-12 aainfo-api-info-div aainfo-ins-div" id="aainfo_ins_div" style="display: none">
            <div class="form-group col-md-6">
                <label data-toggle="tooltip" data-placement="top" title="인스에 등록된 렌트카 회사명">렌트카회사명</label>
                <input type="text" class="form-control aainfo-api-info-input1" data-api-info-num="1" />
            </div>

            <div class="form-group col-md-6">
                <label data-toggle="tooltip" data-placement="top" title="인스에 등록한 렌트카 회사코드">렌트카회사코드</label>
                <input type="text" class="form-control aainfo-api-info-input2" data-api-info-num="2" />
            </div>

        </div>

        <!-- API 정보 섹션 (그림) -->
        <div class="row col-md-12 aainfo-api-info-div aainfo-grim-div" id="aainfo_reborn_div" style="display: none">
            <div class="form-group col-md-3">
                <label data-toggle="tooltip" data-placement="top" title="렌트카업체에 등록된 딜러측 회사명">partner</label>
                <input type="text" class="form-control aainfo-api-info-input1" data-api-info-num="1" />
            </div>

            <div class="form-group col-md-3">
                <label data-toggle="tooltip" data-placement="top" title="렌트카업체에 등록된 딜러측 접속아이디">pmid</label>
                <input type="text" class="form-control aainfo-api-info-input2" data-api-info-num="2" />
            </div>

            <div class="form-group col-md-3">
                <label data-toggle="tooltip" data-placement="top" title="렌트카업체에 등록된 딜러측 접속비밀번호">pmpw</label>
                <input type="text" class="form-control aainfo-api-info-input3" data-api-info-num="3" />
            </div>

            <div class="form-group col-md-3">
                <label data-toggle="tooltip" data-placement="top" title="업체 도메인 ex)jyrentcar.com">업체 도메인</label>
                <input type="text" class="form-control aainfo-api-info-input4" data-api-info-num="4" />
            </div>
        </div>

        <!--주소 검색 섹션-->
<?php
$main_address  = ($is_it_new)? "" : $branch_information['mainAddr'];
$sub_address   = ($is_it_new)? "" : $branch_information['subAddr'];
$addr_readonly = ($is_it_new)? "readonly" : "";

?>
        <div class="row col-md-12">
            <div class="form-group col-md-6">
                <label>업체 주소</label>
                <div class="col-md-12">
                    <div class="input-group">
                        <input type="text" class="form-control" id="aainfo_main_address_input" placeholder="주소 검색" value="<?=$main_address?>" readonly />
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" onclick="clickSearchAddressBtn()"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label>&nbsp;</label>
                <div class="col-md-12">
                    <input type="text" class="form-control" id="aainfo_sub_address_input" value="<?=$sub_address?>" placeholder="상세 주소 입력" <?=$addr_readonly?> />
                </div>
            </div>
        </div>

        <!--연락처/최대 몇일 후 예약까지 잡을 수 있는지-->
<?php
$tel        = ($is_it_new)? "" : $branch_information['tel'];
$max_reserv = ($is_it_new)? "" : $branch_information['maxReserv'];
$tel050     = ($is_it_new)? "" : $branch_information['tel050'];
$enter_date = ($is_it_new)? "" : $branch_information['enterDate'];

$is_matching  = ($tel050 === '')? '0' : '1';
$matching_txt = ($is_matching === '0')? '050번호매칭' : '050번호매칭 완료';

?>
        <div class="row col-md-12">
            <div class="form-group col-md-6">
                <label>업체 연락처</label>
                <div class="col-md-12">
                    <div class="input-group">
                        <input type="tel" class="form-control" id="aainfo_tel_input" value="<?=$tel?>" placeholder="연락처 입력" />
                        <span class="input-group-btn">
                        <button type="button" class="btn btn-default" onclick="click050MatchingBtn(this)" data-is-matching="<?=$is_matching?>" data-matching-tel="<?=$tel050?>" data-toggle="tooltip" data-placement="top" title="<?=$tel050?>"><?=$matching_txt?></button>
                    </span>
                    </div>
                </div>


            </div>

            <div class="form-group col-md-2">
                <label>최대 몇일 후 까지 예약 허용&nbsp;<span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="top" title="따로 설정하지 않으려면 -1 을 입력하세요."></span></label>
                <div class="col-md-12">
                    <div class="input-group">
                        <input type="tel" class="form-control text-right" id="aainfo_max_reserv_input" value="<?=$max_reserv?>" placeholder="50" />
                        <span class="input-group-addon">일</span>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-4">
                <label>입점일</label>
                <div class="col-md-12">
                    <input type="date" class="form-control" min="2017-01-01" max="9999-12-31" id="aainfo_enter_date_input" value="<?=$enter_date?>" />
                </div>
            </div>
        </div>

        <!--카모아영업시간/단기판매 시작시간-->
<?php
$open_time_hour    = ($is_it_new)? "" : substr($branch_information['openTime'], 0, 2);
$open_time_minute  = ($is_it_new)? "" : substr($branch_information['openTime'], 3, 2);
$close_time_hour   = ($is_it_new)? "" : substr($branch_information['closeTime'], 0, 2);
$close_time_minute = ($is_it_new)? "" : substr($branch_information['closeTime'], 3, 2);
$sale_start_hour   = ($is_it_new)? "" : $branch_information['saleStartHour'];

?>
        <div class="row col-md-12">
            <div class="form-group col-md-6">
                <label>카모아 영업시간</label>
                <div class="col-md-12" style="display: flex">
                    <div style="flex: 20; display: flex">
                        <div style="flex: 20">
                            <select class="workspace-select" id="aainfo_open_worktime_hour_select">
                                <option value=""></option>
                                <option value="00">0</option>
                                <option value="01">1</option>
                                <option value="02">2</option>
                                <option value="03">3</option>
                                <option value="04">4</option>
                                <option value="05">5</option>
                                <option value="06">6</option>
                                <option value="07">7</option>
                                <option value="08">8</option>
                                <option value="09">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                            </select>
                        </div>
                        <div style="flex: 1; align-self: center">&nbsp;:&nbsp;</div>
                        <div style="flex: 20">
                            <select class="workspace-select" id="aainfo_open_worktime_minute_select">
                                <option value=""></option>
                                <option value="00">00</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="40">40</option>
                                <option value="50">50</option>
                            </select>
                        </div>
                    </div>
                    <div style="flex: 1; align-self: center">&nbsp;~</div>
                    <div style="flex: 20; display: flex">
                        <div style="flex: 20">
                            <select class="workspace-select" id="aainfo_close_worktime_hour_select">
                                <option value=""></option>
                                <option value="00">0</option>
                                <option value="01">1</option>
                                <option value="02">2</option>
                                <option value="03">3</option>
                                <option value="04">4</option>
                                <option value="05">5</option>
                                <option value="06">6</option>
                                <option value="07">7</option>
                                <option value="08">8</option>
                                <option value="09">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                            </select>
                        </div>
                        <div style="flex: 1; align-self: center">&nbsp;:&nbsp;</div>
                        <div style="flex: 20">
                            <select class="workspace-select" id="aainfo_close_worktime_minute_select">
                                <option value=""></option>
                                <option value="00">00</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="40">40</option>
                                <option value="50">50</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label>n시간 후부터 단기판매 시간</label>
                <div class="col-md-12">
                    <div class="input-group">
                        <input type="tel" class="form-control text-right" id="aainfo_sale_start_input" value="<?=$sale_start_hour?>" placeholder="24" />
                        <span class="input-group-addon">시간</span>
                    </div>
                </div>
            </div>
        </div>

        <!--셔틀정보-->
<?php
$shuttle_name     = ($is_it_new)? "" : $branch_information['stName'];
$shuttle_area     = ($is_it_new)? "" : $branch_information['stArea'];
$shuttle_area_num = ($is_it_new)? "" : $branch_information['stAreaNum'];
$shuttle_race     = ($is_it_new)? "" : $branch_information['stRace'];
$shuttle_spend    = ($is_it_new)? "" : $branch_information['stSpend'];

?>
        <div class="row col-md-12">
            <div class="form-group col-md-3">
                <label>셔틀명</label>
                <div class="col-md-12">
                    <input type="text" class="form-control" id="aainfo_shuttle_name_input" value="<?=$shuttle_name?>" placeholder="가나다렌트카" />
                </div>
            </div>

            <div class="form-group col-md-2">
                <label>셔틀구역</label>
                <div class="col-md-12">
                    <div class="input-group">
                        <input type="tel" class="form-control text-right" id="aainfo_shuttle_area_input" value="<?=$shuttle_area?>" placeholder="5" />
                        <span class="input-group-addon">구역</span>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-2">
                <label>셔틀구역번호</label>
                <div class="col-md-12">
                    <div class="input-group">
                        <input type="tel" class="form-control text-right" id="aainfo_shuttle_area_num_input" value="<?=$shuttle_area_num?>" placeholder="3" />
                        <span class="input-group-addon">번</span>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-3">
                <label>운행간격</label>
                <div class="col-md-12">
                    <input type="text" class="form-control text-right" id="aainfo_shuttle_race_input" value="<?=$shuttle_race?>" placeholder="15~20" />

                </div>
            </div>

            <div class="form-group col-md-2">
                <label>소요시간</label>
                <div class="col-md-12">
                    <div class="input-group">
                        <input type="tel" class="form-control text-right" id="aainfo_shuttle_spend_input" value="<?=$shuttle_spend?>" placeholder="10" />
                        <span class="input-group-addon">분</span>
                    </div>
                </div>
            </div>
        </div>
<?php
$service_string   = ($is_it_new)? "" : $branch_information['serviceString'];
$delivery_string  = ($is_it_new)? "" : $branch_information['deliveryString'];
$special_location = ($is_it_new)? "" : $branch_information['specialLocation'];

?>
        <div class="row col-md-12">
            <div class="form-group col-md-4">
                <label>서비스 지역&nbsp;&nbsp;<button class="btn btn-xs btn-default" style="margin-top: -.5em" id="aainfo_sale_location_btn">서비스지역 설정하기</button></label>

            </div>

            <div class="form-group col-md-4">
                <label>딜리버리 지역&nbsp;&nbsp;<button class="btn btn-xs btn-default" style="margin-top: -.5em" id="aainfo_sale_delivery_btn">딜리버리지역 설정하기</button></label>

            </div>

            <div class="form-group col-md-4">
                <label>주요지역&nbsp;<button class="btn btn-xs btn-default" style="margin-top: -.5em" id="aainfo_special_location_btn">주요지역 설정하기</button></label>
            </div>
        </div>




<?php
$main_src  = ($is_it_new)? "" : $branch_information['logo'];
$main_size = ($is_it_new)? 0 : $branch_information['logoSize'];
$sub_imgs  = ($is_it_new)? 0 : $branch_information['subImgs'];

?>

        <div class="row col-md-12">
            <!--메인이미지-->

            <div class="form-group col-md-6">
                <label>메인이미지</label>
                <div class="dropzone" id="aainfo_main_dropzone_div">
                    <div class="dz-default dz-message" data-dz-message>
                        <span id="aainfo_main_dropzone_default_msg">여기에 드래그 앤 드랍하세요</span>
                    </div>
                </div>

            </div>


            <div class="col-md-6">
                <div class="form-group col-md-12">
                    <label>서브이미지</label>
                    <div class="dropzone" id="aainfo_sub_dropzone_div">
                        <div class="dz-default dz-message" data-dz-message>
                            <span id="aainfo_sub_dropzone_default_msg">여기에 드래그 앤 드랍하세요</span>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <!--알림톡 받아볼 명단-->
<?php
$display_member_add_btn_container = ($is_it_new)? "display: none;" : "";
$display_member_none_container    = ($is_it_new)? "" : "display: none;";
$intro_msg                        = ($is_it_new)? "" : $branch_information['intro'];
$uniqueness                       = ($is_it_new)? "" : $branch_information['uniqueness'];

$biztalk_inventory = ($is_it_new)? json_encode([]) : json_encode($branch_information['biztalkInven']);

?>
        <div class="row col-md-12">
            <div class="col-md-6">
                <div class="form-group col-md-12">
                    <label>업체한마디 </label>
                    <div class="col-md-12">
                        <textarea class="form-control" id="aainfo_affi_intro_textarea" style="resize: vertical"><?=$intro_msg?></textarea>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label>업체특이사항</label>
                    <div class="col-md-12">
                        <textarea class="form-control" id="aainfo_affi_uniqueness_textarea" style="resize: vertical"><?=$uniqueness?></textarea>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6">
                <label>알림톡 명단</label>
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr class="info">
                                <th class="text-center col-md-3">이름 (옵션)</th>
                                <th class="text-center col-md-7">전화번호</th>
                                <th class="text-center col-md-2"></th>
                            </tr>
                        </thead>
                        <tbody id="aainfo_biztalk_member_tbody"></tbody>
                    </table>

                    <div class="pull-right" id="aainfo_biztalk_member_add_btn_container" style="<?=$display_member_add_btn_container?>">
                        <button type="button" class="btn btn-sm btn-success" onclick="appendAainfoBiztalkMemberRow(0, null)">명단 추가</button>
                    </div>

                    <div class="text-center" id="aainfo_biztalk_member_none_container" style="<?=$display_member_none_container?>">
                        <h4>등록된 명단이 없습니다</h4>
                        <button type="button" class="btn btn-success" onclick="appendAainfoBiztalkMemberRow(-1, null)">명단 추가</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row col-md-12">
            <hr>
        </div>

        <div class="row col-md-12">
            <div class="form-group col-md-offset-10 col-md-2 text-right">
                <button type="button" class="btn btn-lg btn-default" onclick="procAainfoIntoInventory()">목록</button>
                <button type="button" class="btn btn-lg btn-danger" id="aainfo_delete_btn" onclick="procAainfoDelete()">삭제</button>
                <button type="button" class="btn btn-lg btn-primary" onclick="procAainfoSave()">저장</button>
            </div>
        </div>

    </div>
</div>

<div id="aainfo_search_address_modal_container"></div>
<div id="aainfo_modal050_container"></div>
<div id="aainfo_sale_location_modal_container"></div>
<div id="aainfo_special_location_modal_container"></div>

<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=6729d81d67bf5731f9c618cbe31cf00a&libraries=services"></script>

<script>
    var assetUrl = '<?=asset_url()?>';

    var mAainfoServerOpenHour = '<?=$open_time_hour?>';
    var mAainfoServerOpenMinute = '<?=$open_time_minute?>';
    var mAainfoServerCloseHour = '<?=$close_time_hour?>';
    var mAainfoServerCloseMinute = '<?=$close_time_minute?>';

    var mAainfoIdx = (Boolean(<?=$is_it_new?>))? -1 : Number(<?=$affiliate_idx?>);                 // 업체 아이디값
    var mAainfoBranchIdx = (Boolean(<?=$is_it_new?>))? -1 : Number(<?=$affiliate_branch_idx?>);    // 지점 아이디값
    var mAainfoApiInfo = JSON.parse('<?=json_encode($api_info)?>');
    var mAainfoBiztalkInventory = JSON.parse('<?=$biztalk_inventory?>');                           // 알림톡 명단
    var mAainfoRecommendNum = (Boolean(<?=$is_it_new?>))? "" : '<?=$branch_information['recommendNum']?>';
    var mAainfoMainOrigSaveName = '<?=$main_src?>';
    var mAainfoMainOrigSize = Number('<?=$main_size?>');
    var mAainfoSubOrigImgs = JSON.parse('<?=json_encode($sub_imgs)?>');
    var mAainfoServiceString = "<?=$service_string?>";
    var mAainfoDeliveryString = "<?=$delivery_string?>";
    var mAainfoSpecialLocationString = "<?=$special_location?>";

</script>
<script src="<?=base_url()?>static/lib/toastr/toastr.min.js"></script>
<script src="<?=asset_url()?>js/image-compressor.min.js"></script>
<script src="<?=asset_url()?>js/commonCompressImg.min.js"></script>
<script src="<?=base_url()?>static/lib/dropzone/dropzone.js"></script>
<script src="<?=asset_url()?>js/common_workspace.min.js?ver=190529"></script>
<script src="<?=asset_url()?>modal/js/common_modal.min.js"></script>
<script src="<?=asset_url()?>modal/js/search_address_modal.min.js"></script>
<script src="<?=asset_url()?>modal/js/matching_050_modal.min.js?ver=190611"></script>
<script src="<?=asset_url()?>modal/js/sale_location_modal.min.js"></script>
<script src="<?=asset_url()?>modal/js/special_location_modal.min.js"></script>
<script src="<?=asset_url()?>js/api_affiliate_information.min.js?ver=190718"></script>