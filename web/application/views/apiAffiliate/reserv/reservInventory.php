<?php
$inventory  = $data['inventory'];
$pagination = $data['pagination'];

$display_bool = (count($inventory) === 0)? false : true;
?>

<div class="container workspace-basic-container col-md-12">
    <div class="page-header clearfix">
        <h3 class="pull-left">API 예약목록</h3>
    </div>

    <div class="clearfix"></div>
<?php
$stateu1 = ((int)$this->input->get('stateu1') === 0)? "" : "checked";
$state0  = ((int)$this->input->get('state0') === 0)? "" : "checked";
$state1  = ((int)$this->input->get('state1') === 0)? "" : "checked";
$state10 = ((int)$this->input->get('state10') === 0)? "" : "checked";
$state11 = ((int)$this->input->get('state11') === 0)? "" : "checked";

if ($stateu1 === "" && $state0 === "" && $state1 === "" && $state10 === "" && $state11 === "")
{
    $state0  = "checked";
    $state1  = "checked";
    $state10 = "checked";

}


?>
    <div class="col-md-12">
        <label>검색기준</label>
        <div class="form-group">
            <input type="checkbox" name="ri_search_state_checkbox" value="stateu1" id="ri_stateu1" <?=$stateu1?>>&nbsp;<label for="ri_stateu1">카드결제전</label>&nbsp;&nbsp;
            <input type="checkbox" name="ri_search_state_checkbox" value="state0" id="ri_state0" <?=$state0?>>&nbsp;<label for="ri_state0">예약완료</label>&nbsp;&nbsp;
            <input type="checkbox" name="ri_search_state_checkbox" value="state1" id="ri_state1" <?=$state1?>>&nbsp;<label for="ri_state1">대여중</label>&nbsp;&nbsp;
            <input type="checkbox" name="ri_search_state_checkbox" value="state10" id="ri_state10" <?=$state10?>>&nbsp;<label for="ri_state10">반납완료</label>&nbsp;&nbsp;
            <input type="checkbox" name="ri_search_state_checkbox" value="state11" id="ri_state11" <?=$state11?>>&nbsp;<label for="ri_state11">취소</label>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-2">
            <select class="workspace-select" id="ri_search_param_select">
                <option value="0">검색조건</option>
                <option value="1">예약번호</option>
                <option value="2">운전자명</option>
                <option value="3">운전자연락처</option>
            </select>
        </div>

        <div class="col-md-10">
            <div class="input-group">
                <input type="text" class="form-control" id="ri_search_input" placeholder="검색조건 입력"/>
                <span class="input-group-btn">
                    <button type="button" class="btn btn-default" onclick="searchRi()"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
        </div>
    </div>
 <?php
 if ($display_bool)
 {

 ?>
    <div class="col-md-12 table-responsive" style="margin-top: 1em">
        <table class="table table-bordered">
            <thead>
                <tr class="info">
                    <th class="text-center">결제상태</th>
                    <th class="text-center">차량 대여정보</th>
                    <th class="text-center">운전자정보</th>
                    <th class="text-center">자차정보</th>
                    <th class="text-center">결제정보</th>
                    <th class="text-center">결제일</th>
                    <th class="text-center">예약상세정보/최신화</th>
                </tr>
            </thead>
            <tbody>
<?php
    $days = ["일", "월", "화", "수", "목", "금", "토"];

    foreach ($inventory as $obj)
    {
        $api                 = (int)$obj->api;
        $state               = (int)$obj->state;
        $reserv_idx          = $obj->reservationIdx;
        $api_label           = get_ri_api_label_class($api);
        $reserv_label        = ($state === 11)? get_ri_label_class(5) : (($state === -1)? get_ri_label_class(0) : get_ri_label_class(1));
        $start_day           = $days[date("w", strtotime($obj->rentalStart))];
        $end_day             = $days[date("w", strtotime($obj->rentalEnd))];
        $rental_diff         = date_diff(new DateTime($obj->rentalStart), new DateTime($obj->rentalEnd));
        $user_grade          = (int)$obj->userGrade;
        $call_reserv         = $obj->callReserv;
        $reserv_type         = ($call_reserv === "y")?
            "<span class='label label-warning'>전화예약 (".$obj->callReserverName.")</span><br/>" :
            (($user_grade === 1)? "" : "<span class='label label-danger'>비회원예약</span><br/>");
        $kakao_id            = $obj->carmoreKakaoId;
        $kakao_id_span       = ($kakao_id === "")? "" : "<span>카카오 아이디 : <a href='/carmore/AppUser?mem_stats=y&sp=kakaoid&sv=".$kakao_id."'>".$kakao_id."</a></span><br/>";
        $carmore_use_span    = ($kakao_id === "" || $call_reserv === "y")? "" : "<br/><span>카모아 누적 이용횟수 : ".$obj->carmoreUseCnt."회 </span><br/>";
        $carmore_review_span = ($kakao_id === "" || $call_reserv === "y")? "" : "<span>카모아 리뷰 작성횟수 : ".$obj->carmoreReviewCnt."회 </span><br/>";
        $view_payment        = ($call_reserv === "y")? $obj->usePoint : $obj->payment;
        $view_use_point      = ($call_reserv === "y")? 0 : $obj->usePoint;
        $coupon_name         = $obj->useCouponName;
        $coupon_string       = ($coupon_name === "")? "" : "(".$coupon_name.")";
        $discount_value      = (int)$view_use_point+(int)$obj->useCouponValue;
        $affiliate_name      = trim($obj->waaName." ".$obj->waabName);
        $pay_day             = ($obj->payDate === "")? "" : $days[date("w", strtotime($obj->payDate))];
        $pay_date_string     = ($pay_day === "")? "" : date_format(date_create($obj->payDate), "y.m.d (".$pay_day.") H:i");
        $update_day          = ($obj->recentUpdateDate === "")? "" : $days[date("w", strtotime($obj->recentUpdateDate))];
        $update_date_string  = ($update_day === "")? "" : "<br/><span>최근갱신일<br>".date_format(date_create($obj->recentUpdateDate), "y.m.d (".$update_day.") H:i")."</span>";
        $reserv_content_btn  = ($pay_day === "")? "" : "<button type='button' class='btn btn-default btn-sm ri-data-row-item-btn'>예약상세정보 보기 및 최신화</button><br/>";

        $diff_string = $rental_diff->d."일";
        if ($rental_diff->h !== 0)
            $diff_string .= " ".$rental_diff->h."시간";

        if ($rental_diff->i !== 0)
            $diff_string .= " ".$rental_diff->i."분";

?>
                <tr class="ri-data-row" data-api="<?=$api?>" data-api-reserv-code="<?=$obj->reservApiCode?>" data-reserv-idx="<?=$reserv_idx?>" data-waab-idx="<?=$obj->waabIdx?>" data-wari-idx="<?=$obj->wariIdx?>">
                    <td class="text-center">
                        <span class="label label-<?=$reserv_label?>"><?=$obj->stateString?></span>
                    </td>
                    <td>
                        <span style="font-size:110%; font-weight: bold"><?=$obj->carName?></span><br/>
                        <span><?=$obj->carAge?> / <?=$obj->carFuel?></span><br/>
                        <br/>
                        <span>예약번호 : <b><?=$reserv_idx?></b></span><br/>
                        <span>예약업체 : <?=$affiliate_name?></span>&nbsp;<span class="label label-<?=$api_label?>"><?=$obj->apiString?></span><br/>
                        <span><?=date_format(date_create($obj->rentalStart), "y.m.d (".$start_day.") H:i")?><br/>
                            ~ <?=date_format(date_create($obj->rentalEnd), "y.m.d (".$end_day.") H:i")?></span>&nbsp;<span><small>[<?=$diff_string?>]</small></span>
                    </td>
                    <td>
                        <?=$reserv_type?>
                        <span style="font-size: 110%; font-weight: bold"><?=$obj->driverName?><br>(<?=$obj->driverPhone?>)</span><br/>
                        <?=$kakao_id_span?>
                        <span>생년월일 (성별) : <?=$obj->driverBirthday?> (<?=$obj->driverGender?>)</span><br/>
                        <?=$carmore_use_span?>
                        <?=$carmore_review_span?>
                    </td>
                    <td>
                        가입자차 : <strong><?=$obj->cdwTypeName?></strong><br>
                        (보상한도 : <?=$obj->cdwTypeCompensation?>만원, 면책금 : <?=$obj->deductible?>)
                    </td>
                    <td>
                        <span style="font-size: 120%">카드결제금액 : <b><?=number_format($view_payment)?></b></span><br/>
                        <br/>
                        <span>원금 : <?=number_format($obj->principal)?></span>원<br/>
                        <span>렌트비 : <?=number_format($obj->rentalCost)?>원, 보험비 : <?=number_format($obj->cdwCost)?>원, 딜리버리비 : <?=number_format($obj->delivCost)?>원</span><br>
                        <span>할인정보 : <span style="color: red"><?=number_format($discount_value)?></span>원 (포인트 : <?=number_format($view_use_point)?>원, 쿠폰 <?=$coupon_string?>: <?=number_format($obj->useCouponValue)?>원)</span>
                    </td>
                    <td class="text-center"><?=$pay_date_string?></td>
                    <td class="text-center">
                        <?=$reserv_content_btn?>
                        <?=$update_date_string?>
                    </td>
                </tr>
<?php
    }

?>
            </tbody>
        </table>
    </div>
    <div class="text-center"><?=$pagination?></div>
<?php
}
else
{

?>
    <div class="col-md-12 text-center" style="margin-top: 1em; font-weight: bold; font-size: 120%">검색조건에 일치하는 예약목록이 존재하지 않습니다.</div>
<?php
}

?>

</div>

<script src="<?=asset_url()?>js/reserv_inventory.min.js"></script>

<?php
function get_ri_api_label_class($api)
{
    switch ((int)$api)
    {
        case 1:
            return get_ri_label_class(1);
        case 2:
            return get_ri_label_class(2);
        case 3:
            return get_ri_label_class(3);
        default:
            return get_ri_label_class(0);
    }
}

function get_ri_label_class($flag)
{
    switch ((int)$flag)
    {
        case 0:
            return "default";
        case 1:
            return "primary";
        case 2:
            return "success";
        case 3:
            return "info";
        case 4:
            return "warning";
        case 5:
            return "danger";
        default:
            return "default";
    }
}

?>