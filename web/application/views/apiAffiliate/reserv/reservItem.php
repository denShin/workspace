<?php
$data_ret = $data['retInfo'];

$reserv_info = $data_ret->info;

$affi_info   = $reserv_info->affiInfo;
$rental_info = $reserv_info->rentalInfo;
$wac_info    = $reserv_info->wacInfo;

$api_reserv_code = $rental_info->apiReservCode;
$waab_idx        = $affi_info->waabIdx;
$api_code        = (int)$affi_info->apiCode;
$moid            = $rental_info->reservationIdx;

?>

<div class="container workspace-basic-container col-md-12">
    <div class="page-header clearfix">
        <h3 class="pull-left">예약정보</h3>
    </div>

    <div class="clearfix"></div>

    <div class="col-md-offset-2 col-md-8">

        <h4>
            <div class="col-md-2">&raquo; 예약번호 : <?=$moid?></div>
            <div class="col-md-offset-8 col-md-2">예약상태 : <?=$rental_info->state?></div>

        </h4>

<?php
$days = ["일", "월", "화", "수", "목", "금", "토"];

$affi_tel        = $affi_info->tel;
$biztalk_members = $affi_info->biztalkMembers;

$coupon_value = (int)$rental_info->useCouponValue;
$use_point    = (int)$rental_info->usePoint;
$discount     = $coupon_value+$use_point;
$coupon_name  = ($rental_info->useCouponIdx === 0)? "" : "(".$rental_info->useCouponName.")";

?>
        <div class="col-md-12">
            <h4>&raquo; 업체정보</h4>
            <div class="panel-group">
                <div class="panel panel-default" style="cursor: pointer">
                    <div class="panel-heading" data-toggle="collapse" href="#ritem_affi_info"><?=$affi_info->name?> (<?=$affi_tel?>)</div>
                </div>
                <div class="panel-collapse collapse" id="ritem_affi_info">
                    <div class="panel-body">

                        <div class="col-md-12 table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td class="active text-center col-md-2" style="font-weight: bold">API</td>
                                        <td class="col-md-2"><?=$affi_info->api?></td>
                                        <td class="active text-center col-md-2" style="font-weight: bold">업체주소</td>
                                        <td class="col-md-6" colspan="3"><?=$affi_info->address?></td>
                                    </tr>
                                    <tr>
                                        <td class="active text-center" style="font-weight: bold">업체 연락처</td>
                                        <td><?=$affi_tel?></td>
                                        <td class="active text-center" style="font-weight: bold">업체 050번호</td>
                                        <td class="col-md-2">추후개발</td>
                                        <td class="active text-center col-md-2" style="font-weight: bold">영업시간</td>
                                        <td class="col-md-2"><?=substr($affi_info->openTime, 0, 5)?> ~ <?=substr($affi_info->closeTime, 0, 5)?></td>
                                    </tr>
                                </tbody>
                            </table>


                        </div>

                        <div class="col-md-12 table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="active">
                                        <th class="text-center" colspan="3">알림톡명단</th>
                                    </tr>
                                    <tr class="active">
                                        <th class="text-center col-md-2">이름</th>
                                        <th class="text-center col-md-8">연락처</th>
                                        <th class="text-center col-md-2">전송내역</th>
                                    </tr>
                                </thead>
                                <tbody>
<?php
foreach ($biztalk_members as $mem)
{
    $tel = $mem['tel'];

    $pure_tel = preg_replace('/[^0-9]/', '', $tel);
?>
                                    <tr>
                                        <td class="text-center"><?=$mem['name']?></td>
                                        <td class="text-center"><?=$tel?></td>
                                        <td class="text-center"><button type="button" class="btn btn-xs btn-success" id="rit_biztalk_send_history_btn"
                                                                        data-moid="<?=$moid?>" data-rec-num="<?=$pure_tel?>">전송내역확인</button></td>
                                    </tr>
<?php
}

?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>

        </div>
<?php
$rental_start = $rental_info->rentalStart;
$rental_end   = $rental_info->rentalEnd;

$start_day = $days[date("w", strtotime($rental_start))];
$end_day   = $days[date("w", strtotime($rental_end))];

?>
        <div class="col-md-12">
            <h4>&raquo; 예약정보</h4>
            <div class="col-md-12 table-responsive">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td class="col-md-1 active" style="font-weight: bold">대여기간</td>
                            <td class="col-md-11" colspan="7"><?=date_format(date_create($rental_start), "y.m.d (".$start_day.") H:i")?> ~ <?=date_format(date_create($rental_end), "y.m.d (".$end_day.") H:i")?></td>
                        </tr>
                        <tr>
                            <td class="col-md-1 active" style="font-weight: bold">배차장소</td>
                            <td class="col-md-5" colspan="3"><?=$rental_info->pikcupAddress?></td>
                            <td class="col-md-1 active" style="font-weight: bold">회차장소</td>
                            <td class="col-md-5" colspan="3"><?=$rental_info->returnAddress?></td>
                        </tr>
                        <tr>
                            <td class="col-md-1 active" style="font-weight: bold">차종명</td>
                            <td class="col-md-2"><?=$rental_info->carName?></td>
                            <td class="col-md-1 active" style="font-weight: bold">제한나이</td>
                            <td class="col-md-2"><?=$rental_info->compensationAge?></td>
                            <td class="col-md-1 active" style="font-weight: bold">차량연식</td>
                            <td class="col-md-2"><?=$rental_info->carAge?></td>
                            <td class="col-md-1 active" style="font-weight: bold">연료</td>
                            <td class="col-md-2"><?=$rental_info->carFuel?></td>
                        </tr>
                        <tr>
                            <td class="active" style="font-weight: bold">운전자명</td>
                            <td><?=$rental_info->driverName?></td>
                            <td class="active" style="font-weight: bold">연락처</td>
                            <td><?=$rental_info->driverPhone?></td>
                            <td class="active" style="font-weight: bold">생년월일</td>
                            <td><?=$rental_info->driverBirthday?></td>
                            <td class="active" style="font-weight: bold">성별</td>
                            <td><?=$rental_info->driverGender?></td>
                        </tr>
                        <tr>
                            <td class="active" style="font-weight: bold">결제금액</td>
                            <td style="font-weight: bold; font-size: 110%"><U><?=number_format($rental_info->payment)?>원</U></td>
                            <td class="active" style="font-weight: bold">원금</td>
                            <td><?=number_format($rental_info->principal)?>원 </td>
                            <td class="active" style="font-weight: bold">할인금액</td>
                            <td colspan="3"><span style="color: red"><?=number_format($discount)?></span>원 (포인트 : <span style="color: red"><?=number_format($use_point)?></span>원, 쿠폰<?=$coupon_name?>: <span style="color: red"><?=$coupon_value?></span>원)</td>
                        </tr>
                        <tr>
                            <td class="active" style="font-weight: bold">대여료</td>
                            <td><?=number_format($rental_info->rentalCost)?>원</td>
                            <td class="active" style="font-weight: bold">자차료</td>
                            <td><?=number_format($rental_info->cdwCost)?>원 (<?=$rental_info->cdwName?>)</td>
                            <td class="active" style="font-weight: bold">딜리버리료</td>
                            <td colspan="3"><?=number_format($rental_info->delivCost)?>원</td>
                        </tr>
                        <tr>
                            <td class="active" style="font-weight: bold">메모</td>
                            <td colspan="7"><?=$rental_info->memo?></td>
                        </tr>
<?php
if ($api_code === 2)
{

?>
                        <tr>
                            <td class="active" style="font-weight: bold">예약변경<br>가능시간<br><small>(인스)</small></td>
                            <td colspan="7" style="vertical-align: middle"><?=($rental_info->etcDate1 === "0000-00-00 00:00:00")? "" : substr($rental_info->etcDate1, 0, 16)?></td>
                        </tr>
<?php
}

?>
                    </tbody>
                </table>

            </div>

        </div>

        <div class="col-md-12">
            <h4>&raquo; 종합보험정보</h4>
<?php
$type = (int)$wac_info->type;

if ($type === 1)
{

?>
            <div class="panel panel-default">
                <div class="panel-heading"><?=$wac_info->name?></div>
            </div>
<?php
}
else
{

?>
            <div class="panel-group">
                <div class="panel panel-default" style="cursor: pointer">
                    <div class="panel-heading" data-toggle="collapse" href="#rit_compensation"><?=$wac_info->name?></div>
                </div>
                <div class="panel-collapse collapse" id="rit_compensation">
                    <div class="panel-body">
                        <!-- 대인/대물 파트 -->
                        <!-- 대인1 -->
                        <div class="col-md-4 form-group">
                            <label>대인 1 보상범위 / 면책금</label>
                            <select class="workspace-select" disabled>
                                <option <?=((int)$wac_info->compensation1 === "")? "selected": ""?> value=""></option>
                                <option <?=((int)$wac_info->compensation1 === 1)? "selected": ""?> value="1">무한</option>
                                <option <?=((int)$wac_info->compensation1 === 2)? "selected": ""?> value="2">자배법 시행령에서 정한 금액</option>
                                <option <?=((int)$wac_info->compensation1 === 5)? "selected": ""?> value="5">1억원</option>
                            </select>
                            <div class="input-group">
                                <input type="tel" class="form-control text-right" value="<?=$wac_info->fee1?>" style="background: white" readonly/>
                                <span class="input-group-addon">만원</span>
                            </div>
                        </div>

                        <!-- 대인2 -->
                        <div class="col-md-4 form-group">
                            <label>대인 2 보상범위 / 면책금</label>
                            <select class="workspace-select" disabled>
                                <option <?=((int)$wac_info->compensation2 === "")? "selected": ""?> value=""></option>
                                <option <?=((int)$wac_info->compensation2 === 1)? "selected": ""?> value="1">무한</option>
                                <option <?=((int)$wac_info->compensation2 === 3)? "selected": ""?> value="3">3억원</option>
                                <option <?=((int)$wac_info->compensation2 === 4)? "selected": ""?> value="4">2억원</option>
                                <option <?=((int)$wac_info->compensation2 === 5)? "selected": ""?> value="5">1억원</option>
                                <option <?=((int)$wac_info->compensation2 === 8)? "selected": ""?> value="8">5천만원</option>
                            </select>
                            <div class="input-group">
                                <input type="tel" class="form-control text-right" value="<?=$wac_info->fee2?>" style="background: white" readonly/>
                                <span class="input-group-addon">만원</span>
                            </div>
                        </div>

                        <!-- 대물 -->
                        <div class="col-md-4 form-group">
                            <label>대물 보상범위 / 면책금</label>
                            <select class="workspace-select" disabled>
                                <option <?=((int)$wac_info->compensation3 === "")? "selected": ""?> value=""></option>
                                <option <?=((int)$wac_info->compensation3 === 3)? "selected": ""?> value="3">3억원</option>
                                <option <?=((int)$wac_info->compensation3 === 4)? "selected": ""?> value="4">2억원</option>
                                <option <?=((int)$wac_info->compensation3 === 5)? "selected": ""?> value="5">1억원</option>
                                <option <?=((int)$wac_info->compensation3 === 6)? "selected": ""?> value="6">8천만원</option>
                                <option <?=((int)$wac_info->compensation3 === 7)? "selected": ""?> value="7">7천만원</option>
                                <option <?=((int)$wac_info->compensation3 === 8)? "selected": ""?> value="8">5천만원</option>
                                <option <?=((int)$wac_info->compensation3 === 9)? "selected": ""?> value="9">3천만원</option>
                                <option <?=((int)$wac_info->compensation3 === 10)? "selected": ""?> value="10">2천만원</option>
                                <option <?=((int)$wac_info->compensation3 === 11)? "selected": ""?> value="11">1천5백만원</option>
                                <option <?=((int)$wac_info->compensation3 === 12)? "selected": ""?> value="12">1천만원</option>
                            </select>
                            <div class="input-group">
                                <input type="tel" class="form-control text-right" value="<?=$wac_info->fee3?>" style="background: white" readonly/>
                                <span class="input-group-addon">만원</span>
                            </div>
                        </div>

                        <!-- 자손 파트 -->
                        <!-- 사망시 -->
                        <div class="col-md-4 form-group">
                            <label>자손(사망시) 보상범위 / 면책금</label>
                            <select class="workspace-select" disabled>
                                <option <?=((int)$wac_info->compensation4 === "")? "selected": ""?> value=""></option>
                                <option <?=((int)$wac_info->compensation4 === 5)? "selected": ""?> value="5">1억원</option>
                                <option <?=((int)$wac_info->compensation4 === 8)? "selected": ""?> value="8">5천만원</option>
                                <option <?=((int)$wac_info->compensation4 === 9)? "selected": ""?> value="9">3천만원</option>
                                <option <?=((int)$wac_info->compensation4 === 10)? "selected": ""?> value="10">2천만원</option>
                                <option <?=((int)$wac_info->compensation4 === 11)? "selected": ""?> value="11">1천5백만원</option>
                            </select>
                            <div class="input-group">
                                <input type="tel" class="form-control text-right" value="<?=$wac_info->fee4?>" style="background: white" readonly/>
                                <span class="input-group-addon">만원</span>
                            </div>
                        </div>

                        <!-- 부상시 -->
                        <div class="col-md-4 form-group">
                            <label>자손(부상시) 보상범위 / 면책금</label>
                            <select class="workspace-select" disabled>
                                <option <?=((int)$wac_info->compensation5 === "")? "selected": ""?> value=""></option>
                                <option <?=((int)$wac_info->compensation5 === 5)? "selected": ""?> value="5">1억원</option>
                                <option <?=((int)$wac_info->compensation5 === 8)? "selected": ""?> value="8">5천만원</option>
                                <option <?=((int)$wac_info->compensation5 === 9)? "selected": ""?> value="9">3천만원</option>
                                <option <?=((int)$wac_info->compensation5 === 10)? "selected": ""?> value="10">2천만원</option>
                                <option <?=((int)$wac_info->compensation5 === 11)? "selected": ""?> value="11">1천5백만원</option>
                                <option <?=((int)$wac_info->compensation5 === 13)? "selected": ""?> value="13">5백만원</option>
                            </select>
                            <div class="input-group">
                                <input type="tel" class="form-control text-right" value="<?=$wac_info->fee5?>" style="background: white" readonly/>
                                <span class="input-group-addon">만원</span>
                            </div>
                        </div>

                        <!-- 후유장애시 -->
                        <div class="col-md-4 form-group">
                            <label>자손(후유장애시) 보상범위 / 면책금</label>
                            <select class="workspace-select" disabled>
                                <option <?=((int)$wac_info->compensation6 === "")? "selected": ""?> value=""></option>
                                <option <?=((int)$wac_info->compensation6 === 5)? "selected": ""?> value="5">1억원</option>
                                <option <?=((int)$wac_info->compensation6 === 8)? "selected": ""?> value="8">5천만원</option>
                                <option <?=((int)$wac_info->compensation6 === 9)? "selected": ""?> value="9">3천만원</option>
                                <option <?=((int)$wac_info->compensation6 === 10)? "selected": ""?> value="10">2천만원</option>
                                <option <?=((int)$wac_info->compensation6 === 11)? "selected": ""?> value="11">1천5백만원</option>
                                <option <?=((int)$wac_info->compensation6 === 13)? "selected": ""?> value="13">5백만원</option>
                            </select>
                            <div class="input-group">
                                <input type="tel" class="form-control text-right" value="<?=$wac_info->fee6?>" style="background: white" readonly/>
                                <span class="input-group-addon">만원</span>
                            </div>
                        </div>

                    </div>

                </div>

<?php
}

?>

       </div>

        <div class="col-md-offset-10 col-md-2">
<?php
if ((int)$rental_info->stateFlag === 0)
{
?>
            <button type="button" class="btn btn-danger pull-right" id="rit_cancel_btn">예약취소</button>
<?php
}
?>
            <button type="button" class="btn btn-default pull-right" id="rit_inventory_btn" style="margin-right: .5em">목록</button>
        </div>

    </div>

</div>

<script>
    var mRitApiReservCode = '<?=$api_reserv_code?>';
    var mRitWaabIdx = Number('<?=$waab_idx?>');
    var mRitApiCode = Number('<?=$api_code?>');
</script>
<script src="<?=base_url()?>static/lib/toastr/toastr.min.js"></script>
<script src="<?=asset_url()?>js/reserv_item.min.js"></script>
