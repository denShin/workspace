

<div class="container workspace-basic-container col-md-12">
    <div class="page-header clearfix">
        <h3 class="pull-left">API 업체 자차관리</h3>
    </div>

    <div class="clearfix"></div>

    <div class="col-md-3">
        <h4>업체 목록</h4>
        <div class="col-md-12">

<?php
$branch_inventory = $data;

$grim_inventory = $branch_inventory->grim;
$ins_inventory  = $branch_inventory->ins;

?>

            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#acm_grim_panel">그림</a>
                        </h4>
                    </div>
                    <div class="panel-collapse" id="acm_grim_panel">
                        <div class="panel-body">
<?php
foreach ($grim_inventory as $grim_obj)
{
    $button = create_api_affiliate_button($grim_obj);

    echo $button;
}

?>                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#acm_ins_panel">인스</a>
                        </h4>
                    </div>
                    <div class="panel-collapse" id="acm_ins_panel">
                        <div class="panel-body">
<?php
foreach ($ins_inventory as $ins_obj)
{
    $button = create_api_affiliate_button($ins_obj);

    echo $button;
}

?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-md-3">
        <h4>업체 자차 목록</h4>
        <div class="col-md-12" id="acm_cdw_inventory_div" style="max-height: 100px"></div>
        <div class=col-md-12" id="acm_cdw_empty_inventory_div">
            해당 업체의 자차정보가 존재하지 않습니다.
        </div>

    </div>
    <div class="col-md-6">
        <h4>자차 정보 / 매칭 정보</h4>
        <div class="col-md-12" id="acm_cdw_information_div">

            <section>
                <header><h3>API 정보</h3></header>
                <div class="col-md-12" id="acm_cdw_api_information_div"></div>
            </section>
            <hr>
            <div class="col-md-12" id="acm_carmore_cdw_div" style="display: none">
                <header><h3>매칭 정보</h3></header>
                <div class="form-group col-md-12">
                    <label for="acm_insurance_category_select">보험종류</label>
                    <select class="workspace-select" id="acm_insurance_category_select">
                        <option value=""></option>
                        <option value="1">일반자차</option>
                        <option value="2">고급자차</option>
                        <option value="3">슈퍼자차</option>
                    </select>
                </div>
                <div class="form-group col-md-12">
                    <label for="acm_compensation_input">보상한도</label>
                    <div class="input-group">
                        <input type="tel" class="form-control text-right" id="acm_compensation_input" value="" />
                        <span class="input-group-addon">만원</span>
                    </div>

                </div>

                <div class="form-group col-md-12">
                    <label for="acm_deductible_div">자기부담금 (면책금)</label>
                    <div id="acm_deductible_div" style="display: flex">
                        <div class="input-group" style="flex: 5">
                            <input type="tel" class="form-control text-right" id="acm_deductible_input1" value="" />
                            <span class="input-group-addon">만원</span>
                        </div>
                        <div style="display:flex; flex: 1; justify-content:center; align-items: center">~</div>
                        <div class="input-group" style="flex: 5">
                            <input type="tel" class="form-control text-right" id="acm_deductible_input2" value="" />
                            <span class="input-group-addon">만원</span>
                        </div>

                    </div>

                </div>

                <div class="form-group col-md-12">
                    <label for="acm_age_select">보험나이</label>
                    <select class="workspace-select" id="acm_age_select">
                        <option value=""></option>
                        <option value="E2">만 21세</option>
                        <option value="E23">만 23세</option>
                        <option value="E4">만 24세</option>
                        <option value="E1">만 26세</option>
                    </select>

                </div>

                <div class="form-group col-md-12">
                    <label for="acm_career_input">운전경력 n년 이상</label>
                    <input type="tel" class="form-control text-right" id="acm_career_input" value="" />

                </div>

                <div class="form-group col-md-12">
                    <label for="acm_description_textarea">설명</label>
                    <textarea class="form-control" id="acm_description_textarea"></textarea>
                </div>

                <div class="col-md-12">
                    <button type="button" class="btn btn-primary pull-right" id="acm_save_button" style="margin-left: 1em">저장</button>
                    <button type="button" class="btn btn-warning pull-right" id="acm_unmatching_button">매칭해제</button>
                </div>

            </div>


        </div>
        <div class="col-md-12" id="acm_cdw_empty_information_div">
            선택된 자차정보가 없습니다.
        </div>
    </div>
</div>

<?php
function create_api_affiliate_button($obj)
{
    $api  = $obj['waabApi'];
    $idx  = $obj['waabIdx'];
    $name = trim($obj['waaName'].' '.$obj['waabName']);

    return "<button type='button' class='col-md-12 btn btn-default btn-sm acm-affiliate-button' data-api-type='".$api."' data-waab-idx='".$idx."' style='margin-bottom: .5em'>".$name."</button>";
}

?>

<script src="<?=asset_url()?>/js/api_cdw_manage.min.js?ver=190802"></script>
