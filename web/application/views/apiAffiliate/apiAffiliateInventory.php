<div class="container workspace-basic-container">
    <div class="page-header clearfix">
        <h3 class="pull-left">API 업체 관리</h3>
    </div>

    <div>
        <form class="form-horizontal" role="form" method="get">
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-2">
                            <select class="form-control" name="searchParam">
                                <option value="1">회사명</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="searchValue" value="" />
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>

    <div class="clearfix"></div>

    <div class="row">
<?php
$inventory = $data['inventory'];

if (count($inventory) !== 0)
{
?>
        <div class="col-md-10" style="margin-top:1em; margin-bottom: 1em">
            <button type="button" class="btn btn-primary pull-right" onclick="location.href='/apiAffiliate/ApiAffiliateInformation?type=-1'">신규등록</button>
        </div>
        <div class="row table-responsive col-md-offset-2 col-md-8">
            <table class="table table-bordered">
                <thead>
                    <tr class="info">
                        <th class="text-center col-md-1">API</th>
                        <th class="text-center col-md-6">회사명</th>
                        <th class="text-center col-md-3">등록일</th>
                        <th class="text-center col-md-2"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                    foreach ($inventory as $obj)
                    {
                        $api = convertApiCodeToString($obj['api']);
                        ?>
                        <tr>
                            <td class="workspace-middle-td"><?=$api?></td>
                            <td class="workspace-middle-td"><?=$obj['waaName']?></td>
                            <td class="workspace-middle-td"><?=$obj['waaRegDate']?></td>
                            <td class="workspace-middle-td">
                                <button type="button" class="btn btn-sm btn-default" onclick="location.href='/apiAffiliate/ApiAffiliateInformation?type=1&waai=<?=base64_encode($obj['waaIdx'])?>&waabi=<?=base64_encode($obj['repWaabIdx'])?>'">설정</button>
                            </td>
                        </tr>
                        <?
                    }

                    ?>
                </tbody>
            </table>
        </div>

<?php
}
else
{
?>
        <div class="text-center" style="margin-top: 100px">
            <h4>등록된 API 업체가 없습니다</h4>
            <button type="button" class="btn btn-lg btn-link" onclick="location.href='/apiAffiliate/ApiAffiliateInformation?type=-1'">지금 바로 등록하기</button>
        </div>
<?
}
?>

    </div>
    <div class="text-center">
        <?=$data['pagination']?>
    </div>

</div>

<?php
function convertApiCodeToString($code)
{
    switch ((int)$code)
    {
        case 1:
            return '그림';
        case 2:
            return '인스';
        case 3:
            return '리본';
        default:
            return '';
    }
}
?>
