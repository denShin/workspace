<?php
	function decrypt($string) {
		$decryptKeyFull = substr($string, strlen($string) - 30, 30);
		$decryptTxt = substr($string, 0, strlen($string) - 30);

		$decryptKey = "";
		for($i = 0; $i < strlen($decryptKeyFull); $i++) {
			if($i % 3 == 0) {
				$decryptKey .= $decryptKeyFull[$i];
			}
		}
		
		$decrypted = '';
		$decryptTxt = base64_decode($decryptTxt);
		for($i=0; $i<strlen($decryptTxt); $i++) {
			$char = substr($decryptTxt, $i, 1);
			$keychar = substr($decryptKey, ($i % strlen($decryptKey))-1, 1);
			$char = chr(ord($char)-ord($keychar));
			$decrypted .= $char;
		}

		return $decrypted;
	}
?>
