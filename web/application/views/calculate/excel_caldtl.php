<?php
$filename =$_GET["filename"];
$filename = $filename.".xls";
$calmainmst_code =$_GET["calmainmst_code"];
$registration_number =$_GET["registration_number"];
$calmaindtl_branch =$_GET["calmaindtl_branch"];


header( "Content-type: application/vnd.ms-excel" );
header( "Content-type: application/vnd.ms-excel; charset=utf-8");
header( "Content-Disposition: attachment; filename = ".$filename );


include ($_SERVER["DOCUMENT_ROOT"]."/application/config/aws_dbcon.php");



define("ID",$aws_db["username"],true);
define("PW",$aws_db["password"],true);
define("NAME",$aws_db["dbname"],true);
define("HOST",$aws_db["host"],true);

$conn = new PDO($aws_db["dns"], $aws_db["username"], $aws_db["password"]);
$conn->exec("set names utf8");



//-- 정상결제
$sql="SELECT COUNT(*) AS cnt , IFNULL(SUM(calmaindtl_totalprice ),0)  AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch'  ";
$carQuery = $conn->prepare($sql);
$carQuery->execute();
$carStmt = $carQuery->fetch();
$result["cnt1"] =  $carStmt['cnt'];
$result["total1"] =  $carStmt['total'];


//-- 단기렌트
$sql="SELECT COUNT(*) AS cnt ,IFNULL( SUM(calmaindtl_totalprice ) ,0) AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch'  AND calmaindtl_period='1' ";
$carQuery = $conn->prepare($sql);
$carQuery->execute();
$carStmt = $carQuery->fetch();
$result["cnt2"] =  $carStmt['cnt'];
$result["total2"] =  $carStmt['total'];


//-- 월렌트
$sql="SELECT COUNT(*) AS cnt , IFNULL( SUM(calmaindtl_totalprice ) ,0) AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch'  AND calmaindtl_period='2'  ";
$carQuery = $conn->prepare($sql);
$carQuery->execute();
$carStmt = $carQuery->fetch();
$result["cnt3"] =  $carStmt['cnt'];
$result["total3"] =  $carStmt['total'];


// -- 취소수수료
$sql=" SELECT COUNT(*) AS cnt , IFNULL( SUM(calmaindtl_totalprice ) ,0) AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch' AND calmaindtl_gubn='2'  ";
$carQuery = $conn->prepare($sql);
$carQuery->execute();
$carStmt = $carQuery->fetch();
$result["cnt4"] =  $carStmt['cnt'];
$result["total4"] =  $carStmt['total'];


//-- 총매출
$sql="SELECT   IFNULL( SUM(calmaindtl_totalprice ) ,0) AS total, COUNT(*) cnt  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch'  ";
$carQuery = $conn->prepare($sql);
$carQuery->execute();
$carStmt = $carQuery->fetch();
$result["cnt5"] =  $carStmt['cnt'];
$result["total5"] =  $carStmt['total'];


//-- 총 정산
$sql="SELECT  COUNT(*) AS cnt , IFNULL( SUM(calmaindtl_calamount ) ,0) AS total  FROM caculate_calmaindtl WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch'  ";
$carQuery = $conn->prepare($sql);
$carQuery->execute();
$carStmt = $carQuery->fetch();
$result["cnt6"] =  $carStmt['cnt'];
$result["total6"] =  $carStmt['total'];
 

$cnt1str = number_format($result["cnt1"],0);
$cnt2str = number_format($result["cnt2"],0);
$cnt3str = number_format($result["cnt3"],0);
$cnt4str = number_format($result["cnt4"],0);
$cnt5str = number_format($result["cnt5"],0);
$cnt6str = number_format($result["cnt6"],0);


$total1str = number_format($result["total1"],0);
$total2str = number_format($result["total2"],0);
$total3str = number_format($result["total3"],0);
$total4str = number_format($result["total4"],0);
$total5str = number_format($result["total5"],0);
$total6str = number_format($result["total6"],0);



$EXCEL_STR= "<table border='1'>
                <tr  >
                    <th >정상결제</th>
                    <th >단기렌트</th>
                    <th >월렌트</th>
                    <th >취소수수료</th>
                    <th >총 매출</th>
                    <th >총 정산</th>
                </tr>

                <tr  >
                    <th >$total1str 원/ $cnt1str 건</th>
                    <th >$total2str 원/ $cnt2str 건</th>
                    <th >$total3str 원/ $cnt3str 건</th>
                    <th >$total4str 원/ $cnt4str 건</th>
                    <th  >$total5str 원/ $cnt5str 건</th>
                    <th  >$total6str 원/ $cnt6str 건</th>
                </tr>
            </table><br>";



$sql=
$sql="SELECT * FROM caculate_calmaindtl      WHERE calmainmst_code = '$calmainmst_code' AND calmaindtl_branch='$calmaindtl_branch'  ";

$calquery = $conn -> prepare($sql);
$calquery -> execute();

// 테이블 상단 만들기
$EXCEL_STR .= "
<table border='1'>
<tr>
<td > 구분 </td>
<td > 기간 </td>
<td > 고객명 </td>
<td  > 기간 / 차량</td>
<td >원결제금액 </td>
<td > 정산가격</td>
<td >수수료 (카드수수료 포함) </td>
<td  >할인금액(카모아 부담)</td>
</tr>";

while( $item=$calquery -> fetch() ) {

    $car_number = $item["rentcar_number"];
    $calmaindtl_idx = $item["calmaindtl_idx"];
    $carmodel = $item["car_model"];
    $driver_name = $item["driver_name"];
    $calmaindtl_gubn = $item["calmaindtl_gubn"];

    $calmaindtl_period = $item["calmaindtl_period"];
    if($calmaindtl_period=="1"){
        $calmaindtl_periodstr="단기";
    }else if($calmaindtl_period=="2"){
        $calmaindtl_periodstr="월";
    }

    $calmaindtl_rentstart = $item["calmaindtl_rentstart"];
    $calmaindtl_rentend = $item["calmaindtl_rentend"];

    $calmaindtl_totalprice = $item["calmaindtl_totalprice"];
    $calmaindtl_calamount = $item["calmaindtl_calamount"];
    $calmaindtl_rentfee = $item["calmaindtl_rentfee"];
    $calmaindtl_discount= $item["calmaindtl_discount"];
    $calmaindtl_stats= $item["calmaindtl_stats"];

    if($calmaindtl_gubn=="1"){
        $calmaindtl_gubnstr="정상결제";
    }else if($calmaindtl_gubn=="2"){
        $calmaindtl_gubnstr="취소";
    }
    $calmaindtl_totalpricestr = $calmaindtl_totalprice;
    $calmaindtl_calamountstr = $calmaindtl_calamount;
    $calmaindtl_rentfeestr = $calmaindtl_rentfee;
    $calmaindtl_discountstr = $calmaindtl_discount;


    $EXCEL_STR .= "
   <tr>
    <td > $calmaindtl_gubnstr </td>
    <td > $calmaindtl_periodstr </td>
    <td > $driver_name </td>
    <td  > $calmaindtl_rentstart ~ $calmaindtl_rentend <Br>$carmodel - $car_number</td>
    <td > $calmaindtl_totalpricestr </td>
    <td > $calmaindtl_calamountstr</td>
    <td > $calmaindtl_rentfeestr </td>
    <td  >$calmaindtl_discountstr</td>
   </tr>
   ";
}

$EXCEL_STR .= "</table>";

echo "<meta content=\"application/vnd.ms-excel; charset=UTF-8\" name=\"Content-type\"> ";
echo $EXCEL_STR;

?>