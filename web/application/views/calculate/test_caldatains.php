<?php

include ($_SERVER["DOCUMENT_ROOT"]."/application/config/aws_dbcon.php");
require_once("newBalanceData.php");
require_once("decrypt.php");


define("ID",$aws_db["username"],true);
define("PW",$aws_db["password"],true);
define("NAME",$aws_db["dbname"],true);
define("HOST",$aws_db["host"],true);

$conn = new PDO($aws_db["dns"], $aws_db["username"], $aws_db["password"]);
$conn->exec("set names utf8");

//  http://workspace2.teamo2.kr/application/views/calculate/test_caldatains.php

// 1일 ~15일 = 16일에 정산
// 16일~말일 = 익월 첫영업일


//////////////////////////////////////////////
/// // 마스터 정보 등록 //
$start = $_POST['calmainmst_startdate'];
$ending = $_POST['calmainmst_enddate'];
$calmainmst_title = $_POST['calmainmst_title'];
$end =date("Y-m-d 23:59:59",strtotime($ending));
$balance = $_POST['balance'];

$calmainmst_code = "CMS".time();


    // 1. 겹치는 기간이 존재하면 error
    $sql="SELECT (select  count(*) as cnt FROM caculate_calmainmst WHERE '$start' BETWEEN   calmainmst_startdate AND   calmainmst_enddate ) as cnt1 
        ,(select  count(*) as cnt FROM caculate_calmainmst WHERE '$ending' BETWEEN   calmainmst_startdate AND   calmainmst_enddate ) as cnt2 
        ,(select  ifnull( max(calmainmst_enddate) ,'')   FROM caculate_calmainmst WHERE '$ending' BETWEEN   calmainmst_startdate AND   calmainmst_enddate ) as maxend ";
    $userQuery = $conn->prepare($sql);
    $userQuery->execute();
    $cntStmt = $userQuery->fetch();
    $cnt1 =  $cntStmt['cnt1'];
    $cnt2 =  $cntStmt['cnt2'];
    $maxend =  $cntStmt['maxend'];


    $totalcnt = $cnt1+$cnt2;

    if($totalcnt > 0){

        if($maxend !=""){
            echo "<script>alert('이미 정산 완료된 기간을 선택하셨습니다. 최근 정산 마감일은 $maxend 입니다.');history.back();</script>";
        }else{
            echo "<script>alert('이미 정산 완료된 기간을 선택하셨습니다.');history.back();</script>";
        }
        exit();
    }

    $sql="INSERT INTO caculate_calmainmst (  calmainmst_code,calmainmst_title,  calmainmst_startdate,  calmainmst_enddate,calmainmst_step,regdate)
    VALUES  (    '$calmainmst_code','$calmainmst_title',    '$start',    '$ending' ,'step2',now() );";
    $stmt = $conn->prepare($sql);
    $stmt->execute();


//////////////////////////////////////////////
    //  '$start 00:00:00' AND f_rentenddate <= '$ending 23:59:59'
    $startime=$start." 00:00:00";
    $endtime=$ending." 00:00:00";

       $sql = "
       SELECT
           balance_tbl.*,
           IFNULL(am.admin_idx, 0) admin_idx
       FROM
       (
           SELECT
               tul.kakaoid,
               CONCAT(car_number1, car_number2, car_number3) rentcar_number, model,
               crti_reserv_rent_type, crti_start_date, crti_end_date, crti_early_return_apply, crti_early_return_use_cost,
               crti_principal, crti_payment, crti_rent_cost, crti_cdw_cost, crti_delivery_cost, crti_use_point, crti_use_coupon_value,
               nrs.driver_name
           FROM
               carmore_reservation_term_information crti INNER JOIN
               tbl_reservation_list trl INNER JOIN
               new_rentSchedule nrs INNER JOIN
               tbl_usr_list tul INNER JOIN
               new_rentCar rentcar INNER JOIN
               new_car car
               ON
                   crti_reservation_idx=trl.f_reservationidx AND trl.f_bookingid=nrs.serial AND tul.usrserial=trl.f_usrserial AND
                   trl.f_carserial=rentcar.serial AND rentcar.car_serial=car.serial
           WHERE
               nrs.driver_name NOT LIKE '팀오투테스트%' AND
                (
                (crti_reserv_rent_type=1 AND crti_end_date >= '$startime' AND crti_end_date <= '$endtime' AND crti_state=1) OR
                   (
                       crti_reserv_rent_type=2 AND
                       (
                       (crti_start_date >= '$startime' AND crti_start_date <= '$endtime' AND crti_state=1) 
                     )
                   )
               )
           ) balance_tbl LEFT OUTER JOIN
           admin_member am
           ON
               balance_tbl.kakaoid=am.kakaoid ";


    $sql="SELECT com.serial AS comserial,bra.serial  AS braserial, NAME, bra.branchName, COUNT(*)
FROM new_rentCompany com INNER JOIN new_rentCompany_branch bra INNER JOIN tbl_reservation_list reLIST
ON com.serial=bra.rentCompany_serial AND bra.serial=reLIST.f_companycode
WHERE f_paystatus=1 AND f_rentstatus=2 AND f_rentenddate >= '$start 00:00:00' AND f_rentenddate <= '$ending 23:59:59'
 AND f_reservationidx > 1014 AND f_usrserial<>1166 AND f_reservationidx <> 1719 AND f_reservationidx <> 55563
GROUP BY f_companycode ORDER BY NAME;";

echo "<Br><BR>";
echo $sql;


exit();
$calquery = $conn -> prepare($sql);
$calquery -> execute();


while( $calrow=$calquery -> fetch() ) {
    $branch = $calrow['braserial'];
    $company = $calrow['comserial'];


    $normalBalance_count = 0;    //정상결제 건수
    $semiCancel_count = 0;        //취소수수료 건수
    $completeCancel_count = 0;    //완전취소 건수

    $normalBalance_ammount = 0;       //정상결제 금액
    $semiCancel_ammount = 0;          //취소수수료 금액
    $completeCancel_ammount = 0;      //완전취소 금액
    $point_ammount = 0;               //포인트 금액
    $coupon_ammount = 0;              //쿠폰 금액
    $pay_ammount = 0;                 //실결제금액

    $total_givePoint = 0;     //총 지급포인트

    $totalBalance_rent = 0;   //대여료 정산
    $totalBenefit_rent = 0;   //대여료 처리

    $totalBalance_count = 0;              //총 정산건수
    $totalBalance_ammount = 0;            //총 정산금액

    $totalOriginal_ammount = 0;           //총원금
    $totalRent_ammount = 0;               //총렌트금
    $totalInsu_ammount = 0;               //총보험금
    $totalDeliv_ammount = 0;              //총딜리버리금
    $totalBenefit_ammount = 0;            //총처리금
    $totalCardCommission_ammount = 0;     //총카드수수료
    $totalRevenue = 0;            //총매출

    $totalCommission = 0;

    $total_carmore_payment = 0;
    $total_carmore_commission = 0;

    $total_normal_cnt = 0;
    $total_month_cnt = 0;

    $total_normal_cost = 0;
    $total_month_cost = 0;


    // 정산해야할 목록 예약정보 테이블
    $query = "SELECT * FROM tbl_account_info WHERE flag=0 ORDER BY regdate DESC";

    $accountInfoQuery = $conn->prepare($query);
    $accountInfoQuery->execute();


    $EXCEL_STR = "  
        <table border='1'>  
        <tr>  
           <td style='text-align:center; font-size:120%;'><b>구분</b></td>  
           <td style='text-align:center; font-size:120%;'><b>단기/월</b></td>  
           <td style='text-align:center; font-size:120%;'><b>고객정보</b></td>
           <td style='text-align:center; font-size:120%;'><b>렌트기간<br>차량</b></td>
           <td style='text-align:center; font-size:120%;'><b>원결제금액</b></td>
           <td style='text-align:center; font-size:120%;'><b>정산가격</b></td>
           <td style='text-align:center; font-size:120%;'><b>수수료</b><br><small>(카드사 수수료 포함)</small></td>
           <td style='text-align:center; font-size:120%;'><b>할인금액</b><br><small>(카모아 부담)</small></td>
        </tr>";

    while ($accountInfoStmt = $accountInfoQuery->fetch()) {
        $driverName = "";
        $status = $accountInfoStmt['status'];
        $reservationIdx = $accountInfoStmt['f_reservationidx'];

        $with = ($branch != 'all') ? ' AND f_companycode="' . $branch . '" ' : '';

        $chk = "SELECT f_usrserial FROM tbl_reservation_list WHERE f_reservationidx='$reservationIdx' AND company_serial='$company' $with and f_usrserial NOT IN (34897, 23314, 15397, 55563)";

        $chkSql = $conn->prepare($chk);
        $chkSql->execute();
        if ($chkStmt = $chkSql->fetch()) {
            $wibble = array(7831);
            if (in_array($chkStmt['f_usrserial'], $wibble))
                continue;
        } else
            continue;


        $basicDate = '';    //정산기준일
        $bookingId = '';

        if ($status == 1) {

            $reserv_idx = explode('E', $reservationIdx)[0];
            $crti_idx = strpos($reservationIdx, 'E') ? trim(explode('E', $reservationIdx)[1]) : '';

            $added_where = ($crti_idx !== '') ? " crti_idx= '" . $crti_idx . "' " : "crti_reservation_idx='" . $reserv_idx . "' ";

            $query = "
                SELECT
                   f_bookingid, f_usrserial, crti_start_date as f_rentstartdate, crti_end_date as f_rentenddate, f_carserial,
                   crti_rent_cost as f_rentprice, crti_cdw_cost as f_insuprice, crti_delivery_cost as f_delivprice,
                   crti_use_coupon_idx as f_couponserial, crti_use_point as f_usepoint, crti_payment as f_totalprice,
                    f_drivername,f_usecarnumber,driver_name_encrypt, f_companycode,
                   crti_early_return_apply, crti_early_return_use_cost, crti_reserv_rent_type
                FROM
                  tbl_reservation_list trl INNER JOIN
                  carmore_reservation_term_information crti
                  ON
                      trl.f_reservationidx=crti.crti_reservation_idx AND crti.crti_state=1
                WHERE 
                     " . $added_where . " AND 
                     (
                        (crti.crti_end_date BETWEEN '$start' AND '$end') OR
                        (crti.crti_early_return_apply=2 AND crti_early_return_confirm_date BETWEEN LEFT('$start', 10) AND LEFT('$end', 10))
                     )";


            $accountQuery = $conn->prepare($query);
            $accountQuery->execute();

            if ($accountStmt = $accountQuery->fetch()) {

                $category = "정상결제";
                $basicDate = $accountStmt['f_rentenddate'];
                $userSerial = $accountStmt['f_usrserial'];
                $carSerial = $accountStmt['f_carserial'];
                $couponSerial = $accountStmt['f_couponserial'];
                $fcompanycode = $accountStmt['f_companycode'];
                $bookingId = $accountStmt['f_bookingid'];
                $startDate = $accountStmt['f_rentstartdate'];
                $endDate = $accountStmt['f_rentenddate'];
                $rentPrice = $accountStmt['f_rentprice'];
                $insuPrice = $accountStmt['f_insuprice'];
                $delivPrice = $accountStmt['f_delivprice'];
                $usePoint = $accountStmt['f_usepoint'];
                $payPrice = $accountStmt['f_totalprice'];
                $encrypt = $accountStmt['driver_name_encrypt'];
                $originalPrice = $rentPrice + $insuPrice + $delivPrice;

                $query = "SELECT usrname,phone, kakaoid FROM tbl_usr_list WHERE usrserial=?";
                $userQuery = $conn->prepare($query);
                $userQuery->execute([$userSerial]);
                $userStmt = $userQuery->fetch();

                $userName = $userStmt['usrname'];   //유저명
                $driverName = $accountStmt['f_drivername']; //운전자명
                if ($driverName == '')
                    $driverName = decrypt($encrypt);

                if (strpos($driverName, '팀오투테스트') !== false)
                    continue;


                $kakaoid = $userStmt['kakaoid'];
                $call_reserv = 0;
                if (!empty($kakaoid) && $kakaoid !== '0') {
                    $admin_query = "SELECT admin_idx FROM admin_member WHERE kakaoid=? AND kakaoid <> '' AND kakaoid <> 0";
                    $admin_sql = $conn->prepare($admin_query);
                    $admin_sql->execute([$kakaoid]);
                    if ($admin_stmt = $admin_sql->fetch())
                        $call_reserv = 1;

                }

                $param_point = $usePoint;
                $param_payment = $payPrice;
                if ($call_reserv === 1) {
                    $param_point = 0;
                    $param_payment = $usePoint;
                }

                if ($accountStmt['crti_early_return_apply'] == 2) {
                    $param_payment = $accountStmt['crti_early_return_use_cost'];
                    if ($call_reserv === 1) {
                        $originalPrice = $param_payment;


                    }

                }


                $carmore_payment = (int)$originalPrice - (int)$param_payment;
                $total_carmore_payment += $carmore_payment;

                $carModel = '';  //차량모델
                $carNumber = '';   //차량번호
                $query = 'SELECT CONCAT(car_number1,car_number2,car_number3) AS car_number,model FROM new_rentCar INNER JOIN new_car ON new_rentCar.car_serial=new_car.serial WHERE new_rentCar.serial="' . $carSerial . '"';
                $carQuery = $conn->prepare($query);
                $carQuery->execute();
                $carStmt = $carQuery->fetch();

                $carModel = $carStmt['model'];  //차량모델
                $carNumber = $carStmt['car_number'];

                $query = 'select value from new_coupon_info info inner join new_coupon_list list on list.info_serial=info.serial where list.serial=:cs';
                $couponQuery = $conn->prepare($query);
                $couponQuery->bindParam(":cs", $couponSerial);
                $couponQuery->execute();
                $couponStmt = $couponQuery->fetch();

                $coupon = $couponStmt['value']; //쿠폰사용액
                if ($coupon == '')
                    $coupon = 0;

                $query = 'select name, branchName FROM new_rentCompany a INNER JOIN new_rentCompany_branch b ON a.serial=b.rentCompany_serial where b.serial="' . $fcompanycode . '"';
                $bcsql = $conn->prepare($query);
                $bcsql->execute();
                $companyname = '고고렌트카';
                if ($bcStmt = $bcsql->fetch()) {
                    $companyname = $bcStmt['name'];
                    $branchName = $bcStmt['branchName'];
                }

                if ($fcompanycode == 2 || $fcompanycode == 30)
                    $balance = new newNormalBalancePrimium($rentPrice, $insuPrice, $delivPrice, $param_payment, $param_point, $coupon, $originalPrice);
                else
                    $balance = new newNormalBalance($rentPrice, $insuPrice, $delivPrice, $param_payment, $param_point, $coupon, $originalPrice);

                $accountAmmount = $balance->getBalanceAmmount();
                $cardCommission = $balance->getCardCommission();
                $benefitAmmount = $balance->getBenefitAmmount();

                $commission = $originalPrice - $accountAmmount - $cardCommission;
                $carmore_commission = $commission + $cardCommission;
                $total_carmore_commission += $carmore_commission;

                $givePoint = $balance->getGivePoint();
                $total_givePoint += $givePoint;

                $totalBenefit_ammount += $benefitAmmount;

                $totalCommission += $commission;
                $totalCardCommission_ammount += $cardCommission;

                $is_it_normal = ($accountStmt['crti_reserv_rent_type'] == 1) ? true : false;

                $rent_type_str = ($is_it_normal) ? '단기 렌트' : '월 렌트';

                $EXCEL_STR .= '<tr>';
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>" . $category . "</td>";
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>" . $rent_type_str . "</td>";
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>" . $userName . "&nbsp;/&nbsp;" . $driverName . "</td>";
                $EXCEL_STR .= "<td>" . substr($startDate, 0, 16) . " ~ " . substr($endDate, 0, 16) . "</td>";
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>" . preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $originalPrice) . "</td>";
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>" . preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $accountAmmount) . "</td>";
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black; border-right: 4px solid darkgoldenrod;'>" . preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $carmore_commission) . "</td>";
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>" . preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $carmore_payment) . "</td>";
                $EXCEL_STR .= "</tr><tr style='background-color:#e3f2fd;'>";
                $EXCEL_STR .= "<td style='border-bottom:2px solid black;'>" . $carModel . " - " . $carNumber . "</td>";

                $normalBalance_count++;
                $normalBalance_ammount += $originalPrice;
                $totalBalance_count++;
                $totalBalance_ammount += $accountAmmount;

                $totalRevenue += $originalPrice;

                $totalOriginal_ammount += $originalPrice;

                $point_ammount += $usePoint;
                $coupon_ammount += $coupon;
                $pay_ammount += $payPrice;

                if ($is_it_normal) {
                    $total_normal_cnt++;
                    $total_normal_cost += $originalPrice;
                } else {
                    $total_month_cnt++;
                    $total_month_cost += $originalPrice;
                }


                ### 정산을 위한 데이터 출력
                if ($is_it_normal == "1") {
                    $calmaindtl_period = "단기렌트";
                } else {
                    $calmaindtl_period = "월렌트";
                }
                $calmaindtl_company = $company;
                $calmaindtl_branch = $branch;
                $calmaindtl_gubn = '1';
                $calmaindtl_period = $calmaindtl_period;
                $calmaindtl_name = $driverName;
                $calmaindtl_rentstart = substr($startDate, 0, 16);
                $calmaindtl_rentend = substr($endDate, 0, 16);
                $calmaindtl_carserial = $carSerial;
                $calmaindtl_totalprice = $originalPrice;
                $calmaindtl_calamount = $accountAmmount;
                $calmaindtl_rentfee = $carmore_commission;
                $calmaindtl_discount = $carmore_payment;

                $reservationIdx = trim($reservationIdx);
                $sql = "INSERT INTO caculate_calmaindtl (   calmainmst_code,  calmaindtl_company,  calmaindtl_branch,  calmaindtl_gubn,
                              calmaindtl_period,  calmaindtl_userserial,  calmaindtl_rentstart,  calmaindtl_rentend,  calmaindtl_carserial,
                              calmaindtl_totalprice,  calmaindtl_calamount,  calmaindtl_rentfee,  calmaindtl_discount,  regdate,f_reservationidx)
                              VALUES  (    '$calmainmst_code',    '$calmaindtl_company',    '$calmaindtl_branch',    '$calmaindtl_gubn',
                                  '$calmaindtl_period',    '$userSerial',    '$calmaindtl_rentstart',    '$calmaindtl_rentend',
                                  '$calmaindtl_carserial',    '$calmaindtl_totalprice',    '$calmaindtl_calamount', '$calmaindtl_rentfee',
                                  '$calmaindtl_discount',    now()  ,'$reservationIdx');";
                $stmt = $conn->prepare($sql);
                $stmt->execute();

            }


        } elseif ($status == 2) {

            //취소수수료
            $query = "SELECT f_regdate FROM tbl_cancel_log WHERE f_reservationidx='$reservationIdx' AND f_regdate BETWEEN '$start' AND '$end'";
            $checkQuery = $conn->prepare($query);
            $checkQuery->execute();
            if ($checkStmt = $checkQuery->fetch()) {
                $category = "취소수수료";
                $basicDate = $checkStmt['f_regdate'];

                $query = "SELECT f_bookingid,f_usrserial,f_rentstartdate,f_rentenddate,f_carserial,f_rentprice,f_insuprice,f_delivprice,f_couponserial,f_usepoint,f_totalprice
,f_drivername,f_usecarnumber,driver_name_encrypt, f_companycode, cancel_per FROM tbl_reservation_list WHERE f_reservationidx='$reservationIdx' AND f_paystatus='2'";
                $accountQuery = $conn->prepare($query);
                $accountQuery->execute();
                if ($accountStmt = $accountQuery->fetch()) {
                    $userSerial = $accountStmt['f_usrserial'];
                    $carSerial = $accountStmt['f_carserial'];
                    $couponSerial = $accountStmt['f_couponserial'];

                    $bookingId = $accountStmt['f_bookingid'];
                    $startDate = $accountStmt['f_rentstartdate'];
                    $endDate = $accountStmt['f_rentenddate'];
                    $rentPrice = $accountStmt['f_rentprice'];
                    $insuPrice = $accountStmt['f_insuprice'];
                    $delivPrice = $accountStmt['f_delivprice'];
                    $usePoint = $accountStmt['f_usepoint'];
                    $payPrice = $accountStmt['f_totalprice'];
                    $originalPrice = $rentPrice + $insuPrice + $delivPrice;
                    $encrypt = $accountStmt['driver_name_encrypt'];
                    $cancel_per = $accountStmt['cancel_per'];

                    $carmore_payment = (int)$originalPrice - (int)$payPrice;
                    $total_carmore_payment += $carmore_payment;

                    $fcompanycode = $accountStmt['f_companycode'];

                    $query = "SELECT usrname,phone,kakaoid FROM tbl_usr_list WHERE usrserial='$userSerial'";
                    $userQuery = $conn->prepare($query);
                    $userQuery->execute();
                    $userStmt = $userQuery->fetch();

                    $userName = $userStmt['usrname'];   //유저명
                    $driverName = $accountStmt['f_drivername']; //운전자명
                    if ($driverName == '')
                        $driverName = decrypt($encrypt);

                    $kakaoid = $userStmt['kakaoid'];
                    $call_reserv = 0;
                    if (!empty($kakaoid) && $kakaoid !== '0') {
                        $admin_query = "SELECT admin_idx FROM admin_member WHERE kakaoid=? AND kakaoid <> '' AND kakaoid <> 0";
                        $admin_sql = $conn->prepare($query);
                        $admin_sql->execute([$kakaoid]);
                        if ($admin_stmt = $admin_sql->fetch())
                            $call_reserv = 1;
                    }

                    $param_point = $usePoint;
                    $param_payment = $payPrice;
                    if ($call_reserv === 1) {
                        $param_point = 0;
                        $param_payment = $usePoint;
                    }

                    $carModel = '';  //차량모델
                    $carNumber = '';   //차량번호
                    $query = 'SELECT CONCAT(car_number1,car_number2,car_number3) AS car_number,model FROM new_rentCar INNER JOIN new_car ON new_rentCar.car_serial=new_car.serial WHERE new_rentCar.serial="' . $carSerial . '"';
                    $carQuery = $conn->prepare($query);
                    $carQuery->execute();
                    $carStmt = $carQuery->fetch();

                    $carModel = $carStmt['model'];  //차량모델
                    $carNumber = $carStmt['car_number'];

                    $query = 'select value from new_coupon_info info inner join new_coupon_list list on list.info_serial=info.serial where list.serial=:cs';
                    $couponQuery = $conn->prepare($query);
                    $couponQuery->bindParam(":cs", $couponSerial);
                    $couponQuery->execute();
                    $couponStmt = $couponQuery->fetch();

                    $coupon = $couponStmt['value']; //쿠폰사용액
                    if ($coupon == '')
                        $coupon = 0;

                    $query = 'select name, branchName FROM new_rentCompany a INNER JOIN new_rentCompany_branch b ON a.serial=b.rentCompany_serial where b.serial="' . $fcompanycode . '"';
                    $bcsql = $conn->prepare($query);
                    $bcsql->execute();
                    $companyname = '고고렌트카';
                    if ($bcStmt = $bcsql->fetch()) {
                        $companyname = $bcStmt['name'];
                        $branchName = $bcStmt['branchName'];
                    }

                    if ($fcompanycode == 2 || $fcompanycode == 30)
                        $balance = new newSemicancelBalancePrimium($rentPrice, $insuPrice, $delivPrice, $param_payment, $param_point, $coupon, $originalPrice, $cancel_per);
                    else
                        $balance = new newSemicancelBalance($rentPrice, $insuPrice, $delivPrice, $param_payment, $param_point, $coupon, $originalPrice, $cancel_per);

                    $accountAmmount = $balance->getBalanceAmmount();
                    $cardCommission = $balance->getCardCommission();
                    $benefitAmmount = $balance->getBenefitAmmount();

                    $givePoint = $balance->getGivePoint();
                    $total_givePoint += $givePoint;

                    $cancelCharge = $balance->getCancellationCharge();
                    $totalRevenue += $cancelCharge;

                    $totalCardCommission_ammount += $cardCommission;

                    $totalBenefit_ammount += $benefitAmmount;

                    $commission = $cancelCharge - $accountAmmount - $cardCommission;
                    $totalCommission += $commission;

                    $carmore_commission = $commission + $cardCommission;
                    $total_carmore_commission += $carmore_commission;
                    $EXCEL_STR .= '<tr>';
                    $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>" . $category . "</td>";
                    $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>단기</td>";
                    $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>" . $userName . "&nbsp;/&nbsp;" . $driverName . "</td>";
                    $EXCEL_STR .= "<td>" . substr($startDate, 0, 16) . " ~ " . substr($endDate, 0, 16) . "</td>";
                    $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>" . preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $cancelCharge) . "</td>";
                    $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>" . preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $accountAmmount) . "</td>";
                    $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black; border-right: 4px solid darkgoldenrod;'>" . preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $carmore_commission) . "</td>";
                    $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>" . preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $carmore_payment) . "</td>";
                    $EXCEL_STR .= "</tr><tr style='background-color:#e3f2fd;'>";
                    $EXCEL_STR .= "<td style='border-bottom:2px solid black;'>" . $carModel . " - " . $carNumber . "</td>";

                    $semiCancel_count++;
                    $semiCancel_ammount += $cancelCharge;
                    $totalBalance_count++;
                    $totalBalance_ammount += $accountAmmount;

                    $point_ammount += $balance->getUsePoint();
                    $coupon_ammount += $coupon;
                    $pay_ammount += $balance->getCancellationChargeForUser();

                    $totalOriginal_ammount += $cancelCharge;


                    ### 정산을 위한 데이터 출력
                    if ($is_it_normal == "1") {
                        $calmaindtl_period = "단기렌트";
                    } else {
                        $calmaindtl_period = "월렌트";
                    }
                    $calmaindtl_company = $company;
                    $calmaindtl_branch = $branch;
                    $calmaindtl_gubn = '2';
                    $calmaindtl_period = $calmaindtl_period;
                    $calmaindtl_name = $driverName;
                    $calmaindtl_rentstart = substr($startDate, 0, 16);
                    $calmaindtl_rentend = substr($endDate, 0, 16);
                    $calmaindtl_carserial = $carSerial;
                    $calmaindtl_totalprice = $cancelCharge;
                    $calmaindtl_calamount = $accountAmmount;
                    $calmaindtl_rentfee = $carmore_commission;
                    $calmaindtl_discount = $carmore_payment;

                    $reservationIdx = trim($reservationIdx);
                    $sql = "INSERT INTO caculate_calmaindtl (   calmainmst_code,  calmaindtl_company,  calmaindtl_branch,  calmaindtl_gubn,
                              calmaindtl_period,  calmaindtl_userserial,  calmaindtl_rentstart,  calmaindtl_rentend,  calmaindtl_carserial,
                              calmaindtl_totalprice,  calmaindtl_calamount,  calmaindtl_rentfee,  calmaindtl_discount,  regdate,f_reservationidx)
                              VALUES  (    '$calmainmst_code',    '$calmaindtl_company',    '$calmaindtl_branch',    '$calmaindtl_gubn',
                                  '$calmaindtl_period',    '$userSerial',    '$calmaindtl_rentstart',    '$calmaindtl_rentend',
                                  '$calmaindtl_carserial',    '$calmaindtl_totalprice',    '$calmaindtl_calamount', '$calmaindtl_rentfee',
                                  '$calmaindtl_discount',    now()  ,'$reservationIdx');";
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();


                }
            }

        }

    }
    $total_benefit_ammount = $totalBenefit_rent; // 카드수수료 지급포인트 제하기 전

 }

 $sql="INSERT INTO caculate_calbranch(calmainmst_code,calmaindtl_branch,dtlcnt)
  SELECT calmainmst_code,calmaindtl_branch,COUNT(*) AS cnt FROM caculate_calmaindtl where  calmainmst_code='$calmainmst_code'
   GROUP BY calmainmst_code,calmaindtl_branch";
$stmt = $conn->prepare($sql);
$stmt->execute();

echo "<script>alert('$start 에서 $ending 까지 초기 정산데이터 설정이 완료되었습니다. Step2 정산 데이터 세부설정을 진행해주세요');location.href='/calculate/Calculatelog';</script>";
exit();
?>