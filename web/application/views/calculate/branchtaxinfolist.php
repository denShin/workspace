<style>
    .fixed-table-body {
        overflow-x: auto;
    }
</style>
<div class="container-fluid" style="padding-top: 50px;padding-bottom: 70px;">

    <div class="page-header clearfix">
        <h3 class="pull-left">업체별 세금계산서/ 정산정보</h3>

    </div>
    <div id="searchdiv" >
        <form id="searchform" class="form-horizontal" role="form" method="get" >
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <select class="form-control" name="sp">
                                <option value="company_name" >회사명</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="sv" value="">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </fieldset>
        </form>

    </div>
    <div class="clearfix"></div>
    <br>
    <div class="clearfix" style="padding-top: 10px"></div>
    <form name="updateConfirm" method="post">
        <input type="hidden" name="emode" value="changestats">
        <input type="hidden" name="stats" >

        <div class="table-responsive ">
            <table class="table" >
                <thead>
                <tr class="info row">
                    <th class="text-center" style="width: 300px">회사/지점명</th>
                    <th class="text-center "  style="width: 200px">별칭(7자이내)<br>사업자번호</th>
                    <th class="text-center"  style="width: 200px">회사명<br>대표자</th>
                    <th class="text-center "  style="width: 200px">업태<br>업종</th>
                    <th class="text-center"  style="width: 200px">정산메일1<br>정산메일2</th>
                    <th class="text-center "  style="width: 200px">세금메일1<br>세금메일2</th>

                    <th class="text-center "  style="width: 600px">주소<br>계좌정보</th>
                    <th class="text-center "  style="width: 100px">저장</th>
                </tr>
                </thead>
                <tbody>

                <?php
                foreach($data["list"] as $entry) {

                    $rentCompany_serial =$entry["rentCompany_serial"];
                    $serial =$entry["serial"];
                    $registration_number=$entry["registration_number"];
                    $tcompany_name=$entry["tcompany_name"];
                    $ceo_name=$entry["ceo_name"];
                    $addr=$entry["addr"];
                    $biztype=$entry["biztype"];
                    $bizclass=$entry["bizclass"];
                    $tax_email=$entry["tax_email"];
                    $cal_email=$entry["cal_email"];
                    $tax_email2=$entry["tax_email2"];
                    $cal_email2=$entry["cal_email2"];
                    $bankinfo=$entry["bankinfo"];
                    $bankaccount=$entry["bankaccount"];
                    $alias=$entry["alias"];
                    $contactname=$entry["contactname"];

                    ?>
                    <tr class="text-center row editrow" id="row<?=$serial?>">
                        <td class="small text-center"   ><?=$rentCompany_serial?>.<?= $entry["company_name"] ?> <Br> <?= $entry["branchName"] ?> /<?= $entry["mem_name"] ?> </td>

                        <td class="small text-center"   >
                                <input type="text" id="alias<?=$serial?>" value="<?=$alias?>" onclick="rowclick('<?=$serial?>')">
                            <br><br><input type="text" id="registration_number<?=$serial?>" value="<?=$registration_number?>" onclick="rowclick('<?=$serial?>')">
                        </td>
                        <td class="small text-center"   >
                            <input type="text"  id="company_name<?=$serial?>" value="<?=$tcompany_name?>"   onclick="rowclick('<?=$serial?>')"><br><br>
                            <input type="text" id="ceo_name<?=$serial?>" value="<?=$ceo_name?>"   onclick="rowclick('<?=$serial?>')">
                        </td>
                        <td class="small text-center"   >
                            <input type="text"  id="biztype<?=$serial?>" value="<?=$biztype?>"   onclick="rowclick('<?=$serial?>')"><br><br>
                            <input type="text"  id="bizclass<?=$serial?>" value="<?=$bizclass?>"   onclick="rowclick('<?=$serial?>')">
                        </td>
                        <td class="small text-center"   >
                            <input type="text"  id="cal_email<?=$serial?>"   value="<?=$cal_email?>" onclick="rowclick('<?=$serial?>')"><br><br>
                            <input type="text"  id="cal_email2<?=$serial?>"   value="<?=$cal_email2?>" onclick="rowclick('<?=$serial?>')">
                        </td>

                        <td class="small text-center"   >
                            <input type="text"  id="tax_email<?=$serial?>"   value="<?=$tax_email?>" onclick="rowclick('<?=$serial?>')"><br><br>
                            <input type="text"  id="tax_email2<?=$serial?>"   value="<?=$tax_email2?>" onclick="rowclick('<?=$serial?>')">
                        </td>
                        <td class="small text-center"   >
                            <input type="text" id="addr<?=$serial?>"   value="<?=$addr?>" onclick="rowclick('<?=$serial?>')" style="width: 90%" ><br><br>
                            <input type="text" id="bankinfo<?=$serial?>"  value="<?=$bankinfo?>" style="width: 30%"  onclick="rowclick('<?=$serial?>')">
                            <input type="text" id="bankaccount<?=$serial?>"  value="<?=$bankaccount?>" style="width: 60%"  onclick="rowclick('<?=$serial?>')">
                        </td>
                        <td class="small text-center"   >

                            <button type="button" class="btn btn-success  btn-block" onclick="savetaxinfo('<?=$serial?>')"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>저장</button>
                        </td>

                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="text-center">
            <?=$data['pagination']?>
        </div>
    </form>
 

</div>

<script language="JavaScript">

    function rowclick(idx){

        $(".editrow").css('background-color','#ffffff');
        $("#row"+idx).css('background-color','#5cea5c');
    }

    function savetaxinfo(serial){

        var registration_number=$("#registration_number"+serial).val();
        var company_name=$("#company_name"+serial).val();
        var ceo_name=$("#ceo_name"+serial).val();
        var addr=$("#addr"+serial).val();
        var biztype=$("#biztype"+serial).val();
        var bizclass=$("#bizclass"+serial).val();


        var contactname=$("#contactname"+serial).val();
        var tax_email=$("#tax_email"+serial).val();
        var cal_email=$("#cal_email"+serial).val();
        var tax_email2=$("#tax_email2"+serial).val();
        var cal_email2=$("#cal_email2"+serial).val();

        var bankinfo=$("#bankinfo"+serial).val();
        var bankaccount=$("#bankaccount"+serial).val();
        var contactname="";

        var alias=$("#alias"+serial).val();

        if(confirm('*세금/정산 정보를 수정하시겠습니까? ')){

            var postdata ={'ptype':'savetaxinfo' ,'branch_serial':serial,'registration_number':registration_number
                ,'company_name':company_name,'ceo_name':ceo_name,'addr':addr,'biztype':biztype,'bizclass':bizclass,'contactname':contactname
                ,'tax_email':tax_email,'cal_email':cal_email,'tax_email2':tax_email2,'cal_email2':cal_email2
                ,'bankinfo':bankinfo ,'bankaccount':bankaccount,'alias':alias}

                console.log(postdata);
            $.ajax({
                url: "/calculate/BranchTaxinfo",type: "post", data:postdata,
                success: function (response) {},
                error: function(jqXHR, textStatus, errorThrown) {},
                complete:  function() {}
            });
        }

    }

</script>


</body>
</html>

