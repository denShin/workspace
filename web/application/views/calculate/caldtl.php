<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Team O2</title>



    <link href="/static/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/static/lib/bootstrap/css/bootstrap-theme.css" rel="stylesheet">


    <!-- Placed at the end of the document so the pages load faster -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="/static/lib/bootstrap/js/bootstrap.min.js"></script>


    <!-- https://fontawesome.com/v4.7.0/icons/-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<?php
$arr_mst = $data["mst"];
$arr_list = $data["list"];
$arr_dtl = $data["dtl"];
$arr_caldtl= $data["calbranchdtl"];
$sumdata = $data["sumdata"];

$cnt1str = number_format($sumdata["cnt1"],0);
$cnt2str = number_format($sumdata["cnt2"],0);
$cnt3str = number_format($sumdata["cnt3"],0);
$cnt4str = number_format($sumdata["cnt4"],0);
$cnt5str = number_format($sumdata["cnt5"],0);
$cnt6str = number_format($sumdata["cnt6"],0);


$total1str = number_format($sumdata["total1"],0);
$total2str = number_format($sumdata["total2"],0);
$total3str = number_format($sumdata["total3"],0);
$total4str = number_format($sumdata["total4"],0);
$total5str = number_format($sumdata["total5"],0);
$total6str = number_format($sumdata["total6"],0);

$calmainmst_code =  $data["calmainmst_code"];
$calmaindtl_branch =  $data["calmaindtl_branch"];
$registration_number =  $data["registration_number"];
$mainbranchcalculate_yn=  $arr_caldtl["calculate_yn"];
$filename =$data["company_name"]."(".$arr_mst["calmainmst_startdate"].")";
$filename = str_replace(" ","",$filename);
$memo = $arr_caldtl['calmemo'];

?>
<script language="JavaScript">
    function exceldown(){
        location.href="/application/views/calculate/excel_caldtl.php?filename=<?=$filename?>&calmainmst_code=<?=$calmainmst_code?>&calmaindtl_branch=<?=$calmaindtl_branch?>";
    }
</script>
<div class="container" style="padding-top: 10px;">

    <style>
        .double-input .form-control {
            width: 50%;
        }

        .show-grid [class^=col-] {
            padding-top: 10px;
            padding-bottom: 10px;
            border: 1px solid #ddd;
            border: 1px solid rgba(86,61,124,.2);
            list-style: none;
        }


        .inactive {
            color: #ccc;
            background-color: #fafafa;
        }
    </style>

    <div class="page-header">

        <h2><?=$data["company_name"]?> 정산 기준서</h2>
    </div>

    <input type="hidden" name="ptype" value="taxproc">
<?php
$normal_sale       = 0; //  정상결제금액
$normal_sale_count = 0; // 정상결제카운트

$self_sale       = 0; //단기렌트금액
$self_sale_count = 0; //단기렌트건수

$month_sale       = 0; //월렌트 금액
$month_sale_count = 0; //월렌트 건수

$cancel_sale       = 0; // 취소수수료 금액
$cancel_sale_count = 0; //취소수수료 건수

$total_sale       = 0; // 총매출
$total_sale_count = 0; // 총 매출 카운트

$total_balance       = 0;  // 총 정산금액
$total_balance_count = 0;  // 총 정산 카운트
?>
    <fieldset>


                 <div class="col-md-12">
                    <div class="panel  panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"> ( <?=$arr_mst["calmainmst_startdate"]?>  ~ <?=$arr_mst["calmainmst_enddate"]?>  ) 정산

                                <div class="pull-right">
                                   
                                    &nbsp;&nbsp;&nbsp;
                                    <button type="button"  class="btn btn-default btn-xs" onclick="exceldown()">엑셀다운로드</button>
                                </div>
                            </h3>


                        </div>
                        <div class="panel-body"   style="height: 700px; overflow-y: scroll;" >

                            <div class="table-responsive">
                                <table class="table table-bordered">

                                    <tr class="success row">
                                        <th class="text-center col-md-2">단기렌트</th>
                                        <th class="text-center col-md-2">월렌트</th>
                                        <th class="text-center col-md-2">정상결제</th>
                                        <th class="text-center col-md-2">취소수수료</th>
                                        <th class="text-center col-md-2">총 매출</th>
                                        <th class="text-center col-md-2">총 정산</th>
                                    </tr>

                                    <tr class=" row">
                                        <th class="text-center" id="caldtl_summary_1"><?=$total1str?> 원/ <?=$cnt1str?>건</th>
                                        <th class="text-center" id="caldtl_summary_2"><?=$total2str?> 원/ <?=$cnt2str?>건</th>
                                        <th class="text-center" id="caldtl_summary_3"><?=$total3str?> 원/ <?=$cnt3str?>건</th>
                                        <th class="text-center" id="caldtl_summary_4"><?=$total4str?> 원/ <?=$cnt4str?>건</th>
                                        <th class="text-center " id="caldtl_summary_5"><?=$total5str?> 원/ <?=$cnt5str?>건</th>
                                        <th class="text-center " id="caldtl_summary_6"><?=$total6str?> 원/ <?=$cnt6str?>건</th>
                                    </tr>
                                </table>
                            </div>

                            <?php
                            if ($memo)
                            {
                                ?>
                                <div class="form-group">
                                    <label for="memo">정산 특이사항</label>
                                    <textarea id="memo" class="form-control" readonly style="background: white; resize: none"><?=$memo?></textarea>
                                </div>
                            <?php
                            }
                            ?>


                            <table class="table" >
                                <tr class="bg-info">
                                     <td class="small text-center"> 구분 </td>
                                    <td class="small  text-center"> 기간 </td>
                                    <td class="small text-center "> 고객명 </td>
                                    <td class="small text-center "> 기간 / 차량</td>
                                    <td class="small  text-center">원결제금액 </td>
                                    <td class="small  text-center"> 정산가격</td>
                                    <td class="small  text-center">수수료 (카드수수료 포함) </td>
                                    <td class="small text-center ">할인금액(카모아 부담)</td>
                                </tr>

                                <?php

                                $sumtotal=0;
                                $sumamount=0;
                                $sumfee=0;
                                $sumdiscount=0;

                                foreach ($arr_dtl as $item) {

                                    $car_number = $item["rentcar_number"];
                                    $calmaindtl_idx = $item["calmaindtl_idx"];
                                    $carmodel = $item["car_model"];
                                    $driver_name = $item["driver_name"];
                                    $calmaindtl_gubn = $item["calmaindtl_gubn"];

                                    $calmaindtl_period = $item["calmaindtl_period"];
                                    if($calmaindtl_period=="1"){
                                        $calmaindtl_periodstr="단기";
                                    }else if($calmaindtl_period=="2"){
                                        $calmaindtl_periodstr="월";
                                    }

                                    $calmaindtl_rentstart = $item["calmaindtl_rentstart"];
                                    $calmaindtl_rentend = $item["calmaindtl_rentend"];

                                    $calmaindtl_totalprice = $item["calmaindtl_totalprice"];
                                    $calmaindtl_calamount = $item["calmaindtl_calamount"];
                                    $calmaindtl_rentfee = $item["calmaindtl_rentfee"];
                                    $calmaindtl_discount= $item["calmaindtl_discount"];
                                    $calmaindtl_stats= $item["calmaindtl_stats"];

                                    if($calmaindtl_gubn=="1"){
                                        $calmaindtl_gubnstr="정상결제";
                                    }else if($calmaindtl_gubn=="2"){
                                        $calmaindtl_gubnstr="취소";
                                    }
                                    $calmaindtl_totalpricestr = number_format($calmaindtl_totalprice,0);
                                    $calmaindtl_calamountstr = number_format($calmaindtl_calamount,0);
                                    $calmaindtl_rentfeestr = number_format($calmaindtl_rentfee,0);
                                    $calmaindtl_discountstr = number_format($calmaindtl_discount,0);

                                         ?>

                                        <tr>
                                             <td class="small  text-center"> <?=$calmaindtl_gubnstr?> </td>
                                            <td class="small  text-center"> <?=$calmaindtl_periodstr?> </td>
                                            <td class="small  text-center"> <?=$driver_name?> </td>
                                            <td class="small  text-center"> <?=$calmaindtl_rentstart?> ~ <?=$calmaindtl_rentend?> <Br><?=$carmodel?> - <?=$car_number?>
                                            </td>
                                            <td class="small  text-center">
                                                <?=$calmaindtl_totalpricestr?> 원
                                            </td>
                                            <td class="small  text-center"><?=$calmaindtl_calamountstr?> 원 </td>
                                            <td class="small text-center "> <?=$calmaindtl_rentfeestr?> 원 </td>
                                            <td class="small  text-center">  <?=$calmaindtl_discountstr?> 원</td>
                                        </tr>
                                    <?
                                    $sumtotal=$calmaindtl_totalprice + $sumtotal;
                                    $sumamount=$sumamount+ $calmaindtl_calamount;
                                    $sumfee=$sumfee+ $calmaindtl_rentfee;
                                    $sumdiscount=$sumdiscount+ $calmaindtl_discount;

                                    // 요약본 데이터
                                    if ((int)$calmaindtl_gubn === 1)
                                    {
                                        $normal_sale += $calmaindtl_totalprice;
                                        $normal_sale_count++;

                                    }
                                    elseif ((int)$calmaindtl_gubn === 2)
                                    {

                                        $cancel_sale += $calmaindtl_totalprice;
                                        $cancel_sale_count++;

                                    }

                                    if ((int)$calmaindtl_period === 1)
                                    {
                                        $self_sale += $calmaindtl_totalprice;
                                        $self_sale_count++;

                                    } elseif ((int)$calmaindtl_period === 2)
                                    {
                                        $month_sale += $calmaindtl_totalprice;
                                        $month_sale_count++;

                                    }

                                    $total_sale += $calmaindtl_totalprice;
                                    $total_sale_count++;

                                    $total_balance += $calmaindtl_calamount;
                                    $total_balance_count++;


                                }

                                $sumtotal =number_format($sumtotal,0);
                                $sumamount =number_format($sumamount,0);
                                $sumfee =number_format($sumfee,0);
                                $sumdiscount =number_format($sumdiscount,0);

                                ?>
                                <tr>
                                    <td colspan="4" align="center"><B>실제 금액 및 결제 금액 총 합계</B></td>
                                    <td   align="center"><b><?=$sumtotal?></b> 원</td>
                                    <td  align="center" ><b><?=$sumamount?></b> 원</td>
                                    <td  align="center" ><b><?=$sumfee?></b> 원</td>
                                    <td   align="center"><b><?=$sumdiscount?></b> 원</td>
                                    <td  align="center" >&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>




    </fieldset>



</div>

<script>

    var mNormalSale = '<?=number_format($normal_sale)?>';
    var mNormalSaleCount = '<?=number_format($normal_sale_count)?>';
    var mSelfSale = '<?=number_format($self_sale)?>';
    var mSelfSaleCount = '<?=number_format($self_sale_count)?>';
    var mMonthSale= '<?=number_format($month_sale)?>';
    var mMonthSaleCount= '<?=number_format($month_sale_count)?>';
    var mCancelSale= '<?=number_format($cancel_sale)?>';
    var mCancelSaleCount= '<?=number_format($cancel_sale_count)?>';
    var mTotalSale= '<?=number_format($total_sale)?>';
    var mTotalSaleCount= '<?=number_format($total_sale_count)?>';
    var mTBalanceSal= '<?=number_format($total_balance)?>';
    var mTBalanceSaleCount= '<?=number_format($total_balance_count )?>';

    document.getElementById('caldtl_summary_3').textContent = mNormalSale.concat("원 / ", mNormalSaleCount, "건");
    document.getElementById('caldtl_summary_1').textContent = mSelfSale.concat("원 / ", mSelfSaleCount, "건");
    document.getElementById('caldtl_summary_2').textContent = mMonthSale.concat("원 / ", mMonthSaleCount, "건");
    document.getElementById('caldtl_summary_4').textContent = mCancelSale.concat("원 / ", mCancelSaleCount, "건");
    document.getElementById('caldtl_summary_5').textContent = mTotalSale.concat("원 / ", mTotalSaleCount, "건");
    document.getElementById('caldtl_summary_6').textContent = mTBalanceSal.concat("원 / ", mTBalanceSaleCount, "건");
</script>

