
<script src="/static/lib/dropzone/dropzone.js"></script>
<link href="/static/lib/dropzone/dropzone.css"rel="stylesheet">
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">

    <style>
        .double-input .form-control {
            width: 50%;
        }

        .show-grid [class^=col-] {
            padding-top: 10px;
            padding-bottom: 10px;
            border: 1px solid #ddd;
            border: 1px solid rgba(86,61,124,.2);
            list-style: none;
        }

        .glyphicon {
            margin-top: 5px;
            margin-bottom: 10px;
            font-size: 35px;
        }

        .inactive {
            color: #ccc;
            background-color: #fafafa;
        }
    </style>

    <div class="page-header">
        <h2>기간별 정산 등록</h2>
    </div>



    <form id="inputform" class="form-horizontal" role="form" method="post"   action="/application/views/calculate/calstep1_datains.php">
        <input type="hidden" name="ptype" value="taxproc">

        <fieldset>

            <div class="row " style="margin-top:15px">
                <ol class="show-grid col-md-8 col-md-offset-2">
                    <li class="col-md-4 active">
                        <div class="media">
                            <div class="pull-left" href="#">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </div>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>Step 1:</strong></h5>
                               정산시작일/마감일 설정
                            </div>
                        </div>
                    </li>
                    <li class="col-md-4 inactive">
                        <div class="media">
                            <div class="pull-left" href="#">
                                <span class="glyphicon glyphicon-folder-close"></span>
                            </div>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>Step 2:</strong></h5>
                                정산 데이터 확인 
                            </div>
                        </div>
                    </li>
                    <li class="col-md-4 inactive">
                        <div class="media">
                            <div class="pull-left" href="#">
                                <span class="glyphicon glyphicon-dashboard"></span>
                            </div>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>Step 3:</strong></h5>
                                세금계산서발행/완료
                            </div>
                        </div>
                    </li>
                </ol>
            </div>


            <div class="row col-md-12"  style="margin-top:25px">

                <div class="col-md-12">


                    <label for="shareCategory" class="col-md-2 control-label">정산날짜</label>
                    <div class="col-md-2">
                        <input type="date" class="form-control" name="calmainmst_startdate" placeholder="시작일"  value="<?=$data["calmainmst_startdate"]?>"   autofocus/>
                    </div>
                    <div class="col-md-2">
                        <input type="date" class="form-control" name="calmainmst_enddate" placeholder="마감일"  value="<?=$data["calmainmst_enddate"]?>"   autofocus/>
                    </div>


                </div>

                <div class="col-md-12"  style="margin-top:15px">


                    <label for="shareCategory" class="col-md-2 control-label">정산제목</label>
                    <div class="col-md-5">
                        <input type="text" class="form-control" name="calmainmst_title" placeholder="정산제목"  value="<?=$data["calmainmst_title"]?>"   autofocus/>
                    </div>


                </div>

            </div>


        </fieldset>

        <div class="clearfix" style="height: 20px"></div>

        <div class="row">
            <div class="col-md-3 col-md-offset-2"><button type="submit" class="btn btn-primary btn-block">정산시작하기</button></div>
            <div class="col-md-3"> <button type="reset" class="btn btn-warning btn-block">다시작성</button></div>
        </div>
    </form>

    <script type="text/javascript">

        $(function() {
            $("#inputform").submit(function() {
 
                if ($("input[name='calmainmst_startdate']").val() =="") {
                    alert("정산 시작일을 입력하세요.");
                    $("input[name='calmainmst_startdate']").focus();
                    return false;
                }
                if ($("input[name='calmainmst_enddate']").val() =="") {
                    alert("정산 마감일을 입력하세요.");
                    $("input[name='calmainmst_enddate']").focus();
                    return false;
                }
                if ($("input[name='calmainmst_title']").val() =="") {
                    alert("정산 제목을 입력하세요.");
                    $("input[name='calmainmst_title']").focus();
                    return false;
                }
                
                if (!confirm("정산 정보를 등록하시겠습니까?")) {

                    return false;
                }
            });

        });


    </script></div>
