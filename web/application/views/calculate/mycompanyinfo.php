
<script src="/static/lib/dropzone/dropzone.js"></script>
<link href="/static/lib/dropzone/dropzone.css"rel="stylesheet">
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">

    <style>
        .double-input .form-control {
            width: 50%;
        }
    </style>

    <div class="page-header">
        <h2>팀오투 사업자관리</h2>
    </div>



    <form id="inputform" class="form-horizontal" role="form" method="post" enctype="multipart/form-data"  action="/calculate/Taxinfo">
        <input type="hidden" name="ptype" value="taxproc">

        <fieldset>

            <div class="form-group">
                <label for="shareCategory" class="col-md-2 control-label">법인명</label>
                <div class="col-md-3">
                    <input type="text" class="form-control" name="company_name" placeholder="법인명"  value="<?=$data["company_name"]?>"   autofocus/>
                </div>
                <label for="shareCategory" class="col-md-2 control-label">등록번호</label>
                <div class="col-md-3">
                    <input type="text" class="form-control" name="company_number" placeholder="등록번호"  value="<?=$data["company_number"]?>"   autofocus/>
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">대표자 명</label>
                <div class="col-md-5">
                    <input type="text" class="form-control" name="ceo_name" placeholder="대표자 명"  value="<?=$data["ceo_name"]?>"   autofocus/>
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">주소</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" name="addr" placeholder="주소"  value="<?=$data["addr"]?>"   autofocus/>
                </div>
            </div>

            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">사업의 종류</label>
                <div class="col-md-3">
                    <input type="text" class="form-control" name="biztype" placeholder="업종" maxlength="50"  value="<?=$data["biztype"]?>"   autofocus/>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" name="bizclass" placeholder="업태" maxlength="50"  value="<?=$data["bizclass"]?>"   autofocus/>
                </div>

            </div>

            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">이메일</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" name="tax_email" placeholder="이메일" maxlength="100"  value="<?=$data["tax_email"]?>"   autofocus/>
                </div>

            </div>

            <hr>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary btn-block">저장하기</button>
                        </div>
                        <div class="col-md-6">
                            <button type="reset" class="btn btn-warning btn-block">다시작성</button>
                        </div>
                    </div>

                </div>
            </div>
        </fieldset>
    </form>

    <script type="text/javascript">

        $(function() {
            $("#inputform").submit(function() {

                if ($("input[name='company_name']").val() =="") {
                    alert("법인명을 입력하세요.");
                    $("input[name='company_name']").focus();
                    return false;
                }

                if ($("input[name='ceo_name']").val() =="") {
                    alert("대표자명을 입력하세요.");
                    $("input[name='ceo_name']").focus();
                    return false;
                }

                if ($("input[name='addr']").val() =="") {
                    alert("주소를 입력하세요.");
                    $("input[name='addr']").focus();
                    return false;
                }

                if ($("input[name='tax_email']").val() =="") {
                    alert("이메일을 입력하세요.");
                    $("input[name='tax_email']").focus();
                    return false;
                }

                if (!confirm("정말 등록하시겠습니까?")) {

                    return false;
                }
            });

        });


    </script></div>
