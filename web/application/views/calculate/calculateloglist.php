
<div class="container-fluid" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>

    <div class="page-header clearfix">
        <h2 class="pull-left">정산 내역 리스트</h2>
        <div class="pull-right" style="padding-top: 30px">
            <a href="?ptype=w" class="btn btn-info">정산등록하기</a>
        </div>
    </div>
    <div id="searchdiv">
        <form id="searchCompanyNotice" class="form-horizontal" role="form" method="get" >
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <select class="form-control" name="sp">
                                <option value="board_title" >제목</option>
                                <option value="board_content" >내용</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="sv" value="">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </fieldset>
        </form>
        <hr/>
    </div>


    <form id="updateConfirm">



        <div class="clearfix"></div>
        <br/>



        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">No</th>
                    <th class="text-center col-md-2">정산제목</th>
                    <th class="text-center col-md-2">상태</th>
                    <th class="text-center col-md-2">정산 시작일~마감일</th>
                    <th class="text-center col-md-1">정산상세</th>
                    <th class="text-center col-md-1">계산서/입금</th>
                    <th class="text-center col-md-1">삭제</th>
                    <th class="text-center col-md-2">등록일</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($data["list"] as $entry) {
                    ?>
                    <tr class="row">
                        <td  class="small text-center"><?=$entry["calmainmst_idx"]?> </td>
                        <td   class="small text-center"  ><?=$entry["calmainmst_title"]?></td>
                        <td   class="small text-center"><?=$entry["calmainmst_stepstr"]?> </td>

                        <td  style="padding-left: 10px;align:left"  class="small">  <?=$entry["calmainmst_startdate"]?> ~ <?=$entry["calmainmst_enddate"]?> </td>
                        <td   class="small text-center">
                            <button type="button" class="btn btn-primary btn-xs" onclick="go_caldtl('<?=$entry["calmainmst_code"]?>','step2')">상세보기</button>
                        </td>
                        <td   class="small text-center">
                            <?if($entry["calmainmst_step"]=="step3" || $entry["calmainmst_step"]=="step4"){?>
                            <button type="button" class="btn btn-info btn-xs" onclick="go_caldtl('<?=$entry["calmainmst_code"]?>','step3')">바로가기</button>
                            <?}?>
                        </td>

                        <td   class="small text-center">
                            <?if($entry["calmainmst_step"] !="step4"){?>
                            <button class="btn btn-danger btn-xs" onclick="deletecalculate('<?=$entry["calmainmst_code"]?>')">정산 삭제</button>
                            <?}?>
                        </td>
                        <td   class="small text-center"><?=$entry["regdate"]?></td>
                    </tr>
                <?}?>

                </tbody>
            </table>
        </div>
    </form>


    <div class="text-center">
        <?=$data['pagination']?>
    </div>

    <script language="JavaScript">

        function go_caldtl(dcode,stepv){
            if(stepv==="step2"){
                location.href="/calculate/Calculatelog?ptype=v&calmainmst_code="+dcode;
            }
            if(stepv==="step3"){
                location.href="/calculate/Calculatelog?ptype=finalcheck&calmainmst_code="+dcode;
            }
        }

        function deletecalculate(calmainmst_code){

            if(confirm('삭제하신 내용은 되돌릴 수 없습니다.\n선택한 정산정보를 삭제하시겠습니까?')){

                var postdata ={'ptype':'cancelcalculate' ,'calmainmst_code':calmainmst_code  }
                $.ajax({
                    url: "/calculate/Calculatelog",type: "post", data:postdata,
                    success: function (response) {
                        console.log("response:%o",response);

                        alert('* 정산정보 삭제가 완료되었습니다.');
                        location.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        //console.log("error");
                    },
                    complete:  function() {
                        //console.log("complete");

                    }
                });

            }
        }

    </script>