<?php

include ($_SERVER["DOCUMENT_ROOT"]."/application/config/aws_dbcon.php");
require_once("newBalanceData.php");
require_once("decrypt.php");


define("ID",$aws_db["username"],true);
define("PW",$aws_db["password"],true);
define("NAME",$aws_db["dbname"],true);
define("HOST",$aws_db["host"],true);

$conn = new PDO($aws_db["dns"], $aws_db["username"], $aws_db["password"]);
$conn->exec("set names utf8");

//  http://workspace2.teamo2.kr/application/views/calculate/test_caldatains.php

// 1일 ~15일 = 16일에 정산
// 16일~말일 = 익월 첫영업일


//////////////////////////////////////////////
/// // 마스터 정보 등록 //
$start = $_POST['calmainmst_startdate'];
$ending = $_POST['calmainmst_enddate'];
$calmainmst_title = $_POST['calmainmst_title'];
$end =date("Y-m-d 23:59:59",strtotime($ending));
$balance = $_POST['balance'];

$calmainmst_code = "CMS".time();


// 1. 겹치는 기간이 존재하면 error
$sql="SELECT (select  count(*) as cnt FROM caculate_calmainmst WHERE '$start' BETWEEN   calmainmst_startdate AND   calmainmst_enddate ) as cnt1 
        ,(select  count(*) as cnt FROM caculate_calmainmst WHERE '$ending' BETWEEN   calmainmst_startdate AND   calmainmst_enddate ) as cnt2 
        ,(select  ifnull( max(calmainmst_enddate) ,'')   FROM caculate_calmainmst WHERE '$ending' BETWEEN   calmainmst_startdate AND   calmainmst_enddate ) as maxend ";
$userQuery = $conn->prepare($sql);
$userQuery->execute();
$cntStmt = $userQuery->fetch();
$cnt1 =  $cntStmt['cnt1'];
$cnt2 =  $cntStmt['cnt2'];
$maxend =  $cntStmt['maxend'];


$totalcnt = $cnt1+$cnt2;
$totalcnt=0;

if($totalcnt > 0){

    if($maxend !=""){
        echo "<script>alert('이미 정산 완료된 기간을 선택하셨습니다. 최근 정산 마감일은 $maxend 입니다.');history.back();</script>";
    }else{
        echo "<script>alert('이미 정산 완료된 기간을 선택하셨습니다.');history.back();</script>";
    }
    exit();
}

$sql="INSERT INTO caculate_calmainmst (  calmainmst_code,calmainmst_title,  calmainmst_startdate,  calmainmst_enddate,calmainmst_step,regdate)
    VALUES  (    '$calmainmst_code','$calmainmst_title',    '$start',    '$ending' ,'step2',now() );";
$stmt = $conn->prepare($sql);
$stmt->execute();


//////////////////////////////////////////////
//  '$start 00:00:00' AND f_rentenddate <= '$ending 23:59:59'
$startime=$start." 00:00:00";
$endtime=$ending." 23:59:59";

$sql = "
       SELECT
           balance_tbl.*,
           IFNULL(am.admin_idx, 0) admin_idx
       FROM
       (
        SELECT trl.f_usrserial, trl.f_paystatus , trl.company_serial ,trl.f_companycode,trl.f_reservationidx,trl.f_carserial
        ,(SELECT settlement_rental_rate FROM new_rentCompany_branch WHERE SERIAL=trl.f_companycode) AS settlement_rental_rate
        ,tul.kakaoid, CONCAT(car_number1, car_number2, car_number3) rentcar_number, CONCAT(brand,' ', model)  as car_model, crti_reserv_rent_type
        , crti_start_date, crti_end_date, crti_early_return_apply, crti_early_return_use_cost, crti_principal, crti_payment
        , crti_rent_cost, crti_cdw_cost, crti_delivery_cost, crti_use_point, crti_use_coupon_value, nrs.driver_name ,crti.cancel_per,crti.crti_state
           FROM
               carmore_reservation_term_information crti INNER JOIN
               tbl_reservation_list trl INNER JOIN
               new_rentSchedule nrs INNER JOIN
               tbl_usr_list tul INNER JOIN
               new_rentCar rentcar INNER JOIN
               new_car car
               ON
                   crti_reservation_idx=trl.f_reservationidx AND trl.f_bookingid=nrs.serial AND tul.usrserial=trl.f_usrserial AND
                   trl.f_carserial=rentcar.serial AND rentcar.car_serial=car.serial
           WHERE
               nrs.driver_name NOT LIKE '팀오투테스트%' AND
                (
                (crti_reserv_rent_type=1 AND crti_end_date >= '$startime' AND crti_end_date <= '$endtime' AND crti_state=1) OR
                   (
                       crti_reserv_rent_type=2 AND
                       (
                       (crti_start_date >= '$startime' AND crti_start_date <= '$endtime' AND crti_state=1) 
                     )
                   ) or 
                   ( 
                   crti_state=2   and ( crti.cancel_per >0 and crti.cancel_per <1)   and  crti_reservation_idx in ( select f_reservationidx from tbl_cancel_log where f_regdate >= '$startime' AND f_regdate <= '$endtime' ) 
                   )
               )
           ) balance_tbl LEFT OUTER JOIN
           (select * from admin_member where  kakaoid <>'') am
           ON
               balance_tbl.kakaoid=am.kakaoid ";

$calquery = $conn -> prepare($sql);
$calquery -> execute();


while( $calrow=$calquery -> fetch() ) {

    $calmaindtl_company =$calrow['company_serial'];
    $calmaindtl_branch = $calrow['f_companycode'];
    $calmaindtl_gubn = $calrow['f_paystatus'];
    $calmaindtl_period =$calrow['crti_reserv_rent_type'];
    $calmaindtl_name =  $calrow['driver_name'];
    $f_reservationidx =  $calrow['f_reservationidx'];
    $reservationIdx = trim($f_reservationidx);
    $calmaindtl_rentstart = substr($calrow['crti_start_date']  , 0, 16);
    $calmaindtl_rentend = substr($calrow['crti_end_date'], 0, 16);


    $driver_name =  $calrow['driver_name'];
    $rentcar_number =  $calrow['rentcar_number'];
    $car_model =  $calrow['car_model'];
    $admin_idx =  $calrow['admin_idx'];

    $crti_state=  $calrow['crti_state'];
    $cancel_per=  $calrow['cancel_per'];

    if($admin_idx <> "0"){
        $calrow['crti_use_point']=0;
    }
    $f_usrserial=  $calrow['f_usrserial'];


    $calmaindtl_carserial = $calrow['f_carserial'];
    $settlement_rental_rate = $calrow['settlement_rental_rate'];
    $calmaindtl_totalprice = $calrow['crti_principal'];

    if($crti_state=="2"){
        $calmaindtl_totalprice = floor($calmaindtl_totalprice * (1-$cancel_per));
    }


    $calmaindtl_rentfee  = floor($calmaindtl_totalprice *  $settlement_rental_rate /100) ; // 정산비율 rate 곱하기
    $calmaindtl_calamount = floor($calmaindtl_totalprice-$calmaindtl_rentfee);
    $calmaindtl_discount = $calrow['crti_use_point']+$calrow['crti_use_coupon_value'];

    $sql = "INSERT INTO caculate_calmaindtl (   calmainmst_code,  calmaindtl_company,  calmaindtl_branch,  calmaindtl_gubn,
                              calmaindtl_period,  calmaindtl_userserial,  calmaindtl_rentstart,  calmaindtl_rentend,  calmaindtl_carserial,
                              calmaindtl_totalprice,  calmaindtl_calamount,  calmaindtl_rentfee,  calmaindtl_discount,  regdate,f_reservationidx
                              ,driver_name,  rentcar_number,  car_model,  admin_idx)
                              VALUES  (    '$calmainmst_code',    '$calmaindtl_company',    '$calmaindtl_branch',    '$calmaindtl_gubn',
                                  '$calmaindtl_period',    '$f_usrserial',    '$calmaindtl_rentstart',    '$calmaindtl_rentend',
                                  '$calmaindtl_carserial',    '$calmaindtl_totalprice',    '$calmaindtl_calamount', '$calmaindtl_rentfee',
                                  '$calmaindtl_discount',    now()  ,'$reservationIdx'
                                  ,'$driver_name','$rentcar_number','$car_model','$admin_idx');";


    $stmt = $conn->prepare($sql);
    $stmt->execute();
}

//////////// 지점 총계 데이터 등록

$sql="INSERT INTO caculate_calbranch(calmainmst_code,calmaindtl_company,calmaindtl_branch,dtlcnt)
  SELECT calmainmst_code,calmaindtl_company,calmaindtl_branch,COUNT(*) AS cnt FROM caculate_calmaindtl where  calmainmst_code='$calmainmst_code'
   GROUP BY calmainmst_code,calmaindtl_company,calmaindtl_branch";
$stmt = $conn->prepare($sql);
$stmt->execute();

echo "<script>alert('$start 에서 $ending 까지 초기 정산데이터 설정이 완료되었습니다. Step2 정산 데이터 세부설정을 진행해주세요');location.href='/calculate/Calculatelog';</script>";
exit();

?>