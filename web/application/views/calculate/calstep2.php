<?php
$arr_mst = $data["mst"];
$arr_list = $data["list"];
$arr_dtl = $data["dtl"];
$arr_caldtl= $data["calbranchdtl"];

$sumdata = $data["sumdata"];
$settlement_rental_rate= $data["settlement_rental_rate"] /100;

$cnt1str = number_format($sumdata["cnt1"],0);
$cnt2str = number_format($sumdata["cnt2"],0);
$cnt3str = number_format($sumdata["cnt3"],0);
$cnt4str = number_format($sumdata["cnt4"],0);
$cnt5str = number_format($sumdata["cnt5"],0);
$cnt6str = number_format($sumdata["cnt6"],0);


$total1str = number_format($sumdata["total1"],0);
$total2str = number_format($sumdata["total2"],0);
$total3str = number_format($sumdata["total3"],0);
$total4str = number_format($sumdata["total4"],0);
$total5str = number_format($sumdata["total5"],0);
$total6str = number_format($sumdata["total6"],0);

$calmainmst_code =  $data["calmainmst_code"];
$calmaindtl_branch =  $data["calmaindtl_branch"];
$mainbranchcalculate_yn=  $arr_caldtl["calculate_yn"];
$calmemo =$arr_caldtl["calmemo"];


$addcalmaindtl_rentstart = $arr_mst["calmainmst_startdate"];
$addcalmaindtl_rentend = $arr_mst["calmainmst_enddate"];
?>
<script src="/static/lib/dropzone/dropzone.js"></script>
<link href="/static/lib/dropzone/dropzone.css"rel="stylesheet">
<div class="container-fluid" style="padding-top: 50px;padding-bottom: 70px;">

    <style>
        .double-input .form-control {
            width: 50%;
        }

        .show-grid [class^=col-] {
            padding-top: 10px;
            padding-bottom: 10px;
            border: 1px solid #ddd;
            border: 1px solid rgba(86,61,124,.2);
            list-style: none;
        }


        .inactive {
            color: #ccc;
            background-color: #fafafa;
        }
    </style>

    <div class="page-header">

        <h2>정산 기준서 ( <?=$arr_mst["calmainmst_startdate"]?>  ~ <?=$arr_mst["calmainmst_enddate"]?>  )</h2>
    </div>

         <input type="hidden" name="ptype" value="taxproc">

        <fieldset>
            <div class="row col-md-12"  style="margin-top:5px">

                <div class="col-md-12">

                    <div class="col-md-2">

                        <div class="panel  panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">정산리스트

                                    <div class="pull-right">
                                        <button class="btn btn-danger btn-xs" onclick="showaddbranchmodal()">추가</button>
                                        <button class="btn btn-success btn-xs" onclick="allcalculatecomplete()">일괄정산</button>
                                    </div>

                                </h3>
                            </div>
                            <div class="panel-body"  style="height: 700px; overflow-y: scroll;">
                                <table class="table">
                                <?php
                                $datacalmaindtl_company = $data["rentCompany_serial"];
                                $datacalmaindtl_branch = $data["calmaindtl_branch"];
                                foreach ($arr_list as $item) {
                                    $companyname =$item["companyname"];
                                    $branchname =$item["branchname"];
                                    $branchserial =$item["branchserial"];
                                    $companyserial =$item["companyserial"];
                                    $calcnt = $item["calcnt"];
                                    $calculate_yn = $item["calculate_yn"];
                                    $bankins_yn = $item["bankins_yn"];
                                    $taxmail_yn = $item["taxmail_yn"];

                                    if($datacalmaindtl_branch== $branchserial){
                                        $activestr =" bg-primary";
                                        $selectinfo =$companyname ." > ".$branchname;
                                    }else{
                                        $activestr =" ";
                                    }

                                    if($calculate_yn=="y"){
                                        $activestr =" bg-success";
                                        $calstats="(완)";
                                    }else{
                                        $calstats="";
                                    }


                                    ?>
                                    <tr><td class="small <?=$activestr?>">
                                            <a href="/calculate/Calculatelog?ptype=v&calmainmst_code=<?=$calmainmst_code?>&calmaindtl_branch=<?=$branchserial?>" class="<?=$activestr?>">
                                             <?=$companyname?> > <?=$branchname?> (<?=$calcnt?>)
                                            </a>

                                        </td></tr>

                                <?}?>
                                </table>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-10">
                        <div class="panel  panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><?=$selectinfo?> 정산

                                    <div class="pull-right">
                                        <?if($mainbranchcalculate_yn=="n"){?>
                                            <button class="btn btn-default btn-xs" onclick="calculatecomplete()">정산확인</button>
                                            <button class="btn btn-default btn-xs" onclick="showtransmodal()">정산이동</button>
                                            <button class="btn btn-danger btn-xs" onclick="delcalculatedtl()">정산삭제</button>
                                            <button class="btn btn-info btn-xs" onclick="showaddtransmodal()">정산추가</button>
                                        <?}?>
                                        &nbsp;&nbsp;&nbsp;
                                        <button type="button"  class="btn btn-default btn-xs" onclick="location.href='/calculate/Calculatelog'">목록가기</button>
                                    </div>
                                </h3>


                            </div>
                            <div class="panel-body"   style="height: 700px; overflow-y: scroll;" >

                                <div class="table-responsive">
                                    <table class="table table-bordered">

                                        <tr class="success row">
                                            <th class="text-center col-md-2">정상결제</th>
                                            <th class="text-center col-md-2">단기렌트</th>
                                            <th class="text-center col-md-2">월렌트</th>
                                            <th class="text-center col-md-2">취소수수료</th>
                                            <th class="text-center col-md-2">총 매출</th>
                                            <th class="text-center col-md-2">총 정산</th>
                                        </tr>

                                        <tr class=" row">
                                            <td class="text-center"><?=$total1str?> 원/ <?=$cnt1str?>건</td>
                                            <td class="text-center"><?=$total2str?> 원/ <?=$cnt2str?>건</td>
                                            <td class="text-center"><?=$total3str?> 원/ <?=$cnt3str?>건</td>
                                            <td class="text-center"><?=$total4str?> 원/ <?=$cnt4str?>건</td>
                                            <td class="text-center "><?=$total5str?> 원/ <?=$cnt5str?>건</td>
                                            <td class="text-center "><?=$total6str?> 원/ <?=$cnt6str?>건</td>
                                        </tr>
                                        <tr class=" row">
                                            <td colspan="6">
                                                <textarea class="form-control" id="calmemo" name="calmemo" style="height:120px" placeholder="메모사항을 기입해주세요."><?=$calmemo?></textarea>
                                               <br> <input type="button" class="btn btn-primary btn-sm" onclick="savememo()" value="등록하기">

                                            </td>
                                        </tr>
                                    </table>
                                </div>


                                <table class="table" >
                                    <tr class="bg-info">
                                        <td class="small text-center"> <input type="checkbox" class="check-all"> </td>
                                        <td class="small text-center"> 구분 </td>
                                        <td class="small  text-center"> 기간 </td>
                                        <td class="small text-center "> 고객명 </td>
                                        <td class="small text-center "> 기간 / 차량</td>
                                        <td class="small  text-center">원결제금액 </td>
                                        <td class="small  text-center"> 정산가격</td>
                                        <td class="small  text-center">수수료 (카드수수료 포함) </td>
                                        <td class="small text-center ">할인금액(카모아 부담)</td>
                                        <td class="small text-center ">저장</td>
                                    </tr>

                                    <?php

                                    $sumtotal=0;
                                    $sumamount=0;
                                    $sumfee=0;
                                    $sumdiscount=0;
                                    $i=1;
                                     foreach ($arr_dtl as $item) {

                                         $car_number = $item["rentcar_number"];
                                         $calmaindtl_idx = $item["calmaindtl_idx"];
                                         $carmodel = $item["car_model"];
                                         $driver_name = $item["driver_name"];
                                         $calmaindtl_gubn = $item["calmaindtl_gubn"];
                                         
                                         $calmaindtl_period = $item["calmaindtl_period"];
                                         if($calmaindtl_period=="1"){
                                             $calmaindtl_periodstr="단기";
                                         }else if($calmaindtl_period=="2"){
                                             $calmaindtl_periodstr="월";
                                         }
                                         
                                         $calmaindtl_rentstart = $item["calmaindtl_rentstart"];
                                         $calmaindtl_rentend = $item["calmaindtl_rentend"];

                                         $calmaindtl_totalprice = $item["calmaindtl_totalprice"];
                                         $calmaindtl_calamount = $item["calmaindtl_calamount"];
                                         $calmaindtl_rentfee = $item["calmaindtl_rentfee"];
                                         $calmaindtl_discount= $item["calmaindtl_discount"];
                                         $calmaindtl_stats= $item["calmaindtl_stats"];

                                         if($calmaindtl_gubn=="1"){
                                             $calmaindtl_gubnstr="정상결제";                                             
                                         }else if($calmaindtl_gubn=="2"){
                                             $calmaindtl_gubnstr="<span style='color:red'>취소</span>";
                                         }
                                         $calmaindtl_totalpricestr = number_format($calmaindtl_totalprice,0);
                                         $calmaindtl_calamountstr = number_format($calmaindtl_calamount,0);
                                         $calmaindtl_rentfeestr = number_format($calmaindtl_rentfee,0);
                                         $calmaindtl_discountstr = number_format($calmaindtl_discount,0);

                                         if($calmaindtl_stats !="end"){
                                        ?>
                                        <tr>
                                            <td class="small  text-center"> <input type="checkbox" class="ab" name="calmaindtl_idx" value="<?=$calmaindtl_idx?>"> </td>
                                            <td class="small  text-center"> <?=$i?>. <?=$calmaindtl_gubnstr?> </td>
                                            <td class="small  text-center"> <?=$calmaindtl_periodstr?> </td>
                                            <td class="small  text-center"> <?=$driver_name?> </td>
                                            <td class="small  text-center"> <?=$calmaindtl_rentstart?> ~ <?=$calmaindtl_rentend?> <Br>
                                                <input type="text"  id="<?=$calmaindtl_idx?>_carmodel" style="width:160px" value="<?=$carmodel?>">   - <input type="text"  id="<?=$calmaindtl_idx?>_carnumber" style="width:100px" value="<?=$car_number?>">
                                            </td>
                                            <td class="small  text-center">
                                                <input type="text" id="<?=$calmaindtl_idx?>_totalprice" maxlength="8" style="width:60%" value="<?=$calmaindtl_totalprice?>" onkeypress="onlyNumber()" onkeyup="caldtlinfo('<?=$calmaindtl_idx?>','total')">
                                            </td>
                                            <td class="small  text-center"> <input type="text" id="<?=$calmaindtl_idx?>_calamount"   maxlength="8" style="width:60%" value="<?=$calmaindtl_calamount?>" onkeypress="onlyNumber()" onkeyup="caldtlinfo('<?=$calmaindtl_idx?>','amount')"> 원 </td>
                                            <td class="small text-center "> <input type="text"  id="<?=$calmaindtl_idx?>_carmorefee" style="width:60%" value="<?=$calmaindtl_rentfee?>" readonly> 원 </td>
                                            <td class="small  text-center">  <?=$calmaindtl_discountstr?> 원</td>
                                            <td class="small  text-center"> <button class="btn btn-primary btn-xs" onclick="savedtlrow('<?=$calmaindtl_idx?>')">저장</button></td>
                                        </tr>
                                    <? }else{?>
                                         <tr>
                                             <td class="small  text-center"> *</td>
                                             <td class="small  text-center"> <?=$i?>.  <?=$calmaindtl_gubnstr?> </td>
                                             <td class="small  text-center"> <?=$calmaindtl_periodstr?> </td>
                                             <td class="small  text-center"> <?=$calmaindtl_periodstr?> </td>
                                             <td class="small  text-center"> <?=$driver_name?> </td>
                                             <td class="small  text-center"> <?=$calmaindtl_rentstart?> ~ <?=$calmaindtl_rentend?> <Br><?=$carmodel?> - <?=$car_number?>
                                             </td>
                                             <td class="small  text-center">
                                                 <?=$calmaindtl_totalpricestr?> 원
                                             </td>
                                             <td class="small  text-center"><?=$calmaindtl_calamountstr?> 원 </td>
                                             <td class="small text-center "> <?=$calmaindtl_rentfeestr?> 원 </td>
                                             <td class="small  text-center">  <?=$calmaindtl_discountstr?> 원</td>
                                             <td class="small  text-center"> &nbsp;</td>
                                         </tr>
                                        <?}
                                         $i++;
                                         $sumtotal=$calmaindtl_totalprice + $sumtotal;
                                         $sumamount=$sumamount+ $calmaindtl_calamount;
                                         $sumfee=$sumfee+ $calmaindtl_rentfee;
                                         $sumdiscount=$sumdiscount+ $calmaindtl_discount;
                                     }

                                    $sumtotal =number_format($sumtotal,0);
                                    $sumamount =number_format($sumamount,0);
                                    $sumfee =number_format($sumfee,0);
                                    $sumdiscount =number_format($sumdiscount,0);

                                     ?>
                                    <tr>
                                        <td colspan="5" align="center"><B>실제 금액 및 결제 금액 총 합계</B></td>
                                        <td   align="center"><b><?=$sumtotal?></b> 원</td>
                                        <td  align="center" ><b><?=$sumamount?></b> 원</td>
                                        <td  align="center" ><b><?=$sumfee?></b> 원</td>
                                        <td   align="center"><b><?=$sumdiscount?></b> 원</td>
                                        <td  align="center" >&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </fieldset>



 </div>


<div id="transmodal" class="modal fade bs-modified-modal-sm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-danger ">정산정보 이동하기</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2">
                        <p class="form-control-static text-right">선택</p>
                    </div>
                    <div class="col-md-9">

                        <select class="form-control" id="selectbranch" ">
                        <?php
                        foreach ($arr_list as $item) {
                            $companyname =$item["companyname"];
                            $branchname =$item["branchname"];
                            $branchserial =$item["branchserial"];
                            $companyserial =$item["companyserial"];
                            $calcnt = $item["calcnt"];

                            ?>
                            <option value="<?=$branchserial?>"> <?=$companyname?> > <?=$branchname?></option>
                        <?}?>

                        </select>

                    </div>
                    <div class="col-md-1"></div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"  onclick="transinfo()">
                    <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="pconfirmBtn">이동하기</span>
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
            </div>
        </div>
    </div>
</div>



<div id="addtransmodal" class="modal fade bs-modified-modal-sm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-danger ">정산정보 추가하기</h4>
            </div>
            <div class="modal-body">
                <div class="row"  style="margin-top:15px">
                    <div class="col-md-3">
                        <p class="form-control-static text-right">정산구분</p>
                    </div>
                    <div class="col-md-9">

                        <select class="form-control" id="addcalmaindtl_gubn" ">
                        <option value="1">정상결제</option>
                        <option value="2">취소</option>

                        </select>

                    </div>
                </div>
                <div class="row"  style="margin-top:15px">
                    <div class="col-md-3">
                        <p class="form-control-static text-right">렌트기간</p>
                    </div>
                    <div class="col-md-9">

                        <select class="form-control" id="addcalmaindtl_period" ">
                        <option value="1">단기렌트</option>
                        <option value="2">월렌트</option>

                        </select>

                    </div>
                </div>
                <div class="row"  style="margin-top:15px">
                    <div class="col-md-3">
                        <p class="form-control-static text-right">고객명</p>
                    </div>
                    <div class="col-md-5">
                        <input type="text" name="addriver_name"  id="addriver_name"     class="form-control" value="">
                    </div>
                </div>

                <div class="row"  style="margin-top:15px">
                    <div class="col-md-3">
                        <p class="form-control-static text-right">기간</p>
                    </div>
                    <div class="col-md-4">
                        <input type="date" class="form-control" name="addcalmaindtl_rentstart"  id="addcalmaindtl_rentstart" placeholder="시작일"  value="<?=$addcalmaindtl_rentstart?>"   autofocus/>
                    </div>
                    <div class="col-md-4">
                        <input type="date" class="form-control" name="addcalmaindtl_rentend"  id="addcalmaindtl_rentend"  placeholder="마감일" value="<?=$addcalmaindtl_rentend?>"   autofocus/>
                    </div>
                </div>

                <div class="row"  style="margin-top:15px">
                    <div class="col-md-3">
                        <p class="form-control-static text-right">차량모델</p>
                    </div>
                    <div class="col-md-5">
                        <input type="text" name="addcar_model"  id="addcar_model"   class="form-control" value=""  >
                    </div>
                </div>
                <div class="row"  style="margin-top:15px">
                    <div class="col-md-3">
                        <p class="form-control-static text-right">차량번호</p>
                    </div>
                    <div class="col-md-5">
                        <input type="text" name="addrentcar_number" id="addrentcar_number"     class="form-control" value=""  >
                    </div>
                </div>
                <div class="row"  style="margin-top:15px">
                    <div class="col-md-3">
                        <p class="form-control-static text-right">원결제</p>
                    </div>
                    <div class="col-md-5">
                        <input  type="text" maxlength="8"    name="addcalmaindtl_totalprice"  id="addcalmaindtl_totalprice"  value="0"    class="form-control"   >
                    </div>
                </div>
                <div class="row" style="margin-top:15px">
                    <div class="col-md-3">
                        <p class="form-control-static text-right">정산금액</p>
                    </div>
                    <div class="col-md-5">
                        <input  type="text" maxlength="8"  name="addcalmaindtl_calamount"  id="addcalmaindtl_calamount"   value="0"      class="form-control"  >
                    </div>
                </div>
                <div class="row" style="margin-top:15px">
                    <div class="col-md-3">
                        <p class="form-control-static text-right">수수료</p>
                    </div>
                    <div class="col-md-5">
                        <input  type="text" maxlength="8"   name="addcalmaindtl_rentfee" id="addcalmaindtl_rentfee"  value="0"  class="form-control"    >
                    </div>
                </div>
                <div class="row" style="margin-top:15px">
                    <div class="col-md-3">
                        <p class="form-control-static text-right">할인금액</p>
                    </div>
                    <div class="col-md-5">
                        <input type="text" maxlength="8"  name="addcalmaindtl_discount" id="addcalmaindtl_discount"  value="0"  class="form-control"   >
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"  onclick="insnewcaldata()">
                    <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="insbtn">등록하기</span>
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
            </div>
        </div>
    </div>
</div>




<div id="addbranchmodal" class="modal fade bs-modified-modal-sm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-danger ">정산업체 추가하기</h4>
            </div>
            <div class="modal-body">

                <div class="row"  style="margin-top:15px">
                    <div class="col-md-3">
                        <p class="form-control-static text-right">회사선택</p>
                    </div>
                    <div class="col-md-6">

                        <select class="form-control" name="companyserial" id="companyserial" onchange="companychange(this.value,'branchserial')">
                            <option value="">==== 선택 ====</option>
                            <?=$data["select_company"]?>
                        </select>

                    </div>
                </div>
                <div class="row"  style="margin-top:15px">
                    <div class="col-md-3">
                        <p class="form-control-static text-right">지점선택</p>
                    </div>
                    <div class="col-md-6">

                        <select class="form-control" name="branchserial" id="branchserial" >
                            <option value="">==== 선택 ====</option>
                        </select>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"  onclick="insnewbranch()">
                    <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="insbtn">등록하기</span>
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
            </div>
        </div>
    </div>
</div>



<script>
    var rental_rate="<?=$settlement_rental_rate?>";
    $( document ).ready( function() {
        $( '.check-all' ).click( function() {
            $( '.ab' ).prop( 'checked', this.checked );
        } );
    } );


    // 정산 대상회사를 수동으로 추가한다.
    function showaddbranchmodal(){
        $("#addbranchmodal").modal().on("hidden.bs.modal", function() {

        });
    }

    function insnewbranch(){
        //companyserial branchserial
        var companyserial =$("#companyserial").val();
        var branchserial =$("#branchserial").val();
        var calmainmst_code="<?=$calmainmst_code?>";

        if(companyserial===""){
            alert('회사를 선택해주세요');
            return;
        }
        if(branchserial===""){
            alert('지점을 선택해주세요');
            return;
        }

        if(confirm('정산대상 지점을 추가하시겠습니까?')){

            var postdata ={'ptype':'addcalbranch' ,'calmainmst_code':calmainmst_code,'calmaindtl_company':companyserial,'calmaindtl_branch':branchserial}
            $.ajax({
                url: "/calculate/Calculatelog",type: "post", data:postdata,
                success: function (response) {
                    console.log("response:%o",response);
                    var json = $.parseJSON(response);
                    var result= json["result"];

                    if(result==="n"){
                        alert('이미 추가되어 있는 지점입니다! ');
                        return;
                    }else if(result==="y"){
                        alert('신규 지점이 추가되었습니다.');
                        location.reload();
                    }

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //console.log("error");
                },
                complete:  function() {
                    //console.log("complete");

                }
            });
        }

    }


    function companychange(companyserial,targetid,initvalue="") {
        $("select[id='"+targetid+"'] option").remove();
        // $('select').children('option:not(:first)').remove();


        $("#"+targetid).append("<option value=''>==== 선택 ====</option>");
        $.ajax({
            type:"post",contentType: "application/json",
            url:"/partners/PartnerMemberBranch?ptype=code&companyserial="+companyserial,
            datatype: "json",
            success: function(data) {


                var json = $.parseJSON(data);
                for(var i=0;i < json.length ; i++) {

                    var row = json[i];
                    var branchname = row["branchName"];
                    var serial = row["serial"];
                    var carmore_normal_available  = row["carmore_normal_available"];
                    var carmore_month_available   = row["carmore_month_available"];

                    console.log("carmore_normal_available:"+carmore_normal_available+",carmore_month_available:"+carmore_month_available)
                    if(serial ===initvalue){
                        $("#"+targetid).append("<option value='"+serial+"' selected>"+branchname+"</option>");
                    }else{
                        $("#"+targetid).append("<option value='"+serial+"'>"+branchname+"</option>");
                    }

                }
            },
            error: function(x, o, e) {

            }
        });
    }


    function savememo(){

        var calmainmst_code="<?=$calmainmst_code?>";
        var calmaindtl_branch="<?=$calmaindtl_branch?>";
        var calmemo =$("#calmemo").val();

        if(confirm('*메모를 등록 하시겠습니까? ')){


            var postdata ={'ptype':'savecalmemo' ,'calmainmst_code':calmainmst_code,'calmaindtl_branch':calmaindtl_branch
            ,'calmemo':calmemo}
            $.ajax({
                url: "/calculate/Calculatelog",type: "post", data:postdata,
                success: function (response) {
                    console.log("response:%o",response);

                    location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //console.log("error");
                },
                complete:  function() {
                    //console.log("complete");

                }
            });

        }

    }





    function insnewcaldata(){


        var calmaindtl_branch ="<?=$datacalmaindtl_branch?>";
        var calmainmst_code="<?=$calmainmst_code?>";
        var calmaindtl_company="<?=$datacalmaindtl_company?>";
        var addcalmaindtl_gubn= $("#addcalmaindtl_gubn").val();
        var addcalmaindtl_period= $("#addcalmaindtl_period").val();
        var addriver_name= $("#addriver_name").val();
        var addcalmaindtl_rentstart= $("#addcalmaindtl_rentstart").val();
        var addcalmaindtl_rentend= $("#addcalmaindtl_rentend").val();
        var addcar_model= $("#addcar_model").val();
        var addrentcar_number= $("#addrentcar_number").val();
        var addcalmaindtl_totalprice= $("#addcalmaindtl_totalprice").val();
        var addcalmaindtl_calamount= $("#addcalmaindtl_calamount").val();
        var addcalmaindtl_rentfee= $("#addcalmaindtl_rentfee").val();
        var addcalmaindtl_discount= $("#addcalmaindtl_discount").val();


        if(addcalmaindtl_gubn===""){
            alert('* 정산 구분을 선택해주세요! ');
            return;
        }
        if(addcalmaindtl_period===""){
            alert('* 렌트 기간을 선택해주세요! ');
            return;
        }
        if(addriver_name===""){
            alert('* 고객명을 기입해주세요! ');
            $("#addriver_name").focus();
            return;
        }
        if(addcalmaindtl_rentstart===""){
            alert('* 렌트 시작일을 기입해주세요! ');
            $("#addcalmaindtl_rentstart").focus();
            return;
        }
        if(addcalmaindtl_rentend===""){
            alert('* 렌트 마감일을 기입해주세요! ');
            $("#addcalmaindtl_rentend").focus();
            return;
        }

        var ar1 = addcalmaindtl_rentstart.split('-');
        var ar2 = addcalmaindtl_rentend.split('-');
        var da1 = new Date(ar1[0], ar1[1], ar1[2]);
        var da2 = new Date(ar2[0], ar2[1], ar2[2]);
        var dif = da2 - da1;

        if(dif < 1){
            alert('* 렌트 시작일과 마감일을 확인 해주세요! ');
            $("#addcalmaindtl_rentend").focus();
            return;
        }

        if(addcar_model===""){
            alert('* 차량 모델명을 기입해주세요! ');
            $("#addcar_model").focus();
            return;
        }
        if(addrentcar_number===""){
            alert('* 차량 번호를 기입해주세요! ');
            $("#addrentcar_number").focus();
            return;
        }
        if(addcalmaindtl_totalprice===""){
            alert('* 원결제금액을 기입해주세요! ');
            $("#addcalmaindtl_totalprice").focus();
            return;
        }

        if(addcalmaindtl_calamount===""){
            alert('* 정산금액을 기입해주세요! ');
            $("#addcalmaindtl_calamount").focus();
            return;
        }
        if(addcalmaindtl_rentfee===""){
            alert('* 수수료를 기입해주세요! ');
            $("#addcalmaindtl_calamount").focus();
            return;
        }

        if(addcalmaindtl_discount===""){
            alert('* 할인금액을 기입해주세요! ');
            $("#addcalmaindtl_discount").focus();
            return;
        }

        if(confirm('*신규 정산데이터를 등록 하시겠습니까?')){


            var postdata ={'ptype':'setnewcaldata' ,'calmainmst_code':calmainmst_code,'calmaindtl_branch':calmaindtl_branch
                ,'calmaindtl_company':calmaindtl_company
                ,'calmaindtl_gubn':addcalmaindtl_gubn ,'calmaindtl_period':addcalmaindtl_period
                ,'driver_name':addriver_name ,'calmaindtl_rentstart':addcalmaindtl_rentstart
                ,'calmaindtl_rentend':addcalmaindtl_rentend ,'car_model':addcar_model
                ,'rentcar_number':addrentcar_number ,'calmaindtl_totalprice':addcalmaindtl_totalprice
                ,'calmaindtl_calamount':addcalmaindtl_calamount ,'calmaindtl_rentfee':addcalmaindtl_rentfee
                ,'calmaindtl_discount':addcalmaindtl_discount }

            console.log("postdata:%o",postdata);
            $.ajax({
                url: "/calculate/Calculatelog",type: "post", data:postdata,
                success: function (response) {
                    console.log("response:%o",response);

                    alert('* 신규데이터 등록이 완료되었습니다.');
                   location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //console.log("error");
                },
                complete:  function() {
                    //console.log("complete");

                }
            });

        }

    }


    function  allcalculatecomplete(){

        var calmainmst_code="<?=$calmainmst_code?>";
        var calmaindtl_branch="<?=$calmaindtl_branch?>";

        if(confirm('*전체 정산데이터를 정산 완료 하시겠습니까? ')){

            var postdata ={'ptype':'allcalculatecomplete' ,'calmainmst_code':calmainmst_code}
            $.ajax({
                url: "/calculate/Calculatelog",type: "post", data:postdata,
                success: function (response) {
                    console.log("response:%o",response);

                    alert('* 상태 변경이 완료되었습니다.');
                     location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //console.log("error");
                },
                complete:  function() {
                    //console.log("complete");

                }
            });

        }

    }


    function calculatecomplete(){
        var checkedcnt = $('input[name="calmaindtl_idx"]:checked').length  ;
        var allcnt = $('input[name="calmaindtl_idx"]').length  ;

        var calmainmst_code="<?=$calmainmst_code?>";
        var calmaindtl_branch="<?=$calmaindtl_branch?>";

        console.log("checkcnt:"+checkedcnt+", allcnt:"+allcnt)
        if(checkedcnt !== allcnt){
            alert('*모든 정산데이터를 선택하셔야 정산확인을 진행할 수 있습니다..');
            return;
        }

        if(confirm('*정산완료 데이터는 더이상 수정할 수 없습니다. \n\n선택하신 정산데이터를 정산완료하시겠습니까? ')){
            var calmaindtl_idx= new Array();
            $("input[name=calmaindtl_idx]:checked").each(function() {
                calmaindtl_idx.push($(this).val());
            });

            var strcalmaindtl_idx = calmaindtl_idx.join("-");

            var postdata ={'ptype':'calculatecomplete' ,'calmainmst_code':calmainmst_code,'calmaindtl_branch':calmaindtl_branch
                ,'strcalmaindtl_idx':strcalmaindtl_idx }
            $.ajax({
                url: "/calculate/Calculatelog",type: "post", data:postdata,
                success: function (response) {
                    console.log("response:%o",response);

                    alert('* 상태 변경이 완료되었습니다.');
                    location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //console.log("error");
                },
                complete:  function() {
                    //console.log("complete");

                }
            });

        }

    }


    function delcalculatedtl(){
        var checkedcnt = $('input[name="calmaindtl_idx"]:checked').length  ;
       
        var calmainmst_code="<?=$calmainmst_code?>";
        var calmaindtl_branch="<?=$calmaindtl_branch?>";

        console.log("checkcnt:"+checkedcnt);
        if(checkedcnt < 1){
            alert('*삭제하실 정산 데이터를 선택해주세요.');
            return;
        }
        if(confirm('*삭제하신 데이터는 복구할 수 없습니다. \n\n선택하신 정산데이터를 삭제 하시겠습니까? ')){
            var calmaindtl_idx= new Array();
            $("input[name=calmaindtl_idx]:checked").each(function() {
                calmaindtl_idx.push($(this).val());
            });

            var strcalmaindtl_idx = calmaindtl_idx.join("-");

            var postdata ={'ptype':'delcalculate' ,'calmainmst_code':calmainmst_code ,'calmaindtl_branch':calmaindtl_branch
                ,'strcalmaindtl_idx':strcalmaindtl_idx }
            $.ajax({
                url: "/calculate/Calculatelog",type: "post", data:postdata,
                success: function (response) {
                    console.log("response:%o",response);

                    alert('*반드시 일괄정산이나 정산확인을 진행후 세금계산서발행을 진행해주세요.\n\n 정보삭제가 완료되었습니다. ');
                    location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //console.log("error");
                },
                complete:  function() {
                    //console.log("complete");

                }
            });

        }

    }


    function caldtlinfo(calmaindtl_idx,part){
        var frental_rate = parseFloat(rental_rate);
        var totalid = calmaindtl_idx+"_totalprice";
        var amountid = calmaindtl_idx+"_calamount";
        var carmorefeeid = calmaindtl_idx+"_carmorefee";

        if(part==="total"){
            var totalprice = $("#"+totalid).val();
            var carmorefee = parseInt(totalprice)*frental_rate;
            carmorefee=parseInt(carmorefee);
            var amountprice = totalprice-carmorefee;

            $("#"+amountid).val(amountprice);
            $("#"+carmorefeeid).val(carmorefee);
        }else if(part==="amount"){
            var totalprice = $("#"+totalid).val();
            var amountprice = $("#"+amountid).val();

            var carmorefee = parseInt(totalprice) -parseInt(amountprice);

            if(carmorefee >0){
                $("#"+carmorefeeid).val(carmorefee);
            }else{
                $("#"+amountid).val(totalprice);
                $("#"+carmorefeeid).val(0);
            }
        }
    }


    function calnewins(part){
        var frental_rate = parseFloat(rental_rate);
        var totalid =  "addcalmaindtl_totalprice";
        var amountid = "addcalmaindtl_calamount";
        var carmorefeeid = "addcalmaindtl_rentfee";

        if(part==="total"){
            var totalprice = $("#"+totalid).val();
            var carmorefee = parseInt(totalprice)*frental_rate;
            carmorefee=parseInt(carmorefee);
            var amountprice = totalprice-carmorefee;

            $("#"+amountid).val(amountprice);
            $("#"+carmorefeeid).val(carmorefee);
        }else if(part==="amount"){
            var totalprice = $("#"+totalid).val();
            var amountprice = $("#"+amountid).val();

            var carmorefee = parseInt(totalprice) -parseInt(amountprice);

            if(carmorefee >0){
                $("#"+carmorefeeid).val(carmorefee);
            }else{
                $("#"+amountid).val(totalprice);
                $("#"+carmorefeeid).val(0);
            }
        }
    }


    function savedtlrow(calmaindtl_idx){

        if(confirm('*금액을 수정처리하시겠습니까?')){

            var totalid = calmaindtl_idx+"_totalprice";
            var amountid = calmaindtl_idx+"_calamount";
            var carmorefeeid = calmaindtl_idx+"_carmorefee";
            var carmorecarmodelid = calmaindtl_idx+"_carmodel";
            var carmorecarnumberid = calmaindtl_idx+"_carnumber";


            var totalprice = $("#"+totalid).val();
            var amountprice = $("#"+amountid).val();
            var feeprice = $("#"+carmorefeeid).val();
            var carmodel = $("#"+carmorecarmodelid).val();
            var carnumber = $("#"+carmorecarnumberid).val();

            var postdata ={'ptype':'changedtlrow' ,'calmaindtl_idx':calmaindtl_idx ,'calmaindtl_totalprice':totalprice
                ,'car_model':carmodel,'rentcar_number':carnumber,'calmaindtl_calamount':amountprice
                ,'calmaindtl_rentfee':feeprice}
            $.ajax({
                url: "/calculate/Calculatelog",type: "post", data:postdata,
                success: function (response) {
                    console.log("response:%o",response);

                    alert('* 금액 변경이 완료되었습니다.');
                    location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //console.log("error");
                },
                complete:  function() {
                    //console.log("complete");

                }
            });

        }
    }


    function showtransmodal(){
        // 체크박스 확인
        var checkedcnt = $('input[name="calmaindtl_idx"]:checked').length  ;

        if(checkedcnt < 1){
            alert('* 선택된 정산내역이 없습니다.');
            return;
        }
        $("#transmodal").modal().on("hidden.bs.modal", function() {

            branchserial="";
            bintrocode="";
            bintromsg="";
        });
    }

    function showaddtransmodal(){
        $("#addtransmodal").modal().on("hidden.bs.modal", function() {

            branchserial="";
            bintrocode="";
            bintromsg="";
        });
    }


    function transinfo(){
        var selectbranch =$("#selectbranch").val();
        var nowbranch ="<?=$datacalmaindtl_branch?>";
        var calmainmst_code="<?=$calmainmst_code?>";

        if(selectbranch===nowbranch){
            alert('*동일한 지점을 선택하셨습니다.');
            return;
        }

        if(confirm('*선택하신 지점으로 정산데이터를 이동하시겠습니까?')){
            var calmaindtl_idx= new Array();
            $("input[name=calmaindtl_idx]:checked").each(function() {
                calmaindtl_idx.push($(this).val());
            });

            var strcalmaindtl_idx = calmaindtl_idx.join("-");

            var postdata ={'ptype':'changebranch' ,'selectbranch':selectbranch,'nowbranch':nowbranch
                ,'strcalmaindtl_idx':strcalmaindtl_idx ,'calmainmst_code':calmainmst_code}
            $.ajax({
                url: "/calculate/Calculatelog",type: "post", data:postdata,
                success: function (response) {
                    console.log("response:%o",response);

                   alert('* 상태 변경이 완료되었습니다.');
                    location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //console.log("error");
                },
                complete:  function() {
                    //console.log("complete");

                }
            });

        }
    }

    function onlyNumber(){

        if((event.keyCode<48)||(event.keyCode>57))

            event.returnValue=false;

    }

    function checknumber(obj){
        var numv = obj.value;

        if(isNaN(numv) ){
            obj.value="0";
        }
    }

</script>