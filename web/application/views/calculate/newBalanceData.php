<?php

//고고 경인 GT 는 우리가 먹는 금액 10% ( 카드 수수료 여기서 제외)
//다른 곳은 우리가 먹는 금액 10% + 카드 수수료 ( 카드수수료 여기서 제외)

class NewBalancePrimium{        //고고,경인,GT
    const point = 0.03;                 //포인트
    const cardCommissionRate = 0.032;        //카드수수료
    const balance = 0.9;                //90% 정산
    const penalty = 0.1;
   // const semicancelBalance = 0.9648;   //1-카드수수료 (e.g. 카드 : 0.03 => 0.97)
    
    protected $rentPrice,$insurancePrice,$deliveryPrice,$cardPrice,$pointPrice,$couponPrice,$originalPrice;        
        
    function __construct($rentPrice_ ,$insurancePrice_ ,$deliveryPrice_ ,$realpayPrice_ ,$usepointPrice_, $usecouponPrice_, $originalPrice_){
        $this->rentPrice = $rentPrice_;
        $this->insurancePrice = $insurancePrice_;
        $this->deliveryPrice = $deliveryPrice_;
        $this->cardPrice = $realpayPrice_;
        $this->pointPrice = $usepointPrice_;
        $this->couponPrice = $usecouponPrice_;
        $this->originalPrice = $originalPrice_;
    }
    
}

class newNormalBalancePrimium extends NewBalancePrimium{
    
    private $balanceAmmount, $benefitAmmount, $cardCommission, $givePoint;
    function __construct( $rentPrice_ ,$insurancePrice_ ,$deliveryPrice_ ,$realpayPrice_ ,$usepointPrice_, $usecouponPrice_, $originalPrice_ ){
        parent::__construct( $rentPrice_ ,$insurancePrice_ ,$deliveryPrice_ ,$realpayPrice_ ,$usepointPrice_, $usecouponPrice_, $originalPrice_ );
        
        $this -> balanceAmmount = floor($this->originalPrice * parent::balance);
        $this -> cardCommission = floor( $this->cardPrice * parent::cardCommissionRate );
        $this -> givePoint = floor( $this->cardPrice * parent::point );
        $this -> benefitAmmount = floor($this->originalPrice - $this->balanceAmmount - $this->cardCommission-$this->givePoint);
    }
    
    public function getBalanceAmmount(){ return $this->balanceAmmount; }
    public function getCardCommission(){ return $this->cardCommission; }
    public function getGivePoint(){ return $this->givePoint; }
    public function getBenefitAmmount(){ return $this->benefitAmmount; }
}

//당일 취소
class newSemicancelBalancePrimium extends NewBalancePrimium{
    
    private $cancellationCharge, $balanceAmmount, $cancellationChargeForUser, $returnPoint, $cardCommission, $benefitAmmount, $usePoint;
    private $cancel_per = 0;

    function __construct($rentPrice_ ,$insurancePrice_ ,$deliveryPrice_ ,$realpayPrice_ ,$usepointPrice_, $usecouponPrice_, $originalPrice_, $cancel_per){
        parent::__construct($rentPrice_ ,$insurancePrice_ ,$deliveryPrice_ ,$realpayPrice_ ,$usepointPrice_, $usecouponPrice_, $originalPrice_);
        $this->cancel_per = 1-floatval($cancel_per);

        $this -> cancellationCharge = floor($this->originalPrice*$this->cancel_per);                         //위약금
        $this -> balanceAmmount = floor( $this->cancellationCharge * parent::balance );
        $this -> cancellationChargeForUser = floor($this->cancellationCharge - $this->pointPrice);         //고객 부담 위약금
        if( $this -> cancellationChargeForUser < 0 ){
            $this -> returnPoint = $this->cancellationChargeForUser*(-1);
            $this -> usepoint = $this->pointPrice - $this->returnPoint;                             //사용포인트
            $this -> cancellationChargeForUser =0 ;
        } else {
            $this -> returnPoint = 0;
            $this -> usePoint = $this -> pointPrice;
        }
        
        $this -> cardCommission = floor($this->cancellationChargeForUser * parent::cardCommissionRate);
        $this -> benefitAmmount = floor($this->cancellationCharge - $this->balanceAmmount - $this->cardCommission + $this->couponPrice);
        
    }
    
    public function getBalanceAmmount(){ return $this->balanceAmmount; }
    public function getCardCommission(){ return $this->cardCommission; }
    public function getBenefitAmmount(){ return $this->benefitAmmount; }
    public function getCancellationCharge(){ return $this->cancellationCharge; }
    public function getCancellationChargeForUser(){ return $this->cancellationChargeForUser; }
    public function getUsePoint(){ return $this->usePoint; }
    public function getGivePoint(){ return 0; }
}

//완전 취소
class newCompleteCancelBalancePrimium extends NewBalancePrimium{

    private $balanceAmmount,$cardCommission,$benefitAmmount,$usePoint,$cancellationChargeForUser;

    function __construct($rentPrice_ ,$insurancePrice_ ,$deliveryPrice_ ,$realpayPrice_ ,$usepointPrice_, $usecouponPrice_, $originalPrice_){
        parent::__construct($rentPrice_ ,$insurancePrice_ ,$deliveryPrice_ ,$realpayPrice_ ,$usepointPrice_, $usecouponPrice_, $originalPrice_);

        $this->balanceAmmount =0;
        $this->cardCommission =0;
        $this->benefitAmmount = $this->couponPrice;
        $this->usePoint =0;
        $this->cancellationChargeForUser=0;
    }
    
    public function getBalanceAmmount(){ return $this->balanceAmmount; }
    public function getCardCommission(){ return $this->cardCommission; }
    public function getBenefitAmmount(){ return $this->benefitAmmount; }
    public function getCancellationChargeForUser(){ return $this->cancellationChargeForUser; }
    public function getUsePoint(){ return $this->usePoint; }
    public function getGivePoint(){ return 0; }
}



class NewBalance{        //고고,경인,GT
    const point = 0.03;                 //포인트
    const cardCommissionRate = 0.032;        //카드수수료
    const balance = 0.868;                //90% 정산
    const penalty = 0.1;
   // const semicancelBalance = 0.9648;   //1-카드수수료 (e.g. 카드 : 0.03 => 0.97)
    
    protected $rentPrice,$insurancePrice,$deliveryPrice,$cardPrice,$pointPrice,$couponPrice,$originalPrice;        
        
    function __construct($rentPrice_ ,$insurancePrice_ ,$deliveryPrice_ ,$realpayPrice_ ,$usepointPrice_, $usecouponPrice_, $originalPrice_){
        $this->rentPrice = $rentPrice_;
        $this->insurancePrice = $insurancePrice_;
        $this->deliveryPrice = $deliveryPrice_;
        $this->cardPrice = $realpayPrice_;
        $this->pointPrice = $usepointPrice_;
        $this->couponPrice = $usecouponPrice_;
        $this->originalPrice = $originalPrice_;
    }
    
}


class newNormalBalance extends NewBalance{
    
    private $balanceAmmount, $benefitAmmount, $cardCommission, $givePoint;
    function __construct( $rentPrice_ ,$insurancePrice_ ,$deliveryPrice_ ,$realpayPrice_ ,$usepointPrice_, $usecouponPrice_, $originalPrice_ ){
        parent::__construct( $rentPrice_ ,$insurancePrice_ ,$deliveryPrice_ ,$realpayPrice_ ,$usepointPrice_, $usecouponPrice_, $originalPrice_ );
        
        $this -> balanceAmmount = floor($this->originalPrice * parent::balance);
        $this -> cardCommission = floor( $this->cardPrice * parent::cardCommissionRate );
        $this -> givePoint = floor( $this->cardPrice * parent::point );
        $this -> benefitAmmount = floor($this->originalPrice - $this->balanceAmmount - $this->cardCommission-$this->givePoint);
    }
    
    public function getBalanceAmmount(){ return $this->balanceAmmount; }
    public function getCardCommission(){ return $this->cardCommission; }
    public function getGivePoint(){ return $this->givePoint; }
    public function getBenefitAmmount(){ return $this->benefitAmmount; }
}

//당일 취소
class newSemicancelBalance extends NewBalance{
    
    private $cancellationCharge, $balanceAmmount, $cancellationChargeForUser, $returnPoint, $cardCommission, $benefitAmmount, $usePoint;
    private $cancel_per = 0;
    
    function __construct($rentPrice_ ,$insurancePrice_ ,$deliveryPrice_ ,$realpayPrice_ ,$usepointPrice_, $usecouponPrice_, $originalPrice_, $cancel_per){
        parent::__construct($rentPrice_ ,$insurancePrice_ ,$deliveryPrice_ ,$realpayPrice_ ,$usepointPrice_, $usecouponPrice_, $originalPrice_);
        $this->cancel_per = $cancel_per;

        $this -> cancellationCharge = floor($this->originalPrice*(1-$this->cancel_per));                         //위약금
        $this -> balanceAmmount = floor( $this->cancellationCharge * parent::balance );
        $this -> cancellationChargeForUser = floor($this->cancellationCharge - $this->pointPrice);         //고객 부담 위약금
        if( $this -> cancellationChargeForUser < 0 ){
            $this -> returnPoint = $this->cancellationChargeForUser*(-1);
            $this -> usepoint = $this->pointPrice - $this->returnPoint;                             //사용포인트
            $this -> cancellationChargeForUser =0 ;
        } else {
            $this -> returnPoint = 0;
            $this -> usePoint = $this -> pointPrice;
        }
        
        $this -> cardCommission = floor($this->cancellationChargeForUser * parent::cardCommissionRate);
        $this -> benefitAmmount = floor($this->cancellationCharge - $this->balanceAmmount - $this->cardCommission + $this->couponPrice);
        
    }
    
    public function getBalanceAmmount(){ return $this->balanceAmmount; }
    public function getCardCommission(){ return $this->cardCommission; }
    public function getBenefitAmmount(){ return $this->benefitAmmount; }
    public function getCancellationCharge(){ return $this->cancellationCharge; }
    public function getCancellationChargeForUser(){ return $this->cancellationChargeForUser; }
    public function getUsePoint(){ return $this->usePoint; }
    public function getGivePoint(){ return 0; }
}

//완전 취소
class newCompleteCancelBalance extends NewBalance{

    private $balanceAmmount,$cardCommission,$benefitAmmount,$usePoint,$cancellationChargeForUser;

    function __construct($rentPrice_ ,$insurancePrice_ ,$deliveryPrice_ ,$realpayPrice_ ,$usepointPrice_, $usecouponPrice_, $originalPrice_){
        parent::__construct($rentPrice_ ,$insurancePrice_ ,$deliveryPrice_ ,$realpayPrice_ ,$usepointPrice_, $usecouponPrice_, $originalPrice_);

        $this->balanceAmmount =0;
        $this->cardCommission =0;
        $this->benefitAmmount = $this->couponPrice;
        $this->usePoint =0;
        $this->cancellationChargeForUser=0;
    }
    
    public function getBalanceAmmount(){ return $this->balanceAmmount; }
    public function getCardCommission(){ return $this->cardCommission; }
    public function getBenefitAmmount(){ return $this->benefitAmmount; }
    public function getCancellationCharge_user(){ return $this->cancellationChargeForUser; }
    public function getUsePoint(){ return $this->usePoint; }
}

/*








class balance{
    const point = 0.03;             //포인트 지급 : 3%
    const cardCommissionRate = 0.035;   //카드수수료 : 3.5%
    
    protected $rentPrice,$insurancePrice,$deliveryPrice,$cardPrice,$pointPrice,$couponPrice,$originalPrice;        
        
    function __construct($rPrice ,$iPrice ,$dPrice ,$payPrice ,$pPrice,$coPrice){
        $this->rentPrice = $rPrice;
        $this->insurancePrice = $iPrice;
        $this->deliveryPrice = $dPrice;
        $this->cardPrice = $payPrice;
        $this->pointPrice = $pPrice;
        $this->couponPrice = $coPrice;
        $this->originalPrice = $this->rentPrice + $this->insurancePrice + $this->deliveryPrice;
    }
    public function get_originalPrice(){ return $this->originalPrice; }
}

//기존고객
class normal_balance_existing extends balance{
    const balance = 0.95;    //기존고객 95% 정산
    private $balanceAmmount,$benefitAmmount,$cardCommission,$givePoint;
    function __construct($rPrice,$iPrice,$dPrice,$payPrice,$pPrice,$coPrice){
        parent::__construct($rPrice,$iPrice,$dPrice,$payPrice,$pPrice,$coPrice);

        $this->balanceAmmount = $this->originalPrice * self::balance;
        $this->cardCommission = floor($this->cardPrice * parent::cardCommissionRate);
        $this->givePoint = floor($this->cardPrice*parent::point);
        $this->benefitAmmount = $this->originalPrice - $this->balanceAmmount - $this->cardCommission - $this->givePoint;
    }       
    
    public function get_balanceAmmount(){ return $this->balanceAmmount; }
    public function get_cardCommission(){ return $this->cardCommission; }
    public function get_givePoint(){ return $this->givePoint; }
    public function get_benefitAmmount(){ return $this->benefitAmmount; }
    public function get_rentBalanceAmmount(){ return $this->rentPrice * self::balance; }
    public function get_insuBalanceAmmount(){ return $this->insurancePrice * self::balance; }
    public function get_delivBalanceAmmount(){ return $this->deliveryPrice * self::balance; }
}
//!--기존고객


class normal_balance_app extends balance{
    const rentBalance = 0.8;        //대여 정산 : 80%
    const insuranceBalance = 0.95;  //보험 정산 : 95%
    const deliveryBalance = 0.95;   //딜리버리 정산 : 95%
    
    private $balanceAmmount,$benefitAmmount,$cardCommission,$givePoint;
    
    function __construct($rPrice,$iPrice,$dPrice,$payPrice,$pPrice,$coPrice){
        parent::__construct($rPrice,$iPrice,$dPrice,$payPrice,$pPrice,$coPrice);
            
        $this->balanceAmmount = floor(($this->rentPrice * self::rentBalance) + ($this->insurancePrice * self::insuranceBalance) + ($this->deliveryPrice * self::deliveryBalance));
        $this->cardCommission = floor($this->cardPrice*parent::cardCommissionRate);
        $this->givePoint = floor($this->cardPrice*parent::point);
        $this->benefitAmmount = $this->originalPrice - $this->balanceAmmount - $this->cardCommission - $this->givePoint;
    }
    
    public function get_balanceAmmount(){ return $this->balanceAmmount; }
    public function get_cardCommission(){ return $this->cardCommission; }
    public function get_givePoint(){ return $this->givePoint; }
    public function get_benefitAmmount(){ return $this->benefitAmmount; }
    public function get_rentBalanceAmmount(){ return $this->rentPrice * self::rentBalance; }
    public function get_insuBalanceAmmount(){ return $this->insurancePrice * self::insuranceBalance; }
    public function get_delivBalanceAmmount(){ return $this->deliveryPrice * self::deliveryBalance; }
}

//당일 취소
class semiCancel_balance extends balance{
    const penalty = 0.1;                 //위약금 10%
    const semiCancel_balance = 0.965;  // 당일취소 정산 : 위약금의 96.5% , 원금의 9.65%
    
    private $cancellationCharge,$balanceAmmount,$cancellationCharge_user,$returnPoint,$cardCommission,$benefitAmmount,$usePoint;
    
    function __construct($rPrice,$iPrice,$dPrice,$payPrice,$pPrice,$coPrice){
        parent::__construct($rPrice,$iPrice,$dPrice,$payPrice,$pPrice,$coPrice);
        
        $this->cancellationCharge = $this->originalPrice*self::penalty;                         //위약금
        $this->balanceAmmount = floor($this->cancellationCharge * self::semiCancel_balance);
        $this->cancellationCharge_user = $this->cancellationCharge - $this->pointPrice;         //고객부담위약금
        if($this->cancellationCharge_user < 0){
            $this->returnPoint = $this->cancellationCharge_user * (-1);                         //반환포인트
            $this->usePoint = $this->pointPrice-$this->returnPoint;                             //사용포인트
            $this->cancellationCharge_user =0 ;
        }else $this->usePoint = $this->pointPrice;
            
        $this->cardCommission = $this->cancellationCharge_user * parent::cardCommissionRate;
        $this -> benefitAmmount = $this->cancellationCharge - $this->balanceAmmount - $this->cardCommission + $this->couponPrice;
    }
    
    public function get_balanceAmmount(){ return $this->balanceAmmount; }
    public function get_cardCommission(){ return $this->cardCommission; }
    public function get_benefitAmmount(){ return $this->benefitAmmount; }
    public function get_cancellationCharge(){ return $this->cancellationCharge; }
    public function get_cancellationCharge_user(){ return $this->cancellationCharge_user; }
    public function get_usePoint(){ return $this->usePoint; }
    public function get_rentBalanceAmmount(){ return $this->rentPrice * self::penalty; }
    public function get_insuBalanceAmmount(){ return $this->insurancePrice * self::penalty; }
    public function get_delivBalanceAmmount(){ return $this->deliveryPrice * self::penalty; }
    public function get_givePoint(){ return 0; }
}

//완전 취소
class completeCancel_balance extends balance{

    private $balanceAmmount,$cardCommission,$benefitAmmount,$usePoint,$cancellationCharge_user;

    function __construct($rPrice,$iPrice,$dPrice,$payPrice,$pPrice,$coPrice){
        parent::__construct($rPrice,$iPrice,$dPrice,$payPrice,$pPrice,$coPrice);

        $this->balanceAmmount =0;
        $this->cardCommission =0;
        $this->benefitAmmount = $this->couponPrice;
        $this->usePoint =0;
        $this->cancellationCharge_user=0;
    }
    
    public function get_balanceAmmount(){ return $this->balanceAmmount; }
    public function get_cardCommission(){ return $this->cardCommission; }
    public function get_benefitAmmount(){ return $this->benefitAmmount; }
    public function get_cancellationCharge_user(){ return $this->cancellationCharge_user; }
    public function get_usePoint(){ return $this->usePoint; }
    public function get_rentBalanceAmmount(){ return $this->rentPrice * 0; }
    public function get_insuBalanceAmmount(){ return $this->insurancePrice * 0; }
    public function get_delivBalanceAmmount(){ return $this->deliveryPrice * 0; }
    public function get_givePoint(){ return 0; }
}
*/

?>