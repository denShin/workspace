<?php
$calmainmst_code =$data["calmainmst_code"];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Team O2</title>

    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" href="http://workspace.teamo2.kr/static/images/logo.png">

    <link href="/static/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/static/lib/bootstrap/css/bootstrap-theme.css" rel="stylesheet">

    <!-- Placed at the end of the document so the pages load faster -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="/static/lib/bootstrap/js/bootstrap.min.js"></script>


    <!-- https://fontawesome.com/v4.7.0/icons/-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script language="JavaScript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            } else {
                return true;
            }
        }
    </script>
</head>


<body  >





<script language="javascript">
    function logoutaction()
    {

        if(confirm("로그아웃 하시겠습니까?"))
        {
            location.href="/adminmanage/Logout";
        }
    }
</script>


<div class="container-fluid" style="padding-top: 10px;padding-bottom: 70px;">

    <style>
        .double-input .form-control {
            width: 50%;
        }

        .show-grid [class^=col-] {
            padding-top: 10px;
            padding-bottom: 10px;
            border: 1px solid #ddd;
            border: 1px solid rgba(86,61,124,.2);
            list-style: none;
        }


        .inactive {
            color: #ccc;
            background-color: #fafafa;
        }
    </style>

    <fieldset>
        <div class="row col-md-12"  style="margin-top:25px">

            <div class="col-md-12">

                <div class="col-md-12">
                    <div class="panel  panel-primary ">
                        <div class="panel-heading">
                            <h3 class="panel-title">세금계산서 발행 / 입금확인 - 세무사무실 확인용


                            </h3>
                        </div>
                        <div class="panel-body"   style="height: 800px; overflow-y: scroll;" >


                            <table class="table" >
                                <tr class="bg-info">

                                    <td class="small  text-center"> 회사명 </td>
                                    <td class="small text-center "> 사업자번호 </td>
                                    <td class="small  text-center">정산 상세 </td>
                                    <td class="small  text-center">원결제금액 </td>
                                    <td class="small  text-center"> 정산가격</td>
                                    <td class="small  text-center">수수료  </td>
                                    <td class="small text-center ">할인금액 </td>
                                </tr>

                                <?php


                                foreach ($data["list"] as $entry) {

                                    $sumcalamount =$entry["sumcalamount"];
                                    $tax_msg = $entry["tax_msg"];
                                    $taxmail_yn =$entry["taxmail_yn"];
                                    ?>
                                    <tr>
                                        <td class="small  text-left"> <?=$entry["companyserial"]?>-<?=$entry["branchserial"]?>. <?=$entry["companyname"]?> > <?=$entry["branchname"]?></td>
                                        <td class="small  text-center"> <?=$entry["registration_number"]?> </td>
                                        <td class="small  text-center">
                                            <button type="button"  class="btn btn-default btn-xs" onclick="showdetail('<?=$calmainmst_code?>','<?=$entry["registration_number"]?>')"> 상세보기</button>
                                        </td>
                                        <td class="small  text-center"> <?=$entry["sumtotalpricestr"]?> </td>
                                        <td class="small  text-center"> <?=$entry["sumcalamountstr"]?> </td>
                                        <td class="small  text-center"> <?=$entry["sumrentfeestr"]?> </td>
                                        <td class="small  text-center"> <?=$entry["sumdiscountstr"]?> </td>
                                    </tr>
                                    <?
                                }
                                ?>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </fieldset>



</div>
<Script>

    function showdetail(calmainmst_code,registration_number){
        window.open("http://paper.teamo2.kr/calculate/CalculateDetail?calmainmst_code="+calmainmst_code+"&registration_number="+registration_number,"","");
    }


</Script>