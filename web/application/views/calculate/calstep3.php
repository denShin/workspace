
<script src="/static/lib/dropzone/dropzone.js"></script>
<link href="/static/lib/dropzone/dropzone.css"rel="stylesheet">
<div class="container-fluid" style="padding-top: 50px;padding-bottom: 70px;">

    <style>
        .double-input .form-control {
            width: 50%;
        }

        .show-grid [class^=col-] {
            padding-top: 10px;
            padding-bottom: 10px;
            border: 1px solid #ddd;
            border: 1px solid rgba(86,61,124,.2);
            list-style: none;
        }


        .inactive {
            color: #ccc;
            background-color: #fafafa;
        }
    </style>

    <fieldset>
        <div class="row col-md-12"  style="margin-top:25px">

            <div class="col-md-12">

                <div class="col-md-12">
                    <div class="panel  panel-primary ">
                        <div class="panel-heading">
                            <h3 class="panel-title">세금계산서 발행 / 입금확인

                                <div class="pull-right">
                                    <button class="btn btn-default btn-xs" onclick="showallcalculate()">세무사/정산로그</button>
                                    <button class="btn btn-default btn-xs" onclick="showexceldata()">대량이체 데이터출력</button>
                                    <button class="btn btn-default btn-xs" onclick="paytranscomplete()">입금완료/메일발송</button>
                                        <button class="btn btn-danger btn-xs" onclick="taxpapercomplete()">세금계산서 발행하기</button>
                                    &nbsp;&nbsp;&nbsp;
                                    <button type="button"  class="btn btn-default btn-xs" onclick="location.href='/calculate/Calculatelog'">목록가기</button>
                                </div>
                            </h3>
                        </div>
                        <div class="panel-body"   style="height: 800px; overflow-y: scroll;" >

                     
                            <table class="table" >
                                <tr class="bg-info">
                                    <td class="small text-center"> <input type="checkbox" class="check-all"> </td>
                                    <td class="small  text-center"> 회사명 </td>
                                    <td class="small text-center "> 사업자번호 </td>
                                    <td class="small  text-center">정산 이메일 </td>
                                    <td class="small  text-center">메일상세 </td>
                                    <td class="small  text-center">세금 이메일 </td>
                                    <td class="small  text-center">원결제금액 </td>
                                    <td class="small  text-center"> 정산가격</td>
                                    <td class="small  text-center">수수료  </td>
                                    <td class="small text-center ">할인금액 </td>
                                    <td class="small text-center "> 입금여부</td>
                                    <td class="small text-center "> 세금계산서</td>
                                </tr>

                                <?php


                                foreach ($data["list"] as $entry) {

                                            $sumcalamount =$entry["sumcalamount"];
                                            $tax_msg = $entry["tax_msg"];
                                            $taxmail_yn =$entry["taxmail_yn"];
                                        ?>
                                        <tr>
                                            <td class="small  text-center">
                                                <?if($sumcalamount> 0){?>
                                                <input type="checkbox" class="ab" name="calbranch_idx" value="<?=$entry["calbranch_idx"]?>">
                                                <?}else{?>
                                                    *
                                                <?}?>
                                            </td>
                                            <td class="small  text-left"><?=$entry["companyname"]?> > <?=$entry["branchname"]?></td>
                                            <td class="small  text-center"> <?=$entry["registration_number"]?> </td>
                                            <td class="small  text-center"> <?=$entry["cal_email"]?> </td>
                                            <td class="small  text-center">  <button type="button"  class="btn btn-default btn-xs" onclick="showsendemail('<?=$entry["branchserial"]?>')"><i class="glyphicon glyphicon-envelope"></i> 보기</button></td>
                                            <td class="small  text-center"> <?=$entry["tax_email"]?> </td>
                                            <td class="small  text-center"> <?=$entry["sumtotalpricestr"]?> </td>
                                            <td class="small  text-center"> <?=$entry["sumcalamountstr"]?> </td>
                                            <td class="small  text-center"> <?=$entry["sumrentfeestr"]?> </td>
                                            <td class="small  text-center"> <?=$entry["sumdiscountstr"]?> </td>
                                            <td class="small  text-center"> <?=$entry["bankins_ynstr"]?> </td>
                                            <td class="small  text-center">
                                                <?if($taxmail_yn=="x"){?>
                                                <a href="#" data-toggle="tooltip" title="<?=$tax_msg?>"> <?=$entry["taxmail_ynstr"]?></a>
                                                <?}else{?>
                                                    <?=$entry["taxmail_ynstr"]?>
                                                <?}?>
                                            </td>
                                        </tr>
                                   <?
                                }
                                ?>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </fieldset>



</div>
<div  id="viewmodal" class="modal fade "  >

    <div class="modal-dialog viewmodalwidth ">

        <div class="modal-content "  style="background-color: white;">
            <div class="modal-header">

                <button type="button" class="btn  btn-outline-black  btn close"  onclick="closeviewmodal()" >
                    <span class="h2">X</span>
                </button><br>

            </div>
            <div class="modal-body" style="text-align: left;background-color: white;">
            </div>

        </div>
    </div>
</div>


<script language="JavaScript">

    $( document ).ready( function() {
        $( '.check-all' ).click( function() {
            $( '.ab' ).prop( 'checked', this.checked );
        } );

        $('[data-toggle="tooltip"]').tooltip();
    } );

    function showallcalculate(){
        var calmainmst_code="<?=$data["calmainmst_code"]?>"

        if(confirm('입금완료,메일발송,세금계산서 발행이 완료된 데이터만 보여집니다.')){
            window.open("http://paper.teamo2.kr/calculate/CalculateAllog?calmainmst_code="+calmainmst_code,"","");
        }

    }


    function showexceldata(){
        var checkedcnt = $('input[name="calbranch_idx"]:checked').length  ;

        if(checkedcnt < 1){
            alert('*이체하실 데이터를 선택하셔야 합니다.');
            return;
        }

        var nullcheck =0;
        var calbranch_idx= new Array();
        $("input[name=calbranch_idx]:checked").each(function() {
            calbranch_idx.push($(this).val());
            if( $(this).val() ==="x"){
                nullcheck++;
            }
        });

        if(nullcheck > 0){
            alert('*세금/정산정보가 등록되지 않은 지점은 정산할 수 없습니다.');
            return;
        }

        var strcalbranch_idx = calbranch_idx.join("-");

        var calmainmst_code="<?=$data["calmainmst_code"]?>"
        var headurl ="/calculate/Calculatelog";
        var postdata ={'ptype':'transcopy' ,'calmainmst_code':calmainmst_code ,'strcalbranch_idx':strcalbranch_idx }

        $.ajax({
            url: headurl,type: "post", data:postdata,cache: false,
        }).done(function(result){

            $('#viewmodal .modal-body').html(result)
            $('#viewmodal').modal('show')

        });
    }


    function showsendemail(branchserial){


        var calmainmst_code="<?=$data["calmainmst_code"]?>"
        var headurl ="/calculate/Calculatelog";
        var postdata ={'ptype':'emailbody' ,'calmainmst_code':calmainmst_code ,'calmaindtl_branch':branchserial }

        $.ajax({
            url: headurl,type: "post", data:postdata,cache: false,
        }).done(function(result){

            $('#viewmodal .modal-body').html(result)
            $('#viewmodal').modal('show')

        });
    }


    function taxpapercomplete(){
        var checkedcnt = $('input[name="calbranch_idx"]:checked').length  ;

        if(checkedcnt < 1){
            alert('*세금계산서 발행대상 데이터를 선택하셔야 합니다.');
            return;
        }

        var calmainmst_code="<?=$data["calmainmst_code"]?>"


        if(confirm('*세금계산서 발행하면 익일 오후 3시에 국세청에 등록 됩니다. \n\n선택하신 데이터를 세금계산서 발행 처리하시겠습니까? ')){

            var nullcheck =0;
            var calbranch_idx= new Array();
            $("input[name=calbranch_idx]:checked").each(function() {
                calbranch_idx.push($(this).val());
                if( $(this).val() ==="x"){
                    nullcheck++;
                }
            });

            if(nullcheck > 0){
                alert('*세금/정산정보가 등록되지 않은 지점은 정산할 수 없습니다.');
                return;
            }

            var strcalbranch_idx = calbranch_idx.join("-");

            var postdata ={'ptype':'settaxpapercomplete' ,'calmainmst_code':calmainmst_code ,'strcalbranch_idx':strcalbranch_idx }

            $.ajax({
                url: "/calculate/Calculatelog",type: "post", data:postdata,
                success: function (response) {
                    console.log("response:%o",response);
                    alert('*세금계산서 발행이 완료되었습니다!');
                    location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //console.log("error");
                },
                complete:  function() {
                    //console.log("complete");

                }
            });

        }

    }



    function paytranscomplete(){
        var checkedcnt = $('input[name="calbranch_idx"]:checked').length  ;

        if(checkedcnt < 1){
            alert('*입금완료 데이터를 선택하셔야 합니다.');
            return;
        }

        var calmainmst_code="<?=$data["calmainmst_code"]?>"


        if(confirm('*입금완료처리하면 거래처에 완료메일이 일괄발송됩니다. \n\n선택하신 데이터를 입금완료 처리하시겠습니까? ')){

            var nullcheck =0;
            var calbranch_idx= new Array();
            $("input[name=calbranch_idx]:checked").each(function() {
                calbranch_idx.push($(this).val());
                if( $(this).val() ==="x"){
                    nullcheck++;
                }
            });

            if(nullcheck > 0){
                alert('*세금/정산정보가 등록되지 않은 지점은 정산할 수 없습니다.');
                return;
            }

            var strcalbranch_idx = calbranch_idx.join("-");

            var postdata ={'ptype':'setpaycomplete' ,'calmainmst_code':calmainmst_code ,'strcalbranch_idx':strcalbranch_idx }

            $.ajax({
                url: "/calculate/Calculatelog",type: "post", data:postdata,
                success: function (response) {
                   console.log("response:%o",response);
                    alert('입금완료 처리 & 메일 발송처리가 완료되었습니다.');
                      location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //console.log("error");
                },
                complete:  function() {
                    //console.log("complete");

                }
            });

        }




    }


    function closeviewmodal(){
        $("#viewmodal").modal("hide");
        location.reload();
    }


</Script>