<?php

require_once("newBalanceData.php");
require_once("../lib/security/decrypt.php");

// 1일 ~15일 = 16일에 정산
// 16일~말일 = 익월 첫영업일
$start = $_GET['start'];
$ending = $_GET['end'];

$start="2019-01-01";
$start="2019-01-15";
$end =date("Y-m-d 23:59:59",strtotime($ending));
$balance = $_GET['balance'];

$company = $_GET['company'];
$branch = $_GET['branch'];

$normalBalance_count =0;    //정상결제 건수
$semiCancel_count=0;        //취소수수료 건수
$completeCancel_count=0;    //완전취소 건수

$normalBalance_ammount=0;       //정상결제 금액
$semiCancel_ammount=0;          //취소수수료 금액
$completeCancel_ammount=0;      //완전취소 금액
$point_ammount=0;               //포인트 금액
$coupon_ammount=0;              //쿠폰 금액
$pay_ammount=0;                 //실결제금액

$total_givePoint=0;     //총 지급포인트

$totalBalance_rent=0;   //대여료 정산
$totalBenefit_rent=0;   //대여료 처리

$totalBalance_count=0;              //총 정산건수
$totalBalance_ammount=0;            //총 정산금액

$totalOriginal_ammount=0;           //총원금
$totalRent_ammount=0;               //총렌트금
$totalInsu_ammount=0;               //총보험금
$totalDeliv_ammount=0;              //총딜리버리금
$totalBenefit_ammount=0;            //총처리금
$totalCardCommission_ammount=0;     //총카드수수료
$totalRevenue=0;            //총매출

$totalCommission=0;

$total_carmore_payment = 0;
$total_carmore_commission = 0;


$total_normal_cnt = 0;
$total_month_cnt = 0;

$total_normal_cost = 0;
$total_month_cost = 0;

// 정산해야할 목록 예약정보 테이블
$query ="SELECT * FROM tbl_account_info WHERE flag=0 ORDER BY regdate DESC";

$accountInfoQuery = $conn -> prepare($query);
$accountInfoQuery -> execute();

$EXCEL_STR = "  
<table border='1'>  
<tr>  
   <td style='text-align:center; font-size:120%;'><b>구분</b></td>  
   <td style='text-align:center; font-size:120%;'><b>단기/월</b></td>  
   <td style='text-align:center; font-size:120%;'><b>고객정보</b></td>
   <td style='text-align:center; font-size:120%;'><b>렌트기간<br>차량</b></td>
   <td style='text-align:center; font-size:120%;'><b>원결제금액</b></td>
   <td style='text-align:center; font-size:120%;'><b>정산가격</b></td>
   <td style='text-align:center; font-size:120%;'><b>수수료</b><br><small>(카드사 수수료 포함)</small></td>
   <td style='text-align:center; font-size:120%;'><b>할인금액</b><br><small>(카모아 부담)</small></td>
</tr>";

while( $accountInfoStmt=$accountInfoQuery -> fetch() ){
    $status = $accountInfoStmt['status'];
    $reservationIdx = $accountInfoStmt['f_reservationidx'];


    $with = ($branch != 'all')? ' AND f_companycode="'.$branch.'" ':'';

    $chk = 'SELECT f_usrserial FROM tbl_reservation_list WHERE f_reservationidx="'.$reservationIdx.'" AND company_serial="'.$company.'" '.$with.' and f_usrserial NOT IN (34897, 23314, 15397, 55563)';
    $chkSql = $conn -> prepare($chk);
    $chkSql -> execute();
    if( $chkStmt = $chkSql -> fetch()){
        $wibble = array(7831);
        if (in_array($chkStmt['f_usrserial'], $wibble))
            continue;
    } else
        continue;


    $basicDate = '';    //정산기준일
    $bookingId = '';
    if($status == 1) {

        $reserv_idx = explode('E', $reservationIdx)[0];
        $crti_idx = strpos($reservationIdx, 'E')? trim(explode('E', $reservationIdx)[1]) : '';

        $added_where = ($crti_idx !== '')? " crti_idx= '".$crti_idx."' " : "crti_reservation_idx='".$reserv_idx."' ";

        $query = "
        SELECT
           f_bookingid, f_usrserial, crti_start_date as f_rentstartdate, crti_end_date as f_rentenddate, f_carserial,
           crti_rent_cost as f_rentprice, crti_cdw_cost as f_insuprice, crti_delivery_cost as f_delivprice,
           crti_use_coupon_idx as f_couponserial, crti_use_point as f_usepoint, crti_payment as f_totalprice,
            f_drivername,f_usecarnumber,driver_name_encrypt, f_companycode,
           crti_early_return_apply, crti_early_return_use_cost, crti_reserv_rent_type
        FROM
          tbl_reservation_list trl INNER JOIN
          carmore_reservation_term_information crti
          ON
              trl.f_reservationidx=crti.crti_reservation_idx AND crti.crti_state=1
        WHERE 
             ".$added_where." AND 
             (
                (crti.crti_end_date BETWEEN '$start' AND '$end') OR
                (crti.crti_early_return_apply=2 AND crti_early_return_confirm_date BETWEEN LEFT('$start', 10) AND LEFT('$end', 10))
             )";
        $accountQuery=$conn->prepare($query);
        $accountQuery->execute();

        if($accountStmt=$accountQuery->fetch()){

            $category = "정상결제";
            $basicDate=$accountStmt['f_rentenddate'];
            $userSerial = $accountStmt['f_usrserial'];
            $carSerial = $accountStmt['f_carserial'];
            $couponSerial = $accountStmt['f_couponserial'];
            $fcompanycode = $accountStmt['f_companycode'];
            $bookingId = $accountStmt['f_bookingid'];
            $startDate = $accountStmt['f_rentstartdate'];
            $endDate = $accountStmt['f_rentenddate'];
            $rentPrice = $accountStmt['f_rentprice'];
            $insuPrice = $accountStmt['f_insuprice'];
            $delivPrice = $accountStmt['f_delivprice'];
            $usePoint = $accountStmt['f_usepoint'];
            $payPrice = $accountStmt['f_totalprice'];
            $encrypt=$accountStmt['driver_name_encrypt'];
            $originalPrice = $rentPrice+$insuPrice+$delivPrice;

            $query = "SELECT usrname,phone, kakaoid FROM tbl_usr_list WHERE usrserial=?";
            $userQuery = $conn ->prepare($query);
            $userQuery -> execute([$userSerial]);
            $userStmt = $userQuery -> fetch();

            $userName = $userStmt['usrname'];   //유저명
            $driverName = $accountStmt['f_drivername']; //운전자명
            if($driverName == '')
                $driverName = decrypt($encrypt);

            if (strpos($driverName, '팀오투테스트') !== false)
                continue;
            

            $kakaoid = $userStmt['kakaoid'];
            $call_reserv = 0;
            if (!empty($kakaoid) && $kakaoid !== '0') {
                $admin_query = "SELECT admin_idx FROM admin_member WHERE kakaoid=? AND kakaoid <> '' AND kakaoid <> 0";
                $admin_sql = $conn->prepare($admin_query);
                $admin_sql->execute([$kakaoid]);
                if ($admin_stmt = $admin_sql->fetch())
                    $call_reserv = 1;

            }

            $param_point = $usePoint;
            $param_payment = $payPrice;
            if ($call_reserv === 1) {
                $param_point = 0;
                $param_payment = $usePoint;
            }

            if ($accountStmt['crti_early_return_apply'] == 2) {
                $param_payment = $accountStmt['crti_early_return_use_cost'];
                if ($call_reserv === 1) {
                    $originalPrice = $param_payment;
                    echo '23rj0ewifhewfhewf0wfw';

                }

            }


            $carmore_payment = (int)$originalPrice - (int)$param_payment;
            $total_carmore_payment += $carmore_payment;

            $carModel = '';  //차량모델
            $carNumber = '';   //차량번호
            $query = 'SELECT CONCAT(car_number1,car_number2,car_number3) AS car_number,model FROM new_rentCar INNER JOIN new_car ON new_rentCar.car_serial=new_car.serial WHERE new_rentCar.serial="'.$carSerial.'"';
            $carQuery = $conn ->prepare($query);
            $carQuery -> execute();
            $carStmt = $carQuery -> fetch();

            $carModel = $carStmt['model'];  //차량모델
            $carNumber = $carStmt['car_number'];

            $query = 'select value from new_coupon_info info inner join new_coupon_list list on list.info_serial=info.serial where list.serial=:cs';
            $couponQuery = $conn ->prepare($query);
            $couponQuery -> bindParam(":cs", $couponSerial);
            $couponQuery -> execute();
            $couponStmt = $couponQuery -> fetch();

            $coupon = $couponStmt['value']; //쿠폰사용액
            if($coupon == '')
                $coupon=0;

            $query = 'select name, branchName FROM new_rentCompany a INNER JOIN new_rentCompany_branch b ON a.serial=b.rentCompany_serial where b.serial="'.$fcompanycode.'"';
            $bcsql = $conn -> prepare($query);
            $bcsql -> execute();
            $companyname = '고고렌트카';
            if($bcStmt = $bcsql -> fetch()){
                $companyname = $bcStmt['name'];
                $branchName = $bcStmt['branchName'];
            }

            if($fcompanycode == 2 || $fcompanycode == 30)
                $balance = new newNormalBalancePrimium($rentPrice,$insuPrice,$delivPrice,$param_payment,$param_point,$coupon,$originalPrice);
            else
                $balance = new newNormalBalance($rentPrice,$insuPrice,$delivPrice,$param_payment,$param_point,$coupon,$originalPrice);

            $accountAmmount = $balance->getBalanceAmmount();
            $cardCommission = $balance->getCardCommission();
            $benefitAmmount = $balance->getBenefitAmmount();

            $commission = $originalPrice-$accountAmmount-$cardCommission;
            $carmore_commission = $commission+$cardCommission;
            $total_carmore_commission += $carmore_commission;

            $givePoint = $balance -> getGivePoint();
            $total_givePoint += $givePoint;

            $totalBenefit_ammount+=$benefitAmmount;

            $totalCommission += $commission;
            $totalCardCommission_ammount+=$cardCommission;

            $is_it_normal = ($accountStmt['crti_reserv_rent_type'] == 1)? true : false;

            $rent_type_str = ($is_it_normal)? '단기 렌트' : '월 렌트';

            $EXCEL_STR .= '<tr>';
            $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>".$category."</td>";
            $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>".$rent_type_str."</td>";
            $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>".$userName."&nbsp;/&nbsp;".$driverName."</td>";
            $EXCEL_STR .= "<td>".substr($startDate,0,16)." ~ ".substr($endDate,0,16)."</td>";
            $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $originalPrice)."</td>";
            $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $accountAmmount)."</td>";
            $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black; border-right: 4px solid darkgoldenrod;'>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $carmore_commission)."</td>";
            $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $carmore_payment)."</td>";
            $EXCEL_STR .= "</tr><tr style='background-color:#e3f2fd;'>";
            $EXCEL_STR .= "<td style='border-bottom:2px solid black;'>".$carModel." - ".$carNumber."</td>";

            $normalBalance_count++;
            $normalBalance_ammount+=$originalPrice;
            $totalBalance_count++;
            $totalBalance_ammount+=$accountAmmount;

            $totalRevenue+=$originalPrice;

            $totalOriginal_ammount+=$originalPrice;

            $point_ammount+=$usePoint;
            $coupon_ammount+=$coupon;
            $pay_ammount+=$payPrice;

            if ($is_it_normal) {
                $total_normal_cnt++;
                $total_normal_cost += $originalPrice;
            } else {
                $total_month_cnt++;
                $total_month_cost += $originalPrice;
            }

        }



    }elseif($status==2){

        //취소수수료
        $query = "SELECT f_regdate FROM tbl_cancel_log WHERE f_reservationidx='$reservationIdx' AND f_regdate BETWEEN '$start' AND '$end'";
        $checkQuery = $conn -> prepare($query);
        $checkQuery -> execute();
        if( $checkStmt = $checkQuery -> fetch() ){
            $category = "취소수수료";
            $basicDate = $checkStmt['f_regdate'];

            $query = "SELECT f_bookingid,f_usrserial,f_rentstartdate,f_rentenddate,f_carserial,f_rentprice,f_insuprice,f_delivprice,f_couponserial,f_usepoint,f_totalprice,f_drivername,f_usecarnumber,driver_name_encrypt, f_companycode, cancel_per FROM tbl_reservation_list WHERE f_reservationidx='$reservationIdx' AND f_paystatus='2'";
            $accountQuery=$conn->prepare($query);
            $accountQuery->execute();
            if($accountStmt=$accountQuery->fetch()){
                $userSerial = $accountStmt['f_usrserial'];
                $carSerial = $accountStmt['f_carserial'];
                $couponSerial = $accountStmt['f_couponserial'];

                $bookingId = $accountStmt['f_bookingid'];
                $startDate = $accountStmt['f_rentstartdate'];
                $endDate = $accountStmt['f_rentenddate'];
                $rentPrice = $accountStmt['f_rentprice'];
                $insuPrice = $accountStmt['f_insuprice'];
                $delivPrice = $accountStmt['f_delivprice'];
                $usePoint = $accountStmt['f_usepoint'];
                $payPrice = $accountStmt['f_totalprice'];
                $originalPrice = $rentPrice+$insuPrice+$delivPrice;
                $encrypt=$accountStmt['driver_name_encrypt'];
                $cancel_per = $accountStmt['cancel_per'];

                $carmore_payment = (int)$originalPrice - (int)$payPrice;
                $total_carmore_payment += $carmore_payment;

                $fcompanycode = $accountStmt['f_companycode'];

                $query = "SELECT usrname,phone,kakaoid FROM tbl_usr_list WHERE usrserial='$userSerial'";
                $userQuery = $conn ->prepare($query);
                $userQuery -> execute();
                $userStmt = $userQuery -> fetch();

                $userName = $userStmt['usrname'];   //유저명
                $driverName = $accountStmt['f_drivername']; //운전자명
                if($driverName == '')
                    $driverName = decrypt($encrypt);

                $kakaoid = $userStmt['kakaoid'];
                $call_reserv = 0;
                if (!empty($kakaoid) && $kakaoid !== '0') {
                    $admin_query = "SELECT admin_idx FROM admin_member WHERE kakaoid=? AND kakaoid <> '' AND kakaoid <> 0";
                    $admin_sql = $conn->prepare($query);
                    $admin_sql->execute([$kakaoid]);
                    if ($admin_stmt = $admin_sql->fetch())
                        $call_reserv = 1;
                }

                $param_point = $usePoint;
                $param_payment = $payPrice;
                if ($call_reserv === 1) {
                    $param_point = 0;
                    $param_payment = $usePoint;
                }

                $carModel = '';  //차량모델
                $carNumber = '';   //차량번호
                $query = 'SELECT CONCAT(car_number1,car_number2,car_number3) AS car_number,model FROM new_rentCar INNER JOIN new_car ON new_rentCar.car_serial=new_car.serial WHERE new_rentCar.serial="'.$carSerial.'"';
                $carQuery = $conn ->prepare($query);
                $carQuery -> execute();
                $carStmt = $carQuery -> fetch();

                $carModel = $carStmt['model'];  //차량모델
                $carNumber = $carStmt['car_number'];

                $query = 'select value from new_coupon_info info inner join new_coupon_list list on list.info_serial=info.serial where list.serial=:cs';
                $couponQuery = $conn ->prepare($query);
                $couponQuery -> bindParam(":cs", $couponSerial);
                $couponQuery -> execute();
                $couponStmt = $couponQuery -> fetch();

                $coupon = $couponStmt['value']; //쿠폰사용액
                if($coupon == '')
                    $coupon=0;

                $query = 'select name, branchName FROM new_rentCompany a INNER JOIN new_rentCompany_branch b ON a.serial=b.rentCompany_serial where b.serial="'.$fcompanycode.'"';
                $bcsql = $conn -> prepare($query);
                $bcsql -> execute();
                $companyname = '고고렌트카';
                if($bcStmt = $bcsql -> fetch()){
                    $companyname = $bcStmt['name'];
                    $branchName = $bcStmt['branchName'];
                }

                if($fcompanycode == 2 || $fcompanycode == 30)
                    $balance = new newSemicancelBalancePrimium($rentPrice,$insuPrice,$delivPrice,$param_payment,$param_point,$coupon,$originalPrice, $cancel_per);
                else
                    $balance = new newSemicancelBalance($rentPrice,$insuPrice,$delivPrice,$param_payment,$param_point,$coupon,$originalPrice, $cancel_per);

                $accountAmmount = $balance->getBalanceAmmount();
                $cardCommission = $balance->getCardCommission();
                $benefitAmmount = $balance->getBenefitAmmount();

                $givePoint = $balance -> getGivePoint();
                $total_givePoint += $givePoint;

                $cancelCharge = $balance->getCancellationCharge();
                $totalRevenue += $cancelCharge;

                $totalCardCommission_ammount+=$cardCommission;

                $totalBenefit_ammount+=$benefitAmmount;

                $commission = $cancelCharge-$accountAmmount-$cardCommission;
                $totalCommission+=$commission;

                $carmore_commission = $commission+$cardCommission;
                $total_carmore_commission += $carmore_commission;
                $EXCEL_STR .= '<tr>';
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>".$category."</td>";
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>단기</td>";
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>".$userName."&nbsp;/&nbsp;".$driverName."</td>";
                $EXCEL_STR .= "<td>".substr($startDate,0,16)." ~ ".substr($endDate,0,16)."</td>";
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $cancelCharge)."</td>";
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $accountAmmount)."</td>";
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black; border-right: 4px solid darkgoldenrod;'>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $carmore_commission)."</td>";
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $carmore_payment)."</td>";
                $EXCEL_STR .= "</tr><tr style='background-color:#e3f2fd;'>";
                $EXCEL_STR .= "<td style='border-bottom:2px solid black;'>".$carModel." - ".$carNumber."</td>";

                $semiCancel_count++;
                $semiCancel_ammount+=$cancelCharge;
                $totalBalance_count++;
                $totalBalance_ammount+=$accountAmmount;

                $point_ammount+=$balance->getUsePoint();
                $coupon_ammount+=$coupon;
                $pay_ammount+=$balance->getCancellationChargeForUser();

                $totalOriginal_ammount+=$cancelCharge;
            }
        }

    }elseif($status==3){

        //전일취소 ,완전취소 --- 업체에 줄돈이 없음
        //3은 필요가없음
        $query = "SELECT f_regdate FROM tbl_cancel_log WHERE f_reservationidx='$reservationIdx' AND f_regdate BETWEEN '$start' AND '$end'";
        $checkQuery = $conn -> prepare($query);
        $checkQuery -> execute();
        if( $checkStmt = $checkQuery -> fetch() ){
            $category = "전일취소";
            $basicDate = $checkStmt['f_regdate'];

            $query = "SELECT f_bookingid,f_usrserial,f_rentstartdate,f_rentenddate,f_carserial,f_rentprice,f_insuprice,f_delivprice,f_couponserial,f_usepoint,f_totalprice,f_drivername,f_usecarnumber,driver_name_encrypt, f_companycode FROM tbl_reservation_list WHERE f_reservationidx='$reservationIdx' AND f_paystatus='2'";
            $accountQuery=$conn->prepare($query);
            $accountQuery->execute();
            if($accountStmt=$accountQuery->fetch()){
                $userSerial = $accountStmt['f_usrserial'];
                $carSerial = $accountStmt['f_carserial'];
                $couponSerial = $accountStmt['f_couponserial'];

                $bookingId = $accountStmt['f_bookingid'];
                $startDate = $accountStmt['f_rentstartdate'];
                $endDate = $accountStmt['f_rentenddate'];
                $rentPrice = $accountStmt['f_rentprice'];
                $insuPrice = $accountStmt['f_insuprice'];
                $delivPrice = $accountStmt['f_delivprice'];
                $usePoint = $accountStmt['f_usepoint'];
                $payPrice = $accountStmt['f_totalprice'];
                $originalPrice = $rentPrice+$insuPrice+$delivPrice;
                $encrypt=$accountStmt['driver_name_encrypt'];
                $fcompanycode = $accountStmt['f_companycode'];
                $query = "SELECT usrname,phone FROM tbl_usr_list WHERE usrserial='$userSerial'";
                $userQuery = $conn ->prepare($query);
                $userQuery -> execute();
                $userStmt = $userQuery -> fetch();

                $carmore_payment = (int)$originalPrice - (int)$payPrice;
                $total_carmore_payment += $carmore_payment;

                $userName = $userStmt['usrname'];   //유저명
                $driverName = $accountStmt['f_drivername']; //운전자명
                if($driverName == '')
                    $driverName = decrypt($encrypt);


                $carModel = '';  //차량모델
                $carNumber = '';   //차량번호
                $query = 'SELECT CONCAT(car_number1,car_number2,car_number3) AS car_number,model FROM new_rentCar INNER JOIN new_car ON new_rentCar.car_serial=new_car.serial WHERE new_rentCar.serial="'.$carSerial.'"';
                $carQuery = $conn ->prepare($query);
                $carQuery -> execute();
                $carStmt = $carQuery -> fetch();

                $carModel = $carStmt['model'];  //차량모델
                $carNumber = $carStmt['car_number'];

                $query = 'select value from new_coupon_info info inner join new_coupon_list list on list.info_serial=info.serial where list.serial=:cs';
                $couponQuery = $conn ->prepare($query);
                $couponQuery -> bindParam(":cs", $couponSerial);
                $couponQuery -> execute();
                $couponStmt = $couponQuery -> fetch();

                $coupon = $couponStmt['value']; //쿠폰사용액
                if($coupon == '')
                    $coupon=0;

                $query = 'select name, branchName FROM new_rentCompany a INNER JOIN new_rentCompany_branch b ON a.serial=b.rentCompany_serial where b.serial="'.$fcompanycode.'"';
                $bcsql = $conn -> prepare($query);
                $bcsql -> execute();
                $companyname = '고고렌트카';
                if($bcStmt = $bcsql -> fetch()){
                    $companyname = $bcStmt['name'];
                    $branchName = $bcStmt['branchName'];
                }

                $balance = new newCompleteCancelBalancePrimium($rentPrice,$insuPrice,$delivPrice,$payPrice,$usePoint,$coupon,$originalPrice);

                $accountAmmount = $balance->getBalanceAmmount();
                $cardCommission = $balance->getCardCommission();
                $benefitAmmount = $balance->getBenefitAmmount();

                $givePoint = $balance -> getGivePoint();
                $total_givePoint += $givePoint;

                $totalBenefit_ammount+=$benefitAmmount;

                $commission = $originalPrice-$accountAmmount-$cardCommission;
                $totalCommission+=$commission;

                $carmore_commission = $commission+$cardCommission;
                $total_carmore_commission += $carmore_commission;
                $EXCEL_STR .= '<tr>';
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>".$category."</td>";
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>단기</td>";
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>".$userName."&nbsp;/&nbsp;".$driverName."</td>";
                $EXCEL_STR .= "<td>".substr($startDate,0,16)." ~ ".substr($endDate,0,16)."</td>";
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>0</td>";
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $accountAmmount)."</td>";
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black; border-right: 4px solid darkgoldenrod;'>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $carmore_commission)."</td>";
                $EXCEL_STR .= "<td rowspan='2' style='border-bottom:2px solid black;'>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $carmore_payment)."</td>";
                $EXCEL_STR .= "</tr><tr style='background-color:#e3f2fd;'>";
                $EXCEL_STR .= "<td style='border-bottom:2px solid black;'>".$carModel." - ".$carNumber."</td>";

                $completeCancel_count++;
                $completeCancel_ammount+=$originalPrice;

                $point_ammount+=$balance->getUsePoint();
                $coupon_ammount+=$coupon;
                $pay_ammount+=$balance->getCancellationChargeForUser();

                $totalOriginal_ammount+=$originalPrice;
                $totalRent_ammount+=$rentPrice;
                $totalInsu_ammount+=$insuPrice;
                $totalDeliv_ammount+=$delivPrice;
            }
        }

    }
    $EXCEL_STR .= "</tr>";
}
$total_benefit_ammount = $totalBenefit_rent; // 카드수수료 지급포인트 제하기 전

$EXCEL_STR .="</tr><tr >";
$EXCEL_STR .="<td colspan='3' style='text-align:center; font-size:120%; border-top:3px solid black'><b>실제 금액 및 결제 금액 총 합계<b></td>";
$EXCEL_STR .="<td style='border-top:3px solid black; font-weight:bold; font-size:120%;'>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $totalOriginal_ammount)."</td>";
$EXCEL_STR .="<td style='border-top:3px solid black; font-weight:bold; font-size:120%;'>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $totalBalance_ammount)."</td>";
$EXCEL_STR .="<td style='border-top:3px solid black; font-weight:bold; font-size:120%; border-right: 4px solid darkgoldenrod;'>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $total_carmore_commission)."</td>";
$EXCEL_STR .="<td style='border-top:3px solid black; font-weight:bold; font-size:120%;'>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $total_carmore_payment)."</td>";
$EXCEL_STR .= "</tr>";

$EXCEL_STR .= "</table>";
$pre_EXCEL_STR = "<table><tr><td></td><td></td><td colspan='7' style='border:2px solid black; text-align:center; font-size:30px;'><B>정산기준서</B></td></tr></table>";
$pre_EXCEL_STR .= "<table><tr><td></td><td></td><td colspan='7' style='border:2px solid black; text-align:center; font-size:30px;'><B>$start ~ $ending</B></td></tr></table>";

$pre_EXCEL_STR .= "<br><br><table border='1'><tr>";
$pre_EXCEL_STR .= "<td style='text-align:center; font-size:120%;'><b>구분</b></td>";
$pre_EXCEL_STR .= "<td style='text-align:center; font-size:120%;'><b>건수</b></td>";
$pre_EXCEL_STR .= "<td style='text-align:center; font-size:120%;'><b>금액</b></td>";
$pre_EXCEL_STR .= "</tr><tr>";
$pre_EXCEL_STR .= "<td><b>정상결제</b></td>";
$pre_EXCEL_STR .= "<td>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $normalBalance_count)."</td>";
$pre_EXCEL_STR .= "<td>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $normalBalance_ammount)."</td>";
$pre_EXCEL_STR .= "</tr><tr>";
$pre_EXCEL_STR .= "<td><b>단기 렌트</b></td>";
$pre_EXCEL_STR .= "<td>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $total_normal_cnt)."</td>";
$pre_EXCEL_STR .= "<td>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $total_normal_cost)."</td>";
$pre_EXCEL_STR .= "</tr><tr>";
$pre_EXCEL_STR .= "<td><b>월 렌트</b></td>";
$pre_EXCEL_STR .= "<td>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $total_month_cnt)."</td>";
$pre_EXCEL_STR .= "<td>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $total_month_cost)."</td>";
$pre_EXCEL_STR .= "</tr><tr>";
$pre_EXCEL_STR .= "<td><b>취소수수료</b></td>";
$pre_EXCEL_STR .= "<td>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $semiCancel_count)."</td>";
$pre_EXCEL_STR .= "<td>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $semiCancel_ammount)."</td>";
$pre_EXCEL_STR .= "</tr><tr>";
$pre_EXCEL_STR .= "<td><b>총 매출</b></td>";
$pre_EXCEL_STR .= "<td></td>";
$pre_EXCEL_STR .= "<td>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $totalRevenue)."</td>";
$pre_EXCEL_STR .= "</tr><tr>";
$pre_EXCEL_STR .= "<td><b>총 정산</b></td>";
$pre_EXCEL_STR .= "<td>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $totalBalance_count)."</td>";
$pre_EXCEL_STR .= "<td>".preg_replace("/(?<=[0-9])(?=(\d{3})+(?!\d))/", ",", $totalBalance_ammount)."</td>";
$pre_EXCEL_STR .= "</tr></table>";

$EXCEL_STR = $pre_EXCEL_STR."<br><br>".$EXCEL_STR;

echo "<meta content=\"application/vnd.ms-excel; charset=UTF-8\" name=\"Content-type\"> ";
echo $EXCEL_STR;

?>