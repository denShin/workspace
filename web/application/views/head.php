<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Team O2</title>

        <!-- Bootstrap core CSS -->
        <link rel="shortcut icon" href="http://workspace.teamo2.kr/static/images/logo.png">

        <link href="/static/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="/static/lib/bootstrap/css/bootstrap-theme.css" rel="stylesheet">

        <!-- Placed at the end of the document so the pages load faster -->

        <link href="<?=asset_url()?>css/workspace_common.min.css" rel="stylesheet" type="text/css" />

<?php
if (isset($library))
{
    foreach ($library as $library_name)
    {
        ?>
        <link href="<?=base_url()?>static/lib/<?=$library_name?>/<?=$library_name?>.min.css" rel="stylesheet" type="text/css" />
        <?
    }
}

if (isset($css))
{
    foreach ($css as $css_file_name)
    {
        ?>
        <link href="<?=asset_url()?>css/<?=$css_file_name?>.min.css" rel="stylesheet" type="text/css" />
        <?
    }
}
?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="/static/lib/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

        <!-- https://fontawesome.com/v4.7.0/icons/-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script language="JavaScript">
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            } else {
                  return true;
              }
            }
        </script>
    </head>
<?php
$loginmaster_yn    = $_SESSION["loginmaster_yn"];
$loginpointauth_yn = $_SESSION["loginpointauth_yn"];
$logincalc_yn      = $_SESSION["logincalc_yn"];
$loginordermng_yn  = $_SESSION["loginordermng_yn"];

?>

    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top navbar-default">

            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"> 카모아관리자</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">DashBoard<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/carmore/Dashboard" >대시보드</a></li>
                                <li class="divider"></li>

                                <li><a href="/carmore/PeroidStaticManage">기간별통계</a></li>
                                <li><a href="/carmore/ReorderStaticManage">신규/재주문통계</a></li>
                                <li><a href="/carmore/CompanyReorderStaticManage">신규/재주문통계(회사별)</a></li>
                                <li class="divider"></li>

                                <li><a href="/carmore/MemberStaticManage">회원가입통계</a></li>
                                <li class="divider"></li>

                                <li><a href="/carmore/TimeStaticManage">요일/시간통계</a></li>
                                <li><a href="/carmore/DateGapManage">예약/대여일 Gap</a></li>
                                <li class="divider"></li>

                                <li><a href="/carmore/CompanyStaticManage">회사별통계</a></li>
                                <li><a href="/carmore/CartypeStaticManage">차종별통계</a></li>
                                <li class="divider"></li>

                                <li><a href="/carmore/CallStaticManage">제휴사 전화통계</a></li>
                                <li><a href="/carmore/CalldtlStaticManage">주간 전화연결 통계</a></li>
                                <li><a href="/carmore/ShortrentStaticManage">단기렌트_검색 통계</a></li>
                                <li><a href="/carmore/LongrentStaticManage">장기렌트_검색 통계</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">카모아 앱 관리<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/carmore/AppUser">회원관리</a></li>
<?php
if ($loginpointauth_yn === "y" || $loginmaster_yn === "y")
{

?>
                                <li><a href="/carmore/CouponLogManage">쿠폰현황관리</a></li>
                                <li><a href="/carmore/PointLogManage">포인트관리</a></li>
<?php
}

?>

                                <li><a href="/carmore/CarmoreCallMng">고객상담내역</a></li>
                                <li><a href="/carmore/CarmoreCallCate">고객상담카테고리</a></li>
                                <li><a href="/carmore/CarmoreBoard?board_code=complain">상담처리내역</a></li>
                                <li class="divider"></li>

                                <li><a href="/carmore/PeakseasonManage">성수기관리</a></li>
                                <li class="divider"></li>
<?php
if ($loginpointauth_yn === "y" || $loginmaster_yn === "y")
{

?>
                                <li><a href="/carmore/CouponManage">쿠폰등록관리</a></li>
<?php
}

?>
                                <li><a href="/carmore/EventManage">이벤트관리</a></li>
                                <li><a href="/carmore/PushManage">푸쉬관리</a></li>
                                <li class="divider"></li>

                                <li><a href="/carmore/log/LocationUsage">위치정보이용 로그</a></li>

                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">차량/예약관리<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                                <li><a href="/carmore/Carinfomaster">차종마스터정보</a></li>
                                <li><a href="/carmore/Carinfomatching">차종매칭하기</a></li>
                                <li><a href="/carmore/Areacarfavorite">지역차종즐겨찾기</a></li>

<?php
if ($loginordermng_yn === "y" || $loginmaster_yn === "y")
{

?>
                                <li class="divider"></li>
                                <li><a href="/carmore/ReservationManage">차량예약관리</a></li>
                                <li><a href="/carmore/Earlycareturn">차량조기반납신청</a></li>
                                <li><a href="/carmore/PayManage">결제관리</a></li>

                                <li class="divider"></li>
                                <li><a href="/carmore/alarm/biztalk/BiztalkInventory">비즈톡 전송목록</a></li>
                                <li><a href="/carmore/alarm/biztalk/BiztalkErrorInventory">비즈톡 전송오류목록</a></li>
<?php
}

?>

                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">제휴사 관리<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/partners/PartnerNoticeBoard">공지사항</a></li>
                                <li class="divider"></li>

                                <li><a href="/partners/PartnerMember">업체 관리</a></li>
                                <li><a href="/partners/PartnerMemberBranch">업체 지점 관리</a></li>
                                <li><a href="/partners/Callpartner">전화걸기 업체관리</a></li>
                                <li class="divider"></li>

                                <li><a href="/partners/Tel050manage?teltype=use">050 전체리스트</a></li>
                                <li><a href="/partners/Tel050manage?teltype=expire">050 결번리스트</a></li>
                                <li><a href="/partners/Tel050manage?teltype=gold">050 골드넘버</a></li>
                                <li class="divider"></li>

                                <li><a href="/partners/Reviewmanage">리뷰관리</a></li>
                                <li class="divider"></li>

                                <li><a href="/carmore/StrollerInventory">유모차/카시트 업체 관리</a></li>
                            </ul>
                        </li>
<?php
if ($loginordermng_yn === "y" || $loginmaster_yn === "y")
{

?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">API 업체 관리<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/apiAffiliate/ApiCompensationManage?type=r">종합보험 관리</a></li>
                                <li><a href="/apiAffiliate/ApiCdwManage">자차보험 관리</a></li>
                                <li class="divider"></li>

                                <li><a href="/apiAffiliate/ApiAffiliateInventory">API 업체 관리</a></li>
                                <li><a href="/apiAffiliate/ApiAffiliateCar?type=r">API 업체 차량관리</a></li>
                                <li class="divider"></li>

                                <li><a href="/apiAffiliate/reserv/ReservInventory">API 업체 예약목록</a></li>

                            </ul>
                        </li>
<?php
}

?>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">정산 관리<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/calculate/Calculatelog">정산관리</a></li>
                                <li><a href="/calculate/Taxinfo?ptype=mailform">정산메일폼관리</a></li>
                                <li class="divider"></li>
                                <li><a href="/calculate/BranchTaxinfo">업체별 정산/세금정보</a></li>

                                <li class="divider"></li>
                                <li><a href="/calculate/Taxinfo?ptype=mycompany">세금계산서 회사정보</a></li>
                                <li><a href="/calculate/Taxpaperlog">세금계산서 발행내역</a></li>
                            </ul>
                        </li>

                        <li class="dropdown" >
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">환경설정<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <?php

                                if($loginmaster_yn=="y"){
                                    ?>
                                    <li><a href="/adminmanage/TeamManage">팀정보 관리</a></li>
                                    <li><a href="/adminmanage/AdminUserManage">관리자 관리</a></li>
                                <?}?>
                                <li class="divider"></li>
                                <li><a href="/adminmanage/Changepwd">비밀번호 변경</a></li>
                            </ul>
                        </li>



                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li >
                            <a href="#" onclick="logoutaction()"  role="button" aria-expanded="false">로그아웃</a>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>




<script language="javascript">
    function logoutaction()
    {

        if(confirm("로그아웃 하시겠습니까?"))
        {
            location.href="/adminmanage/Logout";
        }
    }
</script>
