<Script language="JavaScript">

    function addCommas(intNum) {
        return (intNum + '').replace(/(\d)(?=(\d{3})+$)/g, '$1,');
    }

    function dateparse(str) {
        if(!/^(\d){8}$/.test(str)) return "invalid date";
        var y = str.substr(0,4),
            m = str.substr(4,2),
            d = str.substr(6,2);
        m =m-1;
        return new Date(y,m,d);
    }

    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    }

    function returncardatechange(calcelstartdate,calcelenddate,calceltargetprice,calcelusepoint){
        var canceldate =$("#canceldate").val();

        var vcanceldate = canceldate.replace(/-/gi, "");


        // calcelstartdate, canceldate 비교
        var date1 = dateparse(calcelstartdate);
        var startdateinfo =addDays(date1,3);
        var date2 = dateparse(vcanceldate);
        var today = new Date();

        if(date2 < startdateinfo){
            alert('+3일 이상 대여해야합니다. 날짜 설정이 잘못되었습니다');
            var curr_date = date1.getDate();
            var curr_month = date1.getMonth() + 1; //Months are zero based
            var curr_year = date1.getFullYear();

            $("#canceldate").val(curr_year + "-" + curr_month + "-" + curr_date);
            return ;
        }

        console.log("calcelstartdate="+calcelstartdate+",vcanceldate="+vcanceldate+",date1:"+date1+",date2:"+date2);

        console.log("/carmore/Earlycareturn?ptype=getcancelprice&startdate="+calcelstartdate+"&enddate="+calcelenddate
            +"&targetprice="+calceltargetprice+"&usepoint="+calcelusepoint+"&targetdate="+vcanceldate+"&jsonyn=y");
        $.ajax({
            type: "get",
            url: "/carmore/Earlycareturn?ptype=getcancelprice&startdate="+calcelstartdate+"&enddate="+calcelenddate
                +"&targetprice="+calceltargetprice+"&usepoint="+calcelusepoint+"&targetdate="+canceldate+"&jsonyn=y"  ,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                console.log("response:"+response);
                var json = JSON.parse(response);

                var refundprice = json["refundprice"];
                cancelper = json["cancelper"];
                useprice= json["useprice"];

                $("#emailpenalty").text( addCommas(useprice));
                $("#cancelrefundprice").val(refundprice);
                $("#useprice").val(useprice);
                var refundprice_str = addCommas(refundprice);
                $("#returnprice_str").text(refundprice_str+" 원");

            }
        });
    }

    /*
                <input type="text" name="crti_idx">
            <input type="text" name="cancelamt">
            <input type="text" name="cancelper">
            <input type="text" name="canceldate">
            <input type="text" name="useprice">


     */

   function refundtoclient() {
       var docuform=document.cancelfrm;

       var cancelcrti_idx = "<?=$data["crti_idx"]?>";
       var cancelper = "<?=$data["cancelper"]?>";

       var cancelamt = $("#cancelrefundprice").val();
       var canceldate = $("#canceldate").val();

       if (cancelamt === "") {
           alert('환불금액을 기입해주세요');
           $("#cancelrefundprice").focus();
           return;
       }

       if (canceldate === "") {
           alert('반환날짜를 기입해주세요');
           $("#canceldate").focus();
           return;
       }

       console.log("/carmore/Careturnproc?ptype=setcareturn&crti_idx=" + cancelcrti_idx + "&cancelper=" + cancelper + "&useprice=" + useprice + "&canceldate=" + canceldate + "&cancelamt=" + cancelamt);

       if (confirm('차량 조기반납 처리를 진행하시겠습니까?')) {

           docuform.crti_idx.value= "<?=$data["crti_idx"]?>";
           docuform.cancelamt.value= cancelamt;
           docuform.cancelper.value= "<?=$data["cancelper"]?>";
           docuform.canceldate.value= canceldate;
           docuform.action="/carmore/Careturnproc";
          docuform.submit();

       }

   }


    function sendcomment(){
        var form=document.af;
        if(form.commenttext.value===""){
            alert('코멘트를 기입해주세요');
            form.commenttext.focus();
            return;
        }
        form.datamode.value="new";
        form.submit()
    }


    function delcomment(comment_idx){

        if(confirm('코멘트를 삭제하시겠습니까?')){
            var form=document.af;
            form.comment_idx.value=comment_idx;
            form.datamode.value="del";
            form.submit()
        }
    }

    function changereturnaddr(){

        var crti_idx = "<?=$data["crti_idx"]?>";
        var f_reservationidx = "<?=$data["f_reservationidx"]?>";
        var return_addr = $("#return_addr").val();

        if (return_addr === "") {
            alert('받으실 주소를 기입해주세요');
            $("#return_addr").focus();
            return;
        }

        console.log("/carmore/Careturnproc?ptype=changereturnaddr&crti_idx=<?=$data["crti_idx"]?>&f_reservationidx=<?=$data["f_reservationidx"]?>&return_addr=" +return_addr);
        $.ajax({
            type: "get",
            url: "/carmore/Careturnproc?emode=changereturnaddr&crti_idx=<?=$data["crti_idx"]?>&f_reservationidx=<?=$data["f_reservationidx"]?>&return_addr=" +return_addr,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                console.log("response:"+response);
              alert('처리완료');

            }
        });
    }

</Script>
<style>
    @media (min-width:100px) {
        .txt-xs {
            font-size: 0.1em;
        }
        .txt-s {
            font-size: 0.3em;
        }
        .txt-m {
            font-size: 0.5em;
        }
        .txt-xxl {
            font-size: 1.6em;
        }
        .txt-bold {
            font-weight: 600;
        }
        .table-title {
            width: 30%;
        }
        .table-contents {
            width: 70%;
        }
        .contents-root {
            margin-left: 10px;
            margin-right: 10px;
        }
        .reservation-cancel-root {
            margin-top: 30px;
            margin-bottom: 30px;
        }
    }

    @media (min-width:767px) {
        .txt-xs {
            font-size: 0.5em;
        }
        .txt-s {
            font-size: 0.7em;
        }
        .txt-m {
            font-size: 0.9em;
        }
        .txt-xxl {
            font-size: 2em;
        }
        .txt-bold {
            font-weight: 600;
        }
        .table-title {
            width: 25%;
        }
        .table-contents {
            width: 75%;
        }
        .contents-root {
            margin-left: 30px;
            margin-right: 30px;
        }
        .reservation-cancel-root {
            margin-top: 70px;
            margin-bottom: 30px;
        }
    }

    .table-title {
        padding-right: 10px;
        padding-left: 10px;
        height: 30px;
        text-align: left;
        background-color: #f2f2f2;
        border-bottom: 1px solid #BDBDBD;
    }

    .table-contents {
        padding-right: 10px;
        padding-left: 10px;
        height: 30px;
        border-bottom: 1px solid #BDBDBD;
    }

    .reservation-cancel-root {
        background-color: #f2f2f2;
        border: 1px solid #d9d9d9;
        padding: 15px;
        color: #595959;
    }

</style>
<div class="container-fluid" style="padding-top: 20px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }


    </style>


    <div class="page-header clearfix">
        <form id="searchCompanyNotice" class="form-horizontal" role="form" method="get" >
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-4">
                            <h3>조기반납처리</h3>
                        </div>
                        <div class="pull-right" style="padding-right: 30px">
                                <button type="button" onclick="location.href='/carmore/Earlycareturn'" class="btn btn-primary btn-block">목록가기 </button>
                        
                        </div>

                    </div>
                </div>
            </fieldset>
        </form>

    </div>

    <div class="row">

        <div class="col-md-8" style="overflow-x: hidden; overflow-y: scroll;height: 700px ">
            <div class="clearfix"></div>

            <div style="width: 100%; max-width:780px; margin:auto;">
                <div style="border: 1px solid #d9d9d9;width: 100%;">

                    <!--header-->

                    <div class="contents-root">
                        <div style="margin-top: 30px;font-size: 20px;">
                            <img src="https://carmore.kr/app_v3/php/images/emailimg/appointment.png" style="">
                            <table style="border-spacing: 0;border-top: 3px solid black;padding: 0;margin: 0;width: 100%; text-align: right;">
                                <tr>
                                    <td class="table-title txt-m">회사정보</td>
                                    <td class="table-contents txt-m"><?=$data["company"]?></td>
                                </tr>
                                <tr>
                                    <td class="table-title txt-m">예약번호</td>
                                    <td class="table-contents txt-m"><?=$data["residx"]?></td>
                                </tr>
                                <tr>
                                    <td class="table-title txt-m">예약기간</td>
                                    <td class="table-contents txt-m"><?=$data["startdate"]?> ~ <?=$data["enddate"]?></td>
                                </tr>
                                <tr>
                                    <td class="table-title txt-m">예약차량</td>
                                    <td class="table-contents txt-m"><?=$data["carmodel"]?></td>
                                </tr>
                                <tr>
                                    <td class="table-title txt-m">운전자</td>
                                    <td class="table-contents txt-m"><?=$data["driver"]?></td>
                                </tr>
                                <tr>
                                    <td class="table-title txt-m">배차</td>
                                    <td class="table-contents txt-m"><?=$data["pickupaddr"]?></td>
                                </tr>
                                <tr>
                                    <td class="table-title txt-m">회차</td>
                                    <td class="table-contents txt-m">
                                        <div class="row" style="padding-top: 15px;padding-bottom: 15px">
                                            <div class="col-md-10">
                                            <?=$data["f_delivreturnaddr"]?>
                                            </div>


                                        </div>

                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div style="margin-top: 30px;  font-size: 20px;">
                            <img src="https://carmore.kr/app_v3/php/images/emailimg/payment.png" style="">
                            <table style="border-spacing: 0;  border-top: 3px solid black;  padding: 0;  margin: 0; width: 100%;  text-align: right;">
                                <tr>
                                    <td class="table-title txt-m">총 결제금액</td>
                                    <td class="table-contents txt-m" style="padding-bottom:5px;"><?=$data["origincost"]?> 원
                                        <br>
                                        <div class="txt-xs">(카드결제 <?=$data["dbpayment"]?> 원 + 쿠폰/포인트 <?=$data["discount"]?>원)</div>
                                    </td>
                                </tr>
                           
                                <tr>
                                    <td class="table-title txt-m">취소/환불 수단</td>
                                    <td class="table-contents txt-m">신용카드</td>
                                </tr>
                            </table>
                        </div>


                        <div style="margin-top: 30px;  font-size: 20px;">
                            <h3>취소/환불 정보</h3>
                            <table style="border-spacing: 0;  border-top: 3px solid black;  padding: 0;  margin: 0; width: 100%;  text-align: right;">
                                <tr>
                                    <td class="table-title txt-m">조기반납 신청일</td>
                                    <td class="table-contents txt-m">
                                        <div class="row" style="padding-top: 15px;padding-bottom: 15px">
                                            <div class="col-md-3">
                                                <?if($data["crti_early_return_apply"]=="1"){?>
                                                <input type="date" id="canceldate" value="<?=$data["logdate"]?>"
                                                       onchange="returncardatechange('<?=$data["cstartdate"]?>','<?=$data["cenddate"]?>','<?=$data["crti_principal"]?>', '<?= $data["crti_use_point"] ?>')">
                                                <?}else if($data["crti_early_return_apply"]=="2"){?>
                                                    <?=$data["logdate"]?>
                                                <?}?>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="table-title txt-m">취소 수수료 차감</td>
                                    <td class="table-contents txt-m" style="color:red;" align="left">
                                    <span id="emailpenalty"> <?=number_format($data["emailpenalty"])?></span>   원
                                    </td>
                                </tr>
                                <tr>
                                    <td class="table-title txt-m" style="color:red;">총 취소/환불금액</td>
                                    <td class="table-contents txt-m"   align="left">
                                        <?if($data["crti_early_return_apply"]=="1"){?>

                                            <div class="row" style="padding-top: 15px;padding-bottom: 15px">
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" name="cancelrefundprice" id="cancelrefundprice"  value="<?=$data["cancelrefundprice"]?>">
                                                </div>
                                                <div class="col-md-1">
                                                    원
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" onclick="refundtoclient()"  class="btn btn-danger btn">취소등록하기</button>
                                                </div>
                                            </div>
                                        <?}else if($data["crti_early_return_apply"]=="2"){?>
                                            <?= number_format($data["cancelrefundprice"])?>   원
                                        <?}?>
                                    </td>
                                </tr>


                            </table>
                        </div>


                    </div>
                </div>
            </div>

        </div>



        <form name="cancelfrm" method="post" >

            <input type="hidden" name="emode" value="setcareturn">
            <input type="hidden" name="crti_idx">
            <input type="hidden" name="cancelamt">
            <input type="hidden" name="cancelper">
            <input type="hidden" name="canceldate">
            <input type="hidden" name="useprice" id="useprice" value="<?=$data["useprice"]?>">

        </form>

        <div class="col-md-4" style="overflow-x: hidden; overflow-y: scroll;height: 700px ">
            <div class="row" style="padding-top: 15px">
             <form name="af" method="post" action="/carmore/Careturnproc">
                 <input name="emode" type="hidden" value="commentproc">
                 <input name="datamode" type="hidden">
                 <input name="crti_idx" type="hidden" value="<?=$data["crti_idx"]?>">
                 <input name="comment_idx" type="hidden"  >


                <div class="col-md-8">
                    <textarea   class="form-control"  name="commenttext" placeholder="관리자 코멘트를 남겨주세요"  style="width: 100%;height: 80px"  ><?=$data["carinfomst_memo"]?></textarea>
                </div>
                <div class="col-md-2">
                    <button type="button" onclick="sendcomment()" class="btn btn-primary btn-block">저장</button>
                </div>
             </form>
            </div>
            <br>
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-10">코멘트</th>
                    <th class="text-center col-md-2">삭제</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $now_id = $data["now_id"];
                foreach($data["commentlist"] as $entry) {
                    $regdate = date("m/d H:i",strtotime($entry["regdate"]));
                    $writer_id = $entry["mem_id"];
                    $writer_name = $entry["writer_name"];

                    ?>
                    <tr class="row">

                        <td   class="small text-left"  >
                            <span style="font-size: 1.4em"><?=$entry["commenttext"]?></span><Br>
                                <span style="font-size: 0.8em"><?=$writer_name?> (<?=$regdate?>)</span>
                        </td>

                        <td  class="small text-center">
                            <?if($writer_id==$now_id){?>
                            <button type="button" class="btn btn-danger btn-sm" onclick="delcomment('<?=$entry["comment_idx"]?>')">
                                <span class="glyphicon glyphicon-minus"></span> 삭제
                            </button>
                            <?}?>
                        </td>
                    </tr>
                <?}?>

                </tbody>
            </table>

        </div>


    </div>


