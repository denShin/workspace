 
<div class="container-fluid" style="padding-top: 50px;padding-bottom: 70px;">

    <style>
        .double-input .form-control {
            width: 50%;
        }
    </style>

    <div class="page-header">
        <h2>고객 전화 내역</h2>
    </div>

    <div class="row">

        <div class="col-md-5" style="overflow-x: hidden; overflow-y: scroll;height: 500px ">
            <div id="searchdiv">
                <form id="searchCompanyNotice" class="form-horizontal" role="form" method="get" onsubmit="return false" >
                    <fieldset>
                        <div class="clearfix">
                            <div class="form-group">

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="sv"  id="sv" onkeypress="entersend()" placeholder="*예약자명, 전화번호 뒤 4자리"  >

                                </div>
                                <div class="col-md-2">
                                    <button type="button" onclick="searchdata()" class="btn btn-primary btn-block">검색하기</button>
                                </div>

                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>


            <table class="table table-striped table-hover table-bordered"  id="infotbl" style="width: 100%">
                <tr class="info ">
                    <th class="text-center col-md-2">날짜</th>
                    <th class="text-center col-md-2">고객명</th>
                    <th class="text-center col-md-2">연락처</th>
                    <th class="text-center col-md-6">체크내용</th>
                </tr>
            </table>
        </div>

        <div class="col-md-7">
            <form name="af" class="form-horizontal" role="form" method="post" enctype="multipart/form-data"  action="/carmore/CarmoreCallMng">
                <input type="hidden" name="ptype" value="setcallinfo">
                <input type="hidden" name="emode" value="<?=$data["emode"]?>">
                <input type="hidden" name="call_idx" value="<?=$data["call_idx"]?>">

                <fieldset>
                    <div class="form-group">
                        <label for="shareCategory" class="col-md-2 control-label">만나이계산</label>
                        <div class="col-md-3">
                            <input type="text"  class="form-control" id="birthval"onkeypress="onlyNumber()" maxlength="8"  placeholder="19901225 형태" onkeyup="calage(this)"/>
                        </div>
                        <div class="col-md-2">
                            <p class="form-control-static" id="txtage"></p>
                        </div>
                    </div>

                    <br>
                    <div class="form-group">
                        <label for="shareCategory" class="col-md-2 control-label">고객명</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="call_name" id="call_name" placeholder="고객명 입력하세요" value="<?=$data["call_name"]?>" autofocus/>
                        </div>
                        <label for="shareCategory" class="col-md-1 control-label">연락처</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="call_number" placeholder="연락처 입력하세요" value="<?=$data["call_number"]?>" autofocus/>
                        </div>
                        <label for="shareCategory" class="col-md-1 control-label">예약번호</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="call_reservnum" placeholder="예약번호 입력하세요" value="<?=$data["call_reservnum"]?>" autofocus/>
                        </div>
                    </div>



                    <div class="form-group">


                        <label for="shareCategory" class="col-md-2 control-label">작성자</label>
                        <div class="col-md-2">
                            <p class="form-control-static"><?=$data["mem_name"]?></p>
                        </div>
                        <label for="shareCategory" class="col-md-1 control-label">날짜/시간</label>
                        <div class="col-md-3">
                            <input type="date" id="call_date" name="call_date" value="<?=$data["call_date"]?>"  class="form-control">
                        </div>
                        <div class="col-md-2">

                            <input type="text" placeholder="00:00"  id="call_time"  name="call_time" value="<?=$data["call_time"]?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shareCategory" class="col-md-2 control-label">구분</label>
                        <div class="col-md-2">
                            <select name="call_part" class="form-control">
                                <option value="IN" <?if($data["call_part"]=="IN"){ echo "selected";}?> >IN</option>
                                <option value="OUT" <?if($data["call_part"]=="OUT"){ echo "selected";}?> >OUT</option>
                            </select>
                        </div>

                        <label for="shareCategory" class="col-md-1 control-label">카테고리</label>
                        <div class="col-md-4">

                            <select name="callboardcate_idx" class="form-control">

                                <?php
                                echo $data["cate"];
                                ?>

                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="shareContent" class="col-md-2 control-label">문의내용</label>
                        <div class="col-md-6">
                            <textarea  class="form-control" style="height:100px" name="call_ques" id="call_ques"><?=$data["call_ques"]?></textarea>
                        </div>

                    </div>

                    <div class="form-group">

                        <label for="shareContent" class="col-md-2 control-label">운영자답변</label>
                        <div class="col-md-6">
                            <textarea  class="form-control" style="height:100px" name="call_response"  id="call_response"><?=$data["call_response"]?></textarea>
                        </div>
                    </div>
                    <div class="form-group">

                        <label for="shareContent" class="col-md-2 control-label">비고</label>
                        <div class="col-md-6">
                            <textarea  class="form-control" style="height:100px" name="call_etc" id="call_etc"><?=$data["call_etc"]?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <div class="row">
                                <div class="col-md-5">
                                    <button type="button" onclick="sendit()" class="btn btn-primary btn-block">저장하기</button>
                                </div>
                                <div class="col-md-3">
                                    <button type="reset" class="btn btn-danger btn-block">다시작성</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </fieldset>
            </form>

        </div>
    </div>


    <script type="text/javascript">

        function entersend(){
            if(event.keyCode == 13){
                searchdata();
            }
        }

        function onlyNumber(){
            if((event.keyCode<48)||(event.keyCode>57))
                event.returnValue=false;
        }

        function calage(obj){
            var birthdate =obj.value;

            if( birthdate.length <8){
                $("#txtage").html('');
                return;
            }
            var year = birthdate.substring(0, 4);
            var month = birthdate.substring(4, 6);
            var day = birthdate.substring(6, 8);

            var birthdate= year + '-' + month + '-' + day;

            var birthday = new Date(birthdate);
            console.log(birthday);
            var today = new Date();
            var years = today.getFullYear() - birthday.getFullYear();
            birthday.setFullYear(today.getFullYear());
            if (today < birthday)
            {
                years--;
            }
            if(years){
                $("#txtage").html("만 <b>" + years + "</b> 세 ");
            }else{
                $("#txtage").html('');
            }
        }

        function searchdata(){
            var sv=$("#sv").val();

            if(sv===""){
                alert('*검색어를 기입해주세요!');
                $("#sv").focus();
                return;
            }

            $(".sresult").remove();
            $.ajax({
                type:"get",contentType: "application/json",
                url:"/carmore/CarmoreCallMng?ptype=search&sv="+sv,
                datatype: "json",
                success: function(data) {
                    var json = $.parseJSON(data);
                    var jsonlength = json.length;
                    if(jsonlength >0){

                        for(var i=0;i < jsonlength ; i++) {

                            var row = json[i];
                            var name = row["name"];
                            var tel = row["tel"];
                            var desc  = row["desc"];
                            var date =row["date"];
                            $('#infotbl').append('<tr class="sresult"><td>'+date+'</td><td>'+name+'</td><td>'+tel+'</td><td>' + desc + '</td></tr>');
                        }
                    }else{
                        $('#infotbl').append('<tr class="sresult"><td colspan="4" align="center">`' + sv + '` 검색결과가 없습니다.</td></tr>');
                    }


                },
                error: function(x, o, e) {

                }
            });

        }

        function sendit(){
            var form=document.af;


            if(form.call_number.value===""){
                alert('*고객연락처를 기입해주세요');
                form.call_number.focus();
                return;
            }

            if(form.call_date.value===""){
                alert('*상담날짜를 기입해주세요');
                form.call_date.focus();
                return;
            }
            if(form.call_time.value===""){
                alert('*상담시간을 기입해주세요');
                form.call_time.focus();
                return;
            }
            if(form.call_ques.value===""){
                alert('*문의내용을 기입해주세요');
                form.call_ques.focus();
                return;
            }
            
            if(confirm('*상담내역을 등록하시겠습니까?')){
                document.af.submit();
            }
        }

    </script></div>
