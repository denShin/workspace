<Script language="JavaScript">

    function carimgaemodal(ptype, carinfokey){

        var headurl ="http://workspace.teamo2.kr/carmore/Carinfoimage?ptype="+ptype+"&carinfokey="+carinfokey;


        $.ajax({
            url: headurl,
            type: 'GET',
            cache: false,
        }).done(function(result){

            $('#viewmodal .modal-body').html(result)
            $('#viewmodal').modal('show')
        });


    }

    function closeviewmodal(){
        $("#viewmodal").modal("hide");
        location.reload();
    }


</Script>
<div  id="viewmodal" class="modal fade "  >

    <div class="modal-dialog viewmodalwidth ">

        <div class="modal-content "  style="background-color: white;">
            <div class="modal-header">

                <span class="h2">차종 이미지 관리</span>

                <button type="button" class="btn  btn-outline-black  btn close"  onclick="closeviewmodal()" >
                    <span class="h2">X</span>
                </button><br>

            </div>
            <div class="modal-body" style="text-align: left;background-color: white;">
            </div>

            <!-- Content will be loaded here from "remote.php" file -->
        </div>
    </div>
</div>

<div class="container-fluid" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }

        #viewmodal{
            overflow:auto !important;
            text-align: center;
            padding: 0!important;
        }
        .viewmodalwidth{
            min-width: 70%;
            margin: 0;
            display: inline-block; vertical-align: middle;
        }

    </style>


    <div class="page-header clearfix">
        <h2 class="pull-left">차종 정보 관리</h2>
        <div class="pull-right" style="padding-top: 30px">
            <a href="?ptype=w&board_code=<?=$data["board_code"]?>" class="btn btn-info">등록하기</a>
        </div>
    </div>
    <div id="searchdiv">
        <form id="searchCompanyNotice" class="form-horizontal" role="form" method="get" >
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <select class="form-control" name="sp">
                                <option value="carinfomst_brand" <?if($data["sv"]=="carinfomst_brand"){?>selected<?}?> >브랜드</option>
                                <option value="carinfomst_type"<?if($data["sv"]=="carinfomst_type"){?>selected<?}?>  >타입</option>
                                <option value="carinfomst_model" <?if($data["sv"]=="carinfomst_model"){?>selected<?}?> >모델명</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="sv" value="<?=$data["sv"]?>">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                        <div class="col-md-1">  <button type="button" onclick="location.href='/carmore/Carinfomaster?sp=clear'" class="btn btn-danger btn-sm">초기화</button></div>
                    </div>
                </div>
            </fieldset>
        </form>
        <hr/>
    </div>


    <form id="updateConfirm">
        <div class="clearfix"></div>

        <div class="table-responsive">
            <table class="table  table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">No</th>
                    <th class="text-center col-md-1">제조사</th>
                    <th class="text-center col-md-1">차량등급</th>
                    <th class="text-center col-md-2">차종이름</th>
                    <th class="text-center col-md-2">차량설명</th>
                    <th class="text-center col-md-1">연비</th>
                    <th class="text-center col-md-2">대표사진</th>
                    <th class="text-center col-md-1">서브사진</th>
                    <th class="text-center col-md-1">관리</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($data["list"] as $entry) {

                    $files =$entry["filelist"];
                    $infocheck =$entry["infocheck"];

                    if($infocheck=="n"){
                        $infocheckcolor="#D9D9D9";
                    }else{
                        $infocheckcolor="white";
                    }
                    ?>
                    <tr class="row" style="background: <?=$infocheckcolor?>">
                        <td  class="small text-center"><?=$entry["carinfo_idx"]?> </td>
                        <td   class="small text-center"  ><?=$entry["carinfomst_brand"]?></td>
                        <td   class="small text-center"><?=$entry["carinfomst_typestr"]?> </td>
                        <td  style="padding-left: 10px;align:left"  class="small">  <a href="?ptype=v&carinfokey=<?=$entry["carinfokey"]?>&per_page=<?=$data["per_page"]?>"><?=$entry["carinfomst_model"]?></a>  </td>
                        <td   class="small text-center"><?=$entry["carinfomst_memo"]?> </td>
                        <td   class="small text-center"><?=$entry["min_fuel_efficiency"]?> /<?=$entry["max_fuel_efficiency"]?> </td>
                        <td   class="small text-center">

                            <span style="padding-left: 5px">
                                <img  src="<?=$entry["fullurl"]?>" width="60">
                            </span>

                            <button type="button" class="btn  btn-outline-primary   btn-xs" onclick="carimgaemodal('main','<?=$entry["carinfokey"]?>')" >
                                <span class="glyphicon glyphicon-picture"></span> 메인이미지
                            </button>

                        </td>
                        <td   class="small text-center">
                            <button type="button" class="btn  btn-outline-primary   btn-xs" onclick="carimgaemodal('sub','<?=$entry["carinfokey"]?>')" >
                                <span class="glyphicon glyphicon-picture"></span> <?=$entry["subcnt"]?>개 이미지
                            </button>
                        </td>
                        <td   class="small text-center">
                            <span class="label   label-primary" style="cursor: pointer" onclick="editcarinfo('<?=$entry["carinfokey"]?>')">수정</span>
                            &nbsp;<span class="label   label-danger"style="cursor: pointer" onclick="delcarinfo('<?=$entry["carinfokey"]?>')">삭제</span>

                            </td>
                    </tr>
                <?}?>

                </tbody>
            </table>
        </div>
    </form>
<script>

    function editcarinfo(carinfokey){
        location.href="?ptype=w&carinfokey="+carinfokey+"&board_code=carmst&content_code="+carinfokey;
    }
    function delcarinfo(carinfokey){
        if (confirm("정말 삭제하시겠습니까?")) {
            location.href="/carmore/CarinfomasterProc?emode=del&carinfokey="+carinfokey;
        }


    }
</script>

    <div class="text-center">
        <?=$data['pagination']?>
    </div>
