
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>

    <div class="page-header clearfix">
        <h2 class="pull-left">이벤트 관리</h2>
        <div class="pull-right" style="padding-top: 30px">
            <a href="?ptype=w&board_code=<?=$data["board_code"]?>" class="btn btn-info">등록하기</a>
        </div>
    </div>

    <div class="clearfix" style="padding-top: 10px"></div>

    <button class="btn btn-primary "onclick="location.href='?'">전체이벤트</button>
    <button class="btn btn-success" onclick="location.href='?ptype=n'">진행중이벤트</button>

    <div class="clearfix" style="padding-top: 10px"></div>
    <form id="updateConfirm">

        <div class="clearfix"></div>


        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">No</th>
                    <th class="text-center col-md-2">이미지</th>
                    <th class="text-center col-md-4">제목</th>
                    <th class="text-center col-md-1">상태</th>
                    <th class="text-center col-md-2">시작/마감일</th>
                    <th class="text-center col-md-2">등록일</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $startnum = $data["startnum"];
                foreach($data["list"] as $entry) {

                    ?>
                    <tr class="row">
                        <td  class="small text-center"><?=$startnum?> </td>
                        <td   class="small text-center">
                            <?if($entry["fileurl"] !=""){?>
                                <img src="<?=$entry["fileurl"]?>" width="50">
                            <?}?></td>
                        <td  style="padding-left: 10px;align:left"  class="small">  <a href="?ptype=v&board_code=<?=$entry["board_code"]?>&board_idx=<?=$entry["board_idx"]?>&per_page=<?=$data["per_page"]?>"><?=$entry["board_title"]?></a> </td>
                        <td   class="small text-center"><?=$entry["board_statstr"]?></td>
                        <td   class="small text-center"><?=$entry["startdate"]?> ~ <?=$entry["enddate"]?></td>
                        <td   class="small text-center"><?=$entry["regdate"]?></td>
                    </tr>
                    <?
                    $startnum--;
                }?>

                </tbody>
            </table>
        </div>
    </form>


    <div class="text-center">
        <?=$data['pagination']?>
    </div>
