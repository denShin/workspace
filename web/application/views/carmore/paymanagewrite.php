
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">

    <style>
        .double-input .form-control {
            width: 50%;
        }
    </style>

    <div class="page-header">
        <h2>관리자결제등록</h2>
    </div>



    <form id="inputform" class="form-horizontal" role="form" method="post" enctype="multipart/form-data"  action="/carmore/PayManage">
        <input type="hidden" name="emode" value="newpay">
        <input type="hidden" name="f_logstatus" value="1">
        <input type="hidden" name="f_resultcode" value="3001">

        <fieldset>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">회원serial</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" name="f_usrserial" placeholder="회원 serial"  value="<?=$data["f_usrserial"]?>"   autofocus/>
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">예약serial</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" name="f_reservationidx" placeholder="예약 serial""  value="<?=$data["f_reservationidx"]?>"   autofocus/>
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">렌트 serial</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" name="f_bookingid" placeholder="렌트 serial""  value="<?=$data["f_bookingid"]?>"   autofocus/>
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">금액</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" name="f_Amt" placeholder="결제금액""  value="<?=$data["f_Amt"]?>"   autofocus/>
                </div>
            </div>


            <hr>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary btn-block">저장하기</button>
                        </div>
                        <div class="col-md-6">
                            <button type="reset" class="btn btn-warning btn-block">다시작성</button>
                        </div>
                    </div>

                </div>
            </div>
        </fieldset>
    </form>

    <script type="text/javascript">
        $(function() {
            $("#inputform").submit(function() {

                if ($("input[name='f_usrserial']").val() =="") {
                    alert("회원시리얼을 입력하세요.");
                    $("input[name='f_usrserial']").focus();
                    return false;
                }

                if ($("input[name='f_reservationidx']").val() =="") {
                    alert("예약시리얼을 입력하세요.");
                    $("input[name='f_reservationidx']").focus();
                    return false;
                }

                if ($("input[name='f_bookingid']").val() =="") {
                    alert("렌트 시리얼을 입력하세요.");
                    $("input[name='f_bookingid']").focus();
                    return false;
                }

                if ($("input[name='f_Amt']").val() =="") {
                    alert("금액을 입력하세요.");
                    $("input[name='f_Amt']").focus();
                    return false;
                }

                if (!confirm("정말 등록하시겠습니까?")) {

                    return false;
                }
            });

        });


    </script></div>
