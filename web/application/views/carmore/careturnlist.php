<div class="container-fluid" style="padding-top: 50px;padding-bottom: 70px;">

    <div class="page-header clearfix">
        <h3 class="pull-left">차량 조기반납 관리  </h3>
        <div class="pull-right" style="padding-top: 20px">
            <a href="/carmore/Earlycareturn" class="btn btn-info">전체</a>
            <a href="/carmore/Earlycareturn?returnstats=ready" class="btn btn-info">반납신청</a>
            <a href="/carmore/Earlycareturn?returnstats=complete" class="btn btn-primary">반납완료</a>
        </div>
    </div>

    <div id="searchdiv" >
        <form id="searchform" class="form-horizontal" role="form" method="get" >
            <input type="hidden" name="mem_stats" value="<?=$data["mem_stats"]?>">
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <select class="form-control" name="sp">
                                <option value="f_reservationidx" >예약번호</option>
                                <option value="driver_name" >운전자</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="sv" value="">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </fieldset>
        </form>
        <hr/>
    </div>

    <div class="clearfix"></div>
    <form name="updateConfirm" method="post">
        <input type="hidden" name="emode" value="changestats">
        <input type="hidden" name="stats" >

        <div class="table-responsive">
            <table class="table   table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">Check</th>
                    <th class="text-center col-md-2">차량 대여정보</th>
                    <th class="text-center col-md-1">운전자</th>
                    <th class="text-center col-md-2">결제정보</th>
                    <th class="text-center col-md-1">회사정보</th>
                    <th class="text-center col-md-2">등록일</th>
                    <th class="text-center col-md-3">반환진행</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($data["list"] as $entry) {

                    ?>
                    <tr class="text-center row">
                        <td><input type="checkbox" name="usrserial[]" value="<?= $entry["f_usrserial"] ?>"></td>
                        <td class="small text-left"   >
                            <span style="font-size: medium"><strong><?= $entry["model"] ?></strong></span>
                            <br><?= $entry["brand"] ?>  /<?= $entry["carType"] ?>/<?=$entry["age_info"]?> <span class="label label-primary searchStatus" data-status=""><?= $entry["f_rentstatus"] ?></span>
                            <br><?= $entry["car_number1"] ?> <?= $entry["car_number2"] ?> <?=$entry["car_number3"]?> /  <?=$entry["oil"]?> <?if($entry["color"] !=""){?>/  <?=$entry["color"]?><?}?>

                            <hr>
                            <span class="label label-info ">예약번호</span> <?= $entry["f_reservationidx"] ?><br>
                            <span class="label label-default ">대여기간</span> <?= $entry["f_rentstartdate"] ?> ~  <?= $entry["f_rentenddate"] ?>
                            <?if($entry["f_delivpickaddr"] !=""){?>
                                <br><br> 배차주소 :<?=$entry["f_delivpickaddr"]?>
                                <br>회차주소 :<?=$entry["f_delivpickaddr"]?>
                            <?}?>

                        </td>
                        <td class="small text-left"   >
                            <span style="font-size: medium"><strong><a  style="cursor: pointer" onclick="setuserstats('etc','<?= $entry["f_usrserial"]?>')"><?= $entry["driver_name_encrypt"] ?></a></strong></span>
                            <hr><span style="font-size: medium"><strong><?= $entry["driver_phone_encrypt"] ?></strong></span>
                            <BR>생년월일 : <?= $entry["driver_birthday_encrypt"] ?>
                            <BR><?= $entry["f_driveremail"] ?>
                        </td>
                        <td class="small text-left">
                            <span style="font-size: medium"><strong><?= $entry["f_totalprice"] ?></strong> 원</span>

                            <hr>총 단가 : <?= $entry["original_price"] ?> 원
                            <br>렌트비용 : <?= $entry["f_rentprice"] ?> 원 / 보험비용 : <?= $entry["f_insuprice"] ?> 원
                            <br>배송비용 : <?= $entry["f_delivprice"] ?> 원 / 사용포인트 : <span style="color: red">- <?= $entry["f_usepoint"] ?> </span>  원
                            <br>쿠폰 : <?=$entry["coupon_title"]?> <span style="color: red">- <?= $entry["coupon_price"] ?> </span>  원
                        </td>
                        <td class="small text-left"   >
                            <span style="font-size: medium"><strong><?= $entry["name"] ?></strong></span>
                            <br><?= $entry["branchName"] ?>
                            <br>
                            <br><?= $entry["branchOwner"] ?>
                            <br><?= $entry["branchOwnerTel"] ?>
                        </td>
                        <td class="small text-center"><?= $entry["regdate"] ?></td>
                        <td class="small text-center">
                            <?if($entry["crti_early_return_apply"]=="1"){?>
                            <button type="button" class="btn btn-primary "
                                    onclick="gorentdtl('<?=$entry["crti_idx"]?>')">반환신청/수정</button>
                            <?}else if($entry["crti_early_return_apply"]=="2"){?>
                                반환완료일 : <?=$entry["crti_early_return_confirm_date"]?><br>
                                <button type="button" class="btn btn-success"
                                        onclick="gorentdtl('<?=$entry["crti_idx"]?>')">반환정보보기</button>
                            <?}?>


                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="text-center">
            <?=$data['pagination']?>
        </div>
    </form>

    <div id="returnModal" class="modal fade bs-modified-modal-sm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger ">반환처리하기</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2">
                            <p class="form-control-static text-right">반환날짜</p>
                        </div>
                        <div class="col-md-9">
                            <input type="date" id="canceldate" onchange="returncardatechange()">
                        </div>
                        <div class="col-md-1"></div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-md-2">
                            <p class="form-control-static text-right">반환금액</p>
                        </div>
                        <div class="col-md-9">
                            <p class="form-control-static" style="font-size: 1.5em;color:red"><b><span id="returnprice_str"></span></b> </p>

                            <input type="hidden" class="form-control" id="cancelrefundprice" placeholder="반환금액을 입력하세요"  value="">
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="refundsubmit"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="refundconfirmBtn">반환처리하기</span></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
                </div>
            </div>
        </div>
    </div>


    <div id="returnchangeModal" class="modal fade bs-modified-modal-sm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger ">반환신청일수정</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2">
                            <p class="form-control-static text-right">기존반환날짜</p>
                        </div>
                        <div class="col-md-9">
                            <p class="form-control-static"></p>
                        </div>
                        <div class="col-md-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <p class="form-control-static text-right">신규반환날짜</p>
                        </div>
                        <div class="col-md-9">
                            <input type="date" id="canceldate" onchange="returncardatechange()">
                        </div>
                        <div class="col-md-1"></div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-md-2">
                            <p class="form-control-static text-right">예상반환금액</p>
                        </div>
                        <div class="col-md-9">
                            <p class="form-control-static" style="font-size: 1.5em;color:red"><b><span id="returnprice_str"></span></b> </p>

                            <input type="hidden" class="form-control" id="cancelrefundprice"   value="">
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="refundsubmit"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="refundconfirmBtn">반환처리하기</span></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
            var tempidx="";
            var tempctype="";

            var calcelstartdate="";
            var calcelenddate="";
            var calceltargetprice="";
            var calcelusepoint="";
            var cancelper="";
            var useprice="";
            var cancelcrti_idx="";

            function addCommas(intNum) {
                return (intNum + '').replace(/(\d)(?=(\d{3})+$)/g, '$1,');
            }

            function gorentdtl(crti){
                location.href="/carmore/Earlycareturn?ptype=v&crti_idx="+crti;
            }


            function showreturnmodal(cancelrefundprice,cancelper,canceldate,startdate,  enddate
                , targetprice, usepoint,crti_idx){
                $("#cancelrefundprice").val(cancelrefundprice);
                $("#canceldate").val(canceldate);

                console.log("startdate:"+startdate);
                calcelstartdate=startdate;
                calcelenddate=enddate;
                calceltargetprice=targetprice;
                calcelusepoint=usepoint;
                cancelcrti_idx=crti_idx;


                var returnprice_str = addCommas(cancelrefundprice);
                $("#returnprice_str").text(returnprice_str+" 원");

                $("#returnModal").modal('show');
                returncardatechange();
            }

            $("#refundsubmit").click(function() {

                var cancelamt= $("#cancelrefundprice").val();
                var canceldate= $("#canceldate").val();
                console.log("/carmore/Careturnproc?ptype=setcareturn&crti_idx="+cancelcrti_idx+"&cancelper="+cancelper+"&useprice="+useprice+"&canceldate="+canceldate+"&cancelamt="+cancelamt);

                if(confirm('차량 조기반납 처리를 진행하시겠습니까?')){

                    $.ajax({
                        type: "get",
                        url: "/carmore/Careturnproc?ptype=setcareturn&crti_idx="+cancelcrti_idx+"&cancelper="+cancelper+"&useprice="+useprice+"&canceldate="+canceldate+"&cancelamt="+cancelamt  ,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (response) {
                            console.log("response:"+response);

                        }
                    });

                }

            });

        </script>

        </div>




</body>
</html>
