<?php
$filename =date("YmdHis");
$filename = $filename.".xls";


header( "Content-type: application/vnd.ms-excel" );
header( "Content-type: application/vnd.ms-excel; charset=utf-8");
header( "Content-Disposition: attachment; filename = ".$filename );


include ($_SERVER["DOCUMENT_ROOT"]."/application/config/aws_dbcon.php");



define("ID",$aws_db["username"],true);
define("PW",$aws_db["password"],true);
define("NAME",$aws_db["dbname"],true);
define("HOST",$aws_db["host"],true);

$conn = new PDO($aws_db["dns"], $aws_db["username"], $aws_db["password"]);
$conn->exec("set names utf8");

$callboardcate_idx = $this->input->get('callboardcate_idx', TRUE);
if(!$callboardcate_idx)$callboardcate_idx="";

$sql="select * ,(select callboardcate_name  from admin_callboardcate where callboardcate_idx=a.callboardcate_idx) as callboardcate_name
            from   admin_callboard a  where stats='y' ";
if($callboardcate_idx !=""){
    $sql .= " and callboardcate_idx='$callboardcate_idx'";
}

$calquery = $conn -> prepare($sql);
$calquery -> execute();


// 테이블 상단 만들기

$EXCEL_STR = "
<table border='1'>
<tr>
<td > 상담날짜 </td>
<td > 상담자 </td>
<td > 카테고리/구분 </td>
<td  > 고객</td>
<td >연락처 </td>
<td > 예약번호</td>
<td >고객문의 </td>
<td  >운영자답변</td>
<td  >전달사항</td>
</tr>";

while( $item=$calquery -> fetch() ) {

    $call_idx = $item["call_idx"];
    $mem_name = $item["mem_name"];
    $call_name = $item["call_name"];
    $call_number = $item["call_number"];
    $call_reservnum = $item["call_reservnum"];
    $call_date = $item["call_date"];
    $call_time = $item["call_time"];
    $call_part = $item["call_part"];
    $call_cate = $item["call_cate"];
    $call_ques = $item["call_ques"];
    $call_response = $item["call_response"];
    $call_etc = $item["call_etc"];
    $callboardcate_name = $item["callboardcate_name"];

    $EXCEL_STR .= "
   <tr>
    <td > $call_date $call_time </td>
    <td > $mem_name </td>
    <td > $callboardcate_name / $call_part </td>
    <td  > $call_name </td>
    <td > $call_number </td>
    <td > $call_reservnum</td>
    <td > $call_ques </td>
    <td  >$call_response</td>
    <td  >$call_etc</td>
   </tr>
   ";
}

$EXCEL_STR .= "</table>";

echo "<meta content=\"application/vnd.ms-excel; charset=UTF-8\" name=\"Content-type\"> ";
echo $EXCEL_STR;

?>