<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <form id="updateConfirm">
        <input type="hidden" name="isConfirm" value="" form="updateConfirm"/>
        <input type="hidden" name="emode" value="agree_yn" form="updateConfirm"/>

        <div class="page-header clearfix">
            <h2 class="pull-left">포인트 내역 관리</h2>

        </div>

        <div id="searchdiv" >
            <form id="searchform" class="form-horizontal" role="form" method="get" >
                <input type="hidden" name="mem_stats" value="<?=$data["mem_stats"]?>">
                <fieldset>
                    <div class="clearfix">
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <select class="form-control" name="sp">
                                    <option value="kakaonickname" >카카오닉네임</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="sv" value="">
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </fieldset>
            </form>
            <hr/>
        </div>

        <div class="clearfix"></div>
        <form name="updateConfirm" method="post">
            <input type="hidden" name="emode" value="changecashstats">
            <input type="hidden" name="stats" >
            <input type="hidden" name="coinbuyoffer_idx" >
            <input type="hidden" name="targetprice" >
            <input type="hidden" name="chargefee" >
            <input type="hidden" name="buy_nowprice" >
            <input type="hidden" name="mem_id" >

            <div class="table-responsive">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr class="info row">
                        <th class="text-center col-md-1">No</th>
                        <th class="text-center col-md-3">포인트 정보</th>
                        <th class="text-center col-md-1">닉네임</th>
                        <th class="text-center col-md-2">포인트 </th>
                        <th class="text-center col-md-1">상태 </th>
                        <th class="text-center col-md-2">등록자</th>
                        <th class="text-center col-md-2">등록일</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $startnum =$data["startnum"];
                    foreach($data["list"] as $entry) {
                        ?>
                        <tr class="text-center row">
                            <td class="small" ><?=$startnum?></td>
                            <td class="small text-left" > <b><?= $entry["pointmsg"] ?></b> /<?=$entry["logmsg"]?>  </td>
                            <td class="small" ><?=$entry["kakaonickname"]?>  </td>
                            <td class="small"><?= $entry["valpointstr"]?> p
                                <?if($entry["pointtype"]=="4"){?>
                                <span class="label label-danger " style="cursor: pointer"  onclick="setpointstats('back','<?= $entry["pointsrl"]?>','<?= $entry["usrserial"]?>','<?= $entry["valpoint"]?>')">회수</span>
                                <?}?>
                            </td>
                            <td class="small">
                                <span style="color: <?=$entry["pointtypecolor"]?>"> <?= $entry["pointtypestr"] ?></span>
                            </td>
                            <td class="small"><?= $entry["admin_name"] ?> </td>
                            <td class="small"><?= $entry["regdate"] ?></td>
                        </tr>
                        <?php
                        $startnum--;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="text-center">
                <?=$data['pagination']?>
            </div>
        </form>

        <!--* modal start *-->
        <div id="myModal" class="modal fade bs-modified-modal-sm">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header ">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-danger modalTitle"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="form-control-static text-right">코드선택</p>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" id="pointtype">
                                    <option value="11" >운영자 회수</option>
                                    <option value="10" >회원 탈퇴 회수</option>

                                </select>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <p class="form-control-static text-right">관련 사유</p>
                            </div>
                            <div class="col-md-9">
                                <textarea   class="form-control" id="message" placeholder="관련사유를 입력하세요"  /></textarea>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="goSubmit"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="confirmBtn"></span></button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
                    </div>
                </div>
            </div>
        </div>


        <script type="text/javascript">
            var tempuidx="";
            var temppidx="";
            var tempctype="";
            var valpoint="";

            function setpointstats(ctype,pidx,uidx,point){
                temppidx=pidx;
                tempuidx=uidx;
                tempctype=ctype;
                valpoint=point;

                if(ctype==="back") {
                    $("#confirmBtn").text("회수 하기");
                    $(".modalTitle").text("포인트 회수 처리");
                }

                $("#myModal").modal().on("hidden.bs.modal", function() {
                    $("#confirmBtn").text("");
                    $(".modalTitle").text("");
                    tempctype="";
                    tempfidx="";
                    tempuidx="";
                    valpoint="";
                });
            }


            $("#goSubmit").click(function() {
                var message = $("#message").val();
                var pointtype=$("#pointtype").val();
                var board_title="";
                var board_part="";
                var board_part="point";

                board_title="포인트 회수 처리";
                confirmtext="포인트를 회수 하시겠습니까?";

                if (message =="") {
                    alert("상담내용을 기입해주세요.");
                    return false;
                }else {

                    if(!confirm(confirmtext)){
                        return;
                    }

                    $.ajax({
                        type: "get",
                        url: "/carmore/CarmoreBoardProc?emode=setcomplainajax&usrserial="+tempuidx+"&board_part="+board_part +
                        "&workmode="+tempctype+"&message=" + message+"&board_title="+board_title+"&pointsrl="+temppidx
                        +"&pointtype="+pointtype+"&valpoint="+valpoint ,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (response) {
                            console.log("response:"+response);
                            $('#myModal').modal('hide');
                            $("#message").val("");
                            tempctype="";
                            tempfidx="";
                            tempuidx="";
                            valpoint="";
                            location.reload();
                        }
                    });

                }

            });



        </script>
</div>

</body>
</html>
