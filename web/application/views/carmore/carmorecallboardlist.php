<script language="JavaScript">
    $(document).ready(function(){
        //최상단 체크박스 클릭
        $("#checkall").click(function(){
            //클릭되었으면
            if($("#checkall").prop("checked")){
                //input태그의 name이 chk인 태그들을 찾아서 checked옵션을 true로 정의
                $("input[name=board_idx]").prop("checked",true);
                //클릭이 안되있으면
            }else{
                //input태그의 name이 chk인 태그들을 찾아서 checked옵션을 false로 정의
                $("input[name=board_idx]").prop("checked",false);
            }
        })
    })

    function sendcate(){
        var callboardcate_idx =$("#callboardcate_idx").val();
        if(callboardcate_idx !==""){
            document.cateaf.action="/carmore/CarmoreCallMng";
            document.cateaf.submit();
        }
    }

    function exceldown(){
        location.href="/application/views/carmore/excel_callboard.php?callboardcate_idx=<?=$data["callboardcate_idx"]?>";
    }

    function set_statscomplete(){


        var objboard_idx =  $('[name="board_idx"]');
        var objlength =objboard_idx.length;
        var saveparam=new Array();
        var cnt=0;
        for(var i=0;i < objlength ;i++){
            var obj =objboard_idx[i];
            if(obj.checked){
                var board_idx =objboard_idx[i]["value"];

                if(board_idx !=="" ){
                    saveparam.push(board_idx);
                    cnt++;
                }
            }

        }

        if(cnt===0){
            alert('선택된 고객상담 정보가 없습니다.');
            return;
        }
        saveparam = saveparam.join(",");

        $.ajax({
            type:"get",contentType: "application/json",
            url:"/carmore/CarmoreBoardProc?emode=setallstats&saveparam="+saveparam,
            datatype: "json",
            success: function(data) {
                alert("수정이 완료되었습니다.");
                location.reload();

            },
            error: function(x, o, e) {

            }
        });
    }


    function delinfo(call_idx)
    {
        if ( confirm("상담내역을 삭제 하시겠습니까?")) {
            location.href="/carmore/CarmoreCallMng?ptype=del&emode=del&call_idx="+call_idx ;
        }

    }
</script>
<div class="container-fluid" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>

    <div class="page-header clearfix">
        <h2 class="pull-left">고객 전화 내역</h2>
        <div class="pull-right"  >
            <a href="?ptype=w" class="btn btn-info">등록하기</a>
        </div>
    </div>
    <div id="searchdiv">
        <form id="searchCompanyNotice" class="form-horizontal" role="form" method="get" >
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-2"></div>

                        <div class="col-md-4">
                            <input type="text" class="form-control" name="sv" value="">
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
    <div class="pull-left">
        <button type="button" onclick="exceldown()" class="btn btn-primary btn-sm">액셀다운로드</button>
    </div>
    <div class=" pull-right">
        <form name="cateaf" method="get">
        <div class="col-md-7">
            <select name="callboardcate_idx" id="callboardcate_idx" style="width: 150px" class="form-control">
                <option value="">==선택==</option>
                <?php
                echo $data["cate"];
                ?>

            </select>
        </div>
        <div class="col-md-1">
        <button type="button" onclick="sendcate()" class="btn btn-primary btn-sm">바로가기</button>
        </div>
        </form>
    </div>
    <div class="clearfix" style="padding-bottom: 10px"></div>
    <form id="updateConfirm">

        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">상담날짜</th>
                    <th class="text-center col-md-1">상담자</th>
                    <th class="text-center col-md-1">카테고리/구분</th>
                    <th class="text-center col-md-1">고객</th>
                    <th class="text-center col-md-1">연락처</th>
                    <th class="text-center col-md-1">예약번호</th>
                    <th class="text-center col-md-2">고객문의</th>
                    <th class="text-center col-md-2">운영자답변</th>
                    <th class="text-center col-md-2">전달사항</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($data["list"] as $entry) {
                    ?>
                    <tr class="row">

                        <td   class="small text-center"  ><?=$entry["call_date"]?> <?=$entry["call_time"]?> </td>
                        <td   class="small text-center"  ><a href="/carmore/CarmoreCallMng?ptype=w&call_idx=<?=$entry["call_idx"]?>"><?=$entry["mem_name"]?></a></td>
                        <td   class="small text-center"  ><?=$entry["callboardcate_name"]?> / <?=$entry["call_part"]?></td>
                        <td   class="small text-center"  ><?=$entry["call_name"]?> </td>
                        <td   class="small text-center"  ><?=$entry["call_number"]?></td>
                        <td   class="small text-center"  ><?=$entry["call_reservnum"]?></td>
                        <td   class="small text-center"  ><?=$entry["call_ques"]?></td>
                        <td   class="small text-center"  ><?=$entry["call_response"]?></td>
                        <td   class="small text-center"  ><?=$entry["call_etc"]?> <button type="button" onclick="delinfo('<?=$entry["call_idx"]?>')" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span></button></td>

                    </tr>
                <?}?>

                </tbody>
            </table>
        </div>
    </form>


    <div class="text-center">
        <?=$data['pagination']?>
    </div>
