<style>
    .dz-image img {
        width: 100%;
        height: 100%;
    }
</style>

<?php
$stroller_idx   = (int)$data['csIdx'];
$is_it_new      = $stroller_idx === -1;    # true : 신규 업체
$stroller_obj   = ($is_it_new)? [] : $data['info'];
$matching_inven = ($is_it_new)? [] : $stroller_obj->matchingInventory;
?>

<div class="container workspace-basic-container col-md-12">
    <div class="page-header clearfix">
        <h3 class="pull-left">유모차 업체 설정</h3>
    </div>

    <div class="clearfix"></div>

    <!--지점 정보 섹션-->
    <div class="row col-md-offset-1 col-md-10" style="margin-bottom: 10em">

        <div class="row col-md-12">
<?php
$name = ($is_it_new)? "" : $stroller_obj->name;
?>
            <div class="form-group col-md-3">
                <label>업체명</label>
                <div class="col-md-12">
                    <input type="text" class="form-control st-change-detect-element" id="st_name" value="<?=$name?>" placeholder="가나다 유모차" data-obj-key="name"/>
                </div>
            </div>

<?php
$type = ($is_it_new)? "" : (int)$stroller_obj->type;

?>
            <div class="form-group col-md-3">
                <label>업체 타입</label>
                <select class="workspace-select st-change-detect-element" id="st_type" data-obj-key="type">
                    <option value="" <?=($type === "")? "selected": "" ?>></option>
                    <option value="1" <?=($type === 1)? "selected": "" ?>>유모차 업체</option>
                    <option value="2" <?=($type === 2)? "selected": "" ?>>렌트카 자체운용</option>
                </select>
            </div>

<?php
$plus_id = ($is_it_new)? "" : $stroller_obj->plusId;

?>
            <div class="form-group col-md-3">
                <label>카카오 플친 아이디</label>
                <div class="col-md-12">
                    <input type="text" class="form-control st-change-detect-element" value="<?=$plus_id?>" placeholder="가나다유모차" data-obj-key="plusId"/>
                </div>
            </div>

<?php
$plus_url_id = ($is_it_new)? "" : $stroller_obj->plusUrlId;

?>
            <div class="form-group col-md-3">
                <label>카카오 플친 주소 아이디&nbsp;<span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="top" title="'pf.kakao.com/_pMJjj' 중 _pMJjj"></span></label>
                <div class="col-md-12">
                    <input type="text" class="form-control st-change-detect-element" value="<?=$plus_url_id?>" placeholder="_pMJjj" data-obj-key="plusUrlId"/>
                </div>
            </div>
        </div>

        <div class="row col-md-12">

<?php
$address = ($is_it_new)? "" : $stroller_obj->address;

?>
            <div class="form-group col-md-8">
                <label>업체주소</label>
                <div class="col-md-12" style="display: flex">
                    <input type="text" class="form-control st-change-detect-element" value="<?=$address?>" data-obj-key="address"/>
                </div>
            </div>

<?php
$tel = ($is_it_new)? "" : $stroller_obj->contact;

?>

            <div class="form-group col-md-4">
                <label>비상연락처</label>
                <div class="col-md-12">
                    <input type="tel" class="form-control st-change-detect-element" value="<?=$tel?>" placeholder="01012345678" data-obj-key="tel"/>
                </div>
            </div>
        </div>

        <div class="row col-md-12">
<?php
$logo_info         = ($is_it_new)? [] : $stroller_obj->logoInfo;
$src               = ($is_it_new)? "" : $logo_info->logo;
$size              = ($is_it_new)? 0 : (int)$logo_info->size;

?>
            <div class="form-group col-md-6">
                <label>로고</label>
                <div class="dropzone" id="stinfo_dropzone_div">
                    <div class="dz-default dz-message" data-dz-message>
                        <span id="stinfo_dropzone_default_msg" style="<?=$default_display?>">여기에 드래그 앤 드랍하세요</span>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-6">
                <label>해쉬태그 <small>("#"을 제외하고 입력해주세요)</small></label>
<?php
$hash_tag_arr = ($is_it_new)? [] : $stroller_obj->hashTag;
$hash_tag_value = [
    'tag1' => '',
    'tag2' => '',
    'tag3' => '',
    'tag4' => '',
    'tag5' => ''
];

foreach ($hash_tag_arr as $obj)
{
    $hash_tag_value['tag'.$obj['key']] = $obj['value'];
}

?>
                <div>
                    <input type="text" class="form-control st-hash-tag-input st-change-detect-element" value="<?=$hash_tag_value['tag1']?>" style="margin-bottom: .5em" placeholder="해쉬태그_1" data-obj-key="hashTag1"/>
                    <input type="text" class="form-control st-hash-tag-input st-change-detect-element" value="<?=$hash_tag_value['tag2']?>" style="margin-bottom: .5em" placeholder="해쉬태그_2" data-obj-key="hashTag2"/>
                    <input type="text" class="form-control st-hash-tag-input st-change-detect-element" value="<?=$hash_tag_value['tag3']?>" style="margin-bottom: .5em" placeholder="해쉬태그_3" data-obj-key="hashTag3"/>
                    <input type="text" class="form-control st-hash-tag-input st-change-detect-element" value="<?=$hash_tag_value['tag4']?>" style="margin-bottom: .5em" placeholder="해쉬태그_4" data-obj-key="hashTag4"/>
                    <input type="text" class="form-control st-hash-tag-input st-change-detect-element" value="<?=$hash_tag_value['tag5']?>" placeholder="해쉬태그_5" data-obj-key="hashTag5"/>
                </div>
            </div>
        </div>

        <div class="row col-md-12">
            <div class="form-group col-md-12">
                <label>렌트카 업체 매칭</label>
                <div class="col-md-12">
                    <div class="col-md-12" style="margin-bottom:.5em">
                        <button type="button" class="btn btn-sm btn-info pull-right" id="stinfo_affiliate_matching_btn" onclick="callStinfoRegAffiliate()">신규추가</button>
                    </div>
                    <div class="col-md-12" id="stinfo_affiliate_matching_div">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="info">
                                    <th class="col-md-11">업체명</th>
                                    <th class="col-md-1"></th>
                                </tr>
                            </thead>
                            <tbody class="text-center" id="stinfo_affiliate_matching_tbody"></tbody>
                        </table>
                    </div>
                    <div class="col-md-12 text-center" id="stinfo_affiliate_matching_empty_div" style="padding: 3em 0">
                        매칭된 렌트카 업체가 없습니다.
                    </div>
                </div>
            </div>

        </div>

        <div class="row col-md-12">
<?php
$memo = ($is_it_new)? "" : $stroller_obj->memo;

?>
            <div class="form-group col-md-12">
                <label>메모</label>
                <textarea class="form-control st-change-detect-element" rows="5" data-obj-key="memo" style="resize: none"><?=$memo?></textarea>
            </div>
        </div>

        <div class="row col-md-12">
            <div class="form-group col-md-offset-8 col-md-4 text-right">
                <button type="button" class="btn btn-lg btn-default" onclick="location.href='/carmore/StrollerInventory'">목록</button>
<?php
if (!$is_it_new)
{

?>
                <button type="button" class="btn btn-lg btn-danger" onclick="deleteStinfo()">삭제</button>
<?php
}

?>
                <button type="button" class="btn btn-lg btn-primary" onclick="saveStinfo()">저장</button>
            </div>
        </div>

        <div id="stinfo_search_affiliate_modal_container"></div>

    </div>
</div>

<script>
    var mStinfoAssetUrl            = '<?=asset_url()?>';
    var mStinfoIsItNew             = Boolean(<?=$is_it_new?>);
    var mStinfoIdx                 = Number('<?=$stroller_idx?>');
    var mStinfoOrigSaveName        = '<?=$src?>';
    var mStinfoOrigSaveSize        = Number(<?=$size?>);
    var mStinfoMatchingServerInven = JSON.parse('<?=json_encode($matching_inven)?>');
</script>

<script src="<?=base_url()?>static/lib/toastr/toastr.min.js"></script>
<script src="<?=asset_url()?>js/image-compressor.min.js"></script>
<script src="<?=asset_url()?>js/commonCompressImg.min.js"></script>
<script src="<?=base_url()?>static/lib/dropzone/dropzone.js"></script>
<script src="<?=asset_url()?>js/common_workspace.min.js?ver=190529"></script>
<script src="<?=asset_url()?>modal/js/common_modal.min.js"></script>
<script src="<?=asset_url()?>js/stroller_information.min.js?ver=190529"></script>
<script src="<?=asset_url()?>modal/js/search_affiliate_modal.min.js?ver=190529"></script>