
<Script language="JavaScript">

    $(function () {
        returncardatechange('<?=$data["startdate"]?>')
    });

    function addCommas(intNum) {
        return (intNum + '').replace(/(\d)(?=(\d{3})+$)/g, '$1,');
    }

    function dateparse(str) {
        if(!/^(\d){8}$/.test(str)) return "invalid date";
        var y = str.substr(0,4),
            m = str.substr(4,2),
            d = str.substr(6,2);
        m =m-1;
        return new Date(y,m,d);
    }

    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    }

    function returncardatechange(calcelstartdate){
        var canceldate =$("#tmpcanceldate").val();
        var f_reservationidx="<?=$data["f_reservationidx"]?>";

        var startdate = Date.parse(calcelstartdate);
        var canceldate = Date.parse(canceldate);

        console.log("startdate:"+startdate+", canceldate:"+canceldate);

        // 취소날짜 > 시작일 : 불가

        if(canceldate > startdate){
            alert('취소날짜 오류입니다.시작일 전에 취소되어야 합니다.');

            return ;
        }

        // canceldate = unixtime 전송
        // http://workspace2.teamo2.kr/carmore/ReservationManage?ptype=getcancelprice&canceldate=1543420740000&f_reservationidx=12862&jsonyn=y
        console.log("/carmore/ReservationManage?ptype=getcancelprice&canceldate="+canceldate+"&f_reservationidx="+f_reservationidx+"&jsonyn=y");
        $.ajax({
            type: "get",
            url:"/carmore/ReservationManage?ptype=getcancelprice&canceldate="+canceldate+"&f_reservationidx="+f_reservationidx+"&jsonyn=y" ,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                console.log("response:"+response);

                var json = JSON.parse(response);
                // returnprice returnper  returnpoint cardcancelprice
                var returnprice = json["returnprice"];
                var returnper = json["returnper"];
                var returnpoint = json["returnpoint"];
                var cardcancelprice = json["cardcancelprice"];

                $("#returnprice").val(returnprice);
                $("#returnper").val(returnper);
                $("#returnpoint").val(returnpoint);
                $("#cardcancelprice").val(cardcancelprice);


                $("#returnpricestr").text( addCommas(returnprice));
                $("#returnpointstr").text( addCommas(returnpoint));
                $("#returnperstr").text( returnper);
                $("#tmpcardcancelprice").val(cardcancelprice);

            }
        });
    }



    function cancelreservation() {
        var docuform=document.cancelfrm;
        var canceldate=$("#tmpcanceldate").val();
        var cardcancelprice=$("#tmpcardcancelprice").val();

        if (cardcancelprice === "") {
            alert('환불금액을 기입해주세요');
            $("#tmpcardcancelprice").focus();
            return;
        }

        $("#cardcancelprice").val(cardcancelprice);

        if (canceldate === "") {
            alert('반환날짜를 기입해주세요');
            $("#canceldate").focus();
            return;
        }


        if (confirm('예약취소 처리를 진행하시겠습니까?')) {

            docuform.canceldate.value= canceldate;
            docuform.action="/carmore/ReservationcancelProc";
            docuform.submit();

        }

    }


    function sendcomment(){
        var form=document.af;
        if(form.commenttext.value===""){
            alert('코멘트를 기입해주세요');
            form.commenttext.focus();
            return;
        }
        form.datamode.value="new";
        form.submit()
    }


    function delcomment(comment_idx){

        if(confirm('코멘트를 삭제하시겠습니까?')){
            var form=document.af;
            form.comment_idx.value=comment_idx;
            form.datamode.value="del";
            form.submit()
        }
    }



</Script>
<style>
    @media (min-width:100px) {
        .txt-xs {
            font-size: 0.1em;
        }
        .txt-s {
            font-size: 0.3em;
        }
        .txt-m {
            font-size: 0.5em;
        }
        .txt-xxl {
            font-size: 1.6em;
        }
        .txt-bold {
            font-weight: 600;
        }
        .table-title {
            width: 30%;
        }
        .table-contents {
            width: 70%;
        }
        .contents-root {
            margin-left: 10px;
            margin-right: 10px;
        }
        .reservation-cancel-root {
            margin-top: 30px;
            margin-bottom: 30px;
        }
    }

    @media (min-width:767px) {
        .txt-xs {
            font-size: 0.5em;
        }
        .txt-s {
            font-size: 0.7em;
        }
        .txt-m {
            font-size: 0.9em;
        }
        .txt-xxl {
            font-size: 2em;
        }
        .txt-bold {
            font-weight: 600;
        }
        .table-title {
            width: 25%;
        }
        .table-contents {
            width: 75%;
        }
        .contents-root {
            margin-left: 30px;
            margin-right: 30px;
        }
        .reservation-cancel-root {
            margin-top: 70px;
            margin-bottom: 30px;
        }
    }

    .table-title {
        padding-right: 10px;
        padding-left: 10px;
        height: 30px;
        text-align: left;
        background-color: #f2f2f2;
        border-bottom: 1px solid #BDBDBD;
    }

    .table-contents {
        padding-right: 10px;
        padding-left: 10px;
        height: 30px;
        border-bottom: 1px solid #BDBDBD;
    }

    .reservation-cancel-root {
        background-color: #f2f2f2;
        border: 1px solid #d9d9d9;
        padding: 15px;
        color: #595959;
    }

</style>
<div class="container-fluid" style="padding-top: 20px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }


    </style>


    <div class="page-header clearfix">
        <form id="searchCompanyNotice" class="form-horizontal" role="form" method="get" >
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-4">
                            <h3>예약 취소 처리</h3>
                        </div>
                        <div class="pull-right" style="padding-right: 30px">
                            <button type="button" onclick="location.href='/carmore/ReservationManage'" class="btn btn-primary btn-block">목록가기 </button>

                        </div>

                    </div>
                </div>
            </fieldset>
        </form>

    </div>

    <div class="row">

        <div class="col-md-8" style="overflow-x: hidden; overflow-y: scroll;height: 700px ">
            <div class="clearfix"></div>

            <div style="width: 100%; max-width:780px; margin:auto;">
                <div style="border: 1px solid #d9d9d9;width: 100%;">

                    <!--header-->

                    <div class="contents-root">
                        <div style="margin-top: 30px;font-size: 20px;">
                            <img src="https://carmore.kr/app_v3/php/images/emailimg/appointment.png" style="">
                            <table style="border-spacing: 0;border-top: 3px solid black;padding: 0;margin: 0;width: 100%; text-align: right;">
                                <tr>
                                    <td class="table-title txt-m">회사정보</td>
                                    <td class="table-contents txt-m"><?=$data["company"]?></td>
                                </tr>
                                <tr>
                                    <td class="table-title txt-m">예약번호</td>
                                    <td class="table-contents txt-m"><?=$data["residx"]?></td>
                                </tr>
                                <tr>
                                    <td class="table-title txt-m">예약기간</td>
                                    <td class="table-contents txt-m"><?=$data["startdate"]?> ~ <?=$data["enddate"]?></td>
                                </tr>
                                <tr>
                                    <td class="table-title txt-m">예약차량</td>
                                    <td class="table-contents txt-m"><?=$data["carmodel"]?></td>
                                </tr>
                                <tr>
                                    <td class="table-title txt-m">운전자</td>
                                    <td class="table-contents txt-m"><?=$data["driver"]?></td>
                                </tr>
                                <tr>
                                    <td class="table-title txt-m">배차</td>
                                    <td class="table-contents txt-m"><?=$data["pickupaddr"]?></td>
                                </tr>
                                <tr>
                                    <td class="table-title txt-m">회차</td>
                                    <td class="table-contents txt-m"><?=$data["f_delivreturnaddr"]?></td>
                                </tr>
                            </table>
                        </div>

                        <div style="margin-top: 30px;  font-size: 20px;">
                            <img src="https://carmore.kr/app_v3/php/images/emailimg/payment.png" style="">
                            <table style="border-spacing: 0;  border-top: 3px solid black;  padding: 0;  margin: 0; width: 100%;  text-align: right;">
                                <tr>
                                    <td class="table-title txt-m">총 결제금액</td>
                                    <td class="table-contents txt-m" style="padding-bottom:5px;"><?=number_format($data["crti_principal"])?> 원
                                        <br>
                                        <div class="txt-xs">(카드결제 <?=$data["dbpayment"]?> 원 + 쿠폰 <?=$data["use_coupon"]?> 원 +포인트 <?=$data["use_point"]?>원)</div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="table-title txt-m">취소/환불 수단</td>
                                    <td class="table-contents txt-m">신용카드</td>
                                </tr>
                            </table>
                        </div>


                        <div style="margin-top: 30px;  font-size: 20px;">
                            <h3>취소/환불 정보</h3>
                            * 먼저 포인트에서 차감한 이후, 카드취소액을 설정한다. 쿠폰은 무조건 반환
                            <table style="border-spacing: 0;  border-top: 3px solid black;  padding: 0;  margin: 0; width: 100%;  text-align: right;">
                                <tr>
                                    <td class="table-title txt-m">취소 신청일</td>
                                    <td class="table-contents txt-m">
                                        <div class="row" style="padding-top: 15px;padding-bottom: 15px">
                                            <div class="col-md-3">

                                            <input type="datetime-local" id="tmpcanceldate" value="<?=$data["canceldate"]?>"
                                                   onchange="returncardatechange('<?=$data["startdate"]?>')">

                                            </div>
                                        </div>
                                    </td>
                                </tr>


                                <tr>
                                    <td class="table-title txt-m">총 환불 금액</td>
                                    <td class="table-contents txt-m" style="color:red;" align="left">
                                        <span id="returnpricestr"> 0</span>   원 ( <span id="returnperstr">0</span>%)
                                    </td>
                                </tr>


                                <tr>
                                    <td class="table-title txt-m">포인트 차감</td>
                                    <td class="table-contents txt-m" style="color:red;" align="left">
                                        <span id="returnpointstr"> 0</span>   원
                                    </td>
                                </tr>



                                <tr>
                                    <td class="table-title txt-m" style="color:red;">카드취소금액</td>
                                    <td class="table-contents txt-m"   align="left">

                                            <div class="row" style="padding-top: 15px;padding-bottom: 15px">
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" name="tmpcardcancelprice" id="tmpcardcancelprice"  >
                                                </div>
                                                <div class="col-md-1">
                                                    원
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" onclick="cancelreservation()"  class="btn btn-danger btn">예약취소하기</button>
                                                </div>
                                            </div>

                                    </td>
                                </tr>


                            </table>
                        </div>


                    </div>
                </div>
            </div>

        </div>



        <form name="cancelfrm" method="post" >



            <input type="hidden" name="emode" value="setCancelreservation">
            <input type="hidden" name="crti_idx" value="<?=$data["crti_idx"]?>">
            <input type="hidden" name="f_reservationidx" value="<?=$data["f_reservationidx"]?>">
            <input type="hidden" name="f_bookingid" id="f_bookingid" value="<?=$data["f_bookingid"]?>">
            <input type="hidden" name="f_usrserial" value="<?=$data["f_usrserial"]?>">
            <input type="hidden" name="returnper" id="returnper">
            <input type="hidden" name="returnpoint" id="returnpoint">
            <input type="hidden" name="returnprice" id="returnprice">
            <input type="hidden" name="cardcancelprice" id="cardcancelprice">
            <input type="hidden" name="canceldate" id="canceldate">


        </form>

        <div class="col-md-4" style="overflow-x: hidden; overflow-y: scroll;height: 700px ">
            <div class="row" style="padding-top: 15px">
                <form name="af" method="post" action="/carmore/Careturnproc">
                    <input name="emode" type="hidden" value="commentproc">
                    <input name="datamode" type="hidden">
                    <input name="crti_idx" type="hidden" value="<?=$data["crti_idx"]?>">
                    <input name="comment_idx" type="hidden"  >
                    <input name="repage" type="hidden" value="ReservationManage">
                    <input name="f_reservationidx" type="hidden" value="<?=$data["f_reservationidx"]?>">


                    <div class="col-md-8">
                        <textarea   class="form-control"  name="commenttext" placeholder="관리자 코멘트를 남겨주세요"  style="width: 100%;height: 80px"  ><?=$data["carinfomst_memo"]?></textarea>
                    </div>
                    <div class="col-md-2">
                        <button type="button" onclick="sendcomment()" class="btn btn-primary btn-block">저장</button>
                    </div>
                </form>
            </div>
            <br>
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-10">코멘트</th>
                    <th class="text-center col-md-2">삭제</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $now_id = $data["now_id"];
                foreach($data["commentlist"] as $entry) {
                    $regdate = date("m/d H:i",strtotime($entry["regdate"]));
                    $writer_id = $entry["mem_id"];
                    $writer_name = $entry["writer_name"];

                    ?>
                    <tr class="row">

                        <td   class="small text-left"  >
                            <span style="font-size: 1.4em"><?=$entry["commenttext"]?></span><Br>
                            <span style="font-size: 0.8em"><?=$writer_name?> (<?=$regdate?>)</span>
                        </td>

                        <td  class="small text-center">
                            <?if($writer_id==$now_id){?>
                                <button type="button" class="btn btn-danger btn-sm" onclick="delcomment('<?=$entry["comment_idx"]?>')">
                                    <span class="glyphicon glyphicon-minus"></span> 삭제
                                </button>
                            <?}?>
                        </td>
                    </tr>
                <?}?>

                </tbody>
            </table>

        </div>


    </div>


