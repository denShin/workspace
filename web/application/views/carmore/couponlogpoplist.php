

        <form name="updateConfirm" method="post">
            <input type="hidden" name="emode" value="changecashstats">
            <input type="hidden" name="stats" >
            <input type="hidden" name="coinbuyoffer_idx" >
            <input type="hidden" name="targetprice" >
            <input type="hidden" name="chargefee" >
            <input type="hidden" name="buy_nowprice" >
            <input type="hidden" name="mem_id" >

            <div class="table-responsive">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr class="info row">
                        <th class="text-center col-md-1">No</th>
                        <th class="text-center col-md-4">쿠폰정보</th>
                        <th class="text-center col-md-2">카카오</th>
                        <th class="text-center col-md-3">쿠폰상태</th>
                        <th class="text-center col-md-2">등록일</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $startnum =$data["startnum"];
                    foreach($data["list"] as $entry) {
                        $coupon_status = (int)$entry['couponStatus'];
                        $coupon_idx    = (int)$entry['couponIdx'];
                        $use_btn       = ($coupon_status === 0)? "&nbsp;<button type='button' class='btn btn-xs btn-danger clpoplist-use-btn' data-coupon-idx='$coupon_idx'>사용처리</button>" : "";
                        ?>
                        <tr class="text-center row">
                            <td class="small" ><?=$startnum?></td>
                            <td class="small text-left" > <?= $entry["coupontitle"] ?> <?=$entry["couponprice"]?> 원</td>
                            <td class="small" ><?=$entry["kakaonickname"]?>  </td>
                            <td class="small clpoplist-btn-td"><?= $entry["cstatustr"]?><?=$use_btn?></td>
                            <td class="small"><?= $entry["regdate"] ?></td>
                        </tr>
                        <?php
                        $startnum--;
                    }
                    ?>
                    </tbody>
                </table>
            </div>

            <script>
                $(document).on('click', '.clpoplist-use-btn', function() {
                    var couponIdx = this.dataset.couponIdx;
                    var row = $(this).parents('.row');

                    if (confirm("해당 쿠폰을 사용처리하시겠습니까?")) {
                        $.ajax({
                            type: 'GET',
                            url: '/carmore/CouponManage?ptype=uc&couponIdx='+couponIdx,
                            dataType: 'json',
                            success: function(_res) {
                                if (_res.result === 1) {
                                    row.find('.clpoplist-btn-td').text("사용");
                                    alert('사용처리 완료');
                                }
                            }
                        });


                    }

                });
            </script>