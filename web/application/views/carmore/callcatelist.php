

<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>

    <div class="page-header clearfix">
        <h2 class="pull-left">고객전화 카테고리 리스트</h2>
        <div class="pull-right" style="padding-top: 30px">
            <a href="?ptype=w" class="btn btn-info">등록하기</a>
        </div>
    </div>



    <div class="table-responsive">
        <table class="table table-striped table-hover table-bordered"  >
            <thead>
            <tr class="info row">
                <th class="text-center col-md-3">No</th>
                <th class="text-center col-md-7">카테고리명</th>
                <th class="text-center col-md-2">수정/삭제</th>
            </tr>
            </thead>
            <tbody id="catelist">
            <?php

            foreach($data["list"] as $entry) {
                ?>

                <tr class="text-center row"  id="<?=$entry->callboardcate_idx?>">
                    <td class="col-md-3"><?=$entry->callboardcate_idx?></td>
                    <td class="col-md-7"><?=$entry->callboardcate_name?> </td>
                    <td class="col-md-2"><button type="button" onclick="location.href='?ptype=w&callboardcate_idx=<?=$entry->callboardcate_idx?>&emode=edit'" class="btn btn-success  btn-xs">수정</button>
                        <button type="button" onclick="deletecate('<?=$entry->callboardcate_idx?>')" class="btn btn-danger  btn-xs">삭제</button></td>
                </tr>
                <?
            }
            ?>

            </tbody>
        </table>
    </div>

    <!-- Bootstrap & Core Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>

    <script language="JavaScript">

        $('#catelist').sortable({
            update: function( ) {
                var  order= $("tbody").sortable("toArray");
                console.log("order:"+order);
                var postdata ={'ptype':'setCallCateOrder' ,'order':order   }

                $.ajax({
                    url: "/carmore/CarmoreCallCate",type: "post", data:postdata,
                    success: function (response) {
                       console.log("response:%o",response);
                        alert('정렬순서 변경이 완료되었습니다.');
                       
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        //console.log("error");
                    },
                    complete:  function() {
                        //console.log("complete");

                    }
                });
            }
        });

        function deletecate(callboardcate_idx)
        {
            if ( confirm("카테고리를 삭제 하시겠습니까?")) {
                location.href="/carmore/CarmoreCallCate?ptype=del&emode=del&callboardcate_idx="+callboardcate_idx ;
            }

        }

    </script>
