
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>

    <div class="page-header">
        <h2>차종 정보 관리</h2>
    </div>


    <fieldset>

        <form id="inputform" name="af" class="form-horizontal" role="form" method="post"  action="/carmore/CarmoreBoardProc">
            <input type="hidden" name="emode" id="emode" value="<?=$data["emode"]?>">
            <input type="hidden" name="board_code" value="event">
            <input type="hidden" name="board_idx" value="<?=$data["board_idx"]?>">
            <input type="hidden" name="content_code" value="<?=$data["content_code"]?>">


            <div class="form-group">
                <label for="boardTitle" class="col-md-2 control-label">브랜드 /모델명</label>
                <div class="col-md-10">
                    <p class="form-control-static"><?=$data["carinfomst_brand"]?> <?=$data["carinfomst_model"]?> (<?=$data["carinfomst_typestr"]?>) </p>
                </div>
            </div>

            <div class="form-group">
                <label for="boardTitle" class="col-md-2 control-label">최소/최대연비</label>
                <div class="col-md-10">
                    <p class="form-control-static"><?=$data["min_fuel_efficiency"]?> / <?=$data["max_fuel_efficiency"]?> </p>
                </div>
            </div>


            <hr>
            <div class="form-group">
                <label for="boardText" class="col-md-2 control-label">차량설명</label>
                <div class="col-md-10">
                    <p class="form-control-static"><?=$data["carinfomst_memo"]?></p>
                </div>
            </div>


            <br/>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">

                        <div class="col-md-4">
                            <button type="button" onclick="location.href='?ptype=w&carinfokey=<?=$data["carinfokey"]?>&board_code=carmst&content_code=<?=$data["carinfokey"]?>'" class="btn btn-primary btn-block">수정하기</button>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-danger btn-block" id="deleteBtn">삭제하기</button>
                        </div>

                        <div class="col-md-4">
                            <button type="button" class="btn btn-success btn-block" style="background-color: #aaa; border-color: #aaa;" onclick="location.href='/carmore/Carinfomaster'" >목록가기</button>
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">
                        <div class="col-md-12">
                            <style>
                                #adminReply, .adminReply, .adminReplyInsertBtn { margin-top:10px;}
                                .adminReplyRow { padding:7px 0 7px 0;}
                                .gi-2x{font-size: 2em;}
                                .gi-3x{font-size: 3em;}
                                .gi-4x{font-size: 4em;}
                                .gi-5x{font-size: 5em;}
                                .replyContent {overflow:hidden; word-wrap:break-word;}
                            </style>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </fieldset>


    <script type="text/javascript">
        function deletefile(filename)
        {
            if ( confirm("삭제 하시겠습니까?")) {
                location.href="/application/views/data/deletedropzone.php?board_code=<?=$data["board_code"]?>&content_code=<?=$data["carinfokey"]?>&fileSaveName="+filename+"&backurl=<?=$data["board_code"]?>&page=<?=$data["per_page"]?>";

            }
        }

        $(function() {
            $("#deleteBtn").click(function() {
                if (!confirm("정말 삭제하시겠습니까?")) {
                    return false;
                }
                $(location).attr('href','/carmore/CarinfomasterProc?emode=del&carinfokey=<?=$data["carinfokey"]?>');
            });
        });

    </script>

