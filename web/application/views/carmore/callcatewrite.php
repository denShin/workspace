
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <div class="page-header">
        <h2>고객전화 카테고리관리</h2>
    </div>


    <form id="insertTeam" class="form-horizontal" role="form" method="post" action="/carmore/CarmoreCallCate">
        <input type="hidden" name="emode" value="<?=$data["emode"]?>">
        <input type="hidden" name="ptype" value="setCallCateInfo">
        <input type="hidden" name="callboardcate_idx" value="<?=$data["callboardcate_idx"]?>">


        <fieldset>


            <div class="form-group">
                <label for="team_name" class="col-md-2 control-label">카테고리명</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="callboardcate_name" placeholder="카테고리명을 입력하세요" value="<?=$data["callboardcate_name"]?>" autofocus />
                </div>
            </div>


            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary btn-block">저장하기</button>
                        </div>

                        <div class="col-md-6">
                            <button type="button" class="btn btn-success btn-block" style="background-color: #aaa; border-color: #aaa;" onclick="location.href='/carmore/CarmoreCallCate'">목록가기</button>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>

    <script type="text/javascript">

        $(function() {
            $("#insertTeam").submit(function() {


                if ($("select[name='callboardcate_name']").val() == '') {
                    alert("카테고리명을 입력하세요.");
                    $("select[name='callboardcate_name']").focus();
                    return false;
                }


                if (!confirm("등록하시겠습니까?")) {
                    return false;
                }

            });
        });


    </script></div>