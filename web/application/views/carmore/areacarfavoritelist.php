<Script language="JavaScript">

    function carimgaemodal(ptype, carinfokey){

        var headurl ="/carmore/Carinfoimage?ptype="+ptype+"&carinfokey="+carinfokey;


        $.ajax({
            url: headurl,
            type: 'GET',
            cache: false,
        }).done(function(result){

            $('#viewmodal .modal-body').html(result)
            $('#viewmodal').modal('show')
        });
    }

    function closeviewmodal(){
        $("#viewmodal").modal("hide");
        location.reload();
    }

</Script>

<div class="container-fluid" style="padding-top: 20px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }


    </style>


    <div class="page-header clearfix">
        <form id="searchCompanyNotice" class="form-horizontal" role="form" method="get" >
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-4">
                            <h3>지역차종즐겨찾기</h3>
                        </div>
                        <div class="col-md-8">
                            <div class="row" style="padding-top: 15px">
                                <div class="col-md-2">
                                    <select class="form-control" name="city" id="city" onchange="citychange()">
                                        <option value="">==선택하세요==</option>
                                       <?=$data["select_city"]?>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <select class="form-control" name="township"id="township" onchange="changetownship()">
                                        <option value="">==선택하세요==</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" onclick="savefavoriteorder()" class="btn btn-primary btn-block">즐겨찾기 순서저장하기</button>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </fieldset>
        </form>

    </div>

    <div class="row">

        <div class="col-md-8" style="overflow-x: hidden; overflow-y: scroll;height: 500px ">
            <div class="clearfix"></div>


                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr class="info row">
                        <th class="text-center col-md-2">대표사진</th>
                        <th class="text-center col-md-3">제조사</th>
                        <th class="text-center col-md-3">차종이름</th>
                        <th class="text-center col-md-3">순서</th>
                        <th class="text-center col-md-1">Check</th>
                    </tr>
                    </thead>
                    <tbody id="favorlist">
                    <?php
                    foreach($data["favorlist"] as $entry) {
                        $files =$entry["filelist"];
                        ?>
                        <tr class="row" id="<?=$entry["areafavor_idx"]?>">
                            <td  class="small text-center">
                             <span style="padding-left: 5px">
                                <img  src="<?=$entry["fullurl"]?>" width="60">
                            </span>
                            </td>
                            <td   class="small text-center"  ><?=$entry["carinfomst_brand"]?></td>
                            <td  style="padding-left: 10px;align:left"  class="small"> <?=$entry["carinfomst_model"]?></td>
                            <td  class="small text-center">
                                <button type="button" class="btn btn-default carup ">
                                    <span class="glyphicon glyphicon-arrow-up"></span>
                                </button>
                                <button type="button" class="btn btn-default cardown ">
                                    <span class="glyphicon glyphicon-arrow-down"></span>
                                </button>
                            </td>


                            <td  class="small text-center">
                                <button type="button" class="btn btn-danger btn-block" onclick="areacarfavormanage('delfavor','<?=$entry["carinfo_idx"]?>')">
                                    <span class="glyphicon glyphicon-minus"></span> 삭제
                                </button>
                            </td>
                        </tr>
                    <?}?>

                    </tbody>
                </table>

        </div>
        <div class="col-md-4" style="overflow-x: hidden; overflow-y: scroll;height: 500px ">

                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr class="info row">
                        <th class="text-center col-md-1">Check</th>
                        <th class="text-center col-md-3">대표사진</th>
                        <th class="text-center col-md-3">제조사</th>
                        <th class="text-center col-md-5">차종이름</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($data["alllist"] as $entry) {

                        $files =$entry["filelist"];
                        ?>
                        <tr class="row">
                            <td  class="small text-center">

                                <button type="button" class="btn btn-success btn-block" onclick="areacarfavormanage('addfavor','<?=$entry["carinfo_idx"]?>')">
                                    <span class="glyphicon glyphicon-plus"></span> 추가
                                </button>
                            </td>
                            <td  class="small text-center">
                        <span style="padding-left: 5px">
                                <img  src="<?=$entry["fullurl"]?>" width="60">
                            </span>
                            </td>
                            <td   class="small text-center"  ><?=$entry["carinfomst_brand"]?></td>
                            <td  style="padding-left: 10px;align:left"  class="small"><?=$entry["carinfomst_model"]?></td>

                        </tr>
                    <?}?>

                    </tbody>
                </table>

        </div>


    </div>


    <!-- Bootstrap & Core Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>

    <script type="text/javascript">

        function citychange(){

            var townshipobj=$("#township");
            var city=$("#city").val();
            gettownoption(city,'');
        }

        function gettownoption(city,township) {

            // $carinfomst_brand,$carinfomst_type
            $("select[id='township'] option").remove();
            var targetid ="township";

            console.log("ci:"+city+", town:"+township);


            //  /carmore/Carinfomaster?ptype=mstcode&carinfomst_brand=기아&carinfomst_type=6
            //   http://workspace2.teamo2.kr/carmore/Carinfomaster?ptype=mstcode&carinfomst_brand=기아&carinfomst_type=중형
            console.log("/carmore/Areacarfavorite?ptype=townshipcode&city="+city  );
            $.ajax({
                type:"post",contentType: "application/json",
                url:"/carmore/Areacarfavorite?ptype=townshipcode&city="+city ,
                datatype: "json",
                success: function(data) {

                    var json = $.parseJSON(data);

                    $("#"+targetid).append("<option value='' >==선택==</option>");
                    for(var i=0;i < json.length ; i++) {

                        var row = json[i];
                        var tmptownship = row["township"];

                        if(township ===tmptownship){
                            $("#"+targetid).append("<option value='"+tmptownship+"' selected>"+tmptownship+"</option>");
                        }else{
                            $("#"+targetid).append("<option value='"+tmptownship+"'>"+tmptownship+"</option>");
                        }
                    }

                },
                error: function(x, o, e) {

                }
            });
        }

        function changetownship(){

            var city = $("#city").val();
            var township = $("#township").val();
            location.href="/carmore/Areacarfavorite?city="+city+"&township="+township;
        }


        var order ="";
        $(function() {

            gettownoption('<?=$data["city"]?>','<?=$data["township"]?>')


            $('.carup').click(function(){
                var current = $(this).closest('tr');
                current.prev().before(current);
                order= $("tbody").sortable("toArray");
                console.log("order:"+order);
            });
            $('.cardown').click(function(){
                var current = $(this).closest('tr');
                current.next().after(current);
                order= $("tbody").sortable("toArray");
                console.log("order:"+order);
            });
        });

        $('#favorlist').sortable({
            update: function( ) {
                order= $("tbody").sortable("toArray");
                console.log("order:"+order);
            }
        });


        function areacarfavormanage(ptype,carinfo_idx){
            var city = $("#city").val();
            var township = $("#township").val();
            var confirmtxt="";

            if(city ===""){
                alert('시도를 선택해주세요');
                return;
            }
            if(township ===""){
                alert('구군을 선택해주세요');
                return;
            }

            if(ptype==="addfavor"){
                confirmtxt='즐겨찾기 목록에 등록하시겠습니까?';
            }else if(ptype==="delfavor"){
                confirmtxt='즐겨찾기 목록에서 삭제하시겠습니까?';
            }

            if(confirm(confirmtxt)){

                var procurl ="/carmore/Areacarfavorite?ptype="+ptype+"&city="+city+"&township="+township+"&carinfo_idx="+carinfo_idx;

                $.ajax({
                    url: procurl,
                    type: 'GET',
                    cache: false,
                }).done(function(result){
                    location.href="/carmore/Areacarfavorite?city="+city+"&township="+township;
                });
            }
        }

        function savefavoriteorder(){
            var city = $("#city").val();
            var township = $("#township").val();
            var confirmtxt="즐겨찾기 순서정보를 저장하시겠습니까?";
            order= $("tbody").sortable("toArray");


            if(city ===""){
                alert('시도를 선택해주세요');
                return;
            }
            if(township ===""){
                alert('구군을 선택해주세요');
                return;
            }

            if(order===""){
                alert('설정된 즐겨찾기가 없습니다');
                return;
            }
            if(confirm(confirmtxt)){


                var procurl ="/carmore/Areacarfavorite?ptype=setfavororder&city="+city+"&township="
                    +township+"&order="+order;

                $.ajax({
                    url: procurl,
                    type: 'GET',
                    cache: false,
                }).done(function(result){
                    location.href="/carmore/Areacarfavorite?city="+city+"&township="+township;
                });
            }
        }

    </script>

