<script language="JavaScript">
    $(document).ready(function(){
        //최상단 체크박스 클릭
        $("#checkall").click(function(){
            //클릭되었으면
            if($("#checkall").prop("checked")){
                //input태그의 name이 chk인 태그들을 찾아서 checked옵션을 true로 정의
                $("input[name=board_idx]").prop("checked",true);
                //클릭이 안되있으면
            }else{
                //input태그의 name이 chk인 태그들을 찾아서 checked옵션을 false로 정의
                $("input[name=board_idx]").prop("checked",false);
            }
        })
    })

    function set_statscomplete(){


        var objboard_idx =  $('[name="board_idx"]');
        var objlength =objboard_idx.length;
        var saveparam=new Array();
        var cnt=0;
        for(var i=0;i < objlength ;i++){
            var obj =objboard_idx[i];
            if(obj.checked){
                var board_idx =objboard_idx[i]["value"];

                if(board_idx !=="" ){
                    saveparam.push(board_idx);
                    cnt++;
                }
            }

        }

        if(cnt===0){
            alert('선택된 고객상담 정보가 없습니다.');
            return;
        }
        saveparam = saveparam.join(",");

        $.ajax({
            type:"get",contentType: "application/json",
            url:"/carmore/CarmoreBoardProc?emode=setallstats&saveparam="+saveparam,
            datatype: "json",
            success: function(data) {
                alert("수정이 완료되었습니다.");
                location.reload();

            },
            error: function(x, o, e) {

            }
        });


    }
</script>
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>

    <div class="page-header clearfix">
        <h2 class="pull-left">고객 문의 상담</h2>
        <div class="pull-right" style="padding-top: 30px">
            <a href="?ptype=w&board_code=<?=$data["board_code"]?>" class="btn btn-info">등록하기</a>
        </div>
    </div>
    <div id="searchdiv">
        <form id="searchCompanyNotice" class="form-horizontal" role="form" method="get" >
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <select class="form-control" name="sp">
                                <option value="kakaonickname" >고객명</option>
                                <option value="board_title" >제목</option>
                                <option value="board_content" >내용</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="sv" value="">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </fieldset>
        </form>
        <hr/>
    </div>


    <form id="updateConfirm">

        <button type="button" class="btn btn-primary "onclick="set_statscomplete()">처리완료</button>

        <div class="clearfix" style="padding-top: 10px"></div>

        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">
                        <input type="checkbox" id="checkall">
                        No
                    </th>
                    <th class="text-center col-md-1">분류</th>
                    <th class="text-center col-md-1">상태</th>
                    <th class="text-center col-md-1">고객</th>
                    <th class="text-center col-md-5">제목</th>
                    <th class="text-center col-md-1">등록자</th>
                    <th class="text-center col-md-2">등록일</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($data["list"] as $entry) {
                    ?>
                    <tr class="row">
                        <td  class="small text-center">
                            <input type="checkbox" name="board_idx" value="<?= $entry["board_idx"] ?>">
                            <?=$entry["board_idx"]?>
                        </td>
                        <td   class="small text-center"  ><?=$entry["board_partstr"]?></td>
                        <td   class="small text-center"  ><?=$entry["board_statstr"]?></td>
                        <td   class="small text-center"> <a href="?ptype=v&board_idx=<?=$entry["board_idx"]?>&per_page=<?=$data["per_page"]?>"><?=$entry["kakaonickname"]?> </a></td>
                        <td  style="padding-left: 10px;align:left"  class="small">  <a href="?ptype=v&board_idx=<?=$entry["board_idx"]?>&per_page=<?=$data["per_page"]?>"><?=$entry["board_title"]?></a> <?if($entry["reply_cnt"] >0){?>(<b><?=$entry["reply_cnt"]?>)</b><?}?></td>
                        <td   class="small text-center"><?=$entry["mem_name"]?> </td>
                        <td   class="small text-center"><?=$entry["regdate"]?></td>
                    </tr>
                <?}?>

                </tbody>
            </table>
        </div>
    </form>


    <div class="text-center">
        <?=$data['pagination']?>
    </div>
