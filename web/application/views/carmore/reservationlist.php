<div class="container-fluid" style="padding-top: 50px;padding-bottom: 70px;">

    <div class="page-header clearfix">
        <h3 class="pull-left">차량 예약 관리 <?=$data["listitle"]?></h3>

    </div>

    <div id="searchdiv" >
        <form id="searchform" class="form-horizontal" role="form" method="get" >
            <input type="hidden" name="mem_stats" value="<?=$data["mem_stats"]?>">
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <select class="form-control" name="sp">
                                <option value="f_reservationidx" >예약번호</option>
                                <option value="driver_name" >운전자</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="sv" value="">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </fieldset>
        </form>
        <hr/>
    </div>

    <div class="clearfix"></div>
    <form name="updateConfirm" method="post">
        <input type="hidden" name="emode" value="changestats">
        <input type="hidden" name="stats" >

        <div class="table-responsive">
            <table class="table   table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">Check</th>
                    <th class="text-center col-md-3">차량 대여정보</th>
                    <th class="text-center col-md-2">운전자</th>
                    <th class="text-center col-md-2">결제정보</th>
                    <th class="text-center col-md-2">회사정보</th>
                    <th class="text-center col-md-1">알림톡</th>
                    <th class="text-center col-md-2">등록일</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($data["list"] as $entry) {
                    $moid = $entry["f_reservationidx"];

                    ?>
                    <tr class="text-center row">
                        <td><input type="checkbox" name="usrserial[]" value="<?= $entry["f_usrserial"] ?>"></td>
                        <td class="small text-left"   >
                            <?if($entry["admin_name"] !=""){?>

                                <span class="label label-danger " style="cursor: pointer"  ><b><?=$entry["admin_name"]?></b> 관리자 예약 </span>

                                <br>
                            <?}?>
                           <span style="font-size: medium"><strong><?= $entry["model"] ?></strong></span>
                            <br><?= $entry["brand"] ?>  /<?= $entry["carType"] ?>/<?=$entry["age_info"]?>

                            <?if($entry["f_rentstatustr"]=="결제취소"){?>
                            <span class="label label-danger searchStatus" data-status=""><?= $entry["f_rentstatustr"] ?></span>
                            <?}else{?>
                            <span class="label label-primary searchStatus" data-status=""><?= $entry["f_rentstatustr"] ?></span>
                            <?}?>
                            <br><?= $entry["car_number1"] ?> <?= $entry["car_number2"] ?> <?=$entry["car_number3"]?> /  <?=$entry["oil"]?> <?if($entry["color"] !=""){?>/  <?=$entry["color"]?><?}?>

                            <hr>
                            <span class="label label-info ">예약번호</span> <?=$moid?><br>
                            <span class="label label-default ">대여기간</span> <?= $entry["f_rentstartdate"] ?> ~  <?= $entry["f_rentenddate"] ?>
                            <?if($entry["f_delivpickaddr"] !=""){?>
                            <br><br> 배차주소 :<?=$entry["f_delivpickaddr"]?>
                            <br>회차주소 :<?=$entry["f_delivpickaddr"]?>
                            <?}?>

                        </td>
                        <td class="small text-left"   >
                            <span style="font-size: medium"><strong><a  style="cursor: pointer" onclick="setuserstats('etc','<?= $entry["f_usrserial"]?>')"><?= $entry["driver_name_encrypt"] ?></a></strong></span>
                            <br>카카오아이디 : <a href="/carmore/AppUser?mem_stats=y&sp=kakaoid&sv=<?=$entry["kakaoid"]?>"><?=$entry["kakaoid"]?></a>
                            <hr><span style="font-size: medium"><strong><?= $entry["driver_phone_encrypt"] ?></strong></span>
                            <BR>생년월일 : <?= $entry["driver_birthday_encrypt"] ?>
                            <BR><?= $entry["f_driveremail"] ?>
                        </td>
                        <td class="small text-left">
                            <span style="font-size: medium"><strong><?= $entry["f_totalprice"] ?></strong> 원</span>

                            <hr>총 단가 : <?= $entry["original_price"] ?> 원
                            <br>렌트비용 : <?= $entry["f_rentprice"] ?> 원 / 보험비용 : <?= $entry["f_insuprice"] ?> 원
                            <br>배송비용 : <?= $entry["f_delivprice"] ?> 원 / 사용포인트 : <span style="color: red">- <?= $entry["f_usepoint"] ?> </span>  원
                            <br>쿠폰 : <?=$entry["coupon_title"]?> <span style="color: red">- <?= $entry["coupon_price"] ?> </span>  원
                        </td>
                        <td class="small text-left"   >
                            <span style="font-size: medium"><strong><?= $entry["name"] ?></strong></span>
                            <br><?= $entry["branchName"] ?>
                            <br>
                            <br><?= $entry["branchOwner"] ?>
                            <br><?= $entry["branchOwnerTel"] ?>
                        </td>
                        <td class="small text-center"><button type="button" class="btn btn-success btn-sm" onclick="window.open('/carmore/alarm/biztalk/BiztalkInventory?recNum=&moid='.concat(<?=$moid?>))">전송내역확인</button></td>
                        <td class="small text-center"><?= $entry["regdate"] ?>  <br>
                            <?if($entry["cancelyn"] =="y"){?>
                            <button type="button" class="btn btn-danger"  onclick="gorentdtl('<?=$entry["f_reservationidx"]?>')">
                                <?=$entry["reserv_rent_typestr"]?> 예약취소</button>
                            <?}?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="text-center">
            <?=$data['pagination']?>
        </div>
    </form>

    <!--* modal start *-->
    <div id="myModal" class="modal fade bs-modified-modal-sm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger modalTitle"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2">
                            <p class="form-control-static text-right">관련 사유</p>
                        </div>
                        <div class="col-md-9">
                            <textarea   class="form-control" id="message" placeholder="상담내용을 입력하세요"  /></textarea>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="goSubmit"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="confirmBtn"></span></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function gorentdtl(f_reservationidx){
            location.href="/carmore/ReservationManage?ptype=w&f_reservationidx="+f_reservationidx;
        }


    </script>
</div>




</body>
</html>
