
<link rel="stylesheet" href="/static/lib/fastselect/dist/fastselect.css">
<script src="/static/lib/fastselect/dist/fastselect.standalone.js"></script>
<style>

    .fstElement { font-size: 1.2em; }
    .fstToggleBtn { min-width: 16.5em; }

    .submitBtn { display: none; }

    .singleInputDynamic { width: 100%;  }
    .fstMultipleMode { display: block; }
    .fstMultipleMode .fstControls { width: 100%;  }

</style>
<script>

    function set_valdate(){
        var deadline_type = $(":input:radio[name=deadline_type]:checked").val();
        if(deadline_type==="1"){
            $("#valper1").show();
            $("#valper2").hide();
        }else{
            $("#valper1").hide();
            $("#valper2").show();
        }
    }

</script>

<div class="container" style="padding-top: 50px;padding-bottom: 70px;">



    <div class="page-header">
        <h2>쿠폰 등록 관리</h2>
    </div>

    <form id="inputform" class="form-horizontal" role="form" method="post" enctype="multipart/form-data"  action="/carmore/CouponManageProc">
        <input type="hidden" name="emode" value="<?=$data["emode"]?>">
        <input type="hidden" name="serial" value="<?=$data["serial"]?>">

        <fieldset>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">쿠폰 제목</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" name="title" placeholder="쿠폰 제목"  value="<?=$data["title"]?>"   autofocus/>
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">유효기간타입</label>
                <div class="col-md-3">
                    <label class="radio-inline">
                        <input type="radio" name="deadline_type" id="deadline_type1" value="1" onclick="set_valdate()" <?if($data["deadline_type"]=="" || $data["deadline_type"]=="1"){echo "checked";}?> > 날짜기간
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="deadline_type" id="deadline_type2" value="2" onclick="set_valdate()" <?if($data["deadline_type"]=="2"){echo "checked";}?> > 등록경과일
                    </label>

                </div>
            </div>
            <div class="form-group" id="valper1">
                <label for="shareTitle" class="col-md-2 control-label">유효기간</label>
                <div class="col-md-3">
                    <input type="date" class="form-control" name="start_date" placeholder="시작일"  value="<?=$data["start_date"]?>"   autofocus/>
                </div>
                <div class="col-md-3">
                    <input type="date" class="form-control" name="end_date" placeholder="마감일"  value="<?=$data["end_date"]?>"   autofocus/>
                </div>
            </div>
            <div class="form-group" id="valper2" style="display: none">
                <label for="shareTitle" class="col-md-2 control-label">유효기간</label>
                <div class="col-md-2">
                   등록일부터

                    <input type="number" class="form-control" name="deadline_after_register" placeholder="경과일"  value="<?=$data["deadline_after_register"]?>"  autofocus/>
                </div>
            </div>
            <div class="form-group">
                <label for="shareCategory" class="col-md-2 control-label">쿠폰 종류</label>
                <div class="col-md-5">
                    <select name="type2" class="form-control">
                        <option value="0" <?if($data["type2"]=="0"){ echo "selected";}?> >기본일괄지급쿠폰</option>
                        <option value="1" <?if($data["type2"]=="1"){ echo "selected";}?> >신규가입쿠폰</option>
                        <option value="2" <?if($data["type2"]=="2"){ echo "selected";}?> >주문한번도 안해본 사람 </option>
                        <option value="3" <?if($data["type2"]=="3"){ echo "selected";}?> >리뷰리워드 쿠폰</option>
                        <option value="5" <?if($data["type2"]=="5"){ echo "selected";}?> >여러번 등록가능</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">쿠폰코드</label>
                <div class="col-md-5">
                    <input type="text" class="form-control" name="certificate_key" placeholder="쿠폰코드"  value="<?=$data["certificate_key"]?>"   autofocus/>
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">쿠폰개수</label>
                <div class="col-md-5">
                    <input type="number" class="form-control" name="quantity" placeholder="쿠폰개수"   value="<?=$data["quantity"]?>"   autofocus/>
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">할인 방법</label>
                <div class="col-md-2">
                    <input type="text" class="form-control" name="value" placeholder="할인 방법"  value="<?=$data["value"]?>"   autofocus/>
                </div>
                <div class="col-md-2">
                    <select name="value_type" class="form-control">
                        <option value="1" <?if($data["value_type"]=="1"){ echo "selected";}?> >%</option>
                        <option value="2" <?if($data["value_type"]=="2"){ echo "selected";}?> >원</option>
                    </select>
                </div>
            </div>

            <hr>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">지역 설정</label>
                <div class="col-md-2">
                    <select class="form-control" name="city" id="city" onchange="citychange('city','township')">
                        <option value="">==선택하세요==</option>
                        <?=$data["select_city"]?>
                    </select>
                </div>
                <div class="col-md-2">
                    <select class="form-control" name="township"id="township"  >
                        <option value="">==선택하세요==</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">업체선택</label>
                <div class="col-md-6">
                    <span id="companyname"><?=$data["companyname"]?></span>  <input type="button" onclick="showsearchcompanymodal('car')" value="업체검색">
                    <input type="hidden" name="target_com_idx" id="target_com_idx" value="<?=$data["target_com_idx"]?>" >
                    <input type="hidden" name="target_bra_idx" id="target_bra_idx" value="<?=$data["target_bra_idx"]?>" >
                </div>


            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">연결지역</label>

                <div class="col-md-2">
                    <select class="form-control" name="pcity" id="pcity" onchange="citychange('pcity','ptownship')">
                        <option value="">==선택하세요==</option>
                        <?=$data["pselect_city"]?>
                    </select>
                </div>
                <div class="col-md-2">
                    <select class="form-control" name="ptownship"id="ptownship"  >
                        <option value="">==선택하세요==</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">차종선택</label>
                <div class="col-md-6">
                    <span id="cartitle"><?=$data["cartitle"]?></span>  <input type="button" onclick="showsearchcarmodal('car')" value="차종검색">
                    <input type="hidden" name="target_carinfo_idx" id="target_carinfo_idx"  value="<?=$data["target_carinfo_idx"]?>" >
                </div>

            </div>
            <hr>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">특정예약기간</label>
                <div class="col-md-3">
                    <input type="date" class="form-control" name="target_reserv_term_start" placeholder="시작일"   value="<?=$data["target_reserv_term_start"]?>"   autofocus/>
                </div>
                <div class="col-md-3">
                    <input type="date" class="form-control" name="target_reserv_term_end" placeholder="마감일"   value="<?=$data["target_reserv_term_end"]?>"   autofocus/>
                </div>
            </div>


            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">최소대여시간</label>
                <div class="col-md-3">
                    <input type="number" class="form-control" name="target_reserv_minimum_period" placeholder="숫자만 입력"   value="<?=$data["target_reserv_minimum_period"]?>"   autofocus/>
                </div>

            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">사용가능금액</label>
                <div class="col-md-2">
                    <input type="number" class="form-control" name="limit_pay" placeholder="사용가능금액"  value="<?=$data["limit_pay"]?>"   autofocus/>
                </div>
                <div class="col-md-5">
                    ( 0원은 전체 사용가능)
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">렌트유형</label>
                <div class="col-md-7">

                    <label class="radio-inline">
                        <input type="radio" name="target_rent_type" id="inlineRadio1" value="0" <?if($data["target_rent_type"]=="" || $data["target_rent_type"]=="0"){echo "checked";}?> > 전체
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="target_rent_type" id="inlineRadio2" value="1"  <?if($data["target_rent_type"]=="1"){echo "checked";}?>> 단기
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="target_rent_type" id="inlineRadio3" value="2"  <?if($data["target_rent_type"]=="2"){echo "checked";}?>> 월
                    </label>
                </div>

            </div>
            <hr>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">상태</label>

                <div class="col-md-2">
                    <select name="status" class="form-control">
                        <option value="0" <?if($data["status"]=="0"){ echo "selected";}?> >시작안함</option>
                        <option value="1" <?if($data["status"]=="1"){ echo "selected";}?> >시작함</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="shareContent" class="col-md-2 control-label">쿠폰 설명</label>
                <div class="col-md-10">
                    <textarea   class="form-control"  name="description"  style="width: 60%;height: 80px"  id="description"><?=$data["description"]?></textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary btn-block">저장하기</button>
                        </div>
                        <div class="col-md-6">
                            <button type="reset" class="btn btn-warning btn-block">다시작성</button>
                        </div>
                    </div>

                </div>
            </div>
        </fieldset>
    </form>


    <div id="searchcarmodal" class="modal fade bs-modified-modal-sm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger ">차종 검색하기</h4>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-9">
                            <input type="text" placeholder="차종 검색"  class="singleInputDynamic form-control" data-url="/data/Cardata" data-load-once="true" name="tmp_carinfo"  id="tmp_carinfo" />

                        </div>
                        <div class="col-md-1"></div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"  onclick="set_carinfo()"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="pconfirmBtn">적용하기</span></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
                </div>
            </div>
        </div>
    </div>


    <div id="searchcompanymodal" class="modal fade bs-modified-modal-sm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger ">업체 검색하기</h4>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-9">
                            <input type="text" placeholder="업체 검색" id="tmp_companyinfo" class="singleInputDynamic form-control" data-url="/data/Cardata?emode=getsearchbranch" data-load-once="true" name="companyinfo" />

                        </div>
                        <div class="col-md-1"></div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary"  onclick="set_companyinfo()"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="pconfirmBtn">적용하기</span></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
                </div>
            </div>
        </div>
    </div>




    <script type="text/javascript">
        var searchtype="";

        function showsearchcarmodal(st){
            $("#searchcarmodal").modal('show');
        }

        function set_carinfo(){

            var tmp_carinfo= $("#tmp_carinfo").val();


            var tmparr = tmp_carinfo.split(";");
            var cartitle = tmparr[0];
            var target_carinfo_idx = tmparr[1];


            $("#cartitle").text(cartitle)
            $("#target_carinfo_idx").val(target_carinfo_idx)

            $("#searchcarmodal").modal('hide');
        }

        function showsearchcompanymodal(st){
            $("#searchcompanymodal").modal('show');
        }

        function set_companyinfo(){

            var tmp_companyinfo = $("#tmp_companyinfo").val();
            // $title."-".$companyserial."-".$branchserial;
            var tmparr = tmp_companyinfo.split(";");
            var companytitle = tmparr[0];
            var target_com_idx = tmparr[1];
            var target_bra_idx = tmparr[2];

            //companyname  target_com_idx target_bra_idx
            $("#companyname").text(companytitle)
            $("#target_com_idx").val(target_com_idx)
            $("#target_bra_idx").val(target_bra_idx)

            $("#searchcompanymodal").modal('hide');
        }


        function citychange(nid,targetid,township){

            var townshipobj=$("#"+targetid);
            var city=$("#"+nid).val();
            gettownoption(city,targetid,township);
        }

        function gettownoption(city,targetid,township) {

            // $carinfomst_brand,$carinfomst_type
            $("select[id='"+targetid+"'] option").remove();




            //  /carmore/Carinfomaster?ptype=mstcode&carinfomst_brand=기아&carinfomst_type=6
            //   http://workspace2.teamo2.kr/carmore/Carinfomaster?ptype=mstcode&carinfomst_brand=기아&carinfomst_type=중형
            console.log("/carmore/Areacarfavorite?ptype=newtownshipcode&city="+city  );
            $.ajax({
                type:"post",contentType: "application/json",
                url:"/carmore/Areacarfavorite?ptype=newtownshipcode&city="+city ,
                datatype: "json",
                success: function(data) {

                    var json = $.parseJSON(data);
                    console.log("json:%o",json)

                    $("#"+targetid).append("<option value='' >==선택==</option>");
                    for(var i=0;i < json.length ; i++) {

                        var row = json[i];
                        var ct_value = row["ct_value"];
                        var tmptownship = row["township"];

                        if(township ===ct_value){
                            $("#"+targetid).append("<option value='"+ct_value+"' selected>"+tmptownship+"</option>");
                        }else{
                            $("#"+targetid).append("<option value='"+ct_value+"'>"+tmptownship+"</option>");
                        }
                    }

                },
                error: function(x, o, e) {

                }
            });
        }

        $(function() {
            set_valdate();
            // township ptownship
            <?if($data["target_com_first_location"] !=""){?>
                citychange('city','township','<?=$data["target_com_first_location"]?>');
            <?}?>
            <?if($data["target_location_str"] !=""){?>
            citychange('pcity','ptownship','<?=$data["target_location_str"]?>');
            <?}?>

            $('.multipleInputDynamic').fastselect();
            $('.singleInputDynamic').fastselect();

            $("#inputform").submit(function() {

                if ($("input[name='title']").val() =="") {
                    alert("쿠폰 제목을 입력하세요.");
                    $("input[name='title']").focus();
                    return false;
                }

                if ($("input[name='start_date']").val() =="") {
                    alert("쿠폰 시작일을 입력하세요.");
                    $("input[name='start_date']").focus();
                    return false;
                }

                if ($("input[name='end_date']").val() =="") {
                    alert("쿠폰 마감일을 입력하세요.");
                    $("input[name='end_date']").focus();
                    return false;
                }

                if ($("input[name='value']").val() =="") {
                    alert("쿠폰 사용가능금액을 입력하세요.");
                    $("input[name='value']").focus();
                    return false;
                }


                if (!confirm("정말 등록하시겠습니까?")) {

                    return false;
                }
            });

        });


    </script></div>
