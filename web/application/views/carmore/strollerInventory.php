<?php
$inventory    = $data['inventory'];
$search_value = $data['searchValue'];

?>

<div class="container workspace-basic-container">
    <div class="page-header clearfix">
        <h3 class="pull-left">유모차 업체 관리</h3>
    </div>

    <div>
        <form class="form-horizontal" role="form" method="get">
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-2">
                            <select class="form-control" name="searchParam">
                                <option value="1">업체명</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="searchValue" value="<?=$search_value?>" />
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-10" style="margin-top:1em; margin-bottom: 1em">
            <button type="button" class="btn btn-primary pull-right" onclick="location.href='/carmore/StrollerInformation?crud=r&csIdx='.concat(btoa('-1'))">신규등록</button>
        </div>
<?php
if (count($inventory) !== 0)
{

?>
        <div class="row table-responsive col-md-offset-2 col-md-8">
            <table class="table table-bordered">
                <thead>
                    <tr class="info">
                        <th class="text-center col-md-3">업체구분</th>
                        <th class="text-center col-md-7">업체명</th>
                        <th class="text-center col-md-2"></th>
                    </tr>
                </thead>
                <tbody>
<?php

    foreach ($inventory as $obj)
    {
        $stroller_type_label = ($obj['type'] === 1)? "<span class='label label-info'>유모차 업체</span>" : "<span class='label label-warning'>렌트카 자체운용</span>";

?>
                    <tr>
                        <td class="workspace-middle-td"><?=$stroller_type_label?></td>
                        <td class="workspace-middle-td"><?=$obj['name']?></td>
                        <td class="workspace-middle-td">
                            <button type="button" class="btn btn-sm btn-default" onclick="location.href='/carmore/StrollerInformation?crud=r&csIdx=<?=base64_encode($obj['idx'])?>'">설정</button>
                        </td>
                    </tr>
<?
    }

?>
                </tbody>
            </table>
        </div>
<?php
}
else
{

?>
        <div class="text-center" style="margin-top: 100px">
            <h4>등록된 유모차 업체가 없습니다</h4>
            <button type="button" class="btn btn-lg btn-link" onclick="location.href='/carmore/StrollerInformation?crud=r&csIdx='.concat(btoa('-1'))">지금 바로 등록하기</button>
        </div>
<?php
}

?>
    </div>

</div>
