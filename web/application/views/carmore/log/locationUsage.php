<?php
$date            = $data->date;
$usage_inventory = $data->inventory;
?>
<div class="container workspace-basic-container col-md-12">
    <div class="page-header clearfix">
        <h3 class="pull-left">위치정보 이용 내역</h3>
    </div>

    <div class="row col-md-12">
        <div class="form-group col-md-offset-4 col-md-3">
            <label for="lu_date" class="control-label">검색날짜</label><br>
            <div class="col-md-12">
                <input type="date" class="form-control" id="lu_date" value="<?=$date?>"/>
            </div>
        </div>

        <div class="col-md-1">
            <label for="lu_search_button" class="control-label">&nbsp;</label><br>
            <button type="button" class="btn btn-info" id="lu_search_button"
                    onclick="luOnclickSearchButton()">검색</button>
        </div>
    </div>

    <div class="row col-md-12 table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr class="info">
                    <th class="text-center">대상</th>
                    <th class="text-center">취득경로</th>
                    <th class="text-center">제공서비스</th>
                    <th class="text-center">제공받는자</th>
                    <th class="text-center">이용일시</th>
                </tr>
            </thead>
            <tbody>
<?php
foreach ($usage_inventory as $object) {

?>
                <tr>
                    <td><?=$object->socialId?></td>
                    <td><?=$object->path?></td>
                    <td><?=$object->offerService?></td>
                    <td><?=$object->offerObject?></td>
                    <td><?=$object->registerDate?></td>
                </tr>
<?php
}

?>
            </tbody>
        </table>
    </div>
</div>

<script src="<?=asset_url()?>/js/location_usage.min.js"></script>