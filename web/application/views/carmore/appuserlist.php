<?php
    $loginpointauth_yn=$_SESSION["loginpointauth_yn"];


?>
<style>

    .viewmodalwidth{
        min-width: 50%;
        min-height: 30%;
        margin: 0;
    }

    #viewmodal{
        overflow:auto !important;
    }

</style>
<div class="container-fluid" style="padding-top: 50px;padding-bottom: 70px;">

    <div class="page-header clearfix">
        <h3 class="pull-left"><?=$data["listitle"]?></h3>


    </div>

    <div id="searchdiv" >
        <form id="searchform" class="form-horizontal" role="form" method="get" >
            <input type="hidden" name="mem_stats" value="<?=$data["mem_stats"]?>">
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <select class="form-control" name="sp">
                                <option value="kakaonickname" >이름</option>
                                <option value="kakaoid" >카카오아이디</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="sv" value="">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </fieldset>
        </form>
        <hr/>
    </div>

    <div class="clearfix"></div>
    <form name="updateConfirm" method="post">
        <input type="hidden" name="emode" value="changestats">
        <input type="hidden" name="stats" >

        <div class="table-responsive">
            <table class="table   table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">userserial</th>
                    <th class="text-center col-md-3">닉네임</th>
                    <th class="text-center col-md-1">포인트</th>
                    <th class="text-center col-md-1">쿠폰내역</th>
                    <th class="text-center col-md-2">결제건/금액</th>
                    <th class="text-center col-md-1">취소건/금액</th>
                    <th class="text-center col-md-1">상담내역</th>
                    <th class="text-center col-md-1">처리</th>
                    <th class="text-center col-md-1">가입일</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $startnum =$data["startnum"];
                foreach($data["list"] as $entry) {
                    ?>
                    <tr class="text-center row">
                        <td class="small text-center"  ><?= $entry["usrserial"]?></td>
                        <td class="small text-center"   >

                            <span style='color: <?= $entry["usercolor"] ?>'>
                                <?= $entry["tarcomplaincntstr"]?>
                                <a onclick="viewcontentmodal('toporder','<?= $entry["usrserial"]?>')" style="cursor: pointer">  <?= $entry["kakaonickname"] ?> </a>(<?=$entry["kakaoid"] ?>)<?=$entry["pushlog_typestr"]?>
                                <button type="button" class="btn  btn-outline-primary   btn-xs" onclick="showsmsmodal()" >
                                    <span class="glyphicon glyphicon-envelope"></span>
                                </button>

                            </span>
                            <?if($entry["pushlog_typestr"] !=""){?>
                            <span class="label label-success " style="cursor: pointer" onclick="setnormal('<?= $entry["usrserial"]?>')">정상화</span>
                            <?}?>

                        </td>
                        <td class="small text-center" >
                            <a href="#" onclick="viewcontentmodal('pointout','<?= $entry["usrserial"]?>')"><?= $entry["usrpoint"] ?></a> p
                            <?if($loginpointauth_yn=="y"){?>
                            <span class="label label-default" style="cursor: pointer" onclick="setuserpoint('<?= $entry["usrserial"]?>')">추가</span>
                            <span class="label label-default" style="cursor: pointer" onclick="viewcontentmodal('pointout','<?= $entry["usrserial"]?>')">회수</span>
                            <?}?>
                        </td>
                        <td class="small text-center"><a href="#" onclick="viewcontentmodal('coupon','<?= $entry["usrserial"]?>')"> <?= $entry["couponcnt"]?> </a>건 </td>
                        <td class="small text-center">
                            <a onclick="viewcontentmodal('buy','<?= $entry["usrserial"]?>')" style="cursor: pointer"><?= $entry["paycnt"] ?> 건/ <?= $entry["paysum"] ?> 원</a>
                        </td>
                        <td class="small text-center">
                            <a onclick="viewcontentmodal('cancel','<?= $entry["usrserial"]?>')" style="cursor: pointer">
                                <?= $entry["cancelcnt"] ?> 건/ <?= $entry["cancelsum"] ?> 원 </a></td>
                        <td class="small text-center"><a href="/carmore/CarmoreBoard?board_code=complain&sp=usrserial&sv=<?=$entry["usrserial"]?>"> <?= $entry["complaincnt"] ?> 건 </a>
                            <span class="label label-success"  style="cursor: pointer" onclick="setuserstats('etc','<?= $entry["usrserial"]?>')">추가</span></td>
                        <td class="small text-center">
                            <?if($loginpointauth_yn=="y"){?>
                            <span class="label label-primary " style="cursor: pointer" onclick="setuserstats('block','<?= $entry["usrserial"]?>')">블락</span>
                            <span class="label label-danger " style="cursor: pointer" onclick="setuserstats('out','<?= $entry["usrserial"]?>')">탈퇴</span>
                            <?}?>
                        </td>
                        <td class="small text-center"><?= $entry["regdate"] ?></td>
                    </tr>
                    <?php
                    $startnum--;
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="text-center">
            <?=$data['pagination']?>
        </div>
    </form>

    <!--* modal start *-->
    <div id="myModal" class="modal fade bs-modified-modal-sm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger modalTitle"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2">
                            <p class="form-control-static text-right">관련 사유</p>
                        </div>
                        <div class="col-md-9">
                            <textarea   class="form-control" id="message" placeholder="상담내용을 입력하세요"  /></textarea>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="complainSubmit"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
                        <span id="complainBtn">컴플레인고객</span></button>

                    <button type="button" class="btn btn-primary" id="goSubmit"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="confirmBtn"></span></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
                </div>
            </div>
        </div>
    </div>

    <div id="smsModal" class="modal fade bs-modified-modal-sm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger ">문자 보내기</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2">
                            <p class="form-control-static text-right">전화번호</p>
                        </div>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="smstel" placeholder="전화번호를 입력하세요"  value="">
                        </div>
                        <div class="col-md-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <p class="form-control-static text-right">문자</p>
                        </div>
                        <div class="col-md-9">
                            <textarea   class="form-control" id="smsmessage" placeholder="문자내용을 입력하세요"></textarea>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="smsSubmit"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="smsconfirmBtn">문자보내기</span></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
                </div>
            </div>
        </div>
    </div>


    <div id="pointModal" class="modal fade bs-modified-modal-sm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger ">포인트 추가하기</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2">
                            <p class="form-control-static text-right">포인트</p>
                        </div>
                        <div class="col-md-9">
                            <input type="number" onkeypress="return isNumberKey(event)" class="form-control" id="point" placeholder="포인트를 입력하세요"  value="">
                        </div>
                        <div class="col-md-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <p class="form-control-static text-right">관련 사유</p>
                        </div>
                        <div class="col-md-9">
                            <textarea   class="form-control" id="pointmessage" placeholder="상담내용을 입력하세요"  /></textarea>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="pointSubmit"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="pconfirmBtn">포인트 추가</span></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
                </div>
            </div>
        </div>
    </div>


    <div  id="viewmodal" class="modal fade  " style="z-index: 3200">

        <div class="modal-dialog  modal-lg ">

            <div class="modal-content "  style="background-color: white;">

                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-danger viewtitle">포인트 내역 관리 </h4>

                </div>
                <div class="modal-body" style="background-color: white;">
                </div>

            </div>
        </div>
    </div>



    <script type="text/javascript">
        var tempidx="";
        var tempctype="";

        function showsmsmodal(){
            $("#smsModal").modal('show');
        }

        $("#smsSubmit").click(function() {
            var message = $("#smsmessage").val();
            var smstel = $("#smstel").val();

            if (smstel ==="") {
                alert("휴대폰 번호를 기입해주세요.");
                return ;
            }
            if (message ==="") {
                alert("상담내용을 기입해주세요.");
                return ;
            }
            $.ajax({
                type: "get",
                url: "/carmore/Smsproc?emode=sendsms&smstel="+smstel+"&message=" + message,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log("response:"+response);
                    $('#smsModal').modal('hide');
                    $("#smsmessage").val("");
                    $("#smstel").val("");
                }
            });

        });




        function setuserpoint(idx){
            tempidx=idx;tempctype="addpoint";
            $("#pointModal").modal().on("hidden.bs.modal", function() {
                $("#confirmBtn").text("");
                $(".modalTitle").text("");
                tempctype="";
                tempidx="";
            });
        }


        function setnormal(idx){

            if(confirm("회원 상태를 정상화 하시겠습니까?")){
                $.ajax({
                    type: "get",
                    url: "/carmore/CarmoreBoardProc?emode=setcomplainajax&usrserial="+idx+"&board_part=member" +
                    "&workmode=normal",
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        console.log("response:"+response);
                        $('#myModal').modal('hide');
                        $("#message").val("");
                        tempctype="";
                        tempidx="";
                        location.reload();
                    }
                });
            }
        }


        function setuserstats(ctype,idx){
            tempidx=idx;
            tempctype=ctype;
            if(ctype==="block") {
                $("#confirmBtn").text("블락 하기");
                $(".modalTitle").text("블락 처리");
            }else if(ctype==="out") {
                $("#confirmBtn").text("탈퇴 하기");
                $(".modalTitle").text("탈퇴 처리");
            }else if(ctype==="etc") {
                $("#confirmBtn").text("상담 하기");
                $(".modalTitle").text("기타 상담");
            }

            $("#myModal").modal().on("hidden.bs.modal", function() {
                $("#confirmBtn").text("");
                $(".modalTitle").text("");
                tempctype="";
                tempidx="";
            });
        }

        $("#pointSubmit").click(function() {
            var message = $("#pointmessage").val();
            var valpoint = $("#point").val();
            var board_title="";

            board_title="고객 포인트 추가 ";
            if (valpoint ==="") {
                alert("포인트를 기입해주세요.");
                return ;
            }
            valpoint = parseInt(valpoint);
            if(valpoint > 1000000){
                alert("100만원 이상의 포인트를 기입할수 없습니다");
                return ;
            }
            if(valpoint < 100){
                alert("100원 이하의 포인트를 기입할수 없습니다");
                return ;
            }


            if (message ==="") {
                alert("상담내용을 기입해주세요.");
                return ;
            }
            $.ajax({
                type: "get",
                url: "/carmore/CarmoreBoardProc?emode=setcomplainajax&usrserial="+tempidx+"&board_part=point" +
                "&workmode=init&message=" + message+"&board_title="+board_title+"&valpoint="+valpoint ,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log("response:"+response);
                    $('#myModal').modal('hide');
                    $("#message").val("");
                    tempctype="";
                    tempidx="";
                    location.reload();
                }
            });

        });

        // complainSubmit

        $("#complainSubmit").click(function() {
            var message = $("#message").val();
            var board_title="";
            var board_part="";

            if(tempctype==="block") {
                board_title="고객 블락처리 상담";board_part="member";
            }else if(tempctype==="out"){
                board_title="고객 탈퇴처리 상담";board_part="member";
            }else if(tempctype==="etc"){
                board_title="고객 기타 상담";board_part="etc";
            }

            if (message ==="") {
                alert("상담내용을 기입해주세요.");
                return false;
            }else {


                $.ajax({
                    type: "get",
                    url: "/carmore/CarmoreBoardProc?emode=setcomplainajax&complain_yn=y&usrserial="+tempidx+"&board_part="+board_part +
                        "&workmode="+tempctype+"&message=" + message+"&board_title="+board_title ,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        console.log("response:"+response);
                        $('#myModal').modal('hide');
                        $("#message").val("");
                        tempctype="";
                        tempidx="";
                        location.reload();
                    }
                });

            }

        });



        $("#goSubmit").click(function() {
            var message = $("#message").val();
            var board_title="";
            var board_part="";

            if(tempctype==="block") {
                board_title="고객 블락처리 상담";board_part="member";
            }else if(tempctype==="out"){
                board_title="고객 탈퇴처리 상담";board_part="member";
            }else if(tempctype==="etc"){
                board_title="고객 기타 상담";board_part="etc";
            }

            if (message ==="") {
                alert("상담내용을 기입해주세요.");
                return false;
            }else {


                $.ajax({
                    type: "get",
                    url: "/carmore/CarmoreBoardProc?emode=setcomplainajax&usrserial="+tempidx+"&board_part="+board_part +
                    "&workmode="+tempctype+"&message=" + message+"&board_title="+board_title ,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        console.log("response:"+response);
                        $('#myModal').modal('hide');
                        $("#message").val("");
                        tempctype="";
                        tempidx="";
                        location.reload();
                    }
                });

            }

        });


        function setstorememstats(stats){
            var checkcnt = $("input:checkbox[name='memidinfo[]']:checked").length;
            if(checkcnt <1){
                alert("선택된 회원 정보가 없습니다.");
                return;
            }
            var confirmtxt ="";
            if(stats ==="y"){
                confirmtxt ="선택된 회원의 상태를 정상으로 변경하시겠습니까?";
            }else{
                confirmtxt ="선택된 회원의 상태를 차단으로 변경하시겠습니까?";
            }

            if(confirm(confirmtxt)){
                document.updateConfirm.stats.value=stats;
                document.updateConfirm.action="/coinbackapp/CoinbackMemberProc";
                document.updateConfirm.submit();
            }
        }


        function delmember(){
            var confirmtxt   ="선택된 회원을 삭제 하시겠습니까?";

            var checkcnt = $("input:checkbox[name='memidinfo[]']:checked").length;
            if(checkcnt <1){
                alert("선택된 회원 정보가 없습니다.");
                return;
            }

            if(confirm(confirmtxt)){
                document.updateConfirm.emode.value='delmember';
                document.updateConfirm.action="/coinbackapp/CoinbackMemberProc";
                document.updateConfirm.submit();
            }
        }



        function viewcontentmodal(emode,userserial){
            var url="";

            if(emode==="pointout"){
                url ="/carmore/PointLogManage?ptype=m&sp=a.usrserial&sv="+userserial ;
                $(".viewtitle").text("포인트 내역관리");
            }else if(emode==="toporder"){
                url ="/carmore/ReservationManage?ptype=pop&sf_paystatus=&pmode=all&sp=a.f_usrserial&sv="+userserial ;
                $(".viewtitle").text("최근 주문 내역관리");
            }else if(emode==="buy"){
                url ="/carmore/ReservationManage?ptype=pop&sf_paystatus=1&pmode=buy&sp=a.f_usrserial&sv="+userserial ;
                $(".viewtitle").text("결제주문 내역관리");
            }else if(emode==="cancel"){
                url ="/carmore/ReservationManage?ptype=pop&sf_paystatus=2&pmode=cancel&sp=a.f_usrserial&sv="+userserial ;
                $(".viewtitle").text("취소주문 내역관리");
            }else if(emode==="coupon"){
                url ="/carmore/CouponLogManage?ptype=pop&sp=a.user_serial&sv="+userserial ;
                $(".viewtitle").text("쿠폰 내역관리");
            }


            $.ajax({
                url: url,
                type: 'GET',
                cache: false,
            }).done(function(result){

                $('#viewmodal .modal-body').html(result)
                $('#viewmodal').modal('show')
            });
        }
        function closeviewmodal(){
            $("#viewmodal").modal("hide");
        }



    </script>
</div>




</body>
</html>
