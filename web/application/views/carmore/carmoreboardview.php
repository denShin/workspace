
<div class="container-fluid" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>

    <div class="page-header">
        <h2>고객 상담 문의</h2>
    </div>

    <div class="row">

        <div class="col-md-8" style="overflow-x: hidden; overflow-y: scroll;height: 700px ">
        <fieldset>

        <form id="inputform" name="af" class="form-horizontal" role="form" method="post"  action="/carmore/CarmoreBoardProc">
        <input type="hidden" name="emode" id="emode" value="<?=$data["emode"]?>">
        <input type="hidden" name="board_code" value="complain">
        <input type="hidden" name="board_idx" value="<?=$data["board_idx"]?>">
        <input type="hidden" name="content_code" value="<?=$data["content_code"]?>">


        <div class="form-group">
        <label for="boardTitle" class="col-md-2 control-label">제목</label>
        <div class="col-md-10">
        <p class="form-control-static"><?=$data["board_title"]?></p>
        </div>
        </div>
        <hr>
        <div class="form-group">
        <label for="boardTitle" class="col-md-2 control-label">작성일</label>
        <div class="col-md-2">
        <p class="form-control-static"><?=$data["regdate"]?></p>
        </div>
        <label for="boardTitle" class="col-md-2 control-label">작성자</label>
        <div class="col-md-2">
        <p class="form-control-static"><?=$data["mem_name"]?></p>
        </div>
        <label for="boardTitle" class="col-md-2 control-label">고객</label>
        <div class="col-md-2">
        <p class="form-control-static"><?=$data["kakaonickname"]?></p>
        </div>
        </div>

        <hr>
        <div class="form-group">
        <label for="boardText" class="col-md-2 control-label">내용</label>
        <div class="col-md-10">
        <p class="form-control-static"><?=$data["board_content"]?></p>
        </div>
        </div>
        <div class="form-group">
        <label for="admin_pwd" class="col-md-2 control-label">처리상태</label>
        <div class="col-md-2">
        <select name="board_stats" class="form-control">
        <option value="n" <?if($data["board_stats"]=="n") echo "selected"; ?> >처리대기</option>
        <option value="y"  <?if($data["board_stats"]=="y") echo "selected"; ?> >처리완료</option>
        </select>
        </div>
        <div class="col-md-2">
        <button type="button" onclick="changestats()" class="btn btn-danger btn-block">상태 변경</button>
        </div>
        </div>
        <br/>

        <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
        <div class="row">

        <div class="col-md-4">
        <button type="button" onclick="location.href='?ptype=w&board_idx=<?=$data["board_idx"]?>&board_code=<?=$data["board_code"]?>&regdate=<?=$data["regdate"]?>'" class="btn btn-primary btn-block">수정하기</button>
        </div>
        <div class="col-md-4">
        <button type="button" class="btn btn-danger btn-block" id="deleteBtn">삭제하기</button>
        </div>

        <div class="col-md-4">
        <button type="button" class="btn btn-success btn-block" style="background-color: #aaa; border-color: #aaa;" onclick="location.href='/carmore/CarmoreBoard?board_code=<?=$data["board_code"]?>'" >목록가기</button>
        </div>
        </div>

        </div>
        </div>

        <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
        <div class="row">
        <div class="col-md-12">
        <style>
        #adminReply, .adminReply, .adminReplyInsertBtn { margin-top:10px;}
        .adminReplyRow { padding:7px 0 7px 0;}
        .gi-2x{font-size: 2em;}
        .gi-3x{font-size: 3em;}
        .gi-4x{font-size: 4em;}
        .gi-5x{font-size: 5em;}
        .replyContent {overflow:hidden; word-wrap:break-word;}
        </style>
        </div>
        </div>
        </div>
        </div>
        </form>
        </fieldset>
        </div>

        <div class="col-md-4" style="overflow-x: hidden; overflow-y: scroll;height: 700px ">
            <div class="row" style="padding-top: 15px">
                <form name="comaf" method="post" action="/carmore/CarmoreBoardProc">
                    <input name="emode" type="hidden" value="commentproc">
                    <input name="datamode" type="hidden">
                    <input name="board_idx" type="hidden" value="<?=$data["board_idx"]?>">
                    <input name="comment_idx" type="hidden"  >
                    <input name="repage" type="hidden" value="ReservationManage">
                    <input name="f_reservationidx" type="hidden" value="<?=$data["f_reservationidx"]?>">


                    <div class="col-md-8">
                        <textarea   class="form-control"  name="commenttext" placeholder="관리자 코멘트를 남겨주세요"  style="width: 100%;height: 80px"  ><?=$data["carinfomst_memo"]?></textarea>
                    </div>
                    <div class="col-md-2">
                        <button type="button" onclick="sendcomment()" class="btn btn-primary btn-block">저장</button>
                    </div>
                </form>
            </div>
            <br>
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-10">코멘트</th>
                    <th class="text-center col-md-2">삭제</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $now_id = $data["now_id"];
                foreach($data["commentlist"] as $entry) {
                    $regdate = date("m/d H:i",strtotime($entry["regdate"]));
                    $writer_id = $entry["mem_id"];
                    $writer_name = $entry["writer_name"];

                    ?>
                    <tr class="row">

                        <td   class="small text-left"  >
                            <span style="font-size: 1.4em"><?=$entry["commenttext"]?></span><Br>
                            <span style="font-size: 0.8em"><?=$writer_name?> (<?=$regdate?>)</span>
                        </td>

                        <td  class="small text-center">
                            <?if($writer_id==$now_id){?>
                                <button type="button" class="btn btn-danger btn-sm" onclick="delcomment('<?=$entry["comment_idx"]?>')">
                                    <span class="glyphicon glyphicon-minus"></span>
                                </button>
                            <?}?>
                        </td>
                    </tr>
                <?}?>

                </tbody>
            </table>

        </div>

    </div>



        <script type="text/javascript">


            function sendcomment(){
                var form=document.comaf;
                if(form.commenttext.value===""){
                    alert('코멘트를 기입해주세요');
                    form.commenttext.focus();
                    return;
                }
                form.datamode.value="new";
                form.submit()
            }


            function delcomment(comment_idx){

                if(confirm('코멘트를 삭제하시겠습니까?')){
                    var form=document.comaf;
                    form.comment_idx.value=comment_idx;
                    form.datamode.value="del";
                    form.submit()
                }
            }



            function changestats(){
 

                if (!confirm("처리상태를 변경하시겠습니까?")) {
                    return false;
                }else{
                    $("#emode").val("changestats");
                    document.af.submit();
                }
            }

            $(function() {
                $("#deleteBtn").click(function() {
                    if (!confirm("정말 삭제하시겠습니까?")) {
                        return false;
                    }
                    $(location).attr('href','/carmore/CarmoreBoardProc?emode=del&board_code=<?=$data["board_code"]?>&board_idx=<?=$data["board_idx"]?>');
                });
            });
        </script>

