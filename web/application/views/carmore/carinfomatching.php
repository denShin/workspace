
<div class="container" style="padding-top: 10px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
        .redColor{
            background-color: #E60000;
        }
    </style>

    <div class="page-header clearfix">
        <h2 class="pull-left">차종 정보 매칭</h2>

    </div>
    <div class="row">
        <form id="searchCompanyNotice" class="form-horizontal" role="form" method="get" >

                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-2">
                            <select class="form-control" name="companyserial" id="companyserial" onchange="companychange(this.value,'branchserial')">
                                <option value="">==== 선택 ====</option>
                                <?=$data["select_company"]?>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="branchserial" id="branchserial" onchange="getcarlist()">
                                <option value="">==== 선택 ====</option>
                            </select>
                        </div>
                        <div class="pull-right">
                            <button type="button" onclick="savematching()" class="btn btn-primary btn-block">저장하기</button>
                        </div>
                    </div>
                </div>
        </form>
        <hr/>
    </div>


    <form id="updateConfirm">
        <div class="clearfix"></div>

        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">No</th>
                    <th class="text-center col-md-4">업체차종</th>
                    <th class="text-center col-md-7">카모아차종</th>
                </tr>
                </thead>
                <tbody  id="carlistbl">

                   


                </tbody>
            </table>
        </div>
    </form>
    <script language="JavaScript">


        function companychange(companyserial,targetid,initvalue="") {
            $("select[id='"+targetid+"'] option").remove();
            // $('select').children('option:not(:first)').remove();


            $("#"+targetid).append("<option value=''>==== 선택 ====</option>");
            $.ajax({
                type:"post",contentType: "application/json",
                url:"/partners/PartnerMemberBranch?ptype=code&companyserial="+companyserial,
                datatype: "json",
                success: function(data) {


                    var json = $.parseJSON(data);
                    for(var i=0;i < json.length ; i++) {

                        var row = json[i];
                        var branchname = row["branchName"];
                        var serial = row["serial"];
                        var carmore_normal_available  = row["carmore_normal_available"];
                        var carmore_month_available   = row["carmore_month_available"];

                        console.log("carmore_normal_available:"+carmore_normal_available+",carmore_month_available:"+carmore_month_available)
                        if(serial ===initvalue){
                            $("#"+targetid).append("<option value='"+serial+"' selected>"+branchname+"</option>");
                        }else{
                            $("#"+targetid).append("<option value='"+serial+"'>"+branchname+"</option>");
                        }

                    }
                },
                error: function(x, o, e) {

                }
            });
        }

        function brandchange(car_serial,brandselect){

            var typeobj=$("#cartypeselect"+car_serial);
            var modelobj=$("#modelselect"+car_serial);

            var cartypeoption =get_cartypeselect('');
            typeobj.html(cartypeoption)
            modelobj.html("<option value='' >==선택==</option>");
        }

        function cartypechange(car_serial,cartypeselect){
            var brandselect= $("#brandselect"+car_serial).val();

            var arrcartype = ["경형","소형","준중형","중형","대형","수입","RV","SUV"];
            var cartypeselectindex=0;

                switch(cartypeselect) {
                    case "경형":
                        cartypeselectindex=0;
                        break;
                    case "소형":
                        cartypeselectindex=1;
                        break;
                    case "준중형":
                        cartypeselectindex=2;
                        break;
                    case "중형":
                        cartypeselectindex=3;
                        break;
                    case "대형":
                        cartypeselectindex=4;
                        break;
                    case "수입":
                        cartypeselectindex=5;
                        break;
                    case "RV":
                        cartypeselectindex=6;
                        break;
                    case "SUV":
                        cartypeselectindex=7;
                        break;

                }
            getmstcarlist(brandselect,cartypeselectindex,car_serial,0)
        }

        function getcartypeflag(str){
            var carflag="";
            switch(str) {
                case "경형":
                    carflag=0;
                    break;
                case "소형":
                    carflag=1;
                    break;
                case "준중형":
                    carflag=2;
                    break;
                case "중형":
                    carflag=3;
                    break;
                case "대형":
                    carflag=4;
                    break;
                case "수입":
                    carflag=5;
                    break;
                case "RV":
                    carflag=6;
                    break;
                case "SUV":
                    carflag=7;
                    break;
            }
            return  carflag;
        }

        function getcarlist() {
            var companyserial= $("#companyserial").val();
            var branchserial= $("#branchserial").val();
            $("#carlistbl").html('');

            console.log("companyserial:"+companyserial+",branchserial:"+branchserial)
            $.ajax({
                type:"post",contentType: "application/json",
                url:"/carmore/Carinfomaster?ptype=code&companyserial="+companyserial+"&branchserial="+branchserial,
                datatype: "json",
                success: function(data) {


                    var json = $.parseJSON(data);
                    console.log("data:%o",json);
                    for(var i=0;i < json.length ; i++) {
                        var carbrand="",carmodel="", carType="";
                        var carmstbrand="",carmstmodel="", carmstType="";
                        var carType_flag="";

                        var row = json[i];
                        var car_serial = row["serial"];
                        carbrand = row["brand"];
                        carmodel = row["model"];
                        carType = row["carType"];

                        var rowcolor ="white";

                        var carinfo_idx= row["carinfo_idx"];
                        if(carinfo_idx){
                            carmstbrand = row["carinfomst_brand"];
                            carmstmodel = row["carinfomst_model"];
                            carmstType = row["carinfomst_type"];
                            carType_flag = carmstType;
                         }else{
                            carmstbrand = carbrand;
                            carmstmodel = carmodel;
                            carmstType = getcartypeflag(carType);
                            carType_flag = row["carType_flag"];
                            rowcolor =" #eae951";
                        }



                        var brandoption =get_brandselect(carmstbrand);
                        var cartypeoption =get_cartypeselect(carType_flag);



                        var mstmodeloption =getmstcarlist(carmstbrand,carmstType,car_serial,carinfo_idx);

                        var html="<tr class='row' style='background-color: "+rowcolor+"'>" +
                            "<td  class='small text-center'>"+car_serial+"<input type='hidden' name='carserial' value='"+car_serial+"'></td>" +
                            "<td   class='small text-center'  > "+carbrand+" "+carmodel+" / "+carType+"</td>" +
                            "<td   class='small text-center'> " +
                            "<div class='col-md-3'><select name='brandselect' id='brandselect"+car_serial+"'   onchange=brandchange('"+car_serial+"',this.value)  class=\"form-control\" >"+brandoption+"</select></div>" +
                            "<div class='col-md-3'><select name='cartypeselect' onchange=cartypechange('"+car_serial+"',this.value)  id='cartypeselect"+car_serial+"' class=\"form-control\" >"+cartypeoption+"</select></div>" +
                            "<div class='col-md-6'><select name='modelselect'  id='modelselect"+car_serial+"' class=\"form-control\" >"+mstmodeloption+"</select></div>" +
                            "</td></tr>";


                        $("#carlistbl").append(html);
                    }
                },
                error: function(x, o, e) {

                }
            });
        }

        function getmstcarlist(brandselect,cartypeselect,car_serial,mstidx) {

            // $carinfomst_brand,$carinfomst_type
            $("select[id='modelselect"+car_serial+"'] option").remove();
            var targetid ="modelselect"+car_serial;


            if(mstidx ==="" || !mstidx) mstidx=0;
            //  /carmore/Carinfomaster?ptype=mstcode&carinfomst_brand=기아&carinfomst_type=6
            //   http://workspace2.teamo2.kr/carmore/Carinfomaster?ptype=mstcode&carinfomst_brand=기아&carinfomst_type=중형
            console.log("/carmore/Carinfomaster?ptype=mstcode&carinfomst_brand="+brandselect+"&carinfomst_type="+cartypeselect );
            $.ajax({
                type:"post",contentType: "application/json",
                url:"/carmore/Carinfomaster?ptype=mstcode&carinfomst_brand="+brandselect+"&carinfomst_type="+cartypeselect,
                datatype: "json",
                success: function(data) {
                    var json = $.parseJSON(data);

                    $("#"+targetid).append("<option value='' >==선택==</option>");
                    for(var i=0;i < json.length ; i++) {

                        var row = json[i];
                        var carinfo_idx = row["carinfo_idx"];
                        var carinfomst_model = row["carinfomst_model"];

                        if(carinfo_idx ===mstidx){
                            $("#"+targetid).append("<option value='"+carinfo_idx+"' selected>"+carinfomst_model+"</option>");
                        }else{
                            $("#"+targetid).append("<option value='"+carinfo_idx+"'>"+carinfomst_model+"</option>");
                        }
                    }

                },
                error: function(x, o, e) {

                }
            });
        }

        function get_brandselect(str){


            var arrbrand = ["현대","쌍용","기아","르노삼성","삼성","한국지엠","중한","혼다","푸조","폭스바겐"
                ,"포르쉐","포드","캐딜락","지프","재규어","아우디","쉐보레","볼보","벤츠"
                ,"마세라티","레미탑","랜드로버","도요타","닛산","BMW","제네시스","렉서스","미니","시트로엥","쯔더우"];

            var alloption="<option value=''>==선택==</option>";
            for(var i=0;i < arrbrand.length ; i++){
                var brand = arrbrand[i];
                var html = "<option value='"+arrbrand[i]+"'" ;

                if(brand===str){
                    html =html+" selected";
                }
                html = html + ">"+brand+"</option>";
                alloption =alloption+html;
            }
            return alloption;
        }

        function get_cartypeselect(cartype_flag){

            console.log("cartype_flag:"+cartype_flag)
            var arrcartype = ["경형","소형","준중형","중형","대형","수입","RV","SUV"];

            var cartype_flag =parseInt(cartype_flag);

            var alloption="<option value=''>=선택=</option>";
            for(var i=0;i < arrcartype.length ; i++){
                var cartype = arrcartype[i];
                var html = "<option value='"+arrcartype[i] +"'" ;

                if(i===cartype_flag){
                    html =html+" selected";
                }
                html = html + ">"+cartype+"</option>";
                alloption =alloption+html;
            }
            return alloption;
        }


        function savematching(){
            // carserial  brandselect  cartypeselect modelselect
            var objcarserial =  $('[name="carserial"]');
            var objbrandselect =  $('[name="brandselect"]');
            var objcartypeselect =  $('[name="cartypeselect"]');
            var objmodelselect =  $('[name="modelselect"]');

            var rentCompany_serial =$("#companyserial").val();
            var rentCompany_branch_serial =$("#branchserial").val();

            var objlength =objcarserial.length;
            var saveparam=new Array();

            for(var i=0;i < objlength ;i++){
                var arrcarserial =objcarserial[i];
                var arrbrandselect =objbrandselect[i];
                var arrcartypeselect =objcartypeselect[i];
                var arrmodelselect =objmodelselect[i];

                var carserial =arrcarserial["value"];
                var brandselect =arrbrandselect["value"];
                var cartypeselect =arrcartypeselect["value"];
                var modelselect =arrmodelselect["value"];
                var totalvalue =carserial+":"+brandselect+":"+cartypeselect+":"+modelselect;

                if(carserial !==""){
                    saveparam.push(totalvalue);
                }
            }

            saveparam = saveparam.join("-");
            console.log("saveparam:%o",saveparam);

            //rentCompany_serial rentCompany_branch_serial
            //  http://workspace.teamo2.kr/carmore/CarinfomasterProc?emode=match&saveparam=1615:기아:경형:6
            $.ajax({
                type:"post",contentType: "application/json",
                url:"/carmore/CarinfomasterProc?emode=match&saveparam="+saveparam +"&rentCompany_serial="+rentCompany_serial+"&rentCompany_branch_serial="+rentCompany_branch_serial,
                datatype: "json",
                success: function(data) {


                    getcarlist();
                },
                error: function(x, o, e) {

                }
            });


        }
    </script>
