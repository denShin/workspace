
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>

    <div class="page-header">
        <h2>고객 상담 문의</h2>
    </div>


    <fieldset>

        <form id="inputform" name="af" class="form-horizontal" role="form" method="post"  action="/carmore/CarmoreBoardProc">
            <input type="hidden" name="emode" id="emode" value="<?=$data["emode"]?>">
            <input type="hidden" name="board_code" value="event">
            <input type="hidden" name="board_idx" value="<?=$data["board_idx"]?>">
            <input type="hidden" name="content_code" value="<?=$data["content_code"]?>">


            <div class="form-group">
                <label for="boardTitle" class="col-md-2 control-label">제목</label>
                <div class="col-md-10">
                    <p class="form-control-static"><?=$data["board_title"]?></p>
                </div>
            </div>

            <div class="form-group">
                <label for="boardTitle" class="col-md-2 control-label">시작/종료일</label>
                <div class="col-md-10">
                    <p class="form-control-static"><?=$data["startdate"]?> ~ <?=$data["enddate"]?> </p>
                </div>
            </div>


            <hr>
            <div class="form-group">
                <label for="boardText" class="col-md-2 control-label">내용</label>
                <div class="col-md-10">
                    <p class="form-control-static"><?=$data["board_content"]?></p>
                </div>
            </div>

            <br/>
            <div class="form-group">
                <label for="boardText" class="col-md-2 control-label">파일리스트</label>
                <div class="col-md-10">
                    <?php
                    foreach ($data["filelist"] as $files) {
                        ?>
                        <p class="form-control-static">
                           <img  src="https://s3.ap-northeast-2.amazonaws.com/carmoreweb/<?=$data["board_code"]?>/<?=$data["content_code"]?>/<?=$files["fileSaveName"]?>" width="150">
                            <br>

                            <?=$files["fileOrgName"]?> &nbsp;
                            &nbsp;&nbsp; <button type="button" class="btn btn-primary btn-xs" onclick="location.href='/adminmanage/FileDownload?board_code=<?=$data["board_code"]?>&board_contentcode=<?=$data["board_contentcode"]?>&sfilename=<?=$files["fileSaveName"]?>'">다운로드</button></a>
                            <A href="javascript:deletefile('<?=$files["fileSaveName"]?>')"><button type="button" class="btn btn-danger btn-xs">삭제하기</button></a> </p>
                    <?}?>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label for="admin_pwd" class="col-md-2 control-label">상태</label>
                <div class="col-md-2">
                    <select name="board_stats" class="form-control">
                        <option value="n" <?if($data["board_stats"]=="n") echo "selected"; ?> >Off</option>
                        <option value="y"  <?if($data["board_stats"]=="y") echo "selected"; ?> >On </option>
                    </select>
                </div>
                <div class="col-md-2">
                    <button type="button" onclick="changestats()" class="btn btn-danger btn-block">상태 변경</button>
                </div>
            </div>
            <br/>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">

                        <div class="col-md-4">
                            <button type="button" onclick="location.href='?ptype=w&board_idx=<?=$data["board_idx"]?>&board_code=<?=$data["board_code"]?>&content_code=<?=$data["content_code"]?>'" class="btn btn-primary btn-block">수정하기</button>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-danger btn-block" id="deleteBtn">삭제하기</button>
                        </div>

                        <div class="col-md-4">
                            <button type="button" class="btn btn-success btn-block" style="background-color: #aaa; border-color: #aaa;" onclick="location.href='/carmore/EventManage'" >목록가기</button>
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">
                        <div class="col-md-12">
                            <style>
                                #adminReply, .adminReply, .adminReplyInsertBtn { margin-top:10px;}
                                .adminReplyRow { padding:7px 0 7px 0;}
                                .gi-2x{font-size: 2em;}
                                .gi-3x{font-size: 3em;}
                                .gi-4x{font-size: 4em;}
                                .gi-5x{font-size: 5em;}
                                .replyContent {overflow:hidden; word-wrap:break-word;}
                            </style>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </fieldset>


        <script type="text/javascript">
            function deletefile(filename)
            {
                if ( confirm("삭제 하시겠습니까?")) {
                    location.href="/application/views/data/deletedropzone.php?board_code=<?=$data["board_code"]?>&content_code=<?=$data["content_code"]?>&board_idx=<?=$data["board_idx"]?>&fileSaveName="+filename+"&backurl=<?=$data["board_code"]?>&page=<?=$data["per_page"]?>";

                }
            }

            $(function() {
                $("#deleteBtn").click(function() {
                    if (!confirm("정말 삭제하시겠습니까?")) {
                        return false;
                    }
                    $(location).attr('href','/carmore/CarmoreBoardProc?emode=del&board_code=<?=$data["board_code"]?>&content_code=<?=$data["content_code"]?>&board_idx=<?=$data["board_idx"]?>');
                });
            });


            function changestats(){

                if (!confirm("처리상태를 변경하시겠습니까?")) {
                    return false;
                }else{
                    $("#emode").val("changestats");
                    document.af.submit();
                }
            }
        </script>

