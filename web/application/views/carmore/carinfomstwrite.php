
<script src="/static/lib/dropzone/dropzone.js"></script>
<link href="/static/lib/dropzone/dropzone.css"rel="stylesheet">
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">

    <style>
        .double-input .form-control {
            width: 50%;
        }
    </style>

    <div class="page-header">
        <h2>차종 정보 관리</h2>
    </div>



    <form id="inputform" class="form-horizontal" role="form" method="post" enctype="multipart/form-data"  action="/carmore/CarinfomasterProc">
        <input type="hidden" name="emode" value="<?=$data["emode"]?>">
        <input type="hidden" name="carinfokey" value="<?=$data["carinfokey"]?>">

        <fieldset>

            <div class="form-group">
                <label for="shareCategory" class="col-md-2 control-label">브랜드</label>
                <div class="col-md-5">
                    <select name="carinfomst_brand" class="form-control">

                        <option value="현대" <?if($data["carinfomst_brand"]=="현대"){ echo "selected";}?> >현대</option>
                        <option value="쌍용" <?if($data["carinfomst_brand"]=="쌍용"){ echo "selected";}?> >쌍용</option>
                        <option value="기아" <?if($data["carinfomst_brand"]=="기아"){ echo "selected";}?> >기아</option>
                        <option value="르노삼성" <?if($data["carinfomst_brand"]=="르노삼성"){ echo "selected";}?> >르노삼성</option>
                        <option value="삼성" <?if($data["carinfomst_brand"]=="삼성"){ echo "selected";}?> >삼성</option>
                        <option value="한국지엠" <?if($data["carinfomst_brand"]=="한국지엠"){ echo "selected";}?> >한국지엠</option>
                        <option value="중한" <?if($data["carinfomst_brand"]=="중한"){ echo "selected";}?> >중한</option>

                        <option value="혼다" <?if($data["carinfomst_brand"]=="혼다"){ echo "selected";}?> >혼다</option>
                        <option value="푸조" <?if($data["carinfomst_brand"]=="푸조"){ echo "selected";}?> >푸조</option>
                        <option value="폭스바겐" <?if($data["carinfomst_brand"]=="폭스바겐"){ echo "selected";}?> >폭스바겐</option>
                        <option value="포르쉐" <?if($data["carinfomst_brand"]=="포르쉐"){ echo "selected";}?> >포르쉐   </option>
                        <option value="포드" <?if($data["carinfomst_brand"]=="포드"){ echo "selected";}?> >포드</option>
                        <option value="캐딜락" <?if($data["carinfomst_brand"]=="캐딜락"){ echo "selected";}?> >캐딜락   </option>
                        <option value="지프" <?if($data["carinfomst_brand"]=="지프"){ echo "selected";}?> >지프</option>
                        <option value="재규어" <?if($data["carinfomst_brand"]=="재규어"){ echo "selected";}?> >재규어   </option>
                        <option value="아우디" <?if($data["carinfomst_brand"]=="아우디"){ echo "selected";}?> >아우디   </option>
                        <option value="쉐보레" <?if($data["carinfomst_brand"]=="쉐보레"){ echo "selected";}?> >쉐보레   </option>
                        <option value="볼보" <?if($data["carinfomst_brand"]=="볼보"){ echo "selected";}?> >볼보</option>
                        <option value="벤츠" <?if($data["carinfomst_brand"]=="벤츠"){ echo "selected";}?> >벤츠</option>
                        <option value="마세라티" <?if($data["carinfomst_brand"]=="마세라티"){ echo "selected";}?> >마세라티</option>
                        <option value="레미탑" <?if($data["carinfomst_brand"]=="레미탑"){ echo "selected";}?> >레미탑   </option>
                        <option value="랜드로버" <?if($data["carinfomst_brand"]=="랜드로버"){ echo "selected";}?> >랜드로버</option>
                        <option value="도요타" <?if($data["carinfomst_brand"]=="도요타"){ echo "selected";}?> >도요타   </option>
                        <option value="닛산" <?if($data["carinfomst_brand"]=="닛산"){ echo "selected";}?> >닛산</option>
                        <option value="BMW" <?if($data["carinfomst_brand"]=="BMW"){ echo "selected";}?> >BMW   </option>

                        <option value="제네시스" <?if($data["carinfomst_brand"]=="제네시스"){ echo "selected";}?> >제네시스   </option>
                        <option value="렉서스" <?if($data["carinfomst_brand"]=="렉서스"){ echo "selected";}?> >렉서스   </option>
                        <option value="미니" <?if($data["carinfomst_brand"]=="미니"){ echo "selected";}?> >미니   </option>
                        <option value="시트로엥" <?if($data["carinfomst_brand"]=="시트로엥"){ echo "selected";}?> >시트로엥   </option>
                        <option value="쯔더우" <?if($data["carinfomst_brand"]=="쯔더우"){ echo "selected";}?> >쯔더우   </option>

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">모델 명</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" name="carinfomst_model" placeholder="모델명"  value="<?=$data["carinfomst_model"]?>"   autofocus/>
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">타입</label>
                <div class="col-md-5">
                    <select name="carinfomst_type" class="form-control">
                        <option value="0" <?if($data["carinfomst_type"]=="0"){ echo "selected";}?> >경형</option>
                        <option value="1" <?if($data["carinfomst_type"]=="1"){ echo "selected";}?> >소형</option>
                        <option value="2" <?if($data["carinfomst_type"]=="2"){ echo "selected";}?> >준중형</option>
                        <option value="3" <?if($data["carinfomst_type"]=="3"){ echo "selected";}?> >중형</option>
                        <option value="4" <?if($data["carinfomst_type"]=="4"){ echo "selected";}?> >대형</option>
                        <option value="5" <?if($data["carinfomst_type"]=="5"){ echo "selected";}?> >수입</option>
                        <option value="6" <?if($data["carinfomst_type"]=="6"){ echo "selected";}?> >RV</option>
                        <option value="7" <?if($data["carinfomst_type"]=="7"){ echo "selected";}?> >SUV</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">최소/최대 연비</label>
                <div class="col-md-3">
                    <input type="text" class="form-control" name="min_fuel_efficiency" placeholder="최소연비" maxlength="5"  value="<?=$data["min_fuel_efficiency"]?>"   autofocus/>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" name="max_fuel_efficiency" placeholder="최대연비"  maxlength="5"  value="<?=$data["max_fuel_efficiency"]?>"   autofocus/>
                </div>
            </div>
            <div class="form-group">
                <label for="shareContent" class="col-md-2 control-label">차량설명</label>
                <div class="col-md-10">
                    <textarea   class="form-control"  name="carinfomst_memo"  style="width: 60%;height: 80px"  ><?=$data["carinfomst_memo"]?></textarea>
                </div>
            </div>
            <hr>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary btn-block">저장하기</button>
                        </div>
                        <div class="col-md-6">
                            <button type="reset" class="btn btn-warning btn-block">다시작성</button>
                        </div>
                    </div>

                </div>
            </div>
        </fieldset>
    </form>

    <script type="text/javascript">

        $(function() {
            $("#inputform").submit(function() {

                if ($("input[name='carinfomst_model']").val() =="") {
                    alert("모델명을 입력하세요.");
                    $("input[name='carinfomst_model']").focus();
                    return false;
                }

                if ($("input[name='min_fuel_efficiency']").val() =="") {
                    alert("최소연비를 입력하세요.");
                    $("input[name='min_fuel_efficiency']").focus();
                    return false;
                }

                if ($("input[name='max_fuel_efficiency']").val() =="") {
                    alert("최대연비를 입력하세요.");
                    $("input[name='max_fuel_efficiency']").focus();
                    return false;
                }

                if (!confirm("정말 등록하시겠습니까?")) {

                    return false;
                }
            });

        });


    </script></div>
