<div class="container" style="padding-top: 20px;padding-bottom: 10px;">
    <form id="updateConfirm">
        <input type="hidden" name="isConfirm" value="" form="updateConfirm"/>
        <input type="hidden" name="emode" value="agree_yn" form="updateConfirm"/>

        <div class="clearfix"></div>
        <form name="updateConfirm" method="post">
            <input type="hidden" name="emode" value="changecashstats">
            <input type="hidden" name="stats" >
            <input type="hidden" name="coinbuyoffer_idx" >
            <input type="hidden" name="targetprice" >
            <input type="hidden" name="chargefee" >
            <input type="hidden" name="buy_nowprice" >
            <input type="hidden" name="mem_id" >

            <div class="table-responsive">
                <table class="table table-striped table-hover table-bordered" style="width: 800px">
                    <thead>
                    <tr class="info row">
                        <th class="text-center col-md-3">포인트 정보</th>
                        <th class="text-center col-md-3">닉네임</th>
                        <th class="text-center col-md-3">포인트 </th>
                        <th class="text-center col-md-3">상태 </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $startnum =$data["startnum"];
                    foreach($data["list"] as $entry) {
                        ?>
                        <tr class="text-center row">
                            <td class="small text-left" > <b><?= $entry["pointmsg"] ?></b> /<?=$entry["logmsg"]?>  </td>
                            <td class="small" ><?=$entry["kakaonickname"]?>  </td>
                            <td class="small"><?= $entry["valpointstr"]?> p
                                <?if($entry["pointtype"]=="4"){?>
                                    <span class="label label-danger " style="cursor: pointer"  onclick="setpointoutstats('back','<?= $entry["pointsrl"]?>','<?= $entry["usrserial"]?>','<?= $entry["valpoint"]?>')">회수</span>
                                <?}?>
                            </td>
                            <td class="small">
                                <span style="color: <?=$entry["pointtypecolor"]?>"> <?= $entry["pointtypestr"] ?></span>
                            </td>
                        </tr>
                        <?php
                        $startnum--;
                    }
                    ?>

                    </tbody>
                </table>
            </div>
            <div style="padding-bottom: 150px"></div>

        </form>

        <!--* modal start *-->
        <div id="pointoutModal" class="modal fade bs-modified-modal-sm"  style="z-index: 3300;min-width: 100%;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header ">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-danger modalTitle">포인트 회수 처리</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="form-control-static text-right">코드선택</p>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" id="pointtype">
                                    <option value="11" >운영자 회수</option>
                                    <option value="10" >회원 탈퇴 회수</option>

                                </select>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <p class="form-control-static text-right">관련 사유</p>
                            </div>
                            <div class="col-md-9">
                                <textarea   class="form-control" id="pointoutmessage" placeholder="관련사유를 입력하세요"  /></textarea>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="gopointoutSubmit"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="confirmBtn">회수 하기</span></button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
                    </div>
                </div>
            </div>
        </div>


        <script type="text/javascript">
            var tempuidx="";
            var temppidx="";
            var tempctype="";
            var valpoint="";

            function setpointoutstats(ctype,pidx,uidx,point){
                temppidx=pidx;
                tempuidx=uidx;
                tempctype=ctype;
                valpoint=point;

               // $("#viewmodal").modal("hide");

                $("#pointoutModal").modal("show");
            }


            $("#gopointoutSubmit").click(function() {
                var message = $("#pointoutmessage").val();
                var pointtype=$("#pointtype").val();
                var board_title="";
                var board_part="";
                var board_part="point";

                board_title="포인트 회수 처리";
                confirmtext="포인트를 회수 하시겠습니까?";

                if (message =="") {
                    alert("상담내용을 기입해주세요.");
                    return false;
                }else {

                    if(!confirm(confirmtext)){
                        return;
                    }

                    $.ajax({
                        type: "get",
                        url: "/carmore/CarmoreBoardProc?emode=setcomplainajax&usrserial="+tempuidx+"&board_part="+board_part +
                            "&workmode="+tempctype+"&message=" + message+"&board_title="+board_title+"&pointsrl="+temppidx
                            +"&pointtype="+pointtype+"&valpoint="+valpoint ,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (response) {
                            console.log("response:"+response);
                            $('#pointoutModal').modal('hide');
                            $("#pointoutmessage").val("");
                            tempctype="";
                            tempfidx="";
                            tempuidx="";
                            valpoint="";
                            location.reload();
                        }
                    });

                }

            });



        </script>
</div>

</body>
</html>
