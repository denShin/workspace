
<script src="/static/lib/dropzone/dropzone.js"></script>
<link href="/static/lib/dropzone/dropzone.css"rel="stylesheet">
<div class="container" style="padding-top: 0px;padding-bottom: 70px;">

    <style>
        .double-input .form-control {
            width: 50%;
        }
    </style>


    <script language="JavaScript">
        $(function() {
            <?php


            foreach($data["list"] as $entry) {

                $fullurl = $entry["fullurl"];
                $fileSaveName = $entry["fileSaveName"];
                $publish_ynstr = $entry["publish_ynstr"];
                echo "add_sublist('".$fileSaveName."','".$publish_ynstr."');";
            }

            ?>

        });


        function add_sublist(filename,publish_ynstr){
            var imageurl = "https://s3.ap-northeast-2.amazonaws.com/carmoreweb/carmst/<?=$data["content_code"]?>/"+filename;
            var rowhtml =$("#subimagerow").html();
            var imgid = filename.replace(".","");

            if(publish_ynstr==="배포대기"){
                publish_ynstr="<span style='color:red'>배포대기</span>";

            }

            rowhtml = rowhtml.replace(/\{imageurl}/gi, imageurl);
            rowhtml = rowhtml.replace(/\{publish_ynstr}/gi, publish_ynstr);
            rowhtml = rowhtml.replace(/\{filename}/gi, filename);
            rowhtml = rowhtml.replace(/\{imgid}/gi, imgid);

            $("#subimagelist").append(rowhtml);
        }


    </script>



    <div id="subimagerow" style="display: none">

        <div class="col-md-3" id="{imgid}">
            <div class="thumbnail">
                <img  src="{imageurl}"  id="subimage{imgid}" width="100"    alt="">
                <div class="caption">
                    <h5 id="publish_yn{imgid}">{publish_ynstr} </h5>

                    <p><a href="#" class="btn btn-primary btn-sm" role="button"  onclick="publishsubimage('{filename}')">배포하기</a>
                        <a href="#" class="btn btn-default btn-sm" role="button" onclick="delcarsubimage('{filename}')">삭제하기</a></p>
                </div>
            </div>
        </div>



    </div>




    <fieldset>
        <div class="col-md-10">
            <label for="shareTitle" class="col-md-7 control-label">서브 이미지</label>

        </div>
        <div class="clearfix" style="padding-top: 10px"></div>
        <div class="col-md-10">

            <div id="subimagelist">

            </div>

        </div>
        <div class="clearfix" style="padding-top: 40px"></div>
        <form id="inputform" class="form-horizontal" role="form" method="post" enctype="multipart/form-data" >
            <br>
            <div class="col-md-10">

                <div class="col-md-10">
                    <div class="dropzone dz-clickable" id="myDrop">
                        <div class="dz-default dz-message" data-dz-message="">
                            <span>여기에 드래그앤드랍하세요</span>
                        </div>
                    </div>
                </div>
            </div>

            <br/>

            <div id="fileOrgName"></div>
            <div id="fileSaveName"></div>
            <div id="fileDir"></div>
            <div id="fileSize"></div>
            <div id="fileType"></div>
        </form>

    </fieldset>


    <script type="text/javascript">


        function setimage(targetid,filename){
            $("#"+targetid).attr('src','https://s3.ap-northeast-2.amazonaws.com/carmoreweb/carmst/<?=$data["content_code"]?>/'+filename);

        }

        function publishsubimage(filename) {


            if(filename==="default-no-car-pic.png"){
                return ;
            }
            var imgid = filename.replace(".","");

            if (confirm("이미지를 배포 하시겠습니까?")) {



                //  http://workspace2.teamo2.kr//carmore/CarinfomasterProc?emode=imagepublish&publish_yn=y&filename=20180927053129_716_CARMST_603.png
                console.log("/carmore/CarinfomasterProc?emode=imagepublish&publish_yn=y&filename="+filename)
                $.ajax({
                    type: "get",
                    url: "/carmore/CarinfomasterProc?emode=imagepublish&publish_yn=y&filename="+filename,
                    success: function (response) {
                        console.log("response:"+response);
                        $("#publish_yn"+imgid).text('배포완료');
                    },
                    error:function(request,status,error){
                        console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
                    },
                    complete:  function() {
                        console.log("complete");

                    }

                });
            }
        }


        function delcarsubimage(filename){


            if(confirm("*선택한 이미지를 삭제하시겠습니까?\n한번 삭제하시면 복구할 수 없습니다.")){

                var imgid = filename.replace(".","");

                $.ajax({
                    type: "get",
                    url: "/application/views/data/deletedropzone.php?board_code=carmst&content_code=carmst&deltype=modal&fileSaveName="+filename,
                    success: function (response) {
                        console.log("response:"+response);
                        $("#"+imgid).hide();
                    },
                    error:function(request,status,error){
                        console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
                    },
                    complete:  function() {
                        console.log("complete");

                    }

                });
            }



        }



        var myDropZone;
        $(document).ready(function() {
            Dropzone.autoDiscover = false;
            myDropZone = new Dropzone("#myDrop", {
                url : "/application/views/data/uploaddropzone.php",
                addRemoveLinks : true,
                maxFilesize: 100,
                maxFiles:5,
                acceptedFiles: 'image/*',
                autoProcessQueue : true,
                paramName: "file1234",
                parallelUploads : 100,
                sending:function(file, xhr, formData){
                    formData.append('board_code', 'carmst');
                    formData.append('fileuptype', 'sub');
                    formData.append('content_code', '<?=$data["content_code"]?>');
                    formData.append('mem_id', '');
                },
                success: function( file, response ) {

                    obj = JSON.parse(response);

                    file.fileDir = obj.fileDir;
                    file.fileSaveName = obj.fileSaveName;
                    file.fileidx = obj.uploadfile_infoidx;

                    var input1 = $("<input>");
                    input1.attr({"type" : "hidden", "name" : "fileOrgName[]", "value" : obj.fileOrgName});
                    var input2 = $("<input>");
                    input2.attr({"type" : "hidden", "name" : "fileSaveName[]", "value" : obj.fileSaveName});
                    var input4 = $("<input>");
                    input4.attr({"type" : "hidden", "name" : "fileSize[]", "value" : obj.fileSize});
                    var input5 = $("<input>");
                    input5.attr({"type" : "hidden", "name" : "fileType[]", "value" : obj.fileType});

                    $("#fileOrgName").append(input1);
                    $("#fileSaveName").append(input2);
                    $("#fileSize").append(input4);
                    $("#fileType").append(input5);


                    add_sublist(obj.fileSaveName,'배포대기');
                },

                removedfile: function(file) {

                    if (file.fileSaveName) {
                        $.post("/application/views/data/deletedropzone.php?board_code=carmst&content_code=<?=$data["content_code"]?>&fileSaveName="+file.fileSaveName);
                        $("." + file.fileSaveName.replace('.', '')).remove();
                    }
                    var _ref;
                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                }
            });

        });






    </script></div>

</body>
</html>
