
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>

    <div class="page-header clearfix">
        <h2 class="pull-left">차종 정보 관리</h2>
        <div class="pull-right" style="padding-top: 30px">
            <a href="?ptype=w&board_code=<?=$data["board_code"]?>" class="btn btn-info">등록하기</a>
        </div>
    </div>
    <div id="searchdiv">
        <form id="searchCompanyNotice" class="form-horizontal" role="form" method="get" >
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <select class="form-control" name="sp">
                                <option value="brand" >브랜드</option>
                                <option value="cartype" >타입</option>
                                <option value="model" >모델명</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="sv" value="">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </fieldset>
        </form>
        <hr/>
    </div>


    <form id="updateConfirm">
        <div class="clearfix"></div>

        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">No</th>
                    <th class="text-center col-md-1">제조사</th>
                    <th class="text-center col-md-1">차량등급</th>
                    <th class="text-center col-md-2">차종이름</th>
                    <th class="text-center col-md-2">차량설명</th>
                    <th class="text-center col-md-1">연비</th>
                    <th class="text-center col-md-3">사진</th>
                    <th class="text-center col-md-1">관리</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($data["list"] as $entry) {
                    ?>
                    <tr class="row">
                        <td  class="small text-center"><?=$entry["serial"]?> </td>
                        <td   class="small text-center"  ><?=$entry["brand"]?></td>
                        <td   class="small text-center"><?=$entry["carType"]?> </td>
                        <td  style="padding-left: 10px;align:left"  class="small">  <a href="?ptype=w&serial=<?=$entry["serial"]?>&per_page=<?=$data["per_page"]?>"><?=$entry["model"]?></a>  </td>
                        <td   class="small text-center">차량설명 </td>
                        <td   class="small text-center"><?=$entry["min_fuel_efficiency"]?> /<?=$entry["max_fuel_efficiency"]?> </td>
                        <td   class="small text-center">사진 </td>
                        <td   class="small text-center">관리</td>
                    </tr>
                <?}?>

                </tbody>
            </table>
        </div>
    </form>


    <div class="text-center">
        <?=$data['pagination']?>
    </div>
