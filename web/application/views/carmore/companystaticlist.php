
<div class="container" style="padding-top: 30px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawSeriesChart);

        function drawSeriesChart() {

            var data = google.visualization.arrayToDataTable(<?=$data["chartdata"]?>);
            var options = {
                title: '<?=$data["graphtitle"]?>',
                hAxis: {title: '매출액'},
                vAxis: {title: '계약건수'},
                bubble: {textStyle: {fontSize: 11}}
            };

            var chart = new google.visualization.BubbleChart(document.getElementById('series_chart_div'));
            chart.draw(data, options);
        }

        function set_dateinfo(datepart){
            var enddate="<?=$data["today"]?>";
            var startdate="";

            if(datepart==="1week"){
                startdate="<?=$data["week1"]?>";
            }else if(datepart ==="1month"){
                startdate="<?=$data["month1"]?>";
            }else if(datepart ==="3month"){
                startdate="<?=$data["month3"]?>";
            }else if(datepart ==="6month"){
                startdate="<?=$data["month6"]?>";
            }else if(datepart ==="1year"){
                startdate="<?=$data["year1"]?>";
            }

            $("#startdate").val(startdate);
            $("#enddate").val(enddate);

            document.af.submit();

        }
        function sendorderby(){
            var orderby=$("#pageorderby").val();
            document.af.orderby.value=orderby;
            document.af.submit();

        }

    </script>
    <div class="page-header clearfix">
        <h2 class="pull-left"><?=$data["pagetitle"]?> 정보</h2>

    </div>

    <div class="text-center">

            <form class="form-inline" name="af" method="get" action="?">
                <input type="hidden" name="orderby">
                <input type="date"class="form-control" name="startdate"  id="startdate" value="<?=$data["startdate"]?>" placeholder="시작월">~
                <input type="date" class="form-control"  name="enddate" id="enddate"   value="<?=$data["enddate"]?>"  placeholder="마감월">

                <button type="submit" class="form-control" >검색</button>

                <div class="row">
                    <button type="button" onclick="set_dateinfo('1week')" class="btn btn-link form-control" >1주일</button>
                    <button type="button" onclick="set_dateinfo('1month')"  class="btn btn-link form-control" >1개월</button>
                    <button type="button" onclick="set_dateinfo('3month')"  class="btn btn-link form-control" >3개월</button>
                    <button type="button" onclick="set_dateinfo('6month')"  class="btn btn-link form-control" >6개월</button>
                    <button type="button" onclick="set_dateinfo('1year')"  class="btn btn-link form-control" >1년</button>
                </div>
            </form>

    </div>

    <div class="text-center">

        <div id="series_chart_div" style="width: 1200px; height: 500px;"></div>
    </div>

     <div class="clearfix"></div>

    <div class="pull-right">
        <select class="form-control" name="pageorderby"  id="pageorderby" onchange="sendorderby()">
            <option value="companyname-asc" <?if($data["pageorderby"]=="companyname-asc"){echo "selected";}?>  >회사명 가나다순</option>
            <option value="totalcount-desc" <?if($data["pageorderby"]=="totalcount-desc"){echo "selected";}?>   >계약건수 많은 순</option>
            <option value="totalcount-asc" <?if($data["pageorderby"]=="totalcount-asc"){echo "selected";}?>   >계약건수 적은 순</option>
            <option value="totalprice-desc" <?if($data["pageorderby"]=="totalprice-desc"){echo "selected";}?>   >매출액 많은 순</option>
            <option value="totalprice-asc" <?if($data["pageorderby"]=="totalprice-asc"){echo "selected";}?>   >매출액 적은 순</option>
            <option value="company_regdate-asc" <?if($data["pageorderby"]=="company_regdate-asc"){echo "selected";}?>   >가입일 빠른 순</option>
            <option value="company_regdate-desc" <?if($data["pageorderby"]=="company_regdate-desc"){echo "selected";}?>   >가입일 느린 순</option>

        </select>
    </div>
    <div class="clearfix" style="padding-top: 40px"></div>

    <form id="updateConfirm">

        <div class="table-responsive">
            <table class="table  table-hover table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-3">회사명</th>
                    <th class="text-center col-md-3">계약건수</th>
                    <th class="text-center col-md-3">매출액</th>
                    <th class="text-center col-md-3">가입일</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $startnum = $data["startnum"];
                foreach($data["list"] as $entry) {

                    ?>
                    <tr class="row">
                        <td  class="small text-center"><?=$entry["companyname"]?> </td>
                        <td  class="small text-center"><?=$entry["totalcount"]?> 건</td>
                        <td  class="small text-center"><?=$entry["totalprice"]?> 원</td>
                        <td  class="small text-center"><?=$entry["company_regdate"]?> </td>
                    </tr>
                    <?
                    $startnum--;
                }?>

                </tbody>
            </table>
        </div>
    </form>
