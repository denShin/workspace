<?php
$inventory   = $data['inventory'];
$get_moid    = $this->input->get('moid');
$get_rec_num = $this->input->get('recNum');
?>

<div class="container workspace-basic-container col-md-12">
    <div class="page-header clearfix">
        <h3 class="pull-left">비즈톡 전송목록</h3>
    </div>

    <div class="clearfix"></div>

    <div class="col-md-12" style="margin-bottom: 1em">
        <div class="col-md-5 form-group">
            <label>예약번호</label>
            <div class="col-md-12">
                <input type="text" class="form-control" id="bi_reserv_input" value="<?=$get_moid?>"/>
            </div>

        </div>
        <div class="col-md-5">
            <label>수신번호</label>
            <div class="col-md-12">
                <input type="text" class="form-control" id="bi_rec_num_input" value="<?=$get_rec_num?>"/>
            </div>

        </div>
        <div class="col-md-2">
            <label>&nbsp;</label>
            <div class="col-md-12">
                <button type="button" class="btn btn-default form-control" id="bi_search_btn"><span class="glyphicon glyphicon-search"></span></button>
            </div>
        </div>
    </div>

<?php
if (empty($inventory))
{

?>

    <div class="col-md-12 text-center" style="font-weight: bold; font-size: 120%">
        검색조건에 맞는 비즈톡 목록이 존재하지 않습니다.<br>
        검색조건을 다시 확인해주세요

    </div>

<?php
}
else
{

?>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="col-md-1 info text-center">예약번호</th>
                        <th class="col-md-1 info text-center">전송요청일</th>
                        <th class="col-md-1 info text-center">수신번호</th>
                        <th class="col-md-4 info text-center">내용</th>
                        <th class="col-md-1 info text-center">상태</th>
                        <th class="col-md-1 info text-center">전송결과코드</th>
                        <th class="col-md-1 info text-center">전송결과메시지</th>
                        <th class="col-md-1 info text-center">템플릿 코드</th>
                        <th class="col-md-1 info text-center"></th>
                    </tr>
                </thead>
                <tbody>
<?php
    foreach ($inventory as $obj)
    {
        $mt_pr         = $obj->mtPr;
        $status        = (int)$obj->status;
        $template_code = $obj->templateCode;
        $req_date      = $obj->reqDate;
        $rec_num       = $obj->recNum;

        $action_btn = "";
        if ($status === 1)
            $action_btn = "<button type='button' class='btn btn-danger' onclick='reqBiCancel($mt_pr)'>취소</button>";
        elseif ($status === 3)
            $action_btn = "<button type='button' class='btn btn-info' onclick='reqBiRetransmission($mt_pr)'>재전송</button>";

?>
                    <tr data-mt-pr="<?=$obj->mtPr?>">
                        <td class="text-center"><?=$obj->moid?></td>
                        <td class="text-center"><?=substr($req_date, 2, 14)?></td>
                        <td class="text-center"><?=$rec_num?></td>
                        <td>
                            <span class="bi-biztalk-msg"><?=nl2br($obj->content)?></span>
<?php
        $msg_modify_btn_display = ($status === 1 || $status === 3);
        if ($msg_modify_btn_display)
        {
            $msg_modify_btn = ($status === 1)? "<button type='button' class='btn btn-success btn-xs bi-modify-content-btn' data-modify-type='1' data-template-code='$template_code' data-req-time='$req_date' data-rec-num='$rec_num'>내용수정</button>" : "<button type='button' class='btn btn-success btn-xs bi-modify-content-btn' data-modify-type='2' data-template-code='$template_code' data-rec-num='$rec_num'>내용수정 후 즉시발송</button>";

?>
                            <hr>
                            <?=$msg_modify_btn?>
<?php
        }

?>

                        </td>
                        <td class="text-center"><?=$obj->msgStatus?></td>
                        <td class="text-center"><?=$obj->reportCode?></td>
                        <td class="text-center"><?=$obj->reportStr?></td>
                        <td class="text-center"><?=$template_code?></td>
                        <td class="text-center"><?=$action_btn?></td>
                    </tr>
<?php
    }

?>
                </tbody>
            </table>
            <div class="text-center">
                <?=$data['pagination']?>
            </div>
        </div>
    </div>
<?php

}

?>
    <div id="bi_biztalk_modal_container"></div>

</div>

<script>
    var mBiAssetUrl = '<?=asset_url()?>';
</script>
<script src="<?=base_url()?>static/lib/toastr/toastr.min.js"></script>
<script src="<?=asset_url()?>modal/js/common_modal.min.js"></script>
<script src="<?=asset_url()?>js/common_workspace.min.js?ver=190529"></script>
<script src="<?=asset_url()?>modal/js/biztalk_retransmit_modal.min.js?ver=190531_1"></script>
<script src="<?=asset_url()?>js/biztalk_inventory.min.js?ver=190531_1"></script>














