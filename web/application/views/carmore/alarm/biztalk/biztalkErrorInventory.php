
<div class="container workspace-basic-container col-md-12">
    <div class="page-header clearfix">
        <h3 class="pull-left">비즈톡 전송오류목록
            <small><span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" title="비즈톡 전송시 '알림톡 채널 차단' 등의 이유로 보내지지 않은 알림톡 리스트입니다" style="cursor: pointer"></span></small></h3>
    </div>

    <div class="clearfix"></div>

<?php
if (empty($data)) {

?>
    <div style="text-align: center;">
        <p class="lead">우와!<br>24시간 이내에 전송 실패한 알림톡 목록이 없어요!</p>
        <img src="/static/images/42_tube_boong.png" width="250">
        <p style="text-decoration: line-through"><small>작은 선물</small></p>

    </div>
<?php
} else {

?>
    <div class="col-md-offset-3 col-md-6">
        <table class="table table-bordered">
            <thead>
                <tr class="info">
                    <th>예약번호</th>
                    <th>수신번호</th>
                    <th>요청일</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
<?php
    foreach ($data as $talk) {
?>
                <tr>
                    <td><?=$talk->reservationIdx?></td>
                    <td><?=$talk->phoneNumber?></td>
                    <td><?=$talk->requestDate?></td>
                    <td><button type="button" class="btn btn-success btn-xs" onclick="location.href='/carmore/alarm/biztalk/BiztalkInventory?per_page=1&moid=<?=$talk->reservationIdx?>&recNum=<?=$talk->phoneNumber?>'">바로가기</button></td>
                </tr>


<?php

    }
?>

            </tbody>
        </table>
    </div>
<?php
}

?>

</div>


<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>