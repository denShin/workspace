<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <form id="updateConfirm">
        <input type="hidden" name="isConfirm" value="" form="updateConfirm"/>
        <input type="hidden" name="emode" value="agree_yn" form="updateConfirm"/>

        <div class="page-header clearfix">
            <h2 class="pull-left">결제 내역 관리</h2>
            <div class="pull-right" style="padding-top: 20px">
                &nbsp;&nbsp;&nbsp;&nbsp;
                <a href="?ptype=w" class="btn btn-info">등록하기</a>
            </div>

        </div>

        <div id="searchdiv">
            <form id="searchCompanyNotice" class="form-horizontal" role="form" method="get" >
                <fieldset>
                    <div class="clearfix">
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <select class="form-control" name="sp">
                                    <option value="f_drivername" >예약자 명</option>
                                    <option value="kakaonickname" >카카오 닉네임</option>
                                    <option value="f_CardNo" >카드번호(4자리)</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="sv" value="">
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </fieldset>
            </form>
            <hr/>
        </div>

        <div class="clearfix"></div>
        <form name="updateConfirm" method="post">
            <input type="hidden" name="emode" value="changecashstats">
            <input type="hidden" name="stats" >
            <input type="hidden" name="coinbuyoffer_idx" >
            <input type="hidden" name="targetprice" >
            <input type="hidden" name="chargefee" >
            <input type="hidden" name="buy_nowprice" >
            <input type="hidden" name="mem_id" >

            <div class="table-responsive">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr class="info row">
                        <th class="text-center col-md-1">No</th>
                        <th class="text-center col-md-2">카드 번호</th>
                        <th class="text-center col-md-1">카카오</th>
                        <th class="text-center col-md-1">예약자</th>
                        <th class="text-center col-md-2">메세지</th>
                        <th class="text-center col-md-2">결제금액</th>
                        <th class="text-center col-md-1">결제상태</th>
                        <th class="text-center col-md-2">등록일</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $startnum =$data["startnum"];
                    foreach($data["list"] as $entry) {
                        ?>
                        <tr class="text-center row">
                            <td class="small" ><?=$startnum?></td>
                            <td class="small" ><?= $entry["f_CardNo"] ?> </td>
                            <td class="small" style="padding-left: 20px;"><?= $entry["kakaonickname"] ?> </td>
                            <td class="small" style="padding-left: 20px;"><?= $entry["usrname"] ?> </td>
                            <td class="small"><?= $entry["f_resultmsg"]?></td>
                            <td class="small"><b><?= $entry["f_Amt"]?></b> 원
                                <?if($entry["f_logstatus"]=="1"){?>
                                    <span class="label label-danger"  style="cursor: pointer"  onclick="setpaystats('cancel','<?= $entry["f_paylogidx"]?>','<?= $entry["f_usrserial"]?>')">취소</span>
                                <?}else if($entry["f_logstatus"]=="2"){?>
                                    <span class="label label-info"  style="cursor: pointer"  onclick="setpaystats('recover','<?= $entry["f_paylogidx"]?>','<?= $entry["f_usrserial"]?>')">복구</span>
                                <?}?>


                            </td>
                            <td class="small">
                                <?if($entry["f_logstatus"]=="1"){?>
                                    정상결제
                                <?}else if($entry["f_logstatus"]=="2"){?>
                                    <span style="color:red">결제오류</span>
                                <?}?>
                            </td>
                            <td class="small"><?= $entry["regdate"] ?></td>
                        </tr>
                        <?php
                        $startnum--;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="text-center">
                <?=$data['pagination']?>
            </div>
        </form>

        <!--* modal start *-->
        <div id="myModal" class="modal fade bs-modified-modal-sm">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header ">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-danger modalTitle"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-2">
                                <p class="form-control-static text-right">관련 사유</p>
                            </div>
                            <div class="col-md-9">
                                <textarea   class="form-control" id="message" placeholder="관련사유를 입력하세요"  /></textarea>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="goSubmit"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="confirmBtn"></span></button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            var tempuidx="";
            var tempfidx="";
            var tempctype="";

            function setpaystats(ctype,fidx,uidx){
                tempfidx=fidx;
                tempuidx=uidx;
                tempctype=ctype;
                if(ctype==="cancel") {
                    $("#confirmBtn").text("취소 하기");
                    $(".modalTitle").text("결제취소 처리");
                }else if(ctype==="recover") {
                    $("#confirmBtn").text("복구 하기");
                    $(".modalTitle").text("결제복구 처리");
                }

                $("#myModal").modal().on("hidden.bs.modal", function() {
                    $("#confirmBtn").text("");
                    $(".modalTitle").text("");
                    tempctype="";
                    tempfidx="";
                    tempuidx="";
                });
            }


            $("#goSubmit").click(function() {
                var message = $("#message").val();
                var board_title="";
                var board_part="";
                var board_part="pay";
                var confirmtext="";

                if(tempctype==="cancel") {
                    board_title="결제 취소처리 상담";
                    confirmtext="실결제 취소는 PG 사 홈페이지에서 직접하셔야합니다.\n결제정보를 취소하시겠습니까?";
                }else if(tempctype==="recover"){
                    board_title="결제 복구처리 상담";
                    confirmtext="결제정보를 복구하시겠습니까?";
                }

                if (message =="") {
                    alert("상담내용을 기입해주세요.");
                    return false;
                }else {

                    if(!confirm(confirmtext)){
                        return;
                    }

                    $.ajax({
                        type: "get",
                        url: "/carmore/CarmoreBoardProc?emode=setcomplainajax&usrserial="+tempuidx+"&board_part="+board_part +
                        "&workmode="+tempctype+"&message=" + message+"&board_title="+board_title+"&f_paylogidx="+tempfidx ,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (response) {
                            console.log("response:"+response);
                            $('#myModal').modal('hide');
                            $("#message").val("");
                            tempctype="";
                            tempidx="";
                            location.reload();
                        }
                    });

                }

            });



        </script>
</div>

</body>
</html>
