<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <form id="updateConfirm">
        <input type="hidden" name="isConfirm" value="" form="updateConfirm"/>
        <input type="hidden" name="emode" value="agree_yn" form="updateConfirm"/>

        <div class="page-header clearfix">
            <h2 class="pull-left">쿠폰 현황 관리</h2>

        </div>

        <div id="searchdiv">
            <form id="searchCompanyNotice" class="form-horizontal" role="form" method="get" >
                <fieldset>
                    <div class="clearfix">
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <select class="form-control" name="sp">
                                    <option value="kakaonickname" >카카오 닉네임</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="sv" value="">
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </fieldset>
            </form>
            <hr/>
        </div>

        <div class="clearfix"></div>
        <form name="updateConfirm" method="post">
            <input type="hidden" name="emode" value="changecashstats">
            <input type="hidden" name="stats" >
            <input type="hidden" name="coinbuyoffer_idx" >
            <input type="hidden" name="targetprice" >
            <input type="hidden" name="chargefee" >
            <input type="hidden" name="buy_nowprice" >
            <input type="hidden" name="mem_id" >

            <div class="table-responsive">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr class="info row">
                        <th class="text-center col-md-1">No</th>
                        <th class="text-center col-md-4">쿠폰정보</th>
                        <th class="text-center col-md-2">카카오</th>
                        <th class="text-center col-md-3">쿠폰상태</th>
                        <th class="text-center col-md-2">등록일</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $startnum =$data["startnum"];
                    foreach($data["list"] as $entry) {
                        ?>
                        <tr class="text-center row">
                            <td class="small" ><?=$startnum?></td>
                            <td class="small text-left" > <?= $entry["coupontitle"] ?> <?=$entry["couponprice"]?> 원</td>
                            <td class="small" ><?=$entry["kakaonickname"]?>  </td>
                            <td class="small"><?= $entry["cstatustr"]?></td>
                            <td class="small"><?= $entry["regdate"] ?></td>
                        </tr>
                        <?php
                        $startnum--;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="text-center">
                <?=$data['pagination']?>
            </div>
        </form>
        <script type="text/javascript">

            function setcashstats(stats){
                var confirmtxt ="";
                if(stats=="y"){
                    confirmtxt ="선택된 충전정보를 입금확인으로 변경하시겠습니까?";
                }else{
                    confirmtxt ="선택된 충전정보를 입금미확인으로 변경하시겠습니까?";
                }

                if(confirm(confirmtxt)){
                    document.updateConfirm.stats.value=stats;
                    document.updateConfirm.action="/coinbackstore/StoreCoinBuyOfferProc";
                    document.updateConfirm.submit();
                }
            }

            function setcoin(coinbuyoffer_idx,targetprice,chargefee,mem_id){
                if(confirm("비트코인을 충전 하시겠습니까?")){

                    document.updateConfirm.emode.value="setcointranslog";
                    document.updateConfirm.coinbuyoffer_idx.value=coinbuyoffer_idx;
                    document.updateConfirm.targetprice.value=targetprice;
                    document.updateConfirm.chargefee.value=chargefee;
                    document.updateConfirm.mem_id.value=mem_id;
                    document.updateConfirm.action="/application/views/data/buybtcfrombithumb.php";
                    document.updateConfirm.submit();
                }
            }

            function oldsetcoin(coinbuyoffer_idx,buybtc,buy_nowprice,mem_id){
                if(confirm("비트코인을 충전 하시겠습니까?")){

                    document.updateConfirm.emode.value="setcointranslog";
                    document.updateConfirm.coinbuyoffer_idx.value=coinbuyoffer_idx;
                    document.updateConfirm.buybtc.value=buybtc;
                    document.updateConfirm.mem_id.value=mem_id;
                    document.updateConfirm.buy_nowprice.value=buy_nowprice;
                    document.updateConfirm.action="/coinbackstore/StoreCoinBuyOfferProc";
                    document.updateConfirm.submit();
                }
            }
        </script>
</div>

</body>
</html>
