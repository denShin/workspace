
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawVisualization);

        function drawVisualization() {
            var data = google.visualization.arrayToDataTable(<?=$data["chartdata"]?>);

            var options = {
                title : '<?=$data["graphtitle"]?>',
                vAxis: {title: '매출액'},
                hAxis: {title: '기간'},
                seriesType: 'bars',
                series: {1: {type: 'line'}}
            };

            var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
            chart.draw(data, options);
        }
    </script>


    <h1   class="page-header">대시보드 / <?=$data["today"]?></h1>


    <div class="row ">
        <div class="col-sm-3">

            <div class="panel  panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">올해</h3>
                </div>
                <div class="panel-body">
                    <strong>매출 <?=$data["yeartotalprice"]?> 원/구매  <?=$data["yeartotalcount"]?>건<br>
                        취소  <?=$data["yearcancelprice"]?>원/취소  <?=$data["yearcancelcount"]?>건</strong>
                </div>
            </div>
        </div>
        <div class="col-sm-3">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">이번분기 </h3>
                </div>
                <div class="panel-body">
                    <strong>매출 <?=$data["qtotalprice"]?> 원/구매  <?=$data["qtotalcount"]?>건<br>
                        취소  <?=$data["qcancelprice"]?>원/취소  <?=$data["qcancelcount"]?>건</strong>
                </div>
            </div>
        </div>
        <div class="col-sm-3">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">이번달</h3>
                </div>
                <div class="panel-body">
                    <strong>매출 <?=$data["monthtotalprice"]?> 원/구매  <?=$data["monthtotalcount"]?>건<br>
                        취소  <?=$data["monthcancelprice"]?>원/취소  <?=$data["monthcancelcount"]?>건</strong>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">오늘 </h3>
                </div>
                <div class="panel-body">
                    <strong>매출 <?=$data["todaytotalprice"]?> 원/구매  <?=$data["todaytotalcount"]?>건<br>
                        취소  <?=$data["todaycancelprice"]?>원/취소  <?=$data["todaycancelcount"]?>건</strong>
                </div>
            </div>
        </div>
    </div>



    <div class="row ">
        <div class="col-sm-3">

            <div class="panel  panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">오늘 매출액</h3>
                </div>
                <div class="panel-body">
                    <strong><h2><?=$data["todaytotalprice"]?> 원</h2></strong>
                </div>
            </div>
        </div>
        <div class="col-sm-3">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">오늘 구매/취소건수 </h3>
                </div>
                <div class="panel-body">
                    <strong><h2><?=$data["todaytotalcount"]?> 건 / <?=$data["todaycancelcount"]?>건 </h2></strong>
                </div>
            </div>
        </div>
        <div class="col-sm-3">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">총 취소액</h3>
                </div>
                <div class="panel-body">
                    <strong><h2><?=$data["todaycancelprice"]?> 원</h2></strong>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">오늘 회원가입</h3>
                </div>
                <div class="panel-body">
                    <strong><h2><?=$data["todaymembercnt"]?> 명</h2></strong>
                </div>
            </div>
        </div>
    </div>


    <div class="row ">
        <div class="col-sm-8">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">기간별 매출액 추이 </h3>
                </div>
                <div class="panel-body">
                    <div id="chart_div" style="width: 100%; height: 300px;"></div>
                </div>
            </div>

        </div>
        <div class="col-sm-4">

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">고객 문의/상담 </h3>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-hover table-bordered">

                        <tbody>
                        <?php
                        foreach($data["complainlist"] as $entry) {
                            ?>
                            <tr class="row">
                                <td   class="small text-center"  ><?=$entry["board_partstr"]?></td>
                                <td  style="padding-left: 10px;align:left"  class="small">  <a href="/carmore/CarmoreBoard?ptype=v&board_idx=<?=$entry["board_idx"]?>&per_page=<?=$data["per_page"]?>"><?=$entry["board_title"]?></a> <?if($entry["reply_cnt"] >0){?>(<b><?=$entry["reply_cnt"]?>)</b><?}?></td>
                                <td   class="small text-center"><?=$entry["kakaonickname"]?> </td>
                            </tr>
                        <?}?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>


    <div class="row ">
        <div class="col-sm-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">오늘의 예약 정보 </h3>
                </div>
                <div class="panel-body">
                    <table class="table   table-bordered">

                        <tbody>
                        <?php
                        foreach($data["orderlist"] as $entry) {
                            ?>
                            <tr class="text-center row">

                                <td class="small text-left"   >
                                    <span style="font-size: medium"><strong><?= $entry["model"] ?></strong></span>
                                    <br><?= $entry["brand"] ?>  /<?= $entry["carType"] ?>/<?=$entry["age_info"]?> <span class="label label-primary searchStatus" data-status=""><?= $entry["f_rentstatus"] ?></span>
                                    <br><?= $entry["car_number1"] ?> <?= $entry["car_number2"] ?> <?=$entry["car_number3"]?> /  <?=$entry["oil"]?> <?if($entry["color"] !=""){?>/  <?=$entry["color"]?><?}?>

                                    <hr><span class="label label-default ">대여기간</span> <?= $entry["f_rentstartdate"] ?> ~  <?= $entry["f_rentenddate"] ?>
                                    <?if($entry["f_delivpickaddr"] !=""){?>
                                        <br><br> 배차주소 :<?=$entry["f_delivpickaddr"]?>
                                        <br>회차주소 :<?=$entry["f_delivpickaddr"]?>
                                    <?}?>

                                </td>
                                <td class="small text-left"   >
                                    <span style="font-size: medium"><strong><?= $entry["driver_name_encrypt"] ?></strong></span>
                                    <hr><span style="font-size: medium"><strong><?= $entry["driver_phone_encrypt"] ?></strong></span>
                                    <BR>생년월일 : <?= $entry["driver_birthday_encrypt"] ?>
                                    <BR><?= $entry["f_driveremail"] ?>
                                </td>
                                <td class="small text-left">
                                    <span style="font-size: medium"><strong><?= $entry["f_totalprice"] ?></strong> 원</span>

                                    <hr>총 단가 : <?= $entry["original_price"] ?> 원
                                    <br>렌트비용 : <?= $entry["f_rentprice"] ?> 원 / 보험비용 : <?= $entry["f_insuprice"] ?> 원
                                    <br>배송비용 : <?= $entry["f_delivprice"] ?> 원 / 사용포인트 : <span style="color: red">- <?= $entry["f_usepoint"] ?> </span>  원
                                    <br>쿠폰 : <?=$entry["coupon_title"]?> <span style="color: red">- <?= $entry["coupon_price"] ?> </span>  원
                                </td>
                                <td class="small text-left"   >
                                    <?=$entry["brancharea"]?><br>
                                    <span style="font-size: medium"><strong><?= $entry["name"] ?></strong></span>
                                    <br><?= $entry["branchName"] ?>
                                </td>

                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>



                </div>
            </div>

        </div>
        

    </div>
