
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>

    <div class="page-header clearfix">
        <h2 class="pull-left">진행중 이벤트 순서관리</h2>
    </div>
    <div class="clearfix" style="padding-top: 5px"></div>

    <div class="pull-left">
        <button class="btn btn-primary "onclick="location.href='?'">전체이벤트</button>
        <button class="btn btn-success" onclick="location.href='?ptype=n'">진행중이벤트</button>
    </div>
    <div class="pull-right">
        <button class="btn btn-danger "onclick="changeorder()">현재순서저장</button>
    </div>

    <div class="clearfix" style="padding-top: 40px"></div>
    <form id="updateConfirm">

        <div class="clearfix"></div>


        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">정렬순서</th>
                    <th class="text-center col-md-2">이미지</th>
                    <th class="text-center col-md-4">제목</th>
                    <th class="text-center col-md-1">상태</th>
                    <th class="text-center col-md-2">시작/마감일</th>
                    <th class="text-center col-md-2">등록일</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $startnum = $data["startnum"];
                foreach($data["list"] as $entry) {

                    ?>
                    <tr class="row" id="<?=$entry["board_idx"]?>">
                        <td  class="small text-center"><?=$entry["ordernum"] ?> </td>
                        <td   class="small text-center">
                            <?if($entry["fileurl"] !=""){?>
                                <img src="<?=$entry["fileurl"]?>" width="50">
                            <?}?></td>
                        <td  style="padding-left: 10px;align:left"  class="small">  <a href="?ptype=v&board_code=<?=$entry["board_code"]?>&board_idx=<?=$entry["board_idx"]?>&per_page=<?=$data["per_page"]?>"><?=$entry["board_title"]?></a> </td>
                        <td   class="small text-center"><?=$entry["board_statstr"]?></td>
                        <td   class="small text-center"><?=$entry["startdate"]?> ~ <?=$entry["enddate"]?></td>
                        <td   class="small text-center"><?=$entry["regdate"]?></td>
                    </tr>
                    <?
                    $startnum--;
                }?>

                </tbody>
            </table>
        </div>
    </form>


    <div class="text-center">
        <?=$data['pagination']?>
    </div>

    <!-- Bootstrap & Core Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>

    <script type="text/javascript">



        var order ="";
        $('tbody').sortable({
            update: function( ) {
                order= $("tbody").sortable("toArray");
                //order = $("tbody").sortable("serialize", {key:'order[]'});
                console.log("order:"+order);
            }
        });

        function changeorder(){

            if(order===""){
                alert('순서가 변경된 내역이 없습니다.');
                return;
            }

            if(confirm("이벤트의 순서를 변경하시겠습니까?")){

                $.ajax({
                    type: "get",
                    url: "/carmore/CarmoreBoardProc?emode=setorderajax&order="+order,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        console.log("response:"+response);

                        alert('순서 변경이 완료되었습니다!');

                      //  location.reload();
                    }
                });

            }

        }
    </script>
