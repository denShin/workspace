
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">

    <style>
        .double-input .form-control {
            width: 50%;
        }
    </style>

    <div class="page-header">
        <h2>푸시 전송 관리</h2>
    </div>


    <form id="inputform" class="form-horizontal" role="form" method="post" enctype="multipart/form-data"  action="/carmore/PushManageProc">
        <input type="hidden" name="emode" value="<?=$data["emode"]?>">
        <input type="hidden" name="pushlog_idx" value="<?=$data["pushlog_idx"]?>">

        <fieldset>

            <div class="form-group">
                <label for="shareCategory" class="col-md-2 control-label">푸시 종류</label>
                <div class="col-md-5">
                    <select name="pushlog_type" class="form-control">
                        <option value="all" <?if($data["pushlog_type"]=="all"){ echo "selected";}?> >전체 푸시</option>
                        <option value="ad" <?if($data["pushlog_type"]=="ad"){ echo "selected";}?> >광고 푸시</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="shareCategory" class="col-md-2 control-label">플랫폼 선택</label>
                <div class="col-md-5">
                    <select name="pushlog_plat" class="form-control">
                        <option value="all" <?if($data["pushlog_plat"]=="1"){ echo "selected";}?> >전체</option>
                        <option value="iphone" <?if($data["pushlog_plat"]=="1"){ echo "selected";}?> >아이폰</option>
                        <option value="android" <?if($data["pushlog_plat"]=="2"){ echo "selected";}?> >안드로이드</option>
                    </select>
                </div>
            </div>
              
            <hr>

            <div class="form-group">
                <label for="shareContent" class="col-md-2 control-label">푸시 내용</label>
                <div class="col-md-10">
                    <textarea   class="form-control"  name="pushlog_text"  style="width: 60%;height: 80px"  id="pushlog_text"><?=$data["pushlog_text"]?></textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary btn-block">저장하기</button>
                        </div>
                        <div class="col-md-6">
                            <button type="reset" class="btn btn-warning btn-block">다시작성</button>
                        </div>
                    </div>

                </div>
            </div>
        </fieldset>
    </form>

    <script type="text/javascript">
        $(function() {
            $("#inputform").submit(function() {

                if ($("textarea[name='pushlog_text']").val() =="") {
                    alert("푸시 내용을 입력하세요.");
                    $("textarea[name='pushlog_text']").focus();
                    return false;
                }

                if (!confirm("푸시 발송 정보를 등록 하시겠습니까?")) {

                    return false;
                }
            });

        });


    </script></div>
