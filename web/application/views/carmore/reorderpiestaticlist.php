
<div class="container" style="padding-top: 30px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);


        function drawChart() {

            var data = google.visualization.arrayToDataTable(<?=$data["chartdata"]?>);
            var options = {
                title: '<?=$data["graphtitle"]?>'
            };

            var chart = new google.visualization.PieChart(document.getElementById('chartdiv'));
            chart.draw(data, options);

        }

        function set_dateinfo(datepart){
            var enddate="<?=$data["today"]?>";
            var startdate="";

            if(datepart==="1week"){
                startdate="<?=$data["week1"]?>";
            }else if(datepart ==="1month"){
                startdate="<?=$data["month1"]?>";
            }else if(datepart ==="3month"){
                startdate="<?=$data["month3"]?>";
            }else if(datepart ==="6month"){
                startdate="<?=$data["month6"]?>";
            }else if(datepart ==="1year"){
                startdate="<?=$data["year1"]?>";
            }

            $("#startdate").val(startdate);
            $("#enddate").val(enddate);

            document.af.submit();

        }


    </script>

    <div class="page-header clearfix">
        <h2 class="pull-left"><?=$data["pagetitle"]?> 정보</h2>
        <div class="pull-right" style="padding-top: 20px">
            <a href="/carmore/ReorderStaticManage" class="btn btn-info">꺾은건</a>
            <a href="/carmore/ReorderPieStaticManage" class="btn btn-primary">원형</a>
        </div>
    </div>

    <div class="text-center">
        <form class="form-inline" name="af" method="get" action="?">
            <input type="hidden"  name="stype" value="<?=$data["stype"]?>" >
            <input type="date"class="form-control" name="startdate" id="startdate" value="<?=$data["startdate"]?>" placeholder="시작월">~
            <input type="date" class="form-control"  name="enddate"  id="enddate" value="<?=$data["enddate"]?>"  placeholder="마감월">

            <button type="submit" class="form-control" >검색</button>
            <div class="row">
                <button type="button" onclick="set_dateinfo('1week')" class="btn btn-link form-control" >1주일</button>
                <button type="button" onclick="set_dateinfo('1month')"  class="btn btn-link form-control" >1개월</button>
                <button type="button" onclick="set_dateinfo('3month')"  class="btn btn-link form-control" >3개월</button>
                <button type="button" onclick="set_dateinfo('6month')"  class="btn btn-link form-control" >6개월</button>
                <button type="button" onclick="set_dateinfo('1year')"  class="btn btn-link form-control" >1년</button>
            </div>
        </form>

    </div>

    <div class="text-center">

        <div id="chartdiv" style="width: 900px; height: 500px;"></div>
    </div>


    <form id="updateConfirm">

        <div class="table-responsive">
            <table class="table  table-hover table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-3">날짜</th>
                    <th class="text-center col-md-3">신규주문수</th>
                    <th class="text-center col-md-3">재주문수(2~4회)</th>
                    <th class="text-center col-md-3">재주문수(5회~)</th>

                </tr>
                </thead>
                <tbody>
                <?php
                $startnum = $data["startnum"];
                foreach($data["list"] as $entry) {

                    ?>
                    <tr class="row">
                        <td  class="small text-center"><?=$entry["resultval"]?> </td>
                        <td  class="small text-center"><?=$entry["newcnt"]?> 건</td>
                        <td  class="small text-center"><?=$entry["reordercnt"]?> 건</td>
                        <td  class="small text-center"><?=$entry["reorderovercnt"]?> 건</td>
                    </tr>
                    <?
                    $startnum--;
                }?>

                </tbody>
            </table>
        </div>
    </form>

