
<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>

    <div class="page-header clearfix">
        <h2 class="pull-left">푸시전송 관리</h2>
        <div class="pull-right" style="padding-top: 30px">
            <a href="?ptype=w" class="btn btn-info">등록하기</a>
        </div>
    </div>


    <form name="updateConfirm" method="post">
        <input type="hidden" name="emode"  >
        <input type="hidden" name="pushlog_idx" >


        <div class="clearfix"></div>
        * 전송이 시작되면 취소할 수 없습니다.


        <div class="table-responsive">
            <table class="table  table-hover table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">No</th>
                    <th class="text-center col-md-2">푸시 종류</th>
                    <th class="text-center col-md-2">푸시 플랫폼</th>
                    <th class="text-center col-md-3">푸시 내용</th>
                    <th class="text-center col-md-2">전송 상태</th>
                    <th class="text-center col-md-2">등록일</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $startnum = $data["startnum"];
                foreach($data["list"] as $entry) {

                    ?>
                    <tr class="row" >
                        <td  class="small text-center"><?=$startnum?> </td>
                        <td   class="small text-center"><?=$entry["pushlog_typestr"]?> </td>
                        <td   class="small text-center"><?=$entry["pushlog_platstr"]?> </td>
                        <td   class="small text-center"><?=$entry["pushlog_text"]?></td>
                        <td   class="small text-center"><?=$entry["pushlog_statstr"]?>
                            <?if( $entry["pushlog_stats"] =="n"){?>
                            <span class="label label-danger "  style="cursor: pointer"   onclick="delpush('<?=$entry["pushlog_idx"]?>' )">취소</span>
                            <?}?>
                        </td>
                        <td   class="small text-center"><?=$entry["regdate"]?></td>
                    </tr>
                    <?
                    $startnum--;
                }?>

                </tbody>
            </table>
        </div>
    </form>


    <div class="text-center">
        <?=$data['pagination']?>
    </div>
    <script type="text/javascript">

        function delpush(pushlog_idx){
            var confirmtxt   ="선택된 정보를 삭제 하시겠습니까?";
            if(confirm(confirmtxt)){
                document.updateConfirm.emode.value='del';
                document.updateConfirm.pushlog_idx.value=pushlog_idx ;
                document.updateConfirm.action="/carmore/PushManageProc";
                document.updateConfirm.submit();
            }
        }
    </script>