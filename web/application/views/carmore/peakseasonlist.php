<div class="container" style="padding-top: 50px;padding-bottom: 70px;">
    <form id="updateConfirm">
        <input type="hidden" name="isConfirm" value="" form="updateConfirm"/>
        <input type="hidden" name="emode" value="agree_yn" form="updateConfirm"/>

        <div class="page-header clearfix">
            <h2 class="pull-left">성수기 관리</h2>
            <div class="pull-right" style="padding-top: 20px">
                &nbsp;&nbsp;&nbsp;&nbsp;
                <a href="#" onclick="showPeakmodal()" class="btn btn-info">등록하기</a>
            </div>
        </div>


        <div class="clearfix"></div>
        <div class="pull-left">
            <button type="button" class="btn btn-primary  btn-xs"onclick="setchangestats('y')">보이기</button>
            <button type="button" class="btn btn-danger  btn-xs"onclick="setchangestats('n')">숨기기</button>

        </div>

        <div class="clearfix" style="padding-top: 10px"></div>
        <br>
        <form name="updateConfirm" method="post">
            <input type="hidden" name="emode" value="changecashstats">
            <input type="hidden" name="stats" >
            <input type="hidden" name="coinbuyoffer_idx" >
            <input type="hidden" name="targetprice" >
            <input type="hidden" name="chargefee" >
            <input type="hidden" name="buy_nowprice" >
            <input type="hidden" name="mem_id" >

            <div class="table-responsive">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr class="info row">
                        <th class="text-center col-md-1">No</th>
                        <th class="text-center col-md-1">연도</th>
                        <th class="text-center col-md-2">시작일</th>
                        <th class="text-center col-md-2">마감일</th>
                        <th class="text-center col-md-2">등록자</th>
                        <th class="text-center col-md-2">상태</th>
                        <th class="text-center col-md-2">삭제</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $startnum =$data["startnum"];
                    foreach($data["list"] as $entry) {
                        ?>
                        <tr class="text-center row">
                            <td><input type="checkbox" name="peakseason_idx" value="<?= $entry["peakseason_idx"] ?>"></td>
                            <td class="small" ><b><?=$entry["peakseason_year"]?></b> 년</td>
                            <td class="small"><?= $entry["peakseason_startdate"]?></td>
                            <td class="small"><?= $entry["peakseason_enddate"]?></td>
                            <td class="small"><?= $entry["mem_name"]?></td>
                            <td class="small"><?= $entry["show_ynstr"]?></td>
                            <td class="small"><button class="btn btn-danger btn-sm" onclick="delpeakseason('<?=$entry["peakseason_idx"]?>')">삭제</button> </td>
                        </tr>
                        <?php
                        $startnum--;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="text-center">
                <?=$data['pagination']?>
            </div>
        </form>


</div>

<div id="peakseasonModal" class="modal fade bs-modified-modal-sm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-danger ">성수기 추가하기</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2">
                        <p class="form-control-static text-right">시작일</p>
                    </div>
                    <div class="col-md-9">
                        <input type="date" id="peakseason_startdate"  class="form-control"  placeholder="시작일을 입력하세요"  value="">
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <div class="row" style="padding-top: 10px">
                    <div class="col-md-2">
                        <p class="form-control-static text-right">마감일</p>
                    </div>
                    <div class="col-md-9">
                        <input type="date"   id="peakseason_enddate" class="form-control"  placeholder="마감일을 입력하세요"  value="">
                    </div>
                    <div class="col-md-1"></div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="peakseasonSubmit" onclick="sendpeakseason()"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> <span id="pconfirmBtn">성수기 추가</span></button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> 취소</button>
            </div>
        </div>
    </div>
</div>

<script language="JavaScript">
    function setchangestats(stats){

        var objpeakseason_idx =  $('[name="peakseason_idx"]');
        var objlength =objpeakseason_idx.length;
        var saveparam=new Array();
        var cnt=0;



        for(var i=0;i < objlength ;i++){
            var obj =objpeakseason_idx[i];
            if(obj.checked){
                var idx =objpeakseason_idx[i]["value"];

                if(idx !=="" ){
                    saveparam.push(idx);
                    cnt++;
                }
            }

        }

        if(cnt===0){
            alert('선택된 정보가 없습니다.');
            return;
        }
        saveparam = saveparam.join(",");

        var confirmtxt ="";
        if(stats=="y"){
            confirmtxt ="선택된 정보를 보임으로 변경하시겠습니까?";
        }else{
            confirmtxt ="선택된 정보를 숨김으로 변경하시겠습니까?";
        }

        if(confirm(confirmtxt)){
            $.ajax({
                type:"get",contentType: "application/json",
                url:"/carmore/PeakseasonManage?ptype=changeshowyn&stats="+stats+"&saveparam="+saveparam,
                datatype: "json",
                success: function(data) {
                    alert("수정이 완료되었습니다.");
                    location.reload();

                },
                error: function(x, o, e) {

                }
            });
        }


    }

    function dateparse(str) {
        str = str.replace(/-/gi, "");
        if(!/^(\d){8}$/.test(str)) return "invalid date";
        var y = str.substr(0,4),
            m = str.substr(4,2),
            d = str.substr(6,2);
        m =m-1;
        return new Date(y,m,d);
    }


    function showPeakmodal(){

        $("#peakseasonModal").modal().on("hidden.bs.modal", function() {

        });
    }

    function delpeakseason(peakseason_idx){

        console.log("/carmore/PeakseasonManage?ptype=setpeakseason&peakseason_idx="+peakseason_idx
            +"&datamode=del" );
        if(confirm("성수기를 삭제 하시겠습니까?")){
            $.ajax({
                type: "get",
                url: "/carmore/PeakseasonManage?ptype=setpeakseason&peakseason_idx="+peakseason_idx
                    +"&datamode=del" ,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log("response:"+response);
                    $('#myModal').modal('hide');
                    $("#message").val("");
                    tempctype="";
                    tempidx="";
                    location.reload();
                }
            });
        }
    }

    function sendpeakseason(){

        var peakseason_startdate =$("#peakseason_startdate").val();
        var peakseason_enddate =$("#peakseason_enddate").val();

        if(dateparse(peakseason_startdate) > dateparse(peakseason_enddate)){
            alert('날짜 설정을 잘못 하셨습니다.');
            return;
        }

        if(peakseason_startdate===""){
            alert('시작일을 등록해주세요');
            $("#peakseason_startdate").focus();
            return;
        }

        if(peakseason_enddate===""){
            alert('마감일을 등록해주세요');
            $("#peakseason_enddate").focus();
            return;
        }

        console.log("/carmore/PeakseasonManage?ptype=setpeakseason&peakseason_startdate="+peakseason_startdate
            +"&peakseason_enddate="+peakseason_enddate );
        if(confirm("성수기를 등록 하시겠습니까?")){
            $.ajax({
                type: "get",
                url: "/carmore/PeakseasonManage?ptype=setpeakseason&peakseason_startdate="+peakseason_startdate
                    +"&peakseason_enddate="+peakseason_enddate+"&datamode=new" ,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log("response:"+response);
                    $('#myModal').modal('hide');
                    $("#message").val("");
                    tempctype="";
                    tempidx="";
                    location.reload();
                }
            });
        }
    }

</script>
</body>
</html>
