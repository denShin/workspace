
<script src="/static/lib/dropzone/dropzone.js"></script>
<link href="/static/lib/dropzone/dropzone.css"rel="stylesheet">

<div class="container" style="padding-top: 50px;padding-bottom: 70px;">

    <style>
        .double-input .form-control {
            width: 50%;
        }
    </style>

    <div class="page-header">
        <h2>이벤트 관리</h2>
    </div>



    <form id="inputform" class="form-horizontal" role="form" method="post" enctype="multipart/form-data"  action="/carmore/CarmoreBoardProc">
        <input type="hidden" name="emode" value="<?=$data["emode"]?>">
        <input type="hidden" name="board_code" value="<?=$data["board_code"]?>">
        <input type="hidden" name="board_idx" value="<?=$data["board_idx"]?>">
        <input type="hidden" name="content_code" value="<?=$data["content_code"]?>">

        <fieldset>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">이벤트 제목</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" name="board_title" placeholder="제목을 입력하세요" value="<?=$data["board_title"]?>" autofocus/>
                </div>
            </div>
            <div class="form-group">
                <label for="shareTitle" class="col-md-2 control-label">이벤트 기간</label>
                <div class="col-md-2">
                    <input type="date" class="form-control" name="startdate" placeholder="시작일"  value="<?=$data["startdate"]?>"   autofocus/>
                </div>
                <div class="col-md-2">
                    <input type="date" class="form-control" name="enddate" placeholder="마감일"  value="<?=$data["enddate"]?>"   autofocus/>
                </div>
            </div>

            <div class="form-group">
                <label for="shareContent" class="col-md-2 control-label">이벤트 설명</label>
                <div class="col-md-10">
                    <textarea   class="form-control"  name="board_content"  style="width: 60%;height: 80px"  ><?=$data["board_content"]?></textarea>
                </div>
            </div>
            <hr>


            <div class="form-group">
                <label class="col-md-2 control-label">파일</label>
                <div class="col-md-10">
                    <div class="dropzone dz-clickable" id="myDrop">
                        <div class="dz-default dz-message" data-dz-message="">
                            <span>여기를 클릭해서 파일을 추가하거나 드래그로 추가하시면 즉시 업로드가 시작됩니다. (파일 1개당 1G까지 업로드 가능)</span>
                        </div>
                    </div>
                </div>
            </div>

            <br/>

            <div id="fileOrgName"></div>
            <div id="fileSaveName"></div>
            <div id="fileDir"></div>
            <div id="fileSize"></div>
            <div id="fileType"></div>




            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="row">
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary btn-block">저장하기</button>
                        </div>
                        <div class="col-md-4">
                            <button type="reset" class="btn btn-warning btn-block">다시작성</button>
                        </div>
                        <div class="col-md-4">
                            <button type="button" onclick="cancelwrite()" class="btn btn-danger btn-block">취소하기</button>
                        </div>
                    </div>

                </div>
            </div>
        </fieldset>
    </form>

    <script type="text/javascript">

        function cancelwrite(){

            if(confirm('이벤트 작성을 취소하시겠습니까?')){
                location.href="/carmore/EventManage";
            }
        }

        var myDropZone;
        $(document).ready(function() {
            Dropzone.autoDiscover = false;
            myDropZone = new Dropzone("#myDrop", {
                url : "/application/views/data/uploaddropzone.php",
                addRemoveLinks : true,
                maxFilesize: 500,
                autoProcessQueue : true,
                paramName: "file1234",
                parallelUploads : 100,
                sending:function(file, xhr, formData){
                    formData.append('board_code', '<?=$data["board_code"]?>');
                    formData.append('content_code', '<?=$data["content_code"]?>');
                    formData.append('mem_id', '<?=$data["mem_id"]?>');
                },
                success: function( file, response ) {

                    obj = JSON.parse(response);

                    file.fileDir = obj.fileDir;
                    file.fileSaveName = obj.fileSaveName;
                    file.fileidx = obj.uploadfile_infoidx;

                    var input1 = $("<input>");
                    input1.attr({"type" : "hidden", "name" : "fileOrgName[]", "value" : obj.fileOrgName});
                    var input2 = $("<input>");
                    input2.attr({"type" : "hidden", "name" : "fileSaveName[]", "value" : obj.fileSaveName});
                    var input4 = $("<input>");
                    input4.attr({"type" : "hidden", "name" : "fileSize[]", "value" : obj.fileSize});
                    var input5 = $("<input>");
                    input5.attr({"type" : "hidden", "name" : "fileType[]", "value" : obj.fileType});

                    $("#fileOrgName").append(input1);
                    $("#fileSaveName").append(input2);
                    $("#fileSize").append(input4);
                    $("#fileType").append(input5);
                },

                removedfile: function(file) {

                    if (file.fileSaveName) {
                        $.post("/application/views/data/deletedropzone.php?board_code=<?=$data["board_code"]?>&content_code=<?=$data["content_code"]?>&fileSaveName="+file.fileSaveName);
                        $("." + file.fileSaveName.replace('.', '')).remove();
                    }
                    var _ref;
                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                }
            });

        });



        $(function() {
            $("#inputform").submit(function() {

                if ($("input[name='board_title']").val() ==="") {
                    alert("제목을 입력하세요.");
                    $("input[name='board_title']").focus();
                    return false;
                }

                if ($("input[name='startdate']").val() ==="") {
                    alert("시작일을 입력하세요.");
                    $("input[name='startdate']").focus();
                    return false;
                }
                if ($("input[name='enddate']").val() ==="") {
                    alert("마감일을 입력하세요.");
                    $("input[name='enddate']").focus();
                    return false;
                }

                var startdate = $("input[name='startdate']").val();
                var enddate = $("input[name='enddate']").val();

                var date1 = new Date(startdate);
                var date2 = new Date(enddate);

                if(date1.getTime() > date2.getTime()){
                    alert("시작 마감일이 잘못 입력되었습니다.");
                    return false;
                }

                if ($("textarea[name='board_content']").val() ==="") {
                    alert("이벤트 내용을 입력하세요.");
                    return false;
                }

                if (!confirm("정말 등록하시겠습니까?")) {

                    return false;
                }
            });

        });


    </script></div>
