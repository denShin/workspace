<div class="container" style="padding-top: 50px;padding-bottom: 70px;">

    <div class="page-header clearfix">
        <h3 class="pull-left"><?=$data["listitle"]?></h3>
        <?if($data["mem_stats"]=="y"){?>

            <div class="pull-right" style="padding-top: 20px">
                <button type="button"  onclick="setstorememstats('n')" class="btn btn-danger">회원차단</button>
                <button type="button"  onclick="setstorememstats('y')" class="btn btn-success" >회원승인</button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <a href="?ptype=w" class="btn btn-info">등록하기</a>
            </div>
        <?}?>
    </div>

    <div id="searchdiv" style="display: none">
        <form id="searchform" class="form-horizontal" role="form" method="get" >
            <input type="hidden" name="mem_stats" value="<?=$data["mem_stats"]?>">
            <fieldset>
                <div class="clearfix">
                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <select class="form-control" name="sp">
                                <option value="mem_id" >아이디</option>
                                <option value="mem_nick" >닉네임</option>
                                <option value="mem_email" >이메일</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="sv" value="">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary btn-block">검색하기</button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </fieldset>
        </form>
        <hr/>
    </div>

    <div class="clearfix"></div>
    <form name="updateConfirm" method="post">
        <input type="hidden" name="emode" value="changestats">
        <input type="hidden" name="stats" >

        <div class="table-responsive">
            <table class="table   table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">Check</th>
                    <th class="text-center col-md-3">회사명</th>
                    <th class="text-center col-md-2">대표자</th>
                    <th class="text-center col-md-2">휴대폰</th>
                    <th class="text-center col-md-2">상태</th>
                    <th class="text-center col-md-2">가입일</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($data["list"] as $entry) {
                    ?>
                    <tr class="text-center row">
                        <td><input type="checkbox" name="usrserial[]" value="<?= $entry["usrserial"] ?>"></td>
                        <td class="small text-center"   ><?= $entry["NAME"] ?> </td>
                        <td class="small text-center" ><?= $entry["OWNER"] ?></td>
                        <td class="small text-center" > <?= $entry["ownertel"] ?></td>
                        <td class="small text-center"><?= $entry["mem_statstr"] ?></td>
                        <td class="small text-center"><?= $entry["regdate"] ?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="text-center">
            <?=$data['pagination']?>
        </div>
    </form>


    <script type="text/javascript">

        function setstorememstats(stats){
            var checkcnt = $("input:checkbox[name='memidinfo[]']:checked").length;
            if(checkcnt <1){
                alert("선택된 회원 정보가 없습니다.");
                return;
            }
            var confirmtxt ="";
            if(stats=="y"){
                confirmtxt ="선택된 회원의 상태를 정상으로 변경하시겠습니까?";
            }else{
                confirmtxt ="선택된 회원의 상태를 차단으로 변경하시겠습니까?";
            }

            if(confirm(confirmtxt)){
                document.updateConfirm.stats.value=stats;
                document.updateConfirm.action="/coinbackapp/CoinbackMemberProc";
                document.updateConfirm.submit();
            }
        }


        function delmember(){
            var confirmtxt   ="선택된 회원을 삭제 하시겠습니까?";

            var checkcnt = $("input:checkbox[name='memidinfo[]']:checked").length;
            if(checkcnt <1){
                alert("선택된 회원 정보가 없습니다.");
                return;
            }

            if(confirm(confirmtxt)){
                document.updateConfirm.emode.value='delmember';
                document.updateConfirm.action="/coinbackapp/CoinbackMemberProc";
                document.updateConfirm.submit();
            }
        }
    </script>
</div>




</body>
</html>
