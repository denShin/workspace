
<div class="container" style="padding-top: 30px;padding-bottom: 70px;">
    <style>
        .box {
            width: 50%; height: 50%;
        }
        .table > tbody > tr > td {
            vertical-align:middle;
        }
    </style>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

        google.charts.load('current', {'packages':['bar']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            var data = google.visualization.arrayToDataTable(<?=$data["chartdata"]?>);
            var options = {
                title: '<?=$data["graphtitle"]?>'
                ,bars: 'horizontal'
            };
            var chart = new google.charts.Bar(document.getElementById('chartdiv'));
            chart.draw(data, google.charts.Bar.convertOptions(options));
        }
    </script>

    <div class="page-header clearfix">
        <h2 class="pull-left"><?=$data["pagetitle"]?> 정보</h2>

    </div>

    <div class="text-center">

        <div id="chartdiv" style="width: 900px; height: 500px;"></div>
    </div>


    <form id="updateConfirm">

        <div class="table-responsive">
            <table class="table  table-hover table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-3">날짜차이</th>
                    <th class="text-center col-md-9">예약수</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $startnum = $data["startnum"];
                foreach($data["list"] as $entry) {

                    ?>
                    <tr class="row">
                        <td  class="small text-center"><?=$entry["rdategap"]?> 일 </td>
                        <td  class="small text-center"><?=$entry["cnt"]?> 회</td>
                    </tr>
                    <?
                    $startnum--;
                }?>

                </tbody>
            </table>
        </div>
    </form>