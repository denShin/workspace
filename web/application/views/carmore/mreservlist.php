<div class="container" style="padding-top: 50px;padding-bottom: 70px;">

    <div class="clearfix"></div>
    <form name="updateConfirm" method="post">
        <input type="hidden" name="emode" value="changestats">
        <input type="hidden" name="stats" >

        <div class="table-responsive" style="width: 800px">
            <table class="table   table-bordered">
                <thead>
                <tr class="info row">
                    <th class="text-center col-md-1">Check</th>
                    <th class="text-center col-md-3">차량 대여정보</th>
                    <th class="text-center col-md-2">운전자</th>
                    <th class="text-center col-md-3">결제정보</th>
                    <th class="text-center col-md-2">회사정보</th>
                    <th class="text-center col-md-2">등록일</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($data["list"] as $entry) {
                    ?>
                    <tr class="text-center row">
                        <td><input type="checkbox" name="usrserial[]" value="<?= $entry["usrserial"] ?>"></td>
                        <td class="small text-left"   >
                            <span style="font-size: medium"><strong><?= $entry["model"] ?></strong></span>
                            <br><?= $entry["brand"] ?>  /<?= $entry["carType"] ?>/<?=$entry["age_info"]?> <span class="label label-primary searchStatus" data-status=""><?= $entry["f_rentstatus"] ?></span>
                            <br><?= $entry["car_number1"] ?> <?= $entry["car_number2"] ?> <?=$entry["car_number3"]?> /  <?=$entry["oil"]?> <?if($entry["color"] !=""){?>/  <?=$entry["color"]?><?}?>

                            <hr><span class="label label-default ">대여기간</span> <?= $entry["f_rentstartdate"] ?> ~  <?= $entry["f_rentenddate"] ?>
                            <?if($entry["f_delivpickaddr"] !=""){?>
                                <br><br> 배차주소 :<?=$entry["f_delivpickaddr"]?>
                                <br>회차주소 :<?=$entry["f_delivpickaddr"]?>
                            <?}?>

                        </td>
                        <td class="small text-left"   >
                            <span style="font-size: medium"><strong><?= $entry["driver_name_encrypt"] ?></strong></span>
                            <hr><span style="font-size: medium"><strong><?= $entry["driver_phone_encrypt"] ?></strong></span>
                            <BR>생년월일 : <?= $entry["driver_birthday_encrypt"] ?>
                            <BR><?= $entry["f_driveremail"] ?>
                        </td>
                        <td class="small text-left">
                            <?php
                            if($entry["f_paystatus"]=="2"){?>
                                <span style="font-size: small;color:red"><?=$entry["f_rentstatustr"]?></span><Br>
                            <?}?>

                            <span style="font-size: medium"><strong><?= $entry["f_totalprice"] ?></strong> 원</span>

                            <hr>총 단가 : <?= $entry["original_price"] ?> 원
                            <br>렌트비용 : <?= $entry["f_rentprice"] ?> 원 / 보험비용 : <?= $entry["f_insuprice"] ?> 원
                            <br>배송비용 : <?= $entry["f_delivprice"] ?> 원 / 사용포인트 : <span style="color: red">- <?= $entry["f_usepoint"] ?> </span>  원
                            <br>쿠폰 : <?=$entry["coupon_title"]?> <span style="color: red">- <?= $entry["coupon_price"] ?> </span>  원
                        </td>
                        <td class="small text-left"   >
                            <span style="font-size: medium"><strong><?= $entry["name"] ?></strong></span>
                            <br><?= $entry["branchName"] ?>
                        </td>
                        <td class="small text-center"><?= $entry["regdate"] ?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="text-center">
            <?=$data['pagination']?>
        </div>
    </form>


    <script type="text/javascript">

        function setstorememstats(stats){
            var checkcnt = $("input:checkbox[name='memidinfo[]']:checked").length;
            if(checkcnt <1){
                alert("선택된 회원 정보가 없습니다.");
                return;
            }
            var confirmtxt ="";
            if(stats=="y"){
                confirmtxt ="선택된 회원의 상태를 정상으로 변경하시겠습니까?";
            }else{
                confirmtxt ="선택된 회원의 상태를 차단으로 변경하시겠습니까?";
            }

            if(confirm(confirmtxt)){
                document.updateConfirm.stats.value=stats;
                document.updateConfirm.action="/coinbackapp/CoinbackMemberProc";
                document.updateConfirm.submit();
            }
        }


        function delmember(){
            var confirmtxt   ="선택된 회원을 삭제 하시겠습니까?";

            var checkcnt = $("input:checkbox[name='memidinfo[]']:checked").length;
            if(checkcnt <1){
                alert("선택된 회원 정보가 없습니다.");
                return;
            }

            if(confirm(confirmtxt)){
                document.updateConfirm.emode.value='delmember';
                document.updateConfirm.action="/coinbackapp/CoinbackMemberProc";
                document.updateConfirm.submit();
            }
        }
    </script>
</div>




</body>
</html>
