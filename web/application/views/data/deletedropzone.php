<?php
header('Content-Type: text/plain; charset=utf-8');

include ($_SERVER["DOCUMENT_ROOT"]."/static/lib/aws-sdk-php-master/vendor/autoload.php");
include ($_SERVER["DOCUMENT_ROOT"]."/application/config/aws_account.php");
include ($_SERVER["DOCUMENT_ROOT"]."/application/config/aws_dbcon.php");

use Aws\S3\S3Client;
use Aws\Credentials\Credentials;

$credentials = new Credentials($aws['AWS_KEY'], $aws['AWS_SECRET']);
$dbh = new PDO($aws_db["dns"], $aws_db["username"], $aws_db["password"]);

$S3 = new S3Client([
    'version' => 'latest',
    'credentials' => $credentials,
    'region' => $aws['S3_REGION']
]);


$deltype = $_GET["deltype"];
if($deltype==""){
    $deltype="list";
}

$fileSaveName = $_GET["fileSaveName"];
$backurl= $_GET["backurl"];
$board_code= $_GET["board_code"];
$board_idx= $_GET["board_idx"];
$page= $_GET["page"];
$content_code= $_GET["content_code"];
$thread_code = $_GET["thread_code"];
$bucket = $_GET["bucket"];
$fixoffer_idx = $_GET["fixoffer_idx"];

if($fileSaveName=="")
{
    $fileSaveName = $_POST["fileSaveName"];
    $backurl= $_POST["backurl"];
    $board_code= $_POST["board_code"];
    $board_idx= $_POST["board_idx"];
    $page= $_POST["page"];
    $content_code= $_POST["content_code"];
    $fixoffer_idx= $_POST["fixoffer_idx"];

    $deltype="write";
}
$bucket="carmoreweb";
$s3_key=$board_code."/".$content_code."/".$fileSaveName;

try{
    $S3->deleteObject( array(
        "Bucket" => $bucket,
        "Key" => $s3_key
    ));


} catch(S3Exception $e){
    //	print_r($e);
}

$sql="delete from admin_uploadfile where fileSaveName='$fileSaveName'";
$dbh->query($sql);

if($deltype=="list")
{
    switch ($backurl) {
        case 'event'  : $reurl ="/carmore/EventManage?ptype=v&board_idx=".$board_idx ; break;
        case 'carmst'  : $reurl ="/carmore/Carinfomaster?ptype=v&carinfokey=".$content_code ; break;
    }

    Header("Location:$reurl");
    exit();
}
?>