<?php
include ($_SERVER["DOCUMENT_ROOT"]."/static/lib/aws-sdk-php-master/vendor/autoload.php");
include ($_SERVER["DOCUMENT_ROOT"]."/application/config/aws_account.php");
include ($_SERVER["DOCUMENT_ROOT"]."/application/config/aws_dbcon.php");

use Aws\S3\S3Client;
use Aws\Credentials\Credentials;

$credentials = new Credentials($aws['AWS_KEY'], $aws['AWS_SECRET']);

$S3 = new S3Client([
    'version' => 'latest',
    'credentials' => $credentials,
    'region' => $aws['S3_REGION']
]);

$imageFolder = $_SERVER['DOCUMENT_ROOT']."/adminupload/files/";

$board_code = $_POST["board_code"];
$content_code = $_POST["content_code"];
$thread_code = $_POST["thread_code"];
$bucket = $_POST["bucket"];
$thumbyn = $_POST["thumbyn"];
$fileuptype = $_POST["fileuptype"];

function compress($source, $destination, $quality) {

    $info = getimagesize($source);

    if ($info['mime'] == 'image/jpeg')
        $image = imagecreatefromjpeg($source);

    elseif ($info['mime'] == 'image/gif')
        $image = imagecreatefromgif($source);

    elseif ($info['mime'] == 'image/png')
        $image = imagecreatefrompng($source);

    imagejpeg($image, $destination, $quality);

    return $destination;

}


$bucket="carmoreweb";


$mem_id = "admin";
$randinfo =rand(100, 900);
$savefilename =  $content_code."_".$randinfo;


//파일이 HTTP POST 방식을 통해 정상적으로 업로드되었는지 확인한다.
if(is_uploaded_file($_FILES["file1234"]["tmp_name"]))
{

    // 업로드 파일명
    $origin_filename = $_FILES["file1234"]["name"];
    $origin_filesize = $_FILES["file1234"]["size"];

    $sizekb = (int)($origin_filesize/1024);

    if($sizekb <200){
        $compressper=100;
    }else{

        $compressper= (int)((199/ $sizekb)*100);
    }


    $ext = substr(strrchr($origin_filename, '.'), 1);
    $ext = strtolower($ext);

    //저장 파일명
    $origin_filename = "origin_".$savefilename.".".$ext;
    $real_filename = $savefilename.".".$ext;

    $origindest = $imageFolder . $origin_filename;
    $dest = $imageFolder . $real_filename;
    $uploadfile_path=$dest;
    $tmpfilename = basename( $uploadfile_path);


    $s3_key=$board_code."/".$content_code."/".$real_filename;

    if(move_uploaded_file($_FILES["file1234"]["tmp_name"], $origindest))
    {
        //print_r($dest);
        //$d = compress($origindest, $dest, $compressper);
        //print_r($dest);
        //s3 업로드하기
        try {
            // Upload a file.
            $result = $S3->putObject(array(
                'Bucket' => $bucket,
                'Key' => $s3_key,
                'SourceFile' => $origindest,
                'ContentType' => getimagesize($origindest)['mime'],
                'ACL' => 'public-read',
                'StorageClass' => 'REDUCED_REDUNDANCY'
            ));

            $fileurl="https://s3.ap-northeast-2.amazonaws.com/".$bucket."/". $s3_key;
            if(file_exists( $dest  )) @unlink( $dest );
            if(file_exists( $origindest  )) @unlink( $origindest );

        } catch (S3Exception $e) {
            $fileurl="";
        }


        $dbh = new PDO($aws_db["dns"], $aws_db["username"], $aws_db["password"]);
        $dbh->exec("set names utf8");

        if($board_code=="carmst" || $board_code=="partners"){
            if($fileuptype=="main" ){
                $sql="delete from admin_uploadfile where   content_code='$content_code' and fileuptype='main'";
                $dbh->query($sql);
            }

        }


        $sql=" INSERT INTO admin_uploadfile
					(content_code, 	fileOrgName, fileSaveName,fileSize, fileType,fileuptype,mem_id,board_code)
					VALUES	('$content_code', '$origin_filename'
					, '$real_filename', '$origin_filesize', '$ext','$fileuptype','$mem_id','$board_code');";
        $dbh->query($sql);

        $json=array();
        $product=array();
        $product['fileOrgName']=$origin_filename;
        $product['fileSaveName']=$real_filename;
        $product['fileSize']=$origin_filesize;
        $product['fileType']=$ext;


        echo json_encode($product);

    }else{
        die("fail2");
    }
} else {
    echo "fail1, ".$_FILES["file1234"]["tmp_name"];
}



?>