<?php

include ($_SERVER["DOCUMENT_ROOT"]."/static/lib/aws-sdk-php-master/vendor/autoload.php");
include ($_SERVER["DOCUMENT_ROOT"]."/application/config/aws_account.php");

use Aws\S3\S3Client;
use Aws\Credentials\Credentials;



$credentials = new Credentials($aws['AWS_KEY'], $aws['AWS_SECRET']);

$S3 = new S3Client([
    'version' => 'latest',
    'credentials' => $credentials,
    'region' => $aws['S3_REGION']
]);


/*******************************************************
 * Only these origins will be allowed to upload images *
 ******************************************************/
$accepted_origins = array("http://localhost", "http://workspace.phonesos.kr");

/*********************************************
 * Change this line to set the upload folder *
 *********************************************/
$imageFolder = $_SERVER['DOCUMENT_ROOT']."/adminupload/images/";

$thread_code="";

$board_code = $_POST["board_code"];
$content_code = $_POST["content_code"];
$thread_code = $_POST["thread_code"];
$bucket = $_POST["bucket"];

$bucket="phonesosimage";

$timestamp = time();
$randinfo =rand(100, 900);
$savefilename =  $content_code."_".$randinfo;

reset ($_FILES);
$temp = current($_FILES);

if (is_uploaded_file($temp['tmp_name'])){
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        // same-origin requests won't set an origin. If the origin is set, it must be valid.
        if (in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)) {
            header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
        } else {
            header("HTTP/1.0 403 Origin Denied");
            return;
        }
    }



    // Verify extension
    if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png"))) {
        header("HTTP/1.0 500 Invalid extension.");
        return;
    }

    // Accept upload if there was no origin, or if it is an accepted origin
    $im = @getimagesize($temp['tmp_name']);
    if($im) {
        $width = $im[0];
        $height = $im[1];
        $sizestr = $width."x".$height;
    }


    $ext = substr(strrchr($temp['name'], '.'), 1);
    $ext = strtolower($ext);

    //저장 파일명
    $savefilename = $timestamp."_".$sizestr."-". $content_code."-".$randinfo;
    $real_filename = $savefilename.".".$ext;
    $filetowrite = $imageFolder . $real_filename;

    move_uploaded_file($temp['tmp_name'], $filetowrite);

    // Respond to the successful upload with JSON.
    // Use a location key to specify the path to the saved image resource.
    // { location : '/your/uploaded/image/file'}

    $filetowrite =$_SERVER["DOCUMENT_ROOT"]."/adminupload/images/". $real_filename;

    // s3 이동후 삭제
    $s3_key=$real_filename;


    try {
        // Upload a file.
        $result = $S3->putObject(array(
            'Bucket' => $bucket,
            'Key' =>$s3_key,
            'SourceFile' => $filetowrite,
            'ContentType' => mime_content_type($filetowrite),
            'ACL' => 'public-read',
            'StorageClass' => 'REDUCED_REDUNDANCY'
        ));

        $fileurl="https://s3.ap-northeast-2.amazonaws.com/".$bucket."/". $s3_key;
        if(file_exists( $filetowrite  )) @unlink( $filetowrite );


    } catch (S3Exception $e) {
        $fileurl="";
    }

    echo json_encode(array('location' => $fileurl,'filename'=> $real_filename));
} else {
    // Notify editor that the upload failed
    header("HTTP/1.0 500 Server Error");
}
?>