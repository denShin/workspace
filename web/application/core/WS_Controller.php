<?php
/**
 *
 * @property Admin_model $admin_model
 */

class WS_Controller extends CI_Controller {

    // 050 전화 관련 상수들
    const ONSE_COMPANY_ID   = '443';
    const ONSE_SYSTEM_ID    = '000';
    const ONSE_SEQUENCE     = '                    '; # 고정 20칸
    const ONSE_PACKET_REG   = '2501';    # 번호사용등록요구 패킷 > 3501
    const ONSE_PACKET_UNREG = '2502';    # 번호사용해지요구 패킷 > 3502
    const ONSE_PACKET_CHECK = '2600';    # 상태체크요구 패킷 > 3600
    const ONSE_RESULT_MSG   = '  ';    # 응답패킷 고정2칸
    const ONSE_METHOD       = '1';
    const ONSE_UES_FLAG     = '1';

    public function __construct()
    {
        parent::__construct();

        $this->load->model('admin/admin_model');
    }

    /**
     * @param $id string 이메일
     *
     * @return int
     */
    public function get_admin_idx($id)
    {
        return $this->admin_model->get_admin_idx($id);
    }
}