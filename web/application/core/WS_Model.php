<?php
/**
 * Created by PhpStorm.
 * User: teamo2_server
 * Date: 2019-04-10
 * Time: 11:12
 */

class WS_Model extends CI_Model {
    protected $slave;

    public function __construct()
    {
        parent::__construct();

        $this->slave = $this->load->database('slave', TRUE);
    }

    /**
     * 쿠폰정보 가져오기
     *
     * @author DEN
     *
     * @param $coupon_idx int
     *
     * @return stdClass
     */
    protected function get_coupon_info($coupon_idx)
    {
        $return_object = new stdClass();

        $query = "
        SELECT
            nci.title
        FROM    
            new_coupon_info nci INNER JOIN
            new_coupon_list ncl
            ON
                nci.serial=ncl.info_serial
        WHERE
            ncl.serial=?";

        $sql = $this->slave->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$coupon_idx]);
        if ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            $coupon_info = new stdClass();

            $coupon_info->name = $stmt['title'];

            $return_object->result = 1;
            $return_object->info   = $coupon_info;

            return $return_object;

        }
        else
        {
            $return_object->result = 2;

            return $return_object;

        }

    }


    /**
     * 유저아이디 값으로 유저정보 가져오기
     *
     * @author DEN
     *
     * @param $user_idx int
     *
     * @return stdClass
     */
    protected function get_carmore_user_info($user_idx)
    {
        $return_object = new stdClass();

        $query = "SELECT tul_carmore_use_cnt, tul_review_cnt, kakaoid FROM tbl_usr_list WHERE usrserial=?";

        $sql = $this->slave->conn_id->prepare($query, [PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL]);
        $sql->execute([$user_idx]);
        if ($stmt = $sql->fetch(PDO::FETCH_ASSOC))
        {
            $user_info = new stdClass();

            $user_info->kakaoId   = $stmt['kakaoid'];
            $user_info->useCnt    = $stmt['tul_carmore_use_cnt'];
            $user_info->reviewCnt = $stmt['tul_review_cnt'];

            $return_object->result = 1;
            $return_object->info   = $user_info;

            return $return_object;
        }
        else
        {
            $return_object->result = 2;

            return $return_object;

        }
    }
}
