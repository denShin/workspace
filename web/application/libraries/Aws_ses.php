<?php
/**
 * Aws_ses.php - AWS 이메일 발송 처리 클래스
 */

include ($_SERVER["DOCUMENT_ROOT"]."/static/lib/aws-sdk-php-master/vendor/autoload.php");

use Aws\Ses\SesClient;
use Aws\Credentials\Credentials;

class Aws_ses{

    private $SES;

    public function __construct()
    {
        $credentials = new Credentials(AWS_KEY, AWS_SECRET);

        $this->SES = SesClient::factory(array(
            'credentials' => $credentials,
            'region' => 'us-west-2',
            'version' => 'latest'
        ));

    }

    // 일반 이메일 발송
    public function sendEmail($arr_dest,$title,$content)
    {
        $msg = array();
        print_r($arr_dest);
        $msg['Source'] = "su@teamo2.kr";
        $msg['Destination']['ToAddresses']=$arr_dest;
        $msg['Message']['Subject']['Data'] =$title;
        $msg['Message']['Subject']['Charset'] = "UTF-8";
        $msg['Message']['Body']['Html']['Data'] =$content ;
        $msg['Message']['Body']['Html']['Charset'] = "UTF-8";

        try{
            $result = $this->SES->sendEmail($msg);

            //save the MessageId which can be used to track the request
            $msg_id = $result->get('MessageId');

            print_r($msg_id);
        } catch (Exception $e) {
            //An error happened and the email did not get sent
            echo($e->getMessage());
        }
    }

    // 정산전용 이메일 발송 
    public function sendCalculateEmail($arr_dest,$title,$content)
    {
        $arr_bccdest=array("sean@tm2.kr","harry@tm2.kr","hs@tm2.kr","su@tm2.kr");
        $msg = array();
        $msg['Source'] = "su@teamo2.kr";
        $msg['Destination']['ToAddresses']=$arr_dest;
        $msg['Destination']['BccAddresses']=$arr_bccdest;

        $msg['Message']['Subject']['Data'] =$title;
        $msg['Message']['Subject']['Charset'] = "UTF-8";
        $msg['Message']['Body']['Html']['Data'] =$content ;
        $msg['Message']['Body']['Html']['Charset'] = "UTF-8";

        try{
            $result = $this->SES->sendEmail($msg);

            //save the MessageId which can be used to track the request
            $msg_id = $result->get('MessageId');

            print_r($msg_id);
        } catch (Exception $e) {
            //An error happened and the email did not get sent
            echo($e->getMessage());
        }
    }




}