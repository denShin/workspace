<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 비즈톡 템플릿 관리
 */

class Biztalk_template {

    private $template_inventory = [
        "C00002" => "#{decrypt_driver_name}님 예약완료 안내
예약번호 : #{idx}
예약자명 : #{decrypt_driver_name}
예약시작 : #{rentstart_date}
예약종료 : #{rentend_date}
차량정보 : #{carName}
예약업체 : #{company_name} #{branch_name}
업체연락처 : #{branch_tel}
결제금액 : #{total_cost}원 (차량대여료 #{rental_cost}원, 자차요금 #{cdw_cost}원, 딜리버리비용 #{delivery_cost}원, 할인요금 #{discount}원)
결제방법 : 신용카드/체크카드
배차주소 : #{pickup_address}
반차주소 : #{return_address}

[보험규정]
자차보험 : #{cdw_name}
보상한도 : #{cdw_compensation}
*소모품, 실내부품 포함 일부 항목은 자차보험 미적용

[요금 환불규정]
대여 24시간 이전 : 수수료 없음
대여 24시간 이내 : 수수료 10%
*차량 인수 후 예약취소 또는 조기반납시 결제금액 환불불가

※운행 중 문제 발생시 업체로 전화주세요.
Tel: #{branch_tel}",

        "C00004" => "[카모아 - 취소되었습니다]
#{driver_name}님 취소완료 안내

예약번호 : #{idx}
예약자명 : #{decrypt_driver_name}
예약시작 : #{rentstart_date}
예약종료 : #{rentend_date}
차량정보 : #{carName}
자차 : #{insuranceName} (보상한도 #{insuranceCompensation})

[결제취소정보]
차량대여료: #{rental_cost}원
자차요금: #{cdw_cost}원
딜리버리비용: #{delivery_cost}원
할인요금 : #{discount}원
총 결제요금 : #{amount}원

총 취소/환불금액 : #{cancelAmt}원
취소수수료 차감 : #{email_penalty}원

카모아를 이용해주셔서 감사합니다",

        "C00006" => "[카모아] 관리자님, 새로운 예약 1건이 도착했습니다.

예약번호 : #{idx}
예약자명 : #{decrypt_driver_name}
연락처 : #{decrypt_driver_phone}
예약시작 : #{rentstart_date}
예약종료 : #{rentend_date}
이용일수 : #{use_time}
차량정보 : #{carName}
자차: #{cdw_name} (보상한도 #{cdw_compensation})

[결제정보]
차량대여료: #{rental_cost}원
자차요금: #{cdw_cost}원
딜리버리비용: #{delivery_cost}원

총 결제요금: #{total_cost}원

■파트너스에서 상세내용을 확인하세요!
https://carmore.kr/partners/introduce/main.html",

        "C00007" => "[카모아 - 반납시간 안내]
#{driverName}님 즐거운 여행이 되셨나요?
차량 반납시간까지 2시간 남았습니다.
다음 고객을 위해 시간에 맞춰 반납 부탁드립니다.

이용해 주셔서 감사합니다 ^^",

        "C00008" => "[카모아] 관리자님, 배차 2시간 전 안내드립니다.

예약번호 : #{idx}
예약자명 : #{driverName}
연락처 : #{driverPhone}
예약시작 : #{startDate}
예약종료 : #{endDate}
이용일수 : #{useTime}
배차주소 : #{pickup_address}
반차주소 : #{return_address}
차량정보 : #{carName}
자차: #{cdw_name} (보상한도 #{cdw_compensation})",

        "C00014" => "#{decrypt_driver_name}님,

카모아입니다!
업체에서 열심히 차량준비 중입니다.
3시간 후에 잊지말고 수령해주세요. ^^

궁금하신 점은 카카오톡 '카모아'로 문의주세요!",

        "C001UC-3" => "[카모아 - 예약되었습니다] 

#{decrypt_driver_name}님 예약완료 안내 
예약번호 : #{idx} 
예약자명 : #{decrypt_driver_name} 
예약시작 : #{rentstart_date} 
예약종료 : #{rentend_date} 
출발항구 : #{port} 
배편시간 : #{time} 
나가는항구 : #{outport} 
차량정보 : #{carName} 
예약업체 : #{company_name} #{branch_name} 
업체연락처 : #{branch_tel} 
결제금액 : #{total_cost}원 (차량대여료 #{rental_cost}원, 자차요금 #{cdw_cost}원, 딜리버리비용 #{delivery_cost}원, 할인요금 #{discount}원) 
결제방법 : 신용카드/체크카드 

[보험규정] 
자차보험 : #{cdw_name} 
보상한도 : #{cdw_compensation} 
*소모품, 실내부품 포함 일부 항목은 자차보험 미적용 

[요금 환불규정} 
- 1주일 이전 : 100% 환불 
- 1주일 이내 : 70% 환불 
- 3일 이내 : 50% 환불 
- 당일 취소 : 취소 불가(환불 불가) 
- 자연재해로 인한 결박 : 결박 확인서를 발급받아 카모아 카톡으로 전달 후 100% 취소. 

※ 항구에 내리셔서 검정 쏠라티 차량을 타고 근처 차고지로 이동하셔서 차량을 인수받으시면 됩니다. 
셔틀 운행 담당자 연락처 : #{number} 

※동시에 항구에 배가 도착할 때는 피치 못할 사정으로 차량 인수가 30~1시간 지연될 수 있습니다. 
전날 미리 울릉도에 도착한 고객께서는 고객센터로 연락 주시면 도와드리겠습니다. 

※운행 중 문제 발생시 업체로 전화주세요. 
Tel: #{branch_tel}",

        "C003UM" => "[카모아] #{company_name} 관리자님, 새로운 예약 1건이 도착했습니다. 

예약번호 : #{idx} 
예약자명 : #{decrypt_driver_name} 
연락처 : #{decrypt_driver_phone} 
예약시작 : #{rentstart_date} 
예약종료 : #{rentend_date} 
출발항구 : #{port} 
배편시간 : #{time} 
나가는항구 : #{outport} 
차량정보 : #{carName} 
자차: #{cdw_name} (보상한도 #{cdw_compensation}) 

[결제정보] 
차량대여료: #{rental_cost}원 
자차요금: #{cdw_cost}원 
딜리버리비용: #{delivery_cost}원 

총 결제요금: #{total_cost}원 

■파트너스에서 상세내용을 확인하세요! 
https://carmore.kr/partners/introduce/main.html",

        "C00IAM" => "[카모아] #{company_name} 관리자님, 새로운 예약 1건이 도착했습니다. 

예약번호 : #{idx} 
예약자명 : #{decrypt_driver_name} 
운전자 생년월일 : #{decrypt_driver_date} 

예약시작 : #{rentstart_date} 
예약종료 : #{rentend_date} 
이용일수 : #{use_time} 
배차주소 : #{pickup_address} 
반차주소 : #{return_address} 

차량명 : #{carName} 
연식 : #{car_year} 
연료 : #{fuel} 
차량옵션 : #{option} 
종합보험 연령 : #{insurance_age} 
자차: #{cdw_name} (보상한도 #{cdw_compensation}) 

[결제정보] 
차량대여료: #{rental_cost}원 
자차요금: #{cdw_cost}원 
딜리버리비용: #{delivery_cost}원 
할인금액: #{discount_cost}원 (카모아부담) 

총 결제요금: #{total_cost}원 

■파트너스에서 상세내용을 확인하세요! 
https://carmore.kr/partners/introduce/main.html",

        "C00JAC-1" => "[카모아 - 예약되었습니다] 

#{decrypt_driver_name}님 예약완료 안내 
예약번호 : #{idx} 
예약자명 : #{decrypt_driver_name} 
예약시작 : #{rentstart_date} 
예약종료 : #{rentend_date} 
차량정보 : #{carName} 
예약업체 : #{company_name} #{branch_name} 
업체연락처 : #{branch_tel} 
결제금액 : #{total_cost}원 (차량대여료 #{rental_cost}원, 자차요금 #{cdw_cost}원, 딜리버리비용 #{delivery_cost}원, 할인요금 #{discount}원) 
결제방법 : 신용카드/체크카드 

[보험규정] 
자차보험 : #{cdw_name} 
보상한도 : #{cdw_compensation} 
* 타이어, 휠, 현장출동 서비스, 침수사고, 실내부품, 견인비, 사이드미러, 에어백, 기타소모품에 대해서는 자차보험 미적용 
* 자차보험은 단독사고시 적용 불가합니다.",

        "C00JAC-2" => "예약하신 #{decrypt_driver_name}님, 

[셔틀장소 안내] 
공항 도착 후 '5번 게이트 맞은편'으로 나가셔서 우측 렌트카 하우스 앞 셔틀주차장 (#{n}구역 #{n_1}번) 자리에서 #{company_name}셔틀 타고 오시면 됩니다. 

* 셔틀운행 간격은 #{shuttle_gap}입니다. 
* 차량 인수시 면허증 '필수지참'입니다. 
* 면허증 미소지시 공항 4번 게이트로 나가셔서 자치경찰대 방문하시어 본인확인 후 면허번호 받아오시면 됩니다.",

        "C00JAM" => "[카모아] #{company_name} 관리자님, 새로운 예약 1건이 도착했습니다. 

예약번호 : #{idx} 
예약자명 : #{decrypt_driver_name} 
연락처 : #{decrypt_driver_phone} 
예약시작 : #{rentstart_date} 
예약종료 : #{rentend_date} 
이용일수 : #{use_time} 
차량정보 : #{carName} 
자차: #{cdw_name} (보상한도 #{cdw_compensation}) 

[결제정보] 
차량대여료: #{rental_cost}원 
자차요금: #{cdw_cost}원 
딜리버리비용: #{delivery_cost}원 

총 결제요금: #{total_cost}원 

■파트너스에서 상세내용을 확인하세요! 
https://carmore.kr/partners/introduce/main.html",

        "C00MAM" => "[카모아] 관리자님, 월렌트 예약 1건이 도착했습니다. 

예약번호 : #{idx} 
예약자명 : #{decrypt_driver_name} 
연락처 : #{decrypt_driver_phone} 
예약시작 : #{rentstart_date} 
예약종료 : #{rentend_date} 
이용일수 : #{use_time} 
배차주소 : #{pickup_address} 
반차주소 : #{return_address} 
차량정보 : #{carName} 
자차: #{cdw_name} (보상한도 #{cdw_compensation}) 

[결제정보] 
차량대여료: #{rental_cost}원 
자차요금: #{cdw_cost}원 
보증금: #{deposit}원 

총 결제요금: #{total_cost}원 

■파트너스에서 상세내용을 확인하세요! 
https://carmore.kr/partners/introduce/main.html",

        "C00MIC-1" => "[카모아 - 월렌트 예약완료] 
#{decrypt_driver_name}님 예약완료 안내 
예약번호 : #{idx} 
예약자명 : #{decrypt_driver_name} 
예약시작 : #{rentstart_date} 
예약종료 : #{rentend_date} 
차량정보 : #{carName} 
예약업체 : #{company_name} #{branch_name} 
업체연락처 : #{branch_tel} 
결제금액 : #{total_cost}원 (차량대여료 #{rental_cost}원, 자차요금 #{cdw_cost}원, 보증금 #{deposit}원, 할인요금 #{discount}원) 
결제방법 : 신용카드/체크카드 
배차주소 : #{pickup_address} 
반차주소 : #{return_address} 

▶보증금은 현장에서 현금결제로 진행됩니다. 

[보험규정] 
자차보험 : #{cdw_name} 
보상한도 : #{cdw_compensation} 
*소모품, 실내부품 포함 일부 항목은 자차보험 미적용 

[요금 환불규정] 
대여 7일전 : 수수료 없음 
대여 7일 이내 : 수수료 5% 
대여 3일 이내 : 수수료 10% 
*차량 인수 후 예약취소 또는 조기반납시 이용하신 일별로 환불금액이 다르오니 자세한 사항은 카모아 앱에서 확인해주세요. 

※운행 중 문제 발생시 업체로 전화주세요. 
Tel: #{branch_tel}",

        "C00MRC" => "[카모아 - 반납/연장 안내] 

#{driverName}님 이용해주셔서 감사합니다. 
차량 반납일까지 #{n}일 남았습니다. 
대여 연장은 카모아 앱 하단메뉴 \"My렌트\"에서 가능합니다! 
https://carmore.app.link/LQekSFT1iQ 

궁금한 점은 카카오톡 '카모아'로 문의주세요!",

        "C00SIM" => "[카모아] 관리자님, 새로운 예약 1건이 도착했습니다. 

예약번호 : #{idx} 
예약자명 : #{decrypt_driver_name} 
연락처 : #{decrypt_driver_phone} 
운전자 생년월일 : #{decrypt_driver_date} 
회사정보(지점) : #{company_name} #{branch_name} 
예약시작 : #{rentstart_date} 
예약종료 : #{rentend_date} 
이용일수 : #{use_time} 
배차주소 : #{pickup_address} 
반차주소 : #{return_address} 
차량정보 : #{carName} 
연식 : #{car_year} 
종합보험 연령 : #{insurance_age} 
연료 : #{fuel} 
차량옵션 : #{option} 
자차: #{cdw_name} (보상한도 #{cdw_compensation}) 

[결제정보] 
차량대여료: #{rental_cost}원 
자차요금: #{cdw_cost}원 
딜리버리비용: #{delivery_cost}원 
할인금액: #{discount_cost}원 (카모아부담) 

총 결제요금: #{total_cost}원 

■파트너스에서 상세내용을 확인하세요! 
https://carmore.kr/partners/introduce/main.html",

        "C00UBC" => "#{decrypt_driver_name}님, 

카모아 입니다! 
#{rentstart_date}은 렌트카 예약일 입니다. 
잊지말고 렌트카를 수령해 주세요. 

궁금하신 점은 카카오톡 '카모아'로 문의주세요!",

        "C00UBC-1" => "#{decrypt_driver_name}님, 

카모아입니다! 
업체에서 열심히 차량준비 중입니다. 
3시간 후에 잊지말고 수령해주세요. ^^ 

※ 렌트하기 전에 꼭 확인하세요! 
1. 차량 인수 장소 
2. 인수/반납 시 내외관 촬영 
3. 기름은 쓴 만큼 채우기 
4. 영업 시간 외 인수/반납이라면? -> 업체와 사전 조율 

자세한 내용이 궁금하다면? 
아래 링크에서 확인해보세요! 

▶︎http://bit.ly/2MwAhNz 

※ 궁금한 점은 카카오톡 '카모아'로 알려주세요 ;)",

        "C00UBC-2" => "#{decrypt_driver_name}님, 

카모아입니다! 
(#{rentstart_date})은 렌트카 예약일 입니다. 
잊지말고 렌트카를 수령해 주세요. 

※ 렌트하기 전에 꼭 확인하세요! 
1. 차량 인수 장소 
2. 인수/반납 시 내외관 촬영 
3. 기름은 쓴 만큼 채우기 
4. 영업 시간 외 인수/반납이라면? -> 업체와 사전 조율 

자세한 내용이 궁금하다면? 
아래 링크에서 확인해보세요! 

▶︎http://bit.ly/2MwAhNz 

※ 궁금한 점은 카카오톡 '카모아'로 알려주세요 ;)",

        "C00URC" => "예약하신 #{decrypt_driver_name}님 

[이용평점/후기 안내] 
#{decrypt_driver_name} 님, 이제 카모아 앱에서 이용 후기를 남기실 수 있습니다. 
방법 안내드립니다. 

My렌트>이용한 차량 내역 클릭 

감사합니다. 

▶카모아 바로가기 
https://carmore.app.link/8L13q2hsoS",

        "C00MRM" => "[카모아 - 월렌트 반납안내] 

관리자님, 월렌트 반납 #{N}일 전 입니다. 
#{고객명} 고객님이 이용 연장을 원하시는지 확인해 주세요! 
카모아에서 고객이 직접 연장 결제가 가능합니다.:) 
연장 안할 시 반납 후 파트너스에서 예약상태를 반납으로 변경해주세요! 
(다음 예약을 위해 꼭 반납 상태로 변경해놓으셔야 합니다!) 

------------------------------------------------------------------------------------- 

예약번호 : #{idx} 
예약자명 : #{decrypt_driver_name} 
연락처 : #{decrypt_driver_phone} 
예약시작 : #{rentstart_date} 
예약종료 : #{rentend_date} 
이용일수 : #{use_time} 
반차주소 : #{return_address} 
차량정보 : #{carName} 
자차: #{cdw_name} (보상한도 #{cdw_compensation}) 

[결제정보] 
차량대여료: #{rental_cost}원 
자차요금: #{cdw_cost}원 
보증금: #{deposit}원 

총 결제요금: #{total_cost}원 

■파트너스에서 상세내용을 확인하세요! 
https://carmore.kr/partners/introduce/main.html"
    ];


    /**
     * 코드로 템플릿 가져오기
     *
     * @param $code
     *
     * @return string
     */
    public function get_template($code)
    {
        return $this->template_inventory[$code];
    }
}