<?php
/**
 * Aws_s3.php - S3 파일 업로드,삭제 처리 클래스
 */
include ($_SERVER["DOCUMENT_ROOT"]."/static/lib/aws-sdk-php-master/vendor/autoload.php");

use Aws\S3\S3Client;
use Aws\Credentials\Credentials;


class Aws_s3
{

    private $S3;

    public function __construct()
    {
        $credentials = new Credentials(AWS_KEY, AWS_SECRET);

        $this->S3 = new S3Client([
            'version' => 'latest',
            'credentials' => $credentials,
            'region' => S3_REGION
        ]);

    }

    public function set_S3File($bucketname, $savename, $filepath)
    {
        $returnv = "";

        try {
            // Upload a file.
            $result = $this->S3->putObject(array(
                'Bucket' => $bucketname,
                'Key' => $savename,
                'SourceFile' => $filepath,
                'ContentType' => mime_content_type($filepath),
                'ACL' => 'public-read',
                'StorageClass' => 'REDUCED_REDUNDANCY'
            ));

            $returnv = "y";

        } catch (S3Exception $e) {
            $returnv = "n";
        }
        return $returnv;
    }

    public function del_S3Folder($bucketname, $foldername)
    {
        $returnv = "";

        try{

            $this->S3->deleteMatchingObjects($bucketname, $foldername);
            $returnv = "y";

        } catch(S3Exception $e){
            echo $e;
            $returnv = "n";
        }

        return $returnv;
    }

    public function del_S3File($bucketname, $savename)
    {
        $returnv = "";

        try{
            $this->S3->deleteObject( array(
                "Bucket" => $bucketname,
                "Key" => $savename
            ));
            $returnv = "y";

        } catch(S3Exception $e){
            $returnv = "n";
        }

        return $returnv;
    }
}