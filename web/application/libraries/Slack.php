<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Slack.php - Slack 처리 클래스
 */


class Slack
{
    private $token;
    private $channel;
    private $username;
    private $message;
    private $iconEmoji;
 
    public function __construct($dtoken, $dusername='예약이') {
        $this->token    = $dtoken;
        $this->username = $dusername;
    }
 
    public function setChannel($channel) {
        $this->channel = $channel;
    }
 
    public function setUsetName($username) {
        $this->username = $username;
    }
 
    public function setMessage($message) {
        $this->message = $message;
    }
 
    public function setIconEmoji($iconEmoji) {
        $iconEmoji = strip_tags(trim($iconEmoji));
        
        if ($iconEmoji)
            $this->iconEmoji = $iconEmoji;       
    }
 
    public function send() {
        $postData = array(
            'token'    => $this->token,
            'channel'  => $this->channel,
            'icon_emoji' => $this->iconEmoji,
            'username' => $this->username,
            'text'     => $this->message
        );
 
        $ch = curl_init("https://slack.com/api/chat.postMessage");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,  'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
 
        return $result;
    }
}
