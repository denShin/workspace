<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: teamo2_server
 * Date: 2019-04-09
 * Time: 18:50
 *
 * 공통으로 사용되는 함수
 */

class Workspace_common_func {
    /**
     * 들어온 변수에 대해서 Injection 방지대책
     *
     * @author <den@tm2.kr> Den
     *
     * @param $string  string  들어온 스트링
     * @param $boolean BOOLEAN check_false_value 함수 활용하는지
     *
     * @return string
     */
    public function check_sql_injection($string, $boolean = FALSE)
    {
        return ($boolean)?
            preg_replace('/(\')|(\;)|(--)|(1=1)|(or 1=1)|(sp_)|(xp_)|(@variable)|(@@variable)|(exec)|(sysobject)/', '', $this->check_false_value($string, TRUE)) :
            preg_replace('/(\')|(\;)|(--)|(1=1)|(or 1=1)|(sp_)|(xp_)|(@variable)|(@@variable)|(exec)|(sysobject)/', '', $string);
    }

    /**
     * 들어온 변수가 false 이면 공백 리턴 해주는 함수
     *
     * @author Den
     *
     * @param  boolean|string $param1       들어오는 변수 (boolean 혹은 스트링)
     * @param  boolean        $whether_trim 트림 정제가 필요한지
     *
     * @return string                       가공해서 반환해주는 값
     */
    public function check_false_value($param1, $whether_trim)
    {
        if ( ! $param1)
            return "";
        else
            return ($whether_trim)? trim($param1) : $param1;
    }

    /**
     * 들어온 변수를 숫자만 남겨놓는 함수
     *
     * @author Den
     *
     * @param  string $string
     * @return string
     */
    public function remain_only_number($string)
    {
        return preg_replace('/[^0-9]/', '', $string);
    }

    /**
     * 휴대폰 번호 포맷 정규식
     *
     * @author Den
     *
     * @param  string $phone 휴대폰 번호
     * @return string        포맷팅되어 나온 번호
     */
    public function format_tel($phone)
    {
        $phone  = $this->remain_only_number($phone);

        switch (strlen($phone))
        {
            case 8:
                return preg_replace('/([0-9]{4})([0-9]{4})/', '$1-$2', $phone);
                break;
            case 11:
                return preg_replace('/([0-9]{3})([0-9]{4})([0-9]{4})/', '$1-$2-$3', $phone);
                break;
            case 10:
                return preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '$1-$2-$3', $phone);
                break;
            case 12:
                return preg_replace('/([0-9]{4})([0-9]{4})([0-9]{4})/', '$1-$2-$3', $phone);
                break;
            default:
                return $phone;
                break;
        }
    }

    /**
     * 제주 API 한글화
     *
     * @author Den
     *
     * @param $api    int API 플래그
     *
     * @return string
     */
    public function api_flag_to_value($api)
    {
        switch ((int)$api)
        {
            case 1:
                return "그림";
            case 2:
                return "인스";
            case 3:
                return "리본";
            default:
                return "";
        }
    }

    /**
     * br 태그 textarea 에 쓸 수 있는 값으로
     *
     * @param $str
     *
     * @return string
     */
    public function replace_br_to_front($str)
    {
        return preg_replace('/<br>/', "\r\n", $str);
    }

    /**
     * API 예약상태 글자로 변환
     *
     * @author DEN
     *
     * @param $state  int
     *
     * @return string
     */
    public function api_reserv_flag_to_string($state)
    {
        switch ((int)$state)
        {
            case -1:
                return "카드결제전";
            case 0:
                return "예약완료";
            case 1:
                return "대여중";
            case 10:
                return "반납완료";
            case 11:
                return "결제취소";
        }
    }

    /**
     * 성별 플래그를 글자로 변환
     *
     * @author DEN
     *
     * @param $gender int
     *
     * @return string
     */
    public function gender_flag_to_string($gender)
    {
        switch ((int)$gender)
        {
            case 1:
            case 3:
                return "남";
            case 2:
            case 4:
                return "여";
            default:
                return "";
        }
    }

    /**
     * UTC Timezone 을 한국 시간으로
     *
     * @param $date datetime Y-m-d H:i:s
     *
     * @return string
     */
    public function utc_to_kr($date)
    {
        return (empty($date))? "" : date('Y-m-d H:i:s', strtotime("+9 hours", strtotime($date)));
    }

    /**
     * 한국 시간을 UTC Timezone 으로
     *
     * @param $date datetime Y-m-d H:i:s
     *
     * @return string
     */
    public function kr_to_utc($date)
    {
        return (empty($date))? "" : date('Y-m-d H:i:s', strtotime("-9 hours", strtotime($date)));
    }
}
