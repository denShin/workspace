<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Customfunc.php - 사용자 정의 함수 클래스
 */

class Customfunc  {

    // 컨텐츠 코드에 따른 이미지 리스트가져오기
    function get_imagelist($content_code,$fileuptype){
        $CI =& get_instance();

        $sql="select fileSaveName from admin_uploadfile where content_code='$content_code'
                and  fileuptype='$fileuptype'  order by uploadfile_infoidx asc ";

        $query = $this->db->query($sql)->result_array();

        return $query ;
    }

    // 지역정보 -select 데이터 가져오기
    function get_areainfo($part,$city_value){
        $CI =& get_instance();

        if($part=="sido"){
            $sql=" select city as name,city_value as value From new_area group by city_value";
        }else if($part=="gugun"){
            $sql="  select township as name ,ct_value as value  From new_area where city_value='$city_value' group by  ct_value";

        }

        $row = $CI->db->query($sql)->result_array() ;
        return $row;
    }

    // 지역정보 - option 출력하기
    function get_areaselectoption($checkval,$option,$part,$city_value){

        $arrcheckval = explode(",",$checkval);
        $checkval =" ".$checkval;
        $selectarr = $this->get_areainfo($part,$city_value);
        $returnstr="";
        if($option=="select"){
            $returnstr="<option value=''>====선택====</option>";
        }


        $i=1;
        foreach ($selectarr as $area) {

            $name = $area["name"];
            $value = $area["value"];


            $insyn =false;


            if($option=="select"){
                if(strpos($checkval, $value)){
                    $insyn =true;
                }


                if($insyn==true){
                    $selected = "selected";
                }else{
                    $selected = "";
                }
                $returnstr=$returnstr."<option value='$value' $selected >$name</option>";
            }else{

                foreach ($arrcheckval as $item){

                    if($value ==$item){
                        $insyn =true;
                    }
                }

                if($insyn==true){
                    $checked = " checked";
                }else{
                    $checked = "";
                }
                $returnstr=$returnstr."<input type='checkbox' name='service_location[]' class='form-check-input' value='$value' $checked > $name &nbsp;";

                if( ($i %4)==0  ){
                    $returnstr=$returnstr."<BR>";
                }
            }
            $i++;
        }

        return $returnstr;
    }

    // 각 기업 050 번호 가져오기
    function get_tel050number($idx,$part){

        $CI =& get_instance();
        $sql="select tel050_number from carmore_050info where fk_serial='$idx' and tel050_part='$part' and tel050_useyn='y' limit 1";

        $tel050_number = $CI->db->query($sql)->row()->tel050_number;
        return $tel050_number;
    }

    // 050 번호 추천,초기세팅
    function get_suggest050number($part){

        if($part =="in"){
            $wsql =" and tel050_shortnum < 5000 ";
        }else{
            $wsql =" and tel050_shortnum > 4999 ";
        }

        $CI =& get_instance();
        $sql="select tel050_shortnum from carmore_050info where  tel050_useyn='n' and tel050_goldyn='n'
                                              and expire < 10 $wsql order by tel050_shortnum asc limit 1";

        $tel050_number = $CI->db->query($sql)->row()->tel050_shortnum;

        return $tel050_number;
    }


    // 지역코드->지역 이름으로 리턴
    function get_areatxt($areacode,$codetype){


        $CI =& get_instance();

        if($codetype=="sido"){$col ="city"; $qcol ="city_value"; }
        else if($codetype=="gugun"){$col ="township"; $qcol ="ct_value"; }

        $tmp_arrcode = explode(",",$areacode);

        for($i=0;$i< sizeof($tmp_arrcode);$i++){
            $tval=$tmp_arrcode[$i];
            $sql="select ".$col." as areavalue from new_area where ".$qcol." ='".$tval."' limit 1";

            $area[] = $CI->db->query($sql)->row()->areavalue;


        }

        $result = join(",",$area);

        return $result;
    }
    

    // 휴대폰 번호 포맷 정규식 
    function format_phone($phone){
        $phone = preg_replace("/[^0-9]/", "", $phone);
        $length = strlen($phone);

        switch($length){
        case 11 :
            return preg_replace("/([0-9]{3})([0-9]{4})([0-9]{4})/", "$1-$2-$3", $phone);
            break;
        case 10:
            return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "$1-$2-$3", $phone);
            break;
        case 12:
            return preg_replace("/([0-9]{4})([0-9]{4})([0-9]{4})/", "$1-$2-$3", $phone);
            break;
        default :
            return $phone;
            break;
        }
    }

    // 취소 수수료 계산 함수
    function getcancelfee($f_reservationidx,$canceldate ){

        date_default_timezone_set('Asia/Seoul');
        $canceldate =$canceldate/1000;


        $CI =& get_instance();
        $sql="select * from tbl_reservation_list where f_reservationidx='$f_reservationidx'";
        $revrs = $CI->db->query($sql);

        //select original_price, f_totalprice, f_rentprice ,f_usepoint from tbl_reservation_list where f_reservationidx=12862
        foreach($revrs->result_array() AS $row)
        {
            $reserv_rent_type=$row['reserv_rent_type'];
            $reserv_area_code	=$row['reserv_area_code'];
            $original_price	=$row['original_price']; //총 렌트금액
            $f_totalprice	=$row['f_totalprice']; //카드결제 총액
            $f_usepoint	=$row['f_usepoint']; //포인트사용 총액

            $reserv_area_code	=$row['reserv_area_code'];
            $f_rentstartdate =strtotime( $row['f_rentstartdate']);
            $f_rentenddate =strtotime( $row['f_rentenddate']);
            $f_regdate =strtotime( $row['f_regdate']);

            /*
            echo "areacode:".$reserv_area_code."<BR>";
            echo "reserv_rent_type:".$reserv_rent_type."<BR>";
            echo "start :".$row['f_rentstartdate'].", time :".$f_rentstartdate ."<BR>";
            echo "end :".$row['f_rentenddate'].", time :".$f_rentenddate."<BR>";
            echo "regdate :".$row['f_regdate'].", time :".$f_regdate."<BR>";
            */

        }

        //  결제후 지난 시간
        $paytimegap = (int)(($canceldate-$f_regdate)/60/60);

        // 시작시간까지 남은 시간
        $starttimegap = (int)(($f_rentstartdate-$canceldate)/60/60);

        // 시작시간까지 남은 날짜
        $startdaygap = (int)(($f_rentstartdate-$canceldate)/60/60/24);

        //echo "paytimegap:".$paytimegap."<br>,startdaygap:".$startdaygap."<br>,starttimegap:".$starttimegap;

        // $arr_result 배열 초기화
        $arr_result["original_price"]=$original_price;
        $arr_result["f_totalprice"]=$f_totalprice;
        $arr_result["f_usepoint"]=$f_usepoint;

        //1. 예약일 초과 - 취소불가
        if($startdaygap < 0){
            $arr_result["result"]="n";
            $arr_result["code"]="startover";
            $arr_result["return"]="0";
            return $arr_result;
        }

        // 2. 1시간 이내는 전액 환불
        if($paytimegap < 1){
            $arr_result["result"]="y";
            $arr_result["code"]="";
            $arr_result["return"]="100";
            return $arr_result;
        }

        // 3. 울릉도인가 아닌가?
        if($reserv_area_code=="O_24"){

            $arr_result["result"]="y";
            $arr_result["code"]="";

            if($startdaygap > 7){
                $arr_result["return"]="100";
            }else if($startdaygap <=7 && $startdaygap >3) {
                $arr_result["return"]="70";
            }else if($startdaygap <=3 && $startdaygap >1) {
                $arr_result["return"]="50";
            }else if($startdaygap <1){
                $arr_result["return"]="0";
            }

            return $arr_result;
        }


        // 2. 단기렌트인가 월렌트인가?
        // 2-1. 월렌트의 환불 구하기
        if($reserv_rent_type=="2"){
            $arr_result["result"]="y";
            $arr_result["code"]="";

            if($startdaygap > 7){
                $arr_result["return"]="100";
            }else if($startdaygap <=7 && $startdaygap >3) {
                $arr_result["return"]="95";
            }else if($startdaygap <=3  ) {
                $arr_result["return"]="90";
            }

            return $arr_result;
        }

        // 2-2. 단기렌트의 환불 구하기
        if($reserv_rent_type=="1"){

            $arr_result["result"]="y";
            $arr_result["code"]="";

            // *. 성수기인가 비성수기 인가?
            $peak_yn= $this->peroidcheck($f_rentstartdate ,$f_rentenddate);


            // 1. 성수기일떄 (시작시간기준)
            if($peak_yn=="y"){
                $arr_result["return"] = "100";
                if($starttimegap >= 48 && $starttimegap <= 72 ) {
                    $arr_result["return"] = "90";
                }else  if($starttimegap >=24 && $starttimegap <= 47 ){
                    $arr_result["return"]="80";
                }else  if( $starttimegap <= 23 ){
                    $arr_result["return"]="70";
                }
            }

            // 2. 비수기일떄 (시작시간기준)
            if($peak_yn=="n"){
                if($starttimegap >= 24   ) {
                    $arr_result["return"] = "100";
                } else  {
                    $arr_result["return"]="90";
                }
            }

            return $arr_result;
        }

    }

    // sdate = 20181212 format
    function peroidcheck($sdate,$edate){

        $sdate = date("Ymd",$sdate);
        $edate = date("Ymd",$edate);

        $CI =& get_instance();

        $sql="select count(*) as cnt from (
              select * From carmore_peakseason where '$sdate' between peakseason_startdate and peakseason_enddate
                union
                select * From carmore_peakseason where '$edate' between peakseason_startdate and peakseason_enddate
            ) a";

        $cnt = $CI->db->query($sql)->row()->cnt;
        if($cnt >0){
            $result="y";
        }else{
            $result="n";
        }

        return $result;
    }

    // 숫자 자릿수 - human readable 
    function getCdwTxt($val)
    {
        $val = intval($val);

        if ($val == 0)
            return '무한';

        if ($val >= 10000) {
            $val = $val / 10000;

            $num_str = (string)$val;
            if (strpos($num_str, '.')) {
                $split = explode(".", $num_str);
                return $split[0].'억 '.$split[1].' 천만원';
            } else
                return $num_str.'억원';
        } else
            return number_format($val)."만원";
    }

    // 이모지콘 리턴하기
    public function getIconEmoji($iconEmoji) {
        $iconEmoji = strip_tags(trim($iconEmoji));

        return $iconEmoji;
    }

    //슬랙 메세지 보내기 함수
    public function slacksend($token,$channel,$iconEmoji,$username,$message) {

        $iconEmoji = $this->getIconEmoji($iconEmoji);
        $postData = array(
            'token'    => $token,
            'channel'  => $channel,
            'icon_emoji' => $iconEmoji,
            'username' => $username,
            'text'     => $message
        );

        $ch = curl_init("https://slack.com/api/chat.postMessage");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,  'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    //세금계산서 발행함수
    public function Popbillexec() {

        $postData = array();

        $ch = curl_init("http://workspace.teamo2.kr/Popbill/RegistIssue.php");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,  'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    //날짜간 날짜 차이 가져오기
    public function get_daydiff($startdate,$endate){
        $dStart = new DateTime($startdate);
        $dEnd  = new DateTime($endate);
        $dayDiff = $dStart->diff($dEnd);

        $dategap = $dayDiff->days;
        return $dategap;

    }

    
    //기간에 따른취소 수수료율 가져오기
    public function get_canceldiscountper($startdate,$enddate,$returndate=""){
        if($returndate==""){$returndate=date("Ymd");}
        $dStart = new DateTime($startdate);
        $dEnd  = new DateTime($enddate);
        $uEnd  = new DateTime($returndate);
        $rentDiff = $dStart->diff($dEnd);
        $usediff = $dStart->diff($uEnd);

        $rentdate = $rentDiff->days;
        $usedate = $usediff->days;


        $jsonstr="[{\"d\": \"15\", \"a\": [\"35\",\"44\",\"52\",\"59\",\"66\",\"72\",\"78\",\"83\",\"87\",\"91\",\"94\",\"96\",\"98\",\"99\",\"100\"]},
{\"d\": \"16\", \"a\": [\"34\",\"42\",\"50\",\"57\",\"64\",\"70\",\"75\",\"80\",\"84\",\"88\",\"91\",\"94\",\"96\",\"98\",\"99\",\"100\"]},
{\"d\": \"17\", \"a\": [\"35\",\"43\",\"50\",\"57\",\"63\",\"69\",\"74\",\"79\",\"83\",\"87\",\"90\",\"93\",\"95\",\"97\",\"98\",\"99\",\"100\"]},
{\"d\": \"18\", \"a\": [\"34\",\"41\",\"48\",\"54\",\"60\",\"66\",\"71\",\"76\",\"80\",\"84\",\"87\",\"90\",\"93\",\"95\",\"97\",\"98\",\"99\",\"100\"]},
{\"d\": \"19\", \"a\": [\"33\",\"40\",\"47\",\"53\",\"59\",\"64\",\"69\",\"74\",\"78\",\"82\",\"85\",\"88\",\"91\",\"93\",\"95\",\"97\",\"98\",\"99\",\"100\"]},
{\"d\": \"20\", \"a\": [\"32\",\"39\",\"45\",\"51\",\"57\",\"62\",\"67\",\"72\",\"76\",\"80\",\"84\",\"87\",\"90\",\"92\",\"94\",\"96\",\"97\",\"98\",\"99\",\"100\"]},
{\"d\": \"21\", \"a\": [\"33\",\"39\",\"45\",\"51\",\"56\",\"61\",\"66\",\"70\",\"74\",\"78\",\"81\",\"84\",\"87\",\"90\",\"92\",\"94\",\"96\",\"97\",\"98\",\"99\",\"100\"]},
{\"d\": \"22\", \"a\": [\"33\",\"39\",\"45\",\"50\",\"55\",\"60\",\"65\",\"69\",\"73\",\"77\",\"80\",\"83\",\"86\",\"89\",\"91\",\"93\",\"95\",\"96\",\"97\",\"98\",\"99\",\"100\"]},
{\"d\": \"23\", \"a\": [\"32\",\"38\",\"44\",\"49\",\"54\",\"59\",\"63\",\"67\",\"71\",\"75\",\"78\",\"81\",\"84\",\"87\",\"89\",\"91\",\"93\",\"95\",\"96\",\"97\",\"98\",\"99\",\"100\"]},
{\"d\": \"24\", \"a\": [\"32\",\"38\",\"43\",\"48\",\"53\",\"58\",\"62\",\"66\",\"70\",\"74\",\"77\",\"80\",\"83\",\"86\",\"88\",\"90\",\"92\",\"94\",\"95\",\"96\",\"97\",\"98\",\"99\",\"100\"]},
{\"d\": \"25\", \"a\": [\"32\",\"37\",\"42\",\"47\",\"52\",\"56\",\"60\",\"64\",\"68\",\"72\",\"75\",\"78\",\"81\",\"84\",\"86\",\"88\",\"90\",\"92\",\"94\",\"95\",\"96\",\"97\",\"98\",\"99\",\"100\"]},
{\"d\": \"26\", \"a\": [\"31\",\"36\",\"41\",\"46\",\"51\",\"55\",\"59\",\"63\",\"67\",\"71\",\"74\",\"77\",\"80\",\"83\",\"85\",\"87\",\"89\",\"91\",\"93\",\"94\",\"95\",\"96\",\"97\",\"98\",\"99\",\"100\"]},
{\"d\": \"27\", \"a\": [\"31\",\"36\",\"41\",\"46\",\"50\",\"54\",\"58\",\"62\",\"66\",\"69\",\"72\",\"75\",\"78\",\"81\",\"83\",\"85\",\"87\",\"89\",\"91\",\"93\",\"94\",\"95\",\"96\",\"97\",\"98\",\"99\",\"100\"]},
{\"d\": \"28\", \"a\": [\"30\",\"35\",\"40\",\"44\",\"48\",\"52\",\"56\",\"60\",\"64\",\"67\",\"70\",\"73\",\"76\",\"79\",\"82\",\"84\",\"86\",\"88\",\"90\",\"92\",\"93\",\"94\",\"95\",\"96\",\"97\",\"98\",\"99\",\"100\"]},
{\"d\": \"29\", \"a\": [\"32\",\"37\",\"41\",\"45\",\"49\",\"53\",\"57\",\"61\",\"64\",\"67\",\"70\",\"73\",\"76\",\"79\",\"81\",\"83\",\"85\",\"87\",\"89\",\"91\",\"92\",\"93\",\"94\",\"95\",\"96\",\"97\",\"98\",\"99\",\"100\"]},
{\"d\": \"30\", \"a\": [\"30\",\"35\",\"39\",\"43\",\"47\",\"51\",\"55\",\"59\",\"62\",\"65\",\"68\",\"71\",\"74\",\"77\",\"79\",\"81\",\"83\",\"85\",\"87\",\"89\",\"91\",\"92\",\"93\",\"94\",\"95\",\"96\",\"97\",\"98\",\"99\",\"100\"]}]";

        $jsonarr = json_decode($jsonstr);
        $arridx = $rentdate-15;
        $arruseidx =$usedate-1;
        $tmparr = $jsonarr[$arridx];
        $adataarr = $tmparr->a;
        $endarr = $adataarr[$arruseidx];

        $returnval = 100- $endarr;

        if($rentdate <15 || $rentdate > 30 ){
            $returnval=0;
        }

        /*
        echo "<Br> startdate date:".$startdate;
        echo "<Br> enddate date:".$enddate;
        echo "<Br>origin date:".$rentDiff->days;
        echo "<Br>use date:".$usediff->days;
        echo "<Br>discount per:".$returnval;
*/

        return $returnval;
    }

    // 이메일 html 생성 함수
    public function get_emailhtml($data){


        if($data["part"]==""){
            $data["part"]="returncar";
        }

        if($data["part"]=="returncar"){

            $html="<html>                    
                    <head>
                        <title>카모아</title>
                        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
                        <!-- Meta, title, CSS, favicons, etc. -->
                        <meta charset=\"utf-8\">
                        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
                        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
                    
                        <style>
                            @media (min-width:100px) {
                                .txt-xs {
                                    font-size: 0.1em;
                                }
                                .txt-s {
                                    font-size: 0.3em;
                                }
                                .txt-m {
                                    font-size: 0.5em;
                                }
                                .txt-xxl {
                                    font-size: 1.6em;
                                }
                                .txt-bold {
                                    font-weight: 600;
                                }
                                .table-title {
                                    width: 30%;
                                }
                                .table-contents {
                                    width: 70%;
                                }
                                .contents-root {
                                    margin-left: 10px;
                                    margin-right: 10px;
                                }
                                .reservation-cancel-root {
                                    margin-top: 30px;
                                    margin-bottom: 30px;
                                }
                            }
                    
                            @media (min-width:767px) {
                                .txt-xs {
                                    font-size: 0.5em;
                                }
                                .txt-s {
                                    font-size: 0.7em;
                                }
                                .txt-m {
                                    font-size: 0.9em;
                                }
                                .txt-xxl {
                                    font-size: 2em;
                                }
                                .txt-bold {
                                    font-weight: 600;
                                }
                                .table-title {
                                    width: 25%;
                                }
                                .table-contents {
                                    width: 75%;
                                }
                                .contents-root {
                                    margin-left: 30px;
                                    margin-right: 30px;
                                }
                                .reservation-cancel-root {
                                    margin-top: 70px;
                                    margin-bottom: 30px;
                                }
                            }
                    
                            .table-title {
                                padding-right: 10px;
                                padding-left: 10px;
                                height: 30px;
                                text-align: left;
                                background-color: #f2f2f2;
                                border-bottom: 1px solid #BDBDBD;
                            }
                    
                            .table-contents {
                                padding-right: 10px;
                                padding-left: 10px;
                                height: 30px;
                                border-bottom: 1px solid #BDBDBD;
                            }
                    
                            .reservation-cancel-root {
                                background-color: #f2f2f2;
                                border: 1px solid #d9d9d9;
                                padding: 15px;
                                color: #595959;
                            }
                    
                        </style>
                    </head>
                    
                    <body>
                    <div style=\"width: 100%; max-width:780px; margin:auto;\">
                        <img style=\"width: 20%; margin:10px;\" src=\"https://carmore.kr/app_v3/php/images/emailimg/mail_logo_carmore.png?ver=180807\" />
                        <div style=\"border: 1px solid #d9d9d9;width: 100%;\">
                    
                            <div style=\"width:100%; text-align:center; border-bottom: 1px solid #d9d9d9; background-color: #f2f2f2;\">
                                <img style=\"margin-top: 15px;width: 10%;\" src=\"https://carmore.kr/app_v3/php/images/emailimg/mail_check_Red.png\" />
                                <div class=\"txt-xxl txt-bold\">조기반납 신청이 완료되었습니다.</div>
                                <div class=\"txt-m\" style=\"margin-top:10px; margin-bottom:10px;\">조기반납 신청내역을 안내해 드립니다.<br>카모아를 이용해주셔서 진심으로 감사드립니다</div>
                            </div>
                            <!--header-->
                    
                            <div class=\"contents-root\">
                                <div style=\"margin-top: 30px;font-size: 20px;\">
                                    <img src=\"https://carmore.kr/app_v3/php/images/emailimg/appointment.png\" style=\"\">
                                    <table style=\"border-spacing: 0;border-top: 3px solid black;padding: 0;margin: 0;width: 100%; text-align: right;\">
                                        <tr>
                                            <td class=\"table-title txt-m\">회사정보</td>
                                            <td class=\"table-contents txt-m\">{company}</td>
                                        </tr>
                                        <tr>
                                            <td class=\"table-title txt-m\">예약번호</td>
                                            <td class=\"table-contents txt-m\">{residx}</td>
                                        </tr>
                                        <tr>
                                            <td class=\"table-title txt-m\">예약기간</td>
                                            <td class=\"table-contents txt-m\">{startdate} ~ {enddate}</td>
                                        </tr>
                                        <tr>
                                            <td class=\"table-title txt-m\">예약차량</td>
                                            <td class=\"table-contents txt-m\">{carmodel}</td>
                                        </tr>
                                        <tr>
                                            <td class=\"table-title txt-m\">운전자</td>
                                            <td class=\"table-contents txt-m\">{driver}</td>
                                        </tr>
                                        <tr>
                                            <td class=\"table-title txt-m\">배차</td>
                                            <td class=\"table-contents txt-m\">{pickupaddr}</td>
                                        </tr>
                                        <tr>
                                            <td class=\"table-title txt-m\">회차</td>
                                            <td class=\"table-contents txt-m\">{return_addr}</td>
                                        </tr>
                                    </table>
                                </div>
                    
                                <div style=\"margin-top: 30px;  font-size: 20px;\">
                                    <img src=\"https://carmore.kr/app_v3/php/images/emailimg/payment.png\" style=\"\">
                                    <table style=\"border-spacing: 0;  border-top: 3px solid black;  padding: 0;  margin: 0; width: 100%;  text-align: right;\">
                                        <tr>
                                            <td class=\"table-title txt-m\">총 결제금액</td>
                                            <td class=\"table-contents txt-m\" style=\"padding-bottom:5px;\">{origincost} 원
                                                <br>
                                                <div class=\"txt-xs\">(카드결제 {dbpayment} 원 + 쿠폰/포인트 {discount}원)</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class=\"table-title txt-m\" style=\"color:red;\">총 취소/환불금액</td>
                                            <td class=\"table-contents txt-m\">{cancelamt} 원</td>
                                        </tr>
                                        <tr>
                                            <td class=\"table-title txt-m\">취소 수수료 차감</td>
                                            <td class=\"table-contents txt-m\" style=\"color:red;\">{emailpenalty} 원</td>
                                        </tr>
                                        <tr>
                                            <td class=\"table-title txt-m\">조기반납 신청일</td>
                                            <td class=\"table-contents txt-m\">{logdate}</td>
                                        </tr>
                                        <tr>
                                            <td class=\"table-title txt-m\">취소/환불 수단</td>
                                            <td class=\"table-contents txt-m\">신용카드</td>
                                        </tr>
                                    </table>
                                </div>
                    
                                <div class=\"reservation-cancel-root\">
                                    <div class=\"txt-m txt-bold\">취소/환불 정보</div>
                                    <div class=\"txt-s\" style=\"margin-top:10px;\">
                                        - 신용카드 : 취소완료일로부터 3~5일 영업일 이내 카드 승인 취소<br> - 체크카드 : 취소완료일로부터 카드사에 따라 3~10일 이내 카드사에서 환불<br> - 쿠폰을 사용하여 예약하였다가 취소한 경우, 쿠폰은 재발급되지 않습니다.<br> - 예약 취소 시, 사용하였던 포인트는 재적립 됩니다. (단, 취소 수수료가 발생할 경우 포인트에서 우선 차감됩니다.)<br>
                                    </div>
                                </div>
                                <div style=\"width:100%; margin-bottom:20px;\">
                                    <table style=\"width:100%;\">
                                        <tr style=\"width:100%; margin-left:30px; margin-right:30px;\">
                                            <td style=\"width:35%; text-align:center;\">
                                                <img style=\"width: 100%;\" src=\"https://carmore.kr/app_v3/php/images/emailimg/mail_logo_carmore.png?ver=180807\" />
                                            </td>
                                            <td style=\"width:65%;\">
                                                <div class=\"txt-s\">고객문의 : 070-4077-0505 (평일 10:00 ~ 19:00)</div>
                                                <div class=\"txt-s\">회사명 : (주)팀오투&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;주소 : 서울시 서초구 사평대로 353 서일빌딩 704호</div>
                                                <div class=\"txt-s\">대표이사 : 홍성주&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;사업자 등록번호 : 286-88-00238</div>
                                                <div class=\"txt-s\">개인정보 관리 책임자 : 김성준&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;통신 판매업 신고 번호 : 제 2016-서울서초-0849호</div>
                                                <div class=\"txt-s\">E-mail : feedback@carmore.kr</div>
                                                <div class=\"txt-s\">Copyright carmore. All rights reserved.</div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    </body>
                    
                    </html>";


            $html =str_replace("{company}",$data["company"],$html);
            $html =str_replace("{residx}",$data["residx"],$html);
            $html =str_replace("{startdate}",$data["startdate"],$html);
            $html =str_replace("{enddate}",$data["enddate"],$html);
            $html =str_replace("{carmodel}",$data["carmodel"],$html);
            $html =str_replace("{driver}",$data["driver"],$html);
            $html =str_replace("{pickupaddr}",$data["pickupaddr"],$html);
            $html =str_replace("{return_addr}",$data["return_addr"],$html);
            $html =str_replace("{origincost}",$data["origincost"],$html);
            $html =str_replace("{dbpayment}",$data["dbpayment"],$html);
            $html =str_replace("{discount}",$data["discount"],$html);
            $html =str_replace("{cancelamt}",$data["cancelamt"],$html);
            $html =str_replace("{emailpenalty}",$data["emailpenalty"],$html);
            $html =str_replace("{logdate}",$data["logdate"],$html);


        }

        if($data["part"]=="cancelcar"){

            $html="<html>
                <head>
                  <title>카모아</title>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
 
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
</head>

<body>
    <div style=\"width: 100%; height: 100%; background: #F0F6FD;\">
        <div style=\"width: 100%; max-width: 780px; margin: auto;\">
            <img src=\"https://s3.ap-northeast-2.amazonaws.com/carmore-common/mail/img_reservation_early_return_complete_top.png\" style=\"width: 100%;\">

            <div style=\"text-align: center; margin-top: 10px;\">
                <div style=\"font-size: 0.7em;\">예약번호</div>
                <div style=\"font-size: 1.5em; font-weight: bold;\">{residx}</div>
            </div>

            <div style=\"background: white; margin: 10px; padding: 10px;\">
                <div style=\"color: #4545D1; font-size: 1.2em;\">예약 정보</div>

                <table style=\"width: 100%; border-spacing: 0; font-size: 0.9em;\">
                    <colgroup>
                        <col style=\"width: 30%;\">
                        <col style=\"width: 70%;\">
                    </colgroup>
                    <tbody>
                        <tr>
                            <td style=\"border-bottom: 1px solid #E4E4E4; padding-top: 10px; padding-bottom: 10px;\">회사이름</td>
                            <td style=\"border-bottom: 1px solid #E4E4E4; padding-top: 10px; padding-bottom: 10px; text-align: right;\">{company}</td>
                        </tr>
                        <tr>
                            <td style=\"border-bottom: 1px solid #E4E4E4; padding-top: 10px; padding-bottom: 10px;\">예약기간</td>
                            <td style=\"border-bottom: 1px solid #E4E4E4; padding-top: 10px; padding-bottom: 10px; text-align: right;\">{startdate} ~ {enddate}</td>
                        </tr>
                        <tr>
                            <td style=\"border-bottom: 1px solid #E4E4E4; padding-top: 10px; padding-bottom: 10px;\">예약차량</td>
                            <td style=\"border-bottom: 1px solid #E4E4E4; padding-top: 10px; padding-bottom: 10px; text-align: right;\">{carmodel}</td>
                        </tr>
                        <tr>
                            <td style=\"border-bottom: 1px solid #E4E4E4; padding-top: 10px; padding-bottom: 10px;\">운전자</td>
                            <td style=\"border-bottom: 1px solid #E4E4E4; padding-top: 10px; padding-bottom: 10px; text-align: right;\">{driver}</td>
                        </tr>
                        <tr>
                            <td style=\"border-bottom: 1px solid #E4E4E4; padding-top: 10px; padding-bottom: 10px;\">배차</td>
                            <td style=\"border-bottom: 1px solid #E4E4E4; padding-top: 10px; padding-bottom: 10px; text-align: right;\">{pickupaddr}</td>
                        </tr>
                        <tr>
                            <td style=\"padding-top: 10px; padding-bottom: 10px;\">회차</td>
                            <td style=\"padding-top: 10px; padding-bottom: 10px; text-align: right;\">{return_addr}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        
            <div style=\"background: white; margin: 10px; padding: 10px;\">
                <div style=\"color: #4545D1; font-size: 1.2em;\">결제 내역</div>
                <table style=\"width: 100%; border-spacing: 0; font-size: 0.9em;\">
                    <colgroup>
                        <col style=\"width: 30%;\">
                        <col style=\"width: 70%;\">
                    </colgroup>
                    <tbody>
                        <tr>
                            <td style=\"border-bottom: 1px solid #E4E4E4; padding-top: 10px; padding-bottom: 10px;\">총 결제금액</td>
                            <td style=\"border-bottom: 1px solid #E4E4E4; padding-top: 10px; padding-bottom: 10px; text-align: right;\">{origincost}원 (카드결제 {dbpayment} 원 + 쿠폰/포인트 {discount} 원)</td>
                        </tr>
                        <tr>
                            <td style=\"border-bottom: 1px solid #E4E4E4; padding-top: 10px; padding-bottom: 10px;\">예약취소 신청일</td>
                            <td style=\"border-bottom: 1px solid #E4E4E4; padding-top: 10px; padding-bottom: 10px; text-align: right;\">{logdate}</td>
                        </tr>
                        <tr>
                            <td style=\"padding-top: 10px; padding-bottom: 10px;\">취소 수수료 차감</td>
                            <td style=\"padding-top: 10px; padding-bottom: 10px; text-align: right;\">{emailpenalty} 원</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            <div style=\"padding: 10px; display: flex; background: #4545D1; color: white; margin: -10px 10px 10px 10px;;\">
                <div style=\"text-align: left; width: 30%;\">총 취소/환불금액</div>
                <div style=\"text-align: right; width: 70%;\">
                    <div style=\"font-weight: bold;\">{cancelamt} 원</div>
                </div>
            </div>

            <div style=\"background: white; margin: 10px; padding: 10px;\">
                <div style=\"text-align: left; padding: 10px; color: #FF6363;\">
                    <div style=\"font-size: 0.9em; font-weight: bold;\">취소/환불 정보</div>
                    <div style=\"font-size: 0.7em;\">
                        - 신용카드 : 취소완료일로부터 3~5일 영업일 이내 카드 승인 취소<br> - 체크카드 : 취소완료일로부터 카드사에 따라 3~10일 이내 카드사에서 환불<br> - 쿠폰을 사용하여 예약하였다가 취소한 경우, 쿠폰은 재발급되지 않습니다.<br> - 예약 취소 시, 사용하였던 포인트는 재적립 됩니다. (단, 취소 수수료가 발생할 경우 포인트에서 우선 차감됩니다.)
                    </div>
                </div>
                <div>
                    <table style=\"width: 100%; border-spacing: 0;\">
                        <colgroup>
                            <col style=\"width: 40%;\">
                            <col style=\"width: 60%;\">
                        </colgroup>
                        <tr>
                            <td style=\"text-align: center;\">
                                <img src=\"https://s3.ap-northeast-2.amazonaws.com/carmore-common/logo/logo_carmore_with_txt.png\" style=\"width: 70%;\" />
                            </td>
                            <td style=\"font-size: 0.5em;\">
                                <div>고객문의 : 070-4077-0505 (주중,주말 09:00 ~ 20:00)</div>
                                <div>회사명 : (주)팀오투</div>
                                <div>주소 : 서울시 서초구 사평대로 353 서일빌딩 704호</div>
                                <div>대표이사 : 홍성주</div>
                                <div>사업자 등록번호 : 286-88-00238</div>
                                <div>개인정보 관리 책임자 : 김성준</div>
                                <div>통신 판매업 신고 번호 : 제 2016-서울서초-0849호</div>
                                <div>E-mail : feedback@carmore.kr</div>
                                <div>Copyright carmore. All rights reserved.</div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>

</html>";

            $html =str_replace("{company}",$data["company"],$html);
            $html =str_replace("{residx}",$data["residx"],$html);
            $html =str_replace("{startdate}",$data["startdate"],$html);
            $html =str_replace("{enddate}",$data["enddate"],$html);
            $html =str_replace("{carmodel}",$data["carmodel"],$html);
            $html =str_replace("{driver}",$data["driver"],$html);
            $html =str_replace("{pickupaddr}",$data["pickupaddr"],$html);
            $html =str_replace("{return_addr}",$data["return_addr"],$html);
            $html =str_replace("{origincost}",$data["origincost"],$html);
            $html =str_replace("{dbpayment}",$data["dbpayment"],$html);
            $html =str_replace("{discount}",$data["discount"],$html);
            $html =str_replace("{cancelamt}",$data["cancelamt"],$html);
            $html =str_replace("{emailpenalty}",$data["emailpenalty"],$html);
            $html =str_replace("{logdate}",$data["logdate"],$html);


        }

        return $html;
    }

    // 광역 지역정보 option 값 가져오기
    public function select_city($tmp_city)
    {
        $CI =& get_instance();
        $sql=" select distinct(city) from new_area";
        $applistrs = $CI->db->query($sql);

        $return_str="";
        foreach($applistrs->result_array() AS $row)
        {
            $city	=$row['city'];
            $return_str .= "<option value='".$city."'";
            if($tmp_city ==$city)
            {
                $return_str.= " selected";
            }
            $return_str.= " >".$city."</option>";
        }
        return $return_str;
    }


    // 광역 지역정보 option 값 가져오기
    public function select_newcity($tmp_city)
    {
        $CI =& get_instance();
        $sql=" select city, city_value from new_area group by city_value";
        $applistrs = $CI->db->query($sql);

        $return_str="";
        foreach($applistrs->result_array() AS $row)
        {
            $city	=$row['city'];
            $ct_value	=$row['city_value'];
            $return_str .= "<option value='".$ct_value."'";
            if($tmp_city ==$ct_value)
            {
                $return_str.= " selected";
            }
            $return_str.= " >".$city."</option>";
        }
        return $return_str;
    }

    // 회사 select option 정보 가져오기
    public function select_company($tmp_code)
    {
        $CI =& get_instance();
        $sql="select * from new_rentCompany where test_check='0'
               and state='1' 
                 AND SERIAL IN (SELECT DISTINCT(rentcompany_serial) FROM new_rentCompany_branch
                   WHERE carmore_normal_available  =1 OR carmore_month_available  =1)
             order by name asc";
        $applistrs = $CI->db->query($sql);

        $return_str="";
        foreach($applistrs->result_array() AS $row)
        {
            $serial	=$row['serial'];
            $name	=$row['name'];
            $return_str .= "<option value='".$serial."'";
            if($tmp_code ==$serial)
            {
                $return_str.= " selected";
            }
            $return_str.= " >".$name."</option>";
        }
        return $return_str;
    }


    function get_pageauth($part){
        $loginmaster_yn = $_SESSION["loginmaster_yn"];
        $loginpointauth_yn = $_SESSION["loginpointauth_yn"];
        $logincalc_yn = $_SESSION["logincalc_yn"];
        $loginordermng_yn = $_SESSION["loginordermng_yn"];

        $pageauth ="n";

        if($part=="point"){
            if($loginpointauth_yn =="y"){$pageauth ="y";}
        }
        if($part=="calc"){
            if($logincalc_yn =="y"){$pageauth ="y";}
        }
        if($part=="order"){
            if($loginordermng_yn =="y"){$pageauth ="y";}
        }
        if($part=="master"){
            if($loginmaster_yn =="y"){$pageauth ="y";}
        }

        if($loginmaster_yn =="y"){$pageauth ="y";}

        return $pageauth;
    }

    // 차종코드 가져오기
    function get_cartypestr($codevalue)
    {
        switch ($codevalue) {
            case "0"  :
                $returnstr = "경형";
                break;
            case "1"  :
                $returnstr = "소형";
                break;
            case "2"  :
                $returnstr = "준중형";
                break;
            case "3"  :
                $returnstr = "중형";
                break;
            case "4"  :
                $returnstr = "대형";
                break;
            case "5"  :
                $returnstr = "수입";
                break;
            case "6"  :
                $returnstr = "RV";
                break;
            case "7"  :
                $returnstr = "SUV";
                break;
        }

        return $returnstr;
    }

    // 요일 정보 가져오기
    function getyoilstr($yoilval){
        $yoilval=$yoilval-1;
        $yoil = array("일","월","화","수","목","금","토");
        $yoilstr =$yoil[$yoilval];
        $yoilstr=$yoilstr."요일";
        return $yoilstr;
    }


    // 고객상담 코드 
    function get_complaincodestr($codevalue)
    {
        switch ($codevalue) {
            case "point"  :
                $returnstr = "포인트";
                break;
            case "coupon"  :
                $returnstr = "쿠폰";
                break;
            case "pay"  :
                $returnstr = "결제";
                break;
            case "member"  :
                $returnstr = "회원관련";
                break;
            case "etc"  :
                $returnstr = "기타사항";
                break;
        }

        return $returnstr;
    }


    // 간단한 인젝션
    function SQL_Injection ( $get_String ) {

        $get_String = str_replace( "'", "", $get_String );
        $get_String = str_replace( ";", "", $get_String );
        $get_String = str_replace( "--", "", $get_String );
        $get_String = str_replace( "1=1", "", $get_String );
        $get_String = str_replace( "or 1=1", "", $get_String );
        $get_String = str_replace( "sp_", "", $get_String );
        $get_String = str_replace( "xp_", "", $get_String );
        $get_String = str_replace( "@variable", "", $get_String );
        $get_String = str_replace( "@@variable", "", $get_String );
        $get_String = str_replace( "exec", "", $get_String );
        $get_String = str_replace( "sysobject", "", $get_String );

        return $get_String;
    }


    // 주문정보의 데이터 암호화
    function encrypt($string) {
        $randTxt = $this->generateRandomTxt(30);

        $encryptKey = "";
        for($i = 0; $i < strlen($randTxt); $i++) {
            if($i % 3 == 0) {
                $encryptKey .= $randTxt[$i];
            }
        }

        $result = '';
        for($i=0; $i<strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($encryptKey, ($i % strlen($encryptKey))-1, 1);
            $char = chr(ord($char)+ord($keychar));
            $result .= $char;
        }

        $encrypted = base64_encode($result);

        $encrypted .= $randTxt;

        return $encrypted;
    }

    // 랜덤 텍스트 생성하기
    function generateRandomTxt($length=12) {
        $consonants1 = 'aeuybdghjmnpqrstvz';
        $consonants2 = 'AEUYBDGHJLMNPQRSTVWXZ';
        $consonants3 = '0123456789';
        $consonants4 = '!@#$%^&*';

        $idx = 0;
        $randNum = 0;
        $password='';

        srand( $this->make_seed());

        while(strlen($password) < $length) {
            // 한번씩 골고루 섞여야하므로 하나씩 문자 붙힌 후에 랜덤으로 한다.
            if ($idx < 4) {
                $randNum = $idx;
            } else {
                $randNum = mt_rand(0,3);
            }

            $selectStr = "";

            if ($randNum == 0) {
                $selectStr = $consonants1[(mt_rand() % strlen($consonants1))];
            } else if ($randNum == 1) {
                $selectStr = $consonants2[(mt_rand() % strlen($consonants2))];
            } else if ($randNum == 2) {
                $selectStr = $consonants3[(mt_rand() % strlen($consonants3))];
            } else if ($randNum == 3) {
                $selectStr = $consonants4[(mt_rand() % strlen($consonants4))];
            }

            if (strpos($password, $selectStr) !== false) {
                // 이미 포함된 문자라서 패스시킴
                continue;
            } else {
                $password .= $selectStr;
            }

            $idx++;
        }

        return $password;
    }

    // seed with microseconds
    function make_seed()
    {
        list($usec, $sec) = explode(' ', microtime());
        return $sec + $usec * 1000000;
    }


    function add_phonehyphen($tel)
    {
        $tel = preg_replace("/[^0-9]/", "", $tel);    // 숫자 이외 제거
        if (substr($tel,0,2)=='02')
            return preg_replace("/([0-9]{2})([0-9]{3,4})([0-9]{4})$/", "\\1-\\2-\\3", $tel);
        else if (strlen($tel)=='8' && (substr($tel,0,2)=='15' || substr($tel,0,2)=='16' || substr($tel,0,2)=='18'))
            // 지능망 번호이면
            return preg_replace("/([0-9]{4})([0-9]{4})$/", "\\1-\\2", $tel);
        else
            return preg_replace("/([0-9]{3})([0-9]{3,4})([0-9]{4})$/", "\\1-\\2-\\3", $tel);
    }

    // 주문정보의 데이터 복호화
    function decrypt($string) {
        $decryptKeyFull = substr($string, strlen($string) - 30, 30);
        $decryptTxt = substr($string, 0, strlen($string) - 30);

        $decryptKey = "";
        for($i = 0; $i < strlen($decryptKeyFull); $i++) {
            if($i % 3 == 0) {
                $decryptKey .= $decryptKeyFull[$i];
            }
        }

        $decrypted = '';
        $decryptTxt = base64_decode($decryptTxt);
        for($i=0; $i<strlen($decryptTxt); $i++) {
            $char = substr($decryptTxt, $i, 1);
            $keychar = substr($decryptKey, ($i % strlen($decryptKey))-1, 1);
            $char = chr(ord($char)-ord($keychar));
            $decrypted .= $char;
        }

        return $decrypted;
    }


    // 팀정보 select option
    public function select_team($tmpteam_code)
    {
        $CI =& get_instance();
        $sql="select * from admin_team order by  team_name desc";
        $applistrs = $CI->db->query($sql);

        $return_str="";
        foreach($applistrs->result_array() AS $row)
        {
            $team_code	=$row['team_code'];
            $team_name	=$row['team_name'];
            $return_str .= "<option value='".$team_code."'";
            if($tmpteam_code ==$team_code)
            {
                $return_str.= " selected";
            }
            $return_str.= " >".$team_name."</option>";
        }
        return $return_str;
    }


    // 각 조건에 맞는 관리자 정보 select option
    public function select_member($tmpmem_id,$soption,$grade="")
    {
        $CI =& get_instance();
        $wsql="";
        if($grade !="") $wsql =" and member_grade >'$grade'";
        $sql="select * from admin_member where admin_stats='y'  $wsql order by  admin_name asc";
        $applistrs = $CI->db->query($sql);

        $return_str="";
        foreach($applistrs->result_array() AS $row)
        {
            $admin_name	=$row['admin_name'];
            $admin_email	=$row['admin_email'];
            $strmember_grade	= $this->get_memgrade($row['member_grade']);

            if($soption=="option")
            {
                $return_str .= "<option value='".$admin_email."'";
                if($tmpmem_id ==$admin_email)
                {
                    $return_str .= " selected";
                }
                $return_str .= " >".$admin_name."(".$strmember_grade.")</option>";
            }
            else{
                $return_str.= "<label class='checkbox-inline'><input type='checkbox' name='submem[]' value='$admin_email'";

                if(strpos($tmpmem_id, $admin_email) !== false) {
                    $return_str.= " checked";
                }
                $return_str.= ">$admin_name ($strmember_grade)</label>";

            }
        }
        return $return_str;
    }


    // 관리자 직급 가져오기
    function get_memgrade($member_grade)
    {
        switch ($member_grade) {
            case "0"  :
                $returnstr = "아르바이트";
                break;
            case "1"  :
                $returnstr = "사원";
                break;
            case "2"  :
                $returnstr = "대리";
                break;
            case "3"  :
                $returnstr = "과장";
                break;
            case "4"  :
                $returnstr = "차장";
                break;
            case "5"  :
                $returnstr = "임원";
                break;
        }

        return $returnstr;
    }

    // 관리자 아이디로 관리자 이름 가져오기
    public function get_memname($mem_id)
    {
        $CI =& get_instance();

        $memcnt = substr_count($mem_id, ",");
        $admin_name="";

        if($memcnt < 1) {
            if($mem_id !="") {
                $sql = "select admin_name from admin_member where admin_email='$mem_id'";
                $tmprow = $CI->db->query($sql)->row();
                $admin_name = $tmprow->admin_name;
            }
        }
        else{
            $tmparr = explode(",",$mem_id);

            for($i=0;$i < sizeof($tmparr); $i++) {
                $tmem_id = $tmparr[$i];
                $tmpadmin_name="";
                if($tmem_id !="") {
                    $sql = "select admin_name from admin_member where admin_email='$tmem_id'";
                    $tmprow = $CI->db->query($sql)->row();
                    $tmpadmin_name = $tmprow->admin_name;
                }
                $arradmin_name[] = $tmpadmin_name;
            }
            $admin_name = implode(",",$arradmin_name);
        }

        return $admin_name;
    }

 
    // 컨텐트 코드 생성
    public function get_contentcode($board_code)
    {
        $randinfo =rand(100, 900);
        $return_v = date("YmdHis")."_".$randinfo."_".$board_code;
        return $return_v;
    }
    
    // 날짜별 포맷 쉽게 가져오기
    function get_dateformat($datestr,$short_yn="")
    {
        //  20151208164623
        if($datestr !=""){
            if($short_yn==""){
                $datestr = date("Y-m-d H:i:s", strtotime($datestr));
            }else if($short_yn=="hyphen"){
                $datestr = date("Y-m-d", strtotime($datestr));
            }else if($short_yn=="days"){
                $datestr = date("Y/m/d", strtotime($datestr));
            }else if($short_yn=="ns"){
                $datestr = date("m월 d일 H:i", strtotime($datestr));
            }else if($short_yn=="date"){
                $datestr = date("Y.m.d", strtotime($datestr));
            }else if($short_yn=="datestr"){
                $datestr = date("Y년 m월 d일", strtotime($datestr));
            }else if($short_yn=="month"){
                $datestr = date("Y년 m월", strtotime($datestr));
            }else if($short_yn=="months"){
                $datestr = date("Y.m", strtotime($datestr));
            }else if($short_yn=="regs"){
                $datestr = date("m/d H:i", strtotime($datestr));
            }else if($short_yn=="st"){
                $datestr = date("m/d", strtotime($datestr));
            }else if($short_yn=="Ymd"){
                $datestr = date("Ymd", strtotime($datestr));
            }
        }else{
            $datestr="";
        }
        return $datestr;
    }

    // 팀 이름 가져오기
    function get_team_name($infotype,$mem_id,$team_code)
    {
        $CI =& get_instance();
        if($infotype=="team_code"){
            $sql="select team_name From admin_team where team_code='$team_code'";
        }else{
            $sql="select team_name From admin_team
					where team_code in (select team_code from admin_member  where admin_email='$mem_id')";
        }
        $team_name=$CI->db->query($sql)->row()->team_name;

        return $team_name;
    }


    // 관리자 아이디로 팀 코드 가져오기
    function get_memberteamcode($mem_id)
    {
        $CI =& get_instance();
        $sql= "select team_code from admin_member where admin_email ='$mem_id'";
        $team_code=$CI->db->query($sql)->row()->team_code;
        return $team_code;
    }

    //관리자의 권한 정보가져오기
    function get_permission($part,$mem_id)
    {
        $CI =& get_instance();
        if($mem_id !="") {
            $sql = "select $part as a,master_yn From admin_member where admin_email='$mem_id'";
            $trow = $CI->db->query($sql)->row();

            $master_yn = $trow->master_yn;
            $returnstr = $trow->a;

            if ($master_yn == "y") $returnstr = "y";
        }else{
            $master_yn = "n";
            $returnstr = "n";
        }

        return $returnstr;
    }

    //페이지별권한 리턴 
    function get_permissionArray($mem_id)
    {
        /*
        $data["config_yn"]=$this->get_permission("config_yn",$mem_id);
        $data["buyoffer_yn"]=$this->get_permission("buyoffer_yn",$mem_id);
        $data["offwork_yn"]=$this->get_permission("offwork_yn",$mem_id);
*/
        $data["temp"]="";

        return $data;
    }

    // 쓰기권한 체크함수
    function get_writeauth($login_id,$write_id,$master_yn)
    {
        if($login_id !="" ) {
            $memcnt = substr_count($write_id, $login_id);

            if ($memcnt > 0 || $master_yn == "y") {
                $return_v = "y";
            } else {
                $return_v = "n";
            }
        }else{
            $return_v = "n";
        }

        return $return_v;
    }

    // 라이트 함수 
    function right($value, $count){
        $value = substr($value, (strlen($value) - $count), strlen($value));
        return $value;
    }

    // 레프트함수
    function left($string, $count){
        return substr($string, 0, $count);
    }


    //게시물에 맞는 파일 리스트 가져오기
    public function get_ContentFileList($content_code)
    {
        $CI =& get_instance();
        $sql="SELECT content_code,  fileOrgName,  fileSaveName,  fileType
                              FROM admin_uploadfile where content_code='$content_code'";

        return $CI->db->query($sql)->result_array();
    }


}