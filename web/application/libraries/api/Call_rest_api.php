<?php
/**
 * API 베이직
 *
 * @author DEN
 */

class call_rest_api {

    /**
     * GET curl 호출
     *
     * @author DEN
     *
     * @param  $url        string
     *
     * @return bool|string
     */
    protected function call_get($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($ch);

        curl_close($ch);

        return $output;

    }

    /**
     * POST curl 호출
     *
     * @author DEN
     *
     * @param  $url        string
     * @param  $post_filed string
     *
     * @return bool|string
     */
    protected function call_post($url, $post_filed)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_filed);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($ch);

        curl_close($ch);

        return $output;

    }
}