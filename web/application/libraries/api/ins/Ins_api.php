<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once $_SERVER['DOCUMENT_ROOT'].'/application/libraries/api/Call_rest_api.php';

/**
 * 인스 API 함수 정의
 *
 * @author DEN
 */

class Ins_api extends Call_rest_api {
    const BASIC_URL       = "https://rt.wrent.seejeju.com/rtapi/ws.aspx";
    const BASIC_URL_SEC   = "https://rt1.wrent.seejeju.com/rtapi/ws.aspx";
    const INS_UID         = "teamo2";
    const INS_PASSCODE    = "J2BDIW61U5K5KESRWFZM";
    const CAR_LIST_CMD    = "carlist_r";
    const CDW_LIST_CMD    = "inslist_r";
    const RESERV_INFO_CMD = "revdetail";
    const CANCEL_PROC_CMD = "revcancel";

    /**
     * API 받은 R예약상태 의 상태를 flag 로
     *
     * @param $state string
     *
     * @return int
     */
    public function get_state_flag($state)
    {
        switch ($state)
        {
            case "예약완료":
                return 0;
            case "예약취소":
                return 11;
            default:
                return -1;
        }
    }

    /**
     * @param $rc_name  string
     *
     * @return array
     *
     * @internal 하위 부분 API 정보
     * @internal L렌트회사코드  여행사코드
     * @internal R렌트회사     렌트카회사명
     * @internal R모델번호     차종코드
     * @internal R모델명      차종명
     * @internal R승차인원     차종분류코드
     * @internal R연료        차종분류명
     * @internal R모델옵션     구분코드
     * @internal R구동방식     승용승합 구분명
     * @internal R가능연령     제조사 분류코드
     * @internal R배기량      제조사 분류명
     * @internal R변속기      연료타입코드
     * @internal R연식        연료명
     * @internal R보유수량     정원
     * @internal R타이틀      타이틀
     */
    public function get_affiliate_branch_car_inventory($rc_name)
    {
        $post_string = http_build_query([
            'cmd'      => self::CAR_LIST_CMD,
            'uid'      => self::INS_UID,
            'passcode' => self::INS_PASSCODE,
            'rcname'   => $rc_name
        ]);

        $ret_xml_data = json_decode($this->call_post(self::BASIC_URL_SEC, $post_string));

        if ($ret_xml_data->resCode === "0000")
            return $ret_xml_data->모델목록;
        else
            return [];
    }

    public function get_affiliate_branch_cdw_inventory($rc_name)
    {
        $post_string = http_build_query([
            'cmd'        => self::CDW_LIST_CMD,
            'uid'        => self::INS_UID,
            'passcode'   => self::INS_PASSCODE,
            'rcname'     => $rc_name,
            'option_ins' => 1
        ]);

        $ret_xml_data = json_decode($this->call_post(self::BASIC_URL_SEC, $post_string));

        if ($ret_xml_data->resCode === "0000")
            return $ret_xml_data->보험목록;
        else
            return [];
    }

    /**
     * @param $rc_name    string
     * @param $reserv_num string
     *
     * @return stdClass
     *
     * @internal 하위 부분      API 정보
     * @internal R예약번호      API예약번호
     * @internal R차량료
     * @internal R자차료
     * @internal R요금합계
     * @internal R직불예정금액
     * @internal R모델명
     * @internal R자차보험
     * @internal R자차옵션
     * @internal R예약변경가능시간
     * @internal L렌트회사코드
     * @internal R렌트회사
     * @internal R모델번호
     * @internal R출발일시
     * @internal R반납일시
     * @internal R출발장소
     * @internal R반납장소
     * @internal R이름
     * @internal R전화번호
     * @internal R예약일시
     * @internal R예약상태
     * @internal R후불금액
     * @internal R거래처납입
     * @internal R거래처미납
     * @internal R전달사항
     */
    public function get_reserv_information($rc_name, $reserv_num)
    {
        $post_string = http_build_query([
            'cmd'      => self::RESERV_INFO_CMD,
            'uid'      => self::INS_UID,
            'passcode' => self::INS_PASSCODE,
            'rcname'   => $rc_name,
            'revnum'   => $reserv_num
        ]);

        $ret_xml_data = json_decode($this->call_post(self::BASIC_URL_SEC, $post_string));

        if ($ret_xml_data->resCode === "0000")
            return $ret_xml_data;
        else
        {
            $std_class = new stdClass();

            return $std_class;
        }

    }

    public function proc_cancel($rc_name, $reserv_num)
    {
        $post_string = http_build_query([
            'cmd'      => self::CANCEL_PROC_CMD,
            'uid'      => self::INS_UID,
            'passcode' => self::INS_PASSCODE,
            'rcname'   => $rc_name,
            'revnum'   => $reserv_num
        ]);

        $ret_xml_data = json_decode($this->call_post(self::BASIC_URL_SEC, $post_string));

        return $ret_xml_data;
    }
}
