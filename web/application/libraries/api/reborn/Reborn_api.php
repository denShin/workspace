<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once $_SERVER['DOCUMENT_ROOT'].'/application/libraries/api/Call_rest_api.php';

/**
 * 리본 API 함수 정의
 *
 * @author DEN
 */

class Reborn_api extends Call_rest_api {
    const CAR_INVENTORY_URL = "https://api.roof926.com/api/v1/homepage/carInfo.xml";
    const RESERV_INFO_URL   = "https://api.roof926.com/api/v1/homepage/resveInfo.xml";
    const CANCEL_PROC_URL   = "https://api.roof926.com/api/v1/homepage/carCancel.xml";

    /**
     * API 받은 reservCanclYn 의 상태를 flag 로
     *
     * @param $reserv_cancel_yn string
     *
     * @return int
     */
    public function get_state_flag($reserv_cancel_yn)
    {
        switch ($reserv_cancel_yn)
        {
            case "N":
                return 0;
            case "Y":
                return 11;
            default:
                return -1;
        }
    }

    /**
     * @param $api_key   string API
     * @param $certi_key string 인증키
     *
     * @return array
     *
     * @internal 하위 부분 API 정보
     * @internal clientCode       업체코드
     * @internal clientNm         업체
     * @internal vhctyCode        차종코드
     * @internal vhctyNm          차종명
     * @internal vhctyClCode      차종분류코드
     * @internal vhctyClNm        차종분류명
     * @internal corpPeplcarrCode 승용승합 구분코드
     * @internal corpPeplcarrNm   승용승합 구분명
     * @internal makrClCode       제조사 분류코드
     * @internal makrClNm         제조사 분류명
     * @internal fuelCode         연료타입코드
     * @internal fuelNm           연료명
     * @internal passengers       정원
     * @internal optionSafeCode   차량옵션 안전코드
     * @internal optionSafeNm     차량옵션 안전명
     * @internal optionCnvncCode  차량옵션 편의코드
     * @internal optionCnvncNm    차량옵션 편의명
     * @internal optionSondCode   차량옵션 음향코드
     * @internal optionSondNm     차량옵션 음향명
     * @internal yemodelMin       차량 최소연식
     * @internal yemodelMax       차량 최대연식
     * @internal yemodel          연식화면 표시 ex) 2017~2018
     * @internal license          가능면허 (1:1종, 2:2종)
     */
    public function get_affiliate_branch_car_inventory($api_key, $certi_key)
    {
        $ret_xml_data = json_decode(json_encode(simplexml_load_string($this->call_get(self::CAR_INVENTORY_URL."?crtfcKey=".$api_key."&crtfcAuthorKey=".$certi_key))), TRUE);

        return $ret_xml_data['body']['item'];
    }

    /**
     * 예약정보 조회
     *
     * @param $api_key   string API
     * @param $certi_key string 인증키
     * @param $reserv_no int    예약번호
     *
     * @return array
     *
     * @internal 하위 부분 API 정보
     * @internal resveNo            예약번호
     * @internal rsvctmNm           예약고객명
     * @internal drverNm            운전자명
     * @internal rsvctmCttpc1Encpt  예약자 번화번호 1
     * @internal vhctyCode          차종코드
     * @internal vhctyNm            차종명
     * @internal resveBeginDhm      대여시작일 (YYYYmmddHHii) ex) 201806061130
     * @internal resveEndDhm        대여종료일 (YYYYmmddHHii)
     * @internal vhcleChrgeDscntRt  차량 할인율
     * @internal vhcleChrge         차량 정요금
     * @internal vhcleApplcChrge    대여료
     * @internal insrncApplcChrge   자차료
     * @internal insrncYn           자차가입여부 (N:미가입, Y:가입)
     * @internal insrncKndNm        자차명
     * @internal alntPlcCode        배차장소코드
     * @internal alntPlcEtcContents 배차장소 기타 내용
     * @internal cmmnEtcContents    비고
     * @internal resveCanclYn       예약취소여부 (Y/N)
     */
    public function get_reserv_information($api_key, $certi_key, $reserv_no)
    {
        $post_field = http_build_query([
            'crtfcKey'       => $api_key,
            'crtfcAuthorKey' => $certi_key,
            'resveNo'        => $reserv_no,
        ]);

        $ret_xml_data = json_decode(json_encode(simplexml_load_string($this->call_post(self::RESERV_INFO_URL, $post_field))), TRUE);

        return $ret_xml_data['body']['item'];

    }

    public function proc_cancel($api_key, $certi_key, $reserv_no)
    {
        $post_field = http_build_query([
            'crtfcKey'       => $api_key,
            'crtfcAuthorKey' => $certi_key,
            'resveNo'        => $reserv_no,
        ]);

        $ret_xml_data = json_decode(json_encode(simplexml_load_string($this->call_post(self::CANCEL_PROC_URL, $post_field))), TRUE);

        return $ret_xml_data['head'];
    }
}