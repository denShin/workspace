<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once $_SERVER['DOCUMENT_ROOT'].'/application/libraries/api/Call_rest_api.php';

/**
 * 리본 API 함수 정의
 *
 * @author DEN
 */

class Grim_api extends Call_rest_api {
    const BASIC_URL = "http://data.";
    const CAR_INVENTORY_URL    = "/basic/carinfo.php";
    const RESERV_INVENTORY_RUL = "/basic/carlist.php";
    const RESERV_INFO_URL      = "/basic/yeyakDetail.php";
    const CANCEL_PROC_URL      = "/basic/yeyakCancel.php";

    /**
     * API 받은 condition 의 상태를 flag 로
     *
     * @param $state string
     *
     * @return int
     */
    public function get_state_flag($state)
    {
        switch ($state)
        {
            case "예약대기":
                return 0;
            case "예약취소":
                return 11;
            default:
                return -1;
        }
    }

    /**
     * @param $domain string 업체도메인
     *
     * @return array
     *
     * @internal 하위 부분 API 정보
     * @internal code     차종코드
     * @internal name     차종명
     * @internal gubun    차량분류 (경차|소형|준중형|중형|고급|SUV/RV|승합)
     * @internal gear     미션 (자동|수동)
     * @internal color    차량색상
     * @internal fuel     연료 (휘발유|LPG|경유)
     * @internal jeongwon 탑승인원(숫자)
     * @internal baegi    배기량
     */
    public function get_affiliate_branch_car_inventory($domain)
    {
        $ret_xml_data = json_decode(json_encode(simplexml_load_string($this->call_get(self::BASIC_URL.$domain.self::CAR_INVENTORY_URL))), TRUE);

        return $ret_xml_data['Item'];
    }

    public function get_car_list_inventory($domain, $partner, $partner_id, $partner_pw, $start, $end)
    {

        $url = self::BASIC_URL.$domain.self::RESERV_INVENTORY_RUL."?partner=".$partner."&pmid=".$partner_id."&pmpw=".$partner_pw."&sdate=".$start."&shour=14&smin=10&edate=".$end."&ehour=14&emin=10";

        return json_decode(json_encode(simplexml_load_string($this->call_get($url))), TRUE);

    }

    /**
     * @param $domain      string 업체도메인
     * @param $partner_id  string 아이디
     * @param $partner_pw  string 비밀번호
     * @param $reserv_code string 그림예약번호
     *
     * @return array
     *
     * @internal 하위 부분 API 정보
     * @internal yeyak_no      예약번호
     * @internal yeyak_name    예약자명
     * @internal user_name     ????? 공백 설명없음
     * @internal phone1        ????? 공백 설명없음
     * @internal license_no    ?????
     * @internal car_code      렌트카측 차종 코드
     * @internal car_name      렌트카측 차종명
     * @internal gubun_name    차종구분
     * @internal mission       기어
     * @internal bohum_gaip    가입한보험
     * @internal sYear         대여 연
     * @internal sMonth        대여 월
     * @internal sDay          대여 일
     * @internal sTime         대여 시분
     * @internal eYear         반납 연
     * @internal eMonth        반납 월
     * @internal eDay          반납 일
     * @internal eTime         반납 시분
     * @internal baecha_jangso 배차장소
     * @internal condition     예약상태 ( 예약대기 | 예약취소 ... )
     * @internal salefee       대여료 (현직불??)
     */
    public function get_reserv_information($domain, $partner_id, $partner_pw, $reserv_code)
    {
        $post_field = http_build_query([
            'partnerid' => $partner_id,
            'partnerpw' => $partner_pw,
            'resnum'    => $reserv_code,
        ]);

        $ret_xml_data = json_decode(json_encode(simplexml_load_string($this->call_post(self::BASIC_URL.$domain.self::RESERV_INFO_URL, $post_field))), TRUE);

        return $ret_xml_data;

    }

    /**
     * 예약취소 API
     *
     * @param $domain
     * @param $partner_id
     * @param $partner_pw
     * @param $reserv_code
     *
     * @return array [Ok | Fail]
     */
    public function proc_cancel($domain, $partner_id, $partner_pw, $reserv_code)
    {
        $post_field = http_build_query([
            'partnerid' => $partner_id,
            'partnerpw' => $partner_pw,
            'resnum'    => $reserv_code,
        ]);

        $ret_xml_data = json_decode(json_encode(simplexml_load_string($this->call_post(self::BASIC_URL.$domain.self::CANCEL_PROC_URL, $post_field))), TRUE);

        return $ret_xml_data;
    }

}