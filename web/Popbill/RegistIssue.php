<?php
    //http://workspace.teamo2.kr/Popbill/RegistIssue.php

    include ($_SERVER["DOCUMENT_ROOT"]."/application/config/aws_dbcon.php");

    define("ID",$aws_db["username"],true);
    define("PW",$aws_db["password"],true);
    define("NAME",$aws_db["dbname"],true);
    define("HOST",$aws_db["host"],true);

    $conn = new PDO($aws_db["dns"], $aws_db["username"], $aws_db["password"]);
    $conn->exec("set names utf8");

    $sql="select   calbranch_idx,  calmainmst_code,  calmaindtl_branch,  sumtotalprice,  sumcalamount,
        sumrentfee,  sumdiscount,    registration_number, company_name,  ceo_name,  addr,  bizpart,  tax_email,
        bankinfo,  alias,  bankaccount,  cal_email,  contactname,  biztype,  bizclass,  tax_email2,  cal_email2
        from caculate_calbranch a INNER JOIN calculate_taxinfo b ON a.calmaindtl_branch =b.branch_serial where taxmail_yn='w'  ";

    $taxquery = $conn -> prepare($sql);
    $taxquery -> execute();


?>
<html xmlns="http://www.w3.org/1999/xhtml">


<head>
    <title>전자세금계산서 API 연동 가이드</title>
</head>
<?php

include 'common.php';

while( $item=$taxquery -> fetch() ) {

        $Taxinvoice = new Taxinvoice();// 세금계산서 객체 생성
        $mycorpnum = '2868800238';// 팝빌 연동회원 사업자번호
        $UserID = 'teamo2';// 팝빌 연동회원 아이디

        /************************************************************
         *                        세금계산서 정보
         ************************************************************/
        $nowdate = date("Ymd");// 작성일자
        $nowtime = date("YmdHis");
        $sumrentfee= $item["sumrentfee"];
        $calbranch_idx =$item["calbranch_idx"];
        $origin =  floor($sumrentfee*10/11);
        $vat = $sumrentfee-$origin;

        $calmainmst_code =$item["calmainmst_code"];
        $calmaindtl_branch= $item["calmaindtl_branch"];
        $invoicerMgtKey = $calmainmst_code."-".$calmaindtl_branch."1111"; //111 은 임의로 붙인숫자
        $Taxinvoice->writeDate = $nowdate;
        $Taxinvoice->issueType = '정발행';// 발행형태
        $Taxinvoice->chargeDirection = '정과금';// 과금방향
        $Taxinvoice->purposeType = '영수';// 영수 or 청구
        $Taxinvoice->issueTiming = '직접발행'; // 발행시점
        $Taxinvoice->taxType = '과세';// 과세형태
        $Taxinvoice->supplyCostTotal = $origin;// 공급가액 합계
        $Taxinvoice->taxTotal = $vat;// 세액 합계
        $Taxinvoice->totalAmount = $sumrentfee; // 합계금액
        $Taxinvoice->remark1 = '';// 기재상 '외상'항목

        /************************************************************
         *                         공급자 정보
         ************************************************************/
        $mycorpname="(주)팀오투";
        $myceoname="홍성주";
        $myaddr="서울특별시 서초구 사평대로 353, 704호(반포동, 서일빌딩)";
        $mybiztype="서비스";
        $mybizclass="소프트웨어개발";
        $myemail ="tax@teamo2.kr";
        $Taxinvoice->invoicerCorpNum = $mycorpnum;// 공급자 사업자번호
        $Taxinvoice->invoicerCorpName = $mycorpname;// 공급자 상호
        $Taxinvoice->invoicerMgtKey = $invoicerMgtKey; // 공급자 문서관리번호 - 마스터코드+branchcode
        $Taxinvoice->invoicerCEOName = '홍성주';// 공급자 대표자성명
        $Taxinvoice->invoicerAddr = $myaddr;// 공급자 주소
        $Taxinvoice->invoicerBizClass = $mybizclass;// 공급자 종목
        $Taxinvoice->invoicerBizType = $mybiztype;// 공급자 업태
        $Taxinvoice->invoicerContactName = '임수연';// 공급자 담당자 성명
        $Taxinvoice->invoicerEmail = $myemail;// 공급자 담당자 메일주소
        $Taxinvoice->invoicerSMSSendYN = false;// 공급받는자 담당자에게 알림문자 전송여부

        /************************************************************
         *                      공급받는자 정보
         ************************************************************/
        $clientceo_name = $item["ceo_name"];
        $clientcorpname= $item["company_name"];
        $registration_number= $item["registration_number"];
        $registration_number = str_replace("-","",$registration_number);

        $clientaddr = $item["addr"];
        $clientbiztype = $item["biztype"];
        $clientbizclass = $item["bizclass"];
        $tax_email = $item["tax_email"];
        $tax_email2 = $item["tax_email2"];


        $Taxinvoice->invoiceeType = '사업자';// 공급받는자 구분
        $Taxinvoice->invoiceeCorpNum = $registration_number; // 공급받는자 사업자번호
        $Taxinvoice->invoiceeCorpName = $clientcorpname;// 공급자 상호
        $Taxinvoice->invoiceeCEOName = $clientceo_name; // 공급받는자 대표자성명
        $Taxinvoice->invoiceeAddr = $clientaddr;// 공급받는자 주소
        $Taxinvoice->invoiceeBizType = $clientbiztype;// 공급받는자 업태
        $Taxinvoice->invoiceeBizClass = $clientbizclass; // 공급받는자 종목
        $Taxinvoice->invoiceeContactName1 = $clientceo_name;// 공급받는자 담당자 성명
        $Taxinvoice->invoiceeEmail1 = $tax_email;// 공급받는자 담당자 메일주소
        $Taxinvoice->invoiceeEmail2=$tax_email2;


        $Taxinvoice->detailList = array();

        $Taxinvoice->detailList[] = new TaxinvoiceDetail();
        $Taxinvoice->detailList[0]->serialNum = 1;				      // [상세항목 배열이 있는 경우 필수] 일련번호 1~99까지 순차기재,
        $Taxinvoice->detailList[0]->purchaseDT = date("Ymd");	  // 거래일자
        $Taxinvoice->detailList[0]->itemName = '수수료';	  	// 품명
        $Taxinvoice->detailList[0]->spec = '';				      // 규격
        $Taxinvoice->detailList[0]->qty = '';					        // 수량
        $Taxinvoice->detailList[0]->unitCost = '';		    // 단가
        $Taxinvoice->detailList[0]->supplyCost = $origin;		  // 공급가액
        $Taxinvoice->detailList[0]->tax = $vat;				      // 세액
        $Taxinvoice->detailList[0]->remark = '';		    // 비고


    if($sumrentfee>0){

            $memo=" ";
            try {
                $result = $TaxinvoiceService->RegistIssue($mycorpnum, $Taxinvoice, $UserID,
                    false, false, $memo, '', '');
                $tax_code = $result->code;
                $tax_msg = $result->message;
                $stats="y";
            }
            catch(PopbillException $pe) {
                $tax_code = $pe->getCode();
                $tax_msg = $pe->getMessage();
                $stats="x";
            }

            
            // 세금계산서 로그등록
            $sql="INSERT INTO calculate_taxpaperlog (  taxkey,    mycorpnum,  mycorpname,  myceoname,
                    myaddr,  myemail,  amounttotal,  totalamount,  clientcorpnum,  clientcorpname,  clientceoname,  clientcontactname,
                    clientaddr,  clientbiztype,  clientbizclass,  stats,  regdate,  vattotal,  calbranch_idx,clientemail
                    , mybiztype, mybizclass,tax_code,tax_msg)
                    VALUES
                    ( '$invoicerMgtKey',    '$mycorpnum',    '$mycorpname',    '$myceoname',    '$myaddr',    '$myemail',
                    '$origin',    '$sumrentfee',    '$registration_number',    '$clientcorpname',    '$clientceo_name',
                    '$clientceo_name',    '$clientaddr',    '$clientbiztype',    '$clientbizclass',   '$stats',
                    now(),    '$vat',    '$calbranch_idx' ,'$tax_email', '$mybiztype','$mybizclass' , '$tax_code','$tax_msg' );";


            $logquery = $conn -> prepare($sql);
            $logquery -> execute();

            // caculate_calbranch 업데이트
            $sql="update caculate_calbranch   set taxmail_yn='$stats',tax_code='$tax_code',tax_msg='$tax_msg' where calbranch_idx='$calbranch_idx' " ;
             $branchquery = $conn -> prepare($sql);
            $branchquery -> execute();
        }


}
?>
<body>
Response.code : <?php echo $tax_code ?></p>
Response.message : <?php echo $tax_msg ?></p>
</body>
</html>

